//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using log4net;
using myVRM.DataLayer;
using System.Linq; 
using System.Xml.Linq;
using ns_SqlHelper;
using System.Data;
//
// NOTE Exception handling has changed. 
// All the exceptions in this section have to be reworked. See any other factory!!!!!!
//(must return false instaed of throwing e. 
namespace myVRM.BusinessLayer
{
    #region Class WorkOrderFactory
    /// <summary>
    /// Data Layer Logic for loading/saving Reports(Templates/Schedule)
    /// </summary>
    /// 
    public class WorkOrderFactory
    {
        #region Private Members

        private WorkOrderDAO m_woDAO;
        private InvCategoryDAO m_InvCategoryDAO;
        private IAVItemDAO m_IAVItemDAO;
        private ICAItemDAO m_ICAItemDAO;
        private IHKItemDAO m_IHKItemDAO;
        private InvListDAO m_InvListDAO;
        private InvRoomDAO m_InvRoomDAO;
        private InvWorkOrderDAO m_InvWorkOrderDAO;
        private InvWorkItemDAO m_InvWorkItemDAO;
        private InvWorkChargeDAO m_InvWorkChargeDAO;
        private IConferenceDAO m_IconfDAO;
        private IUserDeptDao m_IuserDeptDAO;

        private conferenceDAO m_confDAO;

        private userDAO m_usrDAO;
        private IUserDao m_vrmUserDAO;

        private emailFactory m_woEmail;
        private UtilFactory m_UtilFactory; //FB 2236
        private IConfRoomDAO m_IconfRoom; //FB 2817
        private LocationDAO m_locationDAO;
        private IRoomDAO m_roomDAO;
        private deptDAO m_deptDAO;

        private static log4net.ILog m_log;
        private string m_configPath;

        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private OrgData orgInfo;
        private const int defaultOrgId = 11;  //Default organization
        private int organizationID = 0;
        private int multiDepts = 1;

        private imageDAO m_imageDAO; //Image Project
        private IImageDAO m_IImageDAO;
        vrmFactory vrmFact = null;
        private myVRMException myvrmEx; //FB 1881
        imageFactory vrmImg = null; //FB 2136

        //FB 2214 Starts
        XmlWriter xWriter = null;
        XmlWriterSettings xSettings = null;
        XPathNavigator xNavigator = null;
        XPathDocument xDoc = null;
        StringReader xStrReader = null;
        XPathNavigator xNode = null;
        StringBuilder OrgOUTXML = null;
        //FB 2214 Ends
        private SqlHelper sqlCon = null; //ZD 101835
        #endregion

        #region WorkOrderFactory
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        public WorkOrderFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;

                m_woDAO = new WorkOrderDAO(obj.ConfigPath, obj.log);
                m_usrDAO = new userDAO(obj.ConfigPath, obj.log);
                m_vrmUserDAO = m_usrDAO.GetUserDao();
                m_InvCategoryDAO = m_woDAO.GetCategoryDAO();
                m_IAVItemDAO = m_woDAO.GetAVItemDAO();
                m_ICAItemDAO = m_woDAO.GetCAItemDAO();
                m_IHKItemDAO = m_woDAO.GetHKItemDAO();
                m_InvListDAO = m_woDAO.GetInvListDAO();
                m_InvWorkOrderDAO = m_woDAO.GetWorkOrderDAO();
                m_InvWorkItemDAO = m_woDAO.GetWorkItemDAO();
                m_InvWorkChargeDAO = m_woDAO.GetWorkChargeDAO();
                m_InvRoomDAO = m_woDAO.GetInvRoomDAO();

                m_locationDAO = new LocationDAO(obj.ConfigPath, obj.log);
                m_roomDAO = m_locationDAO.GetRoomDAO();
                m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log);
                m_IconfDAO = m_confDAO.GetConferenceDao();
                m_woEmail = new emailFactory(ref obj);
                m_deptDAO = new deptDAO(obj.ConfigPath, obj.log);
                m_IuserDeptDAO = m_deptDAO.GetUserDeptDao();
                m_UtilFactory = new UtilFactory(ref obj); //FB 2236
                m_OrgDAO = new orgDAO(obj.ConfigPath, obj.log); //Organization Module Fixes
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();
                m_IconfRoom = m_confDAO.GetConfRoomDao(); //FB 2817
                m_imageDAO = new imageDAO(obj.ConfigPath, obj.log); //Image Project
                m_IImageDAO = m_imageDAO.GetImageDao();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetInventoryDetails
        /// <summary>q
        /// Get All Report templates
        /// </summary>
        /// <returns>Complete list of report templates</returns>
        public bool GetInventoryDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            InventoryCategory ic = null;
            vrmFact = new vrmFactory(ref obj); //Image Project
            vrmImg = new imageFactory(ref obj); //FB 2136
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/ID");
                string InventoryID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/Type");
                string Type = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/StartByDate");
                string StartByDate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/StartByTime");
                string StartByTime = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/CompleteByDate");
                string CompleteByDate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/CompleteByTime");
                string CompleteByTime = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/WorkorderID");
                string WorkorderID = node.InnerXml.Trim();
                if (WorkorderID.Length == 0)
                    WorkorderID = "0";

                string ConfDuration = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/Timezone");

                string ConfTimezone = node.InnerXml.Trim();

                DateTime confFrom, confTo;

                int iType;
                obj.outXml = "<Inventory>";
                if (InventoryID.ToLower().CompareTo("new") == 0)
                {
                    // this is valid. Return an empty list
                    obj.outXml += "<ItemList></ItemList>";
                }
                else
                {
                    iType = Int32.Parse(Type);

                    ic = m_InvCategoryDAO.GetById(Int32.Parse(InventoryID));
                    if (ic != null)
                    {
                        obj.outXml += GetCategoryXml(ic);

                        switch (iType)
                        {
                            case woConstant.CATEGORY_TYPE_AV:
                                if (StartByDate.Length > 1)
                                {
                                    confFrom = DateTime.Parse(StartByDate + " " + StartByTime);
                                    confTo = DateTime.Parse(CompleteByDate + " " + CompleteByTime); ;

                                    timeZone.changeToGMTTime(Int32.Parse(ConfTimezone), ref confFrom);
                                    timeZone.changeToGMTTime(Int32.Parse(ConfTimezone), ref confTo);
                                }
                                else
                                {
                                    confFrom = new DateTime(0);
                                    confTo = new DateTime(0);
                                }
                                XmlNodeList PreselectedItems =
                                xd.GetElementsByTagName("Item");

                                obj.outXml += GetAVInventoryDetails(ic, confFrom, confTo, PreselectedItems, WorkorderID);
                                break;
                            case woConstant.CATEGORY_TYPE_CA:
                                obj.outXml += GetCAInventoryDetails(ic);
                                break;
                            case woConstant.CATEGORY_TYPE_HK:
                                obj.outXml += GetHKInventoryDetails(ic);
                                break;
                        }
                    }
                }
                obj.outXml += GetCategoryRooms(ic, userID);

                obj.outXml += "</Inventory>";

            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }

            return bRet;
        }
        #endregion

        #region GetAVInventoryDetails
        /// <summary>
        /// GetAVInventoryDetails
        /// </summary>
        /// <param name="ic"></param>
        /// <param name="confFrom"></param>
        /// <param name="confTo"></param>
        /// <param name="PreselectedItems"></param>
        /// <param name="WorkOrderId"></param>
        /// <returns></returns>
        private string GetAVInventoryDetails(InventoryCategory ic, DateTime confFrom,
                                             DateTime confTo, XmlNodeList PreselectedItems, string WorkOrderId)
        {
            string outXml;
            int quantity = 0;
            int invQty = 0;
            int iID = 0;
            int iQty = 0;

            try
            {
                //
                // build hastable with inventory usage0
                //

                Hashtable runningTotal = new Hashtable();

                foreach (XmlNode innerNode in PreselectedItems)
                {

                    XmlElement itemElement = (XmlElement)innerNode;
                    iID = Int32.Parse(itemElement.GetElementsByTagName("ID")[0].InnerText);
                    iQty = Int32.Parse(itemElement.GetElementsByTagName("Quantity")[0].InnerText);

                    if (runningTotal.ContainsKey(iID))
                    {
                        iQty += (int)runningTotal[iID];
                        runningTotal.Remove(iID);
                        runningTotal.Add(iID, iQty); //FB 1327 Work Order Fixes
                    }
                    else
                        runningTotal.Add(iID, iQty);
                }
                outXml = "<ItemList>";

                foreach (AVInventoryItemList CatItem in ic.AVItemList)
                {
                    if (CatItem.deleted == 0)
                    {
                        vrmImage imObj = m_IImageDAO.GetById(CatItem.ImageId);

                        string imageDt = "";
                        if (imObj != null)
                            imageDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage);  //FB 2136

                        outXml += "<Item>";
                        outXml += "<ID>" + CatItem.ID.ToString() + "</ID> ";
                        outXml += "<Name>" + CatItem.name.ToString() + "</Name> ";
                        outXml += "<ImageId>" + CatItem.ImageId + "</ImageId>"; //Image Project
                        outXml += "<ImageName>" + CatItem.image + "</ImageName>";
                        outXml += "<Image>" + imageDt + "</Image>";
                        quantity = CatItem.quantity;

                        invQty = 0;

                        DateTime compareDate = new DateTime(0); // compare to null
                        // should be = to DateTime.MinValue()

                        if (confFrom > compareDate)
                        {
                            if (GetInventoryQty(CatItem.ID, 0, confFrom, confTo, ref invQty, WorkOrderId))
                            {
                                quantity -= invQty;
                                if (runningTotal.ContainsKey(CatItem.ID))
                                {
                                    int adjust = (int)runningTotal[CatItem.ID];
                                    quantity -= adjust;
                                }
                                if (quantity < 0)
                                    quantity = 0;

                            }
                            else
                            {
                                //FB 1881 start
                                //outXml = myVRMException.toXml("Error in calculating real time inventory");
                                myvrmEx = new myVRMException(500);
                                outXml = myvrmEx.FetchErrorMsg();
                                //FB 1881 end

                                return outXml;
                            }
                        }
                        outXml += "<Quantity>" + quantity.ToString() + "</Quantity>";
                        //outXml += "<Price>" + CatItem.price.ToString() + "</Price>";
                        outXml += "<Price>" + Decimal.Parse(CatItem.price.ToString()).ToString("0.00") + "</Price>"; // FB 1686
                        outXml += "<SerialNumber>" + CatItem.serialNumber.ToString() + "</SerialNumber> ";
                        //outXml += "<Image>" + CatItem.image.ToString() + "</Image>";
                        outXml += "<Portable>" + CatItem.portable.ToString() + "</Portable>";
                        outXml += "<Comments>" + CatItem.comment.ToString() + "</Comments>";
                        outXml += "<Description>" + CatItem.description + "</Description>";
                        outXml += "<ServiceCharge>" + CatItem.serviceCharge.ToString() + "</ServiceCharge>";
                        outXml += "<DeliveryType>" + CatItem.deliveryType.ToString() + "</DeliveryType>";
                        outXml += "<DeliveryCost>" + CatItem.deliveryCost.ToString() + "</DeliveryCost>";
                        outXml += "<Deleted>" + CatItem.deleted.ToString() + "</Deleted>";

                        outXml += "<Charges>";
                        foreach (WorkItemCharge wc in CatItem.ItemCharge)
                        {
                            outXml += "<Charge>";
                            outXml += "<DeliveryTypeID>" + wc.IDeliveryType.id.ToString() + "</DeliveryTypeID>";
                            outXml += "<DeliveryName>" + wc.IDeliveryType.deliveryType + "</DeliveryName>";
                            outXml += "<DeliveryCost>" + wc.deliveryCharge.ToString("###0.00") + "</DeliveryCost>";
                            outXml += "<ServiceCharge>" + wc.serviceCharge.ToString("###0.00") + "</ServiceCharge>";
                            outXml += "</Charge>";
                        }
                        outXml += "</Charges>";
                        outXml += "</Item>";
                    }
                }
                outXml += "</ItemList>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }

            return outXml;
        }
        #endregion

        #region GetCAInventoryDetails
        /// <summary>
        /// GetCAInventoryDetails
        /// </summary>
        /// <param name="ic"></param>
        /// <returns></returns>
        private string GetCAInventoryDetails(InventoryCategory ic)
        {
            string outXml;
            try
            {
                outXml = "<ItemList>";

                //    foreach (CAInventoryItemList CatItem in ic.CAItemList)
                //    {
                //        if (CatItem.deleted == 0)
                //        {
                //            outXml += "<Item>";
                //            outXml += "<ID>" + CatItem.ID.ToString() + "</ID> ";
                //            outXml += "<Name>" + CatItem.name.ToString() + "</Name> ";
                //            outXml += "<Quantity>" + CatItem.quantity.ToString() + "</Quantity>";
                //            outXml += "<Price>" + CatItem.price.ToString() + "</Price>";
                //            outXml += "<SerialNumber>" + CatItem.serialNumber.ToString() +
                //                "</SerialNumber> ";
                //            outXml += "<Image>" + CatItem.image.ToString() + "</Image>";
                //            outXml += "<Portable>" + CatItem.portable.ToString() + "</Portable>";
                //            outXml += "<Comments>" + CatItem.comment.ToString() + "</Comments>";
                //            outXml += "<Description>" + CatItem.description + "</Description>";
                //            outXml += "<ServiceCharge>" + CatItem.serviceCharge.ToString() + "</ServiceCharge>";
                //            outXml += "<DeliveryType>" + CatItem.deliveryType.ToString() + "</DeliveryType>";
                //            outXml += "<DeliveryCost>" + CatItem.deliveryCost.ToString() + "</DeliveryCost>";
                //            outXml += "<Deleted>" + CatItem.deleted.ToString() + "</Deleted>";
                //            outXml += "</Item>";
                //        }
                //    }
                //    outXml += "</ItemList>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }

            return outXml;
        }
        #endregion

        #region GetHKInventoryDetails
        /// <summary>
        /// GetHKInventoryDetails
        /// </summary>
        /// <param name="ic"></param>
        /// <returns></returns>
        private string GetHKInventoryDetails(InventoryCategory ic)
        {
            string outXml;
            try
            {
                outXml = "<ItemList>";

                foreach (HKInventoryItemList CatItem in ic.HKItemList)
                {
                    if (CatItem.deleted == 0)
                    {
                        vrmImage imObj = m_IImageDAO.GetById(CatItem.ImageId);

                        string imageDt = "";
                        if (imObj != null)
                            imageDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage);  //FB 2136

                        outXml += "<Item>";
                        outXml += "<ID>" + CatItem.ID.ToString() + "</ID> ";
                        outXml += "<Name>" + CatItem.name.ToString() + "</Name> ";
                        outXml += "<Quantity>" + CatItem.quantity.ToString() + "</Quantity>";
                        //outXml += "<Price>" + CatItem.price.ToString() + "</Price>";
                        outXml += "<Price>" + Decimal.Parse(CatItem.price.ToString()).ToString("0.00") + "</Price>"; //FB 1686
                        outXml += "<SerialNumber>" + CatItem.serialNumber.ToString() +
                            "</SerialNumber> ";

                        outXml += "<ImageId>" + CatItem.ImageId + "</ImageId>"; //Image Project
                        outXml += "<ImageName>" + CatItem.image + "</ImageName>";
                        outXml += "<Image>" + imageDt + "</Image>";

                        outXml += "<Portable>" + CatItem.portable.ToString() + "</Portable>";
                        outXml += "<Comments>" + CatItem.comment.ToString() + "</Comments>";
                        outXml += "<Description>" + CatItem.description + "</Description>";
                        outXml += "<ServiceCharge>" + CatItem.serviceCharge.ToString() + "</ServiceCharge>";
                        outXml += "<DeliveryType>" + CatItem.deliveryType.ToString() + "</DeliveryType>";
                        outXml += "<DeliveryCost>" + CatItem.deliveryCost.ToString() + "</DeliveryCost>";
                        outXml += "<Deleted>" + CatItem.deleted.ToString() + "</Deleted>";
                        outXml += "</Item>";
                    }
                }
                outXml += "</ItemList>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }

            return outXml;
        }
        #endregion

        #region GetCategoryXml
        /// <summary>
        /// GetCategoryXml
        /// </summary>
        /// <param name="ic"></param>
        /// <returns></returns>
        private string GetCategoryXml(InventoryCategory ic)
        {
            string outXml;
            try
            {
                outXml = "<ID>" + ic.ID.ToString() + "</ID> ";
                outXml += "<Type>" + ic.Type.ToString() + "</Type> ";
                outXml += "<Name>" + ic.Name.ToString() + "</Name> ";
                outXml += "<Comments>" + ic.Comment.ToString() + "</Comments> ";
                outXml += "<Admin>";
                outXml += "<ID>" + ic.AdminID.ToString() + "</ID>";
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userid", ic.AdminID));
                List<vrmUser> myVrmUser = m_vrmUserDAO.GetByCriteria(criterionList);
                outXml += "<AssignedToName>" + myVrmUser[0].FirstName + " "
                                        + myVrmUser[0].LastName + "</AssignedToName>";
                outXml += "</Admin>";
                outXml += "<Notify>" + ic.Notify.ToString() + "</Notify> ";
                outXml += "<AssignedCostCtr>" + ic.AssignedCostCtr.ToString() + "</AssignedCostCtr>";
                outXml += "<AllowOverBooking>" + ic.AssignedCostCtr.ToString() + "</AllowOverBooking> ";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return outXml;
        }
        #endregion

        #region GetCategoryRooms
        /// <summary>
        /// GetCategoryRooms
        /// </summary>
        /// <param name="ic"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        private string GetCategoryRooms(InventoryCategory ic, string userID)
        {
            string outXml;
            try
            {
                outXml = "<locationList>";

                outXml += "<selected>";

                if (ic != null)
                {
                    foreach (InventoryRoom CatRoom in ic.RoomList)
                    {
                        outXml += "<level1ID>" + CatRoom.IRoom.roomId.ToString() + "</level1ID> ";
                    }
                }
                outXml += "</selected>";

                // removed for performence reasons. (01/14/07)
                // myVRMSearch search = new myVRMSearch(m_configPath, m_log);

                // search.GetRoomList(userID, ref  outXml);

                outXml += "</locationList>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return outXml;
        }
        #endregion

        #region GetInventoryList
        /// <summary>
        /// GetInventoryList
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetInventoryList(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/Type");
                string sType = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                int iType = Int32.Parse(sType);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Type", iType));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID)); //Organization Module Fixes

                List<InventoryCategory> InvList = m_InvCategoryDAO.GetByCriteria(criterionList);

                if (InvList.Count > 0)
                {
                    obj.outXml = "<InventoryList>";
                    obj.outXml += "<Type>" + iType.ToString() + "</Type>";
                    foreach (InventoryCategory ic in InvList)
                    {
                        obj.outXml += "<Inventory>";
                        obj.outXml += "<ID>" + ic.ID.ToString() + "</ID>";
                        obj.outXml += "<Name>" + ic.Name.ToString() + "</Name>";
                        obj.outXml += "<Comment>" + ic.Comment.ToString() + "</Comment>";
                        obj.outXml += "<AdminID>" + ic.AdminID.ToString() + "</AdminID>";
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("userid", ic.AdminID));
                        List<vrmUser> myVrmUser = m_vrmUserDAO.GetByCriteria(criterionList);
                        obj.outXml += "<AssignedToName>" + myVrmUser[0].FirstName + " "
                                                + myVrmUser[0].LastName + "</AssignedToName>";
                        obj.outXml += "<Notify>" + ic.Notify.ToString() + "</Notify>";
                        obj.outXml += "<AssignedCostCtr>" + ic.AssignedCostCtr.ToString() + "</AssignedCostCtr>";
                        obj.outXml += "<AllowOverBooking>" + ic.AllowOverBooking.ToString() + "</AllowOverBooking>";

                        obj.outXml += "<locationList>";
                        foreach (InventoryRoom ir in ic.RoomList)
                        {
                            if (ir.IRoom.disabled == 0 && ir.IRoom.roomId != 11)
                            {
                                obj.outXml += "<selected>";
                                obj.outXml += "<level1ID>" + ir.IRoom.roomId.ToString() + "</level1ID>";
                                obj.outXml += "<level1Name>" + ir.IRoom.Name + "</level1Name>";
                                obj.outXml += "</selected>";
                            }
                        }
                        obj.outXml += "</locationList>";
                        obj.outXml += "</Inventory>";
                    }
                    obj.outXml += "</InventoryList>";

                }
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            return bRet;
        }
        #endregion

        #region GetItems
        /// <summary>
        /// GetItems
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetItems(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/Type");
                string Type = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Type", Int32.Parse(Type)));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));

                List<InventoryList> InvList = m_InvListDAO.GetByCriteria(criterionList);

                if (InvList.Count > 0)
                {
                    vrmFact = new vrmFactory(ref obj);
                    vrmImg = new imageFactory(ref obj); //FB 2136

                    obj.outXml = "<ItemList>";
                    obj.outXml += "<Type>" + Type + "</Type>";

                    foreach (InventoryList it in InvList)
                    {
                        if (it.ImageId <= 0)
                            continue;

                        vrmImage imgObj = m_IImageDAO.GetById(it.ImageId);
                        if (imgObj == null)
                            continue;

                        string imgcontent = vrmImg.ConvertByteArrToBase64(imgObj.AttributeImage);  //FB 2136

                        obj.outXml += "<Item>";
                        obj.outXml += "<ID>" + it.ID.ToString() + "</ID>";
                        obj.outXml += "<Name>" + it.Name.ToString() + "</Name>";
                        obj.outXml += "<ImageName>" + it.Image + "</ImageName>"; //ImageFullName
                        obj.outXml += "<ImageId>" + it.ImageId + "</ImageId>"; //Image Project

                        //ZD 100175
                        if (it.ImgResized == 0)
                        {
                            byte[] imageArr = vrmImg.ConvertBase64ToByteArray(imgcontent);

                            System.IO.MemoryStream myMemStream = new System.IO.MemoryStream(imageArr);
                            System.Drawing.Image fullsizeImage = System.Drawing.Image.FromStream(myMemStream);
                            System.Drawing.Image newImage = fullsizeImage.GetThumbnailImage(250, 250, null, IntPtr.Zero); //ZD 102701
                            MemoryStream ms = new MemoryStream();
                            newImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            byte[] imgArray = ms.ToArray();
                            imgcontent = vrmImg.ConvertByteArrToBase64(imgArray);
                            //imgcontent = imgArray.ToString();

                            imgObj.AttributeImage = imgArray;
                            m_IImageDAO.Update(imgObj);

                            it.ImgResized = 1;
                            m_InvListDAO.Update(it);
                        }

                        obj.outXml += "<Image>" + imgcontent + "</Image>";
                        obj.outXml += "</Item>";
                    }
                    obj.outXml += "</ItemList>";

                }
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }

            return bRet;
        }
        #endregion

        #region GetWorkOrderDetails
        /// <summary>
        /// GetWorkOrderDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetWorkOrderDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmFact = new vrmFactory(ref obj); //Image Project
            vrmImg = new imageFactory(ref obj); //FB 2136
            vrmImage imObj = null, imObjCA = null, imObjWO = null;
            int roomLyoutID = 0, woTimeZone = 26, utcEnabled = 0, Work0rderID = 0;
            int quantity = 0, AVQty = 0;
            string imgData = "", userID = "", imgDataCA = "";
            string[] idArr = null, nameArr = null;
            string WorkOrderID = string.Empty;
            OrgOUTXML = new StringBuilder();
            DateTime startBy = DateTime.Now;
            DateTime completeBy = DateTime.Now;
            List<ICriterion> criterionList = null;
            List<vrmUser> myVrmUser = null;
            WorkOrder work = null;
            WorkItem it = null;
            XmlNode node;
            XmlDocument xd = null;
            AVInventoryItemList AV = null;
            CAInventoryItemList CA = null;
            HKInventoryItemList HK = null;

            xSettings = null;
            obj.outXml = "";
            try
            {
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                node = xd.SelectSingleNode("//login/userID");
                userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/WorkorderID");
                Work0rderID = Int32.Parse(node.InnerXml.Trim());


                if (xd.SelectSingleNode("//login/utcEnabled") != null)
                    int.TryParse(node.SelectSingleNode("//login/utcEnabled").InnerText.Trim(), out utcEnabled);


                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        if (xd.SelectSingleNode("//login/utcEnabled") != null)
                            int.TryParse(node.SelectSingleNode("//login/utcEnabled").InnerText.Trim(), out utcEnabled);

                        xNode = xNavigator.SelectSingleNode("//login/userID");
                        userID = xNode.Value.Trim();

                        xNode = xNavigator.SelectSingleNode("//login/WorkorderID");
                        int.TryParse(xNode.Value.Trim(), out Work0rderID);

                        xNode = xNavigator.SelectSingleNode("//login/utcEnabled");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out utcEnabled);
                    }
                }
                work = m_InvWorkOrderDAO.GetById(Work0rderID);

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    xWriter.WriteStartElement("WorkOrder");
                    xWriter.WriteElementString("ID", work.ID.ToString());
                    xWriter.WriteElementString("Name", work.Name.ToString());
                    xWriter.WriteElementString("Type", work.Type.ToString());

                    xWriter.WriteElementString("SetID", work.IC.ID.ToString());
                    xWriter.WriteElementString("SetName", work.IC.Name);

                    xWriter.WriteElementString("RoomID", work.IRoom.roomId.ToString());
                    xWriter.WriteElementString("RoomName", work.IRoom.Name);
                    //FB 2214 Starts
                    if (work.strData != "")
                    {
                        if (work.IRoom.RoomImage != null)//ZD 100237
                        {
                            if (work.IRoom.RoomImage.Contains(work.strData))
                            {
                                idArr = work.IRoom.RoomImageId.Split(',');
                                nameArr = work.IRoom.RoomImage.Split(',');

                                for (int i = 0; i < idArr.Length; i++)
                                {
                                    if (nameArr[i] == work.strData)
                                        int.TryParse(idArr[i], out roomLyoutID);
                                }
                                if (roomLyoutID > 0)
                                    imObjWO = m_IImageDAO.GetById(roomLyoutID);

                                if (imObjWO != null)
                                    imgData = vrmImg.ConvertByteArrToBase64(imObjWO.AttributeImage);
                            }
                        }
                    }
                    //FB 2214 Ends

                    xWriter.WriteElementString("RoomLayout", work.strData);
                    xWriter.WriteElementString("RoomLayoutData", imgData);

                    xWriter.WriteElementString("AssignedToID", work.AdminID.ToString());

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", work.AdminID));
                    myVrmUser = m_vrmUserDAO.GetByCriteria(criterionList);
                    xWriter.WriteElementString("AssignedToName", myVrmUser[0].FirstName + " "
                                            + myVrmUser[0].LastName);

                    xWriter.WriteElementString("Timezone", work.woTimeZone.ToString());

                    woTimeZone = work.woTimeZone;
                    startBy = work.startBy;
                    completeBy = work.CompletedBy;

                    //FB 2014
                    if (utcEnabled != 1)
                    {
                        timeZone.userPreferedTime(woTimeZone, ref startBy);
                        timeZone.userPreferedTime(woTimeZone, ref completeBy);
                    }

                    xWriter.WriteElementString("StartByDate", startBy.ToString("d"));
                    xWriter.WriteElementString("StartByTime", startBy.ToString("t"));
                    xWriter.WriteElementString("CompletedByDate", completeBy.ToString("d"));
                    xWriter.WriteElementString("CompletedByTime", completeBy.ToString("t"));
                    xWriter.WriteElementString("Status", work.Status.ToString());
                    xWriter.WriteElementString("Comments", work.Comment);
                    xWriter.WriteElementString("Description", m_UtilFactory.ReplaceOutXMLSpecialCharacters(work.description));
                    xWriter.WriteElementString("ServiceCharge", work.serviceCharge.ToString());
                    xWriter.WriteElementString("DeliveryType", work.deliveryType.ToString());
                    xWriter.WriteElementString("DeliveryCost", work.deliveryCost.ToString());
                    xWriter.WriteElementString("Deleted", work.deleted.ToString());
                    xWriter.WriteElementString("Notify", work.Notify.ToString());
                    xWriter.WriteElementString("Reminder", work.Reminder.ToString());
                    xWriter.WriteElementString("TotalCost", work.woTtlCost.ToString());
                    xWriter.WriteElementString("TotalServiceCost", work.woTtlSrvceCost.ToString());//ZD 100237
                    xWriter.WriteElementString("TotalDeliveryCost", work.woTtlDlvryCost.ToString()); //ZD 100237
                    xWriter.WriteStartElement("ItemList");

                    WorkOrderID = string.Empty;

                    for (int wo = 0; wo < work.ItemList.Count; wo++)
                    {
                        it = (WorkItem)work.ItemList[wo];

                        if (it.deleted != 1)     // WO bug
                        {
                            xWriter.WriteStartElement("Item");
                            xWriter.WriteElementString("ID", it.ItemID.ToString());
                            xWriter.WriteElementString("UID", it.ID.ToString());
                            xWriter.WriteElementString("QuantityRequested", it.quantity.ToString());

                            switch (work.Type)
                            {
                                case woConstant.CATEGORY_TYPE_AV:  // Audio visual 
                                    AV = m_IAVItemDAO.GetById(it.ItemID);
                                    if (AV != null)
                                    {
                                        imObj = m_IImageDAO.GetById(AV.ImageId);
                                        if (imObj != null)
                                            imgData = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136

                                        xWriter.WriteElementString("Name", AV.name);
                                        xWriter.WriteElementString("Price", AV.price.ToString());
                                        xWriter.WriteElementString("SerialNumber", AV.serialNumber);

                                        xWriter.WriteElementString("ImageId", AV.image);
                                        xWriter.WriteElementString("ImageName", AV.image);
                                        xWriter.WriteElementString("Image", imgData);

                                        xWriter.WriteElementString("Portable", AV.portable.ToString());
                                        xWriter.WriteElementString("Comments", AV.comment);
                                        xWriter.WriteElementString("Description", AV.description);
                                        AVQty = 0;
                                        if (work.ConfID == vrmConfType.Phantom)
                                        {
                                            if (!GetInventoryQty(AV.ID, work.ID, work.startBy, work.CompletedBy, ref AVQty, WorkOrderID))
                                            {
                                                myvrmEx = new myVRMException(500);
                                                obj.outXml = myvrmEx.FetchErrorMsg();
                                                return false;
                                            }
                                        }
                                        else
                                        {
                                            // FB 322 Work Order Fixes
                                            if (!GetInventoryQty(AV.ID, work.ID, work.ConfID, work.InstanceID, ref AVQty))
                                            {
                                                myvrmEx = new myVRMException(500);
                                                obj.outXml = myvrmEx.FetchErrorMsg();
                                                return false;
                                            }
                                        }
                                        quantity = AV.quantity - AVQty;
                                        if (quantity < 0) // To Handle if Quantity is Less than Zero
                                            quantity = 0;
                                        xWriter.WriteElementString("Quantity", quantity.ToString());
                                    }
                                    break;

                                case myVRM.DataLayer.woConstant.CATEGORY_TYPE_CA:  // Audio visual 
                                    CA = m_ICAItemDAO.GetById(it.ItemID);
                                    if (CA != null)
                                    {
                                        xWriter.WriteElementString("Name", CA.name);
                                        xWriter.WriteElementString("Price", CA.price.ToString());
                                        xWriter.WriteElementString("SerialNumber", CA.serialNumber);

                                        xWriter.WriteElementString("Image", CA.image);
                                        xWriter.WriteElementString("ImageName", CA.image);
                                        xWriter.WriteElementString("Portable", CA.portable.ToString());
                                        xWriter.WriteElementString("Comments", CA.comment);
                                        xWriter.WriteElementString("Description", CA.description);
                                        xWriter.WriteElementString("Quantity", CA.quantity.ToString());
                                    }
                                    break;

                                case woConstant.CATEGORY_TYPE_HK:  // Audio visual 
                                    HK = m_IHKItemDAO.GetById(it.ItemID);
                                    if (HK != null)
                                    {
                                        xWriter.WriteElementString("Name", HK.name);
                                        xWriter.WriteElementString("Price", HK.price.ToString());
                                        xWriter.WriteElementString("SerialNumber", HK.serialNumber);

                                        imgDataCA = ""; //Code added fro WO Bug
                                        imObjCA = m_IImageDAO.GetById(HK.ImageId);
                                        if (imObjCA != null)
                                            imgDataCA = vrmImg.ConvertByteArrToBase64(imObjCA.AttributeImage); //FB 2136

                                        xWriter.WriteElementString("Image", imgDataCA);
                                        xWriter.WriteElementString("ImageName", HK.image);
                                        xWriter.WriteElementString("Portable", HK.portable.ToString());
                                        xWriter.WriteElementString("Comments", HK.comment);
                                        xWriter.WriteElementString("Description", HK.description);
                                        xWriter.WriteElementString("Quantity", HK.quantity.ToString());
                                        xWriter.WriteElementString("RoomLayout", it.IWork.strData);
                                    }
                                    break;
                            }
                            xWriter.WriteElementString("ServiceCharge", it.serviceCharge.ToString());
                            xWriter.WriteElementString("DeliveryType", it.deliveryType.ToString());
                            xWriter.WriteElementString("DeliveryCost", it.deliveryCost.ToString());
                            xWriter.WriteFullEndElement(); //ZD 100237
                        }
                    }
                    xWriter.WriteFullEndElement();//ZD 100237
                    xWriter.WriteFullEndElement();//ZD 100237
                }
                xWriter.Flush();
                obj.outXml = OrgOUTXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
            return bRet;
        }
        #endregion

        #region SearchConferenceWorkOrders
        /// <summary>
        /// SearchConferenceWorkOrders
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SearchConferenceWorkOrders(ref vrmDataObject obj)
        {
            IConferenceDAO m_vrmConfDAO = new conferenceDAO(m_configPath, m_log).GetConferenceDao(); //FB 2075
            List<WorkOrder> WorkList = new List<WorkOrder>();
            bool bRet = true;
            woSearch oSearch = new woSearch();
            //Code added for Fb 1114
            bool ispending = false;
            bool isincomplete = false;
            bool isremoveall = false;
            Hashtable hshConfs = null;
            bool iscomplete = false;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string ownerID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/user/userID");
                string userID = node.InnerXml.Trim();
                int LUserID; //FB 1948
                Int32.TryParse(userID, out LUserID); //FB 1948

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userid", LUserID)); //FB 1948

                List<vrmUser> myVrmUser = m_vrmUserDAO.GetByCriteria(criterionList);
                oSearch.TimeZone = myVrmUser[0].TimeZone;
                oSearch.orgId = organizationID; //organization module

                //FB 2014 
                int utcEnabled = 0;
                if (xd.SelectSingleNode("//login/utcEnabled") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/utcEnabled").InnerText.Trim(), out utcEnabled);

                node = xd.SelectSingleNode("//login/ConfID");
                string confID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/Type");
                string sType = node.InnerXml.Trim();
                if (sType.Length > 0)
                    oSearch.Type = Int32.Parse(sType);

                node = xd.SelectSingleNode("//login/DateFrom");
                oSearch.FromDate = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/TimeFrom");
                if (oSearch.FromDate.Length > 0)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        oSearch.FromDate += " " + node.InnerXml.Trim();
                    else
                        oSearch.FromDate += " 00:00:00 AM";
                }
                node = xd.SelectSingleNode("//login/DateTo");
                oSearch.ToDate = node.InnerXml.Trim();
                if (oSearch.FromDate.Length > 0)
                {
                    node = xd.SelectSingleNode("//login/TimeTo");
                    if (node.InnerXml.Trim().Length > 0)
                        oSearch.ToDate += " " + node.InnerXml.Trim();
                    else
                        oSearch.ToDate += " 12:59:59 PM";
                }
                node = xd.SelectSingleNode("//login/Name");
                if (node.InnerXml.Trim().Length > 0)
                    oSearch.Name = node.InnerXml.Trim();

                XmlNodeList NodeList = xd.SelectNodes(@"/login/Rooms/RoomID");

                foreach (XmlNode Node in NodeList)
                {
                    XmlElement element = (XmlElement)Node;
                    string roomId = element.FirstChild.InnerText;
                    oSearch.Rooms.Add(Int32.Parse(roomId));
                }

                string Status = string.Empty;
                node = xd.SelectSingleNode("//login/Status");
                if (node.InnerXml.Trim().Length > 0)
                    Status = node.InnerXml.Trim();
                if (Status.Length > 0)
                    oSearch.Status = Int32.Parse(Status);
                else
                    oSearch.Status = -1; // nothing

                //
                // implement paging
                //

                int iMaxRecords = 0, ipageNo = 0;
                bool bpageEnable = false;

                node = xd.SelectSingleNode("//login/MaxRecords");
                string MaxRecords = node.InnerXml.Trim();
                if (MaxRecords != string.Empty)
                    iMaxRecords = Int32.Parse(MaxRecords);
                node = xd.SelectSingleNode("//login/pageEnable");
                string pageEnable = node.InnerXml.Trim();
                if (pageEnable != string.Empty)
                    if (Int32.Parse(pageEnable) == 1)
                        bpageEnable = true;
                node = xd.SelectSingleNode("//login/pageNo");
                string pageNo = node.InnerXml.Trim();
                if (pageNo != string.Empty)
                    ipageNo = Int32.Parse(pageNo);
                else
                    ipageNo = 1;

                node = xd.SelectSingleNode("//login/SortBy");
                string sortBy = node.InnerXml.Trim();
                if (sortBy != string.Empty)
                    oSearch.SortBy = Int32.Parse(sortBy);
                if (iMaxRecords > 0)
                    m_InvWorkOrderDAO.pageSize(iMaxRecords);
                m_InvWorkOrderDAO.pageNo(ipageNo);
                //Code added fro FB 1114
                node = xd.SelectSingleNode("//login/Filter");
                string filter = "";

                if (node != null)
                    filter = node.InnerText;

                switch (filter)
                {
                    case "-1":
                        isremoveall = true;
                        break;
                    case "1":
                        ispending = true;
                        isremoveall = false;
                        isincomplete = false;
                        break;
                    case "2":
                        isincomplete = true;
                        ispending = false;
                        isremoveall = false;
                        break;
                    default:
                        isincomplete = false;
                        ispending = false;
                        isremoveall = false;
                        iscomplete = true;
                        break;
                }

                obj.outXml = "<WorkOrderList>";

                if (confID.Trim().Length > 0)
                {
                    CConfID conf = new CConfID(confID);
                    oSearch.confId = conf.ID;
                    oSearch.instance = conf.instance;
                    obj.outXml += "<ConfID>" + oSearch.confId + "</ConfID>";
                }
                else
                {
                    obj.outXml += "<ConfID></ConfID>";
                }
                //FB 2075 Starts
                List<ICriterion> ConfcriterionList;//FB 2075
                ConfcriterionList = new List<ICriterion>();
                if (oSearch.confId > 0) //ZD 100596
                    ConfcriterionList.Add(Expression.Eq("confid", oSearch.confId));

                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                serverTime = serverTime.AddMinutes(-5); // Immd Conf Issue
                ConfcriterionList.Add(Expression.Ge("confdate", serverTime));
                List<vrmConference> confr = m_vrmConfDAO.GetByCriteria(ConfcriterionList, true);
                if (oSearch.instance > 0)
                {
                    if (confr.Count > 1)
                    {
                        confr.RemoveRange(1, confr.Count - 1);
                        oSearch.instance = confr[0].instanceid;
                    }
                }
                //FB 2075 Ends

                if (userID.Trim().Length > 0)
                {
                    oSearch.uId = Int32.Parse(userID);
                }
                if (ownerID.Trim().Length > 0)
                {
                    oSearch.aId = Int32.Parse(ownerID);
                }

                XmlNodeList itemList;

                //node = xd.SelectSingleNode("//login/user/userID");
                //string userID = node.InnerXml.Trim();

                itemList = xd.GetElementsByTagName("RoomID");
                foreach (XmlNode innerNode in itemList)
                {
                    oSearch.Rooms.Add(Int32.Parse(innerNode.InnerXml.Trim()));
                }
                long count = 0;
                if (GetWorkOrder(ref oSearch, ref count, ref WorkList))
                {
                    obj.outXml += "<PageEnable>";
                    if (bpageEnable)
                        obj.outXml += "1";
                    else
                        obj.outXml += "0";

                    long iMaxPage = 1;
                    if (m_InvWorkOrderDAO.getPageSize() > 0)
                    {
                        iMaxPage = count / m_InvWorkOrderDAO.getPageSize();

                        // and modulo remainder...
                        if (count % m_InvWorkOrderDAO.getPageSize() > 0)
                            iMaxPage++;
                    }
                    obj.outXml += "</PageEnable>";
                    obj.outXml += "<PageNo>" + string.Format("{0:d}", ipageNo) + "</PageNo>";
                    obj.outXml += "<TotalPages>" + string.Format("{0:d}", iMaxPage) + "</TotalPages>";
                    obj.outXml += "<TotalRecords>" + string.Format("{0:d}", count) + "</TotalRecords>";

                    hshConfs = new Hashtable();

                    foreach (WorkOrder wk in WorkList)
                    {
                        //Code added for 1272

                        if (wk.ConfID <= 11 && !isremoveall)
                            continue;
                        //ZD 101835
                        vrmConference conferences = new vrmConference();
                        if(wk.woSearchType == null || wk.woSearchType == "")
                            conferences = m_IconfDAO.GetByConfId(wk.ConfID, wk.InstanceID);
                        else
                        {
                            vrmDataObject obj1 = new vrmDataObject(m_configPath);
                            Conference cc = new Conference(ref obj1);
                            List<vrmConference> confList = new List<vrmConference>();
                            List<ICriterion> criterionListNew = new List<ICriterion>();
                            criterionListNew.Add(Expression.Eq("confid", wk.ConfID));
                            criterionListNew.Add(Expression.Eq("instanceid", wk.InstanceID));

                            cc.SearchArchiveConference(ref confList, criterionListNew);
                            if (confList.Count > 0)
                                conferences = confList[0];
                        }

                        if (wk.ConfID > 11)
                        {
                            int cnfstatus = conferences.status;

                            if (!isremoveall)
                            {
                                if ((cnfstatus == vrmConfStatus.Pending && !ispending) || (cnfstatus == vrmConfStatus.Scheduled && ispending))
                                    continue;
                                if (isincomplete)
                                {
                                    if (wk.Status == vrmWorkOrderStatus.Completed)
                                        continue;
                                }

                                if (iscomplete)
                                {
                                    if (wk.Status == vrmWorkOrderStatus.Pending)
                                        continue;
                                }

                            }

                            DateTime testStart = conferences.confdate;
                            timeZone.userPreferedTime(sysSettings.TimeZone, ref testStart);
                            DateTime testEnd = testStart.AddMinutes(conferences.duration);

                            string confsID = wk.ConfID + "," + wk.InstanceID;


                            if (testEnd < DateTime.Now)
                            {
                                if (!hshConfs.Contains(confsID))
                                    hshConfs.Add(confsID, confsID);
                            }

                            if (!hshConfs.Contains(confsID))
                            {

                                hshConfs.Add(confsID, confsID);

                                obj.outXml += "<Conference>";

                                obj.outXml += "<confID>" + conferences.confid + "</confID>";
                                obj.outXml += "<confUniqueID>" + conferences.confnumname + "</confUniqueID>";
                                obj.outXml += "<confName>" + conferences.externalname + "</confName>";

                                DateTime setupTime;
                                DateTime tearDownTime;


                                setupTime = conferences.confdate;
                                tearDownTime = conferences.confdate.AddMinutes(conferences.duration);
                                vrmUser user = m_vrmUserDAO.GetByUserId(Convert.ToInt32(userID));

                                //FB 2014
                                if (utcEnabled != 1)
                                {
                                    timeZone.userPreferedTime(user.TimeZone, ref setupTime);
                                    timeZone.userPreferedTime(user.TimeZone, ref tearDownTime);
                                }

                                obj.outXml += "<confDate>" + setupTime.ToShortDateString() + "</confDate>";
                                obj.outXml += "<confTime1>" + setupTime.ToShortTimeString() + "</confTime1>";

                                timeZoneData tz = new timeZoneData();
                                timeZone.GetTimeZone(conferences.timezone, ref tz);

                                obj.outXml += "<timezone1>" + tz.TimeZone + "</timezone1>";

                                obj.outXml += "</Conference>";

                            }

                        }

                        obj.outXml += "<WorkOrder>";

                        obj.outXml += "<ID>" + wk.ID.ToString() + "</ID>";
                        obj.outXml += "<Name>" + wk.Name.ToString() + "</Name>";
                        obj.outXml += "<Type>" + wk.Type.ToString() + "</Type>";

                        obj.outXml += "<confID>" + string.Format("{0}", wk.ConfID);
                        if (wk.InstanceID > 0)
                            obj.outXml += "," + string.Format("{0}", wk.InstanceID);
                        obj.outXml += "</confID>";

                        obj.outXml += "<SetID>" + wk.IC.ID.ToString() + "</SetID>";
                        obj.outXml += "<SetName>" + wk.IC.Name + "</SetName>";

                        obj.outXml += "<RoomID>" + wk.IRoom.RoomID.ToString() + "</RoomID>";
                        obj.outXml += "<RoomName>" + wk.IRoom.Name + "</RoomName>";

                        obj.outXml += "<RoomLayout>" + wk.strData + "</RoomLayout> "; //FB 2011

                        obj.outXml += "<AssignedToID>" + wk.AdminID.ToString() + "</AssignedToID>";

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("userid", wk.AdminID));
                        string AssignedToName = string.Empty;
                        if (wk.AdminID > 0)
                        {
                            List<vrmUser> admin = m_vrmUserDAO.GetByCriteria(criterionList);
                            AssignedToName = admin[0].FirstName + " " + admin[0].LastName;
                        }
                        obj.outXml += "<AssignedToName>" + AssignedToName + "</AssignedToName>";

                        DateTime CompletedBy = wk.CompletedBy;
                        if (!timeZone.userPreferedTime(oSearch.TimeZone, ref CompletedBy))
                            return false;

                        int woTimeZone = wk.woTimeZone;
                        obj.outXml += "<Timezone>" + wk.woTimeZone.ToString() + "</Timezone>";


                        DateTime startBy = wk.startBy;
                        DateTime completeBy = wk.CompletedBy;

                        //FB 2014
                        if (utcEnabled != 1)
                        {
                            timeZone.userPreferedTime(woTimeZone, ref startBy);
                            timeZone.userPreferedTime(woTimeZone, ref completeBy);
                        }

                        // WO Bug Fix
                        //if (wk.Type == woConstant.CATEGORY_TYPE_CA)
                        //{
                        //    completeBy = startBy.AddMinutes(30);
                        //}
                        obj.outXml += "<StartByDate>" + startBy.ToString("d") + "</StartByDate>";
                        obj.outXml += "<StartByTime>" + startBy.ToString("t") + "</StartByTime>";
                        obj.outXml += "<CompletedByDate>" + completeBy.ToString("d") + "</CompletedByDate>";
                        obj.outXml += "<CompletedByTime>" + completeBy.ToString("t") + "</CompletedByTime>";

                        obj.outXml += "<Status>" + wk.Status.ToString() + "</Status>";
                        obj.outXml += "<Comments>" + wk.Comment + "</Comments>";
                        obj.outXml += "<ServiceCharge>" + wk.serviceCharge.ToString() + "</ServiceCharge>";
                        obj.outXml += "<DeliveryType>" + wk.deliveryType.ToString() + "</DeliveryType>";
                        obj.outXml += "<DeliveryCost>" + wk.deliveryCost.ToString() + "</DeliveryCost>";
                        obj.outXml += "<Deleted>" + wk.deleted.ToString() + "</Deleted>";
                        obj.outXml += "<TotalCost>" + wk.woTtlCost.ToString() + "</TotalCost>";
                        obj.outXml += "<TotalServiceCharge>" + wk.woTtlSrvceCost.ToString() + "</TotalServiceCharge>"; //ZD 100237
                        obj.outXml += "<TotalDeliveryCost>" + wk.woTtlDlvryCost.ToString() + "</TotalDeliveryCost>"; //ZD 100237
                        obj.outXml += "<Notify>" + wk.Notify.ToString() + "</Notify>";

                        obj.outXml += "<Reminder>" + wk.Reminder.ToString() + "</Reminder>";

                        obj.outXml += "</WorkOrder>";
                    }
                    obj.outXml += "</WorkOrderList>";

                }
                else
                {
                    obj.outXml += "<PageEnable></PageEnable>";
                    obj.outXml += "<PageNo></PageNo>";
                    obj.outXml += "<TotalPages></TotalPages>";
                    obj.outXml += "<TotalRecords></TotalRecords>";
                    obj.outXml += "</WorkOrderList>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }

            return bRet;
        }
        #endregion

        #region GetWorkOrder
        /// <summary>
        /// GetWorkOrder
        /// </summary>
        /// <param name="oSearch"></param>
        /// <param name="count"></param>
        /// <param name="WorkList"></param>
        /// <returns></returns>
        private bool GetWorkOrder(ref woSearch oSearch, ref long count, ref List<WorkOrder> WorkList)
        {
            bool bRet = true;
            DateTime dtFrom = new DateTime(0);
            DateTime dtTo = new DateTime(0);

            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", oSearch.orgId)); //organization module
                //FB 2075 Starts
                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);

                criterionList.Add(Expression.Ge("startBy", serverTime));
                //FB 2075 Ends

                if (oSearch.cID > 0)
                {
                    criterionList.Add(Expression.Eq("ID", oSearch.cID));
                }
                else
                {
                    if (oSearch.confId > 0)
                        criterionList.Add(Expression.Eq("ConfID", oSearch.confId));
                    if (oSearch.Type > 0)
                        criterionList.Add(Expression.Eq("Type", oSearch.Type));
                    if (oSearch.instance > 0)
                        criterionList.Add(Expression.Eq("InstanceID", oSearch.instance));
                    if (oSearch.aId > 0)
                    {
                        criterionList.Add(Expression.Eq("AdminID", oSearch.aId));
                        m_InvWorkOrderDAO.addOrderBy(Order.Desc("CompletedBy"));
                    }


                    // if status is - 1 ignore it. 
                    // if it is 2 search for both 0 AND 1
                    //
                    if (oSearch.Status >= 0)
                    {
                        if (oSearch.Status == 2)
                        {
                            criterionList.Add(Expression.Or(
                                Expression.Eq("Status", 0),
                                Expression.Eq("Status", 1)));
                        }
                        else
                            criterionList.Add(Expression.Eq("Status", oSearch.Status));
                    }
                    if (oSearch.Name.Length > 0)
                        criterionList.Add(Expression.Like("Name", "%%" + oSearch.Name.ToLower() + "%%"));

                    if (oSearch.FromDate.Length > 1)
                    {
                        dtFrom = DateTime.Parse(oSearch.FromDate);
                        dtTo = DateTime.Parse(oSearch.ToDate);
                        dtTo = dtTo.AddSeconds(59); // necsssary to guarantee condition

                        if (!timeZone.changeToGMTTime(oSearch.TimeZone, ref dtTo))
                            return false;
                        if (!timeZone.changeToGMTTime(oSearch.TimeZone, ref dtFrom))
                            return false;

                        //FB 2992
                        if (criterionList[1].ToString().IndexOf("startBy") >= 0)
                            criterionList.Remove(criterionList[1]);

                        ICriterion criterium = Expression.Ge("startBy", dtFrom);
                        criterionList.Add(Expression.And(criterium, Expression.Le("startBy", dtTo)));
                        criterium = Expression.Ge("CompletedBy", dtFrom);
                        criterionList.Add(Expression.And(criterium, Expression.Le("CompletedBy", dtTo)));
                    }
                    if (oSearch.Rooms.Count > 0)
                    {
                        criterionList.Add(Expression.In("roomid", oSearch.Rooms));
                    }
                    criterionList.Add(Expression.Eq("deleted", 0));

                    vrmUser user = m_vrmUserDAO.GetByUserId(oSearch.uId);

                    if (user.Admin != vrmUserConstant.SUPER_ADMIN)
                    {

                        switch (user.Admin)
                        {
                            case vrmUserConstant.ADMIN:
                                if (!GetDepartmentalSearchRestrictions(oSearch, ref criterionList, user.Admin))
                                    return false;
                                break;
                            case vrmUserConstant.AV_SPECIALIST:
                                criterionList.Add(Expression.Eq("Type", woConstant.CATEGORY_TYPE_AV));
                                break;
                            case vrmUserConstant.CA_SPECIALIST:
                                criterionList.Add(Expression.Eq("Type", woConstant.CATEGORY_TYPE_CA));
                                break;
                            case vrmUserConstant.HK_SPECIALIST:
                                criterionList.Add(Expression.Eq("Type", woConstant.CATEGORY_TYPE_HK));
                                break;
                            // Genereal user
                            default:
                                if (!GetDepartmentalSearchRestrictions(oSearch, ref criterionList, user.Admin))
                                    return false;
                                break;
                        }
                    }
                    count = m_InvWorkOrderDAO.CountByCriteria(criterionList);

                    switch (oSearch.SortBy)
                    {
                        default:
                            m_InvWorkOrderDAO.addOrderBy(Order.Asc("startBy"));
                            break;
                    }
                }
                List<WorkOrder> Result = new List<WorkOrder>();
                Result = m_InvWorkOrderDAO.GetByCriteria(criterionList);

                //ZD 101835
                if (Result.Count == 0)
                {
                    SearchArchiveWorkorder(ref Result, criterionList);
                }

                m_InvWorkOrderDAO.sledgeHammerReset();

                if (Result.Count < 1)
                    return false;

                for (int i = 0; i < Result.Count; i++)
                {
                    int confID = Result[i].ConfID;
                    if (oSearch.instance == 0)
                        oSearch.instance = 1;


                    bool addIt = true;
                    if (oSearch.FromDate.Length > 1)
                    {
                        dtTo = dtTo.AddSeconds(59); // necsssary to guarantee condition
                        if (Result[i].CompletedBy >= dtFrom && Result[i].CompletedBy <= dtTo)
                            addIt = true;
                        else
                            addIt = false;
                    }

                    //FB 1774
                    if (Result[i].CompletedBy <= DateTime.Now && Result[i].Status == 0)
                        Result[i].Status = 1;

                    if (addIt)
                    {
                        WorkList.Add(Result[i]);
                    }
                }

                if (WorkList.Count == 0)
                    bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return bRet;
        }
        #endregion

        #region GetDepartmentalSearchRestrictions
        /// <summary>
        /// GetDepartmentalSearchRestrictions
        /// </summary>
        /// <param name="oSearch"></param>
        /// <param name="criterionList"></param>
        /// <param name="adminId"></param>
        /// <returns></returns>
        private bool GetDepartmentalSearchRestrictions(woSearch oSearch, ref List<ICriterion> criterionList, int adminId)
        {
            try
            {
                Hashtable confid = new Hashtable();
                List<WorkOrder> Result = new List<WorkOrder>();
                Result = m_InvWorkOrderDAO.GetByCriteria(criterionList);
                //ZD 101835
                if (Result.Count == 0)
                {
                    SearchArchiveWorkorder(ref Result, criterionList);
                }

                foreach (WorkOrder wk in Result)
                {
                    if (!confid.ContainsKey(wk.ConfID))
                    {
                        confid.Add(wk.ConfID, wk.ConfID);
                    }
                }
                List<int> cId = new List<int>();
                IDictionaryEnumerator iEnum = confid.GetEnumerator();
                while (iEnum.MoveNext())
                {
                    cId.Add((int)iEnum.Value);
                }
                // load dept list
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();

                List<ICriterion> deptCriterionList = new List<ICriterion>();
                deptCriterionList.Add(Expression.Eq("userId", oSearch.uId));
                deptList = m_IuserDeptDAO.GetByCriteria(deptCriterionList);

                foreach (vrmUserDepartment dept in deptList)
                {
                    deptIn.Add(dept.departmentId);
                }
                List<ICriterion> userCriterionList = new List<ICriterion>();
                userCriterionList.Add(Expression.In("departmentId", deptIn));

                deptList = m_IuserDeptDAO.GetByCriteria(userCriterionList);
                List<int> userIn = new List<int>();
                foreach (vrmUserDepartment dept in deptList)
                {
                    userIn.Add(dept.userId);
                }

                List<ICriterion> confList = new List<ICriterion>();

                if (adminId == vrmUserConstant.ADMIN)
                {
                    confList.Add(Expression.In("confid", cId));
                    confList.Add(Expression.Or(
                        Expression.Eq("owner", oSearch.uId), Expression.In("owner", userIn)));
                }
                else
                {
                    confList.Add(Expression.In("confid", cId));
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //ALLDEV-839 Start
                     if (orgInfo.AllowRequestortoEdit == 1)
                     {
                         confList.Add(Expression.Or(Expression.Eq("owner", oSearch.uId), Expression.Eq("userid", oSearch.uId)));
                     }
                     else
                         confList.Add(Expression.Eq("owner", oSearch.uId)); //ALLDEV-839 End

                }

                List<vrmConference> conferences = m_IconfDAO.GetByCriteria(confList);
                //ZD 101835
                if (conferences.Count == 0)
                {
                    vrmDataObject obj1 = new vrmDataObject(m_configPath);
                    Conference cc = new Conference(ref obj1);
                    cc.SearchArchiveConference(ref conferences, confList);
                }

                List<int> selectedConferences = new List<int>();
                foreach (vrmConference conf in conferences)
                    selectedConferences.Add(conf.confid);

                criterionList.Add(Expression.Or(Expression.Eq("AdminID", oSearch.uId), Expression.In("ConfID", selectedConferences)));

            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region SetInventoryDetails
        /// <summary>
        /// SetInventoryDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetInventoryDetails(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                InventoryCategory cat = new InventoryCategory();

                node = xd.SelectSingleNode("//Inventory/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //ZD 103364
                node = xd.SelectSingleNode("//Inventory/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//Inventory/ID");
                string ID = node.InnerXml.Trim();
                if (ID.Length < 1 || ID.Trim().ToLower() == "new")
                {
                    cat.ID = 0;
                }
                else
                {
                    cat.ID = Int32.Parse(ID);
                }
                node = xd.SelectSingleNode("//Inventory/Type");
                string Type = node.InnerXml.Trim();
                int iType = Int32.Parse(Type);

                node = xd.SelectSingleNode("//Inventory/Name");
                string Name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//Inventory/Comments");
                string Comment = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//Inventory/Admin/ID");
                string AdminID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//Inventory/Notify");
                string Notify = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//Inventory/AssignedCostCtr");
                string AssignedCostCtr = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//Inventory/AllowOverBooking");
                string AllowOverBooking = node.InnerXml.Trim();

                int adminId = Int32.Parse(AdminID);
                //
                // check admin in charge for authoriziation
                //
                if (!checkInventoryAdmin(adminId, iType))
                {
                    //FB 1881 start
                    //myVRMException e =
                    //    new myVRMException("Error user not authorized as administrator in charge. Inventory NOT saved.");
                    myvrmEx = new myVRMException(487);
                    m_log.Error("sytemException", myvrmEx);
                    throw myvrmEx;
                    //FB 1881 end
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium = Expression.Eq("Name", Name).IgnoreCase();
                criterionList.Add(criterium);
                criterium = Expression.Eq("Type", iType);
                criterionList.Add(criterium);
                criterium = Expression.Eq("deleted", 0);
                criterionList.Add(criterium);
                criterium = Expression.Eq("orgId", organizationID); //organization module
                criterionList.Add(criterium);
                if (cat.ID > 0)
                    criterionList.Add(Expression.Not(Expression.Eq("ID", cat.ID)));

                List<InventoryCategory> result = m_InvCategoryDAO.GetByCriteria(criterionList, true);

                if (result.Count > 0)
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("Error name already exists ", 415);
                    myvrmEx = new myVRMException(488);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }

                cat.Name = Name;
                cat.Comment = Comment;
                cat.AdminID = adminId;
                cat.Notify = Notify;
                cat.AssignedCostCtr = AssignedCostCtr;
                cat.AllowOverBooking = AllowOverBooking;
                cat.orgId = organizationID;

                XmlNodeList itemList;
                XmlNodeList roomList;
                // FB: 367 Check ALL rooms for departmental authorization...
                bool valid = true;
                string roomNames = string.Empty;
                string comma = string.Empty;
                XmlNodeList checkList = xd.GetElementsByTagName("level1ID");

                //FB 2181 - Start
                if (checkList.Count != 0)
                {
                    foreach (XmlNode innerNode in checkList)
                    {
                        XmlElement checkElement = (XmlElement)innerNode;
                        int locId = Int32.Parse(checkElement.InnerText);
                        if (locId == vrmConfType.Phantom && checkList.Count == 1)
                        {
                            myvrmEx = new myVRMException(433);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        else
                        { //ZD 103364 start
                            if (orgInfo.isDeptUser == 1)
                            {
                                if (!checkDepartmentalAuthorization(locId, adminId))
                                {
                                    vrmRoom Loc = m_roomDAO.GetById(locId);
                                    if (roomNames.Length > 0)
                                        comma = ", ";
                                    roomNames += comma + Loc.Name;
                                    valid = false;
                                }
                            } //ZD 103364 End
                        }
                    }
                }
                else
                {
                    myvrmEx = new myVRMException(433);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2181 - End
                if (!valid)
                {
                    //FB 1881 start
                    //myVRMException e =
                    //new myVRMException("Error user does not have departmental authorization to administrator Room(s). ( " +
                    //          roomNames + ") ");
                    myvrmEx = new myVRMException(489);
                    m_log.Error("sytemException", myvrmEx);
                    throw myvrmEx;
                    //FB 1881 end
                }

                switch (iType)
                {
                    case woConstant.CATEGORY_TYPE_AV:
                        cat.Type = woConstant.CATEGORY_TYPE_AV;

                        itemList = xd.GetElementsByTagName("Item");
                        roomList = xd.GetElementsByTagName("level1ID");

                        foreach (XmlNode innerNode in itemList)
                        {

                            XmlElement itemElement = (XmlElement)innerNode;
                            string itemID = itemElement.GetElementsByTagName("ID")[0].InnerText; ;

                            if (itemID.Length < 1 || itemID.Trim().ToLower() == "new")
                                itemID = "0";

                            int itemId = Int32.Parse(itemID);
                            AVInventoryItemList il;
                            /// <summary>
                            /// to implement cascading you must look up the original record then
                            /// clear the collection.(NOTE clear the collection do not assingn it with new)
                            /// </summary>
                            /// 
                            if (itemId > 0)
                                il = m_IAVItemDAO.GetById(itemId, true);
                            else
                                il = new AVInventoryItemList();
                            il.deleted = 0;
                            il.categoryID = cat.ID;
                            // clear collection to remove any old values
                            il.ItemCharge.Clear(); ;

                            il.ID = itemId;
                            il.name = itemElement.GetElementsByTagName("Name")[0].InnerText;
                            il.image = itemElement.GetElementsByTagName("ImageName")[0].InnerText; //Image Project

                            int imgId = 0;
                            Int32.TryParse(itemElement.GetElementsByTagName("ImageId")[0].InnerText, out imgId);

                            if (imgId <= 0)
                            {
                                myVRMException e = new myVRMException(424);
                                obj.outXml = e.FetchErrorMsg();
                                m_log.Error("sytemException", e);
                                return false;
                            }

                            il.ImageId = imgId; //Image Project
                            il.price = float.Parse(itemElement.GetElementsByTagName("Price")[0].InnerText, CultureInfo.InvariantCulture); //ZD 101714
                            il.quantity = Int32.Parse(itemElement.GetElementsByTagName("Quantity")[0].InnerText);
                            //FB 2181 - Start
                            if (il.quantity == 0)
                            {
                                myvrmEx = new myVRMException(546);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                            //FB 2181 - End
                            if (cat.ID > 0 && itemId > 0)
                            {
                                if (!CheckItemCount(itemId, il.quantity, cat.ID))
                                {
                                    //FB 2181,2068 - Start
                                    myVRMException e = new myVRMException(535);
                                    obj.outXml = e.FetchErrorMsg();
                                    m_log.Error("sytemException", e);
                                    return false;
                                    //throw new Exception("Conference are associated with inventory set in quantity greater than the given quantity");
                                    //FB 2181,2068 - End
                                }
                            }


                            il.serialNumber = itemElement.GetElementsByTagName("SerialNumber")[0].InnerText;
                            il.portable = itemElement.GetElementsByTagName("Portable")[0].InnerText;
                            il.comment = itemElement.GetElementsByTagName("Comments")[0].InnerText;
                            il.description = itemElement.GetElementsByTagName("Description")[0].InnerText;
                            il.deleted = Int32.Parse(itemElement.GetElementsByTagName("Deleted")[0].InnerText);
                            XmlNodeList Charges = itemElement.SelectNodes("Charges/Charge");

                            foreach (XmlNode chargeNode in Charges)
                            {
                                WorkItemCharge wi = new WorkItemCharge();
                                wi.ID = 0;
                                wi.ItemId = il.ID;

                                XmlElement charge = (XmlElement)chargeNode;
                                wi.deliveryCharge = float.Parse(charge.GetElementsByTagName("DeliveryCost")[0].InnerText, CultureInfo.InvariantCulture); //ZD 101714
                                wi.serviceCharge = float.Parse(charge.GetElementsByTagName("ServiceCharge")[0].InnerText, CultureInfo.InvariantCulture);
                                wi.TypeId = Int32.Parse(charge.GetElementsByTagName("DeliveryTypeID")[0].InnerText);
                                wi.IDeliveryType.id = wi.TypeId;

                                wi.Item = il;
                                il.ItemCharge.Add(wi);
                            }

                            il.ICategory = cat;
                            cat.AVItemList.Add(il);
                        }
                        SetInventoryRooms(ref cat, roomList);
                        break;
                    case woConstant.CATEGORY_TYPE_CA:
                        cat.Type = woConstant.CATEGORY_TYPE_CA;

                        itemList = xd.GetElementsByTagName("Item");
                        roomList = xd.GetElementsByTagName("level1ID");
                        foreach (XmlNode innerNode in itemList)
                        {
                            CAInventoryItemList il = new CAInventoryItemList();
                            il.deleted = 0;
                            il.categoryID = cat.ID;

                            XmlElement itemElement = (XmlElement)innerNode;

                            string itemID = itemElement.GetElementsByTagName("ID")[0].InnerText; ;
                            if (itemID.Length < 1 || itemID.Trim().ToLower() == "new")
                                itemID = "0";

                            il.ID = Int32.Parse(itemID);
                            il.name = itemElement.GetElementsByTagName("Name")[0].InnerText;
                            il.image = itemElement.GetElementsByTagName("Image")[0].InnerText;
                            il.price = float.Parse(itemElement.GetElementsByTagName("Price")[0].InnerText, CultureInfo.InvariantCulture); //ZD 101714
                            il.categoryID = cat.ID;
                            il.quantity = Int32.Parse(
                                itemElement.GetElementsByTagName("Quantity")[0].InnerText);
                            il.serialNumber = itemElement.GetElementsByTagName("SerialNumber")[0].InnerText;
                            il.portable = itemElement.GetElementsByTagName("Portable")[0].InnerText;
                            il.comment = itemElement.GetElementsByTagName("Comments")[0].InnerText;
                            il.description = itemElement.GetElementsByTagName("Description")[0].InnerText;
                            il.deleted = Int32.Parse(itemElement.GetElementsByTagName("Deleted")[0].InnerText);
                            il.ICategory = cat;
                            // cat.CAItemList.Add(il);
                        }
                        SetInventoryRooms(ref cat, roomList);
                        break;
                    case woConstant.CATEGORY_TYPE_HK:
                        cat.Type = woConstant.CATEGORY_TYPE_HK;

                        itemList = xd.GetElementsByTagName("Item");
                        roomList = xd.GetElementsByTagName("level1ID");
                        foreach (XmlNode innerNode in itemList)
                        {
                            HKInventoryItemList il = new HKInventoryItemList();
                            il.deleted = 0;
                            il.CategoryID = cat.ID;

                            XmlElement itemElement = (XmlElement)innerNode;

                            string itemID = itemElement.GetElementsByTagName("ID")[0].InnerText;
                            if (itemID.Length < 1 || itemID.Trim().ToLower() == "new")
                                itemID = "0";

                            il.ID = Int32.Parse(itemID);
                            il.name = itemElement.GetElementsByTagName("Name")[0].InnerText;
                            il.image = itemElement.GetElementsByTagName("ImageName")[0].InnerText; //Image Project

                            int imgId = 0;
                            Int32.TryParse(itemElement.GetElementsByTagName("ImageId")[0].InnerText, out imgId);

                            if (imgId <= 0)
                            {
                                myVRMException e = new myVRMException(424);
                                obj.outXml = e.FetchErrorMsg();
                                m_log.Error("sytemException", e);
                                return false;
                            }

                            il.ImageId = imgId;
                            il.comment = itemElement.GetElementsByTagName("Comments")[0].InnerText;
                            il.price = float.Parse(itemElement.GetElementsByTagName("Price")[0].InnerText, CultureInfo.InvariantCulture); //ZD 101714
                            il.description = itemElement.GetElementsByTagName("Description")[0].InnerText;
                            il.deleted = Int32.Parse(itemElement.GetElementsByTagName("Deleted")[0].InnerText);
                            il.ICategory = cat;
                            cat.HKItemList.Add(il);
                        }
                        SetInventoryRooms(ref cat, roomList);
                        break;
                    default:
                        throw new myVRMException(420);
                }

                if (cat.ID == 0)
                    m_InvCategoryDAO.Save(cat);
                else
                    m_InvCategoryDAO.SaveOrUpdate(cat);
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region SetInventoryRooms
        /// <summary>
        /// SetInventoryRooms
        /// </summary>
        /// <param name="cat"></param>
        /// <param name="roomList"></param>
        /// <returns></returns>
        private bool SetInventoryRooms(ref InventoryCategory cat, XmlNodeList roomList)
        {
            try
            {
                string checkRoom = "SELECT W.id FROM myVRM.DataLayer.WorkOrder W ";
                checkRoom += "WHERE W.CatID = " + cat.ID.ToString();
                checkRoom += " AND W.Status = 0 AND Deleted = 0 ";

                List<ICriterion> deleteRoomList = new List<ICriterion>();
                deleteRoomList.Add(Expression.Eq("categoryID", cat.ID));

                int i = 0;
                foreach (XmlNode innerNode in roomList)
                {
                    XmlElement itemElement = (XmlElement)innerNode;
                    int iLocationId = Int32.Parse(itemElement.InnerText);
                    // This is the only way I can think of to do this correctly. 
                    // delete all inv_room_D entries for this category and readd them. 
                    // this is a design issue and should be corrected. cascading is set on 
                    // and it should be used !

                    int iId;
                    iId = 0;

                    InventoryRoom rm = new InventoryRoom();
                    rm.ID = iId;
                    rm.categoryID = cat.ID;
                    rm.locationID = iLocationId;
                    rm.IRoom.roomId = rm.locationID;
                    rm.ICategory = cat;
                    cat.RoomList.Add(rm);
                    if (i > 0)
                        checkRoom += ",";
                    else
                        checkRoom += " AND W.roomid NOT IN (";
                    checkRoom += iLocationId.ToString();

                    i++;
                }

                checkRoom += ")";
                IList Result = m_InvRoomDAO.execQuery(checkRoom);
                // cant delete this room is in use.
                if (Result.Count > 0)
                {
                    //FB 1881 start
                    //myVRMException e = new myVRMException("ERROR cannot romove inventory room. Work order pending");
                    myvrmEx = new myVRMException(490);
                    throw myvrmEx;
                    //FB 1881 end

                }

                List<InventoryRoom> deleteList = new List<InventoryRoom>();
                deleteList = m_InvRoomDAO.GetByCriteria(deleteRoomList);

                foreach (InventoryRoom dRoom in deleteList)
                    m_InvRoomDAO.Delete(dRoom);

            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return true;
        }
        #endregion

        #region SetConferenceWorkOrders
        /// <summary>
        /// SetConferenceWorkOrders
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetConferenceWorkOrders(ref vrmDataObject obj)
        {
            bool isRecur = false;
            ns_SqlHelper.SqlHelper sqlCon = null;
            double totalDeliveryCost = 0, totalSerivceCost = 0; //ZD 100237
            String path = "C:\\VRMSchemas_v1.8.3\\";  //hard coded for time being Wo recurrence fix
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                List<WorkOrder> WorkOrderList = new List<WorkOrder>();

                node = xd.SelectSingleNode("//SetConferenceWorkOrders/user/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetConferenceWorkOrders/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//SetConferenceWorkOrders/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (orgInfo == null) //FB 2484
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                node = xd.SelectSingleNode("//SetConferenceWorkOrders/confInfo/ConfID");
                string confID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetConferenceWorkOrders/confInfo/IsRecur");//Code added fro WO  bug reccurence  
                if (node != null)
                {
                    if (node.InnerXml == "1")
                        isRecur = true;
                }
                //FB 2027 SetConference start
                XmlNodeList workNodes = xd.SelectNodes("//SetConferenceWorkOrders/confInfo/WorkOrderList/WorkOrder");//FB 2164
                if (workNodes != null)
                {
                    if (workNodes.Count > 0)
                    {
                        CConfID conf = new CConfID(confID);
                        int cID = conf.ID;
                        int iID = conf.instance;

                        if (iID == 0)
                            iID = 1;

                        List<ICriterion> criterionListConf = new List<ICriterion>();  //Code added fro WO  bug reccurence
                        criterionListConf.Add(Expression.Eq("confid", conf.ID));
                        if (!isRecur)
                            criterionListConf.Add(Expression.Eq("instanceid", iID));
                        //FB 2075 Starts
                        DateTime serverTime = DateTime.Now;
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);

                        criterionListConf.Add(Expression.Ge("confdate", serverTime));
                        //FB 2075 Ends
                        List<vrmConference> WOconferences = m_IconfDAO.GetByCriteria(criterionListConf);


                        //FB 2817 Starts
                        List<ICriterion> RoomcriterionList = new List<ICriterion>();

                        RoomcriterionList.Add(Expression.Eq("confid", conf.ID));

                        if (!isRecur)
                            RoomcriterionList.Add(Expression.Eq("instanceid", iID));
                        else
                            RoomcriterionList.Add(Expression.Eq("instanceid", 1));

                        RoomcriterionList.Add(Expression.Eq("Extroom", 0));
                        List<vrmConfRoom> rList1 = m_IconfRoom.GetByCriteria(RoomcriterionList);
                        //FB 2817 Starts

                        userDAO userDAO = new userDAO(m_configPath, m_log);
                        IUserDao uDAO = userDAO.GetUserDao();
                        vrmUser user = uDAO.GetByUserId(Int32.Parse(userID));

                        int tzID = user.TimeZone;
                        Int32 i = 0;
                        /*** Code added for WO bug reccurence ***/
                        Int32 j = 0;
                        DateTime dtWO = DateTime.MinValue;
                        DateTime dtConf = DateTime.MinValue;
                        TimeSpan diffResult = dtConf.Subtract(dtWO);
                        /*** Code added for WO bug reccurence ***/
                        string ID = ""; int wID = 0;
                        XmlNode WO = null;
                        foreach (vrmConference WOconference in WOconferences)
                        {

                            XmlNodeList workList = xd.GetElementsByTagName("WorkOrder");
                            foreach (XmlNode innerNode in workList)
                            {
                                WorkOrder work = new WorkOrder();
                                XmlElement innerElement = (XmlElement)innerNode;
                                WO = innerNode;
                                wID = 0; ID = "";
                                ID = WO.SelectSingleNode("ID").InnerXml.Trim(); //ZD 102831
                                //string ID = innerElement.GetElementsByTagName("ID")[0].InnerText;
                                if (ID.Length < 1 || ID.Trim().ToLower() == "new")
                                    wID = 0;
                                else
                                    wID = Int32.Parse(ID);

                                if (isRecur)
                                    wID = 0;
                                work.ID = wID;
                                if (work.ID == 0)
                                    work.isNew = true;

                                work.orgId = organizationID;
                                work.ConfID = cID;
                                work.InstanceID = iID;
                                work.deleted = 0;
                                string Type = innerElement.GetElementsByTagName("Type")[0].InnerText;
                                work.Type = Int32.Parse(Type);
                                string Name = innerElement.GetElementsByTagName("Name")[0].InnerText;
                                work.Name = Name;
                                string RoomID = innerElement.GetElementsByTagName("RoomID")[0].InnerText;
                                work.IRoom.roomId = Int32.Parse(RoomID);
                                string RoomLayout = innerElement.GetElementsByTagName("RoomLayout")[0].InnerText;
                                work.strData = RoomLayout;
                                string catID = innerElement.GetElementsByTagName("SetID")[0].InnerText;
                                work.IC.ID = Int32.Parse(catID);
                                string AdminID = innerElement.GetElementsByTagName("AdminID")[0].InnerText;
                                work.AdminID = Int32.Parse(AdminID);
                                string CompletedByDate = innerElement.GetElementsByTagName("CompletedByDate")[0].InnerText;
                                string CompletedByTime = innerElement.GetElementsByTagName("CompletedByTime")[0].InnerText;
                                DateTime dateComplete = DateTime.Parse(CompletedByDate + " " + CompletedByTime);
                                if (!timeZone.changeToGMTTime(tzID, ref dateComplete))
                                    throw new myVRMException("Invalid time format for CompletedByDate tag");
                                work.CompletedBy = dateComplete;
                                string StartByDate = innerElement.GetElementsByTagName("StartByDate")[0].InnerText;
                                string StartByTime = innerElement.GetElementsByTagName("StartByTime")[0].InnerText;
                                DateTime dateStart = DateTime.Parse(StartByDate + " " + StartByTime);
                                if (!timeZone.changeToGMTTime(tzID, ref dateStart))
                                    throw new myVRMException("Invalid time format for CompletedByDate tag");
                                work.startBy = dateStart;

                                /*** Code added for WO bug reccurence ***/
                                TimeSpan WOdur = dateComplete.Subtract(dateStart);

                                if (isRecur && j == 0)
                                {
                                    j = 1;
                                    dtConf = WOconference.SetupTime; //ZD 100433
                                    //dtConf = WOconference.confdate;
                                    dtWO = dateStart;
                                    diffResult = dtConf.Subtract(dtWO);

                                }
                                /*** Code added for WO bug reccurence ***/
                                if (isRecur && work.isNew)
                                {
                                    work.startBy = WOconference.SetupTime; //ZD 100433
                                    //work.startBy = WOconference.confdate; //FB 2075
                                    work.CompletedBy = work.startBy.Add(WOdur);
                                    work.ConfID = WOconference.confid;
                                    work.InstanceID = WOconference.instanceid;

                                }
                                string Status = innerElement.GetElementsByTagName("Status")[0].InnerText;
                                work.Status = Int32.Parse(Status);
                                string Comments = innerElement.GetElementsByTagName("Comments")[0].InnerText;
                                work.Comment = Comments;
                                string Notify = innerElement.GetElementsByTagName("Notify")[0].InnerText;
                                work.Notify = Int32.Parse(Notify);
                                string Reminder = innerElement.GetElementsByTagName("Reminder")[0].InnerText;
                                work.Reminder = Int32.Parse(Reminder);

                                string Description = innerElement.GetElementsByTagName("Description")[0].InnerText;
                                work.description = Description;
                                //string DeliveryType = innerElement.GetElementsByTagName("DeliveryType")[0].InnerText;
                                string DeliveryType = WO.SelectSingleNode("DeliveryType").InnerXml.Trim(); //ZD 102831
                                work.deliveryType = Int32.Parse(DeliveryType);
                                //string ServiceCharge = innerElement.GetElementsByTagName("ServiceCharge")[0].InnerText;
                                string ServiceCharge = WO.SelectSingleNode("ServiceCharge").InnerXml.Trim(); //ZD 102831
                                work.serviceCharge = float.Parse(ServiceCharge);
                                //string DeliveryCost = innerElement.GetElementsByTagName("DeliveryCost")[0].InnerText;
                                string DeliveryCost = WO.SelectSingleNode("DeliveryCost").InnerXml.Trim(); //ZD 102831
                                work.deliveryCost = float.Parse(DeliveryCost);
                                string woTimeZone = innerElement.GetElementsByTagName("Timezone")[0].InnerText;
                                work.woTimeZone = Int32.Parse(woTimeZone);
                                string totalCost = innerElement.GetElementsByTagName("TotalCost")[0].InnerText;
                                work.woTtlCost = float.Parse(totalCost);

                                XmlNodeList itemList = innerNode.SelectNodes("ItemList/Item");
                                totalDeliveryCost = 0; //ZD 100237
                                totalSerivceCost = 0;//ZD 100237
                                foreach (XmlNode itemNode in itemList)
                                {
                                    XmlElement itemElement = (XmlElement)itemNode;

                                    WorkItem wi = new WorkItem();

                                    wi.ID = 0;
                                    wi.deleted = 0;
                                    //Code commented fro Wo reccrence
                                    //string uID = itemElement.GetElementsByTagName("UID")[0].InnerText;
                                    //wi.ID = Int32.Parse(uID);

                                    string ItemID = itemElement.GetElementsByTagName("ID")[0].InnerText;
                                    wi.ItemID = Int32.Parse(ItemID);

                                    wi.WorkOrderID = work.ID;
                                    wi.IWork = work;

                                    string Quantity = itemElement.GetElementsByTagName("Quantity")[0].InnerText;
                                    wi.quantity = Int32.Parse(Quantity);

                                    string wiServiceCharge = itemElement.GetElementsByTagName("ServiceCharge")[0].InnerText;
                                    string wiDeliveryType = itemElement.GetElementsByTagName("DeliveryType")[0].InnerText;
                                    string wiDeliveryCost = itemElement.GetElementsByTagName("DeliveryCost")[0].InnerText;

                                    if (wiServiceCharge.Length > 0)
                                        wi.serviceCharge = float.Parse(wiServiceCharge);
                                    if (wiDeliveryType.Length > 0)
                                        wi.deliveryType = Int32.Parse(wiDeliveryType);
                                    else
                                        wi.deliveryType = work.deliveryType; // Cod eadded for WO bug

                                    if (wiDeliveryCost.Length > 0)
                                        wi.deliveryCost = float.Parse(wiDeliveryCost);

                                    if (wi.quantity > 0)
                                    {
                                        totalDeliveryCost = (wi.deliveryCost * wi.quantity) + totalDeliveryCost; //ZD 100237
                                        totalSerivceCost = (wi.serviceCharge * wi.quantity) + totalSerivceCost; //ZD 100237
                                    }
                                    work.ItemList.Add(wi);

                                }
                                work.woTtlDlvryCost = totalDeliveryCost + work.deliveryCost; //ZD 100237
                                work.woTtlSrvceCost = totalSerivceCost + work.serviceCharge; //ZD 100237
                                /* Following codelines commented for FB 1830 start */
                                //work.email = new vrmEmail();
                                //if (!GetWorkOrderEmail(ref work))
                                //{
                                //    myVRMException e = new myVRMException("Cannot create work order email");
                                //    m_log.Error("sytemException", e);
                                //    obj.outXml = myVRMException.toXml(e.Message);
                                //}
                                /* Following codelines commented for FB 1830 end */
                                WorkOrderList.Add(work);

                            }
                            checkWorkOrderList(WorkOrderList);
                            //ZD 100757 Starts
                            foreach (WorkOrder wk in WorkOrderList)
                            {
                                if (wk.ID > 0)
                                {
                                    sqlCon = new ns_SqlHelper.SqlHelper(path);

                                    sqlCon.OpenConnection();
                                    sqlCon.OpenTransaction();

                                    string qString = "Delete from Inv_WorkItem_D";//Update Inv_WorkItem_D set deleted = 1  ";
                                    if (!isRecur)
                                        qString += " WHERE WorkOrderID =" + wk.ID;
                                    else
                                        qString += " WHERE WorkOrderID in ( Select ID FROM  Inv_WorkOrder_D  WHERE type in (1,3) and ConfID = " + confID + ")";//FB 1917

                                    Int32 strExec = sqlCon.ExecuteNonQuery(qString);

                                    if (isRecur)
                                    {
                                        qString = "Delete from Inv_WorkOrder_D";//"Update Inv_WorkOrder_D set deleted = 1"
                                        qString += " WHERE type in (1,3) and ConfID = " + confID; //FB 1917
                                        strExec = sqlCon.ExecuteNonQuery(qString);

                                    }

                                    sqlCon.CommitTransaction();
                                    sqlCon.CloseConnection();
                                    sqlCon = null;
                                }
                            }
                            //ZD 100757 Ends
                        }
                        m_InvWorkOrderDAO.SaveOrUpdateList(WorkOrderList);
                        if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                        {
                            if (rList1.Count == 1) //FB 2817
                            {
                                if (orgInfo.SingleRoomConfMail == 1)
                                    m_woEmail.sendAVAndHKWOEmails(WOconferences, WorkOrderList);  //FB 1830
                            }
                            else
                                m_woEmail.sendAVAndHKWOEmails(WOconferences, WorkOrderList);  //FB 1830

                        }
                    }
                }
                //FB 2027 SetConference end

                //foreach (WorkOrder work in WorkOrderList)
                //    m_InvWorkOrderDAO.Save(work);

                /* Following codelines commented for FB 1830 start */
                //// FB 324,325,327 Work order emails got to the host if notify is on
                //// they ALWAYS go to the work order admin. 
                //foreach (WorkOrder wk in WorkOrderList)
                //{
                //    WorkOrder admin = wk;

                //    if (GetWorkOrderEmail(ref admin))
                //    {
                //        m_woDAO.SaveWorkOrderEmail(admin.email);
                //    }

                //    if (wk.Notify != 0)
                //    {
                //        List<ICriterion> criterionList = new List<ICriterion>();
                //        criterionList.Add(Expression.Eq("confid", wk.ConfID));
                //        criterionList.Add(Expression.Eq("instanceid", wk.InstanceID));

                //        List<vrmConference> conferences = m_IconfDAO.GetByCriteria(criterionList);

                //        if (conferences.Count == 1)
                //        {
                //            WorkOrder host = wk;
                //            host.toUserID = conferences[0].owner;
                //            //FB 1685
                //            if (wk.AdminID != host.toUserID)
                //            {
                //                if (GetWorkOrderEmail(ref host))
                //                {
                //                    m_woDAO.SaveWorkOrderEmail(host.email);
                //                }
                //            }
                //        }
                //    }
                //}
                /* Following codelines commented for FB 1830 end */
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("myVRMException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
            finally
            {
                if (sqlCon != null)
                {
                    sqlCon.CloseConnection();
                    sqlCon = null;
                }
            }
        }
        #endregion

        #region checkWorkOrderList
        /// <summary>
        /// checkWorkOrderList
        /// </summary>
        /// <param name="WorkOrderList"></param>
        /// <returns></returns>
        private bool checkWorkOrderList(List<WorkOrder> WorkOrderList)
        {

            foreach (WorkOrder wk in WorkOrderList)
            {
                if (!checkWOAdmin(wk.AdminID, wk.Type))
                {
                    //myVRMException e =
                    // new myVRMException("Error user not authorized as administrator in charge. Work Order ( " +
                    //     wk.Name + ") NOT saved.");
                    m_log.Error("myVRMException", myvrmEx);
                    myvrmEx = new myVRMException(491);
                    throw myvrmEx;
                    //FB 1881 end
                }
                // FB 367 check dept authorization (AB 05/08/08)
                 //ZD 103364 start
                if (orgInfo.isDeptUser == 1)
                {
                    if (!checkDepartmentalAuthorization(wk.IRoom.roomId, wk.AdminID))
                    {
                        //FB 1881 start
                        //myVRMException e =
                        //new myVRMException("Error user does not have departmental authorization to administrator Work Order. Work Order ( " +
                        //        wk.Name + ") NOT saved.");
                        m_log.Error("myVRMException", myvrmEx);
                        myvrmEx = new myVRMException(492);
                        throw myvrmEx;
                        //FB 1881 end
                    }
                } //ZD 103364  End
            }
            return true;
        }
        #endregion

        #region checkDepartmentalAuthorization
        /// <summary>
        /// checkDepartmentalAuthorization
        /// </summary>
        /// <param name="roomid"></param>
        /// <param name="AdminID"></param>
        /// <returns></returns>
        private bool checkDepartmentalAuthorization(int roomid, int AdminID)
        {
            if (organizationID < 11)    //Organization Module
                organizationID = defaultOrgId;

            if (orgInfo == null)
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

            if (orgInfo != null)
                multiDepts = orgInfo.MultipleDepartments;

            if (multiDepts == 1)    //Organization Module
            {
                vrmUser user = m_vrmUserDAO.GetByUserId(AdminID);

                if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                {
                    return true;
                }
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("roomId", roomid));

                ILocDeptDAO m_IlocDeptDAO;
                LocationDAO m_locDAO = new LocationDAO(m_configPath, m_log);

                m_IlocDeptDAO = m_locDAO.GetLocDeptDAO(); ;

                deptDAO m_deptDAO = new deptDAO(m_configPath, m_log);
                IUserDeptDao m_IuserDeptDAO = m_deptDAO.GetUserDeptDao();

                List<vrmLocDepartment> locDeptList = m_IlocDeptDAO.GetByCriteria(criterionList);

                List<int> deptList = new List<int>();
                foreach (vrmLocDepartment locDept in locDeptList)
                    deptList.Add(locDept.departmentId);

                criterionList.Clear();
                criterionList.Add(Expression.Eq("userId", AdminID));
                if(deptList != null && deptList.Count > 0) //ZD 103364
                   criterionList.Add(Expression.In("departmentId", deptList));

                List<vrmUserDepartment> usrDept = m_IuserDeptDAO.GetByCriteria(criterionList);

                if (usrDept.Count == 0)
                    return false;
                else
                    return true;
            }
            return true;
        }
        #endregion

        #region DeleteInventoryItem
        /// <summary>
        /// DeleteInventoryItem
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteInventoryItem(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/ID");
                string InventoryID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/Type");
                string Type = node.InnerXml.Trim();

                // FB 287 close session (pass true) before update (AG 04/20/08)
                InventoryCategory cat = m_InvCategoryDAO.GetById(Int32.Parse(InventoryID), true);
                // FB 977/891
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CatID", cat.ID));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Ge("CompletedBy", DateTime.UtcNow)); //ZD 104401
                List<WorkOrder> wkList = m_InvWorkOrderDAO.GetByCriteria(criterionList, true);
                if (wkList.Count > 0)
                {
                    //FB 1881 start
                    //myVRMException e = new myVRMException("Error Cannot delete Inventory in use. ");
                    //obj.outXml = myVRMException.toXml("Error Cannot delete Inventory in use. ");
                    myvrmEx = new myVRMException(493);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    m_log.Error("sytemException", myvrmEx);
                    //FB 1881 end
                    return true;
                }
                else
                {
                    cat.deleted = 1;
                    m_InvCategoryDAO.SaveOrUpdate(cat);
                }
                cat.deleted = 1;
                m_InvCategoryDAO.SaveOrUpdate(cat);

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetRoomSets
        /// <summary>
        /// GetRoomSets
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomSets(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                string roomID = node.InnerXml.Trim();
                int iRoom = Int32.Parse(roomID);

                node = xd.SelectSingleNode("//login/Type");
                string Type = node.InnerXml.Trim();
                int iType = Int32.Parse(Type);
                obj.outXml = "<SetList>";

                string qString;
                qString = "from myVRM.DataLayer.InventoryCategory  Cat, ";
                qString += "myVRM.DataLayer.InventoryRoom Rm ";
                qString += "where Rm.ICategory.ID = Cat.ID and Rm.IRoom.roomId = {0} and Cat.deleted = 0 and Cat.orgId='" + organizationID + "'"; //organization module
                qString += " and Cat.Type = {1}";
                string queryString = string.Format(qString, roomID, iType);

                IList CategoryList = m_InvCategoryDAO.execQuery(queryString);
                if (CategoryList.Count > 0)
                {
                    foreach (object[] roomSets in CategoryList)
                    {
                        InventoryCategory Cat = (InventoryCategory)roomSets[0];
                        obj.outXml += "<Set>";
                        obj.outXml += "<ID>" + Cat.ID.ToString() + "</ID>";
                        obj.outXml += "<Type>" + Cat.Type.ToString() + "</Type>";
                        obj.outXml += "<Name>" + Cat.Name + "</Name>";
                        obj.outXml += "</Set>";
                    }
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            obj.outXml += "</SetList>";

            return bRet;
        }
        #endregion

        #region DeleteWorkOrder
        /// <summary>
        /// DeleteWorkOrder
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteWorkOrder(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);



                XmlNode node;

                //FB 2484 Starts
                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;

                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;

                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                //FB 2484 Ends
                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/WorkorderID");
                int ID = Int32.Parse(node.InnerXml.Trim());

                List<WorkOrder> wkList = new List<WorkOrder>();
                List<WorkOrder> wkEmailList = new List<WorkOrder>();
                if (ID == 0)
                {
                    node = xd.SelectSingleNode("//login/ConferenceID");
                    string conferenceID = node.InnerXml.Trim();

                    CConfID ConfMode = new CConfID(conferenceID);

                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("ConfID", ConfMode.ID));
                    if (ConfMode.instance > 0)
                        criterionList.Add(Expression.Eq("InstanceID", ConfMode.instance));
                    //FB 2075 Starts
                    DateTime serverTime = DateTime.Now;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);

                    criterionList.Add(Expression.Ge("startBy", serverTime));
                    //FB 2075 Ends
                    wkList = m_InvWorkOrderDAO.GetByCriteria(criterionList, true);
                    foreach (WorkOrder w in wkList)
                    {
                        if (w.deleted == 0)
                        {
                            w.deleted = 1;
                            foreach (WorkItem wi in w.ItemList)
                                wi.deleted = 1;

                            wkEmailList.Add(w);
                        }
                    }
                }
                else
                {
                    WorkOrder wk = m_InvWorkOrderDAO.GetById(ID, true);
                    wk.deleted = 1;
                    foreach (WorkItem wi in wk.ItemList)
                        wi.deleted = 1;
                    wkList.Add(wk);
                    wkEmailList.Add(wk);
                }
                if (wkList.Count > 0)
                {
                    m_InvWorkOrderDAO.SaveOrUpdateList(wkList);
                    if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                    {
                        if (!m_woEmail.sendWorkOrderDeleteEmail(wkEmailList)) //FB 1830 - Email
                        {
                            //FB 1881 start
                            //obj.outXml = myVRMException.toXml("Error in sending work order delete emails"); 
                            m_log.Error("Error in sending work order delete emails");
                            obj.outXml = "";
                            //FB 1881 end
                            return false;
                        }
                    }
                    return true;
                }
                // FB: 361 if there are no work 0rders this is NOT n error
                else
                    return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region SendWorkOrderReminder
        /// <summary>
        /// SendWorkOrderReminder
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SendWorkOrderReminder(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                myVRMException myVRMEx = null; //FB 2489
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/WorkOrderID");
                int ID = Int32.Parse(node.InnerXml.Trim());

                //FB 2489 - START
                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (orgInfo == null) //FB 2484
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                //FB 2489 - END

                WorkOrder work = m_InvWorkOrderDAO.GetById(ID);
                work.email = new vrmEmail();
                work.sendReminder = true;

                List<WorkOrder> woList = new List<WorkOrder>(); //FB 1830
                woList.Add(work); //FB 1830

                List<ICriterion> criterionListConf = new List<ICriterion>(); //FB 1830
                criterionListConf.Add(Expression.Eq("confid", work.ConfID));
                criterionListConf.Add(Expression.Eq("instanceid", work.InstanceID));
                //FB 2075 Starts
                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);

                criterionListConf.Add(Expression.Ge("confdate", serverTime));
                //FB 2075 Ends
                List<vrmConference> WOconferences = m_IconfDAO.GetByCriteria(criterionListConf);
                //FB 2817 Starts
                List<ICriterion> RoomcriterionList = new List<ICriterion>();

                RoomcriterionList.Add(Expression.Eq("confid", work.ConfID));

                if (work.InstanceID > 0)
                    RoomcriterionList.Add(Expression.Eq("instanceid", work.InstanceID));
                else
                    RoomcriterionList.Add(Expression.Eq("instanceid", 1));

                RoomcriterionList.Add(Expression.Eq("Extroom", 0));
                List<vrmConfRoom> rList1 = m_IconfRoom.GetByCriteria(RoomcriterionList);
                //FB 2817 Starts

                if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                {
                    //FB 1685
                    if (work.Type == 2)
                    {
                        if (rList1.Count == 1) //FB 2817
                        {
                            if (orgInfo.SingleRoomConfMail == 1)
                            {
                                if (!m_woEmail.sendCateringWOEmails(WOconferences, woList))
                                {
                                    myVRMEx = new myVRMException(547);//FB 2489
                                    obj.outXml = myVRMEx.FetchErrorMsg();
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            if (!m_woEmail.sendCateringWOEmails(WOconferences, woList))
                            {
                                myVRMEx = new myVRMException(547);//FB 2489
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }


                    }
                    else
                    {
                        if (rList1.Count == 1) //FB 2817
                        {
                            if (orgInfo.SingleRoomConfMail == 1)
                            {
                                if (!m_woEmail.sendAVAndHKWOEmails(WOconferences, woList))
                                {
                                    myVRMEx = new myVRMException(547);//FB 2489
                                    obj.outXml = myVRMEx.FetchErrorMsg();
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            if (!m_woEmail.sendAVAndHKWOEmails(WOconferences, woList))
                            {
                                myVRMEx = new myVRMException(547);//FB 2489
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion

        #region GetDeliveryTypes
        /// <summary>
        /// GetDeliveryTypes
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetDeliveryTypes(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetDeliveryTypes/UserID");
                string userID = node.InnerXml.Trim();

                InvDeliveryTypeDAO deliveryTypeDAO = m_woDAO.GetDeliveryTypeDAO();
                List<DeliveryType> DeliveryList = deliveryTypeDAO.GetAll();

                obj.outXml += "<DeliveryTypes>";
                foreach (DeliveryType dt in DeliveryList)
                {
                    obj.outXml += "<Type>";
                    obj.outXml += "<ID>" + dt.id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + dt.deliveryType + "</Name>";
                    obj.outXml += "</Type>";
                }
                obj.outXml += "</DeliveryTypes>";

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion

        #region SetCategoryItem
        /// <summary>
        /// SetCategoryItem
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetCategoryItem(ref vrmDataObject obj)
        {
            myVRMException myVRMEx = null;
            vrmFact = new vrmFactory(ref obj);
            vrmImg = new imageFactory(ref obj); //FB 2136
            //ZD 100263
            string filextn = "";
            string[] roomNameList = null;
            List<string> strExtension = null;
            bool filecontains = false;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//SetCategoryItem/ID");
                string ID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetCategoryItem/Type");
                int type = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//SetCategoryItem/Name");
                string name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetCategoryItem/ImageName"); //Image Project
                string imagename = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetCategoryItem/Image"); //Image Project
                string image = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetCategoryItem/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                //ZD 100263
                if (imagename != "")
                {
                    filextn = imagename.Split('.')[imagename.Split('.').Length - 1];
                    strExtension = new List<string> { "jpg", "jpeg", "png", "bmp", "gif" };
                    filecontains = strExtension.Contains(filextn);
                    if (!filecontains)
                    {
                        myvrmEx = new myVRMException(707);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                }

                byte[] imageData = null; //Image Project
                if (image.Trim() != "")
                    imageData = vrmImg.ConvertBase64ToByteArray(image);  //FB 2136

                if (imageData == null)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (imageData.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                int attrType = 0;

                if (type == 1)
                    attrType = 8; //av
                else if (type == 2)
                    attrType = 9; //cat
                else
                    attrType = 10; //hk

                vrmImage imgObj = new vrmImage();
                imgObj.OrgId = organizationID;
                //imgObj.ImageAssignedTo = 0; //Un-assigned
                imgObj.AttributeType = attrType;
                imgObj.AttributeImage = imageData;

                int imageid = 0;
                bool insSt = vrmFact.SetImage(imgObj, ref imageid);
                if (!insSt)
                {
                    myVRMEx = new myVRMException(401);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                InventoryList inv = new InventoryList();
                if (ID.ToLower() == "new")
                    inv.ID = 0;
                else
                    inv = m_InvListDAO.GetById(Int32.Parse(ID));

                inv.Type = type;
                inv.Name = name;
                inv.Image = imagename; //Image Project
                inv.ImageId = imageid; //Image Project
                inv.orgId = organizationID; //organization module
                inv.ImgResized = 1; //ZD 100175
                m_InvListDAO.SaveOrUpdate(inv);
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region DeleteCategoryItem
        /// <summary>
        /// DeleteCategoryItem
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteCategoryItem(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                //FB 2626 - Start
                List<ICriterion> criterionlist = new List<ICriterion>();
                List<ICriterion> criterionlist1 = new List<ICriterion>();
                myVRMException myVRMEx = null;
                long listCount = 0;
                vrmImage imObj = null;
                InventoryList invList = null;
                bool ItemList = false;

                node = xd.SelectSingleNode("//DeleteCategoryItem/UserID");
                string userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//DeleteCategoryItem/CategoryItemID");
                int catId = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//DeleteCategoryItem/ImageId");
                int imgID = Int32.Parse(node.InnerXml.Trim());

                invList = m_InvListDAO.GetById(catId);

                criterionlist.Add(Restrictions.Eq("itemId", catId));
                criterionlist.Add(Restrictions.Eq("deleted", 0));

                listCount = m_ICAItemDAO.CountByCriteria(criterionlist);
                if (listCount > 0)
                {
                    ItemList = true;
                }
                else
                {
                    criterionlist1.Add(Restrictions.Eq("ImageId", imgID));
                    criterionlist1.Add(Restrictions.Eq("deleted", 0));
                    listCount = m_IAVItemDAO.CountByCriteria(criterionlist1);
                    if (listCount > 0)
                    {
                        ItemList = true;
                    }
                    else
                    {
                        listCount = m_IHKItemDAO.CountByCriteria(criterionlist1);
                        if (listCount > 0)
                            ItemList = true;
                    }
                }
                if (!ItemList && invList != null)
                {
                    imObj = m_IImageDAO.GetById(invList.ImageId);
                    if (imObj != null)
                    {
                        m_IImageDAO.Delete(imObj);
                    }
                    m_InvListDAO.Delete(invList);
                }
                else
                {
                    myVRMEx = new myVRMException(635);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                obj.outXml = "<Success>1</Success>";
                //FB 2626 - End
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region DeleteProviderMenuItem
        /// <summary>
        /// DeleteProviderMenuItem
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteProviderMenuItem(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//DeleteCategoryItem/UserID");
                string userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//DeleteCategoryItem/CategoryItemID");
                int catId = Int32.Parse(node.InnerXml.Trim());

                InventoryList inv = m_InvListDAO.GetById(catId);
                m_InvListDAO.Delete(inv);
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region DeleteProviderMenu
        /// <summary>
        /// DeleteProviderMenu
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteProviderMenu(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                //FB 2626
                List<ICriterion> criterionList2 = new List<ICriterion>();
                myVRMException myVRMEx = null;
                long d_menuList = 0;

                // parsing the inxml
                string userId = xd.SelectSingleNode("//DeleteProviderMenu/UserID").InnerXml.Trim();
                int catID = Int32.Parse(xd.SelectSingleNode("//DeleteProviderMenu/ProviderID").InnerXml.Trim());
                int menuID = Int32.Parse(xd.SelectSingleNode("//DeleteProviderMenu/MenuID").InnerXml.Trim());



                // menu                 
                InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                Menu menu = new Menu();
                menu.id = menuID;
                menu.categoryID = catID;
                //FB 2626 - Start
                criterionList2.Add(Expression.Eq("categoryID", catID));
                d_menuList = menuDAO.CountByCriteria(criterionList2);

                if (d_menuList <= 1)
                {
                    myVRMEx = new myVRMException(636);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                else
                {
                    //FB 1912 start
                    InventoryCategory cat = new InventoryCategory();
                    cat.Type = woConstant.CATEGORY_TYPE_CA;
                    cat.ID = catID;
                    menu.ICategory = cat;
                    //FB 1912 end
                    // check if menu can be deleted
                    string workList = string.Empty;
                    if (canDeleteMenu(menu.id, ref workList))
                    {
                        // Deleting menu service types              
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("menuId", menu.id));

                        InvMenuServiceDAO menuServiceDAO = m_woDAO.GetMenuServiceDAO();
                        List<MenuService> services = menuServiceDAO.GetByCriteria(criterionList);
                        foreach (MenuService ms in services)
                            menuServiceDAO.Delete(ms);

                        // Deleting menu items
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        criterionList1.Add(Expression.Eq("categoryID", menu.categoryID));//Webservice Fix

                        ICAItemDAO caItemsDAO = m_woDAO.GetCAItemDAO();
                        List<CAInventoryItemList> items = m_ICAItemDAO.GetByCriteria(criterionList1);
                        foreach (CAInventoryItemList cas in items)
                            caItemsDAO.Delete(cas);


                        // Deleting menus
                        menuDAO.Delete(menu);

                        return true;
                    }
                    else
                    {
                        //FB 1881 start
                        //obj.outXml = myVRMException.toXml("Menu in use by the following work orders: " + workList + "  Cannot delete.");
                        myvrmEx = new myVRMException(481);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        //FB 1881 end
                        return false;
                    }
                }
                //FB 2626 - End

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetInventoryQty
        /// <summary>
        /// GetInventoryQty
        /// </summary>
        /// <param name="id"></param>
        /// <param name="woID"></param>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        private bool GetInventoryQty(int id, int woID, int confid, int instanceid, ref int quantity)
        {

            try
            {
                IConferenceDAO m_vrmConfDAO = new conferenceDAO(m_configPath, m_log).GetConferenceDao();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", confid));
                criterionList.Add(Expression.Eq("instanceid", instanceid));
                List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList);

                if (confList.Count < 1)
                    return false;

                vrmConference conf = confList[0];

                DateTime confFrom = conf.confdate;
                DateTime confTo = confFrom.AddMinutes(conf.duration);

                string qString = "SELECT c.confid,c.instanceid, c.confdate, it.quantity, wo.ID, c.duration ";

                qString += " FROM myVRM.DataLayer.WorkItem it, ";
                qString += " myVRM.DataLayer.WorkOrder wo, ";
                qString += " myVRM.DataLayer.vrmConference c ";
                qString += " WHERE it.ItemID = " + id.ToString();
                qString += " AND it.IWork.ID = wo.ID";
                qString += " AND wo.Status = 0";
                qString += " AND wo.ConfID = c.confid AND wo.InstanceID = c.instanceid ";
                qString += " AND c.deleted = 0 And it.deleted = 0";//FB 2011

                string test = confFrom.ToString("g");

                IList results = m_InvWorkOrderDAO.execQuery(qString);

                // FB 322 commented out (????)
                // always return  false even if this is the same person (assign a new number ALWAYS)
                //if (results.Count > 0)
                //{
                //    return false;
                //}
                foreach (object[] item in results)
                {
                    if ((int)item[0] != vrmConfType.Phantom)
                    {
                        if (Int32.Parse(item[4].ToString()) != woID)
                        {

                            DateTime fromDate = DateTime.Parse(item[2].ToString());
                            DateTime toDate = fromDate.AddMinutes(Int32.Parse(item[5].ToString()));


                            if ((DateTime.Compare(confFrom, fromDate) >= 0 &&
                                  DateTime.Compare(confFrom, toDate) <= 0) ||
                                  (DateTime.Compare(confTo, toDate) <= 0 &&
                                  DateTime.Compare(confTo, fromDate) >= 0))
                            {

                                quantity += Int32.Parse(item[3].ToString());

                            }
                        }
                    }
                }
                quantity += CheckPhantomInventory(confFrom, confTo, woID, id);

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;

        }
        #endregion

        #region GetInventoryQty
        /// <summary>
        /// GetInventoryQty
        /// </summary>
        /// <param name="id"></param>
        /// <param name="woID"></param>
        /// <param name="confFrom"></param>
        /// <param name="confTo"></param>
        /// <param name="quantity"></param>
        /// <param name="WorkOrderID"></param>
        /// <returns></returns>
        public bool GetInventoryQty(int id, int woID, DateTime confFrom, DateTime confTo, ref int quantity, string WorkOrderID)
        {

            DateTime fromDate;
            DateTime toDate;

            try
            {
                string qString = "SELECT c.confid,c.instanceid, c.confdate, it.quantity, wo.ID, c.duration ";
                qString += " FROM myVRM.DataLayer.WorkItem it, ";
                qString += " myVRM.DataLayer.WorkOrder wo, ";
                qString += " myVRM.DataLayer.vrmConference c ";
                qString += " WHERE it.ItemID = " + id.ToString();
                qString += " AND it.IWork.ID = wo.ID";
                qString += " AND wo.Status = 0";
                qString += " AND wo.ConfID = c.confid AND wo.InstanceID = c.instanceid ";
                if (WorkOrderID.Length > 0)
                    qString += " AND wo.ID != " + WorkOrderID;
                qString += " AND c.deleted = 0 And it.deleted = 0";//FB 2011
                string test = confFrom.ToString("g");

                IList itemList = m_InvWorkOrderDAO.execQuery(qString);

                foreach (object[] item in itemList)
                {
                    if ((int)item[0] != vrmConfType.Phantom)
                    {
                        if (Int32.Parse(item[4].ToString()) != woID)
                        {

                            fromDate = DateTime.Parse(item[2].ToString());
                            toDate = fromDate.AddMinutes(Int32.Parse(item[5].ToString()));

                            if ((DateTime.Compare(confFrom, fromDate) > 0 &&
                                  DateTime.Compare(confFrom, toDate) <= 0) ||
                                  (DateTime.Compare(confTo, toDate) <= 0 &&
                                  DateTime.Compare(confTo, fromDate) > 0))
                            {
                                quantity += Int32.Parse(item[3].ToString());
                            }
                        }
                    }
                }
                quantity += CheckPhantomInventory(confFrom, confTo, woID, id);
            }
            catch (Exception e)
            {
                m_log.Error("Cannot get inventory quantitiy", e);
                return false;
            }
            return true;
        }
        #endregion

        #region CheckPhantomInventory
        /// <summary>
        /// CheckPhantomInventory
        /// </summary>
        /// <param name="confFrom"></param>
        /// <param name="confTo"></param>
        /// <param name="woID"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private int CheckPhantomInventory(DateTime confFrom, DateTime confTo, int woID, int id)
        {
            try
            {
                // get all open av work ordres that are phantoms
                int quantity = 0;
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("ConfID", vrmConfType.Phantom));
                criterionList.Add(Expression.Eq("Status", vrmWorkOrderStatus.Pending));
                criterionList.Add(Expression.Eq("deleted", vrmWorkOrderStatus.Active));
                criterionList.Add(Expression.Eq("Type", woConstant.CATEGORY_TYPE_AV));

                List<WorkOrder> woList = m_InvWorkOrderDAO.GetByCriteria(criterionList);

                // look throuhg list
                foreach (WorkOrder work in woList)
                {
                    // find the item
                    if (woID != work.ID)
                    {
                        foreach (WorkItem item in work.ItemList)
                        {
                            if (item.ItemID == id)
                            {
                                // found an item  now check the dates

                                if ((DateTime.Compare(confFrom, work.startBy) > 0 &&
                                     DateTime.Compare(confFrom, work.CompletedBy) <= 0) ||
                                      (DateTime.Compare(confTo, work.CompletedBy) <= 0 &&
                                      DateTime.Compare(confTo, work.startBy) > 0))
                                {
                                    quantity += item.quantity;
                                }
                            }
                        }
                    }
                }
                return quantity;
            }
            catch (Exception e)
            {
                m_log.Error("Cannot get inventory quantitiy", e);
                throw e;
            }
        }
        #endregion

        #region checkWOAdmin
        /// <summary>
        /// checkWOAdmin
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="iType"></param>
        /// <returns></returns>
        private bool checkWOAdmin(int adminId, int iType)
        {
            // FB 253; If checks fail check role mask for custom role. This is a bitwise check of the last 3 bits. 
            bool bValidAdmin = false;

            try
            {
                vrmUser user = m_vrmUserDAO.GetByUserId(adminId);
                checkAthority hasAthority = new checkAthority(user.MenuMask);

                switch (iType)
                {
                    case woConstant.CATEGORY_TYPE_AV:
                        if (hasAthority.hasAVWO)
                            bValidAdmin = true;
                        break;
                    case woConstant.CATEGORY_TYPE_HK:
                        if (hasAthority.hasHKWO)
                            bValidAdmin = true;
                        break;
                    case woConstant.CATEGORY_TYPE_CA:
                        if (hasAthority.hasCATWO)
                            bValidAdmin = true;
                        break;

                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bValidAdmin;
        }
        #endregion

        #region checkInventoryAdmin
        /// <summary>
        /// checkInventoryAdmin
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="iType"></param>
        /// <returns></returns>
        private bool checkInventoryAdmin(int adminId, int iType)
        {
            // FB 253; If checks fail check role mask for custom role. This is a bitwise check of the last 3 bits. 
            bool bValidAdmin = false;

            try
            {
                vrmUser user = m_vrmUserDAO.GetByUserId(adminId);
                checkAthority hasAthority = new checkAthority(user.MenuMask);

                switch (iType)
                {
                    case woConstant.CATEGORY_TYPE_AV:
                        if (hasAthority.hasAVManagement)
                            bValidAdmin = true;
                        break;
                    case woConstant.CATEGORY_TYPE_HK:
                        if (hasAthority.hasHKManagement)
                            bValidAdmin = true;
                        break;
                    case woConstant.CATEGORY_TYPE_CA:
                        if (hasAthority.hasCATManagement)
                            bValidAdmin = true;
                        break;

                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bValidAdmin;
        }
        #endregion

        #region GetWorkOrderEmail
        /// <summary>
        /// GetWorkOrderEmail
        /// </summary>
        /// <param name="work"></param>
        /// <returns></returns>
        private bool GetWorkOrderEmail(ref WorkOrder work)
        {
            string emailMessage;

            try
            {
                userDAO userDAO = new userDAO(m_configPath, m_log);
                IUserDao uDAO = userDAO.GetUserDao();

                vrmUser user = new vrmUser();

                if (work.toUserID > 0)
                    user = uDAO.GetByUserId(work.toUserID);
                else
                    user = uDAO.GetByUserId(work.AdminID);
                vrmUser sysAdmin = uDAO.GetByUserId(vrmUserConstant.vrmAdminId);
                // right now the confernece is updated in another transaction. Later
                // the conf info will have to come from another oject as it will be 
                // transient at this point...
                conferenceDAO confDAO = new conferenceDAO(m_configPath, m_log);
                IConferenceDAO cDAO = confDAO.GetConferenceDao();

                vrmConference conf = cDAO.GetByConfId(work.ConfID, work.InstanceID);

                work.email = new vrmEmail();
                if (work.isNew)
                    work.email.Subject = "Conference work order setup.";
                else
                    if (work.Status == vrmWorkOrderStatus.Completed)
                        work.email.Subject = "Conference work order completed.";
                    else
                        work.email.Subject = "Conference work order modification.";


                work.email.emailFrom = sysAdmin.Email;
                work.email.emailTo = user.Email;

                DateTime dt = work.CompletedBy;
                if (!timeZone.userPreferedTime(conf.timezone, ref dt))
                    return false;
                emailMessage = "This is a notification for a pending work order assigned to you.<br>";
                emailMessage = "<br>Conference Details:<br><br>";

                string emailBody = string.Empty;
                string dummy = string.Empty;
                //FB 1830
                //m_woEmail.getConferenceDetailsForEmail(user, work.ConfID, work.InstanceID, "", ref emailBody, ref dummy);
                emailMessage += emailBody;

                emailMessage += "<br>Work Order Details:<br><br>";

                emailMessage += "<table border='1' cellpadding='5' cellspacing='0' style='width:628px;background-color:silver;font-size:10pt; font-family: Verdana;'>";
                emailMessage += "<tr><td align='right'><b>Work Order</b></td>";
                emailMessage += "<td align='left'>" + work.Name + "</td></tr>";

                vrmRoom Loc = m_roomDAO.GetById(work.IRoom.roomId);

                emailMessage += "<tr><td align='right'><b>Room Assigned To</b></td>";
                emailMessage += "<td align='left'>" + Loc.tier2.TopTierName + "&gt;"; //ZD 102481
                emailMessage += Loc.tier2.Name + "&gt;" + Loc.Name + "</td></tr>";

                // in cases of new work orders we do not have inventoy set name 
                string inventorySet = string.Empty;

                if (work.IC.Name == null)
                {
                    if (work.ItemList.Count > 0)
                    {
                        WorkItem a_wi = (WorkItem)work.ItemList[0];
                        switch (work.Type)
                        {
                            case woConstant.CATEGORY_TYPE_AV:  // Audio visual 
                                AVInventoryItemList AV = m_IAVItemDAO.GetById(a_wi.ItemID);
                                inventorySet = AV.ICategory.Name;
                                break;
                            case woConstant.CATEGORY_TYPE_CA:  // Audio visual 
                                CAInventoryItemList CA = m_ICAItemDAO.GetById(a_wi.ItemID);
                                inventorySet = CA.ICategory.Name;
                                break;
                            case woConstant.CATEGORY_TYPE_HK:  // Audio visual 
                                HKInventoryItemList HK = m_IHKItemDAO.GetById(a_wi.ItemID);
                                inventorySet = HK.ICategory.Name;
                                break;
                        }
                    }
                }
                else
                {
                    inventorySet = work.IC.Name;
                }
                emailMessage += "<tr><td align='right'><b>Inventory Set Used</b></td>";
                emailMessage += "<td align='left'>" + inventorySet + "</td></tr>";

                if (work.toUserID > 0)
                    user = uDAO.GetByUserId(work.AdminID);

                emailMessage += "<tr><td align='right'><b> Person-in-charge</b></td>";
                emailMessage += "<td align='left'>" + user.FirstName + " " + user.LastName;
                emailMessage += "(<a href='mailto:" +
                    user.Email + "' target='_blank'>" + user.Email + "</a>)</td></tr>"; //FB 1948

                timeZoneData aTz = new timeZoneData();
                timeZone.GetTimeZone(user.TimeZone, ref aTz);

                DateTime woDate = work.startBy;
                timeZone.userPreferedTime(user.TimeZone, ref woDate);

                emailMessage += "<tr><td align='right'><b>Start-by Date/Time</b></td>";

                //WO Bug Fix
                emailMessage += "<td align='left'>" + woDate.ToString(user.DateFormat) + " " + woDate.ToString((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt") + "(" + aTz.TimeZone + ")</td></tr>";

                ////Code changed  by offshore for FB Issue 1073 -- start
                //if (user.DateFormat.Trim() == "dd/MM/yyyy")
                //    emailMessage += "<td align='left'>" + woDate.ToString(user.DateFormat.Trim() + user.TimeFormat) + "(" + aTz.TimeZone + ")</td></tr>";
                //    //emailMessage += "<td align='left'>" + woDate.ToString(user.DateFormat.Trim() + " hh:mm tt") + "(" + aTz.TimeZone + ")</td></tr>";
                //else                
                //emailMessage += "<td align='left'>" + woDate.ToString("g") + "(" + aTz.TimeZone + ")</td></tr>";
                //Code changed  by offshore for FB Issue 1073 -- end
                woDate = work.CompletedBy;
                timeZone.userPreferedTime(user.TimeZone, ref woDate);

                emailMessage += "<tr><td align='right'><b>End-by Date/Time</b></td>";
                //Code changed  by offshore for FB Issue 1073 -- start

                //WO Bug Fix
                emailMessage += "<td align='left'>" + woDate.ToString(user.DateFormat) + " " + woDate.ToString((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt") + "(" + aTz.TimeZone + ")</td></tr>";

                //if (user.DateFormat.Trim() == "dd/MM/yyyy")
                //    emailMessage += "<td align='left'>" + woDate.ToString(user.DateFormat.Trim() + user.TimeFormat) + "(" + aTz.TimeZone + ")</td></tr>";
                //    //emailMessage += "<td align='left'>" + woDate.ToString(user.DateFormat.Trim() + " hh:mm tt") + "(" + aTz.TimeZone + ")</td></tr>";
                //else
                //    emailMessage += "<td align='left'>" + woDate.ToString("g") + "(" + aTz.TimeZone + ")</td></tr>";
                //emailMessage += "<td align='left'>" + woDate.ToString("g") + "(" + aTz.TimeZone + ")</td></tr>";
                //Code changed  by offshore for FB Issue 1073 -- end
                InvDeliveryTypeDAO deliveryTypeDAO = m_woDAO.GetDeliveryTypeDAO();
                DeliveryType delType = deliveryTypeDAO.GetById(work.deliveryType);

                emailMessage += "<tr><td align='right'><b>Delivery Type</b></td>";
                emailMessage += "<td align='left'>" + delType.deliveryType + "</td></tr>";

                emailMessage += "<tr><td align='right'><b>Delivery Cost($)</b></td>";
                emailMessage += "<td align='left'>" + work.deliveryCost.ToString("###0.00") + "</td></tr>";

                emailMessage += "<tr><td align='right'><b>Service Charge($)</b></td>";
                emailMessage += "<td align='left'>" + work.serviceCharge.ToString("###0.00") + "</td></tr>";

                string status = "Pending";
                if (work.Status != 0)
                    status = "Completed";

                emailMessage += "<tr><td align='right'><b>Current Status</b></td>";
                emailMessage += "<td align='left'>" + status + "</td></tr></table><br>";

                emailMessage += "<table border='1' cellpadding='5' cellspacing='0' style='background-color:silver;font-size:10pt; font-family: Verdana;'>";
                emailMessage += "<tr><td align='center'><b>Requested Item</b></td>";
                emailMessage += "<td align='center'><b>Item Serial Number</b></td>";
                emailMessage += "<td align='center'><b>Requested Quantity</b></td>";
                emailMessage += "<td align='center'><b>Service Charge($)</b></td>";
                emailMessage += "<td align='center'><b>Delivery Cost ($)</b></td>";
                emailMessage += "<td align='center'><b>Price ($)</b></td></tr>";

                string Name = string.Empty;
                double cost = 0.0;
                string serialNumber = string.Empty;
                int qty = 0; ;
                double serviceCharge = 0, deliveryCost = 0;

                foreach (WorkItem wi in work.ItemList)
                {
                    qty = wi.quantity;
                    switch (work.Type)
                    {
                        case woConstant.CATEGORY_TYPE_AV:  // Audio visual 
                            AVInventoryItemList AV = m_IAVItemDAO.GetById(wi.ItemID);
                            Name = AV.name;
                            cost = AV.price;
                            serialNumber = AV.serialNumber;
                            serviceCharge = wi.serviceCharge; //ZD 100237
                            deliveryCost = wi.deliveryCost; //ZD 100237
                            break;
                        case woConstant.CATEGORY_TYPE_CA:  // Catering 
                            // WO Bug Fix - Start
                            List<ICriterion> criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("orgId", organizationID)); //organization module
                            criterionList.Add(Expression.Eq("ID", wi.WorkOrderID));
                            criterionList.Add(Expression.Eq("deleted", 0));

                            List<WorkOrder> workOrders = m_InvWorkOrderDAO.GetByCriteria(criterionList);

                            foreach (WorkOrder work1 in workOrders)
                            {
                                if (work1.deleted == 0)
                                {
                                    Name = work1.Name;
                                    cost = work1.woTtlCost;
                                }
                            }

                            //ICAItemDAO caItemsDAO = m_woDAO.GetCAItemDAO();
                            //List<CAInventoryItemList> items = m_ICAItemDAO.GetByCriteria(criterionList1);

                            //CAInventoryItemList CA = m_ICAItemDAO.GetById(wi.ItemID);
                            //Name = CA.name;
                            //cost = CA.price;
                            //serialNumber = CA.serialNumber;
                            //serviceCharge = CA.serviceCharge;
                            //deliveryCost = CA.deliveryCost;
                            // WO Bug Fix - End
                            break;
                        case woConstant.CATEGORY_TYPE_HK:  // House Keeping 
                            HKInventoryItemList HK = m_IHKItemDAO.GetById(wi.ItemID);
                            Name = HK.name;
                            cost = HK.price;
                            serialNumber = HK.serialNumber;
                            serviceCharge = HK.serviceCharge;
                            deliveryCost = HK.deliveryCost;
                            break;
                    }

                    emailMessage += "<tr><td>" + Name + "</td>";
                    string sn = "N/A";
                    if (serialNumber.Length > 0)
                        sn = serialNumber;
                    emailMessage += "<td align='center'>" + sn + "</td>";
                    emailMessage += "<td align='center'>" + qty.ToString() + "</td>";
                    emailMessage += "<td align='center'>" + serviceCharge.ToString("###0.00") + "</td>";
                    emailMessage += "<td align='center'>" + deliveryCost.ToString("###0.00") + "</td>";
                    emailMessage += "<td align='center'>" + cost.ToString("###0.00") + "</td>";
                    emailMessage += "</tr>";
                }
                //FB 1685
                emailMessage += "</table><br> <BR> Total Cost of Work Order = $" + work.woTtlCost.ToString("###0.00") + "<BR>";
                //WO Bug Fix
                emailMessage += " To be completed by: " + dt.ToString(user.DateFormat) +
                    " " + dt.ToString((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                work.email.Message = m_woEmail.AttachEmailLogo(conf.orgId) + emailMessage; //ZD 104183

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetCateringWorkOrderEmail
        // this email is unique to the new catering module
        private bool GetCateringWorkOrderEmail(ref WorkOrder work)
        {
            string emailMessage;

            try
            {
                userDAO userDAO = new userDAO(m_configPath, m_log);
                IUserDao uDAO = userDAO.GetUserDao();

                vrmUser user = new vrmUser();

                vrmUser sysAdmin = uDAO.GetByUserId(vrmUserConstant.vrmAdminId);
                // right now the confernece is updated in another transaction. Later
                // the conf info will have to come from another oject as it will be 
                // transient at this point...
                conferenceDAO confDAO = new conferenceDAO(m_configPath, m_log);
                IConferenceDAO cDAO = confDAO.GetConferenceDao();

                vrmConference conf = cDAO.GetByConfId(work.ConfID, work.InstanceID);

                work.email = new vrmEmail();
                InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                // this is a 2 pass method. First make a hashtable with all of the category id's
                // then enum through the table and creat 1 email for each provide admin that has ALL
                // of their menu's on it. Note if the admin is responsible for more than one provider he/she
                // will get an email for each provider.

                Hashtable providerId = new Hashtable();
                foreach (WorkItem wi in work.ItemList)
                {
                    Menu m = menuDAO.GetById(wi.ItemID);
                    if (!providerId.ContainsKey(m.ICategory.ID))
                        providerId.Add(m.ICategory.ID, m.ICategory.ID);

                }
                string emailBody = string.Empty;
                string dummy = string.Empty;
                //FB 1830
                //m_woEmail.getConferenceDetailsForEmail(user, work.ConfID, work.InstanceID, "", ref emailBody, ref dummy);

                InvServiceDAO serviceDAO = m_woDAO.GetInvServiceDAO();
                InventoryService Service = serviceDAO.GetById(work.deliveryType);
                string serviceName = Service.name;

                IDictionaryEnumerator iEnum = providerId.GetEnumerator();
                while (iEnum.MoveNext())
                {
                    int iKey = (int)iEnum.Value;

                    if (work.isNew)
                        work.email.Subject = "Conference work order setup.";
                    else
                        if (work.Status == vrmWorkOrderStatus.Completed)
                            work.email.Subject = "Conference work order completed.";
                        else
                            work.email.Subject = "Conference work order modification.";

                    emailMessage = "This is a notification for a pending work order assigned to you.<br>";
                    emailMessage += "<br>Conference Details:<br><br>";

                    emailMessage += emailBody;

                    emailMessage += "<br>Work Order Details:<br><br>";

                    foreach (WorkItem wi in work.ItemList)
                    {

                        Menu menu = menuDAO.GetById(wi.ItemID);

                        if (menu.categoryID == iKey)
                        {
                            work.email.emailFrom = sysAdmin.Email;
                            work.email.emailTo = string.Empty;

                            DateTime dt = work.CompletedBy;
                            if (!timeZone.userPreferedTime(conf.timezone, ref dt))
                                return false;
                            //WO Bug Fix
                            emailMessage += "<table border='1' cellpadding='5' cellspacing='0' style='width:628px;background-color:silver;font-size:10pt; font-family: Verdana;'>";
                            emailMessage += "<tr><td align='right'><b>Work Order</b></td>";
                            emailMessage += "<td align='left'>" + work.Name + "</td></tr>";

                            vrmRoom Loc = m_roomDAO.GetById(work.IRoom.roomId);

                            emailMessage += "<tr><td align='right'><b>Room Assigned To</b></td>";
                            emailMessage += "<td align='left'>" + Loc.tier2.TopTierName + "&gt;"; //ZD 102481
                            emailMessage += Loc.tier2.Name + "&gt;" + Loc.Name + "</td></tr>";

                            // in cases of new work orders we do not have inventoy set name 
                            string inventorySet = string.Empty;

                            //FB 1685
                            if (work.toUserID > 0)
                                user = uDAO.GetByUserId(work.toUserID);
                            else if (menu.ICategory.AdminID > 0)
                                user = uDAO.GetByUserId(menu.ICategory.AdminID);
                            else
                                user = uDAO.GetByUserId(work.AdminID);
                            if (work.email.emailTo.Length == 0)
                                work.email.emailTo = user.Email;

                            // FB 1685
                            if (menu.ICategory.AdminID > 0)
                                user = uDAO.GetByUserId(menu.ICategory.AdminID);
                            else
                                user = uDAO.GetByUserId(work.AdminID);

                            emailMessage += "<tr><td align='right'><b> Person-in-charge</b></td>";
                            emailMessage += "<td align='left'>" + user.FirstName + " " + user.LastName;
                            emailMessage += "(<a href='mailto:" +
                            user.Email + "' target='_blank'>" + user.Email + "</a>)</td></tr>"; //FB 1948

                            emailMessage += "<tr><td align='right'><b>Comments</b></td>";
                            emailMessage += "<td align='left'>" + work.Comment + "</td></tr>";

                            emailMessage += "<tr><td align='right'><b>Service Type</b></td>";
                            emailMessage += "<td align='left'>" + serviceName + "</td></tr>";

                            menuDAO = m_woDAO.GetMenuDAO();
                            emailMessage += "<tr><td align='right'><b>Catering Menu</b></td>";
                            emailMessage += "<td align='left'>" + menu.name + "</td></tr>";

                            timeZoneData aTz = new timeZoneData();
                            timeZone.GetTimeZone(user.TimeZone, ref aTz);

                            DateTime woDate = work.startBy;
                            timeZone.userPreferedTime(user.TimeZone, ref woDate);

                            emailMessage += "<tr><td align='right'><b>Deliver-by Date/Time</b></td>";
                            //WO Bug Fix  
                            emailMessage += "<td align='left'>" + woDate.ToString(user.DateFormat) + " " + woDate.ToString((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt") + "(" + aTz.TimeZone + ")</td></tr>";
                            //emailMessage += "<td align='left'>" + woDate.ToString("g") + "(" + aTz.TimeZone + ")</td></tr>";


                            emailMessage += "<tr><td align='right'><b>Total Cost($)</b></td>";
                            emailMessage += "<td align='left'>" + work.woTtlCost.ToString("###0.00") + "</td></tr></table>";  //WO Bug Fix

                            //WO Bug Fix
                            emailMessage += "<br><table border='1' cellpadding='5' cellspacing='0' style='background-color:silver;font-size:10pt; font-family: Verdana;width:628px'>";
                            emailMessage += "<tr><td align='center'><b>Requested Item</b></td>";
                            emailMessage += "<td align='center'><b>Menu Number</b></td>";
                            emailMessage += "<td align='center'><b>Requested Quantity</b></td>";
                            emailMessage += "<td align='center'><b>Special Instructions</b></td>";
                            emailMessage += "<td align='center'><b>Price ($)</b></td></tr>";

                            List<ICriterion> criterionList = new List<ICriterion>();
                            ICriterion criterium = Expression.Eq("categoryID", wi.ItemID);
                            criterionList.Add(criterium);

                            List<CAInventoryItemList> result = m_ICAItemDAO.GetByCriteria(criterionList, true);

                            foreach (CAInventoryItemList ca in result)
                            {

                                emailMessage += "<tr><td>" + ca.name + "</td>";
                                string comment = "N/A";
                                if (ca.comment.Length > 0)
                                    comment = ca.comment;
                                emailMessage += "<td align='center'>" + wi.ItemID.ToString() + "</td>";
                                emailMessage += "<td align='center'>" + wi.quantity.ToString() + "</td>";
                                emailMessage += "<td align='center'>" + comment + "</td>";
                                emailMessage += "<td align='center'>" + ca.price.ToString("###0.00") + "</td>";
                                emailMessage += "</tr>";
                            }
                            emailMessage += "</table><BR><BR>";

                            work.email.Message = m_woEmail.AttachEmailLogo(conf.orgId) + emailMessage; //ZD 104183

                            // store it here (only practical way to do this right now)
                        }
                    }
                    m_woDAO.SaveWorkOrderEmail(work.email);

                }
                return true;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetProviderDetails
        /// <summary>
        /// GetProviderDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetProviderDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmImg = new imageFactory(ref obj);//ZD 100175
            try
            {
                StringBuilder outxml = new StringBuilder(); //FB 2559 - Stringbuilder
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetProviderDetails/ID");
                int Id = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//GetProviderDetails/Type");
                int type = Int32.Parse(node.InnerXml.Trim());
                //ZD 104792 - Start
                string stmt = "";
                stmt = "select u.LastName + ', ' + u.FirstName as adminName, ic.* ";
                stmt += " ,isnull(Stuff ((SELECT distinct  N', ' + cast(r.LocationID as varchar(5)) from Inv_Room_D r  ";
                stmt += " where r.CategoryID = ic.ID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as locations  ";
                stmt += " from Inv_Category_D ic, usr_list_D u ";
                stmt += " where  ic.adminID = u.userid  ";
                if (Id > 0)
                    stmt += " and ic.ID = " + Id.ToString();
                stmt += " ; ";
                stmt += " select im.id, im.name, im.comments,im.price, il.itemId,il.Name as ItemName, l.ImageId,l.ImgResized,l.Image, img.AttributeImage  ";
                stmt += " ,isnull(Stuff ((SELECT distinct  N', ' + cast(isd.serviceId as varchar(5)) from Inv_ItemService_D isd  ";
                stmt += " where isd.menuId = im.id FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as serviceid  ";
                stmt += " from Inv_Menu_D im, Inv_ItemList_CA_D il , Inv_list_D l, Img_List_D img ";
                stmt += " where im.ID = il.CategoryID ";
                if (Id > 0)
                    stmt += " and im.categoryid = " + Id.ToString();
                stmt += " and il.ItemID = l.id and l.ImageId = img.ImageId ";
                stmt += "  order by im.id, il.itemId ";

                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                sqlCon.OpenConnection();
                System.Data.DataSet ds = new System.Data.DataSet();
                ds = sqlCon.ExecuteDataSet(stmt);
                sqlCon.CloseConnection();

                DataTable dtMenu = new DataTable();
                DataTable dtItems = new DataTable();

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        dtMenu = ds.Tables[0];
                        dtItems = ds.Tables[1];
                    }
                }

                outxml.Append("<GetProviderDetails>");
                if (dtMenu != null)
                {
                    for (int c = 0; c < dtMenu.Rows.Count; c++)
                    {
                        outxml.Append("<ID>" + dtMenu.Rows[c]["ID"].ToString() + "</ID>");
                        outxml.Append("<Name>" + dtMenu.Rows[c]["Name"].ToString() + "</Name>");
                        outxml.Append("<Type>" + dtMenu.Rows[c]["Type"].ToString() + "</Type>");
                        outxml.Append("<Notify>" + dtMenu.Rows[c]["Notify"].ToString() + "</Notify>");
                        outxml.Append("<AdminID>" + dtMenu.Rows[c]["AdminID"].ToString() + "</AdminID>");
                        outxml.Append("<AdminName>" + dtMenu.Rows[c]["Adminname"].ToString() + "</AdminName>");

                        outxml.Append("<SelectedLocations>");
                        string[] locations = dtMenu.Rows[c]["locations"].ToString().Split(',');
                        for (Int32 l = 0; l < locations.Length; l++)
                        {
                            outxml.Append("<Selected>");
                            outxml.Append("<ID>" + locations[l] + "</ID>");
                            outxml.Append("</Selected>");
                        }
                        outxml.Append("</SelectedLocations>");

                        outxml.Append("<MenuList>");
                        string menuid = "", itemid = "";
                        string serviceid = "", strservice = "";
                        StringBuilder itemxml = new StringBuilder();
                        StringBuilder itemxmlMain = new StringBuilder();
                        StringBuilder menuxml = new StringBuilder();
                        StringBuilder servicexml = new StringBuilder();
                        for (int i = 0; i < dtItems.Rows.Count; i++)
                        {
                            if (menuid != dtItems.Rows[i]["id"].ToString() || i == dtItems.Rows.Count - 1) 
                            {
                                if(menuid != "")
                                    outxml.Append(menuxml.ToString());

                                if (menuid != dtItems.Rows[i]["id"].ToString())
                                {
                                    menuxml = new StringBuilder();
                                    menuxml.Append("<Menu>");
                                    menuxml.Append("<ID>" + dtItems.Rows[i]["id"].ToString() + "</ID>");  //id or menuid
                                    menuxml.Append("<Name>" + dtItems.Rows[i]["Name"].ToString() + "</Name>");
                                    menuxml.Append("<Comments>" + dtItems.Rows[i]["Comments"].ToString() + "</Comments>");
                                    menuxml.Append("<Price>" + dtItems.Rows[i]["price"].ToString() + "</Price>");
                                }


                            }

                            if (menuid != dtItems.Rows[i]["id"].ToString() || i == dtItems.Rows.Count - 1)
                            {
                                if (menuid != "")
                                    outxml.Append(servicexml.ToString());
                                if (menuid != dtItems.Rows[i]["id"].ToString())
                                {
                                    strservice = "";
                                    string[] strarService = dtItems.Rows[i]["serviceid"].ToString().Split(',');
                                    for (Int32 s = 0; s < strarService.Length; s++)
                                        strservice += "<Service><ID>" + strarService[s] + "</ID></Service>";
                                }
                            }

                            if(strservice != "")
                            {
                                servicexml = new StringBuilder();
                                servicexml.Append("<CateringServices>");
                                servicexml.Append(strservice);
                                servicexml.Append("</CateringServices>");
                                strservice = "";
                            }

                            //if (itemid != dtItems.Rows[i]["itemid"].ToString())
                            //{
                                if ((menuid != dtItems.Rows[i]["id"].ToString() || i == dtItems.Rows.Count - 1))
                                {
                                    if ((menuid == "" && i == dtItems.Rows.Count - 1) || (menuid == dtItems.Rows[i]["id"].ToString() && i == dtItems.Rows.Count - 1))
                                    {
                                        AddItem(dtItems, ref itemxml, i);

                                        itemxmlMain = new StringBuilder();
                                        itemxmlMain.Append("<ItemsList>");
                                        itemxmlMain.Append(itemxml.ToString());
                                        itemxmlMain.Append("</ItemsList>");

                                        if(i != 0)
                                            outxml.Append(itemxmlMain.ToString());
                                        itemxml = new StringBuilder();

                                    }
                                    else if (menuid != dtItems.Rows[i]["id"].ToString() || i == dtItems.Rows.Count - 1)
                                    {
                                        itemxmlMain = new StringBuilder();
                                        itemxmlMain.Append("<ItemsList>");
                                        itemxmlMain.Append(itemxml.ToString());
                                        itemxmlMain.Append("</ItemsList>");

                                        outxml.Append(itemxmlMain.ToString());
                                        itemxml = new StringBuilder();

                                        if (menuid != dtItems.Rows[i]["id"].ToString() && i == dtItems.Rows.Count - 1)
                                        {
                                            itemxml = new StringBuilder();

                                            AddItem(dtItems, ref itemxml, i);

                                            itemxmlMain = new StringBuilder();
                                            itemxmlMain.Append("<ItemsList>");
                                            itemxmlMain.Append(itemxml.ToString());
                                            itemxmlMain.Append("</ItemsList>");
                                        }
                                    }
                                }

                                if (i < dtItems.Rows.Count - 1)
                                    AddItem(dtItems, ref itemxml, i);
                            //}

                            if (menuid != "" && (menuid != dtItems.Rows[i]["id"].ToString() || i == dtItems.Rows.Count - 1))
                            {
                                outxml.Append("</Menu>");
                            }

                            if (menuid != dtItems.Rows[i]["id"].ToString() && i == dtItems.Rows.Count - 1)
                            {
                                outxml.Append(menuxml.ToString());
                                outxml.Append(servicexml.ToString());                               
                                outxml.Append(itemxmlMain.ToString());
                                outxml.Append("</Menu>");
                            }
                          
                            menuid = dtItems.Rows[i]["id"].ToString();
                            serviceid = dtItems.Rows[i]["serviceid"].ToString();
                            itemid = dtItems.Rows[i]["itemid"].ToString();                            
                        }

                        outxml.Append("</MenuList>");
                    }
                }
                //ZD 104792 - End
                outxml.Append("</GetProviderDetails>");
                obj.outXml = outxml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }

            return bRet;
        }
        #endregion

        private void AddItem(DataTable dtItems, ref StringBuilder itemxml, Int32 i)
        {
            itemxml.Append("<Item>");
            itemxml.Append("<ID>" + dtItems.Rows[i]["itemid"].ToString() + "</ID>");
            itemxml.Append("<Name>" + dtItems.Rows[i]["itemname"].ToString() + "</Name>");
            itemxml.Append("<ImageID>" + dtItems.Rows[i]["imageid"].ToString() + "</ImageID>");
            itemxml.Append("<ImageName>" + dtItems.Rows[i]["Image"].ToString() + "</ImageName>");
            itemxml.Append("<Type>2</Type>");

            string imageDt = "";
            InventoryList itList = m_InvListDAO.GetById(Int32.Parse(dtItems.Rows[i]["itemid"].ToString()));
            vrmImage imObj = m_IImageDAO.GetById(Int32.Parse(dtItems.Rows[i]["imageid"].ToString()));
            if (imObj != null)
                imageDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage);
            if (dtItems.Rows[i]["ImgResized"].ToString() == "0")
            {
                byte[] imageArr = vrmImg.ConvertBase64ToByteArray(imageDt);

                System.IO.MemoryStream myMemStream = new System.IO.MemoryStream(imageArr);
                System.Drawing.Image fullsizeImage = System.Drawing.Image.FromStream(myMemStream);
                System.Drawing.Image newImage = fullsizeImage.GetThumbnailImage(200, 200, null, IntPtr.Zero);
                MemoryStream ms = new MemoryStream();
                newImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] imgArray = ms.ToArray();
                imageDt = imgArray.ToString();

                imObj.AttributeImage = imgArray;
                m_IImageDAO.Update(imObj);

                itList.ImgResized = 1;
                m_InvListDAO.Update(itList);
            }

            itemxml.Append("<Image>" + imageDt + "</Image>");
            itemxml.Append("</Item>");
        }

        #region SetProviderDetails
        /// <summary>
        /// SetProviderDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// //ZD 104792 - Modified Nihibernate codes to SQL
        public bool SetProviderDetails(ref vrmDataObject obj)
        {
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                InventoryCategory cat = new InventoryCategory();

                node = xd.SelectSingleNode("//SetProviderDetails/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //ZD 103364

                node = xd.SelectSingleNode("//SetProviderDetails/ID");
                string ID = node.InnerXml.Trim();
                if (ID.Length < 1 || ID.Trim().ToLower() == "new")
                {
                    cat.ID = 0;
                }
                else
                {
                    cat.ID = Int32.Parse(ID);
                }

                node = xd.SelectSingleNode("//SetProviderDetails/Type");
                string Type = node.InnerXml.Trim();
                int iType = Int32.Parse(Type);

                node = xd.SelectSingleNode("//SetProviderDetails/Name");
                string Name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetProviderDetails/AdminID");
                string AdminID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetProviderDetails/Notify");
                string Notify = node.InnerXml.Trim();

                int adminId = Int32.Parse(AdminID);
                //
                // check admin in charge for authoriziation
                //
                if (!checkInventoryAdmin(adminId, iType))
                {
                    //FB 1881 start
                    //myVRMException e =
                    //    new myVRMException("Error user not authorized as administrator in charge. Inventory NOT saved.");
                    myvrmEx = new myVRMException(487);
                    m_log.Error("sytemException", myvrmEx);
                    throw myvrmEx;
                    //FB 1881 end
                }
                /*
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium = Expression.Eq("Name", Name).IgnoreCase();
                criterionList.Add(criterium);
                criterium = Expression.Eq("Type", iType);
                criterionList.Add(criterium);
                criterium = Expression.Eq("deleted", 0);
                criterionList.Add(criterium);
                criterium = Expression.Eq("orgId", organizationID); //organziation module
                criterionList.Add(criterium);
                if (cat.ID > 0)
                    criterionList.Add(Expression.Not(Expression.Eq("ID", cat.ID)));

                List<InventoryCategory> result = m_InvCategoryDAO.GetByCriteria(criterionList, true);

                if (result.Count > 0)
                {
               */
                string stmt = "";
                Int32 invCnt = 0;

                stmt = "select count(*) from Inv_Category_D where Name = '" + Name + "' and Type = " + iType.ToString();
                stmt += " and deleted = 0 and orgId = " + organizationID;
                if (cat.ID > 0)
                    stmt += " and ID not in(" + cat.ID.ToString() + ")";

                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                sqlCon.OpenConnection();
                object retVal = sqlCon.ExecuteScalar(stmt);
                sqlCon.CloseConnection();
                if (retVal != null)
                    invCnt = (int)retVal;

                if (invCnt > 0)
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("Error name already exists ", 415);
                    myvrmEx = new myVRMException(488);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
                // remove old entities
                if (cat.ID > 0)
                {
                    //ZD 104792
                    /*
                    List<ICriterion> deleteRoomList = new List<ICriterion>();
                    deleteRoomList.Add(Expression.Eq("categoryID", cat.ID));

                    List<InventoryRoom> deleteList = new List<InventoryRoom>();
                    deleteList = m_InvRoomDAO.GetByCriteria(deleteRoomList);

                    foreach (InventoryRoom dRoom in deleteList)
                        m_InvRoomDAO.Delete(dRoom);
                     */

                    stmt = "delete from Inv_Room_D where CategoryID = " + cat.ID.ToString();
                    sqlCon.OpenConnection();
                    int strExec1 = sqlCon.ExecuteNonQuery(stmt);
                    sqlCon.CloseConnection();

                }

                cat.Name = Name;
                cat.Comment = string.Empty;
                cat.AdminID = adminId;
                cat.Type = iType;
                cat.Notify = Notify;
                cat.AssignedCostCtr = string.Empty;
                cat.AllowOverBooking = string.Empty;
                cat.orgId = organizationID; //organization module

                XmlNodeList locationList = xd.SelectNodes("//SetProviderDetails/SelectedLocations/Selected");
                foreach (XmlNode lNode in locationList)
                {
                    InventoryRoom rm = new InventoryRoom();
                    rm.IRoom.roomId = Int32.Parse(lNode.SelectSingleNode("ID").InnerText);
                    rm.locationID = rm.IRoom.roomId;
                    rm.categoryID = cat.ID;
                    rm.ICategory = cat;
                    cat.RoomList.Add(rm);
                    //ZD 103364 start
                    if (orgInfo.isDeptUser == 1)
                    {
                        if (!checkDepartmentalAuthorization(rm.IRoom.roomId, adminId))
                        {
                            myvrmEx = new myVRMException(489);
                            m_log.Error("sytemException", myvrmEx);
                            throw myvrmEx;
                        }
                    }
                    ////ZD 103364 End
                }

                XmlNodeList menuList = xd.SelectNodes("//SetProviderDetails/MenuList/Menu");
                Menu menu;

                foreach (XmlNode innerNode in menuList)
                {
                    menu = new Menu();
                    int delete = 0;
                    string mID = innerNode.SelectSingleNode("ID").InnerText;
                    if (mID.ToLower() == "new")
                        mID = string.Empty;

                    if (mID.Length > 0)
                        menu.id = Int32.Parse(mID);
                    else
                        menu.id = 0;
                    menu.name = innerNode.SelectSingleNode("Name").InnerText;
                    delete = Int32.Parse(innerNode.SelectSingleNode("Deleted").InnerText);

                    //FB 1478 - Work order FB - start
                    /* *** Commented the functionality of checking the menu names for duplication - start *** */

                    //check if name exists
                    //if( menu.id == 0)
                    //    if(menuNameExists(menu.name))
                    //        delete = 1;

                    /* *** Commented the functionality of checking the menu names for duplication - end *** */
                    //FB 1478 - Work order FB - end

                    if (delete == 1)
                    {
                        if (menu.id > 0)
                        {
                            string workList = string.Empty;
                            if (canDeleteMenu(menu.id, ref workList))
                            {
                                //ZD 104792
                                sqlCon.OpenConnection();
                                stmt = "delete from Inv_ItemService_D where  menuId =" + menu.id.ToString();
                                int strExec1 = sqlCon.ExecuteNonQuery(stmt);

                                stmt = "delete from Inv_ItemList_CA_D where CategoryID =" + menu.id.ToString();
                                strExec1 = sqlCon.ExecuteNonQuery(stmt);
                                sqlCon.CloseConnection();


                                /*
                                List<ICriterion> deleteMenuServiceList = new List<ICriterion>();
                                deleteMenuServiceList.Add(Expression.Eq("menuId", menu.id));
                                InvMenuServiceDAO menuServiceDAO = m_woDAO.GetMenuServiceDAO();

                                List<MenuService> deleteList = menuServiceDAO.GetByCriteria(deleteMenuServiceList);

                                foreach (MenuService service in deleteList)
                                    menuServiceDAO.Delete(service);

                                List<ICriterion> deleteItemList = new List<ICriterion>();
                                deleteItemList.Add(Expression.Eq("categoryID", menu.id));
                                List<CAInventoryItemList> deleteCAList = m_ICAItemDAO.GetByCriteria(deleteItemList);

                                foreach (CAInventoryItemList a_ca in deleteCAList)
                                    m_ICAItemDAO.Delete(a_ca);

                                InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                                menuDAO.Delete(menu);
                                 */
                                //    InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                                //    List<ICriterion> deleteMenuList = new List<ICriterion>();
                                //    deleteItemList.Add(Expression.Eq("id", menu.id));
                                //    List<Menu> d_menuList = menuDAO.GetByCriteria(deleteMenuList,true);
                                //    if (d_menuList.Count > 0)
                                //    {
                                //        menuDAO = m_woDAO.GetMenuDAO();
                                //        Menu m = d_menuList[0];
                                //        menuDAO.Delete(m);
                                //    }
                            }
                        }

                    }
                    else
                    {
                        menu.name = innerNode.SelectSingleNode("Name").InnerText;
                        menu.comments = innerNode.SelectSingleNode("Comments").InnerText;
                        menu.price = float.Parse(innerNode.SelectSingleNode("Price").InnerText, CultureInfo.InvariantCulture); //ZD 101714

                        XmlNodeList serviceList = innerNode.SelectNodes("CateringServices/Service");
                        if (menu.id > 0)
                        {
                            //ZD 104792
                            sqlCon.OpenConnection();
                            stmt = "delete from Inv_ItemService_D where  menuId =" + menu.id.ToString();
                            int strExec1 = sqlCon.ExecuteNonQuery(stmt);
                            sqlCon.CloseConnection();
                            /*
                            List<ICriterion> deleteMenuServiceList = new List<ICriterion>();
                            deleteMenuServiceList.Add(Expression.Eq("menuId", menu.id));
                            InvMenuServiceDAO menuServiceDAO = m_woDAO.GetMenuServiceDAO();

                            List<MenuService> deleteList = menuServiceDAO.GetByCriteria(deleteMenuServiceList);

                            foreach (MenuService service in deleteList)
                                menuServiceDAO.Delete(service);
                             */
                        }

                        foreach (XmlNode sNode in serviceList)
                        {
                            MenuService ms = new MenuService();
                            ms.menuId = menu.id;
                            ms.serviceId = Int32.Parse(sNode.SelectSingleNode("ID").InnerText);
                            ms.IMenu = menu;

                            menu.MenuServiceList.Add(ms);
                        }
                        XmlNodeList itemList = innerNode.SelectNodes("ItemsList/Item");
                        if (menu.id > 0)
                        {
                            //ZD 104792
                            sqlCon.OpenConnection();
                            stmt = "delete from Inv_ItemList_CA_D where CategoryID =" + menu.id.ToString();
                            Int32 strExec1 = sqlCon.ExecuteNonQuery(stmt);
                            sqlCon.CloseConnection();

                            /*
                            List<ICriterion> deleteItemList = new List<ICriterion>();
                            deleteItemList.Add(Expression.Eq("categoryID", menu.id));
                            List<CAInventoryItemList> deleteList = m_ICAItemDAO.GetByCriteria(deleteItemList);

                            foreach (CAInventoryItemList a_ca in deleteList)
                                m_ICAItemDAO.Delete(a_ca);
                             */
                        }

                        foreach (XmlNode iNode in itemList)
                        {
                            InventoryList item = m_InvListDAO.GetById(Int32.Parse(iNode.SelectSingleNode("ID").InnerText));

                            CAInventoryItemList ca = new CAInventoryItemList();
                            ca.categoryID = menu.id;
                            ca.name = item.Name;
                            ca.itemId = item.ID;
                            ca.IMenu = menu;
                            menu.CAItemList.Add(ca);
                        }
                        menu.categoryID = cat.ID;
                        menu.ICategory = cat;
                        cat.MenuList.Add(menu);
                    }
                }
                if (cat.ID > 0)
                {
                    m_InvCategoryDAO.Update(cat);
                }
                else
                {
                    m_InvCategoryDAO.Save(cat);
                }
                obj.outXml = "<SetProviderDetails>";
                obj.outXml += "<ID>" + cat.ID.ToString() + "</ID>";
                obj.outXml += "</SetProviderDetails>";

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("myVRMException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                if (sqlCon != null) //ZD 104792
                    sqlCon.CloseConnection();
                sqlCon = null;
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                if (sqlCon != null) //ZD 104792
                    sqlCon.CloseConnection();
                sqlCon = null;
                return false;
            }
        }

        public bool SetProviderDetailsOld(ref vrmDataObject obj)
        {
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                InventoryCategory cat = new InventoryCategory();

                node = xd.SelectSingleNode("//SetProviderDetails/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //ZD 103364

                node = xd.SelectSingleNode("//SetProviderDetails/ID");
                string ID = node.InnerXml.Trim();
                if (ID.Length < 1 || ID.Trim().ToLower() == "new")
                {
                    cat.ID = 0;
                }
                else
                {
                    cat.ID = Int32.Parse(ID);
                }

                node = xd.SelectSingleNode("//SetProviderDetails/Type");
                string Type = node.InnerXml.Trim();
                int iType = Int32.Parse(Type);

                node = xd.SelectSingleNode("//SetProviderDetails/Name");
                string Name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetProviderDetails/AdminID");
                string AdminID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetProviderDetails/Notify");
                string Notify = node.InnerXml.Trim();

                int adminId = Int32.Parse(AdminID);
                //
                // check admin in charge for authoriziation
                //
                if (!checkInventoryAdmin(adminId, iType))
                {
                    //FB 1881 start
                    //myVRMException e =
                    //    new myVRMException("Error user not authorized as administrator in charge. Inventory NOT saved.");
                    myvrmEx = new myVRMException(487);
                    m_log.Error("sytemException", myvrmEx);
                    throw myvrmEx;
                    //FB 1881 end
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium = Expression.Eq("Name", Name).IgnoreCase();
                criterionList.Add(criterium);
                criterium = Expression.Eq("Type", iType);
                criterionList.Add(criterium);
                criterium = Expression.Eq("deleted", 0);
                criterionList.Add(criterium);
                criterium = Expression.Eq("orgId", organizationID); //organziation module
                criterionList.Add(criterium);
                if (cat.ID > 0)
                    criterionList.Add(Expression.Not(Expression.Eq("ID", cat.ID)));

                List<InventoryCategory> result = m_InvCategoryDAO.GetByCriteria(criterionList, true);

                if (result.Count > 0)
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("Error name already exists ", 415);
                    myvrmEx = new myVRMException(488);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
                // remove old entities
                if (cat.ID > 0)
                {
                    List<ICriterion> deleteRoomList = new List<ICriterion>();
                    deleteRoomList.Add(Expression.Eq("categoryID", cat.ID));

                    List<InventoryRoom> deleteList = new List<InventoryRoom>();
                    deleteList = m_InvRoomDAO.GetByCriteria(deleteRoomList);

                    foreach (InventoryRoom dRoom in deleteList)
                        m_InvRoomDAO.Delete(dRoom);
                }

                cat.Name = Name;
                cat.Comment = string.Empty;
                cat.AdminID = adminId;
                cat.Type = iType;
                cat.Notify = Notify;
                cat.AssignedCostCtr = string.Empty;
                cat.AllowOverBooking = string.Empty;
                cat.orgId = organizationID; //organization module

                XmlNodeList locationList = xd.SelectNodes("//SetProviderDetails/SelectedLocations/Selected");
                foreach (XmlNode lNode in locationList)
                {
                    InventoryRoom rm = new InventoryRoom();
                    rm.IRoom.roomId = Int32.Parse(lNode.SelectSingleNode("ID").InnerText);
                    rm.locationID = rm.IRoom.roomId;
                    rm.categoryID = cat.ID;
                    rm.ICategory = cat;
                    cat.RoomList.Add(rm);
                    //ZD 103364 start
                    if (orgInfo.isDeptUser == 1)
                    {
                        if (!checkDepartmentalAuthorization(rm.IRoom.roomId, adminId))
                        {
                            myvrmEx = new myVRMException(489);
                            m_log.Error("sytemException", myvrmEx);
                            throw myvrmEx;
                        }
                    }
                    ////ZD 103364 End
                }

                XmlNodeList menuList = xd.SelectNodes("//SetProviderDetails/MenuList/Menu");
                Menu menu;

                foreach (XmlNode innerNode in menuList)
                {
                    menu = new Menu();
                    int delete = 0;
                    string mID = innerNode.SelectSingleNode("ID").InnerText;
                    if (mID.ToLower() == "new")
                        mID = string.Empty;

                    if (mID.Length > 0)
                        menu.id = Int32.Parse(mID);
                    else
                        menu.id = 0;
                    menu.name = innerNode.SelectSingleNode("Name").InnerText;
                    delete = Int32.Parse(innerNode.SelectSingleNode("Deleted").InnerText);

                    //FB 1478 - Work order FB - start
                    /* *** Commented the functionality of checking the menu names for duplication - start *** */

                    //check if name exists
                    //if( menu.id == 0)
                    //    if(menuNameExists(menu.name))
                    //        delete = 1;

                    /* *** Commented the functionality of checking the menu names for duplication - end *** */
                    //FB 1478 - Work order FB - end

                    if (delete == 1)
                    {
                        if (menu.id > 0)
                        {
                            string workList = string.Empty;
                            if (canDeleteMenu(menu.id, ref workList))
                            {
                                List<ICriterion> deleteMenuServiceList = new List<ICriterion>();
                                deleteMenuServiceList.Add(Expression.Eq("menuId", menu.id));
                                InvMenuServiceDAO menuServiceDAO = m_woDAO.GetMenuServiceDAO();

                                List<MenuService> deleteList = menuServiceDAO.GetByCriteria(deleteMenuServiceList);

                                foreach (MenuService service in deleteList)
                                    menuServiceDAO.Delete(service);

                                List<ICriterion> deleteItemList = new List<ICriterion>();
                                deleteItemList.Add(Expression.Eq("categoryID", menu.id));
                                List<CAInventoryItemList> deleteCAList = m_ICAItemDAO.GetByCriteria(deleteItemList);

                                foreach (CAInventoryItemList a_ca in deleteCAList)
                                    m_ICAItemDAO.Delete(a_ca);

                                InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                                menuDAO.Delete(menu);
                                //    InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                                //    List<ICriterion> deleteMenuList = new List<ICriterion>();
                                //    deleteItemList.Add(Expression.Eq("id", menu.id));
                                //    List<Menu> d_menuList = menuDAO.GetByCriteria(deleteMenuList,true);
                                //    if (d_menuList.Count > 0)
                                //    {
                                //        menuDAO = m_woDAO.GetMenuDAO();
                                //        Menu m = d_menuList[0];
                                //        menuDAO.Delete(m);
                                //    }
                            }
                        }

                    }
                    else
                    {
                        menu.name = innerNode.SelectSingleNode("Name").InnerText;
                        menu.comments = innerNode.SelectSingleNode("Comments").InnerText;
                        menu.price = float.Parse(innerNode.SelectSingleNode("Price").InnerText, CultureInfo.InvariantCulture); //ZD 101714

                        XmlNodeList serviceList = innerNode.SelectNodes("CateringServices/Service");
                        if (menu.id > 0)
                        {
                            List<ICriterion> deleteMenuServiceList = new List<ICriterion>();
                            deleteMenuServiceList.Add(Expression.Eq("menuId", menu.id));
                            InvMenuServiceDAO menuServiceDAO = m_woDAO.GetMenuServiceDAO();

                            List<MenuService> deleteList = menuServiceDAO.GetByCriteria(deleteMenuServiceList);

                            foreach (MenuService service in deleteList)
                                menuServiceDAO.Delete(service);
                        }

                        foreach (XmlNode sNode in serviceList)
                        {
                            MenuService ms = new MenuService();
                            ms.menuId = menu.id;
                            ms.serviceId = Int32.Parse(sNode.SelectSingleNode("ID").InnerText);
                            ms.IMenu = menu;

                            menu.MenuServiceList.Add(ms);
                        }
                        XmlNodeList itemList = innerNode.SelectNodes("ItemsList/Item");
                        if (menu.id > 0)
                        {
                            List<ICriterion> deleteItemList = new List<ICriterion>();
                            deleteItemList.Add(Expression.Eq("categoryID", menu.id));
                            List<CAInventoryItemList> deleteList = m_ICAItemDAO.GetByCriteria(deleteItemList);

                            foreach (CAInventoryItemList a_ca in deleteList)
                                m_ICAItemDAO.Delete(a_ca);
                        }

                        foreach (XmlNode iNode in itemList)
                        {
                            InventoryList item = m_InvListDAO.GetById(Int32.Parse(iNode.SelectSingleNode("ID").InnerText));

                            CAInventoryItemList ca = new CAInventoryItemList();
                            ca.categoryID = menu.id;
                            ca.name = item.Name;
                            ca.itemId = item.ID;
                            ca.IMenu = menu;
                            menu.CAItemList.Add(ca);
                        }
                        menu.categoryID = cat.ID;
                        menu.ICategory = cat;
                        cat.MenuList.Add(menu);
                    }
                }
                if (cat.ID > 0)
                {
                    m_InvCategoryDAO.Update(cat);
                }
                else
                {
                    m_InvCategoryDAO.Save(cat);
                }
                obj.outXml = "<SetProviderDetails>";
                obj.outXml += "<ID>" + cat.ID.ToString() + "</ID>";
                obj.outXml += "</SetProviderDetails>";

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("myVRMException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
        }
        #endregion

        #region GetCateringServices
        /// <summary>
        /// GetCateringServices
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetCateringServices(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetCateringServices/UserID");
                string userID = node.InnerXml.Trim();

                InvServiceDAO serviceDAO = m_woDAO.GetInvServiceDAO();
                List<InventoryService> ServiceList = serviceDAO.GetAll();

                obj.outXml += "<CateringServices>";
                foreach (InventoryService Is in ServiceList)
                {
                    obj.outXml += "<Service>";
                    obj.outXml += "<ID>" + Is.id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + Is.name + "</Name>";
                    obj.outXml += "</Service>";
                }
                obj.outXml += "</CateringServices>";

            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }

            return bRet;
        }
        #endregion

        #region GetProviderWorkorderDetails
        /// <summary>
        /// GetProviderWorkorderDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetProviderWorkorderDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetProviderWorkorderDetails/WorkorderID");
                string ID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetProviderWorkorderDetails/Type");
                string Type = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetProviderWorkorderDetails/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                //FB 2274 Starts
                node = xd.SelectSingleNode("//GetProviderWorkorderDetails/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                //FB 2014 
                int utcEnabled = 0;
                if (xd.SelectSingleNode("//GetProviderWorkorderDetails/utcEnabled") != null)
                {
                    Int32.TryParse(xd.SelectSingleNode("//GetProviderWorkorderDetails/utcEnabled").InnerText.Trim(), out utcEnabled);
                }

                int workID = 0;
                if (ID.Length > 0)
                    workID = Int32.Parse(ID);

                node = xd.SelectSingleNode("//GetProviderWorkorderDetails/ConfID");
                string ConfID = node.InnerXml.Trim();


                obj.outXml = "<GetProviderWorkorderDetails>";
                obj.outXml += "<ConfID>" + ConfID + "</ConfID>";
                obj.outXml += "<Type>" + Type + "</Type>";

                List<ICriterion> criterionList = new List<ICriterion>();
                int cID = 0;
                int iID = 0;

                criterionList.Add(Expression.Eq("orgId", organizationID)); //organization module
                if (workID > 0)
                {
                    criterionList.Add(Expression.Eq("ID", workID));
                    criterionList.Add(Expression.Eq("deleted", 0));

                }
                else
                {
                    CConfID conf;

                    if (ConfID.Length > 0)
                    {
                        conf = new CConfID(ConfID);
                        cID = conf.ID;
                        iID = conf.instance;
                        if (iID == 0)
                            iID = 1;
                    }
                    criterionList.Add(Expression.Eq("ConfID", cID));
                    criterionList.Add(Expression.Eq("InstanceID", iID));
                }

                List<WorkOrder> workOrders = m_InvWorkOrderDAO.GetByCriteria(criterionList);
                List<ICriterion> criterionList1 = new List<ICriterion>();//ZD 100757

                obj.outXml += "<Workorders>";
                foreach (WorkOrder work in workOrders)
                {
                    if (work.Type == 2)
                    {
                        if (work.deleted == 0)
                        {
                            obj.outXml += "<Workorder>";

                            obj.outXml += "<ID>" + work.ID.ToString() + "</ID>";
                            obj.outXml += "<Name>" + work.Name + "</Name>";
                            obj.outXml += "<SelectedService>" + work.deliveryType.ToString() + "</SelectedService>";
                            obj.outXml += "<Comments>" + work.Comment + "</Comments>";
                            obj.outXml += "<RoomID>" + work.IRoom.roomId.ToString() + "</RoomID>";
                            obj.outXml += "<RoomName>" + work.IRoom.Name + "</RoomName>";

                            //WO Bug Fix
                            int woTimeZone = work.woTimeZone;
                            DateTime completeBy = work.CompletedBy;
                            //FB 2014
                            if (utcEnabled != 1)
                                timeZone.userPreferedTime(woTimeZone, ref completeBy);

                            obj.outXml += "<DeliverByDate>" + completeBy.ToString("d") + "</DeliverByDate>";
                            obj.outXml += "<DeliverByTime>" + completeBy.ToString("t") + "</DeliverByTime>";
                            obj.outXml += "<Price>" + work.woTtlCost.ToString() + "</Price>";

                            //ZD 100757- START
                            obj.outXml += "<AssignedAdminID>" + work.AdminID.ToString() + "</AssignedAdminID>";
                            criterionList1 = new List<ICriterion>();
                            criterionList1.Add(Expression.Eq("userid", work.AdminID));
                            List<vrmUser> myVrmUser = m_vrmUserDAO.GetByCriteria(criterionList1);
                            obj.outXml += "<AssignedToName>" + myVrmUser[0].FirstName + " " + myVrmUser[0].LastName + "</AssignedToName>";
                            //ZD 100757- END

                            obj.outXml += "<MenuList>";

                            foreach (WorkItem wi in work.ItemList)
                            {
                                obj.outXml += "<Menu>";
                                obj.outXml += "<ID>" + wi.ItemID.ToString() + "</ID>";
                                obj.outXml += "<Quantity>" + wi.quantity.ToString() + "</Quantity>";

                                InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                                Menu m = menuDAO.GetById(wi.ItemID);

                                obj.outXml += "<Name>" + m.name + "</Name>";
                                obj.outXml += "<ItemsList>";
                                foreach (CAInventoryItemList ca in m.CAItemList)
                                {
                                    obj.outXml += "<Item>";
                                    obj.outXml += "<ID>" + ca.ID.ToString() + "</ID>";
                                    obj.outXml += "<Name>" + ca.name + "</Name>";
                                    obj.outXml += "</Item>";
                                }
                                obj.outXml += "</ItemsList>";
                                obj.outXml += "</Menu>";

                            }
                            obj.outXml += "</MenuList>";
                            obj.outXml += "</Workorder>";
                        }

                    }
                }
                obj.outXml += "</Workorders>";
                obj.outXml += "</GetProviderWorkorderDetails>";
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }

            return bRet;
        }
        #endregion

        #region SearchProviderMenus
        /// <summary>
        /// SearchProviderMenus
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SearchProviderMenus(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//SearchProviderMenus/RoomID");
                string RoomID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SearchProviderMenus/ServiceID");
                string ServiceID = node.InnerXml.Trim();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("locationID", Int32.Parse(RoomID)));

                List<InventoryRoom> roomList = m_InvRoomDAO.GetByCriteria(criterionList);

                List<int> catId = new List<int>();
                foreach (InventoryRoom rm in roomList)
                {
                    catId.Add(rm.categoryID);
                }

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Type", 2));
                criterionList.Add(Expression.In("ID", catId));

                List<InventoryCategory> catList = m_InvCategoryDAO.GetByCriteria(criterionList);

                int sId = Int32.Parse(ServiceID);
                obj.outXml = "<SearchProviderMenus>";
                foreach (InventoryCategory cat in catList)
                {
                    foreach (Menu menu in cat.MenuList)
                    {
                        foreach (MenuService ser in menu.MenuServiceList)
                        {
                            if (ser.serviceId == sId)
                            {
                                obj.outXml += "<Menu>";
                                obj.outXml += "<ID>" + menu.id.ToString() + "</ID>";
                                obj.outXml += "<Name>" + menu.name + "</Name>";
                                obj.outXml += "<Price>" + menu.price + "</Price> ";
                                obj.outXml += "<ItemsList>";
                                foreach (CAInventoryItemList ca in menu.CAItemList)
                                {
                                    obj.outXml += "<Item>";
                                    obj.outXml += "<ID>" + ca.ID.ToString() + "</ID>";
                                    obj.outXml += "<Name>" + ca.name + "</Name>";
                                    obj.outXml += "</Item>";
                                }
                                obj.outXml += "</ItemsList>";
                                obj.outXml += "</Menu>";
                                break;
                            }
                        }


                    }
                }
                obj.outXml += "</SearchProviderMenus>";
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
            return bRet;
        }
        #endregion

        #region SetProviderWorkorderDetails
        /// <summary>
        /// SetProviderWorkorderDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetProviderWorkorderDetails(ref vrmDataObject obj)
        {
            bool isRecur = false;
            ns_SqlHelper.SqlHelper sqlCon = null;
            String path = "C:\\VRMSchemas_v1.8.3\\";  //hard coded for time being Wo recurrence fix
            WorkOrder workCAT = null; //ZD 100757
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                List<WorkOrder> WorkOrderList = new List<WorkOrder>();

                node = xd.SelectSingleNode("//SetProviderWorkorderDetails/Type");
                string Type = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetProviderWorkorderDetails/ConfID");
                string ConfID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetProviderWorkorderDetails/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetProviderWorkorderDetails/IsRecur");//Code added fro WO  bug reccurence //FB 2075
                if (node != null)
                {
                    if (node.InnerXml == "1")
                        isRecur = true;
                }

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//SetProviderWorkorderDetails/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                //FB 2027 SetConference - start

                if (orgInfo == null) //FB 2484
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                XmlNodeList workList = xd.SelectNodes("//SetProviderWorkorderDetails/Workorders/Workorder");
                if (workList != null)
                {
                    if (workList.Count > 0)
                    {
                        CConfID conf = new CConfID(ConfID);
                        int cID = conf.ID;
                        int iID = conf.instance;

                        if (iID == 0)
                            iID = 1;

                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", cID));
                        criterionList.Add(Expression.Eq("instanceid", iID));

                        List<vrmConference> conferences = m_IconfDAO.GetByCriteria(criterionList);
                        int tzID = conferences[0].timezone;
                        int adminId = conferences[0].owner;

                        List<ICriterion> criterionListConf = new List<ICriterion>();  //Code added fro WO  bug reccurence
                        criterionListConf.Add(Expression.Eq("confid", conf.ID));
                        if (!isRecur)
                            criterionListConf.Add(Expression.Eq("instanceid", iID));
                        //FB 2075 Starts
                        DateTime serverTime = DateTime.Now;
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);

                        criterionListConf.Add(Expression.Ge("confdate", serverTime));
                        //FB 2075 Ends
                        List<vrmConference> WOconferences = m_IconfDAO.GetByCriteria(criterionListConf);

                        //FB 2817 Starts
                        List<ICriterion> RoomcriterionList = new List<ICriterion>();

                        RoomcriterionList.Add(Expression.Eq("confid", conf.ID));

                        if (!isRecur)
                            RoomcriterionList.Add(Expression.Eq("instanceid", iID));
                        else
                            RoomcriterionList.Add(Expression.Eq("instanceid", 1));

                        RoomcriterionList.Add(Expression.Eq("Extroom", 0));
                        List<vrmConfRoom> rList1 = m_IconfRoom.GetByCriteria(RoomcriterionList);
                        //FB 2817 Starts
                        /*** Code added for WO bug reccurence ***/
                        Int32 j = 0;
                        DateTime dtWO = DateTime.MinValue;
                        DateTime dtConf = DateTime.MinValue;
                        TimeSpan diffResult = dtConf.Subtract(dtWO);
                        /*** Code added for WO bug reccurence ***/

                        foreach (vrmConference WOconference in WOconferences)
                        {
                            foreach (XmlNode innerNode in workList)
                            {
                                WorkOrder work = new WorkOrder();
                                workCAT = new WorkOrder(); //ZD 100757

                                XmlElement innerElement = (XmlElement)innerNode;

                                int wID;
                                string sID = innerElement.GetElementsByTagName("ID")[0].InnerText;
                                if (sID.Length < 1 || sID.Trim().ToLower() == "new")
                                    wID = 0;
                                else
                                {
                                    wID = Int32.Parse(sID);
                                }

                                if (isRecur)
                                    wID = 0;


                                work.ID = wID;
                                if (work.ID == 0)
                                    work.isNew = true;

                                if (wID > 0)//ZD 100757
                                    workCAT = m_InvWorkOrderDAO.GetById(wID);

                                work.orgId = organizationID; //organziation module
                                work.ConfID = cID;
                                work.InstanceID = iID;
                                work.deleted = 0;
                                work.Type = 2;
                                work.Name = innerElement.GetElementsByTagName("Name")[0].InnerText;
                                work.Comment = innerElement.GetElementsByTagName("Comments")[0].InnerText;
                                work.woTimeZone = tzID;
                                //FB 1685
                                //work.AdminID = adminId;
                                string Price = innerElement.GetElementsByTagName("Price")[0].InnerText;
                                work.woTtlCost = float.Parse(Price);
                                string service = innerElement.GetElementsByTagName("CateringService")[0].InnerText;
                                work.deliveryType = Int32.Parse(service);
                                string RoomID = innerElement.GetElementsByTagName("RoomID")[0].InnerText;
                                work.IRoom.roomId = Int32.Parse(RoomID);
                                string CompletedByDate = innerElement.GetElementsByTagName("DeliverByDate")[0].InnerText;
                                string CompletedByTime = innerElement.GetElementsByTagName("DeliverByTime")[0].InnerText;
                                DateTime dateComplete = DateTime.Parse(CompletedByDate + " " + CompletedByTime);
                                if (!timeZone.changeToGMTTime(tzID, ref dateComplete))
                                    throw new myVRMException("Invalid time format for CompletedByDate tag");
                                work.CompletedBy = dateComplete;
                                work.startBy = dateComplete;


                                DateTime dateStart = DateTime.Parse(CompletedByDate + " " + CompletedByTime);
                                if (!timeZone.changeToGMTTime(tzID, ref dateStart))
                                    throw new myVRMException("Invalid time format for CompletedByDate tag");
                                /*** Code added for WO bug reccurence ***/
                                work.startBy = dateStart;
                                TimeSpan WOdur = dateComplete.Subtract(dateStart);

                                if (isRecur && j == 0)
                                {
                                    j = 1;
                                    dtConf = WOconference.SetupTime; //ZD 100433
                                    //dtConf = WOconference.confdate;
                                    dtWO = dateStart;
                                    diffResult = dtConf.Subtract(dtWO);

                                }
                                /*** Code added for WO bug reccurence ***/
                                if (isRecur && work.isNew)
                                {
                                    work.startBy = WOconference.SetupTime.Add(diffResult);
                                    //work.startBy = WOconference.confdate.Add(diffResult);
                                    work.CompletedBy = work.startBy.Add(WOdur);
                                    work.ConfID = WOconference.confid;
                                    work.InstanceID = WOconference.instanceid;
                                }

                                work.Status = 0;
                                work.Notify = 1;
                                work.Reminder = 0;

                                XmlNodeList menuList = innerNode.SelectNodes("MenuList/Menu");
                                int id = 0;

                                //if (wID > 0) //ZD 100757
                                //{
                                //    InvWorkItemDAO woDAO = m_woDAO.GetWorkItemDAO();
                                //    List<ICriterion> deleteItemList = new List<ICriterion>();
                                //    deleteItemList.Add(Expression.Eq("WorkOrderID", wID));
                                //    List<WorkItem> deleteList = woDAO.GetByCriteria(deleteItemList);

                                //    foreach (WorkItem a_wo in deleteList)
                                //        woDAO.Delete(a_wo);
                                //}
                                foreach (XmlNode itemNode in menuList)
                                {
                                    XmlElement itemElement = (XmlElement)itemNode;

                                    WorkItem wi = new WorkItem();
                                    string ItemID = itemElement.GetElementsByTagName("ID")[0].InnerText;
                                    int itemId = Int32.Parse(ItemID);
                                    wi.ItemID = itemId;
                                    // you must insert a category id(use the first one)
                                    InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                                    Menu m = menuDAO.GetById(itemId);
                                    //FB 1685

                                    work.AdminID = m.ICategory.AdminID;
                                    //ZD 100757- START
                                    if (workCAT != null)
                                    {
                                        if (workCAT.woCatAdmin > 0)
                                            work.AdminID = workCAT.woCatAdmin;

                                        work.woCatAdmin = workCAT.woCatAdmin;
                                    }
                                    int AdminId = 0;
                                    if (innerElement.GetElementsByTagName("AssignedAdminID")[0] != null && innerElement.GetElementsByTagName("AssignedAdminID")[0].InnerText != "")
                                    {
                                        int.TryParse(innerElement.GetElementsByTagName("AssignedAdminID")[0].InnerText, out AdminId);
                                        work.woCatAdmin = AdminId;
                                        work.AdminID = AdminId;
                                    }
                                    //ZD 100757- END

                                    work.CatID = m.ICategory.ID;
                                    work.IC.ID = m.ICategory.ID;

                                    id++;
                                    wi.ID = 0;
                                    wi.deleted = 0;

                                    wi.WorkOrderID = work.ID;
                                    wi.IWork = work;

                                    string Quantity = itemElement.GetElementsByTagName("Quantity")[0].InnerText;
                                    wi.quantity = Int32.Parse(Quantity);
                                    wi.serviceId = work.deliveryType;
                                    work.ItemList.Add(wi);

                                }
                                WorkOrderList.Add(work);
                            }

                            //ZD 100757 - Start
                            // DO NOT  check authorization yet 
                            checkWorkOrderList(WorkOrderList);

                            foreach (WorkOrder wk in WorkOrderList)
                            {
                                if (wk.ID > 0)
                                {
                                    sqlCon = new ns_SqlHelper.SqlHelper(path);

                                    sqlCon.OpenConnection();
                                    sqlCon.OpenTransaction();

                                    string qString = "Delete from Inv_WorkItem_D";//Update Inv_WorkItem_D set deleted = 1  ";
                                    if (!isRecur)
                                        qString += " WHERE WorkOrderID =" + wk.ID;
                                    else
                                        qString += " WHERE WorkOrderID in ( Select ID FROM  Inv_WorkOrder_D  WHERE type in (2) and ConfID = " + cID + ")"; //FB 1917

                                    Int32 strExec = sqlCon.ExecuteNonQuery(qString);

                                    if (isRecur)
                                    {
                                        qString = "Delete from Inv_WorkOrder_D";//"Update Inv_WorkOrder_D set deleted = 1"
                                        qString += " WHERE type in (2) and ConfID = " + cID; //FB 1917
                                        strExec = sqlCon.ExecuteNonQuery(qString);
                                    }

                                    sqlCon.CommitTransaction();
                                    sqlCon.CloseConnection();
                                    sqlCon = null;
                                }
                            }
                            //ZD 100757 - End

                            m_InvWorkOrderDAO.SaveOrUpdateList(WorkOrderList);
                            if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                            {
                                if (rList1.Count == 1) //FB 2817
                                {
                                    if (orgInfo.SingleRoomConfMail == 1)
                                        m_woEmail.sendCateringWOEmails(WOconferences, WorkOrderList); //FB 1830
                                }
                                else
                                    m_woEmail.sendCateringWOEmails(WOconferences, WorkOrderList); //FB 1830
                            }

                        }
                    }
                }
                //FB 2027 SetConference - end

                //foreach (WorkOrder wk in WorkOrderList)
                //{
                //    WorkOrder emWork = wk;
                //    if (!GetCateringWorkOrderEmail(ref emWork))
                //    {
                //        myVRMException e = new myVRMException("Cannot create work order email");
                //        m_log.Error("sytemException", e);
                //    }

                //    //FB 1685

                //    if (wk.AdminID != adminId)
                //    {
                //        emWork.toUserID = adminId;

                //        if (!GetCateringWorkOrderEmail(ref emWork))
                //        {
                //            myVRMException e = new myVRMException("Cannot create work order email");
                //            m_log.Error("sytemException", e);
                //        }
                //    }
                //}
                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message); //FB 1881
                obj.outXml = e.FetchErrorMsg();//FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
        }
        #endregion

        #region canDeleteMenu
        /// <summary>
        /// canDeleteMenu
        /// </summary>
        /// <param name="id"></param>
        /// <param name="workList"></param>
        /// <returns></returns>
        private bool canDeleteMenu(int id, ref string workList)
        {
            bool iRet = false;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium = Expression.Eq("ItemID", id);
                criterionList.Add(criterium);
                criterium = Expression.Eq("deleted", 0);
                criterionList.Add(criterium);

                List<WorkItem> result = m_InvWorkItemDAO.GetByCriteria(criterionList, true);

                if (result.Count == 0)
                {
                    iRet = true;
                }
                else
                {
                    Hashtable wl = new Hashtable();
                    foreach (WorkItem wi in result)
                    {
                        if (!wl.ContainsKey(wi.IWork.ID))
                        {
                            wl.Add(wi.IWork.ID, wi.IWork.Name);
                        }
                    }
                    IDictionaryEnumerator iEnum = wl.GetEnumerator();
                    while (iEnum.MoveNext())
                    {
                        if (workList.Length > 0)
                            workList += ", ";
                        workList += (string)iEnum.Value;
                        if (workList.Length > 512)
                        {
                            workList += " ...";
                            break;
                        }
                    }

                }
                return iRet;

            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region menuNameExists
        /// <summary>
        /// menuNameExists
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool menuNameExists(string name)
        {
            bool iRet = true;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium = Expression.Eq("name", name);
                criterionList.Add(criterium);

                InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();

                List<Menu> result = menuDAO.GetByCriteria(criterionList, true);

                if (result.Count == 0)
                {
                    iRet = false;
                }
                return iRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion


        


        //Code added fro WO bug
        //Method Modified for FB 2181,2068 - Start

        #region Check Item Count
        private bool CheckItemCount(Int32 itemID, Int32 count, Int32 catID)
        {
            try
            {
                //ZD 101188 Starts
                WorkOrder work = null;
                vrmConference conf = null;
                DateTime today = DateTime.Now;
                DateTime fromDate = today;
                DateTime toDate = today;
                //ZD 101188 Ends
                int quantity = 0;
                if (itemID > 0)
                {

                    InvWorkItemDAO woDAO = m_woDAO.GetWorkItemDAO();
                    List<ICriterion> deleteItemList = new List<ICriterion>();
                    deleteItemList.Add(Expression.Eq("ItemID", itemID));
                    //deleteItemList.Add(Expression.Gt("quantity", count));
                    deleteItemList.Add(Expression.Eq("deleted", 0));
                    List<WorkItem> deleteList = woDAO.GetByCriteria(deleteItemList);
                    if (deleteList.Count > 0)
                    {
                        //for (int i = 0; i < deleteList.Count; i++) //ZD 101188
                        //{
                        //    quantity += deleteList[i].quantity;
                        //}

                        foreach (WorkItem a_wo in deleteList)
                        {
                            quantity = a_wo.quantity; //ZD 101188
                            work = m_woDAO.m_IWorkOrderDAO.GetById(a_wo.WorkOrderID);

                            if (quantity > count)
                            {
                                if (work.CatID == catID)
                                {
                                    if (work.Status != 1 && work.CompletedBy > DateTime.Now)
                                    {
                                        conf = m_IconfDAO.GetByConfId(work.ConfID, work.InstanceID);
                                        if (conf.status != vrmConfStatus.Deleted || conf.status != vrmConfStatus.Terminated || conf.status != vrmConfStatus.Completed)
                                        {

                                            today = DateTime.Now;
                                            timeZone.changeToGMTTime(sysSettings.TimeZone, ref today);
                                            fromDate = today;
                                            toDate = today;
                                            fromDate = fromDate.AddMinutes(5);
                                            toDate = toDate.AddMinutes(conf.duration);


                                            if (conf.confdate >= fromDate || (conf.confdate <= fromDate && today <= toDate))
                                                return false;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
        #endregion

        //Method Modified for FB 2181,2068 - End

        #region DeleteWorkOrder
        /// <summary>
        /// DeleteWorkOrder
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool DeleteWorkOrderRecurrence(Int32 woID, CConfID ConfMode)
        {
            try
            {

                int ID = woID;

                List<WorkOrder> wkList = new List<WorkOrder>();
                List<WorkOrder> wkEmailList = new List<WorkOrder>();
                if (ID == 0 && ConfMode != null)
                {


                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("ConfID", ConfMode.ID));
                    if (ConfMode.instance > 0)
                        criterionList.Add(Expression.Eq("InstanceID", ConfMode.instance));

                    wkList = m_InvWorkOrderDAO.GetByCriteria(criterionList, true);
                    foreach (WorkOrder w in wkList)
                    {
                        if (w.deleted == 0)
                        {
                            w.deleted = 1;
                            foreach (WorkItem wi in w.ItemList)
                                wi.deleted = 1;

                            wkEmailList.Add(w);
                        }
                    }
                }
                else
                {
                    WorkOrder wk = m_InvWorkOrderDAO.GetById(ID, true);
                    wk.deleted = 1;
                    foreach (WorkItem wi in wk.ItemList)
                        wi.deleted = 1;
                    wkList.Add(wk);
                    wkEmailList.Add(wk);
                }
                m_InvWorkOrderDAO.SaveOrUpdateList(wkList);

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //ZD 101443 Start

        #region FetchUserAssignedInventory
        /// <summary>
        /// FetchUserAssignedInventory
        /// </summary>
        /// <param name="organizationID"></param>
        /// <param name="userid"></param>
        /// <param name="selectedOption"></param>
        /// <param name="InvList"></param>
        public void FetchUserAssignedInventory(int organizationID, int userid, int selectedOption, ref List<InventoryCategory> InvList)
        {
            List<ICriterion> Icrit = new List<ICriterion>();
            try
            {
                Icrit.Add(Expression.Eq("orgId", organizationID));
                Icrit.Add(Expression.Eq("AdminID", userid));
                Icrit.Add(Expression.Eq("Type", selectedOption - 3));
                InvList = m_InvCategoryDAO.GetByCriteria(Icrit, true);

            }
            catch (Exception ex)
            {
                m_log.Error("FetchUserAssignedInventory :" + ex.Message);
            }
        }

        #endregion

        #region UpdateUserAssignedInventory

        public Boolean UpdateUserAssignedInventory(XPathNavigator xNode, String inXml, int assignedUserID, int WOType, int organizationID, ref int ErrorNo)
        {
            List<ICriterion> Icrit = new List<ICriterion>();
            try
            {

                if (xNode != null)
                {
                    List<ICriterion> CriteriaMCU = new List<ICriterion>();
                    List<InventoryCategory> invList = new List<InventoryCategory>();
                    var idlist = (XDocument.Parse(inXml).Descendants("SelectedID").Elements("ID").Select(element => element.Value).ToList());

                    idlist.Remove("");

                    Icrit = new List<ICriterion>();
                    Icrit.Add(Restrictions.In("ID", idlist));
                    invList = m_InvCategoryDAO.GetByCriteria(Icrit, true);
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //ZD 103364
                    if (invList.Count > 0)
                    {
                        if (!checkInventoryAdmin(assignedUserID, WOType))
                        {
                            ErrorNo = 487;
                            return false;
                        }
                        for (int i = 0; i < invList.Count; i++)
                        {
                            for (int x = 0; x < invList[i].RoomList.Count(); x++)
                            {
                                if (orgInfo.isDeptUser == 1) //ZD 103364 start
                                {
                                    if (!checkDepartmentalAuthorization(invList[i].RoomList[x].IRoom.roomId, assignedUserID))
                                    {
                                        ErrorNo = 489;
                                        return false;
                                    }
                                }//ZD 103364 end
                            }
                            invList[i].AdminID = assignedUserID;

                            m_InvCategoryDAO.Update(invList[i]);
                        }
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                m_log.Error("UpdateUserAssignedInventory :" + ex.Message);
                return false;
            }
        }

        #endregion

        #region FetchUserAssignedWorkorder
        /// <summary>
        /// FetchUserAssignedWorkorder
        /// </summary>
        /// <param name="organizationID"></param>
        /// <param name="userid"></param>
        /// <param name="selectedOption"></param>
        /// <param name="InvList"></param>
        public void FetchUserAssignedWorkorder(int organizationID, int userid, int selectedOption, ref List<WorkOrder> WoList)
        {
            List<ICriterion> Icrit = new List<ICriterion>();
            try
            {
                Icrit.Add(Expression.Eq("orgId", organizationID));
                Icrit.Add(Expression.Eq("AdminID", userid));
                Icrit.Add(Expression.Eq("deleted", 0));
                Icrit.Add(Expression.Eq("Type", selectedOption - 6));

                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);

                Icrit.Add(Expression.Ge("startBy", serverTime));
                m_InvWorkOrderDAO.addOrderBy(Order.Desc("CompletedBy"));

                WoList = m_InvWorkOrderDAO.GetByCriteria(Icrit, true);

            }
            catch (Exception ex)
            {
                m_log.Error("FetchUserAssignedInventory :" + ex.Message);
            }
        }

        #endregion

        #region UpdateUserAssignedWorkorder

        public Boolean UpdateUserAssignedWorkorder(XPathNavigator xNode, String inXml, int assignedUserID, int WOTYPE,int organizationID, ref int ErrorNo) //ZD 103364
        {
            List<ICriterion> Icrit = new List<ICriterion>();
            List<vrmConfRoom> rList1 = null;
            List<vrmConference> WOconferences = null;
            List<ICriterion> RoomcriterionList = null;
            List<ICriterion> criterionListConf = null;
            List<WorkOrder> woList = new List<WorkOrder>();
            List<WorkOrder> WorkOrderList = new List<WorkOrder>();
            try
            {

                if (xNode != null)
                {

                    List<ICriterion> CriteriaMCU = new List<ICriterion>();
                    List<WorkOrder> workOrder = new List<WorkOrder>();
                    var idlist = (XDocument.Parse(inXml).Descendants("SelectedID").Elements("ID").Select(element => element.Value).ToList());

                    idlist.Remove("");

                    Icrit = new List<ICriterion>();
                    Icrit.Add(Restrictions.In("ID", idlist));
                    woList = m_InvWorkOrderDAO.GetByCriteria(Icrit, true);

                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //ZD 103364
                    if (woList.Count > 0)
                    {
                        for (int cnt = 0; cnt < woList.Count; cnt++)
                        {
                           
                                if (!checkWOAdmin(assignedUserID, WOTYPE))
                                {
                                    ErrorNo = 491;
                                    return false;
                                }

                                if (orgInfo.isDeptUser == 1)
                                {
                                    if (!checkDepartmentalAuthorization(woList[cnt].IRoom.roomId, assignedUserID))
                                    {
                                        ErrorNo = 492;
                                        return false;
                                    }
                                }
                        }

                        for (int i = 0; i < woList.Count; i++)
                        {
                            WorkOrderList = new List<WorkOrder>();
                            woList[i].AdminID = assignedUserID;

                            m_InvWorkOrderDAO.Update(woList[i]);

                            WorkOrderList.Add(woList[i]);


                            criterionListConf = new List<ICriterion>();
                            criterionListConf.Add(Expression.Eq("confid", woList[i].ConfID));
                            //if (!isRecur)
                            criterionListConf.Add(Expression.Eq("instanceid", woList[i].InstanceID));
                            DateTime serverTime = DateTime.Now;
                            timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                            criterionListConf.Add(Expression.Ge("confdate", serverTime));

                            WOconferences = m_IconfDAO.GetByCriteria(criterionListConf);

                            RoomcriterionList = new List<ICriterion>();

                            RoomcriterionList.Add(Expression.Eq("confid", woList[i].ConfID));

                            //if (!isRecur)
                            RoomcriterionList.Add(Expression.Eq("instanceid", woList[i].InstanceID));
                            //else
                            //  RoomcriterionList.Add(Expression.Eq("instanceid", 1));

                            RoomcriterionList.Add(Expression.Eq("Extroom", 0));
                            rList1 = m_IconfRoom.GetByCriteria(RoomcriterionList);
                            if (orgInfo.SendConfirmationEmail == 0)
                            {
                                if (WOTYPE == 2)
                                {
                                    if (rList1.Count == 1)
                                    {
                                        if (orgInfo.SingleRoomConfMail == 1)
                                        {
                                            if (!m_woEmail.sendCateringWOEmails(WOconferences, WorkOrderList))
                                            {
                                                ErrorNo = 547;
                                                return false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!m_woEmail.sendCateringWOEmails(WOconferences, WorkOrderList))
                                        {
                                            ErrorNo = 547;
                                            return false;
                                        }
                                    }

                                }
                                else
                                {
                                    if (rList1.Count == 1)
                                    {
                                        if (orgInfo.SingleRoomConfMail == 1)
                                        {
                                            if (!m_woEmail.sendAVAndHKWOEmails(WOconferences, WorkOrderList))
                                            {
                                                ErrorNo = 547;
                                                return false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!m_woEmail.sendAVAndHKWOEmails(WOconferences, WorkOrderList))
                                        {
                                            ErrorNo = 547;
                                            return false;
                                        }
                                    }

                                }
                            }

                        }
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                m_log.Error("UpdateUserAssignedInventory :" + ex.Message);
                return false;
            }
        }

        #endregion

        //ZD 101443 End

        //ZD 101835
        #region SearchArchiveWorkorder
        private void SearchArchiveWorkorder(ref List<WorkOrder> Result, List<ICriterion> criterionList)
        {
            try
            {
                string strSearch = "";
                String[] delimitedArr = { ">=" };

                for (Int32 c = 0; c < criterionList.Count; c++)
                {
                    if (criterionList[c].ToString().Contains("startBy"))
                    {
                        String[] str1 = criterionList[c].ToString().Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries);
                        if (str1.Length == 2)
                            strSearch += " and w." + str1[0] + " " + delimitedArr[0] + " '" + str1[1] + "')";
                    }

                    else
                        strSearch += " and " + "w." + criterionList[c].ToString();
                }


                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                sqlCon.OpenConnection();
                sqlCon.OpenTransaction();
                String stmtArchive = "select m.*,w.*,";
                stmtArchive += " isnull(Stuff ((SELECT distinct  N', ' + cast(d.RoomID as varchar(50)) + '|' + d.Name  from loc_room_d d where w.Roomid = d.Roomid ";
                stmtArchive += " FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [RoomsInWO]";
                stmtArchive += " from Archive_Inv_WorkOrder_D w ";
                stmtArchive += " left outer join (select b.Name as itemName, a.ID as itemID,a.WorkOrderID from Archive_Inv_WorkItem_D a, Inv_List_D b where a.ItemID = b.ID)m on w.ID = m.WorkOrderID ";
                stmtArchive += " where 1 = 1 " + strSearch;

                DataSet ds = sqlCon.ExecuteDataSet(stmtArchive);
                sqlCon.CommitTransaction();
                sqlCon.CloseConnection();

                if (ds != null && ds.Tables.Count > 0)
                {
                    for (Int32 c = 0; c < ds.Tables[0].Rows.Count; c++)
                    {
                        WorkOrder wo = new DataLayer.WorkOrder();
                        wo.woSearchType = "A";
                        wo.ID = Int32.Parse(ds.Tables[0].Rows[c]["ID"].ToString());
                        wo.Name = ds.Tables[0].Rows[c]["Name"].ToString();
                        wo.CatID = Int32.Parse(ds.Tables[0].Rows[c]["CatID"].ToString());
                        wo.ConfID = Int32.Parse(ds.Tables[0].Rows[c]["ConfID"].ToString());
                        wo.InstanceID = Int32.Parse(ds.Tables[0].Rows[c]["InstanceID"].ToString());
                        wo.startBy = DateTime.Parse(ds.Tables[0].Rows[c]["startBy"].ToString());
                        wo.CompletedBy = DateTime.Parse(ds.Tables[0].Rows[c]["CompletedBy"].ToString());
                        wo.Status = Int32.Parse(ds.Tables[0].Rows[c]["Status"].ToString());
                        wo.Type = Int32.Parse(ds.Tables[0].Rows[c]["Type"].ToString());
                        wo.strData = ds.Tables[0].Rows[c]["strData"].ToString();
                        wo.woTimeZone = Int32.Parse(ds.Tables[0].Rows[c]["woTimeZone"].ToString());
                        wo.AdminID = Int32.Parse(ds.Tables[0].Rows[c]["AdminID"].ToString());
                        wo.Comment = ds.Tables[0].Rows[c]["Comment"].ToString();
                        wo.serviceCharge = double.Parse(ds.Tables[0].Rows[c]["serviceCharge"].ToString());
                        wo.deliveryType = Int32.Parse(ds.Tables[0].Rows[c]["deliveryType"].ToString());
                        wo.deliveryCost = double.Parse(ds.Tables[0].Rows[c]["deliveryCost"].ToString());
                        wo.deleted = Int32.Parse(ds.Tables[0].Rows[c]["deleted"].ToString());
                        wo.woTtlCost = double.Parse(ds.Tables[0].Rows[c]["woTtlCost"].ToString());
                        wo.woTtlSrvceCost = double.Parse(ds.Tables[0].Rows[c]["woTtlSrvceCost"].ToString());
                        wo.woTtlDlvryCost = double.Parse(ds.Tables[0].Rows[c]["woTtlDlvryCost"].ToString());
                        wo.Notify = Int32.Parse(ds.Tables[0].Rows[c]["Notify"].ToString());
                        wo.Reminder = Int32.Parse(ds.Tables[0].Rows[c]["Reminder"].ToString());

                        wo.IC.ID = Int32.Parse(ds.Tables[0].Rows[c]["itemID"].ToString());
                        wo.IC.Name = ds.Tables[0].Rows[c]["itemName"].ToString();

                        if (ds.Tables[0].Rows[c]["RoomsInWO"].ToString() != "")
                        {
                            String[] strRoom = ds.Tables[0].Rows[c]["RoomsInWO"].ToString().Split('|');
                            wo.IRoom.roomId = Int32.Parse(strRoom[0]);
                            wo.IRoom.Name = strRoom[1];
                        }
                        Result.Add(wo);
                    }
                }
            }
            catch (Exception e)
            {
                //ZD 101835
                if (sqlCon != null)
                {
                    sqlCon.RollBackTransaction();
                    sqlCon.CloseConnection();
                }
                m_log.Error("sytemException", e);
            }
        }
        #endregion
    }
    #endregion
}