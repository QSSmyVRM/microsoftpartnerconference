//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 ZD 100886 End
namespace NS_CONFIG
{
	#region References
	using System;
	using System.Xml;
	using System.Diagnostics;
	using System.IO;
	#endregion 

	class Config 
	{
		public Config ()
		{
			// constructor	
		}

		public bool Initialize (string configPath,ref NS_MESSENGER.ConfigParams configParams,ref string errMsg, String appConfig, String rtcConfig)
		{			
			configParams.localConfigPath = configPath;
            Change_Local_Log_File(appConfig,rtcConfig);
			bool ret = Read_Local_Config_File(ref configParams,ref errMsg,appConfig);


			if (!ret) return (false);

			return(true);
		}


        private bool Read_Local_Config_File(ref NS_MESSENGER.ConfigParams configParams, ref string errMsg, String appConfig)
		{
			// Read from XML config file 
            String time = "";
            Int32 timeinms = 0;
            string vrmMaintserviceLog = "MaintenanceLogs\\VRMMaintServiceLog";
            string vrmMaintserviceconfig = "VRMConfig.xml";
			try 
			{ 
				// Open an XML file 
				XmlDocument xmlConfig = new XmlDocument();
				xmlConfig.Load(configParams.localConfigPath);

				XmlNode node;

				node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/VRM_Config_File_Path");
                if (appConfig != "")
                {
                    node.InnerXml = appConfig + vrmMaintserviceconfig;
                }
				configParams.globalConfigPath = node.InnerXml.Trim();
				
				node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/Log_File_Path");

                if (appConfig != "")
                {
                    node.InnerXml = appConfig + vrmMaintserviceLog;
                }

				configParams.logFilePath = node.InnerXml.Trim();				

				node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/Debug");
				string debug = node.InnerXml.Trim();
				if (debug.Equals("yes") || debug.Equals("Yes") || debug.Equals("YES"))
				{
					configParams.debugEnabled = true;
				}
				
				node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/SiteURL");
				configParams.siteUrl = node.InnerXml.Trim();


                node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/ActivationTimer");
                if (node != null)
                {
                    time = node.InnerXml.Trim();
                    if (time != "")
                    {
                        if(Int32.TryParse(time,out timeinms))
                        {
                            if (timeinms < 8 && timeinms > 0)
                            {
                                configParams.activationTimer = Convert.ToDouble(timeinms * 24 * 60 * 60 * 1000);
                            }

                        }
                    }
 
                }

                //FB 2363
                node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/ReportFilePath");

                if (node != null)
                {
                    configParams.reportFilePath = node.InnerText.Trim();
                }
                //FB 2363
               
                

                xmlConfig.Save(configParams.localConfigPath);

				
				return true;
			}
			catch (Exception e)
			{
				errMsg = "Error reading local config file. Exception Message : " + e.Message;
				return false;
			}			
		}

        private bool Change_Local_Log_File(String appConfig, String rtcConfig)
        {
            // Read from XML config file 
            String comconfig = "";
            string vrmconfig = "VRMConfig.xml";
            string vrmrtcLog = "RTCLogs\\VRMRTCLog";
            string comlog = "VRMCOMLog.log";
            try
            {
                comconfig = appConfig + "COMConfig.xml";
                // Open an XML file 
                XmlDocument xmlConfig = new XmlDocument();
                xmlConfig.Load(rtcConfig);

                XmlNode node;

                node = xmlConfig.SelectSingleNode("//RTCConfig/VRM_Config_File_Path");
                
                if (node != null)
                {
                    node.InnerXml = appConfig + vrmconfig;
                    
                }

                node = xmlConfig.SelectSingleNode("//RTCConfig/Log_File_Path");

                if (node != null)
                {
                    node.InnerXml = appConfig + vrmrtcLog;
                    
                }
                xmlConfig.Save(rtcConfig);

                xmlConfig = new XmlDocument();
                xmlConfig.Load(comconfig);

                node = xmlConfig.SelectSingleNode("//COMConfig/VRMConfig");

                if (node != null)
                {
                    node.InnerXml = appConfig + vrmconfig;
                    
                }
                node = xmlConfig.SelectSingleNode("//COMConfig/COMLog");
                
                if (node != null)
                {
                    node.InnerXml = appConfig + comlog;
                    
                }
                xmlConfig.Save(comconfig);

                return true;
            }
            catch (Exception e)
            {
                
                return false;
            }
        }
		

	}
}	