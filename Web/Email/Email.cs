//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* <summary>
 * FILE : Email.cs
 * DESCRIPTION : All SMTP commands are stored in this file. 
 * AUTHOR : Kapil M
 * </summary>
 */
namespace NS_EMAIL
{
	#region References
	using System;	
	using System.Threading;
	using System.Collections;
	using Dart.PowerTCP.Mail;
    using System.IO;
    using System.Net; //ICAL Fix
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using System.Linq;//FB 2410
    using System.Data;//ALLDEV-524
    //using System.Net.Mime;
    //using System.Net.Mail;
	#endregion 

	class Email
	{
		private NS_LOGGER.Log logger;
		private NS_MESSENGER.ConfigParams configParams;
		internal string errMsg = null;
        private int mailLogo = 0; //FB 1658
        private string mailLogoPath = "";
        private string mailLogoName = ""; //FB 1658

        //Added for FB 1710 Start
        private string footerImageName = "";
        private bool hasFooterImage = false;
        //bool hasSecBadge = false;   //FB 2136
        //Added for FB 1710 End
        FileStream newFile;//FB 2410

		internal Email(NS_MESSENGER.ConfigParams config)
		{
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;										
			logger = new NS_LOGGER.Log(configParams);
		}

        private void smtp1_Trace(object sender, Dart.PowerTCP.Mail.SegmentEventArgs e)
		{
			// Smtp traffic trace 
            logger.Trace(e.Segment.ToString());
		}
		
		// Create MailMessage object to send thru SMTP
        private bool CreateEmail(NS_MESSENGER.Email emailObj, string footerMessage, ref MessageStream msg, ref DataSet ds)//ALLDEV-524
        {
            try
            {
                // REPLY-TO
                if (emailObj.From != null)
                {
                    try
                    {
                        msg.Header.Add(HeaderLabelType.ReplyTo, emailObj.From.Trim());
                    }
                    catch (Exception)
                    {
                        logger.Trace("No FROM address...");
                    }
                }

                // TO
                if (emailObj.To.Length > 3)
                {
                    msg.To.Add(new MailAddress(emailObj.To.Trim()));
                }
                //ZD 104852 starts
                //else
                //{
                if (emailObj.CC != null)
                {
                    try
                    {
                        string[] emaillst = emailObj.CC.ToString().Split(';');
                        for (int e = 0; e < emaillst.Length; e++)
                            msg.To.Add(new MailAddress(emaillst[e].Trim()));
                    }
                    catch (Exception)
                    {
                        logger.Trace("No CC address...");
                    }
                }
                if (emailObj.BCC != null)
                {
                    try
                    {
                        string[] emaillst = emailObj.CC.ToString().Split(';');
                        for (int e = 0; e < emaillst.Length; e++)
                            msg.To.Add(new MailAddress(emaillst[e].Trim()));
                    }
                    catch (Exception)
                    {
                        logger.Trace("No BCC address...");
                    }
                }
                //}
                //ZD 104852 ends

                // Subject
                msg.Subject = emailObj.Subject;

                //FB 1658 - Code Changes - start

                // Append the footer text to message
                if (mailLogo == 0 && !hasFooterImage) //FB 1710 & FB 2136 //&& !hasSecBadge
                {
                    if (footerMessage != null)
                    {
                        if (footerMessage.Trim().Length > 2)
                        {
                            emailObj.Body += "<HR><BR>" + footerMessage.Trim();
                        }
                    }
                    msg.Parts.Add(new MessagePartStream(emailObj.Body, ContentType.TextHtml, ContentEncoding.Base64, "UTF-8"));//ZD 100878
                }
                //FB 1658 - Code Changes - end

                // Attachment
                if (emailObj.Attachment != null)
                {
                    if (emailObj.Attachment.Length > 3)
                    {
                        // attachment has valid text 
                        logger.Trace("Email has an attachment : " + emailObj.Attachment.Trim());
                        //FB 2410 starts
                        MimeAttachmentStream attach = null;
                        if (emailObj.isCalender == 1)
                        {
                            byte[] content = System.Text.Encoding.UTF8.GetBytes(emailObj.Attachment.Trim()); //FB 2208
                            MemoryStream m = new MemoryStream(content);
                            attach = new MimeAttachmentStream(m, "ConferenceMeeting.ics", ContentType.MultipartRelated, ContentEncoding.Base64, "");
                        }
                        else
                        {  
							newFile = new FileStream(emailObj.Attachment, FileMode.Open);
                            attach = new MimeAttachmentStream(newFile);                                           
                        }
                        //Add it to the message.
                        //FB 2410 Ends
                        msg.Attachments.Add(attach);

                        logger.Trace("Email attachment Finished Successfully");//FB 2410
                        attach = null;
                    }
                }

                //ALLDEV-524 - Start
                logger.Trace("Entering Room Email Attachments...");

                MimeAttachmentStream AttachStream = null;
                byte[] RoomMailDocument = null;
                string FileName = "", FileExtn = "", fName = "";

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (ds.Tables[0].Rows[i]["FileName"] != null)
                            {
                                FileName = ds.Tables[0].Rows[i]["FileName"].ToString().Trim();
                            }
                            if (ds.Tables[0].Rows[i]["FileExtn"] != null)
                            {
                                FileExtn = ds.Tables[0].Rows[i]["FileExtn"].ToString().Trim();
                            }
                            if (ds.Tables[0].Rows[i]["FileData"] != null)
                            {
                                if (ds.Tables[0].Rows[i]["FileData"].ToString() != "")
                                    RoomMailDocument = (byte[])ds.Tables[0].Rows[i]["FileData"];
                            }
                            if (RoomMailDocument != null)
                            {
                                fName = "Printer Instructions_" + FileName + "." + FileExtn;
                                logger.Trace("fName" + fName);
                                MemoryStream m = new MemoryStream(RoomMailDocument);

                                switch (FileExtn.ToLower())
                                {
                                    case "doc":
                                    case "docx":
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.ApplicationMSWord, ContentEncoding.Base64, "");
                                        break;
                                    case "pdf":
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.ApplicationPdf, ContentEncoding.Base64, "");
                                        break;
                                    case "txt":
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.TextPlain, ContentEncoding.Base64, "");
                                        break;
                                    case "xlsx":
                                    case "xls":
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.ApplicationVndmsExcell, ContentEncoding.Base64, "");
                                        break;
                                    case "jpg":
                                    case "jpeg":
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.ImageJpeg, ContentEncoding.Base64, "");
                                        break;
                                    case "gif":                                    
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.ImageGif, ContentEncoding.Base64, "");                                        
                                        break;                                    
                                    case "zip":
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.ApplicationZip, ContentEncoding.Base64, "");
                                        break;
                                    case "xml":
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.TextXml, ContentEncoding.Base64, "");
                                        break;
                                    case "html":
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.TextHtml, ContentEncoding.Base64, "");
                                        break;
                                    default:
                                        AttachStream = new MimeAttachmentStream(m, fName, ContentType.AutoDetect, ContentEncoding.Base64, "");
                                        break;
                                }
                                msg.Attachments.Add(AttachStream);

                                logger.Trace("Room Email attachment Finished Successfully");
                                AttachStream = null;
                            }
                        }
                    }
                }
                //ALLDEV-524 - End

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

		private bool SendEmail (NS_MESSENGER.Smtp smtpServer,ref MessageStream msg) 
		{	
			try 
			{
                // Send mail 
                Smtp smtp = new Smtp();

				// Specify the server					
				smtp.Server = smtpServer.ServerAddress;
				smtp.ServerPort = smtpServer.PortNumber;
                smtp.Timeout = smtpServer.ServerTimeout*1000; //ZD 100317 converted secs to millisecs 
                logger.Trace("Server Timeout:" + smtp.Timeout * 1000);//ZD 100317
                if (smtpServer.Login == null)
                {
                    smtpServer.Login = "";
                }
                if (smtpServer.Password == null)
                {
                    smtpServer.Password = "";
                }

                smtp.Username = smtpServer.Login;
                smtp.Password = smtpServer.Password;
            

				System.Collections.IEnumerator myEnumerator = msg.To.GetEnumerator();
				while ( myEnumerator.MoveNext() )
				{
					MailAddress ea = (MailAddress)myEnumerator.Current;					
					logger.Trace("Email being sent to : " + ea.Address);
				}
                
				// Message Headers
                logger.Trace("Adding message headers...");
                //msg.Header.Add(HeaderLabelType.From,"\"" + smtpServer.DisplayName +"\"<"+ smtpServer.CompanyMailAddress + ">");
                //msg.Header.Add(HeaderLabelType.ContentType,"text/html;"); ////charset = \"iso-8859-1\"
                msg.Header.Add(HeaderLabelType.Encoding, "quoted-printable"); //FB 2957 Starts
                //msg.Header.Add(HeaderLabelType.Subject,msg.Subject);				

                msg.Header.Add(HeaderLabelType.ContentType, "multipart/mixed;");
                msg.Header.Add(HeaderLabelType.Date, DateTime.UtcNow.ToString("R")); // current date time of the server				
				//FB 2957 Ends
		
		        // From
                logger.Trace("Adding From address...");
                msg.From = new MailAddress("\"" + smtpServer.DisplayName +"\"<" + smtpServer.CompanyMailAddress + ">");
                                
                
				// Send the message                
                logger.Trace("Trying to send email...");
				SmtpResult sr = smtp.Send(msg);
					
				// Check the information about the message sent
				logger.Trace ("Success ! Bytes of data sent = " + sr.SentLength);
				//foreach(MailAddress ma in sr.Recipients)
				//	logger.Trace("Mail sent to: " + ma.Address);

				// Check if connection is active
				if(smtp.Connected)
				{
					try
					{
						// Try to close gracefully
						smtp.Close();
					}
					catch(Exception ex)
					{
						this.errMsg = ex.Message;
						logger.Exception (100,ex.Message);
						// If an error occurs here, just abruptly close.
						smtp.Dispose();
					}
				}

				return true;
			} 
			catch(Exception e)
			{	
				this.errMsg = e.Message;
				logger.Exception(100,e.Message + " --- " + e.StackTrace);				
				return false;
			}			
		}

        // New method added forICAL Fix
        private bool SendCalendar(NS_MESSENGER.Smtp smtpServer,ref NS_MESSENGER.Email emailobj) 
        {
            try
            {
                DateTime startcaldelay = DateTime.Now;

                //while (DateTime.Now.Subtract(startcaldelay).Seconds < 15)
                //{
                //    System.Threading.Thread.Sleep(1);
                //}
               

                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(smtpServer.ServerAddress, smtpServer.PortNumber);
                smtpClient.Credentials = new NetworkCredential(smtpServer.Login, smtpServer.Password);
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                System.Net.Mail.MailMessage app = new System.Net.Mail.MailMessage();

                System.Net.Mime.ContentType calType = new System.Net.Mime.ContentType("text/calendar");
                calType.Parameters.Add("name", "meeting.ics");
                calType.Parameters.Add("method", "REQUEST"); 

                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(emailobj.Body, calType);
                htmlView.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable; //ZD 100878
                app.AlternateViews.Add(htmlView);
                app.From = new System.Net.Mail.MailAddress(emailobj.From);
                app.Subject = emailobj.Subject;

                foreach (String toAdd in emailobj.To.Split(';'))
                {
                    if(toAdd != "")
                        app.To.Add(toAdd);
                }
                logger.Trace("Trying to send email...");
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtpClient.Send(app);
                

                // Check the information about the message sent
                logger.Trace("Success ! Bytes of data sent");
                //foreach(MailAddress ma in sr.Recipients)
                //	logger.Trace("Mail sent to: " + ma.Address);

                smtpClient = null;

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message + " --- " + e.StackTrace);
                return false;
            }
        }

        //FB 1658 - Embedded Image Starts...
        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);
                
                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                logger.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //FB 1658 -  Embedded Image Ends...


        
		internal bool SendTestEmail(NS_MESSENGER.Smtp smtpServer,NS_MESSENGER.Email email)
		{
            try
            {
                //FB 1830 - start
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                
                string siteURL = email.Body;
                string Displayname = smtpServer.DisplayName;//FB 2189
                string emailBody = "", emailSubject = "";
                db.FetchEmailString(11, ref emailBody, ref emailSubject, 2); //Test Mail Server Connection
                emailBody = emailBody.Replace("{36}", siteURL);
                emailBody = emailBody.Replace("{2}", Displayname);//FB 2189
                db.FetchTechInfo(11, ref emailBody);//FB 2189
                email.Body = emailBody;
                email.Subject = emailSubject;
                //FB 1830 - end
                
                //FB 2189 Starts
                // SMTP server object				
                NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();

                mailLogo = 0;
                mailLogoName = "Test_11mail_logo.gif";

                if (File.Exists((configParams.mailLogoPath + mailLogoName)))
                    mailLogo = 1;

                string tempStr = "";
                if (mailLogo > 0)
                    tempStr = "<img id=\"MImgId2\" src=\"" + mailLogoName + "\" alt />";

                email.Body = email.Body.Replace("{1}", tempStr); 


                MessageStream msg = null;
                MemoryStream msgStream = null;

                if (mailLogo > 0)
                {

                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim()); //FB 2208
                    msgStream = new MemoryStream(byteArray);
                    msg = new MessageStream(msgStream, configParams.mailLogoPath);
                }
                else
                {
                    msg = new MessageStream();
                }
                //FB 2189 Ends

                //Create the email
                DataSet ds = new DataSet();//ALLDEV-524
                bool ret = CreateEmail(email, smtpServer.FooterMessage, ref msg, ref ds);//ALLDEV-524
                if (!ret)
                {
                    logger.Trace("Test email create failed.");
                    return false;
                }

                // Send the email 
                ret = false;
                ret = SendEmail(smtpServer, ref msg);
                if (!ret)
                {
                    logger.Trace("Test email send failed.");
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Trace(e.Message);
            }
            return true;
        }

        //Method added for FB 1758
        #region Test Company Email
        /// <summary>
        /// Test Company Email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="footerMessage"></param>
        /// <returns></returns>
        internal bool SendTestCompanyMail(NS_MESSENGER.Email email, String footerMessage)
        {
            try
            {
                //Get the SMTP Server info
                logger.Trace("Initializing db object...");
                logger.Trace(configParams.databaseLogin);
                logger.Trace(configParams.databaseName);
                logger.Trace(configParams.databasePwd);
                logger.Trace(configParams.databaseServer);

                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                // SMTP server object				
                NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();

                logger.Trace("Fetching smtp server info...");
                bool ret = db.FetchSmtpServerInfo(ref smtpServerInfo);
                if (!ret)
                {
                    logger.Trace("Error retreiving SMTP server info from db.");
                    return false;
                }
                if (smtpServerInfo.ServerAddress.Length < 1)
                {
                    logger.Exception(100, "Mail server not valid");
                    return false;
                }
                //FB 1830 - start
                string emailBody="", emailSubject="";

                db.FetchEmailString(email.orgID, ref emailBody, ref emailSubject,3); //Test Mail Connection content
                emailBody = emailBody.Replace("{7}", DateTime.Now.ToString("F"));//ALLBUGS-157
                db.FetchTechInfo(email.orgID, ref emailBody);
                email.Body = emailBody;
                email.Subject = emailSubject;
                //FB 1830 - end

                mailLogo = 0;
                mailLogoName = "Test_" + email.orgID + "mail_logo.gif";
                
                if (File.Exists((configParams.mailLogoPath + mailLogoName)))
                    mailLogo = 1;

                string tempStr = "";
                if (mailLogo > 0)
                    tempStr = "<img id=\"MImgId2\" src=\"" + mailLogoName + "\" alt />";
                   
                email.Body = email.Body.Replace("{1}", tempStr); //FB 1830

                hasFooterImage = false;
                footerImageName = "Test_" + email.orgID + "_footerimage.gif";

                if (footerMessage.IndexOf("<img") >= 0)
                    hasFooterImage = true;

                smtpServerInfo.FooterMessage = footerMessage;
                
                MessageStream msg = null;
                MemoryStream msgStream = null;
                                    
                if (mailLogo > 0)
                {
                    // Append the footer text to message
                    if (smtpServerInfo.FooterMessage != null)
                    {
                        if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                        {
                            email.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();
                        }
                    }

                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim()); //FB 2208
                    msgStream = new MemoryStream(byteArray);
                    msg = new MessageStream(msgStream, configParams.mailLogoPath);
                }
                else
                {
                    if (hasFooterImage) 
                    {
                        // Append the footer text to message
                        if (smtpServerInfo.FooterMessage != null)
                        {
                            if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                            {
                                email.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();
                            }
                        }

                        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim()); //FB 2208
                        msgStream = new MemoryStream(byteArray);
                        msg = new MessageStream(msgStream, configParams.mailLogoPath);
                    }
                    else
                    {
                        msg = new MessageStream();
                    }
                }
                //Create the email
                DataSet ds = new DataSet();//ALLDEV-524
                ret = CreateEmail(email, smtpServerInfo.FooterMessage, ref msg, ref ds);//ALLDEV-524
                if (!ret)
                {
                    logger.Trace("Test company email create failed.");
                    return false;
                }

                // Send the email 
                ret = false;
                ret = SendEmail(smtpServerInfo, ref msg);
                if (!ret)
                {
                    logger.Trace("Test company email send failed.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace(ex.Message);
            }
            return true;
        }
        #endregion

        internal bool SendAllEmails()
		{
			try
			{
                mailLogo = 0; // FB 1658
                byte[] mailLogoImage = null;
                byte[] footerImage = null; //Added for FB 1710
                int tempOrgid = 0; //FB 1758
                String footerMessage = ""; //FB 1758
				// fetch & send vrm emails 

				//Get the SMTP Server info
				logger.Trace ("Initializing db object...");
				logger.Trace (configParams.databaseLogin);
				logger.Trace (configParams.databaseName);
				logger.Trace (configParams.databasePwd);
				logger.Trace (configParams.databaseServer);
		
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			
				// SMTP server object				
				NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();
				
				logger.Trace ("Fetching smtp server info...");
				bool ret = db.FetchSmtpServerInfo(ref smtpServerInfo);
				if (!ret) 
				{
					logger.Trace("Error retreiving SMTP server info from db."); 
					return false;
				}

				if (smtpServerInfo.ServerAddress.Length < 1)
				{
					logger.Exception (100,"Mail server not valid");
					return false;
				}

				//email queue declaration
				Queue newEmails = new Queue();

				// fetch new emails
				ret =false;
				ret = db.FetchNewEmails(ref newEmails);
				if (!ret) return false;
                
				// Check if queue contains emails
				while (newEmails.Count > 0 )
				{
					// Retreive email details from the queue
					NS_MESSENGER.Email emailObj = new NS_MESSENGER.Email();						
					emailObj = (NS_MESSENGER.Email) newEmails.Dequeue();

                    try
					{
                        //ALLDEV-524 - Start
                        logger.Trace("Entering Fetch Room Document...");

                        DataSet ds = new DataSet();
                        bool RoomAttach = false;
                        string[] uArr = null;
                        string[] delimiter = { "�" }; //Embedded Image Alt 241
                        string RoomIDs = "";

                        uArr = emailObj.Body.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                        if (uArr != null)
                        {
                            if (uArr.Length > 1)
                            {
                                RoomIDs = uArr[1];
                                emailObj.Body = uArr[0] + uArr[2];
                            }
                        }


                        logger.Trace("RoomIDs...: " + RoomIDs);

                        emailObj.Body = emailObj.Body.Replace("�", "");

                        if (RoomIDs != "")
                        {
                            RoomAttach = db.FetchRoomDocument(emailObj.orgID, RoomIDs, ref ds);
                            logger.Trace("Dataset Count: " + ds.Tables.Count);
                        }
                        //ALLDEV-524 - End

						// Check if email has already been tried more than 100 times  
						//if (emailObj.RetryCount > 100) 
                        if (emailObj.RetryCount >= smtpServerInfo.RetryCount)  //FB 1784 //FB 2552
						{
							NS_MESSENGER.Email failureEmail = new NS_MESSENGER.Email();
                            
							if (emailObj.From != null)
							{
								failureEmail.To = emailObj.From;
								//failureEmail.Subject = "[VRM Alert] Email delivery failed after 100 attempts.";
                                failureEmail.Subject = "[VRM Alert] Email delivery failed after "+ smtpServerInfo.RetryCount.ToString()+" attempts."; // FB 1784 //FB 2552
								failureEmail.Body = " <html> <body> This is a system notification for email delivery failure. A copy of email is attached below:";
								failureEmail.Body += "<BR> --------------" ;
								failureEmail.Body += "<BR> From: " + emailObj.From;
								failureEmail.Body += "<BR> To: " + emailObj.To;
								failureEmail.Body += "<BR> Subject: " + emailObj.Subject;
								failureEmail.Body += "<BR> Message: " + emailObj.Body;
                                failureEmail.orgID = emailObj.orgID;    //FB 1710
								//failureEmail.Body += "<BR> --------------" ;
							
								ret = false;
								ret = db.InsertEmailInQueue (failureEmail);
								if (ret)
								{
									db.DeleteEmail(emailObj);
									continue;
								}
							}
						}

                        logger.Trace("Entering create email...");

                        //Create the email
                        ret = false;
                        bool sent = false; //ICAL Fix

                        //Fetch Email Logo Status - FB 1658 code starts
                                                
                        mailLogoName = "Org_" + emailObj.orgID + "mail_logo.gif";
                        
                        if (tempOrgid != emailObj.orgID)
                        {
                            if (File.Exists((configParams.mailLogoPath + mailLogoName)))
                                File.Delete((configParams.mailLogoPath + mailLogoName));
                            mailLogo = 0;
                            mailLogoImage = null;
                            footerImage = null;
                            footerMessage = "";
                            hasFooterImage = false;
                            //hasSecBadge = false; //FB 2136
                        }

                        if (!File.Exists((configParams.mailLogoPath + mailLogoName)))
                        {
                            mailLogo = db.FetchMailLogo(emailObj.orgID, ref mailLogoImage);
                            if (mailLogoImage != null)
                                WriteToFile((configParams.mailLogoPath + mailLogoName), ref mailLogoImage);
                        }
                        
                        emailObj.Body = emailObj.Body.Replace("�", "").Replace("�", "");
                        //Fetch Email Logo Status - FB 1658 code ends
                        
                        //Code Added for FB 1710 Footer Image Start

                        if (footerMessage == "") //FB 1758 - Modified
                        {
                            footerMessage = db.FetchFooterMessage(emailObj.orgID, ref footerImage);

                            if (footerMessage.IndexOf("<img") >= 0)
                                hasFooterImage = true;

                            footerImageName = "Org_" + emailObj.orgID + "_footerimage.gif";
                            
                            if (File.Exists(configParams.mailLogoPath + footerImageName))
                                File.Delete((configParams.mailLogoPath + footerImageName));
                            
                            if (footerImage != null)
                                WriteToFile((configParams.mailLogoPath + footerImageName), ref footerImage);
                        }
                        smtpServerInfo.FooterMessage = footerMessage; 
                        //FB 1710 Footer Image End

                        tempOrgid = emailObj.orgID; //FB 1758

                        //FB 2136 - Start
                        /*hasSecBadge = false;
                        List<Uri> links = new List<Uri>();
                        string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                        string regexAltSrc = @"<img[^>]*?alt\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                        MatchCollection matchesImgSrc = Regex.Matches(emailObj.Body, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        MatchCollection matchesAltSrc = Regex.Matches(emailObj.Body, regexAltSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        Int32 i = 0;
                        string secBadgeName = "";
                        foreach (Match m in matchesImgSrc)
                        {
                            secBadgeName = m.Groups[1].Value;

                            if (secBadgeName.IndexOf("SecImg_") >= 0)
                                break;

                            i = i + 1;
                        }
                        String srcFile = "";
                        if (secBadgeName != "" && secBadgeName.IndexOf("SecImg_") >= 0)
                        {
                            string secImageCont = "";
                            secImageCont = matchesAltSrc[0].Groups[1].Value;
                            srcFile = configParams.mailLogoPath + secBadgeName;
                            byte[] secImageArr = null;
                            
                            if(secImageCont != "")
                                secImageArr = Convert.FromBase64String(secImageCont);

                            if (File.Exists(srcFile))
                                File.Delete(srcFile);

                            WriteToFile(srcFile, ref secImageArr);
                            hasSecBadge = true;
                        }*/
                        //FB 2136 - End

                        //MessageStream msg = new MessageStream(); //FB 1658 Commented
                        MessageStream msg = null; //FB 1658
                        MemoryStream msgStream = null; //FB 1658
                        if (emailObj.isCalender == 1) //ICAL Fix
                        {
                            msg = new MessageStream(); //FB 1658
                            logger.Trace("Entering send email...");
                            // Send the email
                            sent = SendCalendar(smtpServerInfo, ref emailObj);

                        }
                        else
                        {
                            //FB 1658 code starts...
                            if (mailLogo > 0)
                            {
                                // Append the footer text to message
                                if (smtpServerInfo.FooterMessage != null)
                                {
                                    if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                                    {
                                        emailObj.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();
                                    }
                                }

                                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(emailObj.Body.Trim());//FB 2208 //ZD 100288
                                msgStream = new MemoryStream(byteArray);
                                msg = new MessageStream(msgStream, configParams.mailLogoPath);
                            }
                            else
                            {
                                // Added for FB 1710 Start
                                if (hasFooterImage) 
                                {
                                    // Append the footer text to message
                                    if (smtpServerInfo.FooterMessage != null)
                                    {
                                        if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                                        {
                                            emailObj.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();
                                        }
                                    }

                                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(emailObj.Body.Trim()); //FB 2208 //ZD 100288
                                    msgStream = new MemoryStream(byteArray);
                                    msg = new MessageStream(msgStream, configParams.mailLogoPath);
                                } // Added for FB 1710 End
                                else
                                {
                                    msg = new MessageStream();//ZD 102068
                                }
                                /*else
                                {
                                    //FB 2136 start
                                    if(hasSecBadge)
                                    {
                                        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(emailObj.Body.Trim()); //FB 2208 //ZD 100288
                                        msgStream = new MemoryStream(byteArray);
                                        msg = new MessageStream(msgStream, configParams.mailLogoPath);
                                        logger.Trace("hasSecImage: " + hasSecBadge);
                                    } //FB 2136 end
                                    else
                                    {
                                        msg = new MessageStream();
                                    }
                                }*/
                            }

                            
                            ret = CreateEmail(emailObj, smtpServerInfo.FooterMessage, ref msg, ref ds);//ALLDEV-524
                            if (!ret) continue;                            

                            logger.Trace("Entering send email...");
                            // Send the email 

                            sent = SendEmail(smtpServerInfo, ref msg); //ICAL Fix
                          
                        }
						if (sent)
						{
                            logger.Trace("Enter into the Delete Email");
                            if(!string.IsNullOrEmpty(emailObj.Attachment))
                                newFile.Close();//FB 2410                            
							db.DeleteEmail (emailObj);                            
                            String destFile = emailObj.Attachment;
                            logger.Trace(destFile);                            
                            if (File.Exists(destFile))
                                File.Delete(destFile);
                    	}
						else
						{
							db.UpdateUnsentEmail(emailObj);
						}
					}
					catch (Exception e)
					{
						logger.Exception(100,e.Message);
					}
				}
			
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				logger.Exception (100,e.Message);
				return false;

			}
		}
    }
}