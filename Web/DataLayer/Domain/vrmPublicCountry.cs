﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

using log4net;


namespace myVRM.DataLayer
{
	public class vrmPublicCountry
	{
		#region Private Internal Members
		private int m_countryID;
		private string m_countryName;
		#endregion

		#region Public Properties
		public int CountryID
		{
			get { return m_countryID; }
			set { m_countryID = value; }
		}
		public string CountryName
		{
			get { return m_countryName; }
			set { m_countryName = value; }
		}
		
		#endregion

		public vrmPublicCountry()
		{
		}
	}
}
