/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Text; //FB 1830 DeleteEmailLang
//FB 2481 Start
using System.IO;
using System.Web;
using System.Collections.Generic;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
using System.Collections;
//FB 2481 End

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_MyVRM
{
    public partial class UserProfile : System.Web.UI.Page
    {
        #region Private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        MyVRMNet.Util utilObj; //FB 2721
        MyVRMNet.LoginManagement loginMgmt; // ZD 100172

        //code added for ticker -- Start
        private String tickerDisplay = "";
        private String tickerDisplay1 = "";
        //code added for ticker -- End
        #endregion

        #region Protected Members
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstTimeZone;
        protected System.Web.UI.WebControls.DropDownList lstDefaultGroup;
        protected System.Web.UI.WebControls.DropDownList lstSearchTemplate;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;
        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DropDownList lstBridgeType;//ZD 104821
        protected System.Web.UI.WebControls.DropDownList lstSystemLocation;//ZD 104821
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstMCUAddressType;
        protected System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstProtocol;
        protected System.Web.UI.WebControls.ListBox lstDepartment;
        protected System.Web.UI.WebControls.DropDownList lstAddressBook;
        protected System.Web.UI.WebControls.DropDownList lstAdmin;
        protected System.Web.UI.WebControls.DropDownList lstEmailNotification;
        protected System.Web.UI.WebControls.DropDownList lstIsOutsideNetwork;
        protected System.Web.UI.WebControls.DropDownList lstSendBoth;
        protected System.Web.UI.WebControls.DropDownList lstUserRole;
        protected System.Web.UI.WebControls.DropDownList lstNewUserRole; //ZD 100164 - Changes made by Inncrewin Ram Manohar
        protected System.Web.UI.WebControls.DropDownList lstConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstEncryption;
        protected System.Web.UI.WebControls.DropDownList lstRoles; //ZD 103686
        
        //protected System.Web.UI.WebControls.TextBox txtUserID; //ZD 100263
        protected System.Web.UI.WebControls.TextBox txtUserFirstName;
        protected System.Web.UI.WebControls.TextBox txtUserLastName;
        protected System.Web.UI.WebControls.TextBox txtUserEmail;
        protected System.Web.UI.WebControls.TextBox txtUserEmail2;
        protected System.Web.UI.WebControls.TextBox txtUserLogin;
        protected System.Web.UI.WebControls.TextBox txtPassword1;
        protected System.Web.UI.WebControls.TextBox txtPassword2;
        protected System.Web.UI.WebControls.TextBox txtAccountExpiry;
        protected System.Web.UI.WebControls.TextBox txtTotalTime;//ZD 102043
        protected System.Web.UI.WebControls.Label lblTimeRemaining;//ZD 102043
        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected System.Web.UI.WebControls.TextBox txtEndpointID;
        protected System.Web.UI.WebControls.TextBox txtLocation;
        protected System.Web.UI.WebControls.TextBox txtEndpointName;
        protected System.Web.UI.WebControls.TextBox txtEPPassword1;
        protected System.Web.UI.WebControls.TextBox txtEPPassword2;
        protected System.Web.UI.WebControls.TextBox txtWebAccessURL;
        protected System.Web.UI.WebControls.TextBox txtAssociateMCUAddress;
        protected System.Web.UI.WebControls.TextBox txtLotusLoginName;
        protected System.Web.UI.WebControls.TextBox txtLotusPassword;
        protected System.Web.UI.WebControls.TextBox txtLotusDBPath, txtSysLoc; //ZD 104821

        protected System.Web.UI.HtmlControls.HtmlInputHidden txtLevel;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtPassword1_1;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtPassword1_2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLocation;

        protected System.Web.UI.WebControls.Label lblLicencesRemaining, SysLocation; //ZD 104821
        protected System.Web.UI.WebControls.Label lblTotalUsers;

        protected System.Web.UI.WebControls.RequiredFieldValidator reqPassword1;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqBridges;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqLastName;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqFirstName;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqUserEmail;

        protected System.Web.UI.WebControls.RegularExpressionValidator regPassword1;
        protected System.Web.UI.WebControls.RegularExpressionValidator regPassword2;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail1_1;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail1_2;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail2_1;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail2_2;

        protected System.Web.UI.WebControls.CompareValidator cmpValPassword1;
        protected System.Web.UI.WebControls.CompareValidator cmpValPassword2;
        protected System.Web.UI.WebControls.CustomValidator cusVal1;

        protected System.Web.UI.HtmlControls.HtmlTableRow EnableTR, EbleWO, EbleCatWO;//Code changed for MOJ Phase 2 QA Bug //102231
        //Added for License modification START
        protected System.Web.UI.HtmlControls.HtmlTableRow trUser;
        protected System.Web.UI.WebControls.DropDownList DrpExc;
        protected System.Web.UI.WebControls.DropDownList DrpDom;
        protected System.Web.UI.WebControls.DropDownList DrpMob; //FB 1979
        //Added for License modification END

        //FB 1830 Starts
        protected System.Web.UI.WebControls.DropDownList lstLanguage;
        protected System.Web.UI.WebControls.TextBox txtEmailLang;
        protected System.Web.UI.WebControls.Button btnDefine;
        //FB 1830 Ends

        /* *** Code added for FB 1425 QA Bug -Start *** */
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD1, lblDOmino; //ZD 10231
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD2;
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD3;
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD4;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdntzone;
        /* *** Code added for FB 1425 QA Bug -End *** */

        //Code added by Offshore for FB Issue 1073 -- Start
        protected String format = "", tformat = "";//ZD 100664
        protected System.Web.UI.WebControls.DropDownList DrpDateFormat;
        protected System.Web.UI.HtmlControls.HtmlInputHidden oldFormat;
        //Code added by Offshore for FB Issue 1073 -- End

        //Code added for AV Switch FB 1426,1429,1425 -- Start
        protected Boolean isAVEnabled = true;
        protected String enableAV = "0";
        protected System.Web.UI.WebControls.DropDownList DrpEnableAV;
        protected System.Web.UI.WebControls.Label AVLBL;


        protected Boolean isParEnabled = true;
        protected String enablePar = "0";
        protected System.Web.UI.WebControls.DropDownList DrpEnvPar;

        //ZD 101122
        protected Boolean isAVWOEnabled = true;
        protected Boolean isCateringWOEnabled = true;
        protected Boolean isFacilityWOEnabled = true;

        protected String enableAVWO = "0";
        protected String enableCatWO = "0";
        protected String enableFacWO = "0";

        protected System.Web.UI.WebControls.Label LblAVWO;
        protected System.Web.UI.WebControls.Label LblCateringWO;
        protected System.Web.UI.WebControls.Label LblFacilityWO;

        protected System.Web.UI.WebControls.DropDownList DrpAVWO; 
        protected System.Web.UI.WebControls.DropDownList DrpCateringWO;
        protected System.Web.UI.WebControls.DropDownList DrpFacilityWO;

        protected System.Web.UI.WebControls.Label ParLBL;

        protected System.Web.UI.HtmlControls.HtmlTableRow AVParams;

        protected String timeFormat = "";
        protected String tzDisplay = "";
        protected System.Web.UI.WebControls.DropDownList lstTimeFormat;
        protected System.Web.UI.WebControls.DropDownList lstTimeZoneDisplay;

        //Code added for AV Switch -- End

        protected System.Web.UI.HtmlControls.HtmlTableRow trLblAV; //Code Added For MOJ - Phase2

        //Code added for Ticker -- Start
        protected System.Web.UI.WebControls.DropDownList drpTickerStatus;
        protected System.Web.UI.WebControls.DropDownList drpTickerPosition;
        protected System.Web.UI.WebControls.DropDownList drpTickerSpeed;
        protected System.Web.UI.WebControls.DropDownList drpTickerDisplay;
        protected System.Web.UI.WebControls.TextBox txtTickerBknd;
        protected System.Web.UI.WebControls.TextBox txtFeedLink;
        protected System.Web.UI.WebControls.DropDownList drpTickerStatus1;
        protected System.Web.UI.WebControls.DropDownList drpTickerPosition1;
        protected System.Web.UI.WebControls.DropDownList drpTickerSpeed1;
        protected System.Web.UI.WebControls.DropDownList drpTickerDisplay1;
        protected System.Web.UI.WebControls.TextBox txtTickerBknd1;
        protected System.Web.UI.WebControls.TextBox txtFeedLink1;
        protected System.Web.UI.HtmlControls.HtmlTableCell feedLink;
        protected System.Web.UI.HtmlControls.HtmlTableCell feedLink1;
        //Ticker issues START
        //protected System.Web.UI.WebControls.RequiredFieldValidator reqtxtfeed;
        //protected System.Web.UI.WebControls.RequiredFieldValidator reqtxtfeed1;
        //protected System.Web.UI.WebControls.RequiredFieldValidator reqTxtTickerBknd1;
        //public System.Web.UI.WebControls.RequiredFieldValidator reqTxtTickerBknd;
        //Ticker issues End
        //Code added fro Ticker -- End

        /**** code added for room search  ****/

        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;
        protected System.Web.UI.HtmlControls.HtmlSelect RoomList;
        protected System.Web.UI.WebControls.TextBox txtExchangeID; //Cisco Telepresence fix
        protected System.Web.UI.HtmlControls.HtmlTableCell TDprefLocation;
        protected System.Web.UI.HtmlControls.HtmlAnchor roomclick;
        protected System.Web.UI.HtmlControls.HtmlImage roomimage;
        protected System.Web.UI.WebControls.TextBox txtApiportno;//API Port...
        //Added for FB 1642-Audio add on - Starts 
        protected System.Web.UI.WebControls.CheckBox chkAudioAddon;
        protected System.Web.UI.WebControls.TextBox txtWorkPhone;
        protected System.Web.UI.WebControls.TextBox txtCellPhone;
        protected System.Web.UI.WebControls.TextBox txtConfCode;
        protected System.Web.UI.WebControls.TextBox txtLeaderPin, txtPartyCode; //ZD 101446
        //Added for FB 1642-Audio add on - End 

        protected System.Web.UI.WebControls.CheckBox ChkBlockEmails;//FB 1860
        protected System.Web.UI.WebControls.Button BtnBlockEmails;//FB 1860
        protected System.Web.UI.WebControls.Label LblBlockEmails;//FB 1860

        protected System.Web.UI.HtmlControls.HtmlTableRow tdOtherSettings; //FB 1985
        protected System.Web.UI.WebControls.Button btnlogin;

        protected System.Web.UI.WebControls.DropDownList DrpPIMNotifications;//FB 2141
        //FB 2227 Start
        protected System.Web.UI.WebControls.TextBox txtIntvideonum;
        protected System.Web.UI.WebControls.TextBox txtExtvideonum;
        //FB 2227 End
        
        //FB 2268 start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHelpReq;
        protected System.Web.UI.WebControls.ListBox lstHelpReqEmail;
        protected System.Web.UI.WebControls.TextBox txtHelpReqEmail;
        protected System.Web.UI.WebControls.TextBox txtHelpReqPhone;
        protected System.Web.UI.WebControls.TextBox txtRFIDValue;//FB 2724
        protected System.Web.UI.WebControls.Button btnAddHelpReq;
        protected System.Web.UI.WebControls.Button btnnewSubmit; // FB 2025
        //FB 2268 start
        protected System.Web.UI.HtmlControls.HtmlGenericControl spanpassword;//FB 2339
        protected String enablePass = "0";//FB 2339
        protected System.Web.UI.WebControls.DropDownList drpSurveyEmail;//FB 2348

        protected System.Web.UI.WebControls.CheckBox chkVNOC;//FB 2608
        protected System.Web.UI.WebControls.Label Label1;//FB 2670

        //FB 2481 Start
        //protected AjaxControlToolkit.ModalPopupExtender PrivateVMRPopup; // ZD 102535
        protected DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor PrivateVMR;
       //FB 2481 End
		protected System.Web.UI.WebControls.ImageButton delEmailLang; //FB 2502
        //FB 2599 Starts
        //FB 2262
        protected System.Web.UI.WebControls.TextBox txtVidyoURL;
        protected System.Web.UI.WebControls.TextBox txtPin;
        protected System.Web.UI.WebControls.TextBox txtConfirmPin;
        protected System.Web.UI.WebControls.TextBox txtExtension;
        protected System.Web.UI.HtmlControls.HtmlTableRow trpin;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMeetlink;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdExten;
        protected int Cloud = 0;//FB 2262
        //FB 2599 End
        //FB 2392-Whygo Starts
        protected System.Web.UI.HtmlControls.HtmlInputText txtParentReseller;
        protected System.Web.UI.HtmlControls.HtmlInputText txtCorpUser;
        protected System.Web.UI.WebControls.DropDownList lstRegion;
        protected System.Web.UI.HtmlControls.HtmlInputText txtESUserID;
        protected System.Web.UI.HtmlControls.HtmlTable whygouserSettings;
        protected System.Web.UI.HtmlControls.HtmlTable whygousersettings1;//FB 2671
        //FB 2392-Whygo End
        //protected System.Web.UI.WebControls.CheckBox chkSecured; //FB 2595
        //protected System.Web.UI.WebControls.Label LblSecured;//FB 2595
        protected System.Web.UI.HtmlControls.HtmlTableCell tdcancel;//FB 2565
        protected System.Web.UI.HtmlControls.HtmlTableCell tdnewsubmit1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdreset;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdsubmit;
        protected int EnableZulu = 0;//FB 2588
		protected System.Web.UI.HtmlControls.HtmlTableCell tdScheduleMin; //FB 2659
        protected int enableCloudInstallation = 0;//FB 2659
		//FB 2670
        //protected System.Web.UI.WebControls.Button btnPrivateVMRSubmit;
        //protected System.Web.UI.HtmlControls.HtmlButton btnPrivateVMRSubmit; //ZD 100420 // ZD 102535
        protected System.Web.UI.WebControls.Button btnSubmit;

        // FB 2693 Starts
        //protected System.Web.UI.HtmlControls.HtmlTableRow trBJ;//ZD 104021
        protected System.Web.UI.HtmlControls.HtmlTableRow trJB;
        protected System.Web.UI.HtmlControls.HtmlTableRow trLync;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVidtel;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVidyo;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPcConf;
        //protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkBJ;//ZD 104021
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkJB;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkLync;
        //protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkVidtel; //ZD 102004
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkVidyo;
        protected System.Web.UI.HtmlControls.HtmlTable tblPcConf;
        //protected System.Web.UI.HtmlControls.HtmlTextArea txtAreaBlueJeans;//ZD 104021
       
        protected System.Web.UI.HtmlControls.HtmlTextArea txtAreaJabber;
        protected System.Web.UI.HtmlControls.HtmlTextArea txtAreaLync;
        protected System.Web.UI.HtmlControls.HtmlTextArea txtAreaVidtel;
        protected System.Web.UI.HtmlControls.HtmlTextArea txtAreaVidyo;

        protected System.Web.UI.HtmlControls.HtmlInputText txtSkypeBJ;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDialinIP;
        protected System.Web.UI.HtmlControls.HtmlInputText txtMeetingVC;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTollBJ;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTollFreeBJ;
        protected System.Web.UI.HtmlControls.HtmlInputText txtMeetingPC;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDetail;

        protected System.Web.UI.HtmlControls.HtmlInputText txtSkypeVidtel;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDialinSIP;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDialinH323;
        protected System.Web.UI.HtmlControls.HtmlInputText txtPinVc;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTollVidtel;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTollFreeVidtel;
        protected System.Web.UI.HtmlControls.HtmlInputText txtPinPc;

        protected System.Web.UI.HtmlControls.HtmlInputText txtSkypeJB;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDialinIPJB;
        protected System.Web.UI.HtmlControls.HtmlInputText txtMeetingVCJB;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTollJB;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTollFreeJB;
        protected System.Web.UI.HtmlControls.HtmlInputText txtMeetingPCJB;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDetailJB;

        protected System.Web.UI.HtmlControls.HtmlInputText txtSkypeLync;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDialinIPLync;
        protected System.Web.UI.HtmlControls.HtmlInputText txtMeetingVCLync;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTollLync;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTollFreeLync;
        protected System.Web.UI.HtmlControls.HtmlInputText txtMeetingPCLync;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDetailLync;
        protected int PCUserLimit = 0;
        // FB 2693 Ends
        protected System.Web.UI.HtmlControls.HtmlInputText txtBrokerNum; //FB 3301
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPasschange; //FB 3054
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEPPWD; //FB 3054
        protected string ModifiedUserID = ""; //ZD 100263
        protected System.Web.UI.HtmlControls.HtmlTableCell tdUsrRole;//ZD 100263
        protected System.Web.UI.HtmlControls.HtmlTableCell tdNewUsrRole; //ZD 100164 - Changes made by Inncrewin Ram Manohar
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDrpDownUsrRole;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdNewDrpDownUsrRole; //ZD 100164 - Changes made by Inncrewin Ram Manohar
		protected System.Web.UI.WebControls.Button btnReset; //ZD 100263
		//ZD 100164 START
        protected System.Web.UI.HtmlControls.HtmlTableRow trPreferredLocation;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDateFormat;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableMobile;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEmailLanguage;
        protected System.Web.UI.HtmlControls.HtmlTableRow trLobbyManagement;
        protected System.Web.UI.HtmlControls.HtmlTableRow trHelpRequestorPhone;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSurveyEmail;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVNOCSelection;
        protected System.Web.UI.HtmlControls.HtmlTableRow trwhygouserSettings;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPCConferencing;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSendBoth;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWork;
        protected System.Web.UI.HtmlControls.HtmlTableRow trLDAPLogin;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAccountExpiration;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDefaultGroup;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlstDefaultGroup;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSecondaryEmail;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdtxtUserEmail;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdPreferredAddressBook;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlstAddressBook;
        protected int enableAdvancedUserOption = 0;
        protected int RoleID = 0;
        //ZD 100164 END
        
        protected System.Web.UI.WebControls.RegularExpressionValidator regUserLogin; // ZD 100263 
        //ZD 100164 12/18/2013 inncrewin
        protected System.Web.UI.HtmlControls.HtmlTableCell tdNewEmail_lbl;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdNewEmail_txt;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMailinfo;
        //ZD 100164 12/18/2013 inncrewin
        protected System.Web.UI.HtmlControls.HtmlTableRow trStaticID; //ZD 100755 Inncrewin Starts
        protected System.Web.UI.HtmlControls.HtmlTableCell tdClear1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdClear2;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdClear3;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdClear4;
        protected System.Web.UI.WebControls.CheckBox chkEnblStaticId;
        protected System.Web.UI.WebControls.TextBox txtStaticBox;
        //ZD 100755 Inncrewin Ends
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTimeRm; //ZD 100263
        protected System.Web.UI.WebControls.Label LblMob; //ZD 100263
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLblPIM; //ZD 100263
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDrpPIM; //ZD 100263
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLblMob;//ZD 100263
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDrpMob; //ZD 100263.
        //protected System.Web.UI.HtmlControls.HtmlTableRow tdPC; //ZD 100263
        protected System.Web.UI.WebControls.DropDownList drpPageList; // ZD 100172 
        protected System.Web.UI.HtmlControls.HtmlTableRow trLandingPage;

		//ZD 100221 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow tblWebExConf;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox ChkWebexConf;
        protected System.Web.UI.WebControls.TextBox txtWebExUserName;
        protected System.Web.UI.WebControls.TextBox txtWebExPassword;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnWebEX;
        protected System.Web.UI.HtmlControls.HtmlTable tblWebEx;
        int WebExUserLimit = 0;
        //ZD 100221 End
        bool hasReports; //ZD 100873
        protected System.Web.UI.WebControls.DataGrid dgChangedHistory;//ZD 100664
        protected System.Web.UI.HtmlControls.HtmlTableRow trHistory; //ZD 100664

        protected System.Web.UI.WebControls.DropDownList drpadditionaloption;//ZD 101093
        protected System.Web.UI.WebControls.Label Lbladdopt;//ZD 101093
        //ZD 103550
        protected System.Web.UI.HtmlControls.HtmlTableRow tblBJNConf;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox ChkBJNConf;
        protected System.Web.UI.WebControls.TextBox txtBJNUserName;
        protected System.Web.UI.HtmlControls.HtmlTable tblBlueJeansLogin;
        protected System.Web.UI.WebControls.TextBox txtUserDomain;//ZD 104850
        #endregion

        public UserProfile()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            utilObj = new MyVRMNet.Util(); //FB 2721
            loginMgmt = new MyVRMNet.LoginManagement(); // ZD 100172
            //
            // TODO: Add constructor logic here
            //
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            // ZD 100263 Starts
            string confChkArg = string.Empty;
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                //obj.AccessandURLConformityCheck("ManageUserProfile.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());

                if (Request.QueryString["t"] != null)
                    confChkArg = "?t=" + Request.QueryString["t"].ToString();

                obj.AccessConformityCheck("ManageUserProfile.aspx" + confChkArg);

                if (Session["roomCascadingControl"] != null && Session["roomCascadingControl"].ToString().Equals("0"))
                    regUserLogin.Enabled = false;

                // ZD 100263 Ends

                //ZD 102266
                txtPassword1.Attributes.Add("value", txtPassword1.Text);
                txtPassword2.Attributes.Add("value", txtPassword2.Text);
                
                //FB 2025 //FB 2565 Start
                if (Request.QueryString["t"] == null)
                {
                    tdreset.Attributes.Add("align", "right");
                    tdreset.Attributes.Add("style", "padding-right:20px");
                    tdcancel.Attributes.Add("style", "display:none;");
                    tdnewsubmit1.Attributes.Add("style", "display:none;");
                    tdsubmit.Attributes.Add("align", "left");
                    tdsubmit.Attributes.Add("style", "width:50%");
                }
                // Fb 2025 //FB 2565 End
                //PSU Fix
                if (Application["CosignEnable"] == null)
                    Application["CosignEnable"] = "0";

                //FB 1985 - Start
                if (Application["Client"].ToString().ToUpper() == "DISNEY")  //FB 2611
                    tdOtherSettings.Attributes.Add("style", "display:none");
                //FB 1985 - End

                if (Session["UsrCrossAccess"] != null && Session["organizationID"] != null) //FB 1598
                {
                    if (Session["UsrCrossAccess"].ToString().Equals("1") && !Session["organizationID"].ToString().Equals("11"))
                    {
                        lstDefaultGroup.Enabled = true;
                        lstSearchTemplate.Enabled = true;
                        lstBridges.Enabled = true;
                        //roomimage.Attributes.Add("style", "display:block");//FB 1598
                        roomimage.Visible = true; //FB 1598

                    }
                }
                //FB 2339 Start
                if (Session["EnablePasswordRule"] != null)
                {
                    if (Session["EnablePasswordRule"].ToString() != "")
                        enablePass = Session["EnablePasswordRule"].ToString();
                    if (enablePass == "1")
                    spanpassword.Attributes.Add("style", "display:block");
                    else
                    {
                        spanpassword.Attributes.Add("style", "display:none");
                        spanpassword.Parent.Visible = false;//ZD 100164 - Changes made by Inncrewin Ram Manohar
                    }
                }
                //FB 2339 End
                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt"); //ZD 100664

                //Code added by Offshore for FB Issue 1073 -- End

                //Code added for AV Switch  FB 1425,1426,1429-- Start
                if (Session["enableAV"] != null)
                {
                    if (Session["enableAV"].ToString() != "")
                        enableAV = Session["enableAV"].ToString();
                }

                if (Session["enableParticipants"] != null)
                {
                    if (Session["enableParticipants"].ToString() != "")
                        enablePar = Session["enableParticipants"].ToString();
                }

                //ZD 101122 - Start
                if (Session["enableAVWorkOrder"] != null)
                {
                    if (Session["enableAVWorkOrder"].ToString() != "")
                        enableAVWO = Session["enableAVWorkOrder"].ToString();
                }

                if (Session["enableCateringWO"] != null)
                {
                    if (Session["enableCateringWO"].ToString() != "")
                        enableCatWO = Session["enableCateringWO"].ToString();
                }
                if (Session["enableFacilityWO"] != null)
                {
                    if (Session["enableFacilityWO"].ToString() != "")
                        enableFacWO = Session["enableFacilityWO"].ToString();
                }
                //ZD 101122 - End
                if (Session["timeFormat"] != null)
                {
                    if (Session["timeFormat"].ToString() != "")
                        timeFormat = Session["timeFormat"].ToString();
                }
                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        tzDisplay = Session["timeZoneDisplay"].ToString();
                }
              
                //code added for ticker --- Start

                if (Session["tickerDisplay"] != null)
                {
                    if (Session["tickerDisplay"].ToString() != "")
                        tickerDisplay = Session["tickerDisplay"].ToString();
                }

                if (Session["tickerDisplay1"] != null)
                {
                    if (Session["tickerDisplay1"].ToString() != "")
                        tickerDisplay1 = Session["tickerDisplay1"].ToString();
                }

                if (drpTickerDisplay.SelectedValue.Trim() == "1")
                {
                    txtFeedLink.Attributes.Add("style", "display:block");
                    feedLink.Attributes.Add("style", "display:block");
                }
                else
                {

                    txtFeedLink.Attributes.Add("style", "display:none");
                    feedLink.Attributes.Add("style", "display:none");
                }

                if (drpTickerDisplay1.SelectedValue.Trim() == "1")
                {
                    txtFeedLink1.Attributes.Add("style", "display:block");
                    feedLink1.Attributes.Add("style", "display:block");

                }
                else
                {
                    txtFeedLink1.Attributes.Add("style", "display:none");
                    feedLink1.Attributes.Add("style", "display:none");

                }
                //code added for ticker --- End
                //FB 2693 Starts

                string[] mary = Session["sMenuMask"].ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');
                int topMenu = Convert.ToInt32(ccary[1]);
                int adminMenu = Convert.ToInt32(mmary[10].Split('*')[1]); // Convert.ToInt32(mmary[1].Split('*')[1]); ZD 101233 103095

                if (Convert.ToBoolean(topMenu & 16))
                {
                    hasReports = Convert.ToBoolean(adminMenu & 4);
                }

                if (Session["PCUserLimit"] != null)
                {
                    int.TryParse(Session["PCUserLimit"].ToString(), out PCUserLimit);
                    chkPcConf.Disabled = true;
                }

                //ZD 100873 start

                if (Session["admin"] != null)
                {
                    if (Session["admin"].ToString().Equals("2") && (hasReports == true))
                    {
                        trHistory.Attributes.Add("Style", "visibility:visible;"); //ZD 100664
                        trPCConferencing.Visible = true;
                        if (PCUserLimit > 0)
                            chkPcConf.Disabled = false;
                        else
                            trPCConferencing.Visible = false; //ZD 100263
                    }
                    else
                    {
                        trHistory.Attributes.Add("Style", "visibility:hidden;"); //ZD 100664
                        trPCConferencing.Visible = false;
                        chkPcConf.Checked = false;
                    }
                }
                //ZD 100873 End

                //ZD 104021
                //if (Session["EnableBlueJeans"] != null)
                //{
                //    if (Session["EnableBlueJeans"].ToString().Equals("0"))
                //    {
                //        trBJ.Attributes.Add("style", "display:none");
                //    }
                //}
                if (Session["EnableJabber"] != null)
                {
                    if (Session["EnableJabber"].ToString().Equals("0"))
                    {
                        trJB.Attributes.Add("style", "display:none");
                    }
                }
                if (Session["EnableLync"] != null)
                {
                    if (Session["EnableLync"].ToString().Equals("0"))
                    {
                        trLync.Attributes.Add("style", "display:none");
                    }
                }
                if (Session["EnableVidtel"] != null)
                {
                    if (Session["EnableVidtel"].ToString().Equals("0"))
                    {
                        trVidtel.Attributes.Add("style", "display:none");
                    }
                }
                if (Session["EnableCloud"] != null)
                {
                    if (Session["EnableCloud"].ToString().Equals("0"))
                    {
                        trVidyo.Attributes.Add("style", "display:none");
                    }
                }
                //FB 2693 Ends
                if (Session["admin"] != null)
                {
                    if (Session["admin"].ToString().Equals("0"))
                    {
                        isAVEnabled = false;
                        isParEnabled = false;
                        isAVWOEnabled = false;
                        isCateringWOEnabled = false;
                        isFacilityWOEnabled = false;

                        //FB 1860
                        LblBlockEmails.Attributes.Add("style", "display:none");
                        ChkBlockEmails.Attributes.Add("style", "display:none");
                        BtnBlockEmails.Attributes.Add("style", "display:none");
                        //FB 1860

                        chkVNOC.Enabled = false;//FB 2608
                        ChkWebexConf.Disabled = true; //ZD 101015 
                        
                    }
                    //FB 2097 start
                    if (Session["admin"].ToString().Trim() != "2")
                    {
                        DrpMob.Enabled = false;
                        DrpPIMNotifications.Enabled = false;
                        DrpExc.Enabled = false;
                        DrpDom.Enabled = false;
                    }
                    //FB 2097 end
                }
                //FB 2595 Start
                //if (Session["SecureSwitch"] != null)
                //{
                //    if (Session["SecureSwitch"].ToString() == "1")
                //    {
                //        LblSecured.Visible = true;
                //        chkSecured.Visible = true;
                //    }
                //    else
                //    {
                //        LblSecured.Visible = false;
                //        chkSecured.Visible = false;
                //    }
                //}
                //if (Session["admin"] != null)
                //{
                //    if (Session["admin"].ToString().Trim() == "2")
                //        chkSecured.Enabled = true;
                //    else
                //        chkSecured.Enabled = false;
                //}
                //FB 2595 End
                //ZD 103686
                if (!isAVEnabled)
                {
                    //AVLBL.Attributes.Add("style", "display:none");
                    //DrpEnableAV.Attributes.Add("style", "display:none");
                    DrpEnableAV.Enabled = false;
                }

                if (!isParEnabled)
                {
                    //ParLBL.Attributes.Add("style", "display:none");
                    //DrpEnvPar.Attributes.Add("style", "display:none");
                    DrpEnvPar.Enabled = false;
                }

                //ZD 101122 - Start
                if (!isAVWOEnabled)
                {
                    //LblAVWO.Attributes.Add("style", "display:none");
                    //DrpAVWO.Attributes.Add("style", "display:none");
                    DrpAVWO.Enabled = false;
                }

                if (!isCateringWOEnabled)
                {
                    //LblCateringWO.Attributes.Add("style", "display:none");
                    //DrpCateringWO.Attributes.Add("style", "display:none");
                    DrpCateringWO.Enabled = false;
                }

                if (!isFacilityWOEnabled)
                {
                    //LblFacilityWO.Attributes.Add("style", "display:none");
                    //DrpFacilityWO.Attributes.Add("style", "display:none");
                    DrpFacilityWO.Enabled = false;
                }
                //ZD 101122 - End
               

                if (Application["Client"].ToString().ToUpper().Equals("BCS") || Application["Client"].ToString().ToUpper().Equals("MOJ"))
                    AVParams.Attributes.Add("style", "display:none");

                //Code Modified For MOJ Phase2 - Start
                if (Application["Client"].ToString().ToUpper().Equals("MOJ"))
                {
                    trLblAV.Attributes.Add("style", "display:none");
                }
                //Code Modified For MOJ Phase2 - End  

                
                
                //FB 2594 - Starts
                whygouserSettings.Visible = false;
                whygousersettings1.Visible = false;//FB 2671
                if (Session["EnablePublicRooms"] != null)
                {
                    if (Session["EnablePublicRooms"].ToString() == "1")
                    {
                        whygouserSettings.Visible = true;
                        whygousersettings1.Visible = true;
                    }
                        
                }
                //FB 2594 - End
                //ZD 100221 Starts
                tblWebEx.Visible = false;
                if (Session["WebexUserLimit"] != null)
                    int.TryParse(Session["WebexUserLimit"].ToString(), out WebExUserLimit);

                if (WebExUserLimit > 0)
                    tblWebEx.Visible = true;
                //ZD 100221 Ends

				//FB 2588 - Start
                if (Session["EnableZulu"] != null)
                {
                    if (Session["EnableZulu"].ToString() == "1")
                    {
                        lstTimeZone.SelectedValue = "33";
                        lstTimeFormat.SelectedValue = "2";
                        lstTimeZoneDisplay.SelectedValue = "0";
                        lstTimeFormat.Enabled = false;
                        lstTimeZone.Enabled = false;
                        lstTimeZoneDisplay.Enabled = false;
                        lstTimeFormat.Visible = false; //ZD 100263
                        TzTD1.Visible = false; //ZD 100263
                        TzTD2.Visible = false; //ZD 100263
                        TzTD3.Visible = false;//ZD 100263
                        TzTD4.Visible = false; //ZD 100263
                    }
                    else
                    {
                        if (lstTimeFormat.Items.FindByValue("2") != null)
                            lstTimeFormat.Items.Remove(lstTimeFormat.Items.FindByValue("2"));
                        lstTimeFormat.Enabled = true;
                        lstTimeZone.Enabled = true;
                        lstTimeZoneDisplay.Enabled = true;
                    }
                }
                //FB 2588 End

                //FB 2659 ZD 100164 START
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString(), out enableCloudInstallation);
                if (Session["EnableAdvancedUserOption"] != null)
                    int.TryParse(Session["EnableAdvancedUserOption"].ToString(), out enableAdvancedUserOption);
                if ((enableCloudInstallation == 1 && enableAdvancedUserOption == 1) && (Session["UsrCrossAccess"].ToString() != "1" || Session["admin"].ToString() == "3" || Session["UsrCrossAccess"].ToString() == "1"))
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "cellchange", "<script>$('#trtzone').append($('#trUser').contents());</script>", false); //ZD 102231
                    TDBAdvancedUserProfile();
                }
                if (enableCloudInstallation == 1)
                {
                    tdScheduleMin.Visible = false;
					//ZD 102043 Starts
					lblTimeRemaining.Visible = false;
                    txtTotalTime.Visible = false;
               		//ZD 102043 End              
				}
                //ZD 100164 END

                if (!IsPostBack)
                {
                    //ZD 103550
                    if (Session["EnableBlueJeans"] != null && Session["EnableBlueJeans"].ToString() == "1")
                        tblBlueJeansLogin.Visible = true;
                    else
                        tblBlueJeansLogin.Visible = false;

                    ChkBJNConf.Checked = false;
                    tblBJNConf.Style.Add("display", "none");

                    //Response.Write(Application["ssoMode"].ToString().ToUpper().Equals("NO"));
                    //ZD 100263
                    //txtUserID.Text = Session["userID"].ToString();
                    //ZD 100221_17Feb Starts
                    ChkWebexConf.Checked = false;
                    tblWebExConf.Style.Add("display", "none");
                    txtWebExPassword.Text = "";
                    //ZD 100221_17Feb Ends
                    Session.Remove("ModifiedUserID");
                    Session.Add("ModifiedUserID", Session["userID"].ToString());
                    if (Request.QueryString["t"] != null)
                        if (Request.QueryString["t"].ToString().Equals("1"))
                            Session["ModifiedUserID"] = Session["UserToEdit"].ToString();
                            //txtUserID.Text = Session["UserToEdit"].ToString();
                    BindDataUser();
                }
                
               
                //Ticker changes - Start
                //ZD 100263
                //if (txtUserID.Text.Trim().ToLower().Equals("new"))
                if (Session["ModifiedUserID"].ToString().Trim().ToLower().Equals("new"))
                {
                    trHistory.Attributes.Add("Style", "visibility:hidden;"); //ZD 100664
                    if (drpTickerDisplay.SelectedValue == "0")
                    {
                        txtFeedLink.Attributes.Add("style", "display:none");
                        feedLink.Attributes.Add("style", "display:none");
                        feedLink.Attributes.Add("style", "display:none");
                        //ZD 100288 Starts
                        btnDefine.Enabled = false; 
                        btnlogin.Enabled = false;
                        delEmailLang.Enabled = false;
                        btnDefine.Attributes.Add("Class", "btndisable");
                        btnlogin.Attributes.Add("Class", "btndisable");
                        btnDefine.Attributes.Add("style", "width:200px");
                        btnlogin.Attributes.Add("style", "width:200px");
                       
                        //ZD 100288 Ends

                        Session.Remove("UsrEmailLangID"); //ZD 101578
                    }
                    if (drpTickerDisplay1.SelectedValue == "0")
                    {
                        txtFeedLink1.Attributes.Add("style", "display:none");
                        feedLink1.Attributes.Add("style", "display:none");

                    }
                    //ZD 102231 Starts
                    if (enableCloudInstallation == 1)
                    {
                        DrpExc.ClearSelection();
                        DrpExc.SelectedValue = "1";
                    }
                    //ZD 102231 Ends
                   
                }
                //ZD 100580 START
                lstLanguage.Attributes.Add("onchange", "fnHandleChangeLang();");
                delEmailLang.Visible = false;
                if (Session["UsrEmailLangID"] != null)
                {
                    if (Session["UsrEmailLangID"].ToString() != "")
                    {
                        if (Session["UsrEmailLangID"].ToString() == "0")
                            delEmailLang.Visible = false;
                        else
                            delEmailLang.Visible = true;
                    }
                }
                //ZD 100580 END
                //Ticker changes - End

                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        //Code added by Offshore for FB Issue 1073 -- Start
                        if (Request.QueryString["dt"] != null)
                        {
                            if (Request.QueryString["dt"].ToString().Equals("1"))
                            {
                                errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
                                errLabel.Visible = true;
                            }
                            //Code added by Offshore for FB Issue 1073 -- End
                            else
                            {

                                errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                                errLabel.Visible = true;
                            }
                        }
                    }
                /* *** Code added for FB 1425 QA Bug -Start *** */

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    EnableTR.Attributes.Add("style", "display:none");//Code changed for MOJ Phase 2 QA Bug
                    TzTD1.Attributes.Add("style", "display:none");
                    TzTD2.Attributes.Add("style", "display:none");
                    TzTD3.Attributes.Add("style", "display:none");
                    TzTD4.Attributes.Add("style", "display:none");

                    if (hdntzone.Value == "")
                    {
                        lstTimeZone.SelectedValue = "31";
                        lstTimeZone.SelectedValue = Session["systemTimeZoneID"].ToString();

                        //String timezne = obj.GetSystemTimeZone(Application["COM_ConfigPath"].ToString());
                        String timezne = obj.GetSystemTimeZone(Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                        if (timezne != "")
                        {
                            lstTimeZone.ClearSelection();
                            lstTimeZone.SelectedValue = timezne;
                        }
                    }
                }

                /* *** Code added for FB 1425 QA Bug -End *** */
                //Added for FB 1428 Start
                if (Application["Client"].ToString().ToUpper() == "MOJ")
                {
                    lstUserRole.Items[2].Text = "Hearing Administrator";

                    if (lstUserRole.Items.FindByValue("4") != null)
                        lstUserRole.Items.Remove(lstUserRole.Items.FindByValue("4"));
                    if (lstUserRole.Items.FindByValue("5") != null)
                        lstUserRole.Items.Remove(lstUserRole.Items.FindByValue("5"));
                    if (lstUserRole.Items.FindByValue("6") != null)
                        lstUserRole.Items.Remove(lstUserRole.Items.FindByValue("6"));

                    if (lstAdmin.Items.FindByValue("4") != null)
                        lstAdmin.Items.Remove(lstAdmin.Items.FindByValue("4"));
                    if (lstAdmin.Items.FindByValue("5") != null)
                        lstAdmin.Items.Remove(lstAdmin.Items.FindByValue("5"));
                    if (lstAdmin.Items.FindByValue("6") != null)
                        lstAdmin.Items.Remove(lstAdmin.Items.FindByValue("6"));

                }
              
                //Added for FB 1428 End

                if (Session["UsrCrossAccess"] != null)
                {
                    if (Session["UsrCrossAccess"].ToString() != "1" || Session["organizationID"].ToString() != "11")
                    {
                        //ZD 100288
                        if (lstUserRole.Items.FindByText(obj.GetTranslatedText("Site Administrator")) != null)
                        {
                            for (int i = 0; i < lstAdmin.Items.Count; i++)
                            {
                                if (lstAdmin.Items[i].Value == lstUserRole.Items.FindByText(obj.GetTranslatedText("Site Administrator")).Value)
                                {
                                    lstAdmin.Items.Remove(lstAdmin.Items[i]);
                                    break;
                                }
                            }

                            lstUserRole.Items.Remove(lstUserRole.Items.FindByText(obj.GetTranslatedText("Site Administrator")));
                        }
                    }
                }
               
				//FB 2262,//FB 2599 - Starts
                if (Session["Cloud"] != null)
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }
                if (Cloud == 1)
                {
                    //btnnewSubmit.Attributes.Add("style", "display:none");//FB 2565                 
                    //tdnewsubmit1.Attributes.Add("style", "display:none");//FB 2717
                    txtVidyoURL.Visible = true;
                    trpin.Visible = true;
                    txtExtension.Visible = true;
                    tdExten.Visible = true;
                    tdMeetlink.Visible = true;
                }
                //FB 2262,//FB 2599 - Ends

                //2670 starts
                if (lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("2") || (lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("3")))
                {

                    chkVNOC.Attributes.Add("Style", "Display:block");
                    Label1.Attributes.Add("Style", "Display:block");
                }
                else
                {
                    chkVNOC.Attributes.Add("style", "Display:none");
                    Label1.Attributes.Add("Style", "Display:none");
                }
                //2670 End
                if (Session["admin"].ToString() == "3" && (Request.QueryString["t"] != null && Request.QueryString["t"].ToString().Equals("1")))
                {
                    
                    //btnPrivateVMRSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796 
                    //btnPrivateVMRSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray;// FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                   
                    //btnnewSubmit.ForeColor = System.Drawing.Color.Gray;// FB 2796
                    //btnnewSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    //btnPrivateVMRSubmit.Visible = false; // ZD 102535
                    btnSubmit.Visible = false;
                    btnnewSubmit.Visible = false;
                    btnReset.Visible = false;

                }
                // FB 2796 Start
                else
                {
                    //btnPrivateVMRSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat"); // ZD 102535
                    btnSubmit.Attributes.Add("Class", "altLongBlueButtonFormat");
                    btnnewSubmit.Attributes.Add("Class", "altLongBlueButtonFormat");

                }
                // FB 2796 End
                //FB 2865 Start
                if(Session["LoginUserOrgID"] != null)
                {
                    if (Session["organizationID"].ToString() != Session["LoginUserOrgID"].ToString() && (Request.QueryString["t"] == null))
                    {
                        btnSubmit.Enabled = false;
                        btnSubmit.Attributes.Add("Class", "btndisable");
                        btnDefine.Enabled = false;
                        btnDefine.Attributes.Add("Class", "btndisable");
                        btnDefine.Attributes.Add("Style", "width:150pt");
                        btnlogin.Enabled = false;
                        btnlogin.Attributes.Add("Class", "btndisable");
                        btnlogin.Attributes.Add("Style", "width:150pt");
                        btnAddHelpReq.Enabled = false;
                        btnAddHelpReq.Attributes.Add("Class", "btndisable");
                        //btnPrivateVMRSubmit.Disabled = true;//ZD 100420 // ZD 102535
                        //btnPrivateVMRSubmit.Attributes.Add("Class", "btndisable"); // ZD 102535
                        //btnReset.Enabled = false;
                        //btnReset.Attributes.Add("Class", "btndisable"); 
                        
                    }
                }
                //FB 2865 Ends
                BindRoomToList();
                //FB 2953 - Start
                if (Session["isExpressUser"] != null)
                {
                    if (Session["isExpressUser"].ToString() == "1")
                    {
                        btnDefine.Enabled = false;
                        btnDefine.Attributes.Add("Class", "btndisable");
                        btnDefine.Attributes.Add("Style", "width:150pt");
                        btnlogin.Enabled = false;
                        btnlogin.Attributes.Add("Class", "btndisable");
                        btnlogin.Attributes.Add("Style", "width:150pt");
                    }
                }
                //FB 2953 - End
                if ((enableCloudInstallation == 1 && enableAdvancedUserOption == 1) && (Session["UsrCrossAccess"].ToString() != "1" || Session["admin"].ToString() == "3" || Session["UsrCrossAccess"].ToString() == "1"))
                {
                    populateNewUserRole();//ZD 100164 - Changes made by Inncrewin Ram Manohar
                    //trStaticID.Attributes.Add("Style", "display:none"); //ZD 100755 Inncrewin Starts
                }
                else if (enableCloudInstallation == 1 && enableAdvancedUserOption == 0) //ZD 100755 Inncrewin Starts
                {
                    trStaticID.Attributes.Add("Style", "");
                    tdClear1.Attributes.Add("Style", "display:none");
                    tdClear2.Attributes.Add("Style", "display:none");
                    tdClear3.Attributes.Add("Style", "display:none");
                    tdClear4.Attributes.Add("Style", "display:none");
                }
                else
                    trStaticID.Attributes.Add("Style", "display:none");
                //ZD 100164 - Changes made by Inncrewin Ram Manohar
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
               // errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace);
            }

        }
        protected void BindData(Object sender, EventArgs e)
        {
            BindDataUser();
        }

        protected void BindDataUser()
        {
            try
            {
                //PSU Fix
                if ((Application["ssoMode"] != null && Application["ssoMode"].ToString().ToUpper().Equals("YES"))
                    || (Application["CosignEnable"] != null && Application["CosignEnable"].ToString().Equals("1")))
                {
                    txtUserEmail.Enabled = false;
                    txtUserEmail2.Enabled = false;
                    reqUserEmail.Enabled = false;
                    regEmail1_1.Enabled = false;
                    regEmail1_1.Enabled = false;
                    regEmail1_2.Enabled = false;
                    regEmail2_1.Enabled = false;
                    regEmail2_2.Enabled = false;
                    if (Application["Client"].ToString().ToUpper().Equals("WASHU"))
                    {
                        txtUserEmail.Enabled = false;
                        txtUserEmail2.Enabled = false;
                        reqUserEmail.Enabled = false;
                        regEmail1_1.Enabled = false;
                        regEmail1_1.Enabled = false;
                        regEmail1_2.Enabled = false;
                        regEmail2_1.Enabled = false;
                        regEmail2_2.Enabled = false;
                    }
                    txtUserFirstName.Enabled = false;
                    txtUserLastName.Enabled = false;
                    txtPassword1.Enabled = false;
                    txtPassword2.Enabled = false;
                    //txtUserLogin.Enabled = false; //ZD 104850
                    //reqPassword1.Enabled = false; ZD 100263
                    cmpValPassword1.Enabled = false;
                    cmpValPassword2.Enabled = false;
                    reqFirstName.Enabled = false;
                    reqLastName.Enabled = false;
                    regPassword1.Enabled = false;
                    regPassword2.Enabled = false;
                    //Ticker Fixes START
                    //reqtxtfeed.Enabled = false; //Ticker changes
                    //reqtxtfeed1.Enabled = false; //Ticker Changes
                    //reqTxtTickerBknd.Enabled = false; //Ticker Changes
                    //reqTxtTickerBknd1.Enabled = false; //Ticker Changes
                    //Ticker Fixes End
                }
                lblHeader.Text = obj.GetTranslatedText("Create New User");//FB 1830 - Translation
                /* Get Timezones list from the system */
                String selTZ = "-1";
                lstTimeZone.ClearSelection();
                obj.GetTimezones(lstTimeZone, ref selTZ);

                /* Get Groups List in the system */
                //if (txtUserID.Text.Trim() != "" && !(txtUserID.Text.Trim().ToLower().Contains("new"))) //FB 1598
                    //obj.GetGroups(lstDefaultGroup, txtUserID.Text.Trim());

                if (Session["ModifiedUserID"].ToString() != "" && !(Session["ModifiedUserID"].ToString().Trim().ToLower().Contains("new"))) //FB 1598
                    obj.GetGroups(lstDefaultGroup, Session["ModifiedUserID"].ToString().Trim());
                else
                    obj.GetGroups(lstDefaultGroup);

                /* Get User Roles in the system */
                obj.GetUserRoles(lstUserRole, "ID", "name");
                obj.GetUserRoles(lstAdmin, "ID", "level");
                obj.GetUserRoles(lstRoles, "ID", "menuMask"); //ZD 103686
                
                /* Get Search Templates in the system */
                obj.GetSearchTemplateList(lstSearchTemplate);
                /* Get Line Rate in the system */
                obj.BindLineRate(lstLineRate);
                /* Get All bridges/MCUs in the system */
                obj.BindBridges(lstBridges);
                obj.BindBridgesType(lstBridgeType);	//ZD 104821 
                /* Get Address Type in the system */
                obj.BindAddressType(lstAddressType);
                /* Get Address Type in the system for MCU Address Type dropdown */
                obj.BindAddressType(lstMCUAddressType);
                /* Get Video Equipment list in the system */
                obj.BindVideoEquipment(lstVideoEquipment);
                /* Get All available Video protocols */
                obj.BindVideoProtocols(lstProtocol);
                /* Get Departments in the system */
                obj.GetManageDepartment(lstDepartment);
                /* Get All Connection types */
                obj.BindDialingOptions(lstConnectionType);

                //FB 2392-WhyGo Starts
                int r = 0;
                foreach (myVRMNet.NETFunctions.Region myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.Region)))
                {
                    ListItem li = new ListItem(myValue.ToString(), ((Int32)myValue).ToString());
                    lstRegion.Items.Insert(r++, li);
                }
                //FB 2392-WhyGo End

                txtUserFirstName.Text = "";
                txtUserLastName.Text = "";
                txtUserLogin.Text = "";
                txtPassword1.Text = "";
                txtPassword2.Text = "";
                txtPassword1_1.Value = "";
                txtPassword1_2.Value = "";
                txtUserEmail.Text = "";
                txtUserEmail2.Text = "";
                //Added for FB 1642-Audio add on - Starts 
                txtWorkPhone.Text = "";
                txtCellPhone.Text = "";
                txtConfCode.Text = "";
                txtLeaderPin.Text = "";
                txtPartyCode.Text = ""; //ZD 101446
                //Added for FB 1642-Audio add on - End 
                //FB 2227 Start
                txtIntvideonum.Text = "";
                txtExtvideonum.Text = "";
                //FB 2227 End
                txtEndpointName.Text = "";
                txtEPPassword1.Text = "";
                txtEPPassword2.Text = "";
                txtAddress.Text = "";
                txtWebAccessURL.Text = "";
                txtAssociateMCUAddress.Text = "";
                txtAccountExpiry.Text = "";
                if (enableCloudInstallation == 1 && enableAdvancedUserOption == 1 && (Session["UsrCrossAccess"].ToString() != "1" || Session["UsrCrossAccess"].ToString() == "1")) //ZD 100164
                    txtAccountExpiry.Text = "2030-06-24 00:00:00.000";
               	//ZD 102043 Starts
                lblTimeRemaining.Text = "1000000";//ZD 101127
                txtTotalTime.Text = "1000000";//ZD 101127
                if (enableCloudInstallation == 1) //FB 2659\
                {
                    txtTotalTime.Text = "10000";
                }
				//ZD 102043 End
                lstAddressBook.ClearSelection();
                lstAddressType.ClearSelection();
                lstBridges.ClearSelection();
                lstBridgeType.ClearSelection(); //ZD 104821
                lstConnectionType.ClearSelection();
                lstDefaultGroup.ClearSelection();
                lstEmailNotification.ClearSelection();
                lstLineRate.ClearSelection();
                lstProtocol.ClearSelection();
                lstSearchTemplate.ClearSelection();
                lstTimeZone.ClearSelection();
                lstUserRole.ClearSelection();
                lstVideoEquipment.ClearSelection();
                lstIsOutsideNetwork.ClearSelection();
                lstEncryption.ClearSelection();
                lstAddressType.ClearSelection();
                lstSendBoth.ClearSelection();
                //Code added For Date Format by offshore - FB Issue No 1073 - Start
                DrpDateFormat.ClearSelection();
                //Code added For Date Format  by offshore - FB Issue No 1073 - End
                //Code added for AV Switch FB 1426,1429,1425
                DrpEnableAV.ClearSelection();
                DrpEnvPar.ClearSelection();
                //ZD 101122
                DrpAVWO.ClearSelection();
                DrpCateringWO.ClearSelection();
                DrpFacilityWO.ClearSelection();

                lstTimeFormat.ClearSelection();
                lstTimeZoneDisplay.ClearSelection();

                //Ticker Start
                txtTickerBknd.Text = "";
                txtFeedLink.Text = "";
                drpTickerDisplay.ClearSelection();
                drpTickerPosition.ClearSelection();
                drpTickerSpeed.ClearSelection();
                drpTickerStatus.ClearSelection();
                txtTickerBknd1.Text = "";
                txtFeedLink1.Text = "";
                drpTickerDisplay1.ClearSelection();
                drpTickerPosition1.ClearSelection();
                drpTickerSpeed1.ClearSelection();
                drpTickerStatus1.ClearSelection();
                //Ticker End
                //ZD 100263
                //if (!txtUserID.Text.Trim().ToLower().Equals("new"))
                if (!Session["ModifiedUserID"].ToString().ToLower().Equals("new"))
                {
                    GetUserProfile();
                }
                else
                {
                    //FB 1830 starts
                    obj.GetLanguages(lstLanguage);//FB 1830
                    lstLanguage.Items.FindByValue("1").Selected = true;
                    //FB 1830 Ends

                    //Ticker changes - Start
                    txtFeedLink.Attributes.Add("style", "display:none");
                    feedLink.Attributes.Add("style", "display:none");
                    txtFeedLink1.Attributes.Add("style", "display:none");
                    feedLink1.Attributes.Add("style", "display:none");
                    //FB 1567 -- Start
                    drpTickerStatus.SelectedValue = "1";
                    drpTickerStatus1.SelectedValue = "1";
                    txtTickerBknd.Text = "#660066";
                    txtTickerBknd1.Text = "#99ff99";
                    //FB 1567 -- End

                    //Ticker changes - End
                    trLandingPage.Visible = false; // ZD 100172


                    //ZD 104116 Starts
                    if (Session["SiteBJNuserLimit"].ToString() == "-2")
                    {
                        ChkBJNConf.Checked = true;
                        ChkBJNConf.Disabled = true;
                        tblBJNConf.Style.Add("display", "");
                    }
                    //ZD 104116 Ends
                }
                //FB 2588 - Start
                if (Session["EnableZulu"] != null)
                {
                    if (Session["EnableZulu"].ToString() == "1")
                    {
                        lstTimeZone.SelectedValue = "33";
                        lstTimeFormat.SelectedValue = "2";
                        lstTimeZoneDisplay.SelectedValue = "0";
                        lstTimeFormat.Enabled = false;
                        lstTimeZone.Enabled = false;
                        lstTimeZoneDisplay.Enabled = false;
                    }
                    else
                    {
                        if (lstTimeFormat.Items.FindByValue("2") != null)
                            lstTimeFormat.Items.Remove(lstTimeFormat.Items.FindByValue("2"));
                        lstTimeFormat.Enabled = true;
                        lstTimeZone.Enabled = true;
                        lstTimeZoneDisplay.Enabled = true;
                    }
                }
                //FB 2588 End

                //ZD 100164 START TDB - New User Creation
                //Allowing only Organization Administrator 1,Organization Administrator 2 user role can able to create the following roles only ie) General User,Express Profile
                if (Session["UsrCrossAccess"] != null)
                {
                    if (enableCloudInstallation == 1 && enableAdvancedUserOption == 1 && (Session["UsrCrossAccess"].ToString() == "0" || Session["UsrCrossAccess"].ToString() == "1"))
                    {
                        if (Session["UsrCrossAccess"] != null)
                        {
                            if (enableCloudInstallation == 1 && enableAdvancedUserOption == 1 && (Session["UsrCrossAccess"].ToString() == "0" || Session["UsrCrossAccess"].ToString() == "1"))
                            {
                                if (Session["ModifiedUserID"].ToString().Trim().ToLower().Equals("new"))
                                {
                                    if (lstUserRole.Items.FindByText("Catering Administrator") != null)
                                        lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Catering Administrator"));

                                    if (lstUserRole.Items.FindByText("Audiovisual Administrator") != null)
                                        lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Audiovisual Administrator"));

                                    if (lstUserRole.Items.FindByText("Facility Administrator") != null)
                                        lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Facility Administrator"));

                                    if (lstUserRole.Items.FindByText("Express Profile Advanced") != null)
                                        lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Express Profile Advanced"));

                                    if (lstUserRole.Items.FindByText("Express Profile Manage") != null)
                                        lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Express Profile Manage"));

                                    if (lstUserRole.Items.FindByText("View-Only") != null)
                                        lstUserRole.Items.Remove(lstUserRole.Items.FindByText("View-Only"));

                                    if (lstUserRole.Items.FindByText("VNOC Operator") != null)
                                        lstUserRole.Items.Remove(lstUserRole.Items.FindByText("VNOC Operator"));

                                   

                                }
                            }
                        }
                    }
                }

               //ZD 100164 END- TDB - New User Creation
                
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
               // errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        protected void GetUserProfile()
        {
            //string emailBlock = "";//FB 1860 FB 2027

            try
            {
                Session["multisiloOrganizationID"] = null; //FB 2274
                Session.Add("userPW", ""); //FB 3054
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><user><userID>" + Session["ModifiedUserID"].ToString() + "</userID></user></login>";//Organization Module Fixes
                //String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><user><userID>" + txtUserID.Text + "</userID></user></login>";//Organization Module Fixes
                
                //FB 2027 Start
                String outXML = obj.CallMyVRMServer("GetOldUser", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = obj.CallCOM("GetOldUser", inXML, Application["COM_ConfigPath"].ToString());
                //FB 2027 End
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    if (Session["admin"].ToString() == "3") //FB 2670
                        lblHeader.Text = obj.GetTranslatedText("View User Details");
                    else
                        lblHeader.Text = obj.GetTranslatedText("Edit Existing User");//FB 1830 - Translation


                    //ZD 100664 Start
                    XmlNodeList changedHistoryNodes = null;
                    ArrayList colNames = null;
                    changedHistoryNodes = xmldoc.SelectNodes("//oldUser/LastModifiedDetails/LastModified");
                    if (changedHistoryNodes != null)
                    {
                        DataTable dtable = new DataTable();
                        colNames = new ArrayList();
                        colNames.Add("ModifiedUserId");
                        colNames.Add("ModifiedDateTime");
                        colNames.Add("ModifiedUserName");
                        colNames.Add("Description");
                        colNames.Add("ModifiedDetails");
                        dtable = obj.LoadDataTable(changedHistoryNodes, colNames);
                        DateTime ModifiedDateTime = DateTime.Now;
                        for (Int32 i = 0; i < dtable.Rows.Count; i++)
                        {
                            DateTime.TryParse(dtable.Rows[i]["ModifiedDateTime"].ToString(), out ModifiedDateTime);
                            dtable.Rows[i]["ModifiedDateTime"] = myVRMNet.NETFunctions.GetFormattedDate(ModifiedDateTime.Date.ToString()) + " " + ModifiedDateTime.ToString(tformat);

                            dtable.Rows[i]["Description"] = obj.GetTranslatedText(dtable.Rows[i]["Description"].ToString());
                        }

                        dgChangedHistory.DataSource = dtable;
                        dgChangedHistory.DataBind();
                    }
                    //ZD 100664 End


                    txtUserFirstName.Text = xmldoc.SelectSingleNode("//oldUser/userName/firstName").InnerText;
                    txtUserLastName.Text = xmldoc.SelectSingleNode("//oldUser/userName/lastName").InnerText;
                    txtUserLogin.Text = xmldoc.SelectSingleNode("//oldUser/login").InnerText;
                    //FB 3054 Starts
                    //txtPassword1.Attributes.Add("value", ns_MyVRMNet.vrmPassword.UserProfile); // ZD 100263
                    //txtPassword2.Attributes.Add("value", ns_MyVRMNet.vrmPassword.UserProfile); // ZD 100263
                    //txtPassword1_1.Value = ns_MyVRMNet.vrmPassword.UserProfile; // ZD 100263
                    //txtPassword1_2.Value = ns_MyVRMNet.vrmPassword.UserProfile; // ZD 100263
                    Session.Remove("userPW");
                    if (xmldoc.SelectSingleNode("//oldUser/password").InnerText.Trim() != "")
                        Session["userPW"] = xmldoc.SelectSingleNode("//oldUser/password").InnerText;
                    //txtPassword1.Attributes.Add("value", xmldoc.SelectSingleNode("//oldUser/password").InnerText);
                    //txtPassword2.Attributes.Add("value", xmldoc.SelectSingleNode("//oldUser/password").InnerText);
                    //txtPassword1_1.Value = xmldoc.SelectSingleNode("//oldUser/password").InnerText;
                    //txtPassword1_2.Value = xmldoc.SelectSingleNode("//oldUser/password").InnerText;
                    //FB 3054 Ends
                    txtUserEmail.Text = xmldoc.SelectSingleNode("//oldUser/userEmail").InnerText;
                    txtUserEmail2.Text = xmldoc.SelectSingleNode("//oldUser/alternativeEmail").InnerText;
                    //Code added by Offshore for FB Issue 1073 -- Start
                    // txtAccountExpiry.Text = xmldoc.SelectSingleNode("//oldUser/expiryDate").InnerText;
                    //ZD 100995 start
                    DateTime accExpiry = Convert.ToDateTime(xmldoc.SelectSingleNode("//oldUser/expiryDate").InnerText);
                    txtAccountExpiry.Text = accExpiry.ToString(format);//myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//oldUser/expiryDate").InnerText);
                    //ZD 100995 End
                    //Code added by Offshore for FB Issue 1073 -- end
					//ZD 102043 Starts
                    lblTimeRemaining.Text = xmldoc.SelectSingleNode("//oldUser/timeRemaining").InnerText;
                    txtTotalTime.Text = xmldoc.SelectSingleNode("//oldUser/initialTime").InnerText;
					//ZD 102043 End
                    txtLevel.Value = xmldoc.SelectSingleNode("//oldUser/status/level").InnerText;
                    //FB 1642-Audio add on -Starts
                    txtWorkPhone.Text = xmldoc.SelectSingleNode("//oldUser/workPhone").InnerText;
                    txtCellPhone.Text = xmldoc.SelectSingleNode("//oldUser/cellPhone").InnerText;
                    txtConfCode.Text = xmldoc.SelectSingleNode("//oldUser/conferenceCode").InnerText;
                    txtLeaderPin.Text = xmldoc.SelectSingleNode("//oldUser/leaderPin").InnerText;
                    txtPartyCode.Text = xmldoc.SelectSingleNode("//oldUser/participantCode").InnerText; //ZD 101446

                    if (xmldoc.SelectSingleNode("//oldUser/Audioaddon").InnerText == "1")
                    {
                        chkAudioAddon.Checked = true;
                    }
                    else
                    {
                        chkAudioAddon.Checked = false;
                    }
                    //FB 1642-Audio add on - End
                    //FB 2227 Start
                    txtIntvideonum.Text = xmldoc.SelectSingleNode("//oldUser/IntVideoNum").InnerText;
                    txtExtvideonum.Text = xmldoc.SelectSingleNode("//oldUser/ExtVideoNum").InnerText;
                    //FB 2227 End
                    //FB 1830 - Start
                    XmlNodeList nodes = xmldoc.SelectNodes("//oldUser/languages/language");
                    //LoadLanguage(nodes);
                    obj.GetLanguages(lstLanguage);
                    if (!xmldoc.SelectSingleNode("//oldUser/languageID").InnerText.Equals("0"))
                        lstLanguage.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/languageID").InnerText).Selected = true;

                    Session.Remove("UsrEmailLangID"); //FB 2283
                    if (xmldoc.SelectSingleNode("//oldUser/EmailLang").InnerText.Trim() != "")
                    {
                        txtEmailLang.Text = xmldoc.SelectSingleNode("//oldUser/EmailLangName").InnerText;
                        Session["UsrEmailLangID"] = xmldoc.SelectSingleNode("//oldUser/EmailLang").InnerText;
                    }
                    //FB 1830 - End

                    //FB 2502 Starts
                    //delEmailLang.Visible = false;
                    //if (Session["UsrEmailLangID"] != null)
                    //{
                    //    if (Session["UsrEmailLangID"].ToString() != "")
                    //    {
                    //        if(Session["UsrEmailLangID"].ToString() == "0")
                    //            delEmailLang.Visible = false;
                    //        else
                    //            delEmailLang.Visible = true;
                    //    }
                    //}
                    //FB 2502 Ends

                    //Code added for AV Switch
                    DrpEnableAV.Enabled = true;
                    if (!txtLevel.Value.Equals("0"))
                    {
                        DrpEnableAV.Enabled = false;
                        DrpEnableAV.SelectedIndex = 0;
                    }
                    else if (xmldoc.SelectSingleNode("//oldUser/enableAV") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/enableAV").InnerText != "")
                            DrpEnableAV.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/enableAV").InnerText).Selected = true;
                    }

                    DrpEnvPar.Enabled = true;
                    if (!txtLevel.Value.Equals("0"))
                    {
                        DrpEnvPar.Enabled = false;
                        DrpEnvPar.SelectedIndex = 0;
                    }
                    else if (xmldoc.SelectSingleNode("//oldUser/enableParticipants") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/enableParticipants").InnerText != "")
                            DrpEnvPar.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/enableParticipants").InnerText).Selected = true;
                    }

                    //ZD 101122 - Start
                    DrpAVWO.Enabled = true;
                    if (!txtLevel.Value.Equals("0"))
                    {
                        DrpAVWO.Enabled = false;
                        DrpAVWO.SelectedIndex = 0;
                    }
                    else if (xmldoc.SelectSingleNode("//oldUser/enableAVWorkOrder") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/enableAVWorkOrder").InnerText != "")
                            DrpAVWO.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/enableAVWorkOrder").InnerText).Selected = true;
                    }
                    DrpCateringWO.Enabled = true;
                    if (!txtLevel.Value.Equals("0"))
                    {
                        DrpCateringWO.Enabled = false;
                        DrpCateringWO.SelectedIndex = 0;
                    }
                    else if (xmldoc.SelectSingleNode("//oldUser/enableCateringWO") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/enableCateringWO").InnerText != "")
                            DrpCateringWO.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/enableCateringWO").InnerText).Selected = true;
                    }
                    DrpFacilityWO.Enabled = true;
                    if (!txtLevel.Value.Equals("0"))
                    {
                        DrpFacilityWO.Enabled = false;
                        DrpFacilityWO.SelectedIndex = 0;
                    }
                    else if (xmldoc.SelectSingleNode("//oldUser/enableFacilityWO") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/enableFacilityWO").InnerText != "")
                            DrpFacilityWO.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/enableFacilityWO").InnerText).Selected = true;
                    }
                    //ZD 101122 - End

                    //ZD 101093 START
                    if (xmldoc.SelectSingleNode("//oldUser/enableAdditionalOption") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/enableAdditionalOption").InnerText != "")
                            drpadditionaloption.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/enableAdditionalOption").InnerText).Selected = true;
                    }
                    //ZD 101093 END

                    txtAddress.Text = xmldoc.SelectSingleNode("//oldUser/IPISDNAddress").InnerText;
                    txtEndpointID.Text = xmldoc.SelectSingleNode("//oldUser/EndpointID").InnerText;
                    try
                    {
                        lstTimeZone.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/timeZone").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        //lstAddressBook.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/addressTypeID").InnerText).Selected = true;//FB 765
                        //Code Modified on 21Mar09 For FB 412 - Start
                        //lstAddressBook.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/addressTypeID").InnerText).Selected = true;//FB 765
                        lstAddressBook.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/emailClient").InnerText).Selected = true;//FB 765,1361
                        //Code Modified on 21Mar09 For FB 412 - End
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    //ZD 100164 START TDB - New User Creation
                    try
                    {
                        if (enableCloudInstallation == 1 && enableAdvancedUserOption == 1 && (Session["UsrCrossAccess"].ToString() != "1" || Session["UsrCrossAccess"].ToString() == "1"))
                        {
                            lstUserRole.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/roleID").InnerText).Selected = true;
                            int.TryParse(xmldoc.SelectSingleNode("//oldUser/roleID").InnerText.Trim(), out RoleID);
                            if (RoleID == 4 || RoleID == 5 || RoleID == 6 || RoleID == 8 || RoleID == 9 || RoleID == 10 || RoleID == 11)
                            {
                                lstUserRole.Enabled = false;
                            }
                            if (RoleID == 1 || RoleID == 2 || RoleID == 7 || RoleID == 12 || RoleID > 12 || RoleID==3)
                            {
                                if(lstUserRole.Items.FindByText("Catering Administrator") != null)
                                    lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Catering Administrator"));

                                if (lstUserRole.Items.FindByText("Audiovisual Administrator") != null)
                                    lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Audiovisual Administrator"));

                                if (lstUserRole.Items.FindByText("Facility Administrator") != null)
                                    lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Facility Administrator"));

                                if (lstUserRole.Items.FindByText("Express Profile Advanced") != null)
                                    lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Express Profile Advanced"));

                                if (lstUserRole.Items.FindByText("Express Profile Manage") != null)
                                    lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Express Profile Manage"));

                                if (lstUserRole.Items.FindByText("View-Only") != null)
                                    lstUserRole.Items.Remove(lstUserRole.Items.FindByText("View-Only"));

                                if (lstUserRole.Items.FindByText("VNOC Operator") != null)
                                    lstUserRole.Items.Remove(lstUserRole.Items.FindByText("VNOC Operator"));
                            }
                        }
                        else
                        {
                            if (xmldoc.SelectSingleNode("//oldUser/roleID").InnerText != null)
                                lstUserRole.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/roleID").InnerText).Selected = true;

                            //ZD 103686
                            DisplayConferenceOptions();
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //ZD 100164 END TDB - New User Creation
                    
                    try
                    {
                        //lstEmailNotification.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/emailClient").InnerText).Selected = true;//FB 765
                        lstEmailNotification.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/emailMask").InnerText).Selected = true;//FB 1361
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstSendBoth.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/sendBoth").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstSearchTemplate.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/SavedSearch").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    try   //FB 1598
                    {
                        lstDefaultGroup.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/defaultAGroups/groupID").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    //Code Changed For Date Format  by Offshore - FB IssueNo 1073 -Start
                    try
                    {
                        DrpDateFormat.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/dateFormat").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //Code Changed For Date Format  by Offshore - FB IssueNo 1073 - End

                    //Code Changed For Date Format  by Offshore - FB IssueNo 1426,1425
                    try
                    {
                        lstTimeFormat.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/timeFormat").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //Code Added for License modification START
                    try
                    {
                        DrpDom.SelectedValue = xmldoc.SelectSingleNode("//oldUser/dominoUser").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        DrpExc.SelectedValue = xmldoc.SelectSingleNode("//oldUser/exchangeUser").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //FB 1979 - Start
                    try
                    {
                        DrpMob.SelectedValue = xmldoc.SelectSingleNode("//oldUser/mobileUser").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + ":" + ex.StackTrace);
                    }
                    //FB 1979 - End
                    //FB 2141 - Start
                    try
                    {
                        DrpPIMNotifications.SelectedValue = xmldoc.SelectSingleNode("//oldUser/PIMNotification").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + ":" + ex.StackTrace);
                    }
                    //FB 2141 - End
                    //Code Added for License modification END
                    try
                    {
                        lstTimeZoneDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/timezoneDisplay").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    //code added for ticker -- Satart


                    String tickerstatus = "1";    //FB 1567
                    if (xmldoc.SelectSingleNode("//oldUser/tickerStatus") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerStatus").InnerText != "")
                        {
                            tickerstatus = xmldoc.SelectSingleNode("//oldUser/tickerStatus").InnerText;
                        }
                    }

                    drpTickerStatus.Items.FindByValue(tickerstatus).Selected = true;

                    String tickerspeed = "6";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerSpeed") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerSpeed").InnerText != "")
                        {
                            tickerspeed = xmldoc.SelectSingleNode("//oldUser/tickerSpeed").InnerText;
                        }
                    }
                    drpTickerSpeed.Items.FindByValue(tickerspeed).Selected = true;

                    string tickerdisplay = "0";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerDisplay") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerDisplay").InnerText != "")
                        {
                            tickerdisplay = xmldoc.SelectSingleNode("//oldUser/tickerDisplay").InnerText;
                        }
                    }
                    drpTickerDisplay.Items.FindByValue(tickerdisplay).Selected = true;

                    if (tickerdisplay == "0")
                    {
                        txtFeedLink.Attributes.Add("style", "display:none");
                        feedLink.Attributes.Add("style", "display:none");

                    }
                    else
                    {
                        txtFeedLink.Attributes.Add("style", "display:block");
                        feedLink.Attributes.Add("style", "display:block");
                    }

                    String tickerpos = "0";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerPosition") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerPosition").InnerText != "")
                        {
                            tickerpos = xmldoc.SelectSingleNode("//oldUser/tickerPosition").InnerText;
                        }
                    }
                    drpTickerPosition.Items.FindByValue(tickerpos).Selected = true;

                    if (xmldoc.SelectSingleNode("//oldUser/tickerBackground") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerBackground").InnerText != "")
                        {
                            txtTickerBknd.Text = xmldoc.SelectSingleNode("//oldUser/tickerBackground").InnerText;
                        }
                        else    //FB 1567
                        {
                            txtTickerBknd.Text = "#660066";
                        }


                    }

                    if (xmldoc.SelectSingleNode("//oldUser/rssFeedLink") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/rssFeedLink").InnerText != "")
                        {
                            txtFeedLink.Text = xmldoc.SelectSingleNode("//oldUser/rssFeedLink").InnerText;
                        }
                    }

                    String tickerstatus1 = "1";          //Fb 1567
                    if (xmldoc.SelectSingleNode("//oldUser/tickerStatus1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerStatus1").InnerText != "")
                        {
                            tickerstatus1 = xmldoc.SelectSingleNode("//oldUser/tickerStatus1").InnerText;
                        }
                    }

                    drpTickerStatus1.Items.FindByValue(tickerstatus1).Selected = true;

                    String tickerspeed1 = "6";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerSpeed1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerSpeed1").InnerText != "")
                        {
                            tickerspeed1 = xmldoc.SelectSingleNode("//oldUser/tickerSpeed1").InnerText;
                        }
                    }
                    drpTickerSpeed1.Items.FindByValue(tickerspeed1).Selected = true;

                    string tickerdisplay1 = "0";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerDisplay1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerDisplay1").InnerText != "")
                        {
                            tickerdisplay1 = xmldoc.SelectSingleNode("//oldUser/tickerDisplay1").InnerText;
                        }
                    }
                    drpTickerDisplay1.Items.FindByValue(tickerdisplay1).Selected = true;
                    if (tickerdisplay1 == "0")
                    {
                        txtFeedLink1.Attributes.Add("style", "display:none");
                        feedLink1.Attributes.Add("style", "display:none");

                    }
                    else
                    {
                        txtFeedLink1.Attributes.Add("style", "display:block");
                        feedLink1.Attributes.Add("style", "display:block");
                    }
                    String tickerpos1 = "0";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerPosition1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerPosition1").InnerText != "")
                        {
                            tickerpos1 = xmldoc.SelectSingleNode("//oldUser/tickerPosition1").InnerText;
                        }
                    }
                    drpTickerPosition1.Items.FindByValue(tickerpos1).Selected = true;

                    if (xmldoc.SelectSingleNode("//oldUser/tickerBackground1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerBackground1").InnerText != "")
                        {
                            txtTickerBknd1.Text = xmldoc.SelectSingleNode("//oldUser/tickerBackground1").InnerText;
                        }
                        else    //FB 1567
                        {
                            txtTickerBknd1.Text = "#99ff99";
                        }
                    }

                    if (xmldoc.SelectSingleNode("//oldUser/rssFeedLink1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/rssFeedLink1").InnerText != "")
                        {
                            txtFeedLink1.Text = xmldoc.SelectSingleNode("//oldUser/rssFeedLink1").InnerText;
                        }
                    }
                    //code added for ticker - End

                    nodes = xmldoc.SelectNodes("//oldUser/departments/selected"); //FB 1830
                    //Response.Write(Session["admin"].ToString());
                    foreach (ListItem li in lstDepartment.Items)
                    {
                        if (txtLevel.Value.Trim().Equals("2") || txtLevel.Value.Trim().Equals("3"))//ZD 101764
                        {
                            li.Selected = true;
                            lstDepartment.Enabled = false;
                        }
                        foreach (XmlNode node in nodes)
                        {
                            if (li.Value.Trim().Equals(node.InnerText.Trim()))
                                li.Selected = true;
                        }
                    }
                    if (xmldoc.SelectNodes("//oldUser/locationList/selected/level1ID").Count > 0)// code added for room search
                    {
                        //txtLocation.Text = xmldoc.SelectSingleNode("//oldUser/locationList/selected/selectedName").InnerText;

                        if (xmldoc.SelectSingleNode("//oldUser/locationList/selected/level1ID").InnerText != "")
                        {
                            myVRMNet.NETFunctions ntfuncs = new myVRMNet.NETFunctions();
                            locstrname.Value = ntfuncs.RoomDetailsString(xmldoc.SelectSingleNode("//oldUser/locationList/selected/level1ID").InnerText);
                            selectedloc.Value = xmldoc.SelectSingleNode("//oldUser/locationList/selected/level1ID").InnerText;
                            BindRoomToList();
                            ntfuncs = null;
                        }

                        hdnLocation.Value = xmldoc.SelectSingleNode("//oldUser/locationList/selected/level1ID").InnerText;
                    }
                    if (lstAddressBook.SelectedValue.Equals("2"))
                    {
                        txtLotusLoginName.Text = xmldoc.SelectSingleNode("//oldUser/lotus/lnLoginName").InnerText;
                        txtLotusPassword.Attributes.Add("value", xmldoc.SelectSingleNode("//oldUser/lotus/lnLoginPwd").InnerText);
                        txtLotusDBPath.Text = xmldoc.SelectSingleNode("//oldUser/lotus/lnDBPath").InnerText;
                    }
                    //Response.Write(txtEndpointID.Text);
                    if (!txtEndpointID.Text.Equals("0") && !txtEndpointID.Text.Equals("-1"))
                        LoadEndpointDetails();

                    hdntzone.Value = "1";//Code added for  FB 1425 QA Bug 

                    //Commented for FB 2027
                    //FB 1860
                    //emailBlock = obj.CallCommand("GetUserEmailsBlockStatus", inXML);
                    //if (emailBlock.IndexOf("<error>") < 0)
                    //{
                    //    XmlDocument blck = new XmlDocument();
                    //    blck.LoadXml(emailBlock);

                    //    XmlNode node = blck.SelectSingleNode("//login/emailBlockStatus");

                    if (xmldoc.SelectSingleNode("//oldUser/login/emailBlockStatus") != null)
                        {
                            if (xmldoc.SelectSingleNode("//oldUser/login/emailBlockStatus").InnerText == "1")
                            {
                                ChkBlockEmails.Checked = true;
                                 if (isAVEnabled) //FB 1522
                                    BtnBlockEmails.Attributes.Add("style", "display:block");
                            }
                        }
                        
 
                    //}
                    //FB 1860
                    //FB 2268 start
                    if (xmldoc.SelectSingleNode("//oldUser/HelpRequsetorEmail") != null)
                    {
                        hdnHelpReq.Value = xmldoc.SelectSingleNode("//oldUser/HelpRequsetorEmail").InnerText.Trim();
                        String[] HelpReq = xmldoc.SelectSingleNode("//oldUser/HelpRequsetorEmail").InnerText.Trim().Split('�');
                        for (int i = 0; i < HelpReq.Length; i++)
                        {
                            if (HelpReq[i].Trim() != "")
                            {
                                ListItem item = new ListItem();
                                item.Text = HelpReq[i];
                                item.Attributes.Add("title", HelpReq[i]);
                                lstHelpReqEmail.Items.Add(item) ;
                            }
                        }
                    }
                    if (xmldoc.SelectSingleNode("//oldUser/HelpRequsetorPhone") != null)
                        txtHelpReqPhone.Text = xmldoc.SelectSingleNode("//oldUser/HelpRequsetorPhone").InnerText.Trim();
                    
                    //FB 2268 end

                    //FB 2724
                    if (xmldoc.SelectSingleNode("//oldUser/RFIDValue") != null)
                        txtRFIDValue.Text = xmldoc.SelectSingleNode("//oldUser/RFIDValue").InnerText.Trim();
                    

                    //FB 2348
                    if (xmldoc.SelectSingleNode("//oldUser/SendSurveyEmail") != null)
                        drpSurveyEmail.SelectedValue = xmldoc.SelectSingleNode("//oldUser/SendSurveyEmail").InnerText.Trim();

                    // FB 2608 Start

                    if (xmldoc.SelectSingleNode("//oldUser/EnableVNOCselection") != null && xmldoc.SelectSingleNode("//oldUser/EnableVNOCselection").InnerText == "1")
                        chkVNOC.Checked = true;
                    else
                        chkVNOC.Checked = false;

                    if (Session["Admin"] != null)
                    {
                        if (Session["Admin"].ToString() == "0")
                        {
                            chkVNOC.Enabled = false;
                            ChkWebexConf.Disabled = true; //ZD 101015 
                        }
                        else
                        {
                            chkVNOC.Enabled = true;
                            ChkWebexConf.Disabled = false; //ZD 101015 
                        }
                    }

                    // FB 2608 End
                    
                    //FB 2481
                    if (xmldoc.SelectSingleNode("//oldUser/PrivateVMR") != null)
                        PrivateVMR.Html = utilObj.ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//oldUser/PrivateVMR").InnerXml.Trim(), 1); //FB 2721

                    //PrivateVMR.SettingsText.DesignViewTab = obj.GetTranslatedText("Design"); // ZD 100288 
                    //PrivateVMR.SettingsText.PreviewTab = obj.GetTranslatedText("Preview"); // ZD 100288 
                 
					 //FB 2599 Starts
                    //FB 2262
                    if (xmldoc.SelectSingleNode("//oldUser/VidyoURL") != null)
                        txtVidyoURL.Text = txtPin.Text = xmldoc.SelectSingleNode("//oldUser/VidyoURL").InnerText.Trim();

                    if (xmldoc.SelectSingleNode("//oldUser/Pin") != null)
                        txtConfirmPin.Text = txtPin.Text = xmldoc.SelectSingleNode("//oldUser/Pin").InnerText.Trim();

                    if (xmldoc.SelectSingleNode("//oldUser/Extension") != null)
                        txtExtension.Text = xmldoc.SelectSingleNode("//oldUser/Extension").InnerText.Trim();
                    //FB 2599 End
                    //FB 2594 Starts
                    whygouserSettings.Visible = false;//FB 2671
                    whygousersettings1.Visible = false;
                    if (Session["EnablePublicRooms"] != null)
                    {
                        if (Session["EnablePublicRooms"].ToString() == "1")
                        {
                            whygouserSettings.Visible = true;
                            whygousersettings1.Visible = true;
                            //FB 2392-Whygo Starts
                            if (xmldoc.SelectSingleNode("//oldUser/ParentReseller") != null)
                                txtParentReseller.Value = xmldoc.SelectSingleNode("//oldUser/ParentReseller").InnerText;
                            if (xmldoc.SelectSingleNode("//oldUser/CorpUser") != null)
                                txtCorpUser.Value = xmldoc.SelectSingleNode("//oldUser/CorpUser").InnerText;
                            if (xmldoc.SelectSingleNode("//oldUser/Region") != null)
                            {
                                if (lstRegion.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/Region").InnerText.Trim()) != null)
                                    lstRegion.SelectedValue = xmldoc.SelectSingleNode("//oldUser/Region").InnerText.Trim();
                            }
                            if (xmldoc.SelectSingleNode("//oldUser/ESUserID") != null)
                                txtESUserID.Value = xmldoc.SelectSingleNode("//oldUser/ESUserID").InnerText;
                            //FB 2392-Whygo End
                            if (xmldoc.SelectSingleNode("//oldUser/BrokerRoomNum") != null) //FB 3001
                                txtBrokerNum.Value = xmldoc.SelectSingleNode("//oldUser/BrokerRoomNum").InnerText;
                        }
                    }
                    //FB 2594 Ends
                    //FB 2595 Start
                    //if (xmldoc.SelectSingleNode("//oldUser/Secured") != null)
                    //{
                    //    if (xmldoc.SelectSingleNode("//oldUser/Secured").InnerText.Trim() == "1")
                    //        chkSecured.Checked = true;
                    //    else
                    //        chkSecured.Checked = false;
                    //}
                    //FB 2595 End

                    //FB 2693 Starts
                    if (xmldoc.SelectSingleNode("//oldUser/EnablePCUser") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/EnablePCUser").InnerText.Trim() == "1")
                        {
                            chkPcConf.Checked = true;
                            tblPcConf.Style.Add("display", "block");
                        }
                        else
                            chkPcConf.Checked = false;
                    }
                    if (xmldoc.SelectNodes("//oldUser/PCDetails/PCDetail") != null)
                    {
                        nodes = xmldoc.SelectNodes("//oldUser/PCDetails/PCDetail");
                        if (nodes.Count > 0)
                        {
                            LoadPCDetails(nodes);
                        }
                        
                    }
                    //FB 2693 Ends
                    //inncrewin ZD100755 start
                    if (enableCloudInstallation == 1)
                    {
                        //ZD 100890 Start
                        if (xmldoc.SelectSingleNode("//oldUser/IsStaticIDEnabled") != null)
                        {
                            if (xmldoc.SelectSingleNode("//oldUser/IsStaticIDEnabled").InnerText.Trim() == "1")
                            {
                                chkEnblStaticId.Checked = true;
                            }
                            else
                            {
                                chkEnblStaticId.Checked = false;
                            }
                        }

                        if (xmldoc.SelectSingleNode("//oldUser/StaticID") != null)
                            txtStaticBox.Text = xmldoc.SelectSingleNode("//oldUser/StaticID").InnerText.Trim();
                        //ZD 100890 End

                    }
					//ZD 100755 Inncrewin Starts
					//ZD 100221 Starts
                    if (xmldoc.SelectSingleNode("//oldUser/EnableWebexConf") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/EnableWebexConf").InnerText.Trim() == "1")
                        {
                            ChkWebexConf.Checked = true;
                            tblWebExConf.Style.Add("display", "");
                        }
                        else
                        {
                            ChkWebexConf.Checked = false;
                            tblWebExConf.Style.Add("display", "none");
                        }
                    }

                    if (xmldoc.SelectSingleNode("//oldUser/WebexUserName") != null)
                        txtWebExUserName.Text = xmldoc.SelectSingleNode("//oldUser/WebexUserName").InnerText.Trim();
                    //ZD 100221 End
                    
                    //ZD 103550 
                    //ZD 104116 Starts
                    if (Session["SiteBJNuserLimit"].ToString() == "-2" && (xmldoc.SelectSingleNode("//oldUser/EnableBJNConf").InnerText.Trim() == "0"))
                    {
                        ChkBJNConf.Checked = true;
                        ChkBJNConf.Disabled = true;
                        tblBJNConf.Style.Add("display", "");
                        txtBJNUserName.Text = xmldoc.SelectSingleNode("//oldUser/userEmail").InnerText;
                    }
                    else//ZD 104116  Ends
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/EnableBJNConf") != null)
                        {
                            if (xmldoc.SelectSingleNode("//oldUser/EnableBJNConf").InnerText.Trim() == "1")
                            {
                                ChkBJNConf.Checked = true;
                                if (Session["SiteBJNuserLimit"].ToString() == "-2")
                                    ChkBJNConf.Disabled = true;
                                tblBJNConf.Style.Add("display", "");
                            }
                            else
                            {
                                ChkBJNConf.Checked = false;
                                tblBJNConf.Style.Add("display", "none");
                            }
                        }
                        if (xmldoc.SelectSingleNode("//oldUser/BJNUserName") != null)
                            txtBJNUserName.Text = xmldoc.SelectSingleNode("//oldUser/BJNUserName").InnerText.Trim();
                    }
                    // ZD 100172 Starts
                    List<ListItem> lst = new List<ListItem>();
                    string menuMask="", LandingPageText="", LandingPageLink="";
                    if (xmldoc.SelectSingleNode("//oldUser/MenuMask") != null)
                    {
                        menuMask = xmldoc.SelectSingleNode("//oldUser/MenuMask").InnerText;
                        if (menuMask != "")
                        {
                            lst = (List<ListItem>)loginMgmt.FillAccessSpecifiers(menuMask, false);

                            lst.Sort(delegate(ListItem thisItem, ListItem otherItem)
                            {
                                return thisItem.Text.CompareTo(otherItem.Text);
                            });

                            drpPageList.DataSource = lst;
                            drpPageList.DataBind();
                        }
                    }
                    //ZD 104850 start
                    if (xmldoc.SelectSingleNode("//oldUser/UserDomain") != null)
                        txtUserDomain.Text = xmldoc.SelectSingleNode("//oldUser/UserDomain").InnerText.Trim();
                    //ZD 104850 End

                    if (xmldoc.SelectSingleNode("//oldUser/LandingPageTitle") != null)
                        if (xmldoc.SelectSingleNode("//oldUser/LandingPageTitle").InnerText.Trim() != "")
                            LandingPageText = xmldoc.SelectSingleNode("//oldUser/LandingPageTitle").InnerText.Replace("&amp;", "&");

                    if (xmldoc.SelectSingleNode("//oldUser/LandingPageLink") != null)
                        if (xmldoc.SelectSingleNode("//oldUser/LandingPageLink").InnerText.Trim() != "")
                            LandingPageLink = xmldoc.SelectSingleNode("//oldUser/LandingPageLink").InnerText.Replace("&amp;", "&");

                    if (LandingPageText != "" && LandingPageLink != "" && lst.IndexOf(new ListItem(obj.GetTranslatedText(LandingPageText), LandingPageLink)) > -1)
                    {
                        drpPageList.Items.FindByValue(LandingPageLink).Selected = true;
                    }
                    else
                    {
                        if (lst.IndexOf(new ListItem(obj.GetTranslatedText("Home"), "settingselect2.aspx")) > -1)
                            drpPageList.Items.FindByValue("settingselect2.aspx").Selected = true;
                        else if (lst.IndexOf(new ListItem(obj.GetTranslatedText("Express Form"), "ExpressConference.aspx?t=n&op=1")) > -1)
                            drpPageList.Items.FindByValue("ExpressConference.aspx?t=n&op=1").Selected = true;
                    }

                    XmlNodeList roles = xmldoc.SelectNodes("//oldUser/roles/role");
                    Session["rolesLanding"] = roles;
                    
                    // var bookA = newBooks.Where(x => x.Attributes["id"].Value == 123);
                    // ZD 100172 Ends 

                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                //ZD 100263
                //if (txtUserID.Text.Equals(Session["userID"].ToString()))
                if (Session["ModifiedUserID"].ToString().Equals(Session["userID"].ToString()))
                    SetUserProfile();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
               // errLabel.Text = "GetUserProfile: " + ex.Message + " : " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GetUserProfile:" + ex.Message);//ZD 100263
                log.Trace(ex.StackTrace);
            }
        }

        protected void SetUserProfile()
        {
            try
            {
                txtTotalTime.Enabled = false;//ZD 102043
                //FB 1318 - Start
                if (txtLevel.Value == "2" && Session["UsrCrossAccess"].ToString() == "1")
                    txtAccountExpiry.Enabled = true;
                else
                    txtAccountExpiry.Enabled = false;
                //FB 1318 - End
                lstDepartment.Enabled = false;
                lstUserRole.Enabled = false;
                //ZD 100263 - Start
                if (Session["roomCascadingControl"] != null)
                {
                    if (Session["roomCascadingControl"].ToString().Equals("1"))
                    {
                        lstUserRole.Visible = false;
                        tdUsrRole.Attributes.Add("Style", "visibility:hidden");
                        tdDrpDownUsrRole.Attributes.Add("Style", "visibility:hidden");
                        //ZD 100263 - Phase 2 Starts
                        lblTimeRemaining.Visible = false;//ZD 102043
                        txtTotalTime.Visible = false;//ZD 102043
                        tdScheduleMin.Attributes.Add("Style", "visibility:hidden");
                        tdTimeRm.Attributes.Add("Style", "visibility:hidden");

                        if (!(txtLevel.Value == "2"))
                        {
                            trUser.Attributes.Add("Style", "visibility:hidden");

                            LblMob.Visible = false;
                            DrpMob.Attributes.Add("Style", "visibility:hidden");

                            tdDrpMob.Attributes.Add("Style", "visibility:hidden");
                            tdLblMob.Attributes.Add("Style", "visibility:hidden");

                            DrpPIMNotifications.Visible = false;
                            tdLblPIM.Attributes.Add("Style", "visibility:hidden");
                            tdDrpPIM.Attributes.Add("Style", "visibility:hidden");
                            
                            if ((txtLevel.Value == "3") || (txtLevel.Value == "1"))
                                EnableTR.Attributes.Add("Style", "visibility:hidden");
                        }
                        else
                            EnableTR.Attributes.Add("Style", "visibility:hidden");

                       
                        //ZD 100263 - Phase 2 Ends
                    }
                }                
                //ZD 100263 - End
                //ZD 103686 - Commented Set SelectedIndex is zero
                //DrpEnableAV.SelectedIndex = 0;//For AV Switch
                DrpEnableAV.Enabled = false;//For AV Switch
                //DrpEnvPar.SelectedIndex = 0;//For AV Switch
                DrpEnvPar.Enabled = false;//For AV Switch
                //ZD 101122
                //DrpAVWO.SelectedIndex = 0;
                DrpAVWO.Enabled = false;
               // DrpCateringWO.SelectedIndex = 0;
                DrpCateringWO.Enabled = false;
                //DrpFacilityWO.SelectedIndex = 0;
                DrpFacilityWO.Enabled = false;

                lblHeader.Text = obj.GetTranslatedText("Account Settings");//FB 1830 - Translation

                if (Session["organizationID"] != null && Session["UsrCrossAccess"] != null) //FB 1598
                {
                    if (Session["UsrCrossAccess"].ToString().Equals("1") && !Session["organizationID"].ToString().Equals("11"))
                    {
                        lstDefaultGroup.Enabled = false;
                        lstSearchTemplate.Enabled = false;
                        lstBridges.Enabled = false;
                        //roomimage.Attributes.Add("style", "display:none;");
                        roomimage.Visible = false; //FB 1598

                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        protected void LoadEndpointDetails()
        {
            try
            {
                Session.Add("EPPW", ""); //FB 3054
                //Response.Write("EndpointDetaails");
                String inXML = "<EndpointDetails>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                //ZD 100263
                //inXML += "  <UserID>" + txtUserID.Text + "</UserID>";
                inXML += "  <UserID>" + Session["ModifiedUserID"].ToString() + "</UserID>";
                inXML += "  <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                inXML += "  <EntityType>U</EntityType>";
                inXML += "</EndpointDetails>";
                String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    txtEndpointName.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ProfileName").InnerText;
                    //FB 3054 Starts
                    Session.Remove("EPPW");
                    //txtEPPassword1.Attributes.Add("value", ns_MyVRMNet.vrmPassword.UserProfile);
                    //txtEPPassword2.Attributes.Add("value", ns_MyVRMNet.vrmPassword.UserProfile);
                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Password") != null)
                        Session["EPPW"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Password").InnerText.Trim();
                    //txtEPPassword1.Attributes.Add("value", xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Password").InnerText);
                    //txtEPPassword2.Attributes.Add("value", xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Password").InnerText);
                    //FB 3054 Ends
                    txtAddress.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Address").InnerText;
                    txtWebAccessURL.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/URL").InnerText;
                    txtAssociateMCUAddress.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/MCUAddress").InnerText;
                    txtExchangeID.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ExchangeID").InnerText; //Cisco Telepresence fix
                    txtApiportno.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ApiPortno").InnerText;//API Port...
                    // Code changed for FB 1713 - Start
                    txtConfCode.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/conferenceCode").InnerText;
                    txtLeaderPin.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/leaderPin").InnerText;
                    int lineRateVal = 0;
                    String txtTemp = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/LineRate").InnerText;
                    int.TryParse(txtTemp, out lineRateVal);

                    if (lineRateVal < 1)
                        txtTemp = "384";

                    // Code changed for FB 1713 - End
                    try
                    {
                        lstLineRate.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    /* Protocol is missing */
                    try
                    {
                        lstProtocol.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/DefaultProtocol").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Connection Type
                    try
                    {
                        lstConnectionType.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ConnectionType").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Address Type
                    try
                    {
                        lstAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/AddressType").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Video Equipment
                    try
                    {
                        lstVideoEquipment.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/VideoEquipment").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Bridge ID
                    try
                    {
                        lstBridges.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Bridge").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Is outside Network
                    try
                    {
                        lstIsOutsideNetwork.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/IsOutside").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstMCUAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/MCUAddressType").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstEncryption.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/EncryptionPreferred").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //ZF 104821 Starts
                    int BrdgeID = 0, BridgeType = 0;
                    SysLocation.Visible = false;
                    lstSystemLocation.Visible = false;
                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/BridgeType") != null)
                        int.TryParse(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/BridgeType").InnerText, out BridgeType);
                    if (!lstBridges.SelectedValue.Equals("-1"))
                    {
                        int.TryParse(lstBridges.SelectedValue, out BrdgeID);
                        if (BridgeType == 16)
                        {
                            SysLocation.Visible = true;
                            lstSystemLocation.Visible = true;
                            if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/SysLocation") != null)
                                txtSysLoc.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/SysLocation").InnerText;
                            BindSystemLocation(BrdgeID, BridgeType, lstSystemLocation);
                            lstSystemLocation.Items.FindByValue(txtSysLoc.Text).Selected = true;
                        }
                    }
                    //ZDF 104821 Ends
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadEndpointDetails:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region SubmitUser
        // FB 2025 Start
        protected void SubmitUser(Object sender, EventArgs e)
        {
            int ChangeVal = 1;
            Submit(ChangeVal);
        }
        protected void SubmitNewUser(object Sender, EventArgs e)
        {
            int ChangeVal = 2;
            Submit(ChangeVal);
        }
        // FB 2025 Ends
        protected void Submit(int ChangeVal)
        {
           // int ChangeVal = ChangeVal;
            int exc = 0, isBJNConf = 0;//ZD 100890
            try
            {
                if (Page.IsValid)
                {
                    //txtUserLogin.Text = txtUserFirstName.Text; //Org fixes
                    //FB 2027 Start
                    //String inXML = "";
                    StringBuilder inXML = new StringBuilder();
                    inXML.Append("<saveUser>");
                    inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                    inXML.Append("<login>");
                    inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                    inXML.Append("</login>");
                    inXML.Append("<user>");
                    //ZD 100263
                    //inXML.Append("<userID>" + txtUserID.Text + "</userID>");
                    inXML.Append("<userID>" + Session["ModifiedUserID"].ToString() + "</userID>");
                    inXML.Append("<userName>");
                    inXML.Append("<firstName>" + txtUserFirstName.Text + "</firstName>");
                    inXML.Append("<lastName>" + txtUserLastName.Text + "</lastName>");
                    inXML.Append("</userName>");
                    inXML.Append("<ExchangeID>" + txtEndpointID.Text + "</ExchangeID>"); //Cisco Telepresence fix
                    inXML.Append("<login>" + txtUserLogin.Text.Trim() + "</login>");//FB 1921
                    inXML.Append("<UserDomain>" + txtUserDomain.Text + "</UserDomain>"); //ZD 104850
                    //FB 3054 Starts
                    if (hdnPasschange.Value != "" && hdnPasschange.Value == "true")
                    {
                        inXML.Append("<password>" + UserPassword(txtPassword1.Text) + "</password>");
                        inXML.Append("<isPwdChanged>1</isPwdChanged>"); //ZD 100781
                    }
                    else
                        inXML.Append("<password>" + Session["userPW"].ToString() + "</password>"); 
                    //FB 3054 Ends
                    inXML.Append("<userEmail>" + txtUserEmail.Text + "</userEmail>");
                    inXML.Append("<alternativeEmail>" + txtUserEmail2.Text + "</alternativeEmail>");
                    inXML.Append("<workPhone>" + txtWorkPhone.Text.Trim() + "</workPhone>");//FB 1642-Audio Add on
                    inXML.Append("<cellPhone>" + txtCellPhone.Text.Trim() + "</cellPhone>");//FB 1642-Audio Add on
                    inXML.Append("<sendBoth>" + lstSendBoth.SelectedValue + "</sendBoth>");
                    inXML.Append("<emailClient>" + lstAddressBook.SelectedValue + "</emailClient>");
                    inXML.Append("<SavedSearch>" + lstSearchTemplate.SelectedValue + "</SavedSearch>");
                    //ZD 100263
                    //if (txtUserID.Text.Equals(Session["userID"].ToString()))
                    if (Session["ModifiedUserID"].ToString().Equals(Session["userID"].ToString()))
                        Session["SearchID"] = lstSearchTemplate.SelectedValue;
                    inXML.Append("<userInterface>2</userInterface>");
                    inXML.Append("<timeZone>" + lstTimeZone.SelectedValue + "</timeZone>");
                    //inXML += "    <location>" + hdnLocation.Value + "</location>";//Code  added for room search
                    inXML.Append("<location>" + selectedloc.Value + "</location>");//Code  added for room search
                    //Code Added For Date Format by Offshore -  FB IssueNo 1073- Start
                    inXML.Append("<dateFormat>" + DrpDateFormat.SelectedValue + "</dateFormat>");
                    //Code Added For Date Format  by Offshore -  FB IssueNo 1073 -End
                    //Code added for AV Switch FB 1426,1425,1429 -- Start
                    if (isAVEnabled)
                        inXML.Append("<enableAV>" + DrpEnableAV.SelectedValue + "</enableAV>");
                    else
                        inXML.Append("<enableAV>" + enableAV + "</enableAV>");

                    if (isParEnabled)
                        inXML.Append("<enableParticipants>" + DrpEnvPar.SelectedValue + "</enableParticipants>");
                    else
                        inXML.Append("<enableParticipants>" + enablePar + "</enableParticipants>");

                    //ZD 101122 - Start
                    if (isAVWOEnabled)
                        inXML.Append("<enableAVWorkOrder>" + DrpAVWO.SelectedValue + "</enableAVWorkOrder>");
                    else
                        inXML.Append("<enableAVWorkOrder>" + enableAVWO + "</enableAVWorkOrder>");

                    if (isCateringWOEnabled)
                        inXML.Append("<enableCateringWO>" + DrpCateringWO.SelectedValue + "</enableCateringWO>");
                    else
                        inXML.Append("<enableCateringWO>" + enableCatWO + "</enableCateringWO>");

                    if (isFacilityWOEnabled)
                        inXML.Append("<enableFacilityWO>" + DrpFacilityWO.SelectedValue + "</enableFacilityWO>");
                    else
                        inXML.Append("<enableFacilityWO>" + enableFacWO + "</enableFacilityWO>");
                    //ZD 101122 - End
                    
                    //ZD 101093 START
                    inXML.Append("<enableAdditionalOption>" + drpadditionaloption.SelectedValue + "</enableAdditionalOption>");
                    //ZD 101093 END
                    inXML.Append("<timeFormat>" + lstTimeFormat.SelectedValue + "</timeFormat>");
                    inXML.Append("<timezoneDisplay>" + lstTimeZoneDisplay.SelectedValue + "</timezoneDisplay>");

                    //Code added for AV Switch -- End
                    //code added for ticker -Start
                    inXML.Append("<tickerStatus>" + drpTickerStatus.SelectedValue + "</tickerStatus>");
                    inXML.Append("<tickerPosition>" + drpTickerPosition.SelectedValue + "</tickerPosition>");
                    inXML.Append("<tickerSpeed>" + drpTickerSpeed.SelectedValue + "</tickerSpeed>");
                    inXML.Append("<tickerBackground>" + txtTickerBknd.Text.Trim() + "</tickerBackground>");
                    if (drpTickerStatus.SelectedValue == "0")
                    {
                        if (txtTickerBknd.Text.Trim() == "")
                        {
                             errLabel.Text =obj.GetTranslatedText("Please select the ticker background color.");
                             errLabel.Visible = true;
                             exc = 1;
                             throw new Exception(errLabel.Text);

                        }
                    }
                    inXML.Append("<tickerDisplay>" + drpTickerDisplay.SelectedValue + "</tickerDisplay>");

                    inXML.Append("<rssFeedLink>" + txtFeedLink.Text + "</rssFeedLink>");
                    if (drpTickerDisplay.SelectedValue == "1")
                    {
                        if (txtFeedLink.Text == "")
                        {
                            errLabel.Text = obj.GetTranslatedText("Please enter the RSS Feed link.");
                            errLabel.Visible = true;
                            exc = 1;
                            throw new Exception(errLabel.Text);
                        }
                    }
                    inXML.Append("<tickerStatus1>" + drpTickerStatus1.SelectedValue + "</tickerStatus1>");
                    inXML.Append("<tickerPosition1>" + drpTickerPosition1.SelectedValue + "</tickerPosition1>");
                    inXML.Append("<tickerSpeed1>" + drpTickerSpeed1.SelectedValue + "</tickerSpeed1>");
                    inXML.Append("<tickerBackground1>" + txtTickerBknd1.Text.Trim() + "</tickerBackground1>");
                    if (drpTickerStatus1.SelectedValue == "0")
                    {
                        if (txtTickerBknd1.Text.Trim() == "")
                        {
                            errLabel.Text =obj.GetTranslatedText("Please select the ticker background color.");
                            errLabel.Visible = true;
                            exc = 1;
                            throw new Exception(errLabel.Text);

                        }
                    }
                    inXML.Append("<tickerDisplay1>" + drpTickerDisplay1.SelectedValue + "</tickerDisplay1>");

                    inXML.Append("<rssFeedLink1>" + txtFeedLink1.Text + "</rssFeedLink1>");

                    if (drpTickerDisplay1.SelectedValue == "1")
                    {
                        if (txtFeedLink1.Text == "")
                        {
                            errLabel.Text =obj.GetTranslatedText("Please enter the RSS Feed link.");
                            errLabel.Visible = true;
                            exc = 1;
                            throw new Exception(errLabel.Text);
                        }
                    }

                    if (drpTickerStatus.SelectedValue == "0" || drpTickerStatus1.SelectedValue == "0")
                    {

                        if (txtTickerBknd.Text != "" && txtTickerBknd1.Text != "")
                            if (txtTickerBknd.Text == txtTickerBknd1.Text)
                            {
                                errLabel.Text =obj.GetTranslatedText("Both Tickers cannot have the same background color.");
                                errLabel.Visible = true;
                                exc = 1;
                                throw new Exception(errLabel.Text);
                            }
                    }

                    //code added for Ticker --End
                    string confcode = txtConfCode.Text.Trim(); //FB 1642-Audio add on start
                    string leaderPin = txtLeaderPin.Text.Trim();

                    confcode = confcode.Replace("D", "");
                    confcode = confcode.Replace("+", "");

                    leaderPin = leaderPin.Replace("D", "");
                    leaderPin = leaderPin.Replace("+", ""); //FB 1642-Audio add on end

                    inXML.Append("<lineRateID>" + lstLineRate.SelectedValue + "</lineRateID>");
                    inXML.Append("<videoProtocol>" + lstProtocol.SelectedValue + "</videoProtocol>");
                    inXML.Append("<conferenceCode>" + confcode + "</conferenceCode>");//FB 1642-Audio add on
                    inXML.Append("<leaderPin>" + leaderPin + "</leaderPin>");//FB 1642-Audio add on
                    inXML.Append("<participantCode>" + txtPartyCode.Text.Trim() + "</participantCode>");//ZD 101446
                    inXML.Append("<IPISDNAddress>" + txtAddress.Text + "</IPISDNAddress>");
                    inXML.Append("<connectionType>" + lstConnectionType.SelectedValue + "</connectionType>");
                    inXML.Append("<videoEquipmentID>" + lstVideoEquipment.SelectedValue + "</videoEquipmentID>");
                    inXML.Append("<isOutside>" + lstIsOutsideNetwork.SelectedValue + "</isOutside>");
                    inXML.Append("<addressTypeID>" + lstAddressType.SelectedValue + "</addressTypeID>");
                    inXML.Append("<group>" + lstDefaultGroup.SelectedValue + "</group>");
                    inXML.Append("<ccGroup></ccGroup>");
                    inXML.Append("<roleID>" + lstUserRole.SelectedValue + "</roleID>");
					//ZD 102043 Starts
                    if (enableCloudInstallation == 1 && txtTotalTime.Text == "")//FB 2659
                    {
                        txtTotalTime.Text = "100000";
                    }
                    inXML.Append("<initialTime>" + txtTotalTime.Text + "</initialTime>");
					//ZD 102043 Ens
                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //inXML += "    <expiryDate>" + txtAccountExpiry.Text + "</expiryDate>";

                    //ZD 100164 START - TDB - New User Creation
                    if (enableCloudInstallation == 1 && enableAdvancedUserOption == 1 && Session["UsrCrossAccess"].ToString() != "1")
                    {
                        inXML.Append("<expiryDate>2030-06-24 00:00:00.000</expiryDate>");

                    }
                    //ZD 100164 END - TDB - New User Creation
                    
                    inXML.Append("<expiryDate>" + myVRMNet.NETFunctions.GetDefaultDate(txtAccountExpiry.Text) + "</expiryDate>");
                    //Code changed by Offshore for FB Issue 1073 -- End
                    inXML.Append("<bridgeID>" + lstBridges.SelectedValue + "</bridgeID>");
                    //FB 1830 - Start
                    inXML.Append("<languageID>" + lstLanguage.SelectedValue + " </languageID>");
                    if (Session["UsrEmailLangID"] != null)//FB 2283
                        inXML.Append("<EmailLang>" + Session["UsrEmailLangID"].ToString() + " </EmailLang>");
                    else
                        inXML.Append("<EmailLang></EmailLang>");
                    //FB 1830 - ENd
                    inXML.Append("<departments>");
                    //Response.Write(lstAdmin.Items[lstUserRole.SelectedIndex].Text);
                    if (lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("2") || lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("3")) //ZD 101764
                        foreach (ListItem li in lstDepartment.Items)
                            inXML.Append("<id>" + li.Value + "</id>");
                    else
                        foreach (ListItem li in lstDepartment.Items)
                        {
                            if (li.Selected.Equals(true))
                                inXML.Append("<id>" + li.Value + "</id>");
                        }
                    inXML.Append("</departments>");
                    inXML.Append("<status>");
                    inXML.Append("<deleted>0</deleted>");
                    inXML.Append("<locked>0</locked>");
                    inXML.Append("</status>");
                    if (lstAddressBook.SelectedValue.Equals("2"))
                    {
                        inXML.Append("<lotus>");
                        inXML.Append("<lnLoginName>" + txtLotusLoginName.Text + "</lnLoginName>");
                        inXML.Append("<lnLoginPwd>" + txtLotusPassword.Text + "</lnLoginPwd>");
                        inXML.Append("<lnDBPath>" + txtLotusDBPath.Text + "</lnDBPath>");
                        inXML.Append("</lotus>");
                    }
                    inXML.Append("<creditCard>0</creditCard>");
                    //FB 2227 Start
                    inXML.Append("<IntVideoNum>" + txtIntvideonum.Text + "</IntVideoNum>");
                    inXML.Append("<ExtVideoNum>" + txtExtvideonum.Text + "</ExtVideoNum>");
                    //FB 2227 End
                    inXML.Append("</user>");
                    inXML.Append("<emailMask>" + lstEmailNotification.SelectedValue + "</emailMask>");
                    //Code Added for License modification START
                    inXML.Append("<exchangeUser>" + DrpExc.SelectedValue + "</exchangeUser>");
                    inXML.Append("<dominoUser>" + DrpDom.SelectedValue + "</dominoUser>");
                    inXML.Append("<mobileUser>" + DrpMob.SelectedValue + "</mobileUser>"); //FB 1979
                    //Code Added for License modification END
                    inXML.Append("<PIMNotification>" + DrpPIMNotifications.SelectedValue + "</PIMNotification>");//FB 2164
                    //Audio Add on..
                    //if (chkAudioAddon.Checked) //FB 2023
                    //    inXML.Append("<audioaddon>1</audioaddon>");
                    //else
                    inXML.Append("<audioaddon>0</audioaddon>");
                    
                    inXML.Append("<HelpRequsetorEmail></HelpRequsetorEmail>"); //FB 2268
                    inXML.Append("<HelpRequsetorPhone></HelpRequsetorPhone>");//FB 2268
                    inXML.Append("<RFIDValue>" + txtRFIDValue.Text + "</RFIDValue>");//FB 2724
                    inXML.Append("<SendSurveyEmail>" + drpSurveyEmail.SelectedValue + "</SendSurveyEmail>");//FB 2348

                    //FB 2608 Start
                    int EnableVNOCselectionVal = 0;                    
                    //FB 2670  starts
                    if (lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("2") || (lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("3")))
                    {
                        if (chkVNOC.Checked == true)
                            EnableVNOCselectionVal = 1;
                        else
                            EnableVNOCselectionVal = 0;
                    }                  

                    //FB 2670 End
                   inXML.Append("<EnableVNOCselection>" + EnableVNOCselectionVal + "</EnableVNOCselection>");
                    //FB 2608 End
                    //FB 2595 Start
                    //if(chkSecured.Checked)
                    //    inXML.Append("<Secured>1</Secured>");
                    //else
                    //    inXML.Append("<Secured>0</Secured>");
                    //FB 2595 End
                    inXML.Append("<PrivateVMR>" + utilObj.ReplaceInXMLSpecialCharacters(PrivateVMR.Html) + "</PrivateVMR>");//FB 2481 //FB 2721
                    //FB 2599 Starts
                    //FB 2262
                    inXML.Append("<VidyoURL>" + txtVidyoURL.Text + "</VidyoURL>");
                    inXML.Append("<Pin>" + txtPin.Text + "</Pin>");
                    inXML.Append("<Extension>" + txtExtension.Text + "</Extension>");
                    //FB 2599 End

                    //FB 2594 Starts
                    if (Session["EnablePublicRooms"].ToString() == "1")
                    {
                        //FB 2392-Whygo Starts
                        inXML.Append("<ParentReseller>" + txtParentReseller.Value + "</ParentReseller>");
                        inXML.Append("<CorpUser>" + txtCorpUser.Value + "</CorpUser>");
                        inXML.Append("<Region>" + lstRegion.SelectedValue.Trim() + "</Region>");
                        inXML.Append("<ESUserID>" + txtESUserID.Value.Trim() + "</ESUserID>");
                        inXML.Append("<BrokerRoomNum>" + txtBrokerNum.Value + "</BrokerRoomNum>"); //FB 3001
                        //FB 2392-Whygo End
                    }
                    else
                    {
                        inXML.Append("<ParentReseller></ParentReseller>");
                        inXML.Append("<CorpUser></CorpUser>");
                        inXML.Append("<Region></Region>");
                        inXML.Append("<ESUserID></ESUserID>");
                        inXML.Append("<BrokerRoomNum></BrokerRoomNum>"); //FB 3001
                    }
                    //FB 2594 Ends
                    //FB 2693 Starts
                    int isPCUser = 0;
                    if (chkPcConf.Checked)
                        isPCUser = 1;
                    inXML.Append("<EnablePCUser>" + isPCUser + "</EnablePCUser>");
                    Session["EnablePCUser"] = isPCUser;//ZD 100873
                    

                    string landingTitle = "", landingLink = ""; // ZD 100172
                    string ModifiedUserID = Session["ModifiedUserID"].ToString();
                    if (ModifiedUserID != null)
                    {
                        if (!ModifiedUserID.ToString().ToLower().Equals("new") && enableCloudInstallation == 0) //ZD 100969
                        {
                            landingTitle = drpPageList.SelectedItem.Text.Replace("&", "&amp;");
                            landingLink = drpPageList.SelectedItem.Value.Replace("&", "&amp;");
                        }
                    }
                    inXML.Append("<LandingPageTitle>" + landingTitle + "</LandingPageTitle>");
                    inXML.Append("<LandingPageLink>" + landingLink + "</LandingPageLink>");

                    if (isPCUser > 0)
                    {
                        inXML.Append("<PCDetails>");

                        //ZD 104021
                        /*#region BlueJeans
                        if (chkBJ.Checked)
                        {
                            inXML.Append("<PCDetail>");
                            inXML.Append("<PCId>" + ns_MyVRMNet.vrmPC.BlueJeans + "</PCId>");
                            inXML.Append("<Description>" + txtAreaBlueJeans.Value + "</Description>");
                            inXML.Append("<SkypeURL>" + txtSkypeBJ.Value + "</SkypeURL>");
                            inXML.Append("<VCDialinIP>" + txtDialinIP.Value + "</VCDialinIP>");
                            inXML.Append("<VCMeetingID>" + txtMeetingVC.Value + "</VCMeetingID>");
                            inXML.Append("<VCDialinSIP></VCDialinSIP>");
                            inXML.Append("<VCDialinH323></VCDialinH323>");
                            inXML.Append("<VCPin></VCPin>");
                            inXML.Append("<PCDialinNum>" + txtTollBJ.Value + "</PCDialinNum>");
                            inXML.Append("<PCDialinFreeNum>" + txtTollFreeBJ.Value + "</PCDialinFreeNum>");
                            inXML.Append("<PCMeetingID>" + txtMeetingPC.Value + "</PCMeetingID>");
                            inXML.Append("<PCPin></PCPin>");
                            inXML.Append("<Intructions>" + txtDetail.Value + "</Intructions>");
                            inXML.Append("</PCDetail>");
                        }
                        #endregion
                         */

                        if (chkJB.Checked)
                        {
                            inXML.Append("<PCDetail>");
                            inXML.Append("<PCId>" + ns_MyVRMNet.vrmPC.Jabber + "</PCId>");
                            inXML.Append("<Description>" + txtAreaJabber.Value + "</Description>");
                            inXML.Append("<SkypeURL>" + txtSkypeJB.Value + "</SkypeURL>");
                            inXML.Append("<VCDialinIP>" + txtDialinIPJB.Value + "</VCDialinIP>");
                            inXML.Append("<VCMeetingID>" + txtMeetingVCJB.Value + "</VCMeetingID>");
                            inXML.Append("<VCDialinSIP></VCDialinSIP>");
                            inXML.Append("<VCDialinH323></VCDialinH323>");
                            inXML.Append("<VCPin></VCPin>");
                            inXML.Append("<PCDialinNum>" + txtTollJB.Value + "</PCDialinNum>");
                            inXML.Append("<PCDialinFreeNum>" + txtTollFreeJB.Value + "</PCDialinFreeNum>");
                            inXML.Append("<PCMeetingID>" + txtMeetingPCJB.Value + "</PCMeetingID>");
                            inXML.Append("<PCPin></PCPin>");
                            inXML.Append("<Intructions>" + txtDetailJB.Value + "</Intructions>");
                            inXML.Append("</PCDetail>");
                        }
                        if (chkLync.Checked)
                        {
                            inXML.Append("<PCDetail>");
                            inXML.Append("<PCId>" + ns_MyVRMNet.vrmPC.Lync + "</PCId>");
                            inXML.Append("<Description>" + txtAreaLync.Value + "</Description>");
                            inXML.Append("<SkypeURL>" + txtSkypeLync.Value + "</SkypeURL>");
                            inXML.Append("<VCDialinIP>" + txtDialinIPLync.Value + "</VCDialinIP>");
                            inXML.Append("<VCMeetingID>" + txtMeetingVCLync.Value + "</VCMeetingID>");
                            inXML.Append("<VCDialinSIP></VCDialinSIP>");
                            inXML.Append("<VCDialinH323></VCDialinH323>");
                            inXML.Append("<VCPin></VCPin>");
                            inXML.Append("<PCDialinNum>" + txtTollLync.Value + "</PCDialinNum>");
                            inXML.Append("<PCDialinFreeNum>" + txtTollFreeLync.Value + "</PCDialinFreeNum>");
                            inXML.Append("<PCMeetingID>" + txtMeetingPCLync.Value + "</PCMeetingID>");
                            inXML.Append("<PCPin></PCPin>");
                            inXML.Append("<Intructions>" + txtDetailLync.Value + "</Intructions>");
                            inXML.Append("</PCDetail>");
                        }
                        //ZD 102004
                        /*
                        if (chkVidtel.Checked)
                        {
                            #region Vidtel
                            inXML.Append("<PCDetail>");
                            inXML.Append("<PCId>" + ns_MyVRMNet.vrmPC.Vidtel + "</PCId>");
                            inXML.Append("<Description>" + txtAreaVidtel.Value + "</Description>");
                            inXML.Append("<SkypeURL>" + txtSkypeVidtel.Value + "</SkypeURL>");
                            inXML.Append("<VCDialinIP></VCDialinIP>");
                            inXML.Append("<VCMeetingID></VCMeetingID>");
                            inXML.Append("<VCDialinSIP>" + txtDialinSIP.Value + "</VCDialinSIP>");
                            inXML.Append("<VCDialinH323>" + txtDialinH323.Value + "</VCDialinH323>");
                            inXML.Append("<VCPin>" + txtPinVc.Value + "</VCPin>");
                            inXML.Append("<PCDialinNum>" + txtTollVidtel.Value + "</PCDialinNum>");
                            inXML.Append("<PCDialinFreeNum>" + txtTollFreeVidtel.Value + "</PCDialinFreeNum>");
                            inXML.Append("<PCMeetingID></PCMeetingID>");
                            inXML.Append("<PCPin>" + txtPinPc.Value + "</PCPin>");
                            inXML.Append("<Intructions></Intructions>");
                            inXML.Append("</PCDetail>");
                            #endregion
                        }
                         * */
                        //if (chkVidyo.Checked)
                        //{
                        //    inXML.Append("<ExtVMR>");
                        //    inXML.Append("<PCId>" + ns_MyVRMNet.ExtVMR.Vidyo + "</PCId>");
                        //    inXML.Append("<Description>" + txtAreaVidyo.Value + "</Description>");
                        //    inXML.Append("<SkypeURL></SkypeURL>");
                        //    inXML.Append("<VCDialinSIP></VCDialinSIP>");
                        //    inXML.Append("<VCDialinH323></VCDialinH323>");
                        //    inXML.Append("<VCPin></VCPin>");
                        //    inXML.Append("<VCDialinIP></VCDialinIP>");
                        //    inXML.Append("<VCMeetingID></VCMeetingID>");
                        //    inXML.Append("<PCDialinNum></PCDialinNum>");
                        //    inXML.Append("<PCDialinFreeNum></PCDialinFreeNum>");
                        //    inXML.Append("<PCMeetingID></PCMeetingID>");
                        //    inXML.Append("<PCPin></PCPin>");
                        //    inXML.Append("<Intructions></Intructions>");
                        //    inXML.Append("</ExtVMR>");
                        //}
                        inXML.Append("</PCDetails>");
                    }
                    else
                    {
                        inXML.Append("<PCDetails></PCDetails>");
                    }
                    //FB 2693 Ends
                    //inncrewin ZD100755 start
                    if (chkEnblStaticId.Checked)
                    {
                        inXML.Append("<IsStaticIDEnabled>1</IsStaticIDEnabled>");
                        inXML.Append("<StaticID>" + txtStaticBox.Text + "</StaticID>");
                    }
                    else
                    {
                        inXML.Append("<IsStaticIDEnabled>0</IsStaticIDEnabled>");
                        inXML.Append("<StaticID></StaticID>");
                    }
                    //ZD 100755 Inncrewin Starts

                    //ZD 100221 Starts
                    int isWebexConf = 0;
                    if (ChkWebexConf.Checked)
                        isWebexConf = 1;
                    inXML.Append("<EnableWebexConf>" + isWebexConf + "</EnableWebexConf>");
                    inXML.Append("<WebexUserName>" + txtWebExUserName.Text + "</WebexUserName>");
                    if (hdnWebEX.Value != "" && hdnWebEX.Value == "true")
                        inXML.Append("<WebexPassword>" + UserPassword(txtWebExPassword.Text) + "</WebexPassword>");
                    //ZD 100221 End
                    //ZD 103550
                    if (ChkBJNConf.Checked)
                        isBJNConf = 1;
                    inXML.Append("<EnableBJNConf>" + isBJNConf + "</EnableBJNConf>");
                    if (ChkBJNConf.Checked && string.IsNullOrEmpty(txtBJNUserName.Text)) //ZD 104116
                        txtBJNUserName.Text = txtUserEmail.Text;
                    inXML.Append("<BJNUserName>" + txtBJNUserName.Text + "</BJNUserName>");

                    inXML.Append("</saveUser>");
                    log.Trace(inXML.ToString());
                    inXML.Replace("&", "&amp;"); //FB 2262

                    string outXML = obj.CallMyVRMServer("SetUser", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    //String outXML = obj.CallCOM("SetUser", inXML, Application["COM_ConfigPath"].ToString());
                    //FB 2027 End
                    //Response.Write(obj.Transfer(outXML));
                    //Response.End();

                    Session.Remove("UserdeptXML");//Coded added for FB 1597
                    BindRoomToList();//Room Search

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;

                        //FB 2268 start
                        lstHelpReqEmail.Items.Clear();
                        String[] HelpReq = hdnHelpReq.Value.Split('�');
                        for (int i = 0; i < HelpReq.Length; i++)
                        {
                            if (HelpReq[i].Trim() != "")
                            {
                                ListItem item = new ListItem();
                                item.Text = HelpReq[i];
                                item.Attributes.Add("title", HelpReq[i]);
                                lstHelpReqEmail.Items.Add(item);
                            }
                        }
                        //FB 2268 end
                    }
                    else
                    {
                   
                        if (SetEndpointDetails(outXML))
                        {
                            //ZD 100890 Start
                            if (Session["organizationID"] != null && enableCloudInstallation == 1) //Need to verify chkEnblStaticId.Checked &&
                            {
                                string outRadXml = obj.CallCOM2("CreateUpdateUserOnRAD", outXML, Application["RTC_ConfigPath"].ToString());
                                if (outRadXml.ToLower().IndexOf("error") >= 0)
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outRadXml);
                                    errLabel.Visible = true;
                                    exc = 1;
                                    throw new Exception(errLabel.Text);
                                }
                            }
                            //ZD 100890 End
                            
                            //ZD 100263
                            //if (Session["userID"].ToString().Trim().Equals(txtUserID.Text.Trim()))
                            if (Session["userID"].ToString().Trim().Equals(Session["ModifiedUserID"].ToString().Trim()))
                            {
                                //Code added by offshore for FB Issue 1073 -- start
                                string dt = "0";
                                if (Session["FormatDateType"].ToString() != DrpDateFormat.SelectedValue)
                                    dt = "1";
                                Session.Remove("FormatDateType");
                                Session.Add("FormatDateType", DrpDateFormat.SelectedValue);
                                dt = "0";
                                if (Session["timeFormat"].ToString() != lstTimeFormat.SelectedValue)
                                    dt = "1";
                                Session.Remove("timeFormat");
                                Session.Add("timeFormat", lstTimeFormat.SelectedValue);
                                //Code Modified For FB 1425
                                dt = "0";
                                if (Session["timeZoneDisplay"].ToString() != lstTimeZoneDisplay.SelectedValue)
                                    dt = "1";
                                Session.Remove("timeZoneDisplay");
                                Session.Add("timeZoneDisplay", lstTimeZoneDisplay.SelectedValue);
                                //code for ticker --Start
                                Session.Remove("tickerStatus");
                                Session.Add("tickerStatus", drpTickerStatus.SelectedValue);
                                Session.Remove("tickerSpeed");
                                Session.Add("tickerSpeed", drpTickerSpeed.SelectedValue);
                                Session.Remove("tickerPosition");
                                Session.Add("tickerPosition", drpTickerPosition.SelectedValue);
                                if (Session["tickerDisplay"].ToString() != drpTickerDisplay.SelectedValue)
                                    dt = "1";
                                Session.Remove("tickerDisplay");
                                Session.Add("tickerDisplay", drpTickerDisplay.SelectedValue);
                                Session.Remove("tickerBackground");
                                Session.Add("tickerBackground", txtTickerBknd.Text.Trim());
                                Session.Remove("rssFeedLink");
                                Session.Add("rssFeedLink", txtFeedLink.Text);
                                Session.Remove("tickerStatus1");
                                Session.Add("tickerStatus1", drpTickerStatus1.SelectedValue);
                                Session.Remove("tickerSpeed1");
                                Session.Add("tickerSpeed1", drpTickerSpeed1.SelectedValue);
                                Session.Remove("tickerPosition1");
                                Session.Add("tickerPosition1", drpTickerPosition1.SelectedValue);
                                if (Session["tickerDisplay1"].ToString() != drpTickerDisplay1.SelectedValue)
                                    dt = "1";
                                Session.Remove("tickerDisplay1");
                                Session.Add("tickerDisplay1", drpTickerDisplay1.SelectedValue);
                                Session.Remove("tickerBackground11");
                                Session.Add("tickerBackground1", txtTickerBknd1.Text.Trim());
                                Session.Remove("rssFeedLink1");
                                Session.Add("rssFeedLink1", txtFeedLink1.Text);
                                Session.Remove("timezoneID"); //FB 2468
                                Session.Add("timezoneID", lstTimeZone.SelectedValue);

                                // FB 2608 Start
                                Session.Remove("EnableVNOCselection");
                                if (chkVNOC.Checked)
                                    Session.Add("EnableVNOCselection", 1);
                                else
                                    Session.Add("EnableVNOCselection", 0);
                                // FB 2608 End

                               


                                //FB 1830 Start
                                switch (Session["language"].ToString())
                                {
                                    case "en":
                                        if (lstLanguage.SelectedValue != "1" && lstLanguage.SelectedValue != "2")
                                            dt = "1";
                                        break;
                                    case "sp":
                                        if (lstLanguage.SelectedValue != "3")
                                            dt = "1";
                                        break;
                                    case "ch":
                                        if (lstLanguage.SelectedValue != "4")
                                            dt = "1";
                                        break;
                                    case "fr":
                                        if (lstLanguage.SelectedValue != "5")
                                            dt = "1";
                                        break;
                                   
                                }
                                Session.Remove("CurrencyFormat"); 
                                Session.Remove("NumberFormat"); 
                                if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
                                {
                                    Session.Add("CurrencyFormat", ns_MyVRMNet.vrmCurrencyFormat.bound);
                                    Session.Add("NumberFormat", ns_MyVRMNet.vrmNumberFormat.european);
                                }
                                else
                                {
                                    Session.Add("CurrencyFormat", ns_MyVRMNet.vrmCurrencyFormat.dollar);
                                    Session.Add("NumberFormat", ns_MyVRMNet.vrmNumberFormat.american);
                                   
                                }
                                //FB 1830 End
                                //code for ticker --end
                                //ZD 101093 Starts
                                Session.Remove("enableAdditionalOption");
                                Session.Add("enableAdditionalOption", drpadditionaloption.SelectedValue);
                                //ZD 101093 Ends

                                //ZD 102585 Starts
                                Session.Remove("IsStaticIDEnabled");
                                if (chkEnblStaticId.Checked)
                                    Session.Add("IsStaticIDEnabled", "1"); 
                                else
                                    Session.Add("IsStaticIDEnabled", "0");
                                //ZD 102585 End

                                //FB 2025 starts
                                if (ChangeVal == 1) 
                                    Response.Redirect("ManageUserProfile.aspx?m=1&dt=" + dt);
                                else 
                                    Response.Redirect("ManageUserProfile.aspx?m=1&dt=");
                                //FB 2025 Ends
                                
                            }
                            else
                            {
                                string dt = "0";
                                if (Session["FormatDateType"].ToString() != DrpDateFormat.SelectedValue)
                                    dt = "1";
                                //Session.Remove("FormatDateType"); //Commented for FB 1762
                                //Session.Add("FormatDateType", DrpDateFormat.SelectedValue); //Commented for FB 1762
                                dt = "0";
                                if (Session["timeFormat"].ToString() != lstTimeFormat.SelectedValue)
                                    dt = "1";
                                //Session.Remove("timeFormat"); //Commented for FB 1762
                                //Session.Add("timeFormat", lstTimeFormat.SelectedValue); //Commented for FB 1762
                                //Code Modified For FB 1425
                                dt = "0";
                                if (Session["timeZoneDisplay"].ToString() != lstTimeZoneDisplay.SelectedValue)
                                    dt = "1";
                                //Session.Remove("timeZoneDisplay"); //Commented for FB 1762
                                //Session.Add("timeZoneDisplay", lstTimeZoneDisplay.SelectedValue); //Commented for FB 1762

                                Session["UserToEdit"] = "";
                                //Code changed by offshore for FB Issue 1073 -- start
                                //Response.Redirect("ManageUser.aspx?t=1&m=1");
                                //FB 2025 starts
                                if (ChangeVal == 1) 
                                    Response.Redirect("ManageUser.aspx?t=1&m=1&dt=" + dt);
                                else //FB 2025
                                {
                                    Session.Add("UserToEdit", "new"); //FB 2025
                                    Response.Redirect("ManageUserProfile.aspx?t=1"); 
                                }
                                //FB 2025 ends
                                //Code changed by offshore for FB Issue 1073 -- end
                               
                            }
                        }
                    }
                }

            }
            catch (System.Threading.ThreadAbortException) { } // FB 1762
            catch (Exception ex)
            {
                errLabel.Visible = true;
                if (exc == 1) //ZD 100890
                    errLabel.Text = ex.Message;//ZD 100263
                else
                    errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region UpdateDepartments
        protected void UpdateDepartments(Object sender, EventArgs e)
        {
            try
            {
                if (lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("2") || lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("3")) //ZD 101764
                {
                    foreach (ListItem li in lstDepartment.Items)
                        li.Selected = true;
                    lstDepartment.Enabled = false;
                }
                else
                    lstDepartment.Enabled = true;

                //ZD 101122 - Start
                //Code added for AV switch
                DrpEnableAV.Enabled = true;
                DrpEnvPar.Enabled = true;
                DrpAVWO.Enabled = true;
                DrpCateringWO.Enabled = true;
                DrpFacilityWO.Enabled = true;
                if (!lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("0"))
                {
                    DrpEnableAV.Enabled = false;
                    DrpEnableAV.SelectedIndex = 0;

                    DrpEnvPar.Enabled = false;
                    DrpEnvPar.SelectedIndex = 0;

                    DrpAVWO.Enabled = false;
                    DrpAVWO.SelectedIndex = 0;

                    DrpCateringWO.Enabled = false;
                    DrpCateringWO.SelectedIndex = 0;

                    DrpFacilityWO.Enabled = false;
                    DrpFacilityWO.SelectedIndex = 0;
                }
                //ZD 101122 - End
                //ZD 103686
                DisplayConferenceOptions();

                if (!Session["ModifiedUserID"].ToString().ToLower().Equals("new")) // ZD 100172
                {
                    XmlNodeList rol = (XmlNodeList)Session["rolesLanding"];
                    foreach (XmlNode xn in rol)
                    {
                        if (xn.ChildNodes[0].InnerText == lstUserRole.SelectedItem.Value)
                        {
                            string menuMask = xn.ChildNodes[2].InnerText;
                            List<ListItem> lst = new List<ListItem>();
                            lst = (List<ListItem>)loginMgmt.FillAccessSpecifiers(menuMask, false);

                            lst.Sort(delegate(ListItem thisItem, ListItem otherItem)
                            {
                                return thisItem.Text.CompareTo(otherItem.Text);
                            });

                            drpPageList.DataSource = lst;
                            drpPageList.DataBind();

                            if (lst.IndexOf(new ListItem(obj.GetTranslatedText("Home"), "settingselect2.aspx")) > -1)
                                drpPageList.Items.FindByValue("settingselect2.aspx").Selected = true;
                            else if (lst.IndexOf(new ListItem(obj.GetTranslatedText("Express Form"), "ExpressConference.aspx?t=n&op=1")) > -1)
                                drpPageList.Items.FindByValue("ExpressConference.aspx?t=n&op=1").Selected = true;

                            break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SetEndpointDetails
        protected bool SetEndpointDetails(String outXML)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                //ZD 100263
                Session["ModifiedUserID"] = xmldoc.SelectSingleNode("//SetUser/userID").InnerText;
                //txtUserID.Text = xmldoc.SelectSingleNode("//SetUser/userID").InnerText;
                String inXML = GenerateInxml();
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                outXML = obj.CallMyVRMServer("SetEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {

                    //FB 1860
                    inXML = "  <login>";
                    //ZD 100263
                    inXML += "<userID>" + Session["ModifiedUserID"].ToString() + "</userID>";
                    //inXML += "<userID>" + txtUserID.Text + "</userID>";
                    
                    if (ChkBlockEmails.Checked)
                        inXML += "<MailBlocked>1</MailBlocked>";
                    else
                        inXML += "<MailBlocked>0</MailBlocked>";
                    inXML += "  </login>";

                    outXML = obj.CallCommand("SetUserEmailsBlockStatus", inXML);

                    if (outXML.IndexOf("<error>") < 0)
                        return true;

                    //FB 1860

                   


                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region GenerateInxml
        protected String GenerateInxml()
        {
            try
            {
                // Code changed for FB 1713 - Start
                int lineRate = 0;
                int.TryParse(lstLineRate.SelectedValue, out lineRate);
                string confcode = txtConfCode.Text.Trim(); //FB 1642-Audio add on start
                string leaderPin = txtLeaderPin.Text.Trim();

                confcode = confcode.Replace("D", "");
                confcode = confcode.Replace("+", "");

                leaderPin = leaderPin.Replace("D", "");
                leaderPin = leaderPin.Replace("+", ""); //FB 1642-Audio add on end

                if (lineRate < 1)
                    lineRate = 384;
                // Code changed for FB 1713 - End

                if (txtEndpointID.Text.Trim().Equals(""))
                    txtEndpointID.Text = "new";

                String inXML = "<SetEndpoint>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "      <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                inXML += "      <EndpointName>" + txtUserFirstName.Text + " " + txtUserLastName.Text + "</EndpointName>";
                inXML += "      <EntityType>U</EntityType>";
                inXML += "      <audioaddon>0</audioaddon>"; //ALLDEV-814
                //ZD 100263
                inXML += "<UserID>" + Session["ModifiedUserID"].ToString() + "</UserID>";
                //inXML += "<UserID>" + txtUserID.Text + "</UserID>";
                inXML += "      <Profiles>";
                inXML += "        <Profile>";
                inXML += "          <ProfileID>1</ProfileID>"; //ALLDEV-814

                //ZD 102863 - Start
                if (txtEndpointName.Text.Trim().Equals(""))
                {
                    string str = txtUserFirstName.Text + " " + txtUserLastName.Text;     

                    str = str.Replace("'", "").Replace("\"", "").Replace("^", "").Replace(";", "").Replace(":", "");
                    
                    txtEndpointName.Text = str;
                }
                //ZD 102863 - End

                inXML += "          <ProfileName>" + txtEndpointName.Text + "</ProfileName>";
                inXML += "          <Default>0</Default>";
                inXML += "          <EncryptionPreferred>" + lstEncryption.SelectedValue + "</EncryptionPreferred>";
                inXML += "          <AddressType>" + lstAddressType.SelectedValue + "</AddressType>";
                //FB 3054 Starts
                if (Session["EPPW"] == null)
                    Session["EPPW"] = "";
                if (hdnEPPWD.Value != "" && hdnEPPWD.Value == "true")
                    inXML += "          <Password>" + UserPassword(txtEPPassword1.Text) + "</Password>"; //FB 3054
                else
                    inXML += "          <Password>" + Session["EPPW"].ToString() + "</Password>"; //FB 3054
                //FB 3054 Ends
                inXML += "          <Address>" + txtAddress.Text + "</Address>";
                inXML += "          <URL>" + txtWebAccessURL.Text + "</URL>";
                inXML += "          <IsOutside>" + lstIsOutsideNetwork.SelectedValue + "</IsOutside>";
                inXML += "          <ConnectionType>" + lstConnectionType.SelectedValue + "</ConnectionType>";
                inXML += "          <VideoEquipment>" + lstVideoEquipment.SelectedValue + "</VideoEquipment>";
                inXML += "          <LineRate>" + lineRate + "</LineRate>";
                inXML += "          <Bridge>" + lstBridges.SelectedValue + "</Bridge>";
                inXML += "          <MCUAddress>" + txtAssociateMCUAddress.Text + "</MCUAddress>";
                //API Port Starts...
                if (txtApiportno.Text != "")
                    inXML += "          <ApiPortno>" + txtApiportno.Text + "</ApiPortno>";
                else
                    inXML += "          <ApiPortno>23</ApiPortno>";
                //API Port Ends...
                inXML += "          <DefaultProtocol>" + lstProtocol.SelectedValue + "</DefaultProtocol>";
                inXML += "          <MCUAddressType>" + lstMCUAddressType.SelectedValue + "</MCUAddressType>";
                inXML += "          <Deleted>0</Deleted>";
                inXML += "<ExchangeID>" + txtExchangeID.Text + "</ExchangeID>";//Cisco Telepresence fix
                //Code Added For FB 1422 - Start
                inXML += "          <TelnetAPI>0</TelnetAPI>";
                //Code Added For FB 1422 - End
                inXML += "          <SSHSupport>0</SSHSupport>";//ZD 101363
                inXML += "    <conferenceCode>" + confcode + "</conferenceCode>";//FB 1642-Audio add on
                inXML += "    <leaderPin>" + leaderPin + "</leaderPin>";//FB 1642-Audio add on
                //FB 2595 Start
                //if (chkSecured.Checked)
                //    inXML += "<Secured>1</Secured>";
                //else
                //    inXML += "<Secured>0</Secured>";
                //FB 2595 End
                inXML += "    <SysLocation>" + lstSystemLocation.SelectedValue + "</SysLocation>";//ZD 104821
                inXML += "        </Profile>";
                inXML += "      </Profiles>";
                inXML += "    </SetEndpoint>";
                return inXML;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GenerateInxml:" + ex.Message);//ZD 100263
                return "<error></error>";
            }
        }
        #endregion

        #region ValidateIPAddress
        protected void ValidateIPAddress(Object sender, ServerValidateEventArgs e)
        {
            try
            {
                //FB 3012 Starts
                errLabel.Text = "";
                String patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                String patternISDN = @"^[0-9]+$"; //[1-9][0-9]\d{3,9}
                Regex checkIP = new Regex(patternIP);
                Regex checkISDN = new Regex(patternISDN);
                
                e.IsValid = true;
                switch (lstAddressType.SelectedValue)
                {
                    case ns_MyVRMNet.vrmAddressType.IP:
                        {                            
                            if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.IP))
                            {
                                if (txtAddress.Text == "")
                                {
                                    e.IsValid = false;
                                    errLabel.Text = obj.GetTranslatedText("Invalid IP Address for user");
                                }
                                else
                                {
                                    e.IsValid = checkIP.IsMatch(txtAddress.Text);
                                    if (!e.IsValid)
                                        errLabel.Text = obj.GetTranslatedText("Invalid IP Address for user");
                                }
                            }
                            else if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                            {
                                e.IsValid = false;
                                errLabel.Text = obj.GetTranslatedText("Protocol does not match with selected Address Type.");
                            }
                            break;
                        }
                    case ns_MyVRMNet.vrmAddressType.ISDN:
                        {                           
                            if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                            {
                                if (txtAddress.Text == "")
                                {
                                    e.IsValid = false;
                                    errLabel.Text = obj.GetTranslatedText("Invalid ISDN Address for user");
                                }
                                else
                                {
                                    e.IsValid = checkISDN.IsMatch(txtAddress.Text);
                                    if (!e.IsValid)
                                        errLabel.Text = obj.GetTranslatedText("Invalid ISDN Address for user");
                                }
                            }
                            else
                            {
                                e.IsValid = false;
                                errLabel.Text = obj.GetTranslatedText("Protocol does not match with selected Address Type.");
                            }
                            break;
                        }
                    default:
                        {
                            e.IsValid = true;
                            break;
                        }
                }
                //Code changed by Offshore for FB Issue 1073 -- Start                
                //if (DateTime.Parse(txtAccountExpiry.Text) <= DateTime.Now)
                if (DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtAccountExpiry.Text)) <= DateTime.Now)
                //Code changed by Offshore for FB Issue 1073 -- End
                {
                    e.IsValid = false;
                    errLabel.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.InvalidAccountExpiry); //ZD 100288
                    txtAccountExpiry.Focus();
                }
                //if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                //    if (!lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) || !lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI) || !lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                //    {
                //        valid = false;
                //        cusVal1.Text = obj.GetTranslatedText("Invalid selection for Address Type, Protocol and Connection Type.");//FB 1830 - Translation
                //        lstAddressType.Focus();
                //    }
                //if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) && (lstBridges.SelectedValue.Equals("-1")))
                //{
                //    valid = false;
                //    errLabel.Text = obj.GetTranslatedText("Please select MCU.");//FB 1830 - Translation
                //    lstBridges.Focus();
                //}
                //Response.Write(valid);
                //Response.End();
                //e.IsValid = valid;

                //FB 3012 Ends
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region ValidateTypes
        protected void ValidateTypes(Object sender, EventArgs e)
        {
            try
            {
                switch (lstAddressType.SelectedValue)
                {
                    /*commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                case ns_MyVRMNet.vrmAddressType.MPI:
                    lstConnectionType.ClearSelection();
                    lstConnectionType.Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.Direct).Selected = true;
                    lstProtocol.ClearSelection();
                    lstProtocol.Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.MPI).Selected = true;
                    reqBridges.Enabled = true;
                    break;
                     */
                    //commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage END
                    case ns_MyVRMNet.vrmAddressType.IP:
                        if (lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                        {
                            lstConnectionType.ClearSelection();
                            lstConnectionType.Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                        }
                        lstProtocol.ClearSelection();
                        lstProtocol.Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.IP).Selected = true;
                        //Removed MPI for ZD 103595- Operations - Remove MPI Functionality and Verbiage
                        if (lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN))
                        {
                            lstMCUAddressType.ClearSelection();
                            lstMCUAddressType.Items.FindByValue(ns_MyVRMNet.vrmAddressType.IP).Selected = true;
                        }
                        break;
                    case ns_MyVRMNet.vrmAddressType.ISDN:
                        if (lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                        {
                            lstConnectionType.ClearSelection();
                            lstConnectionType.Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                        }
                        lstProtocol.ClearSelection();
                        lstProtocol.Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.ISDN).Selected = true;
                        lstMCUAddressType.ClearSelection();
                        lstMCUAddressType.Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //RSS Feed
        #region RSSFeed
        protected void RSSFeed(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("RSSGenerator.aspx");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region Bind to Rooms

        public void BindRoomToList()
        {
            String[] locsName = null;
            string Roomid = "";//ZD 103569
            try
            {
                if (locstrname.Value != "")
                {

                    locsName = locstrname.Value.Split('+');
                    RoomList.Items.Clear();
                    foreach (String s in locsName)
                    {
                        if (s != "")
                        {

                            if (s.Split('|').Length > 1)
                            {
                                RoomList.Items.Add(new ListItem(s.Split('|')[1], s.Split('|')[0]));
                                if (s.Split('|')[0] != "") //ZD 103569 start
                                {
                                    if(string.IsNullOrEmpty(Roomid))
                                        Roomid = s.Split('|')[0];
                                    else
                                        Roomid += "," + s.Split('|')[0];//ZD 103569 End
                                }
                                
                            }
                        }
                    }
                }
                Session["UserPreferedRoom"] = Roomid; //ZD 103569 
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(ex.Message);
            }
        }
        #endregion

        //FB 1830 - Start
        #region LoadLanguage
        protected void LoadLanguage(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    lstLanguage.DataSource = dt;
                    lstLanguage.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region DefineEmailLanguage

        protected void DefineEmailLanguage(object sender, EventArgs e)
        {
            try
            {
				//FB 2283 Start
                Session.Remove("UsrBaseLang");
                Session["UsrBaseLang"] = lstLanguage.SelectedValue;
				//FB 2283 End
                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("1"))
                        Response.Redirect("EmailCustomization.aspx?tp=au");

                Response.Redirect("EmailCustomization.aspx?tp=u");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion
        //FB 1830 - End

        //FB 1860
        #region EditBlockEmails

        protected void EditBlockEmails(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("1"))
                        Response.Redirect("ViewBlockedMails.aspx?tp=au");

                Response.Redirect("ViewBlockedMails.aspx?tp=u");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //FB 1830 - DeleteEmailLang start
        #region DeleteEmailLangugage
        /// <summary>
        /// DeleteEmailLangugage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteEmailLangugage(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                String outXML = "", emailLangID = "";

                if (Session["UsrEmailLangID"] != null)//FB 2283
                    if (Session["UsrEmailLangID"].ToString() != "")
                        emailLangID = Session["UsrEmailLangID"].ToString();

                if (emailLangID.Trim() != "")
                {
                    inXML.Append("<DeleteEmailLang>");
                    inXML.Append(obj.OrgXMLElement());
                    //ZD 100263
                    inXML.Append("<userid>" + Session["ModifiedUserID"].ToString() + "</userid>");
                    //inXML.Append("<userid>" + txtUserID.Text + "</userid>");
                    inXML.Append("<emaillangid>" + emailLangID.ToString() + "</emaillangid>");
                    inXML.Append("<langOrigin>u</langOrigin>");
                    inXML.Append("</DeleteEmailLang>");

                    outXML = obj.CallMyVRMServer("DeleteEmailLanguage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        txtEmailLang.Text = "";
                        Session["UsrEmailLangID"] = null; //FB 2283
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                        delEmailLang.Visible = false;//ZD 100580
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("UserProfile DeleteEmailLangugage :" + ex.Message);
            }
        }
        #endregion
        //FB 1830 - DeleteEmailLang end

        protected void lobbyManage(object sender,EventArgs e)
        {

            try
            {
                String reqstr="?p=1"; // FB 2567
                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("1"))
                        reqstr += "&t=au"; // FB 2567
                Response.Redirect("LobbyManagement.aspx" + reqstr);
            }
            catch(Exception ex)
            {
                log.Trace(ex.Message);
            }
        }

        //FB 2481 Start

        #region Private VMR Cancel
        /// <summary>
        /// video guest location Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // ZD 102535 Starts
        //protected void fnPrivateVMRCancel(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PrivateVMRPopup.Hide();
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Trace("fnPrivateVMRCancel" + ex.Message);
        //    }
        //}
        // ZD 102535 Ends
        #endregion 

        #region Private VMR Submit
        /// <summary>
        /// Video Guest Location Submit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // ZD 102535 Starts
        //protected void fnPrivateVMRSubmit(object sender, EventArgs e)
        //{
        //    try
        //    {
               
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Trace("fnPrivateVMRSubmit" + ex.Message);
        //    }
        //}
        // ZD 102535 Ends
        #endregion video guest location submit
        //FB 2481 End


        // FB 2693 Starts

        protected void LoadPCDetails(XmlNodeList nodes)
        {
            try
            {
                int PCID = 0;

                for (int i = 0; i < nodes.Count; i++)
                {
                    if (nodes[i].SelectSingleNode("PCId") != null)
                        int.TryParse(nodes[i].SelectSingleNode("PCId").InnerText.Trim(), out PCID);

                    switch (PCID)
                    {
                        //ZD 104021
                        /*case ns_MyVRMNet.vrmPC.BlueJeans:
                            chkBJ.Checked = true;
                            if (nodes[i].SelectSingleNode("Description") != null)
                                txtAreaBlueJeans.Value = nodes[i].SelectSingleNode("Description").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("SkypeURL") != null)
                                txtSkypeBJ.Value = nodes[i].SelectSingleNode("SkypeURL").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinIP") != null)
                                txtDialinIP.Value = nodes[i].SelectSingleNode("VCDialinIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCMeetingID") != null)
                                txtMeetingVC.Value = nodes[i].SelectSingleNode("VCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinNum") != null)
                                txtTollBJ.Value = nodes[i].SelectSingleNode("PCDialinNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinFreeNum") != null)
                                txtTollFreeBJ.Value = nodes[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCMeetingID") != null)
                                txtMeetingPC.Value = nodes[i].SelectSingleNode("PCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("Intructions") != null)
                                txtDetail.Value = nodes[i].SelectSingleNode("Intructions").InnerText.Trim();
                            
                            break;*/
                        case ns_MyVRMNet.vrmPC.Jabber:
                            chkJB.Checked = true;
                            if (nodes[i].SelectSingleNode("Description") != null)
                                txtAreaJabber.Value = nodes[i].SelectSingleNode("Description").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("SkypeURL") != null)
                                txtSkypeJB.Value = nodes[i].SelectSingleNode("SkypeURL").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinIP") != null)
                                txtDialinIPJB.Value = nodes[i].SelectSingleNode("VCDialinIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCMeetingID") != null)
                                txtMeetingVCJB.Value = nodes[i].SelectSingleNode("VCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinNum") != null)
                                txtTollJB.Value = nodes[i].SelectSingleNode("PCDialinNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinFreeNum") != null)
                                txtTollFreeJB.Value = nodes[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCMeetingID") != null)
                                txtMeetingPCJB.Value = nodes[i].SelectSingleNode("PCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("Intructions") != null)
                                txtDetailJB.Value = nodes[i].SelectSingleNode("Intructions").InnerText.Trim();
                            break;
                        case ns_MyVRMNet.vrmPC.Lync: 
                            chkLync.Checked =true;
                            if (nodes[i].SelectSingleNode("Description") != null)
                                txtAreaLync.Value = nodes[i].SelectSingleNode("Description").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("SkypeURL") != null)
                                txtSkypeLync.Value = nodes[i].SelectSingleNode("SkypeURL").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinIP") != null)
                                txtDialinIPLync.Value = nodes[i].SelectSingleNode("VCDialinIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCMeetingID") != null)
                                txtMeetingVCLync.Value = nodes[i].SelectSingleNode("VCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinNum") != null)
                                txtTollLync.Value = nodes[i].SelectSingleNode("PCDialinNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinFreeNum") != null)
                                txtTollFreeLync.Value = nodes[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCMeetingID") != null)
                                txtMeetingPCLync.Value = nodes[i].SelectSingleNode("PCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("Intructions") != null)
                                txtDetailLync.Value = nodes[i].SelectSingleNode("Intructions").InnerText.Trim();
                            break;
                        //ZD 102004
                        //case ns_MyVRMNet.vrmPC.Vidtel:
                        //    chkVidtel.Checked = true;
                        //    if (nodes[i].SelectSingleNode("Description") != null)
                        //        txtAreaVidtel.Value = nodes[i].SelectSingleNode("Description").InnerText.Trim();
                        //    if (nodes[i].SelectSingleNode("SkypeURL") != null)
                        //        txtSkypeVidtel.Value = nodes[i].SelectSingleNode("SkypeURL").InnerText.Trim();
                        //    if (nodes[i].SelectSingleNode("VCDialinSIP") != null)
                        //        txtDialinSIP.Value = nodes[i].SelectSingleNode("VCDialinSIP").InnerText.Trim();
                        //    if (nodes[i].SelectSingleNode("VCDialinH323") != null)
                        //        txtDialinH323.Value = nodes[i].SelectSingleNode("VCDialinH323").InnerText.Trim();
                        //    if (nodes[i].SelectSingleNode("VCPin") != null)
                        //        txtPinVc.Value = nodes[i].SelectSingleNode("VCPin").InnerText.Trim();
                        //    if (nodes[i].SelectSingleNode("PCDialinNum") != null)
                        //        txtTollVidtel.Value = nodes[i].SelectSingleNode("PCDialinNum").InnerText.Trim();
                        //    if (nodes[i].SelectSingleNode("PCDialinFreeNum") != null)
                        //        txtTollFreeVidtel.Value = nodes[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();
                        //    if (nodes[i].SelectSingleNode("PCPin") != null)
                        //        txtPinPc.Value = nodes[i].SelectSingleNode("PCPin").InnerText.Trim();
                        //    break;
                        default: 

                            break;

                    }
                }


            }
            catch (Exception ex)
            {
                log.Trace("LoadPCDetails" + ex.Message);
            }
        }
        // FB 2693 Ends

        protected string UserPassword(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace("UserPassword:" + ex.Message);//ZD 100263
            }
            return Encrypted;
        }
        
        //ZD 100164 START
        protected void TDBAdvancedUserProfile()
        {
            trPreferredLocation.Attributes.Add("style", "display:none");
            trDateFormat.Attributes.Add("style", "display:none");
            trEnableMobile.Attributes.Add("style", "display:none");
            trEmailLanguage.Attributes.Add("style", "display:none");
            trLobbyManagement.Attributes.Add("style", "display:none");
            trHelpRequestorPhone.Attributes.Add("style", "display:none");
            trSurveyEmail.Attributes.Add("style", "display:none");
            trVNOCSelection.Attributes.Add("style", "display:none");
            trwhygouserSettings.Attributes.Add("style", "display:none");
            trPCConferencing.Attributes.Add("style", "display:none");
            tdOtherSettings.Attributes.Add("style", "display:none");
            //ZD 102231 Starts
            //trUser.Attributes.Add("style", "display:none");
            EnableTR.Attributes.Add("style", "display:none");
            DrpDom.Attributes.Add("style", "display:none");
            lblDOmino.Attributes.Add("style", "display:none");
            EbleWO.Attributes.Add("style", "display:none");
            EbleCatWO.Attributes.Add("style", "display:none");
            //ZD 102231 Ends
            trSendBoth.Attributes.Add("style", "display:none");
            trWork.Attributes.Add("style", "display:none");
            trLDAPLogin.Attributes.Add("style", "display:none");
            trAccountExpiration.Attributes.Add("style", "display:none");
            tdDefaultGroup.Attributes.Add("style", "display:none");
            tdlstDefaultGroup.Attributes.Add("style", "display:none");
            tdSecondaryEmail.Attributes.Add("style", "display:none");
            tdtxtUserEmail.Attributes.Add("style", "display:none");
            tdPreferredAddressBook.Attributes.Add("style", "display:none");
            tdlstAddressBook.Attributes.Add("style", "display:none");
            tdScheduleMin.Attributes.Add("style", "display:none");
            lblTimeRemaining.Attributes.Add("style", "display:none");//ZD 102043
            txtTotalTime.Attributes.Add("style", "display:none");//ZD 102043
            trLandingPage.Attributes.Add("style", "display:none"); //ZD 100969
            tblWebEx.Attributes.Add("style", "display:none"); //ZD 100969
 			//ZD 100164 - Changes made by Inncrewin Ram Manohar
            tdUsrRole.Attributes.Add("style", "display:none");
            tdDrpDownUsrRole.Attributes.Add("style", "display:none");
            lstNewUserRole.Enabled = false;
            tdNewUsrRole.Attributes.Add("style", "display:block;padding-top:10px");
            tdNewDrpDownUsrRole.Attributes.Remove("style");
            if (enablePass.Trim() == "0")
            {
                tdNewEmail_lbl.Attributes.Remove("style");
                tdNewEmail_txt.Attributes.Remove("style");
                tdNewEmail_txt.Controls.Add(txtUserEmail);
                tdNewEmail_txt.Controls.Add(reqUserEmail);
                tdNewEmail_txt.Controls.Add(regEmail1_1);
                tdNewEmail_txt.Controls.Add(regEmail1_2);
                trMailinfo.Controls.Clear();
                lstNewUserRole.Enabled = false;
            }
            else
                tdNewDrpDownUsrRole.Attributes.Add("style", "width:30%");
            
 			
        }


        protected void populateNewUserRole()
        {
            if (!IsPostBack)
            {
                lstNewUserRole.Items.Clear();
                foreach (ListItem item in lstUserRole.Items)
                {
                    lstNewUserRole.Items.Add(item);
                }
                if (Session["ModifiedUserID"].ToString() == "" || (Session["ModifiedUserID"].ToString().Trim().ToLower().Contains("new")))
                {
                    lstNewUserRole.SelectedValue = "7";
                    lstUserRole.SelectedIndex = lstNewUserRole.SelectedIndex;
                    
                }
                lstNewUserRole.Enabled = false;
            }
        }
        protected void UpdateNewDepartment(Object sender, EventArgs e)
        {
            lstUserRole.SelectedIndex = lstNewUserRole.SelectedIndex;
            UpdateDepartments(this, new EventArgs());
        }
        //ZD 103686
        private void DisplayConferenceOptions()
        {
            string mMask = "";
            Boolean hasExpConference, hasConference;
            mMask = lstRoles.Items[lstUserRole.SelectedIndex].Text;
            string[] mary = mMask.Split('-');
            string[] mmary = mary[1].Split('+');
            hasExpConference = Convert.ToBoolean(Int32.Parse(mmary[9].Split('*')[1]) & 2);
            hasConference = Convert.ToBoolean(Int32.Parse(mmary[9].Split('*')[1]) & 1);

            if (!hasConference)
            {
                ParLBL.Attributes.Add("style", "display:none");
                DrpEnvPar.Visible = false;
                AVLBL.Attributes.Add("style", "display:none");
                DrpEnableAV.Visible = false;
                
                Lbladdopt.Attributes.Add("style", "display:none");
                drpadditionaloption.Visible = false;
                LblAVWO.Attributes.Add("style", "display:none");
                DrpAVWO.Visible = false;

                LblCateringWO.Attributes.Add("style", "display:none");
                DrpCateringWO.Visible = false;
                LblFacilityWO.Attributes.Add("style", "display:none");
                DrpFacilityWO.Visible = false;
            }
            else
            {
                ParLBL.Attributes.Add("style", "display:Block");
                DrpEnvPar.Visible = true;
                AVLBL.Attributes.Add("style", "display:Block");
                DrpEnableAV.Visible = true;

                Lbladdopt.Attributes.Add("style", "display:Block");
                drpadditionaloption.Visible = true;
                LblAVWO.Attributes.Add("style", "display:Block");
                DrpAVWO.Visible = true;

                LblCateringWO.Attributes.Add("style", "display:Block");
                DrpCateringWO.Visible = true;
                LblFacilityWO.Attributes.Add("style", "display:Block");
                DrpFacilityWO.Visible = true;
            }
        }

        //ZD104821 Starts
        #region ChangeMCU
        protected void ChangeMCU(Object sender, EventArgs e)
        {
            try
            {
                int BridgeType = 0, BrdgeID = -1;

                SysLocation.Visible = false;
                lstSystemLocation.Visible = false;
                if (!lstBridges.SelectedValue.Equals("-1"))
                {
                    lstBridgeType.SelectedValue = lstBridges.SelectedValue;
                    int.TryParse(lstBridges.SelectedValue, out BrdgeID);
                    int.TryParse(lstBridgeType.SelectedItem.Text, out BridgeType);
                    lstSystemLocation.Visible = false;
                    if (BridgeType == 16)
                    {
                        SysLocation.Visible = true;
                        lstSystemLocation.Visible = true;
                        BindSystemLocation(BrdgeID, BridgeType, lstSystemLocation);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("UpdateEndpointModel: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindSystemLocation
        /// <summary>
        /// BindSystemLocation
        /// </summary>
        protected void BindSystemLocation(int BridgeID, int BridgeType, DropDownList drpdwnSysLocation)
        {
            string outXML = "";
            StringBuilder inXML = new StringBuilder();
            XmlDocument xmlDoc = null;
            DataSet ds = null;
            try
            {
                inXML.Append("<GetSystemLocation>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<organizationID>" + Session["OrganizationID"].ToString() + "</organizationID>");
                inXML.Append("<BridgeId>" + BridgeID + "</BridgeId>");
                inXML.Append("<BridgeTypeId>" + BridgeType + "</BridgeTypeId>");
                inXML.Append("</GetSystemLocation>");

                outXML = obj.CallMyVRMServer("GetSystemLocation", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);
                    ds = new DataSet();
                    ds.ReadXml(new XmlNodeReader(xmlDoc));
                    XmlNodeList nodes = xmlDoc.SelectNodes("//GetSystemLocations/GetSystemLocation");
                    drpdwnSysLocation.Items.Clear();
                    if (nodes.Count > 0)
                        LoadList(drpdwnSysLocation, nodes);
                    drpdwnSysLocation.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0"));
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindSystemLocation" + ex.Message);
            }
        }
        #endregion

        protected void LoadList(DropDownList lstTemp, XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                if (dt.Columns.Contains("name"))//FB 2272
                    foreach (DataRow dr in dt.Rows)
                        dr["name"] = obj.GetTranslatedText(dr["name"].ToString());
                if (dt.Columns.Contains("name"))
                    dt.DefaultView.Sort = "name ASC";//ZD 103787
                lstTemp.DataSource = dt;
                lstTemp.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        //ZD 104821 Ends
    }
}
