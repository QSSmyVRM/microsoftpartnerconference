/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Text;//ZD 102550


namespace ns_MyVRM
{
    public partial class ViewBlockedMails : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;

        #region Protected Data Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Table tblNoBlockMail;
        protected System.Web.UI.WebControls.DataGrid dgBlockEmail;
        protected System.Web.UI.WebControls.Button btnCreateCusAtt;
        protected System.Web.UI.HtmlControls.HtmlTableCell ConfMessage;
        protected System.Web.UI.WebControls.Button CustomTrigger;
        protected System.Web.UI.WebControls.Table ConfListTbl;
        protected System.Web.UI.HtmlControls.HtmlInputHidden HdnCustOptID;
        protected System.Web.UI.WebControls.Button BtnEditCA;
        protected System.Web.UI.WebControls.Button BtnDeleteAll;

        protected System.Web.UI.WebControls.ImageButton btnExcel;
        //protected System.Web.UI.WebControls.Button btnSubmit;//FB 2670
        protected System.Web.UI.HtmlControls.HtmlButton btnSubmit;//FB 2670 //ZD 100420
        //FB 2711 Starts
        protected System.Web.UI.WebControls.CheckBox ChkRel;
        protected System.Web.UI.WebControls.CheckBox Chkdel;
        protected System.Web.UI.WebControls.Label lbsel;
        protected System.Web.UI.WebControls.Label lbdel;
        //FB 2711 Ends

        string confListHeader = "";
        XmlNodeList confList = null;
        ns_Logger.Logger log;
        MyVRMNet.Util utilObj;//FB 2236

        protected string customAttrID = "";
        ArrayList colNames = null;

        //ZD 102550 start
        protected System.Web.UI.WebControls.CheckBox ChkRelAll;
        protected System.Web.UI.WebControls.CheckBox ChkdelAll;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChkDelAll;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChkRelAll;
        protected System.Web.UI.WebControls.Label lblRelAll;
        protected System.Web.UI.WebControls.Label lbldelAll;
        //ZD 102550 End
        #endregion

        public ViewBlockedMails()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            utilObj = new MyVRMNet.Util(); //FB 2236
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region  Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = "";
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());

                if (Request.QueryString["tp"] != null)
                    confChkArg = Request.QueryString["tp"].ToString();
                obj.AccessConformityCheck("viewblockedmails.aspx?tp=" + confChkArg);
                // ZD 100263 Ends

                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }

                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    // btnSubmit.ForeColor = System.Drawing.Color.Gray; //FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnSubmit.Visible = false;
                }
                else
                    btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796

                if (!IsPostBack)
                    BindData();

                ChkRel.Checked = false;//FB2711
                Chkdel.Checked = false;//FB2711
                ChkdelAll.Checked = false; //ZD 102550
                ChkRelAll.Checked = false; //ZD 102550 

            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("PageLoad:" + ex.Message);//ZD 100263
            }
        }
        #endregion

        //ZD 102550 start
        #region BindData
        private void BindData()
        {
            string command = "GetUserEmails";
            string inXML = "", outXML = ""; //ZD 102550
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                string blockUserId = Session["userID"].ToString();
                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"].ToLower().Equals("au"))
                    {
                        if (Session["UserToEdit"] != null && !string.IsNullOrEmpty(Session["UserToEdit"].ToString()))
                            blockUserId = Session["UserToEdit"].ToString();
                    }
                    else if (Request.QueryString["tp"].ToLower().Equals("o"))
                        command = "GetOrgEmails";
                }

                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + blockUserId + "</userID></login>";

                outXML = obj.CallMyVRMServer(command, inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    int rowCnt = 0;
                    DataRow dr;
                    XmlNodeList nodes = null;
                    XmlDocument xmldoc = new XmlDocument();
                    outXML = outXML.Replace("&", "&amp;");
                    xmldoc.LoadXml(outXML);

                    CreateDtColumnNames();
                    DataTable dtable = obj.LoadDataTable(nodes, colNames);
                    nodes = xmldoc.SelectNodes("//Emails/Email");
                    XmlNode node = null;
                    for (int n = 0; n < nodes.Count; n++)
                    {
                        node = nodes[n];
                        dr = dtable.NewRow();
                        rowCnt = rowCnt + 1;
                        dr["UUID"] = node.SelectSingleNode("uuid").InnerText;
                        dr["RowID"] = rowCnt.ToString();
                        dr["From"] = node.SelectSingleNode("emailFrom").InnerText;
                        dr["To"] = node.SelectSingleNode("emailTo").InnerText;
                        dr["Subject"] = node.SelectSingleNode("Subject").InnerText;
                        dr["Message"] = utilObj.ReplaceOutXMLSpecialCharacters((node.SelectSingleNode("Message").InnerText), 1);//FB 2236
                        dr["Iscalendar"] = node.SelectSingleNode("Iscalendar").InnerText;
                        dtable.Rows.Add(dr);
                    }
                    if (dtable != null && dtable.Rows.Count > 0)
                    {
                        LinkButton BtnEdit = null;
                        CheckBox chkDelete = null;
                        CheckBox chkRelease = null;

                        dgBlockEmail.DataSource = dtable;
                        dgBlockEmail.DataBind();

                        dgBlockEmail.Visible = true;
                        tblNoBlockMail.Visible = false;

                        for (int dg = 0; dg < dgBlockEmail.Items.Count; dg++)
                        {
                            BtnEdit = (LinkButton)dgBlockEmail.Items[dg].FindControl("btnEdit"); //FB 2670
                            dgBlockEmail.Items[dg].Cells[7].Text = obj.GetTranslatedText("No");
                            if (dgBlockEmail.Items[dg].Cells[1].Text.Trim() == "1")
                            {
                                dgBlockEmail.Items[dg].Cells[7].Text = obj.GetTranslatedText("Yes");

                                if (BtnEdit != null)
                                    BtnEdit.Enabled = false;
                            }
                            //FB 2670
                            if (Session["admin"].ToString() == "3")
                            {
                                //BtnEdit.Attributes.Remove("onClick");
                                //BtnEdit.Style.Add("cursor", "default");
                                //BtnEdit.ForeColor = System.Drawing.Color.Gray;
                                //ZD 100263
                                BtnEdit.Visible = false;
                            }

                            if (hdnChkDelAll.Value == "1" || hdnChkRelAll.Value == "1")
                            {
                                chkDelete = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkDelete");
                                chkRelease = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkRelease");
                                if (chkDelete != null && hdnChkDelAll.Value == "1")
                                {
                                    chkDelete.Checked = true;
                                    ChkdelAll.Checked = true;
                                    chkRelease.Checked = false;
                                    ChkRelAll.Checked = false;
                                }
                                else if (chkRelease != null && hdnChkRelAll.Value == "1")
                                {
                                    chkRelease.Checked = true;
                                    ChkRelAll.Checked = true;
                                    chkDelete.Checked = false;
                                    ChkdelAll.Checked = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        dgBlockEmail.Visible = false;
                        tblNoBlockMail.Visible = true;
                        lbdel.Visible = false;
                        lbsel.Visible = false;
                        ChkRel.Visible = false;
                        Chkdel.Visible = false;

                        lbldelAll.Visible = false;
                        lblRelAll.Visible = false;
                        ChkRelAll.Visible = false;
                        ChkdelAll.Visible = false;
                        //ZD 102550 End
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }
        #endregion
        //ZD 102550 End

        #region Delete Block Mail
        protected void DeleteBlockMail(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<DeleteEmailsList>");
                inXML.Append(obj.OrgXMLElement());
                //ZD 102550 start
                if (hdnChkDelAll.Value == "1")
                {
                    inXML.Append("<DeleteAllEmail>1</DeleteAllEmail>");
                    if (Request.QueryString["tp"].ToLower() != "o")
                    {
                        if (Session["UserToEdit"] != null && !string.IsNullOrEmpty(Session["UserToEdit"].ToString()))
                            inXML.Append("<EdituserID>" + Session["UserToEdit"].ToString() + "</EdituserID>");
                        else
                            inXML.Append("<EdituserID>" + Session["userID"].ToString() + "</EdituserID>");
                    }
                }
                else if (hdnChkRelAll.Value == "1")
                {
                    inXML.Append("<ReleaseAllEmail>1</ReleaseAllEmail>");
                    if (Request.QueryString["tp"].ToLower() != "o")
                    {
                        if (Session["UserToEdit"] != null)
                            inXML.Append("<EdituserID>" + Session["UserToEdit"].ToString() + "</EdituserID>");
                        else
                            inXML.Append("<EdituserID>" + Session["userID"].ToString() + "</EdituserID>");
                    }
                }
                //ZD 102550 End
                else
                {
                    CheckBox chkDelete = null;
                    CheckBox chkRelease = null;
                    for (int dg = 0; dg < dgBlockEmail.Items.Count; dg++)
                    {
                        chkDelete = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkDelete");
                        if (chkDelete != null && chkDelete.Checked) //ZD 102550
                        {
                            inXML.Append("<DeleteEmail>");
                            inXML.Append("<uuid>" + dgBlockEmail.Items[dg].Cells[0].Text + "</uuid>");
                            inXML.Append("</DeleteEmail>");
                        }

                        // Release email
                        chkRelease = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkRelease");
                        if (chkRelease != null && chkRelease.Checked) //ZD 102550
                        {
                            inXML.Append("<ReleaseEmail>");
                            inXML.Append("<uuid>" + dgBlockEmail.Items[dg].Cells[0].Text + "</uuid>");
                            inXML.Append("</ReleaseEmail>");
                        }
                    }
                }
                inXML.Append("</DeleteEmailsList>");
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                string outXML = obj.CallMyVRMServer("DeleteEmails", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                    Response.Redirect("ViewBlockedMails.aspx?m=1&tp=" + Request.QueryString["tp"].ToString());
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteBlockMail:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion
        // FB 1860 - Paging
        #region BindBlockEmail
        protected void BindBlockEmail(object sender, DataGridPageChangedEventArgs e)
        {
            try
            {
                dgBlockEmail.CurrentPageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.Message);
                // errLabel.Text = "Error 122: Please contact your VRM Administrator";ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region EditBlockEmail
        protected void EditBlockEmail(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Response.Redirect("EditBlockEmail.aspx?uuid=" + e.Item.Cells[0].Text + "&tp=" + Request.QueryString["tp"].ToString());
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.Message);
                //errLabel.Text = "Error 122: Please contact your VRM Administrator";ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Create Column Names
        /// <summary>
        /// CreateDtColumnNames
        /// </summary>
        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("UUID");
            colNames.Add("RowID");
            colNames.Add("From");
            colNames.Add("To");
            colNames.Add("Subject");
            colNames.Add("Message");
            colNames.Add("Iscalendar");

        }
        #endregion

        #region Redirect To TargetPage

        protected void RedirectToTargetPage(Object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"].ToLower().Equals("o"))
                        Response.Redirect("OrganisationSettings.aspx");
                    else
                    {
                        if (Request.QueryString["tp"].ToLower().Equals("au"))
                            Response.Redirect("ManageUserProfile.aspx?t=1");
                        else
                            Response.Redirect("ManageUserProfile.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("EmailCustomization:" + ex.Message);
            }
        }
        #endregion

        //Release Emails
        #region Delete All Block Mail
        protected void DeleteAllBlockMail(object sender, EventArgs e)
        {
            try
            {
                CheckBox chkDelete = null;
                CheckBox chkRelease = null;
                for (int dg = 0; dg < dgBlockEmail.Items.Count; dg++)
                {
                    chkDelete = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkDelete");
                    if (chkDelete != null)
                        chkDelete.Checked = true;

                    chkRelease = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkRelease");
                    if (chkRelease != null)
                        chkRelease.Checked = false;
                }
                DeleteBlockMail(null, null);
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteAllBlockMail:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion
    }
}
