/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
namespace myVRMNet
{
    #region References
    using System;
    using System.ComponentModel;
    using System.Web.Services;
    using System.Xml;
    using System.Text.RegularExpressions;
    using System.Collections.Generic;
    #endregion

    [WebService(Namespace = "http://www.myvrm.com/webservices/")]
    public class VRMNewWebService : System.Web.Services.WebService
    {

        //Client definition enum added - reference Businesslayer\UserFactory.cs
        public enum clientOrigin { None = 0, OutlookYork, OutlookGeneric, LotusYork, LotusGeneric, AppleDevices, AndroidDevices, Exchange }; //ZD 103569

        #region Private Data Members
        private string sChkUsr = "CheckUserCredentials"; //Added for Webservice, To check User Authentication
        //Merge WS in to Web Start
        private string vrmConfigPath = "C:\\VRMSchemas_v1.8.3\\VRMConfig.xml"; // default value. read from local config. 
        private int organizationID = 0;
        private int defaultOrgId = 11;
        //Merge WS in to Web End
        #endregion

        //private VRM oComInstance;

        #region VRMNewWebService Constructor
        public VRMNewWebService()
        {
            //CODEGEN: This call is required by the ASP.NET Web Services Designer
            InitializeComponent();
        }
        #endregion

   
        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion        

        #region InvokeWebservice
        
        [WebMethod]

        #region Old Code
        /**
        public string InvokeWebserviceOLD(string inputXML)
        {
            string outputXML = "";
            string aspCommand = "";            
            string sCmdType = "";
            string resultXML = "";
            string sInXML = "";
            string sVersion = "";
            string cmdoutXML = "";
            string sClient = "";
            NETFunctions obj = null;
            try
            {

                XmlDocument xd = new XmlDocument();
                if (inputXML != "")
                {
                    obj = new NETFunctions();
                    xd.LoadXml(inputXML);

                    XmlNode node;

                    // Retriving the Command Name
                    node = xd.SelectSingleNode("//myVRM/commandname");
                    if (node.InnerText != "")
                        aspCommand = node.InnerXml.Trim();

                    //InXML to send to the Business layers 
                    node = xd.SelectSingleNode("//myVRM/command");
                    if (node.InnerText != "")
                        sInXML = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//myVRM/version");
                    if (node.InnerText != "")
                        sVersion = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//myVRM/client");
                    if (node.InnerText != "")
                        sClient = node.InnerXml.Trim();
                    //Authorization
                    //Window Authentication START
                    if (System.Web.HttpContext.Current.User.Identity.AuthenticationType.ToString() != "")
                    {
                        String sAuthUsr = System.Web.HttpContext.Current.User.Identity.Name;
                        string sWinUsr = "";
                        if (sAuthUsr != "")
                        {
                            Int32 iAuthPathUsr = sAuthUsr.IndexOf("\\");
                            sWinUsr = sAuthUsr.Substring(iAuthPathUsr + 1);
                            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                            {
                                node = xd.SelectSingleNode("//myVRM/login");
                                if(node == null)
                                    node = xd.CreateElement("login");
                                node.InnerText = sWinUsr.Trim();
                                xd.DocumentElement.InsertBefore(node, xd.SelectSingleNode("//myVRM/version"));

                                node = xd.SelectSingleNode("//myVRM/password");
                                if (node == null)
                                    node = xd.CreateElement("password");
                                node.InnerText = sWinUsr.Trim();
                                node.InnerText = "";
                                xd.DocumentElement.InsertBefore(node, xd.SelectSingleNode("//myVRM/version"));
                                node = xd.CreateElement("IsAuthenticated");
                                node.InnerText = "Yes";
                                xd.DocumentElement.InsertBefore(node, xd.SelectSingleNode("//myVRM/version"));
                                inputXML = xd.InnerXml;
                            }
                        }
                        //return inputXML;
                    }
                    //Window Authentication END start
                    //resultXML = CallMyVRMServer(sChkUsr, inputXML, aspilConfigPath);
                    resultXML = obj.CallCommand(sChkUsr,inputXML); //Authorization Validation
                    if (resultXML.IndexOf("<success>") >= 0)
                    {
                        if (aspCommand != "" && inputXML != "" && sInXML != "")
                        {
                            cmdoutXML = obj.CallCommand(aspCommand,sInXML);
                            outputXML = "<myVRM><version>" + sVersion + "</version><client>" + sClient + "</client><commandname>" + aspCommand + "</commandname><command>" + cmdoutXML + "</command></myVRM>";

                            #region Old Code 
                            //wsF = new WSFactory();

                            //sCmdType = wsF.GetWSData(aspCommand, inputXML); //To get the Command Type

                            //if (sCmdType.ToUpper().ToString() == "NET")
                            //{
                            //    cmdoutXML = CallMyVRMServer(aspCommand, sInXML, aspilConfigPath);
                            //    outputXML = "<myVRM><version>" + sVersion + "</version><client>" + sClient + "</client><commandname>" + aspCommand + "</commandname><command>" + cmdoutXML + "</command></myVRM>";                                
                            //}
                            //else if (sCmdType.ToUpper().ToString() == "COM")
                            //{
                            //    cmdoutXML = CallCOM(aspCommand, sInXML, comConfigPath);
                            //    outputXML = "<myVRM><version>" +sVersion + "</version><client>"+sClient+"</client><commandname>"+aspCommand+"</commandname><command>" + cmdoutXML + "</command></myVRM>";
                            //}
                            //else if(sCmdType.ToUpper().ToString() == "RTC")
                            //{
                            //    cmdoutXML = CallCOM2(aspCommand, sInXML, vrmRTCConfigPath);
                            //    outputXML = "<myVRM><version>" + sVersion + "</version><client>" + sClient + "</client><commandname>" + aspCommand + "</commandname><command>" + cmdoutXML + "</command></myVRM>";
                            //}
                            #endregion

                        }
                    }
                    else
                    {
                        outputXML += "<error><errorCode>205</errorCode><message>User is not registered in myVRM.</message></error>";
                    }
                }
                else
                {
                    // invalid input 
                    outputXML += "<error><errorCode>100</errorCode><message>Invalid input. ASPCommand = " + aspCommand + ",inputXml = " + inputXML + ". Client=" + sClient + ".</message></error>";
                }

            }
            catch (Exception e)
            {
                try
                {
                    outputXML += "<error><errorCode>100</errorCode><message>" + e.Message + "</message></error>";
                }
                catch (Exception ex)
                {
                    outputXML += "<error><errorCode>100</errorCode><message>Error in instantiating internal objects. Please contact myVRM Technical Support at 1-888-MYVRM-98 for further help. Message = " + ex.Message + "</message></error>";
                }
            }
            return outputXML;
        }
        **/
        #endregion

        #region Modified version of codes
        /// <summary>
        /// Input XML from API
        /// </summary>
        /// <param name="inputXML"></param>
        /// <returns></returns>
        public string InvokeWebservice(string inputXML)
        {
            string outputXML = "";
            string aspCommand = "";
            string orgAspCommand = ""; //FB 2038
            string sCmdType = "";
            string resultXML = "";
            string sInXML = "";
            string sVersion = "";
            string cmdoutXML = "";
            int sClient = 0; //ZD 103569
            string sLogin = "";
            string sPIMServiceType = "";//FB 2038
            string Login = "", IsAuthenticated = ""; //ZD 101308
            int iPIMServiceType = 0, userId = 0;//FB 2038 //FB 2724
            //WebServiceFactory.WSFactory wsF = null; //Merge WS in to Web
            NETFunctions obj = null; //Merge WS in to Web
            try
            {

                if (inputXML == "")
                    return "<error><errorCode>422</errorCode><message>Invalid Operation.</message></error>";

                XmlDocument xd = new XmlDocument();
                XmlNode node = null;
                obj = new NETFunctions();//Merge WS in to Web

                XmlDocument vrmXmlConfig = new XmlDocument();
                //FB 2392 start WhyGo 
                List<String> WhyGoCommandList = new List<String>();
                WhyGoCommandList.Add("SetConference"); //PushtoWhyGO
                WhyGoCommandList.Add("GetPublicRoomsAvailability");
                WhyGoCommandList.Add("GetPublicRoomsPrices");
                WhyGoCommandList.Add("GetLocationUpdate");
                WhyGoCommandList.Add("DeleteConference");
                WhyGoCommandList.Add("CreateUser");
                //FB 2392 end WhyGo
                
                vrmXmlConfig.Load(vrmConfigPath);

                xd.LoadXml(inputXML);

                //ZD 101308 Start
                node = xd.SelectSingleNode("//myVRM/client");

                //ZD 103569 changed sclient to integer variable from string
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out sClient);

                //ZD 101308 Start

                //Window Authentication START
                //if (System.Web.HttpContext.Current.User.Identity.AuthenticationType.ToString() != "" && sClient.Trim() != "05")//SSo Mode  ZD 101308
                if (System.Web.HttpContext.Current.User.Identity.AuthenticationType.ToString() != "" && sClient != (int)clientOrigin.AppleDevices) //ZD 103569 //SSo Mode  ZD 101308
                {
                    String sAuthUsr = System.Web.HttpContext.Current.User.Identity.Name;
                    string sWinUsr = "";
                    if (sAuthUsr != "")
                    {
                        Int32 iAuthPathUsr = sAuthUsr.IndexOf("\\");
                        sWinUsr = sAuthUsr.Substring(iAuthPathUsr + 1);
                        if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                        {
                            node = xd.SelectSingleNode("//myVRM/login");
                            if (node == null)
                                node = xd.CreateElement("login");
                            node.InnerText = sWinUsr.Trim();
                            xd.DocumentElement.InsertBefore(node, xd.SelectSingleNode("//myVRM/version"));

                            node = xd.SelectSingleNode("//myVRM/password");
                            if (node == null)
                                node = xd.CreateElement("password");
                            node.InnerText = sWinUsr.Trim();
                            node.InnerText = "";
                            xd.DocumentElement.InsertBefore(node, xd.SelectSingleNode("//myVRM/version"));
                            node = xd.CreateElement("IsAuthenticated");
                            node.InnerText = "Yes"; //FB 2963
                            xd.DocumentElement.InsertBefore(node, xd.SelectSingleNode("//myVRM/version"));
                            inputXML = xd.InnerXml;
                        }
                    }//Window Authentication END
                }

                node = xd.SelectSingleNode("//myVRM/login");
                if (node.InnerText != "")
                    sLogin = node.InnerXml.Trim();

                if (sLogin.IndexOf("@") <= 0)
                    sChkUsr = "ChkUsrAuthentication";

                //ZD 100196 Start

                node = xd.SelectSingleNode("//myVRM/commandname");
                if (node.InnerText != "")
                    aspCommand = node.InnerXml.Trim();

                if (sLogin == "" && aspCommand == "RoomValidation")
                    sChkUsr = "ChkRoomAuthentication";

                //ZD 100196 End

                //Merge WS in to Web START
                //ASPIL.VRMServer vrmServerObj = new VRMServer(); 
                //resultXML = vrmServerObj.Operations(aspilConfigPath, sChkUsr, inputXML); 
                resultXML = obj.CallCommand(sChkUsr, inputXML); //Authorization Validation
                //Merge WS in to Web START
                //resultXML = CallMyVRMServer(sChkUsr, inputXML, aspilConfigPath);
                if (resultXML == "")
                    return "<error><errorCode>236</errorCode><message>A system error has occurred. Please contact your myVRM system administrator and give them the following error code.</message></error>"; //ZD 102432

                xd.LoadXml(resultXML);
                if (resultXML.IndexOf("<error>") >= 0)
                    return resultXML;

                node = xd.SelectSingleNode("//organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText;

                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                    return "<error><errorCode>422</errorCode><message>Invalid Operation.</message></error>";

                //FB 2038
                node = xd.SelectSingleNode("//PIMServiceType");
                if (node != null)
                    sPIMServiceType = node.InnerText;

                Int32.TryParse(sPIMServiceType, out iPIMServiceType);
                //FB 2038

                //FB 2724 Start
                node = xd.SelectSingleNode("//LoginUserId");
                string userid = "";
                if (node != null)
                    userid = node.InnerText;

                Int32.TryParse(userid, out userId);
                //FB 2724 End

                // Retriving the Client Name
                string client = vrmXmlConfig.SelectSingleNode("//VRMConfig/Client").InnerXml.Trim().ToUpper();

                xd = null;
                xd = new XmlDocument();
                xd.LoadXml(inputXML);

                // Retriving the Command Name
                node = xd.SelectSingleNode("//myVRM/commandname");
                if (node.InnerText != "")
                    aspCommand = node.InnerXml.Trim();

                orgAspCommand = aspCommand; //FB 2038

                node = xd.SelectSingleNode("//myVRM/command");
                if (node.InnerText != "")
                    sInXML = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//myVRM/version");
                if (node.InnerText != "")
                    sVersion = node.InnerXml.Trim();

                //ZD 103569 - commented duplication of code during 103569 - Valli
                //node = xd.SelectSingleNode("//myVRM/client");
                //if (node != null && node.InnerText != "")
                //    sClient = node.InnerXml.Trim();
               

                if (aspCommand == "" || inputXML == "" || sInXML == "")
                    return "<error><errorCode>100</errorCode><message>Invalid input. ASPCommand = " + orgAspCommand + ",inputXml = " + inputXML + ". Client=" + client + ".</message></error>"; //FB 2038

                //if ((aspCommand == "SetConference" && (sClient == "07" || sClient == "7")) || (aspCommand == "TDScheduleNewConference" && (sClient == "02" || sClient == "2"))) //ZD 102195
                //if ((aspCommand == "SetConference" && (sClient == "07" || sClient == "7"))) //ZD 102578

                if (aspCommand == "SetConference" && sClient == (int)clientOrigin.Exchange) //ZD 103569 - sClient modified from string
                    aspCommand = "SetConferenceDetails";

                xd = null;
                node = null;
                string parentXml = "";
                xd = new XmlDocument();
                xd.LoadXml(sInXML);
                node = xd.FirstChild;
                parentXml = node.InnerXml;
                parentXml += "<organizationID>" + organizationID + "</organizationID>";
                if ((aspCommand == "GetOldConference" && sClient == (int)clientOrigin.OutlookGeneric) || aspCommand == "GetRoomProfile" || aspCommand == "GetAvailableRoom" ||
                    aspCommand == "SetConferenceDetails" || aspCommand == "GetOldTemplate" || aspCommand == "GetTemplate") //ZD 104388 //FB 2558 //ZD 103569 client tag added to GetRoomProfile for sending image string //ALLBUGS-156
                {
                    parentXml += "<Client>" + sClient.ToString() + "</Client>"; ///parentXml += "<Client>2</Client>"; //ZD 103596
                }
                //FB 2153
                if (aspCommand == "SaveFiles")
                {
                    parentXml += "<conferenceuploadpath>" + System.Web.HttpContext.Current.Request.MapPath(".").ToString() + "\\upload" + "</conferenceuploadpath>";
                }
                //parentXml += "<utcEnabled>1</utcEnabled>"; // FB 2014 As per Onsite request

                //FB 2724
                if (aspCommand == "RoomValidation" || aspCommand == "GetRoomConferenceMonthlyView" || aspCommand == "SetConferenceSignin" || aspCommand == "GetModifiedConference")
                {
                    parentXml += "<LoginUserId>" + userId + "</LoginUserId>";
                }


                //FB 2038
                if (aspCommand == "GetAvailableRoom")
                {
                    if (iPIMServiceType == 0)
                        aspCommand = "GetRoombyMediaService";
 
                }
                //FB 2038

                if (aspCommand == "GetAllOrgSettings" && sClient == (int)clientOrigin.OutlookGeneric) //FB 2943 //ZD 103569 - sClient modified from string "02"
                {
                    parentXml += "<isBannerImages>1</isBannerImages>";
                }
                if (aspCommand == "DeleteConference" || aspCommand == "DeleteRecurInstance" || aspCommand == "DelRecurInstanceByUID") //ZD 102644
                    parentXml += "<confOrigin>" + sClient.ToString() + "</confOrigin>"; //ZD 103569 - sClient modified from string

                xd.FirstChild.InnerXml = parentXml;
                sInXML = xd.InnerXml;

                bool hasPublicRoom = false; //FB 2392 -WhyGo
                if (aspCommand != "" && inputXML != "" && sInXML != "")//Merge WS in to Web
                {
                    //FB 2004 - Starts
                    if (aspCommand == "SetConference" || aspCommand == "SetConferenceDetails") //ZD 104235
                    {
                        if (sInXML.IndexOf("<error>") < 0)
                        {
                            xd = null;
                            xd = new XmlDocument();
                            xd.LoadXml(sInXML);

                            node = xd.SelectSingleNode("//conference/confInfo/confOrigin");
                            if (node != null)
                            {
                                node.InnerText = sClient.ToString(); //ZD 103569 - sClient modified from string
                                sInXML = xd.InnerXml;
                            }

                           //FB 2392 Start -WhyGo
                           XmlNodeList nodes = xd.SelectNodes("//conference/confInfo/publicLocationList/location");
                           if (nodes.Count > 0)
                               hasPublicRoom = true;
                           //FB 2392 End
                        }
                    }
                    //FB 2004 - End

                    //FB 2558 WhyGo - Starts
                    if (aspCommand == "GetHome" && sClient == (int)clientOrigin.OutlookGeneric) //ZD 103569 - sClient modified from string "02"
                    {
                        aspCommand = "UserAuthentication";
                    }
                    //FB 2558 WhyGo - End

                    cmdoutXML = obj.CallCommand(aspCommand, sInXML);
                    //ZD 102195 Start
                    if (aspCommand == "SetConferenceDetails")
                    {
                        if (cmdoutXML.IndexOf("<error>") < 0)
                        {
                            cmdoutXML = "<setConference>" + cmdoutXML + "</setConference>";
                        }
                    }
                    //ZD 102195 End
                    //FB 1772 - Code start
                    if (aspCommand == "DeleteRecurInstance" || aspCommand == "DelRecurInstanceByUID")
                    {
                        if (cmdoutXML.IndexOf("<error>") < 0)
                        {
                            xd = null;
                            xd = new XmlDocument();
                            xd.LoadXml(inputXML);

                            // Retriving the Command Name
                            node = xd.SelectSingleNode("//myVRM/commandname");
                            node.InnerText = "DeleteConference";

                            node = xd.SelectSingleNode("//myVRM/command");
                            node.InnerXml = cmdoutXML;

                            cmdoutXML = obj.CallCommand("DeleteConference", xd.InnerXml);
                        }
                    }
                    else if (aspCommand == "EditRecurInstance" || aspCommand == "EditRecurInstanceByUID")
                    {
                        if (cmdoutXML.IndexOf("<error>") < 0)
                        {
                            xd = null;
                            xd = new XmlDocument();
                            xd.LoadXml(inputXML);

                            // Retriving the Command Name
                            node = xd.SelectSingleNode("//myVRM/commandname");
                            node.InnerText = "SetConferenceDetails"; // ZD 104750

                            node = xd.SelectSingleNode("//myVRM/command");
                            node.InnerXml = cmdoutXML;

                            cmdoutXML = obj.CallCommand("SetConferenceDetails", cmdoutXML) ; // ZD 104750
                            if (cmdoutXML.IndexOf("<error>") < 0)
                            {
                                cmdoutXML = "<setConference>" + cmdoutXML + "</setConference>";
                            }
                        }
                    }
                    //FB 1772 - Code end


                    if (aspCommand == "DeleteConference" || aspCommand == "DeleteRecurInstance" || aspCommand == "DelRecurInstanceByUID")
                    {
                        if (cmdoutXML.IndexOf("<error>") < 0)
                        {
                            xd = null;
                            xd = new XmlDocument();
                            xd.LoadXml(cmdoutXML);

                            // Retriving the confid
                            node = xd.SelectSingleNode("//conferences/conference/confID");
                            if (node != null)
                            {

                                String inxml = "<DeleteCiscoICAL>";
                                inxml += "<UserID>11</UserID>";
                                inxml += "<ConfID>" + node.InnerXml + "</ConfID>";
                                inxml += "</DeleteCiscoICAL>";
                                obj.CallCommand("DeleteCiscoICAL", inxml);
                            }



                        }
                    }
                    
                    //FB 2363 - Start
                    if (aspCommand == "DeleteConference" || aspCommand == "DeleteRecurInstance" || aspCommand == "DelRecurInstanceByUID" || aspCommand == "SetConference")
                    {
                        if (cmdoutXML.IndexOf("<error>") < 0)
                        {
                            xd = null;
                            xd = new XmlDocument();
                            xd.LoadXml(cmdoutXML);

                            // Retriving the confid
                            node = xd.SelectSingleNode("//conferences/conference/confID");
                            if (node != null)
                            {
                                try
                                {
                                    if (Application["External"].ToString() != "")
                                    {
                                        String inEXML = "";
                                        inEXML = "<SetExternalScheduling>";
                                        inEXML += "<confID>" + node.InnerText + "</confID>";
                                        inEXML += "</SetExternalScheduling>";

                                        String outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                                    }

                                }
                                catch (Exception)
                                {
                                    
                                    // We do nothins as this is a failure to external user. Must alert the user in future.
                                }
                                
                            }
                        }
                    }

                    //FB 2363 - End



                    outputXML = "<myVRM><version>" + sVersion + "</version><client>" + sClient.ToString() + "</client><commandname>" + orgAspCommand + "</commandname><command>" + cmdoutXML + "</command></myVRM>"; //FB 2038 //ZD 103569 - sClient modified from string
                }
                else
                {
                    outputXML += "<error><message>Invalid Command.</message></error>";
                }
            }
            catch (Exception e)
            {
                try
                {
                    outputXML += "<error><errorCode>100</errorCode><message>" + e.Message + "</message></error>";
                }
                catch (Exception ex)
                {
                    outputXML += "<error><errorCode>100</errorCode><message>Error in instantiating internal objects. .Please contact VRM Technical Support at 1-888-MYVRM-98 for further help. Message = " + ex.Message + "</message></error>";
                }
            }
            return outputXML;
        }

        #endregion

        #endregion
    }
}
