/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
//Added for FB 1710 Start
using System.Collections.Generic;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
//Added for FB 1710 End
/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>


    public partial class OrganisationSettings : System.Web.UI.Page
    {

        #region protected Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblLicenseDetails;
        protected System.Web.UI.WebControls.TextBox txtLicenseKey;
        protected System.Web.UI.WebControls.TextBox txtSystemEmail;
        protected System.Web.UI.WebControls.TextBox txtApprover1;
        protected System.Web.UI.WebControls.TextBox txtApprover2;
        protected System.Web.UI.WebControls.TextBox txtApprover3;
        protected System.Web.UI.WebControls.TextBox hdnApprover1;
        protected System.Web.UI.WebControls.TextBox hdnApprover2;
        protected System.Web.UI.WebControls.TextBox hdnApprover3;
        protected System.Web.UI.WebControls.TextBox txtTestEmailId; // FB 1758
       
        protected System.Web.UI.HtmlControls.HtmlButton btnSubmit; //ZD 100420
        protected System.Web.UI.WebControls.Button btntestmail; // FB 1758
        protected System.Web.UI.WebControls.DropDownList lstBillingScheme;
        protected System.Web.UI.WebControls.DropDownList lstLDAPConnectionTimeout;
        protected System.Web.UI.WebControls.DropDownList CustomAttributeDrop;
        protected System.Web.UI.WebControls.DropDownList lstWorkingHours; //FB 2343
        protected System.Web.UI.WebControls.DropDownList lstEM7Orgsilo; //FB 2501 EM7

        //FB 2565 Starts
        //protected System.Web.UI.WebControls.CheckBox chkP2P;
        //protected System.Web.UI.WebControls.CheckBox chkAllowOver;
        protected System.Web.UI.WebControls.DropDownList drpAlloverAllocation;
        protected System.Web.UI.WebControls.DropDownList drpAllowP2PConf;
        //FB 2565 Ends
        protected System.Web.UI.HtmlControls.HtmlTableRow EntityRow;
        protected System.Web.UI.WebControls.Label LblActRooms;
        protected System.Web.UI.WebControls.Label LblActUsers;
        protected System.Web.UI.WebControls.Label LblActMCU;
        protected System.Web.UI.WebControls.Label LblFdMod;
        protected System.Web.UI.WebControls.Label LblRsMod;

        protected System.Web.UI.WebControls.Label LblNonVidRooms;
        protected System.Web.UI.WebControls.Label LblDuser;
        protected System.Web.UI.WebControls.Label LblExchUser;
        protected System.Web.UI.WebControls.Label LblMobUser; //FB 1979
        protected System.Web.UI.WebControls.Label LblWebexUser; //ZD 100221
        protected System.Web.UI.WebControls.Label LblEpts;
        protected System.Web.UI.WebControls.Label LblFacility;
        protected System.Web.UI.WebControls.Label LblApi;
        //protected System.Web.UI.WebControls.Label LblPC; //FB 2347
        protected System.Web.UI.WebControls.Label LblEnchancedMCU;//FB 2486
        //FB 2426 Start
        protected System.Web.UI.WebControls.Label LblGstRooms;
        protected System.Web.UI.WebControls.Label LblGstRoomsPerUser;
        //FB 2426 End
        protected System.Web.UI.WebControls.Label LblBJNPerUser;//ZD 104021
        protected System.Web.UI.WebControls.Label LblVMRRooms;//FB 2586
        protected System.Web.UI.WebControls.Label LblHDVRooms;//FB 2694
        protected System.Web.UI.WebControls.Label LblHDNVRooms;//FB 2694

        protected System.Web.UI.WebControls.Label lblUploadMap1;//fb 1602
        protected System.Web.UI.WebControls.Button btnRemoveMap1;
        protected System.Web.UI.WebControls.Label hdnUploadMap1;
        protected System.Web.UI.WebControls.Button btnUploadImages;
        protected myVRMWebControls.ImageControl Map1ImageCtrl;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Map1ImageDt;
        protected System.Web.UI.HtmlControls.HtmlInputFile fleMap1;

        protected DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor dxHTMLEditor; //Added for FB 1710
        // FB 1830 Starts
        protected System.Web.UI.WebControls.TextBox txtEmailLang;
        protected System.Web.UI.WebControls.DropDownList drporglang;
        protected System.Web.UI.WebControls.Button btnDefine;
        // FB 1830 Ends  

        protected System.Web.UI.WebControls.CheckBox ChkBlockEmails;//FB 1860
        protected System.Web.UI.WebControls.Button BtnBlockEmails;//FB 1860
        //FB 1849
        protected System.Web.UI.HtmlControls.HtmlTableRow trSwt; 
        //protected AjaxControlToolkit.ModalPopupExtender RoomPopUp; //FB 2719
        //protected DropDownList DrpOrganization;//FB 2719

        protected System.Web.UI.WebControls.TextBox txtCustomerName;
        protected System.Web.UI.WebControls.TextBox txtCustomerID;
        //FB 2363        
        protected System.Web.UI.WebControls.TextBox txtUsrRptDestination;
        protected System.Web.UI.WebControls.TextBox lstUsrRptFrequencyCount;
        protected System.Web.UI.WebControls.ImageButton delEmailLang; //FB 2502
        protected System.Web.UI.WebControls.DropDownList drpEmailDateFormat;  //FB 2555

        //FB 2598 Starts EnableEM7
        protected System.Web.UI.HtmlControls.HtmlTableRow trEM7OrgSetting;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEM7Organization; 
        //FB 2598 Ends EnableEM7
        //FB 2262,FB 2599 Starts
        protected System.Web.UI.WebControls.Label LblCloud; 
        protected System.Web.UI.WebControls.TextBox txtvidyoURL;
        protected System.Web.UI.WebControls.TextBox txtvidyoLogin;
        protected System.Web.UI.WebControls.TextBox txtvidyoPassword1;
        protected System.Web.UI.WebControls.TextBox txtvidyoPassword2;
        protected System.Web.UI.WebControls.TextBox txtproxyAdd;
        protected System.Web.UI.WebControls.TextBox txtvidyoPort;
        protected System.Web.UI.WebControls.Button btnvidyoPoll;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCloud;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCloudDetails;
        //FB 2262,FB 2599 Ends
		//FB 2594 Starts
        protected System.Web.UI.WebControls.Label lblPublicRoom;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblAction;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdAction;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdAction2;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdAction3;
        //FB 2594 Ends
		//protected System.Web.UI.HtmlControls.HtmlInputButton btnManageBatchRpt; //FB 2670 //ZD 100420
        protected System.Web.UI.HtmlControls.HtmlButton btnManageBatchRpt; //FB 2670 //ZD 100420
        //FB 2693 Starts
        protected System.Web.UI.WebControls.Label LblPCUser;
        //protected System.Web.UI.WebControls.Label LblBJ; ZD 104021
        protected System.Web.UI.WebControls.Label LblJabber;
        protected System.Web.UI.WebControls.Label LblLync;
        //protected System.Web.UI.WebControls.Label LblVidtel; //ZD 102004
        //FB 2693 Ends
		//FB 2659 Starts
        protected System.Web.UI.HtmlControls.HtmlInputText txtSubject;
        protected System.Web.UI.HtmlControls.HtmlTextArea txtInvitaion;
        protected System.Web.UI.HtmlControls.HtmlTableRow tdSubject;
        protected System.Web.UI.HtmlControls.HtmlTableRow tdInvitation;
		//FB 2659 End
		protected System.Web.UI.WebControls.Label LblAdvReport;//FB 2593
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFooterMsg;//FB 2681
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPasschange;//FB 3054
		//ZD 100420
        protected System.Web.UI.HtmlControls.HtmlButton btnReset;//ZD 100263
       //protected System.Web.UI.HtmlControls.HtmlButton Button1;
        //protected System.Web.UI.HtmlControls.HtmlButton Button3;        
		//ZD 100420
        protected System.Web.UI.WebControls.Label LbliControlRoom;//ZD 101098
        //ZD 101443 start
        protected System.Web.UI.WebControls.TextBox txtLDAPServerAddress;
        protected System.Web.UI.WebControls.TextBox txtLDAPAccountLogin;
        protected System.Web.UI.WebControls.TextBox txtLDAPAccountPassword1;
        protected System.Web.UI.WebControls.TextBox txtLDAPAccountPassword2;
        protected System.Web.UI.WebControls.TextBox txtLDAPServerPort;
        protected System.Web.UI.WebControls.TextBox txtLDAPSearchFilter;
        protected System.Web.UI.WebControls.TextBox txtLDAPLoginKey;
        protected System.Web.UI.WebControls.TextBox txtLDAPPrefix;
        //protected System.Web.UI.WebControls.DropDownList lstLDAPConnectionTimeout;
        protected System.Web.UI.WebControls.CheckBoxList chkLstDays;
        protected MetaBuilders.WebControls.ComboBox lstLDAPScheduleTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLDAPPW;
        protected System.Web.UI.WebControls.RegularExpressionValidator reglstLDAPScheduleTime;
        protected System.Web.UI.WebControls.DropDownList drpAuthType;

        //ZD 101443 End
        protected System.Web.UI.HtmlControls.HtmlTableCell tdManageBatchRpt; //ZD 101674
        protected System.Web.UI.HtmlControls.HtmlTableRow trActMsgDeli; //ZD 101757

        //ZD 101835
        protected System.Web.UI.WebControls.DropDownList lstArchiveConfOld;
        protected System.Web.UI.WebControls.DropDownList lstArchivePeriod;
        protected MetaBuilders.WebControls.ComboBox lstConfArchiveTime;
        //ALLDEV-833 START
        protected System.Web.UI.WebControls.TextBox txtEWSURL;
        protected System.Web.UI.WebControls.TextBox txtEWSUserName;
        protected System.Web.UI.WebControls.TextBox txtEWSPassword;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEWSDetails;
        protected System.Web.UI.WebControls.DropDownList lstEWSVersion;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEWSPassChange;
        //ALLDEV-833 END

        
        
        #endregion

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        String tformat = "hh:mm tt";
        protected Int32 orgId = 11;//FB 1849
        //MyVRMNet.Util utilObj;//FB 2681 //FB 3011

        String fName; //fb 1602
        string fileext;
        String pathName;
        myVRMNet.ImageUtil imageUtilObj = null;
        byte[] imgArray = null; 
        string footerImageName = "_footerimage.gif"; //Added for FB 1710
        string footerMessage = ""; //Added for FB 1710
        bool isMoreThanOneImg = false; //Added for FB 1710
        string testFootImgName = "_footerimage.gif"; //FB 1758
        string testMailLogoName = "mail_logo.gif"; //FB 1758
        internal int isCloudEnabled = 0; //FB 2645
        //protected DropDownList CustomAttributeDrop; Custom Attribute Fix

        protected int enableCloudInstallation = 0;//FB 2659
        protected int isLDAP = 0, isULBJN = 0; //ZD 101443 //ZD 104116
        protected System.Web.UI.WebControls.Label LblActDirectory;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveUsers, SpanBJNUsers; //ZD 104116
       
        #region Constructor
        public OrganisationSettings()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            imageUtilObj = new myVRMNet.ImageUtil(); //fb 1602
            //utilObj = new MyVRMNet.Util();//FB 2681 //FB 3011
        }

        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on PreRender
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            string js = "<script language=javascript>";
            js += "  function _fixsmartnav(){";
            js += "     if(window.__smartNav!=null){";
            js += "        var a=window.__smartNav.update.toString();";
            js += "        a=a.replace('hdm.appendChild(k);','try{hdm.appendChild(k);}catch(e){}');";
            js += "        eval('window.__smartNav.update='+a);";
            js += "        document.detachEvent('onmousemove', _fixsmartnav);";
            js += "     }";
            js += "  }";
            js += "  document.attachEvent('onmousemove', _fixsmartnav);";
            js += "  document.body.onload=_fixsmartnav;";
            js += "</script>";
            RegisterClientScriptBlock("_CdgMnk_FixSmartNavBug", js);
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            //            Response.Write(Session["sMenuMask"].ToString());
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("OrganisationSettings.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                
                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                //ZD 101835
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray; //FB 2796
                    //btnManageBatchRpt.Disabled = true;
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnManageBatchRpt.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnSubmit.Visible = false;
                    btnManageBatchRpt.Visible = false;
                    btnReset.Visible = false;
                    tdManageBatchRpt.Visible = false;//ZD 101674
                }
                //FB 2670 End
                // FB 2796 start
                else
                {
                    btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                    btnManageBatchRpt.Attributes.Add("Class", "altLongBlueButtonFormat");
                    tdManageBatchRpt.Visible = true;//ZD 101674
                }
                // FB 2796 End

                //FB 1849 - Starts
                if (!IsPostBack)
                {
                    if (Request.QueryString["c"] != null) // Added for New Menu Design
                    {
                        if (Request.QueryString["c"] != "")
                            //FB 2719 Starts
                            //if (Request.QueryString["c"].Equals("1"))
                            //{
                            //    trSwt.Attributes.Add("style", "display:none");
                            //    RoomPopUp.Show();

                            int.TryParse(Request.QueryString["c"].ToString(), out orgId);
                                obj.SetOrgSession(orgId);

                                if (Session["organizationID"] != null)
                                {
                                    Int32.TryParse(Session["organizationID"].ToString(), out orgId);

                                }
                        //}
                        //FB 2719 Ends
                    }

                    //ZD 101835
                    obj.BindTimeToListBox(lstConfArchiveTime, false, false);

                    //lstConfArchiveTime.Text =

                }
                //FB 1849 - End
                //FB 2645 Start
                if (Session["Cloud"] != null)
                    if (Session["Cloud"].ToString().Trim() == "1")
                        isCloudEnabled = 1;
                //FB 2645 End

                //FB 2659 - Starts
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString().Trim(), out enableCloudInstallation);

                if (enableCloudInstallation == 1)
                {
                    tdSubject.Visible = true;
                    tdInvitation.Visible = true;
                }
                else
                {
                    tdSubject.Visible = false;
                    tdInvitation.Visible = false;
                }

                //FB 2659 - End

                //ZD 101443 Starts
                if (Session["IsLDAP"] != null && Session["IsLDAP"].ToString() != "")
                    int.TryParse(Session["IsLDAP"].ToString(), out isLDAP);
                //ZD 101443 End
                //ZD 104116 Starts
                if (Session["SiteBJNuserLimit"] != null && Session["SiteBJNuserLimit"].ToString() != "")
                    int.TryParse(Session["SiteBJNuserLimit"].ToString(), out isULBJN);
                //ZD 104116 Ends

                if (!IsPostBack)
                {
                    try
                    {
                        //ZD 101757
                        String McuEnhanceLimit = "0";
                        if (Session["McuEnchancedLimit"] != null)
                            McuEnhanceLimit = Session["McuEnchancedLimit"].ToString();

                        if (Session["EnableActMsgDelivery"] != null)
                        {
                            if (Session["EnableActMsgDelivery"].ToString() == "0" || Int32.Parse(McuEnhanceLimit) <= 0)
                                trActMsgDeli.Visible = false;
                            else
                                trActMsgDeli.Visible = true;
                        }

                        Session["multisiloOrganizationID"] = null; //FB 2274 
                        //FB 1849 - Starts
                        //obj.BindOrganizationNames(DrpOrganization);
                        //DrpOrganization.ClearSelection(); //FB 2719

                        //if (DrpOrganization.Items.FindByValue(orgId.ToString()) != null)//FB 2719
                        //    DrpOrganization.Items.FindByValue(orgId.ToString()).Selected = true;
                        if (Session["organizationID"] != null)
                            Int32.TryParse(Session["organizationID"].ToString(), out orgId);
                        //FB 1849 - End                        
                        String inXML = "<GetOrgSettings><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "</GetOrgSettings>";
                        //Response.Write(Application["COM_ConfigPath"].ToString());
                        String outXML = obj.CallMyVRMServer("GetOrgSettings", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //Response.Write(obj.Transfer(outXML));
                        //                    Session["outxml"] = outXML;
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            Session.Add("outXML", outXML);
                            BindData();
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                        }
                        if (Request.QueryString["m"] != null)
                        {
                            if (Request.QueryString["m"].ToString().Equals("1"))
                            {
                                errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes."); //Custom attribute fixes //FB 1830 - Translation
                                errLabel.Visible = true;
                            }
                        }

                        //FB 2598 Starts EnableEM7
                        if (Session["EnableEM7"] != null)
                        {
                            if (Session["EnableEM7"].ToString() == "0")
                            {
                                trEM7Organization.Visible = false;
                                trEM7OrgSetting.Visible = false;
                            }
                            else
                            {
                                trEM7Organization.Visible = true;
                                trEM7OrgSetting.Visible = true;
                            }
                        }
                        //FB 2598 Ends
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.StackTrace);
						errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                        //errLabel.Text = "Error in Getting System Administrator Settings. Please contact VRM Administrator. ";
                        errLabel.Visible = true;
                    }
                }
                
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("Page_Load" + ex.Message);//ZD 100263

            }
        }


        private void BindData()
        {
            try
            {
                Session.Add("VidyoPW",""); //FB 3054
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(Session["outxml"].ToString());
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                XmlNodeList nodes = node.SelectNodes("//preference/accountingLogics/logic");

                DisplayMailLogoImages(node);

                int length = nodes.Count;
                int index = 0;
                lstBillingScheme.Items.Clear();
                while (index < length)
                {
                    lstBillingScheme.Items.Add(new ListItem(obj.GetTranslatedText(nodes.Item(index).SelectSingleNode("name").InnerText), nodes.Item(index).SelectSingleNode("ID").InnerText)); //FB 2272 //ZD 100288
                    index++;
                }
                try
                {
                    lstBillingScheme.Items.FindByValue(node.SelectSingleNode("//preference/accountingLogic").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace(ex.Source); }
                //FB 2565 Starts
                if (node.SelectSingleNode("//preference/billPoint2Point") != null)
                {
                    if (node.SelectSingleNode("//preference/billPoint2Point").InnerText != "")
                        drpAllowP2PConf.Items.FindByValue(node.SelectSingleNode("//preference/billPoint2Point").InnerText).Selected = true;
                }
                if (node.SelectSingleNode("//preference/allowAllocation") != null)
                {
                    if (node.SelectSingleNode("//preference/allowAllocation").InnerText != "")
                        drpAlloverAllocation.Items.FindByValue(node.SelectSingleNode("//preference/allowAllocation").InnerText).Selected = true;
                }
                //if (node.SelectSingleNode("//preference/billPoint2Point").InnerText.Equals("1"))
                //{
                //    chkP2P.Checked = true;
                //}
                //if (node.SelectSingleNode("//preference/allowAllocation").InnerText.Equals("1"))
                //{
                //    chkAllowOver.Checked = true;
                //}
                //FB 2565 Ends
                //Custom Attribute -- start
                if (node.SelectSingleNode("//preference/enableCustomAttribute") != null)
                {
                    if (node.SelectSingleNode("//preference/enableCustomAttribute").InnerText != "")
                    {
                        CustomAttributeDrop.ClearSelection();
                        CustomAttributeDrop.Items.FindByValue(node.SelectSingleNode("//preference/enableCustomAttribute").InnerText).Selected = true;
                    }
                }

                if (node.SelectSingleNode("//preference/VideoRooms") != null)
                {
                    if (node.SelectSingleNode("//preference/VideoRooms").InnerText != "")
                        LblActRooms.Text = node.SelectSingleNode("//preference/VideoRooms").InnerText;
                }

                if (node.SelectSingleNode("//preference/NonVideoRooms") != null)
                {
                    if (node.SelectSingleNode("//preference/NonVideoRooms").InnerText != "")
                        LblNonVidRooms.Text = node.SelectSingleNode("//preference/NonVideoRooms").InnerText;
                }

                if (node.SelectSingleNode("//preference/VMRRooms") != null)//FB 2586
                {
                    if (node.SelectSingleNode("//preference/VMRRooms").InnerText != "")
                        LblVMRRooms.Text = node.SelectSingleNode("//preference/VMRRooms").InnerText;
                }

                //ZD 101098 START
                if (node.SelectSingleNode("//preference/iControlRooms ") != null)
                {
                    if (node.SelectSingleNode("//preference/iControlRooms ").InnerText != "")
                        LbliControlRoom.Text = node.SelectSingleNode("//preference/iControlRooms").InnerText;
                }
                //ZD 101098 END

                //FB 2694 Start
                if (node.SelectSingleNode("//preference/HotdeskingVideoRooms") != null)
                {
                    if (node.SelectSingleNode("//preference/HotdeskingVideoRooms").InnerText != "")
                        LblHDVRooms.Text = node.SelectSingleNode("//preference/HotdeskingVideoRooms").InnerText;
                }
                if (node.SelectSingleNode("//preference/HotdeskingNonVideoRooms") != null)
                {
                    if (node.SelectSingleNode("//preference/HotdeskingNonVideoRooms").InnerText != "")
                        LblHDNVRooms.Text = node.SelectSingleNode("//preference/HotdeskingNonVideoRooms").InnerText;
                }
                //FB 2694 End

                if (node.SelectSingleNode("//preference/UserLimit") != null)
                {
                    if (node.SelectSingleNode("//preference/UserLimit").InnerText != "")
                        LblActUsers.Text = node.SelectSingleNode("//preference/UserLimit").InnerText;
                    //ZD 101525
                    if (isLDAP == 1)
                        LblActUsers.Text = "&nbsp;";
                }

                if (node.SelectSingleNode("//preference/MobileUserLimit") != null) //FB 1979
                {
                    if (node.SelectSingleNode("//preference/MobileUserLimit").InnerText != "")
                        LblMobUser.Text = node.SelectSingleNode("//preference/MobileUserLimit").InnerText;

                    if (isLDAP == 1 && enableCloudInstallation == 1)
                        LblMobUser.Text = "&nbsp;";
                }

                if (node.SelectSingleNode("//preference/WebexUserLimit") != null) //ZD 100221
                {
                    if (node.SelectSingleNode("//preference/WebexUserLimit").InnerText != "")
                        LblWebexUser.Text = node.SelectSingleNode("//preference/WebexUserLimit").InnerText;
                }

                //FB 2426 Start
                if (node.SelectSingleNode("//preference/GuestRooms") != null) 
                {
                    if (node.SelectSingleNode("//preference/GuestRooms").InnerText != "")
                        LblGstRooms.Text = node.SelectSingleNode("//preference/GuestRooms").InnerText;
                }
                if (node.SelectSingleNode("//preference/GuestRoomPerUser") != null) 
                {
                    if (node.SelectSingleNode("//preference/GuestRoomPerUser").InnerText != "")
                        LblGstRoomsPerUser.Text = node.SelectSingleNode("//preference/GuestRoomPerUser").InnerText;
                }
                //FB 2426 End
                //ZD 104021 Start
                if (node.SelectSingleNode("//preference/BluejeanPerUser") != null)
                {
                    if (node.SelectSingleNode("//preference/BluejeanPerUser").InnerText != "")
                        LblBJNPerUser.Text = node.SelectSingleNode("//preference/BluejeanPerUser").InnerText;

                    if (isULBJN == -2) //ZD 104116
                        LblBJNPerUser.Text = "&nbsp;";
                }
                //ZD 104021 End
                if (node.SelectSingleNode("//preference/ExchangeUserLimit") != null)
                {
                    if (node.SelectSingleNode("//preference/ExchangeUserLimit").InnerText != "")
                        LblExchUser.Text = node.SelectSingleNode("//preference/ExchangeUserLimit").InnerText;

                    if (isLDAP == 1 && enableCloudInstallation == 1)
                        LblExchUser.Text = "&nbsp;";
                }

                if (node.SelectSingleNode("//preference/DominoUserLimit") != null)
                {
                    if (node.SelectSingleNode("//preference/DominoUserLimit").InnerText != "")
                        LblDuser.Text = node.SelectSingleNode("//preference/DominoUserLimit").InnerText;
                }

                if (node.SelectSingleNode("//preference/MCULimit") != null)
                {
                    if (node.SelectSingleNode("//preference/MCULimit").InnerText != "")
                        LblActMCU.Text = node.SelectSingleNode("//preference/MCULimit").InnerText;
                }

                if (node.SelectSingleNode("//preference/MCUEnchancedLimit") != null)//FB 2486
                {
                    if (node.SelectSingleNode("//preference/MCUEnchancedLimit").InnerText != "")
                        LblEnchancedMCU.Text = node.SelectSingleNode("//preference/MCUEnchancedLimit").InnerText;
                }
                
                if (node.SelectSingleNode("//preference/EnableCatering") != null)
                {
                    if (node.SelectSingleNode("//preference/EnableCatering").InnerText == "1")
                        LblFdMod.Text = obj.GetTranslatedText("Enabled");//FB 1830 - Translation
                }
                if (node.SelectSingleNode("//preference/EnableHouseKeeping") != null)
                {
                    if (node.SelectSingleNode("//preference/EnableHouseKeeping").InnerText == "1")
                        LblRsMod.Text = obj.GetTranslatedText("Enabled");//FB 1830 - Translation
                }

                if (node.SelectSingleNode("//preference/EnableFacilites") != null)
                {
                    if (node.SelectSingleNode("//preference/EnableFacilites").InnerText == "1")
                        LblFacility.Text = obj.GetTranslatedText("Enabled");//FB 1830 - Translation
                }

                if (node.SelectSingleNode("//preference/EnableAPIs") != null)
                {
                    if (node.SelectSingleNode("//preference/EnableAPIs").InnerText == "1")
                        LblApi.Text = obj.GetTranslatedText("Enabled");//FB 1830 - Translation
                }
                //FB 2693 Starts
                if (node.SelectSingleNode("//preference/PCUserLimit") != null)
                {
                    if (node.SelectSingleNode("//preference/PCUserLimit").InnerText != "")
                        LblPCUser.Text = node.SelectSingleNode("//preference/PCUserLimit").InnerText;
                }
                //ZD 104021
                //if (node.SelectSingleNode("//preference/EnableBlueJeans") != null)
                //{
                //    if (node.SelectSingleNode("//preference/EnableBlueJeans").InnerText == "1")
                //        LblBJ.Text = obj.GetTranslatedText("Enabled");
                //}
                if (node.SelectSingleNode("//preference/EnableJabber") != null)
                {
                    if (node.SelectSingleNode("//preference/EnableJabber").InnerText == "1")
                        LblJabber.Text = obj.GetTranslatedText("Enabled");
                }
                if (node.SelectSingleNode("//preference/EnableLync") != null)
                {
                    if (node.SelectSingleNode("//preference/EnableLync").InnerText == "1")
                        LblLync.Text = obj.GetTranslatedText("Enabled");
                }
                //ZD 102004
                //if (node.SelectSingleNode("//preference/EnableVidtel") != null)
                //{
                //    if (node.SelectSingleNode("//preference/EnableVidtel").InnerText == "1")
                //        LblVidtel.Text = obj.GetTranslatedText("Enabled");
                //}
                //FB 2693 Ends
                //FB 2593 Start
                if (node.SelectSingleNode("//preference/EnableAdvancedReport") != null)
                {
                    if (node.SelectSingleNode("//preference/EnableAdvancedReport").InnerText == "1")
                        LblAdvReport.Text = obj.GetTranslatedText("Enabled");
                }
                //FB 2593 End

                //if (node.SelectSingleNode("//preference/EnablePC") != null) //FB 2347
                //{
                //    if (node.SelectSingleNode("//preference/EnablePC").InnerText == "1")
                //        LblPC.Text = obj.GetTranslatedText("Enabled");
                //}
                //FB 2599 Starts
                if (node.SelectSingleNode("//preference/EnableCloud") != null) //FB 2262
                {
                    if (node.SelectSingleNode("//preference/EnableCloud").InnerText == "1")
                    {
                        LblCloud.Text = obj.GetTranslatedText("Enabled");
                        //if(Session["OrganizationsLimit"].ToString().Trim() == "0" || Session["OrganizationsLimit"].ToString().Trim() == "1")
                        trCloud.Visible = true;
                        trCloudDetails.Visible = true;
                    }
                }
                //FB 2599 End
                if (node.SelectSingleNode("//preference/EnablePublicRoom") != null) //FB 2594
                {
                    if (node.SelectSingleNode("//preference/EnablePublicRoom").InnerText == "1")
                        lblPublicRoom.Text = obj.GetTranslatedText("Enabled");
                }

                if (node.SelectSingleNode("//preference/EndPoints") != null)
                {
                    if (node.SelectSingleNode("//preference/EndPoints").InnerText != "")
                        LblEpts.Text = node.SelectSingleNode("//preference/EndPoints").InnerText;
                }
                //FB 1830 starts
                Session.Remove("OrgEmailLangID"); //FB 2283
                if (node.SelectSingleNode("//preference/OrgEmailLanguageID").InnerText.Trim() != "0")
                {
                    txtEmailLang.Text = node.SelectSingleNode("//preference/OrgEmailLanguageName").InnerText;
                    Session["OrgEmailLangID"] = node.SelectSingleNode("//preference/OrgEmailLanguageID").InnerText;
                }

                //FB 2502 Starts
                //ZD 100580- Issue with language of myVRM e-mails START
                drporglang.Attributes.Add("onchange", "fnHandleChangeLang();");
                delEmailLang.Visible = false;
                if (Session["OrgEmailLangID"] != null)
                {
                    if (Session["OrgEmailLangID"].ToString() != "")
                    {
                        if (Session["OrgEmailLangID"].ToString() == "0")
                            delEmailLang.Visible = false;
                        else
                            delEmailLang.Visible = true;
                    }
                }
                //ZD 100580 END
                //FB 2502 Ends

                //FB 2555 Starts
                if (node.SelectSingleNode("//preference/EmailDateFormat") != null)
                {
                    if (node.SelectSingleNode("//preference/EmailDateFormat").InnerText != "")
                        drpEmailDateFormat.Items.FindByValue(node.SelectSingleNode("//preference/EmailDateFormat").InnerText).Selected = true;
                }
                //FB 2555 Ends

                //FB 2343
                if (node.SelectSingleNode("//preference/WorkingHours") != null)
                {
                    if (node.SelectSingleNode("//preference/WorkingHours").InnerText != "")
                    {
                        lstWorkingHours.ClearSelection();
                        lstWorkingHours.Items.FindByValue(node.SelectSingleNode("//preference/WorkingHours").InnerText.Trim()).Selected = true;
                    }
                }

                //FB 2363 - Start
                if (node.SelectSingleNode("//preference/CustomerName") != null)
                {
                    if (node.SelectSingleNode("//preference/CustomerName").InnerText != "")
                        txtCustomerName.Text = node.SelectSingleNode("//preference/CustomerName").InnerText;
                }

                if (node.SelectSingleNode("//preference/CustomerID") != null)
                {
                    if (node.SelectSingleNode("//preference/CustomerID").InnerText != "")
                        txtCustomerID.Text = node.SelectSingleNode("//preference/CustomerID").InnerText;
                }


                //FB 2262,//FB 2599 - Starts
                if (node.SelectSingleNode("//preference/VidyoSettings/VidyoURL") != null)
                {
                    if (node.SelectSingleNode("//preference/VidyoSettings/VidyoURL").InnerText != "")
                        txtvidyoURL.Text = node.SelectSingleNode("//preference/VidyoSettings/VidyoURL").InnerText;
                }

                if (node.SelectSingleNode("//preference/VidyoSettings/VidyoLogin") != null)
                {
                    if (node.SelectSingleNode("//preference/VidyoSettings/VidyoLogin").InnerText != "")
                        txtvidyoLogin.Text = node.SelectSingleNode("//preference/VidyoSettings/VidyoLogin").InnerText;
                }
                if (node.SelectSingleNode("//preference/VidyoSettings/VidyoPassword") != null)
                {
                    //FB 3054 Starts
                    
                    if (node.SelectSingleNode("//preference/VidyoSettings/VidyoPassword").InnerText != "")
                    {
                        Session.Remove("VidyoPW"); //ZD 101117
                        //txtvidyoPassword1.Attributes.Add("value", ns_MyVRMNet.vrmPassword.Vidyo);
                        //txtvidyoPassword2.Attributes.Add("value", ns_MyVRMNet.vrmPassword.Vidyo); 
                        Session["VidyoPW"] = node.SelectSingleNode("//preference/VidyoSettings/VidyoPassword").InnerText.Trim();
                    }
                        //txtvidyoPassword1.Text = node.SelectSingleNode("//preference/VidyoSettings/VidyoPassword").InnerText;
                   // txtvidyoPassword1.Attributes.Add("value", node.SelectSingleNode("//preference/VidyoSettings/VidyoPassword").InnerText);
                    //FB 3054 Ends
                }
              
                if (node.SelectSingleNode("//preference/VidyoSettings/ProxyAdd") != null)
                {
                    if (node.SelectSingleNode("//preference/VidyoSettings/ProxyAdd").InnerText != "")
                        txtproxyAdd.Text = node.SelectSingleNode("//preference/VidyoSettings/ProxyAdd").InnerText;
                }
                if (node.SelectSingleNode("//preference/VidyoSettings/VidyoPort") != null)
                {
                    if (node.SelectSingleNode("//preference/VidyoSettings/VidyoPort").InnerText != "")
                        txtvidyoPort.Text = node.SelectSingleNode("//preference/VidyoSettings/VidyoPort").InnerText;
                }

                //ZD 101443 Starts
                if (isLDAP == 1)
                {
                    LblActDirectory.Text = obj.GetTranslatedText("Enabled");
                    SpanActiveUsers.InnerText = " (" + obj.GetTranslatedText("Active directory enabled") + ")";
                    SpanActiveUsers.Visible = true;
                }
                else
                {
                    LblActDirectory.Text = obj.GetTranslatedText("Disabled");
                    SpanActiveUsers.InnerText = "";
                    SpanActiveUsers.Visible = false;
                }
                //ZD 101443 End
                //ZD 104116 Starts
                if (isULBJN == -2)
                {
                    SpanBJNUsers.InnerText = " (" + obj.GetTranslatedText("Limited to Active Users") + ")";
                    SpanBJNUsers.Visible = true;
                }
                else
                {
                    SpanBJNUsers.InnerText = "";
                    SpanBJNUsers.Visible = false;
                }
                //ZD 104116 End

                //FB 2262,//FB 2599 Ends

                // FB 2474
                //Commented this, because not deliver for this Phase II delivery
                //if (node.SelectSingleNode("//preference/RptDestination") != null)
                //{
                //    if (node.SelectSingleNode("//preference/RptDestination").InnerText != "")
                //        txtUsrRptDestination.Text = node.SelectSingleNode("//preference/RptDestination").InnerText;
                //}

                //if (node.SelectSingleNode("//preference/FrequencyCount") != null)
                //{
                //    if (node.SelectSingleNode("//preference/FrequencyCount").InnerText != "")
                //        lstUsrRptFrequencyCount.Text = node.SelectSingleNode("//preference/FrequencyCount").InnerText;
                //}

                //FB 2363 - End
                
                Session.Remove("orglanguageID");
                obj.GetLanguages(drporglang);
                if (node.SelectSingleNode("//preference/OrgLanguage") != null)
                {
                    drporglang.ClearSelection();
                    if (node.SelectSingleNode("//preference/OrgLanguage").InnerText != "")
                        drporglang.Items.FindByValue(node.SelectSingleNode("//preference/OrgLanguage").InnerText).Selected = true;
                    else
                    {
                        if (drporglang.Items.Count > 1)
                            drporglang.Items.FindByValue("1").Selected = true;
                    }
                }
                //FB 1830 ends

                //Fb 1936
                txtApprover1.Text = "";
                hdnApprover1.Text = "";
                txtApprover2.Text = "";
                hdnApprover2.Text = "";
                txtApprover3.Text = "";
                hdnApprover3.Text = "";
                //FB 1936


                XmlNodeList nodes1 = node.SelectNodes("//preference/approvers/approver");
                if (nodes1.Count >= 1)
                {
                    txtApprover1.Text = nodes1.Item(0).SelectSingleNode("firstName").InnerText + " " + nodes1.Item(0).SelectSingleNode("lastName").InnerText;
                    hdnApprover1.Text = nodes1.Item(0).SelectSingleNode("ID").InnerText;
                }
                if (nodes1.Count >= 2)
                {
                    txtApprover2.Text = nodes1.Item(1).SelectSingleNode("firstName").InnerText + " " + nodes1.Item(1).SelectSingleNode("lastName").InnerText;
                    hdnApprover2.Text = nodes1.Item(1).SelectSingleNode("ID").InnerText;
                }
                if (nodes1.Count >= 3)
                {
                    txtApprover3.Text = nodes1.Item(2).SelectSingleNode("firstName").InnerText + " " + nodes1.Item(2).SelectSingleNode("lastName").InnerText;
                    hdnApprover3.Text = nodes1.Item(2).SelectSingleNode("ID").InnerText;
                }
                //FB 2594 Starts
                if (Session["EnablePublicRooms"].ToString() == "1")
                {
                    tdAction.Visible = false;
                    tdAction2.Visible = false;
                    tdAction3.Visible = false;
                    tdlblAction.Visible = false;

                    hdnApprover1.Text = "11";
                    hdnApprover2.Text = "";
                    hdnApprover3.Text = "";

                    txtApprover2.Text = "";
                    txtApprover3.Text = "";
                }
                //FB 2594 Ends
                DisplayFooterImage(node); //Added for FB 1710  
                
                if (node.SelectSingleNode("//preference/MailBlocked") != null)// FB 1860
                {
                    if (node.SelectSingleNode("//preference/MailBlocked").InnerText == "1")
                    {
                        ChkBlockEmails.Checked = true;
                        BtnBlockEmails.Attributes.Add("style", "display:block");
                    }
                }

                //FB 2501 EM7 Starts
                if (node.SelectSingleNode("//preference/EM7Organization/Profile") != null)
                {
                    nodes = node.SelectNodes("//preference/EM7Organization/Profile");
                    length = nodes.Count;
                    index = 0;
                    lstEM7Orgsilo.Items.Clear();
                    lstEM7Orgsilo.Items.Add(new ListItem(obj.GetTranslatedText("Please Select..."), "-1"));
                    while (index < length)
                    {
                        lstEM7Orgsilo.Items.Add(new ListItem(nodes.Item(index).SelectSingleNode("Name").InnerText, nodes.Item(index).SelectSingleNode("ID").InnerText));
                        index++;
                    }
                }
                if (node.SelectSingleNode("//preference/EM7Organization/EM7SelectedSilo") != null)
                    lstEM7Orgsilo.Items.FindByValue(node.SelectSingleNode("//preference/EM7Organization/EM7SelectedSilo").InnerText).Selected = true;
                //FB 2501 EM7 Ends

                //FB 2659 - Starts
                if (node.SelectSingleNode("//preference/DefaultSubject") != null)
                {
                    txtSubject.Value = node.SelectSingleNode("//preference/DefaultSubject").InnerText.Trim().Replace("&amp;", "&").Replace("&gt;", ">").Replace("&lt;", "<");
                }
                if (node.SelectSingleNode("//preference/DefaultInvitation") != null)
                {
                    txtInvitaion.Value = node.SelectSingleNode("//preference/DefaultInvitation").InnerText.Trim().Replace("&amp;", "&").Replace("&gt;", ">").Replace("&lt;", "<");
                }
                //FB 2695 - End
                //ZD 101443 start
                if (node.SelectSingleNode("//preference/LDAP").InnerXml != "") //FB 2027
                {
                    txtLDAPServerAddress.Text = node.SelectSingleNode("//preference/LDAP/serverAddress").InnerText;
                    txtLDAPServerPort.Text = node.SelectSingleNode("//preference/LDAP/portNo").InnerText;
                    txtLDAPAccountLogin.Text = node.SelectSingleNode("//preference/LDAP/loginName").InnerText;
                    //FB 3054 STarts
                    //txtLDAPAccountPassword1.Attributes.Add("value", ns_MyVRMNet.vrmPassword.LDAP);
                    //txtLDAPAccountPassword2.Attributes.Add("value", ns_MyVRMNet.vrmPassword.LDAP);
                    Session.Remove("LDAPPW");
                    if (node.SelectSingleNode("//preference/LDAP/loginPassword") != null)
                        Session["LDAPPW"] = node.SelectSingleNode("//preference/LDAP/loginPassword").InnerText.Trim();
                    //FB 3054 Starts
                    txtLDAPLoginKey.Text = node.SelectSingleNode("//preference/LDAP/loginkey").InnerText;
                    txtLDAPSearchFilter.Text = node.SelectSingleNode("//preference/LDAP/searchfilter").InnerText;
                    txtLDAPPrefix.Text = node.SelectSingleNode("//preference/LDAP/LDAPPrefix").InnerText.Trim();
                    String schDays = "";
                    try
                    {
                        schDays = node.SelectSingleNode("//preference/LDAP/scheduler/Days").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    if (schDays.IndexOf(",") > 0)
                        for (int i = 0; i < schDays.Split(',').Length; i++)
                            chkLstDays.Items.FindByValue(schDays.Split(',')[i].Trim()).Selected = true;
                    else if (schDays.Length.Equals(1))
                        chkLstDays.Items.FindByValue(schDays.Trim()).Selected = true;
                    try
                    {
                        lstLDAPScheduleTime.Text = DateTime.Parse(node.SelectSingleNode("//preference/LDAP/scheduler/Time").InnerText).ToString(tformat);
                    }
                    //Code Modified for Synchronization Time Issues - Start
                    catch (Exception)
                    {
                        if (lstLDAPScheduleTime.Text.Trim().Equals(""))
                            lstLDAPScheduleTime.Text = lstLDAPScheduleTime.Items[0].Text;
                    }
                    //Code Modified for Synchronization Time Issues - End
                    ListItem temp;
                    temp = lstLDAPConnectionTimeout.Items.FindByValue(node.SelectSingleNode("//preference/LDAP/connectionTimeout").InnerText);
                    if (temp != null)
                        temp.Selected = true;

                    //FB 2993 LDAP START
                    temp = drpAuthType.Items.FindByValue(node.SelectSingleNode("//preference/LDAP/AuthenticationType").InnerText);
                    if (temp != null)
                        temp.Selected = true;
                    //FB 2993 LDAP END
                }
                if (txtLDAPServerPort.Text.Trim().Equals(""))
                    txtLDAPServerPort.Text = "389";
                if (lstLDAPScheduleTime.Text.Trim().Equals(""))//FB 2027
                    lstLDAPScheduleTime.Text = lstLDAPScheduleTime.Items[0].Text;

                //ALLDEV-833 START
                if (node.SelectSingleNode("//preference/ExchangeCredentials").InnerXml != "") 
                {
                    ListItem temp;
                    txtEWSURL.Text = node.SelectSingleNode("//preference/ExchangeCredentials/EWSURL").InnerText;
                    txtEWSUserName.Text = node.SelectSingleNode("//preference/ExchangeCredentials/EWSUsername").InnerText;
                    Session.Remove("userEWSPW");
                    Session["userEWSPW"] = node.SelectSingleNode("//preference/ExchangeCredentials/EWSPassword").InnerText;
                    //txtEWSPassword.Text = node.SelectSingleNode("//preference/ExchangeCredentials/EWSPassword").InnerText;
                    temp = lstEWSVersion.Items.FindByValue(node.SelectSingleNode("//preference/ExchangeCredentials/EWSVersion").InnerText);
                    if (temp != null)
                        temp.Selected = true;
                }
                //ALLDEV-833 END

                GetConfArchiveConfiguration(null, null);
            }
            //ZD 101443 End
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            InitializeUIComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.Load += new System.EventHandler(this.Page_Load);

        }

        private void InitializeUIComponent()
        {

        }
        #endregion

        #region btnSubmit_Click
        /// <summary>
        /// btnSubmit_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //FB 2594 Starts
                if (Session["EnablePublicRooms"].ToString() == "1")
                {
                    hdnApprover1.Text = "11";
                    hdnApprover2.Text = "";
                    hdnApprover3.Text = "";
                }
                //FB 2594 Ends
                //Response.Write("here<p>" + Application["COM_ConfigPath"]);
                errLabel.Visible = true;
                errLabel.Text = Page.IsValid.ToString();
                string xmldoc, temp = "0";
                xmldoc = "<SetOrgSettings>";
                xmldoc += obj.OrgXMLElement();
                xmldoc += "<userID>" + Session["userID"].ToString() + "</userID><preference>";
                xmldoc += "     <accountingLogic>" + lstBillingScheme.SelectedValue.ToString() + "</accountingLogic>";
                //FB 2565 Starts
                //if (chkP2P.Checked == true) temp = "1";
                //else temp = "0";
                xmldoc += "     <billPoint2Point>" + drpAllowP2PConf.SelectedValue.ToString() + "</billPoint2Point>";
                //if (chkAllowOver.Checked == true) temp = "1";
                //else temp = "0";
                xmldoc += "     <allowAllocation>" + drpAlloverAllocation.SelectedValue.ToString()+ "</allowAllocation>";
                //FB 2565 Ends
                xmldoc += "     <enableCustomAttribute>" + CustomAttributeDrop.SelectedValue + "</enableCustomAttribute>";
                xmldoc += "     <approvers>";
                xmldoc += "         <responseMsg></responseMsg>";
                xmldoc += "         <responseTime></responseTime>";
                xmldoc += "         <approver>";
                xmldoc += "             <ID>" + hdnApprover1.Text.ToString() + "</ID>";
                xmldoc += "         </approver>";
                xmldoc += "         <approver>";
                xmldoc += "             <ID>" + hdnApprover2.Text.ToString() + "</ID>";
                xmldoc += "         </approver>";
                xmldoc += "         <approver>";
                xmldoc += "             <ID>" + hdnApprover3.Text.ToString() + "</ID>";
                xmldoc += "         </approver>";
                xmldoc += "     </approvers>";
                xmldoc += "  <MailLogoImage>" + Map1ImageDt.Value.Trim() + "</MailLogoImage>";
                //Added for FB 1710 Starts
                isMoreThanOneImg = false;
                FooterMessage();
                if (isMoreThanOneImg)
                {
                  errLabel.Text = obj.GetTranslatedText("Only one image can be uploaded for footer information.");//FB 1830 - Translation
                  errLabel.Visible = true;
                  return;
                }
                xmldoc += footerMessage;
                //Added for FB 1710 Ends
                // FB 1830  start
                if (Session["OrgEmailLangID"] != null)//FB 2283
                    xmldoc += "    <OrgEmailLanguage>" + Session["OrgEmailLangID"].ToString() + " </OrgEmailLanguage>";
                else
                    xmldoc += "    <OrgEmailLanguage></OrgEmailLanguage>";
                xmldoc += " <OrgLanguage>" + drporglang.SelectedValue + "</OrgLanguage>";
                // FB 1830 end
                xmldoc += " <EmailDateFormat>" + drpEmailDateFormat.SelectedValue + "</EmailDateFormat>"; //FB 2555
                if (ChkBlockEmails.Checked)//FB 1860
                    xmldoc += "<MailBlocked>1</MailBlocked>";
                else
                    xmldoc += "<MailBlocked>0</MailBlocked>";

                xmldoc += "<WorkingHours>" + lstWorkingHours.SelectedValue + "</WorkingHours>"; //FB 2343

                //FB 2363
                if (Application["External"].ToString() != "")
                {
                    xmldoc += "<CustomerName>" + txtCustomerName.Text + "</CustomerName>";
                    xmldoc += "<CustomerID>" + txtCustomerID.Text + "</CustomerID>";

                    //Commented for this, because not deliver for this Phase II delivery
                    //xmldoc += "<RptDestination>" + txtUsrRptDestination.Text + "</RptDestination>";
                    //xmldoc += "<FrequencyCount>" + lstUsrRptFrequencyCount.Text + "</FrequencyCount>";
                    //xmldoc += "<FrequencyType>4</FrequencyType>";
                    xmldoc += "<Type>O</Type>";
                }
                //FB 2501 EM7 Starts
                xmldoc += "<EM7OrgID>" + lstEM7Orgsilo.SelectedValue + "</EM7OrgID>";
                //FB 2501 EM7 Ends

                //FB 2645,//FB 2599 Starts
                if (isCloudEnabled == 1) 
                {
                    xmldoc += "<VidyoSettings>";
                    xmldoc += "<VidyoURL>" + txtvidyoURL.Text + "</VidyoURL>";
                    xmldoc += "<VidyoLogin>" + txtvidyoLogin.Text + "</VidyoLogin>";
                    //FB 3054 Start
                    if (hdnPasschange.Value != "" && hdnPasschange.Value == "true")
                        xmldoc += "<VidyoPassword>" + Password(txtvidyoPassword1.Text) + "</VidyoPassword>";
                    else
                        xmldoc += "<VidyoPassword>" + Session["VidyoPW"].ToString() + "</VidyoPassword>";
                    //FB 3054 Ends
                    xmldoc += "<ProxyAdd>" + txtproxyAdd.Text + "</ProxyAdd>";
                    xmldoc += "<VidyoPort>" + txtvidyoPort.Text + "</VidyoPort>";
                    xmldoc += "</VidyoSettings>";
                }
                //FB 2262,//FB 2599 Ends
                //FB 2659 - Starts
                xmldoc += "<DefaultSubject>" + txtSubject.Value.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;") + "</DefaultSubject>";
                xmldoc += "<DefaultInvitation>" + txtInvitaion.Value.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;") + "</DefaultInvitation>";
                //FB 2659 - End
                //ZD 101443 start
                xmldoc+="<LDAP>";
                xmldoc+="<serverAddress>" + txtLDAPServerAddress.Text.ToString() + "</serverAddress>";
                xmldoc+="<loginName>" + txtLDAPAccountLogin.Text.ToString() + "</loginName>";
                //FB 3054 Starts
                if (hdnLDAPPW.Value != "" && hdnLDAPPW.Value == "true")
                    xmldoc+="<loginPassword>" + Password(txtLDAPAccountPassword1.Text.ToString()) + "</loginPassword>";

                else
                    xmldoc+="<loginPassword>" + Session["LDAPPW"].ToString() + "</loginPassword>";
                //FB 3054 Ends
                xmldoc +="<portNo>" + txtLDAPServerPort.Text.ToString() + "</portNo>";
                xmldoc +="<connectionTimeout>" + lstLDAPConnectionTimeout.SelectedValue.ToString() + "</connectionTimeout>";
                xmldoc+="<scheduler>";
                //FB 2462 START
                if (!Session["timeFormat"].ToString().Equals("2"))
                {
                    xmldoc+="<Time>" + DateTime.Now.ToShortDateString() + " " + DateTime.Parse(lstLDAPScheduleTime.Text).ToString("hh:mm tt") + "</Time>";
                }
                {
                    xmldoc +="<Time>" + myVRMNet.NETFunctions.ChangeTimeFormat(lstLDAPScheduleTime.Text) + "</Time>";
                }
                //FB 2462 END
                xmldoc+="<Days>";

                String strTemp = "";
                foreach (ListItem li in chkLstDays.Items)
                    if (li.Selected)
                        strTemp += li.Value + ",";
                if (strTemp.Length > 0)
                    strTemp = strTemp.Substring(0, strTemp.Length - 1);

                xmldoc +=strTemp.Trim();
                xmldoc +="</Days>";
                xmldoc +="</scheduler>";
                xmldoc +="<loginkey>" + txtLDAPLoginKey.Text.ToString() + "</loginkey>";
                //FB 2462 START
                if (!Session["timeFormat"].ToString().Equals("2"))
                {
                    xmldoc+="<synctime>" + DateTime.Parse(lstLDAPScheduleTime.Text).ToString("hh:mm tt") + "</synctime>";
                }
                else
                {
                    xmldoc+="<synctime>" + myVRMNet.NETFunctions.ChangeTimeFormat(lstLDAPScheduleTime.Text) + "</synctime>";
                }
                //FB 2462 END
                xmldoc+="<searchfilter>" + txtLDAPSearchFilter.Text.ToString().Replace("&", "&amp;") + "</searchfilter>";
                xmldoc+="<LDAPPrefix>" + txtLDAPPrefix.Text.Trim() + "</LDAPPrefix>";
                xmldoc+="<AuthenticationType>" + drpAuthType.SelectedValue.ToString() + "</AuthenticationType>";//FB 2993 LDAP
                xmldoc+="</LDAP>";
                //ZD 101443 end
                //ALLDEV-833 START
                xmldoc += "<ExchangeCredentials>";
                xmldoc += "<EWSURL>" + txtEWSURL.Text.ToString() + "</EWSURL>";
                xmldoc += "<EWSUsername>" + txtEWSUserName.Text.ToString() + "</EWSUsername>";
                if (hdnEWSPassChange.Value != "" && hdnEWSPassChange.Value == "true")
                    xmldoc += "<EWSPassword>" + Password(txtEWSPassword.Text) + "</EWSPassword>";
                else
                    xmldoc += "<EWSPassword>" + Session["userEWSPW"].ToString() + "</EWSPassword>";
                //xmldoc += "<EWSPassword>" + txtEWSPassword.Text.ToString() + "</EWSPassword>";
                xmldoc += "<EWSVersion>" + lstEWSVersion.SelectedValue.ToString() + "</EWSVersion>";
                xmldoc += "</ExchangeCredentials>";
                //ALLDEV-833 END

                xmldoc += "</preference></SetOrgSettings>";
                //Response.Write(obj.Transfer(xmldoc));
                string outxml = obj.CallMyVRMServer("SetOrgSettings", xmldoc, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write("<BR>" + obj.Transfer(outxml));
                //Response.End();
                if (outxml.Length > 0)
                    if (outxml.IndexOf("<error>") >= 0)
                    {
                        errLabel.Visible = true;
                        /* *** Code added for FB 1425 QA Bug -Start *** */

                        if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                        {
                            errLabel.Text = obj.ShowErrorMessage(outxml);
                            if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                                errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                        }
                        else
                            /* *** Code added for FB 1425 QA Bug -End *** */
                            errLabel.Text = obj.ShowErrorMessage(outxml);


                        BindData();// FB 1936
                    }
                    else
                    {
                        //Custom attribute fixes
                        //Session.Add("errMsg", "Operation Successful!");

                        if (obj == null)
                            obj = new myVRMNet.NETFunctions();

                        

                        if(Session["organizationID"] != null)
                        {
                            if(Session["organizationID"].ToString() != "")
                                Int32.TryParse(Session["organizationID"].ToString(),out orgId);
                        }

                        obj.SetOrgSession(orgId);

                        Response.Redirect("OrganisationSettings.aspx?m=1");
                    }
            }
            catch (Exception ex)
            {
                /* *** Code added for FB 1425 QA Bug -Start *** */

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                    if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                    {
                        errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                        errLabel.Visible = true;
                    }
                }
                else
                {
                    /* *** Code added for FB 1425 QA Bug -End *** */
                    errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                    errLabel.Visible = true;
                } /* *** Code added for FB 1425 QA Bug **/
            }
            //Response.End();
        }
        #endregion
        
        //Method added for FB 1758 - Test organization mail
        #region TestEmailConnection
        /// <summary>
        /// TestEmailConnection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TestEmailConnection(Object sender, EventArgs e)
        {
            try
            {
                string orgid = Session["organizationID"].ToString().Trim();

                if (txtTestEmailId.Text.Trim() == "")
                {
                    errLabel.Text = obj.GetTranslatedText("Please enter email address for test mail");//FB 1830 - Translation
                    errLabel.Visible = true;
                    return;
                }

                String inXML = "";
                inXML += "<TestOrgEmail>";
                inXML += obj.OrgXMLElement();
                inXML += "<userid>" + Session["userEmail"].ToString() + "</userid>";
                inXML += "<testemailid>" + txtTestEmailId.Text.Trim() + "</testemailid>";
                
                isMoreThanOneImg = false;

                string footerInfo = GetFooterMessage();
                
                if (isMoreThanOneImg)
                {
                    errLabel.Text = obj.GetTranslatedText("Only one image can be uploaded for footer information.");//FB 1830 - Translation
                    errLabel.Visible = true;
                    return;
                }
                inXML += footerInfo;

                inXML += "</TestOrgEmail>";
                log.Trace(inXML);

                String outXML = obj.CallCOM2("TestOrgEmail", inXML, Application["RTC_ConfigPath"].ToString());
                log.Trace(outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                }
                errLabel.Visible = true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

    //Method added for FB 1710 - This method is modified for FB 1830
       #region FooterMessage
    /// <summary>
    /// FooterMessage
    /// </summary>
    private void FooterMessage()
    {
        try
        {
            String fmText, imgTag, temp, imgpath;
            String picfilename = "";
            String srcLine = "";

            //fmText = utilObj.ReplaceInXMLSpecialCharacters(hdnFooterMsg.Value);//FB 2681 //FB 3011
            fmText = dxHTMLEditor.Html;

            if (fmText.IndexOf("<img") != -1)
            {
                imgTag = fmText.Substring(fmText.IndexOf("<img"));
                temp = imgTag.Substring(imgTag.IndexOf("/>"));

                if (temp.IndexOf("<img") > 0)
                {
                    isMoreThanOneImg = true;
                    return;
                }

                imgTag = imgTag.Replace(temp, "/>");
                String[] imgArr = imgTag.Split(' ');

                for (int lp = 0; lp < imgArr.Length; lp++)
                {
                    if (imgArr[lp].ToLower().IndexOf("src") >= 0)
                    {
                        srcLine = imgArr[lp];
                        int lpos = imgArr[lp].LastIndexOf('/');
                        picfilename = imgArr[lp].Substring(lpos + 1);
                        picfilename = picfilename.Replace("\"", "");
                        break;
                    }
                }

                if (Session["organizationID"] != null)
                {
                    if (Session["organizationID"].ToString() != "")
                        Int32.TryParse(Session["organizationID"].ToString(), out orgId);
                }
                fmText = fmText.Replace(srcLine, "src=\"Org_" + orgId.ToString() + footerImageName + "\"");
                imgpath = Server.MapPath("..") + "\\Image\\Maillogo\\" + picfilename;

                footerMessage = "<FooterImage>" + imageUtilObj.ConvertImageToBase64(imgpath) + "</FooterImage>";
            }
            else
                footerMessage = "<FooterImage></FooterImage>";

            footerMessage += "<FooterMessage>" + fmText + "</FooterMessage>";

        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("FooterMessage" + ex.Message);//ZD 100263
            errLabel.Visible = true;
        }
    }
    #endregion

        #region btn1_Click
        protected void btn1_Click(object sender, EventArgs e)
        {
            btnSubmit_Click(sender, e);
        }
        #endregion

        //ZD 102102 - Start
        //#region btnChangeUIDesign_Click
        //protected void btnChangeUIDesign_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("UISettings.aspx");
        //}
        //#endregion
        //ZD 102102 - End

        #region Upload MailLogo Images
        /// <summary>
        /// UploadOtherImages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // Method modified for FB 1830
        protected void UploadMailLogoImages(Object sender, EventArgs e)
        {
            try
            {
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData = null;
                String errMsg = "";
                string fileExtn = "";
                //ZD 100263
                string filextn = "";
                List<string> strExtension = null;
                bool filecontains = false;

                if (!fleMap1.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMap1.Value);
                    myFile = fleMap1.PostedFile;
                    nFileLen = myFile.ContentLength;

                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    //ZD 100263 Starts
                    filextn = Path.GetExtension(fName);
                    strExtension = new List<string> { ".jpg", ".jpeg", ".png", ".bmp", ".gif" };
                    filecontains = strExtension.Contains(filextn);
                    if (!filecontains)
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End

                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 100000)
                    {
                        hdnUploadMap1.Text = fName;
                        Map1ImageDt.Value = "";
                        Map1ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);

                        if (Session["organizationID"] != null)
                        {
                            if (Session["organizationID"].ToString() != "")
                                Int32.TryParse(Session["organizationID"].ToString(), out orgId);
                        }
                        // FB 1758  Starts

                        testMailLogoName = "Test_" + orgId.ToString() + "mail_logo.gif";

                        if (File.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\Image\\Maillogo\\Org_" + orgId.ToString() + "mail_logo.gif"))
                            File.Delete(HttpContext.Current.Request.MapPath("..").ToString() + "\\Image\\Maillogo\\Org_" + orgId.ToString() + "mail_logo.gif");

                        WriteToFile(HttpContext.Current.Request.MapPath("..").ToString() + "\\Image\\Maillogo\\Org_" + orgId.ToString() + "mail_logo.gif", ref myData);

                        if (File.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\Image\\Maillogo\\" + testMailLogoName))
                            File.Delete(HttpContext.Current.Request.MapPath("..").ToString() + "\\Image\\Maillogo\\" + testMailLogoName);

                        WriteToFile(HttpContext.Current.Request.MapPath("..").ToString() + "\\Image\\Maillogo\\" + testMailLogoName, ref myData);

                        // FB 1758 Ends

                        MemoryStream ms1 = new MemoryStream(myData, 0, myData.Length);
                        ms1.Write(myData, 0, myData.Length);

                        if (fileExtn.ToLower() == "gif")
                            Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        else
                            Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        Map1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                        Map1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                        myData = null;
                        Map1ImageCtrl.Visible = true;
                        fleMap1.Visible = false;
                        btnUploadImages.Visible = false;
                        btnRemoveMap1.Visible = true;
                    }
                    else
                        errMsg += "Mail Logo attachment is greater than 100KB. File has not been uploaded.";
                }

                if (errLabel.Text == "")
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Display Mail Logo
        /// <summary>
        /// Display Mail Logo - Added for Image Project Edit Mode
        /// </summary>
        /// <param name="imgNode"></param>
        //Method modified for FB 1830
        private void DisplayMailLogoImages(XmlNode imgNode)
        {
            XmlNode node = null;
            string imgString = "";
            try
            {
                if (Session["organizationID"] != null)
                {
                    if (Session["organizationID"].ToString() != "")
                        Int32.TryParse(Session["organizationID"].ToString(), out orgId);
                }

                imgString = "";
                fName = "Org_" + orgId.ToString() + "mail_logo.gif";
                testMailLogoName = "Test_" + orgId.ToString() + testMailLogoName;

                node = imgNode.SelectSingleNode("//preference/MailLogoImage");
                if (node != null)
                    imgString = node.InnerText;

                hdnUploadMap1.Text = fName;
                Map1ImageDt.Value = imgString;

                imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString);
                if (imgArray == null)
                {
                    hdnUploadMap1.Text = "";
                    Map1ImageDt.Value = "";
                    Map1ImageCtrl.Visible = false;
                    fleMap1.Visible = true;
                    btnRemoveMap1.Visible = false;
                    return;
                }
                if (imgArray.Length <= 0)
                {
                    hdnUploadMap1.Text = "";
                    Map1ImageDt.Value = "";
                    Map1ImageCtrl.Visible = false;
                    fleMap1.Visible = true;
                    btnRemoveMap1.Visible = false;
                    return;
                }

                // FB 1758 Starts
                if (File.Exists(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + fName))
                    File.Delete(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + fName);

                WriteToFile(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + fName, ref imgArray);

                if (File.Exists(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + testMailLogoName))
                    File.Delete(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + testMailLogoName);

                WriteToFile(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + testMailLogoName, ref imgArray);

                // FB 1758 Ends

                MemoryStream ms1 = new MemoryStream(imgArray, 0, imgArray.Length);
                ms1.Write(imgArray, 0, imgArray.Length);
                Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                Map1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                Map1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);

                imgArray = null;

                Map1ImageCtrl.Visible = true;
                fleMap1.Visible = false;
                btnRemoveMap1.Visible = true;
                btnUploadImages.Visible = false;


            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //Method added for FB 1710
        #region Display Footer Image
    /// <summary>
    /// Display Footer Image
    /// </summary>
    /// <param name="imgNode"></param>
    // Method modified for FB 1830
    private void DisplayFooterImage(XmlNode imgNode)
    {
        try
        {
            XmlNode node = null;
            string imgString = "", footermsg = "", imgPathName = "";

            //FB 1758 - Start
            if (Session["organizationID"] != null)
            {
                if (Session["organizationID"].ToString() != "")
                    Int32.TryParse(Session["organizationID"].ToString(), out orgId);
            }
            testFootImgName = "Test_" + orgId.ToString() + testFootImgName;
            //FB 1758 - End

            node = imgNode.SelectSingleNode("//preference/FooterImage");
            if (node != null)
                imgString = node.InnerText.Trim();

            node = imgNode.SelectSingleNode("//preference/FooterImgName");
            if (node != null)
                footerImageName = node.InnerText.Trim();

            node = imgNode.SelectSingleNode("//preference/FooterMessage");
            if (node != null)
                footermsg = node.InnerXml;

            footermsg = footermsg.Replace("&lt;", "<").Replace("&gt;", ">"); //FB 2595

            if (imgString != "" && footerImageName != "")
            {
                byte[] imgArr = imageUtilObj.ConvertBase64ToByteArray(imgString);
                //imgPathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/en/") + 3) + "/Image/Maillogo/";
                imgPathName = "../Image/Maillogo/";

                if (File.Exists(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + footerImageName))
                    File.Delete(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + footerImageName);

                if (File.Exists(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + testFootImgName)) //FB 1758
                    File.Delete(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + testFootImgName);

                if (imgArr != null)
                {
                    WriteToFile(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + footerImageName, ref imgArr);
                    WriteToFile(HttpContext.Current.Request.MapPath("..") + "\\Image\\Maillogo\\" + testFootImgName, ref imgArr); //FB 1758

                    imgArray = null;
                }

                imgPathName += footerImageName;
                footermsg = footermsg.Replace(footerImageName, imgPathName);
            }

            //dxHTMLEditor.Html = utilObj.ReplaceOutXMLSpecialCharacters(footermsg, 1);//FB 2681 //FB 3011
            dxHTMLEditor.Html = System.Web.HttpUtility.HtmlDecode(footermsg); // FB 3028
            // ZD 100288 Starts
            //dxHTMLEditor.SettingsText.DesignViewTab = obj.GetTranslatedText("Design");
            //dxHTMLEditor.SettingsText.PreviewTab = obj.GetTranslatedText("Preview");
            // ZD 100288 Ends
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

    //Method Added for FB 1758
    #region TestFooterMessage
    /// <summary>
    /// FooterMessage
    /// </summary>
    //Method modified for FB 1830
    private string GetFooterMessage()
    {
        string testFooterStr = "";
        if (Session["organizationID"] != null)
        {
            if (Session["organizationID"].ToString() != "")
                Int32.TryParse(Session["organizationID"].ToString(), out orgId);
        }
        try
        {
            String fmText, imgTag, temp, imgpath = "";
            String picfilename = "";
            String srcLine = "";
            //fmText = utilObj.ReplaceInXMLSpecialCharacters(hdnFooterMsg.Value);//FB 2681 //FB 3011
            fmText = dxHTMLEditor.Html;

            if (fmText.IndexOf("<img") != -1)
            {
                imgTag = fmText.Substring(fmText.IndexOf("<img"));
                temp = imgTag.Substring(imgTag.IndexOf("/>"));

                if (temp.IndexOf("<img") > 0)
                {
                    isMoreThanOneImg = true;
                    return "";
                }

                imgTag = imgTag.Replace(temp, "/>");
                String[] imgArr = imgTag.Split(' ');

                for (int lp = 0; lp < imgArr.Length; lp++)
                {
                    if (imgArr[lp].ToLower().IndexOf("src") >= 0)
                    {
                        srcLine = imgArr[lp];
                        int lpos = imgArr[lp].LastIndexOf('/');
                        picfilename = imgArr[lp].Substring(lpos + 1);
                        picfilename = picfilename.Replace("\"", "");
                        break;
                    }
                }

                fmText = fmText.Replace(srcLine, "src=\"Test_" + orgId.ToString() + footerImageName + "\"");
                imgpath = Server.MapPath("..") + "\\Image\\Maillogo\\" + picfilename;
                String testimgpath = Server.MapPath("..") + "\\Image\\Maillogo\\Test_" + orgId.ToString() + footerImageName;

                if (File.Exists(testimgpath))
                    File.Delete(testimgpath);

                File.Copy(imgpath, testimgpath);

            }
            fmText = fmText.Replace("&nbsp;", "");
            testFooterStr += "<FooterMessage>" + fmText + "</FooterMessage>";
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
        }
        return testFooterStr;
    }
    #endregion

        #region RemoveFile
        //Method modified for FB 1830
        protected void RemoveFile(Object sender, CommandEventArgs e)
        {
            try
            {

                Map1ImageCtrl.Dispose();
                Map1ImageCtrl.Visible = false;
                fleMap1.Visible = true;
                lblUploadMap1.Visible = false;
                lblUploadMap1.Text = "";
                hdnUploadMap1.Visible = false;
                hdnUploadMap1.Text = "";
                btnRemoveMap1.Visible = false;
                Map1ImageDt.Value = "";
                btnUploadImages.Visible = true;
                // FB 1758 Starts
                if (Session["organizationID"] != null)
                {
                    if (Session["organizationID"].ToString() != "")
                        Int32.TryParse(Session["organizationID"].ToString(), out orgId);
                }
                testMailLogoName = "Test_" + orgId.ToString() + testMailLogoName;

                String maillogopath = Server.MapPath("..") + "\\Image\\Maillogo\\Org_" + orgId.ToString() + "mail_logo.gif";

                if (File.Exists(maillogopath))
                    File.Delete(maillogopath);

                String testmaillogopath = Server.MapPath("..") + "\\Image\\Maillogo\\" + testMailLogoName;

                if (File.Exists(testmaillogopath))
                    File.Delete(testmaillogopath);

                // FB 1758 Ends
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //FB 1830 - method added
        #region DefineEmailLanguage
        protected void DefineEmailLanguage(object sender, EventArgs e)
        {
            try
            {
				//FB 2283 Start
                Session.Remove("OrgBaseLang");
                Session["OrgBaseLang"] = drporglang.SelectedValue;
				//FB 2283 End
                Response.Redirect("EmailCustomization.aspx?tp=o");
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                //errLabel.Text = "Error 122: Please contact your VRM Administrator";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }

        #endregion

        //FB 1860
        #region EditBlockEmails

        protected void EditBlockEmails(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ViewBlockedMails.aspx?tp=o");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //FB 1830 - DeleteEmailLang start
        #region DeleteEmailLangugage
        /// <summary>
        /// DeleteEmailLangugage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteEmailLangugage(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                String outXML = "", emailLangID = "";

                if (Session["OrgEmailLangID"] != null)//FB 2283
                    if (Session["OrgEmailLangID"].ToString() != "")
                        emailLangID = Session["OrgEmailLangID"].ToString();

                if (emailLangID.Trim() != "")
                {
                    inXML.Append("<DeleteEmailLang>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<userid>" + Session["userID"].ToString() + "</userid>");
                    inXML.Append("<emaillangid>" + emailLangID.ToString() + "</emaillangid>");
                    inXML.Append("<langOrigin>o</langOrigin>");
                    inXML.Append("</DeleteEmailLang>");

                    outXML = obj.CallMyVRMServer("DeleteEmailLanguage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        txtEmailLang.Text = "";
                        Session["OrgEmailLangID"] = null; //FB 2283
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                        delEmailLang.Visible = false;//ZD 100580
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("OrgSettings DeleteEmailLangugage :" + ex.Message);
            }
        }
        #endregion
        //FB 1830 - DeleteEmailLang end
		
        //FB 1849 - Start //FB 2719 Starts

        //#region btnChgOrg_Click
        //protected void btnChgOrg_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (obj == null)
        //            obj = new myVRMNet.NETFunctions();

        //        if (!Int32.TryParse(DrpOrganization.SelectedValue, out orgId))
        //            throw new Exception("Invalid Organization");

        //        obj.SetOrgSession(orgId);

        //        if (Session["organizationID"] != null) //FB 1662
        //        {
        //            Int32.TryParse(Session["organizationID"].ToString(), out orgId);

        //        }
        //        DrpOrganization.ClearSelection();

        //        if (DrpOrganization.Items.FindByValue(orgId.ToString()) != null)
        //            DrpOrganization.Items.FindByValue(orgId.ToString()).Selected = true;
                
        //        Session.Remove("EndpointXML"); //FB 1552
        //        Response.Redirect("OrganisationSettings.aspx");
        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Visible = true;
        //        errLabel.Text = ex.Message;
        //        log.Trace(ex.Message);
        //    }
        //}
        //#endregion
        //FB 1849 - End //FB 2719 Ends

        //FB 2154

        //ZD 102102 - Start
        //#region EditEmaiDomain

        //protected void EditEmaiDomain(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Response.Redirect("ManageEmailDomain.aspx");
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Trace(ex.Message);
        //    }
        //}
        //#endregion

        ////FB 2045 - Start
        //#region bntEntityCode
        //protected void bntEntityCode(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Response.Redirect("ManageEntityCode.aspx");
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Trace(ex.Message);
        //    }
        //}
        //#endregion
        //FB 2045 - End
        //ZD 102102 - End

        //FB 2337
        #region CustomizeLicenseAgreement
        /// <summary>
        /// CustomizeLicenseAgreement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustomizeLicenseAgreement(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CustomizeLicenseAgreement.aspx");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //FB 2599 Start
        #region CloudImport
        /// <summary>
        /// CloudImport
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloudImport(object sender, EventArgs e)
        {
            try
            {
                String InXML = "<PollVidyo>" + obj.OrgXMLElement() + "</PollVidyo>";
                String OutXML = obj.CallCommand("PollVidyo", InXML);
                if (OutXML.IndexOf("<error>") >= 0) //FB 1599
                {
                    errLabel.Text = obj.ShowErrorMessage(OutXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    errLabel.Text = obj.ShowSuccessMessage();
                    String roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 2594
                    if (File.Exists(roomxmlPath))
                        File.Delete(roomxmlPath);
                    errLabel.Visible = true;
                }
                
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion
        //FB 2599 End

        protected string Password(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }

        // ZD 101216
        public string fnGetTranslatedText(string par)
        {
            return obj.GetTranslatedText(par);
        }

        //ZD 101443 start
        #region TestLDAPConnection
        /// <summary>
        /// TestLDAPConnection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TestLDAPConnection(object sender, EventArgs e)
        {
            try
            {
                string inXML = "";
                inXML += "<login>";
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<AccountLogin>" + txtLDAPAccountLogin.Text + "</AccountLogin>";
                //FB 3054 Starts
                if (hdnLDAPPW.Value != "" && hdnLDAPPW.Value == "true")
                    inXML += "<AccountPwd>" + Password(txtLDAPAccountPassword1.Text) + "</AccountPwd>";
                else
                    inXML += "<AccountPwd>" + Session["LDAPPW"].ToString() + "</AccountPwd>";
                //FB 3054 Ends
                inXML += "<ServerAddress>" + txtLDAPServerAddress.Text + "</ServerAddress>";
                inXML += "<Port>" + txtLDAPServerPort.Text + "</Port>";
                inXML += "<ConnectionTimeout>" + lstLDAPConnectionTimeout.SelectedValue + "</ConnectionTimeout>";
                inXML += "<LoginKey>" + txtLDAPLoginKey.Text + "</LoginKey>";
                inXML += "<SearchFilter>" + txtLDAPSearchFilter.Text.Replace("&", "&amp;").Trim() + "</SearchFilter>";
                inXML += "<LDAPPrefix>" + txtLDAPPrefix.Text + "</LDAPPrefix>";
                inXML += "<Days>";
                string strTemp = "";
                foreach (ListItem li in chkLstDays.Items)
                    if (li.Selected)
                        strTemp += li.Value + ",";
                if (strTemp.Length > 0)
                    strTemp = strTemp.Substring(0, strTemp.Length - 1);
                inXML += strTemp.Trim();
                inXML += "</Days>";
                inXML += "<SyncTime>" + DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(lstLDAPScheduleTime.Text)).ToString("hh:mm tt") + "</SyncTime>";
                inXML += "<AuthenticationType>" + drpAuthType.SelectedValue + "</AuthenticationType>";
                inXML += "</login>";

                string outXML = obj.CallCOM2("TestLDAPConnection", inXML, Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        //ZD 101443 End
        #endregion

        #region SetConfArchiveConfiguration
        /// <summary>
        /// DeleteEmailLangugage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetConfArchiveConfiguration(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                String outXML = "";


                inXML.Append("<ConfArchiveConfiguration>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<ArchiveConfOlderThan>" + lstArchiveConfOld.SelectedValue + "</ArchiveConfOlderThan>");
                inXML.Append("<ArchivePeriod>" + lstArchivePeriod.SelectedValue + "</ArchivePeriod>");
                inXML.Append("<ArchiveTime>" +  DateTime.Parse(lstConfArchiveTime.SelectedValue).ToString("hh:mm tt") + "</ArchiveTime>");
                inXML.Append("</ConfArchiveConfiguration>");

                outXML = obj.CallMyVRMServer("SetConfArchiveConfiguration", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    txtEmailLang.Text = "";
                    Session["OrgEmailLangID"] = null;
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                    delEmailLang.Visible = false;
                }

            }
            catch (Exception ex)
            {
                log.Trace("OrgSettings SetConfArchiveConfiguration :" + ex.Message);
            }
        }
        #endregion

        #region SetConfArchiveConfiguration
        /// <summary>
        /// DeleteEmailLangugage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GetConfArchiveConfiguration(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                String outXML = "";


                inXML.Append("<ConfArchiveConfiguration>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("</ConfArchiveConfiguration>");

                outXML = obj.CallMyVRMServer("GetConfArchiveConfiguration", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    lstArchiveConfOld.SelectedValue = xmldoc.SelectSingleNode("//ConfArchiveConfiguration/Settings/ArchiveConfOlderThan").InnerText;
                    lstArchivePeriod.SelectedValue = xmldoc.SelectSingleNode("//ConfArchiveConfiguration/Settings/ArchivePeriod").InnerText;
                    lstConfArchiveTime.Text = DateTime.Parse(xmldoc.SelectSingleNode("//ConfArchiveConfiguration/Settings/ArchiveTime").InnerText).ToString(tformat);
                    lstConfArchiveTime.SelectedValue = lstConfArchiveTime.Text;
                }

            }
            catch (Exception ex)
            {
                log.Trace("OrgSettings SetConfArchiveConfiguration :" + ex.Message);
            }
        }
        #endregion
    }