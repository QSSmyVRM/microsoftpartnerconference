/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace MyVRMNet
{
    public class Util
    {
        #region private members

        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        
        #endregion

        #region Constructor
        public Util()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region ReplaceInXMLSpecialCharacters
        public string ReplaceInXMLSpecialCharacters(string strXml)
        {
            try
            {
                strXml = strXml.Replace("<", ns_MyVRMNet.vrmSpecialChar.LessThan)
                               .Replace(">", ns_MyVRMNet.vrmSpecialChar.GreaterThan)
                               .Replace("&", ns_MyVRMNet.vrmSpecialChar.ampersand);

            }
            catch (Exception ex)
            {
                log.Trace("Util_InXML :" + ex.StackTrace);
            }
            return strXml;
        }
        #endregion

        #region ReplaceOutXMLSpecialCharacters
        public string ReplaceOutXMLSpecialCharacters(string strXml,int control)
        {
            try
            {
                if (control == 1) //Textbox
                    strXml = strXml.Replace(ns_MyVRMNet.vrmSpecialChar.LessThan, "<")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.GreaterThan, ">")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.ampersand, "&");
                else if (control == 2) //Label
                    strXml = strXml.Replace(ns_MyVRMNet.vrmSpecialChar.LessThan, "&lt;")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.GreaterThan, "&gt;")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.ampersand, "&amp;")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.doubleQuotes, @"\""");
                else if (control == 3) //RSS Label
                    strXml = strXml.Replace(ns_MyVRMNet.vrmSpecialChar.LessThan, "&lt; ")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.GreaterThan, "&gt;")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.ampersand, "&amp;");
            }
            catch (Exception ex)
            {
                log.Trace("Util_OutXML :" + ex.StackTrace);
            }
            return strXml;
        }
        #endregion


        //FB 2343 Start

        #region GetStartAndEndOfWeek
        public  void GetStartAndEndOfWeek(int year, int week, out DateTime startOfWeek, out DateTime endOfWeek)
        {
            // find the first week.
            CalendarWeekRule cwr = CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule;
            DayOfWeek firstDayOfWeek = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;

            DateTime firstdayofyear = new DateTime(year, 1, 1);
            int offset = 0;

            if (firstdayofyear.DayOfWeek != firstDayOfWeek)
            {
                // find first first day.
                if (cwr == CalendarWeekRule.FirstFourDayWeek)
                {
                    DateTime firstFullWeekStart = firstdayofyear;
                    while (firstFullWeekStart.DayOfWeek != firstDayOfWeek)
                        firstFullWeekStart = firstFullWeekStart.AddDays(1);
                    if (firstFullWeekStart.Subtract(firstdayofyear).Days >= 4)
                        offset = -1;
                }

                if (cwr == CalendarWeekRule.FirstDay)
                    offset = -1;
            }

            startOfWeek = firstdayofyear.AddDays(7 * (week + offset));

            while (startOfWeek != firstdayofyear && startOfWeek.DayOfWeek != firstDayOfWeek)
                startOfWeek = startOfWeek.AddDays(-1);

            endOfWeek = startOfWeek;
            do
            {
                endOfWeek = endOfWeek.AddDays(1);
            } while (endOfWeek < new DateTime(year + 1, 1, 1).AddDays(-1) && endOfWeek.AddDays(1).DayOfWeek != firstDayOfWeek);
        }
        # endregion

        #region GetStartAndEndOfMonth
        public void GetStartAndEndOfMonth(int year, int month, out DateTime startOfmonth, out DateTime endOfmonth)
        {
            startOfmonth = new DateTime(year, month, 1, 0, 0, 0);
            endOfmonth = new DateTime(year, month, startOfmonth.AddMonths(1).AddDays(-startOfmonth.Day).Day, 23, 59, 59);
        }
        # endregion
        //FB 2343 Start
    }

    class MyComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            int xVal, yVal;
            var xIsVal = int.TryParse(x, out xVal);
            var yIsVal = int.TryParse(y, out yVal);

            if (xIsVal && yIsVal)   // both are numbers...
                return xVal.CompareTo(yVal);
            if (!xIsVal && !yIsVal) // both are strings...
                return x.CompareTo(y);
            if (xIsVal)             // x is a number, sort first
                return -1;
            return 1;               // x is a string, sort last
        }
    }

    //string[] input = new[] {"a", "1", "10", "b", "2", "c"};
    //string[] e = input.OrderBy( s => s, new MyComparer() );
}
