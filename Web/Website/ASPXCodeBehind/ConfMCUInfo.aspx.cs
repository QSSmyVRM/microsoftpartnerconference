﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;

namespace ns_ConfMCUInfo
{
    public partial class ConfMCUInfo : System.Web.UI.Page
    {
        #region Protected Data Memebers
        protected System.Web.UI.HtmlControls.HtmlTable tblConfMCUinfo;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDatabaseServerTime;
        protected System.Web.UI.WebControls.Label lblConfName;
        protected System.Web.UI.WebControls.Label lblStartTime;
        protected System.Web.UI.WebControls.Label lblWebServerTime;
        protected System.Web.UI.WebControls.Label lblDatabaseServerTime;
        protected System.Web.UI.WebControls.Label lblError;
        protected System.Web.UI.WebControls.Label tdNoMCU;
        //ZD 100995 Starts
        protected string format = "MM/dd/yyyy", dtformat = "", tformat = "hh:mm tt";
        DateTime ConfStart = new DateTime();
        DateTime WebDateTime = new DateTime();
        DateTime ServerDateTime = new DateTime();
        DateTime BrigeDate = new DateTime();
        //DateTime ConfStart = new DateTime();
        //ZD 100995 Ends
        #endregion

        #region Variable Declaration

        private myVRMNet.NETFunctions obj = null;
        private ns_Logger.Logger log = null;

        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page_Load

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("confmcuinfo.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                obj = new myVRMNet.NETFunctions();
                log = new ns_Logger.Logger();

                BindData();
            }
            catch (Exception ew)
            {
                log.Trace("Page_Load" + ew.Message);
            }
        }

        #endregion

        #region BindData

        private void BindData()
        {
            try
            {
                String inXML = "", outXML = "";

                //ZD 100995 Starts
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                dtformat = format; 
                if (Session["EmailDateFormat"] != null && Session["EmailDateFormat"].ToString() == "1")
                    dtformat = "dd MMM yyyy";
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";

                //ZD 100995 Ends

                inXML = BuildInXML();

                outXML = obj.CallCommand("FetchConfMCUDetails", inXML.ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    lblError.Text = obj.ShowErrorMessage(outXML);
                    lblError.Visible = true;
                    return;
                }

                outXML = obj.CallMyVRMServer("GetConfMCUDetails", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xd = new XmlDocument();
                    XmlNodeList nodes = null;
                    XmlNode node = null;
                    xd.LoadXml(outXML);

                    node = xd.SelectSingleNode("//GetConfMCUDetails/confinfo/confName");
                    if (node != null)
                        lblConfName.Text = node.InnerText.ToString();

                    //ZD 100995 Starts
                    node = xd.SelectSingleNode("//GetConfMCUDetails/confinfo/ConfStartDatetime");
                    if (node != null)
                        DateTime.TryParse(node.InnerText.ToString(), out ConfStart);
                    lblStartTime.Text = ConfStart.ToString(dtformat); //ZD 
                        //lblStartTime.Text = node.InnerText.ToString();

                    node = xd.SelectSingleNode("//GetConfMCUDetails/WebserverDateTime");
                    if (node != null)
                        DateTime.TryParse(node.InnerText.ToString(), out WebDateTime);
                    lblWebServerTime.Text = WebDateTime.ToString(dtformat) + " " + WebDateTime.ToLongTimeString();
                        //lblWebServerTime.Text = node.InnerText.ToString();

                    node = xd.SelectSingleNode("//GetConfMCUDetails/DatabaseServerDateTime");
                    if (node != null)
                        DateTime.TryParse(node.InnerText.ToString(), out ServerDateTime);
                        //lblDatabaseServerTime.Text = node.InnerText.ToString();
                    lblDatabaseServerTime.Text = ServerDateTime.ToString(dtformat) + " " + ServerDateTime.ToLongTimeString();
                    //ZD 100995 Ends
                    String BridgeName = "", BrigeDateTime = "", BrigeTimezone = "";
                    int j = -1;
                    nodes = xd.SelectNodes("//GetConfMCUDetails/Bridgelist/Bridge");
                    if (nodes != null)
                    {
                        if (nodes.Count > 0)
                        {
                            foreach (XmlNode Node in nodes)
                            {
                                BridgeName = ""; BrigeDateTime = ""; BrigeTimezone = "";
                                DateTime.TryParse(Node.SelectSingleNode("BrigeDateTime").InnerText.ToString(), out BrigeDate);
                                BridgeName = Node.SelectSingleNode("BridgeName").InnerText.ToString();
                                BrigeDateTime = BrigeDate.ToString(dtformat) + " " + BrigeDate.ToLongTimeString(); //ZD 100995
                                BrigeTimezone = Node.SelectSingleNode("BrigeTimezone").InnerText.ToString();

                                j++;
                                HtmlTableRow tr = new HtmlTableRow();
                                HtmlTableCell tCell = new HtmlTableCell();


                                tCell = new HtmlTableCell();
                                tCell.InnerText = BridgeName;
                                tCell.Style.Add(HtmlTextWriterStyle.TextAlign, "Right");
                                tCell.NoWrap = true;
                                tCell.Width = "25%";
                                tr.Controls.Add(tCell);


                                tCell = new HtmlTableCell();
                                tCell.InnerText = BrigeDateTime + "(" + BrigeTimezone + ")";
                                tCell.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                                tCell.NoWrap = true;
                                tr.Controls.Add(tCell);

                                tblConfMCUinfo.Rows.Insert(j, tr);
                            }
                        }
                        else
                            tdNoMCU.Visible = true;
                    }
                    else
                        tdNoMCU.Visible = true;
                }
                else
                {
                    lblError.Text = obj.ShowErrorMessage(outXML);
                    lblError.Visible = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindData" + ex.Message);
            }
        }

        #endregion

        #region BuildInXML

        private String BuildInXML()
        {
            StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<GetConfMCUDetails>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<ConfID>" + Request.QueryString["confid"].ToString() + "</ConfID>");
                inXML.Append("</GetConfMCUDetails>");
                log.Trace(inXML.ToString());
            }
            catch (Exception e)
            {
                log.Trace("BuildInXML" + e.Message);
            }
            return inXML.ToString();
        }

        #endregion
    }

}