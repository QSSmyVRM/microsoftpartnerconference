<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Debug="true" Inherits="ns_MYVRM.MainAdministrator" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
    
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<script type="text/javascript" src="script/myprompt.js"></script>

<!-- JavaScript begin -->

<script language="JavaScript1.2" src="inc/functions.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script><%--FB 2659--%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>

<script language="JavaScript">
<!--
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //<%--FB 1490 Start--%>
    function fncheckTime() {
        var stdate = '';
        if (document.getElementById("systemEndTime_Text") && document.getElementById("systemStartTime_Text")) {
            stdate = GetDefaultDate('01/01/1901', '<%=((Session["timeFormat"] == null) ? "1" : Session["timeFormat"])%>');
            if (Date.parse(stdate + " " + document.getElementById("systemEndTime_Text").value) < Date.parse(stdate + " " + document.getElementById("systemStartTime_Text").value)) {
                alert(MainEndTime); //FB 2148
                document.getElementById("systemStartTime_Text").focus();
                return false;
            }
            else if (Date.parse(stdate + " " + document.getElementById("systemEndTime_Text").value) == Date.parse(stdate + " " + document.getElementById("systemStartTime_Text").value)) {
                alert(MainEndTime);
                document.getElementById("systemEndTime_Text").focus();
                return false;
            }
        }
        return true;
    }
    //<%--FB 1490 End--%>
    //FB 2486
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block" || ele.style.display == "") {
            ele.style.width = "100%";
            ele.style.display = "none";
            text.innerHTML = "More";
        }
        else {
            ele.style.display = "";
            ele.style.width = "100%";
            text.innerHTML = "Less";
        }
    }
    function open24() {
        t = (document.frmMainadminiatrator.Open24.checked) ? "none" : ''; //Edited For FF...
        for (var i = 1; i < 5; i++) {
            document.getElementById("Open24DIV" + i).style.display = t;
        }
        document.getElementById("systemStartTime_Text").style.width = "100px";
        document.getElementById("systemEndTime_Text").style.width = "100px";
    }


    function ValidateInput() {
        if ((document.getElementById("lstDefaultConferenceType").value == "2") && (document.getElementById("lstEnableAudioVideoConference").value == "0")) {
            alert(MainAudVide);
            DataLoading(0); //ZD 102691  
            document.getElementById("lstEnableAudioVideoConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "7") && (document.getElementById("lstEnableRoomConference").value == "0")) {
            alert(MainRoomconf);
            DataLoading(0); //ZD 102691  
            document.getElementById("lstEnableRoomConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "6") && (document.getElementById("lstEnableAudioOnlyConference").value == "0")) {
            alert(MainAudonly);
            DataLoading(0); //ZD 102691  
            document.getElementById("lstEnableAudioOnlyConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "4") && (document.getElementById("p2pConfEnabled").value == "0")) {
            alert(MainP2PConf);
            DataLoading(0); //ZD 102691  
            document.getElementById("p2pConfEnabled").focus();
            return false;
        }
        //ZD 100719 - Start
        if ((document.getElementById("lstDefaultConferenceType").value == "8") && (document.getElementById("lstEnableHotdeskingConference").value == "0")) {
            alert(MainHotDesk);
            DataLoading(0); //ZD 102691  
            document.getElementById("lstEnableHotdeskingConference").focus();
            return false;
        }
        //ZD 100719 - End
        //ZD 100513 Starts
        if ((document.getElementById("lstDefaultConferenceType").value == "9") && (document.getElementById("drpOBTPConf").value == "0")) {
            alert(MainOBTP);
            DataLoading(0); //ZD 102691  
            document.getElementById("drpOBTPConf").focus();
            return false;
        }
        //ZD 100513 Ends
        //<%--FB 1490 Start--%>
        if (!fncheckTime())
            return false;
        //<%--FB 1490 End--%>

        return true;
    }

    //FB 2136
   /* function modedisplay() {
        var mode = document.getElementById("drpenablesecuritybadge");
        var type = document.getElementById("drpsecuritybadgetype");

        if (document.getElementById("drpenablesecuritybadge").value == "1") {
            document.getElementById("tdSecurityType").style.display = "block";
            type.style.display = "block";
            emaildisplay();
        }
        else {
            document.getElementById("tdSecurityType").style.display = "none";
            type.style.display = "none";
            document.getElementById("tdsecdeskemailid").style.visibility = "hidden";
        }
    }

    function emaildisplay() {
        var type = document.getElementById("drpsecuritybadgetype");

        if (document.getElementById("drpsecuritybadgetype").value == "2" || document.getElementById("drpsecuritybadgetype").value == "3") {
            document.getElementById("tdsecdeskemailid").style.visibility = "visible";
        }
        else {
            document.getElementById("tdsecdeskemailid").style.visibility = "hidden";
        }
    }*/
    //FB 2348 Start
    // ZD 103416 Start
    function modedisplay1() {
        if (document.getElementById("drpenablesurvey").value == "1") {
            document.getElementById("divSurveytimedur").style.display = "block";
            document.getElementById("divSurveyURL").style.display = "block";
           // document.getElementById("tdSurveyengine").style.visibility = "visible";
            //document.getElementById("tdsurveyoption").style.visibility = "visible";
          //  modesurvey()

        }
        else {
            document.getElementById("divSurveytimedur").style.display = "none";
            document.getElementById("divSurveyURL").style.display = "none";
           // document.getElementById("tdSurveyengine").style.visibility = "hidden";
            //document.getElementById("tdsurveyoption").style.visibility = "hidden";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);

        }
    }
    /*function modesurvey() {
        if (document.getElementById("drpsurveyoption").value == "2") {
            document.getElementById("divSurveyURL").style.display = "block";
            document.getElementById("divSurveytimedur").style.display = "block";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), true);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), true);
            ValidatorEnable(document.getElementById("RegTimeDur"), true);
        }
        else {
            document.getElementById("divSurveyURL").style.display = "none";
            document.getElementById("divSurveytimedur").style.display = "none";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);

        }
    }*/
    // ZD 103416 End
    //FB 2347T 
    // ZD 103416 Start
    function ChangeValidator() {
        // if (document.getElementById("drpenablesurvey").value == "2" || document.getElementById("drpsurveyoption").value == "1")
       if (document.getElementById("drpenablesurvey").value == "0")
        {
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);
        }
    }
    // ZD 103416 End
    // FB 2841 Start
    function ExpandAll() {
        var bool = "";
        if (document.getElementById("chkExpandCollapse").checked == true)
            bool = "0";
        else
            bool = "1"
        ExpandCollapse(document.getElementById("img_USROPT"), "trUSROPT", bool);
        ExpandCollapse(document.getElementById("img_CONFOPT"), "trCONFOPT", bool);
        ExpandCollapse(document.getElementById("img_CONFDEF"), "trCONFDEF", bool);
        ExpandCollapse(document.getElementById("img_CONFTYPE"), "trCONFTYPE", bool);
        ExpandCollapse(document.getElementById("img_FEAT"), "trFEAT", bool);
        ExpandCollapse(document.getElementById("img_AUD"), "trAUD", bool);
        ExpandCollapse(document.getElementById("img_CONFMAIL"), "trCONFMAIL", bool);
        if ('<%=Session["EnableNetworkFeatures"]%>' == "1") //FB 2993
            ExpandCollapse(document.getElementById("img_NETSWT"), "trNETSWT", bool);
        ExpandCollapse(document.getElementById("img_EPT"), "trEPT", bool);
        ExpandCollapse(document.getElementById("img_RoomSync"), "trRoomSync", bool);//ZD 101527

        //if ('<%=Session["GuestRooms"]%>' != "0")//ZD 101296 //ZD 103522
        ExpandCollapse(document.getElementById("img_FLY"), "trFLY", bool);

        ExpandCollapse(document.getElementById("img_AUTO"), "trAUTO", bool);
        ExpandCollapse(document.getElementById("img_SYS"), "trSYS", bool);
        //ExpandCollapse(document.getElementById("img_CONFSECDESK"), "trCONFSECDESK", bool);
        ExpandCollapse(document.getElementById("img_PIM"), "trPIM", bool);
        ExpandCollapse(document.getElementById("img_SUR"), "trSUR", bool);
        ExpandCollapse(document.getElementById("img_ADMOPT"), "trADMOPT", bool);
        ExpandCollapse(document.getElementById("img_ICP"), "trICP", bool); //FB 2724
        if ('<%=Session["WebexUserLimit"]%>' != "0") //ZD 100935
            ExpandCollapse(document.getElementById("img_WEBCONF"), "trWebCre", bool); //ZD 100221
        if ('<%=Session["foodModule"]%>' != "0" || '<%=Session["hkModule"]%>' != "0" || '<%=Session["roomModule"]%>' != "0") //ZD 101228 
            ExpandCollapse(document.getElementById("img_WO"), "trWO", bool);
        
    }
    // FB 2841 End
    function ExpandCollapse(img, str, frmCheck) {
        obj = document.getElementById(str);

        if (str == "trFLY" && frmCheck == true) // FB 2426
        {
            var drptopObj = document.getElementById("lstTopTier");
            var drpmiddleObj = document.getElementById("lstMiddleTier");
            if(drptopObj.options[drptopObj.selectedIndex] != "undefined" && drptopObj.options[drptopObj.selectedIndex] != null)//ZD 100719
                var selectop = drptopObj.options[drptopObj.selectedIndex].text;
            if(drpmiddleObj.options[drpmiddleObj.selectedIndex] != "undefined" && drpmiddleObj.options[drpmiddleObj.selectedIndex] != null)
                var selecmiddle = drpmiddleObj.options[drpmiddleObj.selectedIndex].text;
            if (selectop == "Please select..." && obj.style.display == "")
                return false;
            if (selecmiddle == "Please select..." && obj.style.display == "")
                return false;
        }
       //ZD 100068 start
        if (str == "trPIM" && frmCheck == true) 
        {
            var drptopObj1 = document.getElementById("lstVMRTopTier");
            var drpmiddleObj1 = document.getElementById("lstVMRMiddleTier");
            if (drptopObj1.options[drptopObj1.selectedIndex] != "undefined" && drptopObj1.options[drptopObj1.selectedIndex] != null)//ZD 100719
                var selectop1 = drptopObj1.options[drptopObj1.selectedIndex].text;
            if (drpmiddleObj1.options[drpmiddleObj1.selectedIndex] != "undefined" && drpmiddleObj1.options[drpmiddleObj1.selectedIndex] != null)
                var selecmiddle1 = drpmiddleObj1.options[drpmiddleObj1.selectedIndex].text;
            if (selectop1 == "Please select..." && obj.style.display == "")
                return false;
            if (selecmiddle1 == "Please select..." && obj.style.display == "")
                return false;
                
        }
       //ZD 100068 End
        if (obj != null)
        {
                if (frmCheck == "1")
                {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                    img.className = "0";// ZD 100419
                }
                else
                {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                    img.className = "1"; // ZD 100419
                }
          }
        document.getElementById("systemStartTime_Text").style.width = "100px";
        document.getElementById("systemEndTime_Text").style.width = "100px";
    }
    //FB 2348 End
    //ZD 101757
    
    function fnActiveMsgDelivery()
    {
        if (document.getElementById("drpActiveMsgDel").value == "1") 
        {
            document.getElementById("tdTextmsg").style.display = "";
            document.getElementById("tdMsgPopup").style.display = "";
        }
        else
        {
            document.getElementById("tdTextmsg").style.display = "None";
            document.getElementById("tdMsgPopup").style.display = "None";
        }        
    }

    function fnBufferOptions()//FB 2398
    {
        if (document.getElementById("EnableBufferZone").value == "1") {
            document.getElementById("trBufferOptions").style.display = "";//TCK  #100154
            //document.getElementById("trMCUBufferOptions").style.display = ""; //FB 2440  //TCK  #100154 //ZD 100085
            //document.getElementById("trForceMCUBuffer").style.display = ""; //FB 2440 //TCK  #100154
        }
        else {
            document.getElementById("trBufferOptions").style.display = "None"; //TCK  #100154
            //document.getElementById("trMCUBufferOptions").style.display = "None"; //FB 2440 //TCK  #100154 //ZD 100085
            //document.getElementById("trForceMCUBuffer").style.display = "None"; //FB 2440 //TCK  #100154
        }
    }

    //FB 2426 Start
    // ZD 103416 Start
    function Submit() {
       // if (document.getElementById('drpenablesurvey').value == "0" || document.getElementById("drpsurveyoption").value == "1")
        if (document.getElementById('drpenablesurvey').value == "0")
         {
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);
        }
        // ZD 103416 End
        //ZD 103857
        if (!fnDaysValiation())
            return false;

        fnUpdatePosVertStatus(); // ZD 101019
        if (!Page_ClientValidate())//ZD 100381
            return Page_IsValid;
    	//ZD 100068 starts
        var topVal = document.getElementById("lstVMRTopTier").value;
        if (topVal == '-1') {            
            document.getElementById("reqTopTier1").style.display = 'block';            
            return false;
        }

        var MidVal = document.getElementById("lstVMRMiddleTier").value;        
        if (MidVal == '-1') {
            document.getElementById("reqMiddleTier1").style.display = 'block';
            return false;
        }
   		//ZD 100068 ends //ZD 100381 - Start
        var lsttoptier;
        var lstmiddletier;
        if(document.getElementById('lstTopTier')!= null)
             lsttoptier = document.getElementById('lstTopTier').value;
        if (document.getElementById('lstMiddleTier') != null)
            lstmiddletier = document.getElementById('lstMiddleTier').value;
        //FB 2501
        //ZD 100420

        if (lsttoptier == "-1") {
            document.getElementById('reqTopTier').style.display = 'block';
            document.getElementById('lstTopTier').focus();                      
            return false;
        }
        if (lstmiddletier == "-1") {
            document.getElementById('reqMiddleTier').style.display = 'block';
            document.getElementById('lstMiddleTier').focus();
            return false;
        }
        //ZD 100381 - End
        var SercureLaunch = document.getElementById('txtSecureLaunch').value;
        if (SercureLaunch < 0)
            return false;
        DataLoading(1); //ZD 100176
        
        //ZD 100433 Validation Part Starts
        var setupDur = 0; var mcupreStart = 0;
        if (document.getElementById("txtSetupTime") != null)
            setupDur = parseInt(document.getElementById("txtSetupTime").value, 10);
        if (document.getElementById("txtMCUSetupTime") != null)
            mcupreStart = parseInt(document.getElementById("txtMCUSetupTime").value, 10);

        if (setupDur != 0) {
            if (setupDur < mcupreStart && setupDur != mcupreStart) {
                document.getElementById("customMCUPreStart").style.display = 'block';
            }
            else {
                document.getElementById("customMCUPreStart").style.display = 'none';
            }
        }
        else {
            document.getElementById("customMCUPreStart").style.display = 'none';
        }

        var TearDown = 0; var mcupreEnd = 0;
        if (document.getElementById("txtTearDownTime") != null)
            TearDown = parseInt(document.getElementById("txtTearDownTime").value, 10);
        if (document.getElementById("txtMCUTearDownTime") != null)
            mcupreEnd = parseInt(document.getElementById("txtMCUTearDownTime").value, 10);
        if (TearDown != 0) {
            if (TearDown < mcupreEnd && TearDown != mcupreEnd) {
                document.getElementById("customMCUPreEnd").style.display = 'block';
            }
            else {
                document.getElementById("customMCUPreEnd").style.display = 'none';
            }
        }
        else {
            document.getElementById("customMCUPreEnd").style.display = 'none';
        }
        //ZD 100433 Validation Part End        
        //FB 3020
        if (document.getElementById("RangeDefaultConfDuration").style.display == 'inline' || document.getElementById("RegDefaultConfDuration").style.display == 'inline' || document.getElementById("RegScheduleLimit").style.display == 'inline') { //ZD 100899
            window.location.hash = "#";
            window.location.hash = "#topSpace";
            return false;
        }
        //ZD 100433 Validation Part Starts
        if (document.getElementById("customMCUPreStart").style.display == 'block' || document.getElementById("RegMCUSetupTime").style.display == 'inline' || document.getElementById("rangesetuptime").style.display == 'inline' || document.getElementById("RegtxtSetupTime").style.display == 'inline')//ZD 100433 
        {
            window.location.hash = "#";
            window.location.hash = "#DefaultPublic"; //Used to focus the cursor on error after submit the page.
            return false;
        }
        if (document.getElementById("customMCUPreEnd").style.display == 'block' || document.getElementById("RegMCUTearDownTime").style.display == 'inline' || document.getElementById("Rangeteardowntime").style.display == 'inline' || document.getElementById("RegtxtTearDownTime").style.display == 'inline')//ZD 100433
        {
            window.location.hash = "#";
            window.location.hash = "#DefaultPublic";
            return false;
        }
        //ZD 100433 Validation Part End
        //ZD 100781 Starts
        if (document.getElementById("RangePwdExpDays").style.display == 'inline' || document.getElementById("RegPwdExpDays").style.display == 'inline' || document.getElementById("reqExpDays").style.display == 'inline') {
            window.location.hash = "#";
            window.location.hash = "#topSpace";
            return false;
        }
        //ZD 100781 Ends
    }
    //FB 2426 End
    //FB 2595 Start
    function Securedisplay() {
        if (document.getElementById("drpSecureSwitch").value == "1") {
            document.getElementById("tdsecureadminaddress").style.visibility = "visible";
            document.getElementById("txtHardwareAdminEmail").style.visibility = "visible";
            document.getElementById("trNwtSwtiching").style.visibility = "visible";
            document.getElementById("trNwtCallBuffer").style.visibility = "visible";
        }
        else {
            document.getElementById("tdsecureadminaddress").style.visibility = "hidden";
            document.getElementById("txtHardwareAdminEmail").style.visibility = "hidden";
            document.getElementById("trNwtSwtiching").style.visibility = "hidden";
            document.getElementById("trNwtCallBuffer").style.visibility = "hidden";
        }
    }
    //FB 2595 End
//-->
    //ZD 102085 Starts
    function getYourOwnAddressList() {

        var url = "../en/emaillist2main.aspx?t=e&frm=party2NET&chk=frmbridge&fn=Setup&n=";

        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470px,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else if (!winrtc.closed) {
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470px ,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=920ss,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }
    
    function ClearAllSelection() {
        document.getElementById("txtMultipleAssistant").value = "";
    }
    //ZD 102085 Ends
</script>

<%--FB 2486--%>

<script type="text/javascript">

    function fnCheck(arg) {
        var srcID = document.getElementById(arg);
        var ckboxName = "chkmsg";
        var ctrlIDNo = arg.substring(arg.length, arg.length - 1)
        var drpName = srcID.id.replace(ctrlIDNo, "");
        var pVal = srcID.getAttribute("PreValue");
        var ckboxSel = document.getElementById(ckboxName + "" + ctrlIDNo);

        if (ckboxSel && ckboxSel.checked == false)
            return true;

        if (pVal == null)
            pVal = srcID.options[0].text;

        for (var i = 1; i <= 9; i++) {
            var ckbox = document.getElementById(ckboxName + "" + i);
            if (ckbox) {
                if (i == ctrlIDNo)
                    continue;

                if (ckbox.checked) {
                    var destDrpName = document.getElementById(drpName + i);
                    if (destDrpName) {
                        if (destDrpName.value == srcID.value) {
                            srcID.value = pVal;
                            alert(ConfSelectTimemsg);
                            if (ckboxSel)
                                ckboxSel.checked = false;
                            return false;
                        }
                    }
                }
            }
        }

        srcID.setAttribute("PreValue", srcID.value);
        return true;
    }

    //FB 2632
    function fnUpdateCngSupport() {
        var e = document.getElementById("drpCngSupport");
        var drpVal = e.options[e.selectedIndex].value;
        if (drpVal == 0) {
            document.getElementById("chkMeetandGreet").disabled = true;
            document.getElementById("chkOnSiteAVSupport").disabled = true;
            document.getElementById("chkConciergeMonitoring").disabled = true;
            document.getElementById("chkDedicatedVNOCOperator").disabled = true;
        }
        else {
            document.getElementById("chkMeetandGreet").disabled = false;
            document.getElementById("chkOnSiteAVSupport").disabled = false;
            document.getElementById("chkConciergeMonitoring").disabled = false;
            document.getElementById("chkDedicatedVNOCOperator").disabled = false;

        }
    }
    //FB 2588
    function fnChangeZulu() {
        document.getElementById("hdnZuluChange").value = "1";
    }

    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif' alt='Loading..'>";//ZD 100419
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End


    //ZD 100263 Starts
    function AddRemoveWhiteList(opr) {
        var lstFileList = document.getElementById("lstWhiteList");
        var txtFileList = document.getElementById("txtWhiteList");
        var hdnFileList = document.getElementById("hdnFileWhiteList");

        if (opr == "Rem") {
            var i;
            for (i = lstFileList.options.length - 1; i >= 0; i--) {
                if (lstFileList.options[i].selected) {
                    hdnFileList.value = hdnFileList.value.replace(lstFileList.options[i].text, "").replace(/;/i, "");
                    lstFileList.remove(i);
                }
            }
        }
        else if (opr == "add") {
            if (txtFileList.value.replace(/\s/g, "") == "") //trim the textbox
            {
                txtFileList.value = "";
                return false;
            }

            if (!txtFileList.value.match(/^[a-zA-Z]+$/)) {
                alert(MainAlpha);
                return false;
            }

            var fileList = hdnFileList.value.split(';');

            for (i = 0; i < fileList.length; i++) {
                if (fileList[i].toLowerCase() == txtFileList.value.toLowerCase()) {
                    alert(MainList);
                    return false;
                }
            }

            if (lstFileList.options.length > 0)
                hdnFileList.value = hdnFileList.value + ";";

            var option = document.createElement("Option");
            option.text = txtFileList.value;
            option.title = txtFileList.value;
            lstFileList.add(option);
            hdnFileList.value = hdnFileList.value + txtFileList.value;

            txtFileList.value = "";
            txtFileList.focus();
        }

        return false;
    }

    //ZD 103046 - Start
    function AddRemovePersonnelAlert(arg) {
        var lstPersonnelList = document.getElementById("lstPersonnelAlert");
        var txtPersonnelList = document.getElementById("txtPersonnelAlert");
        var hdnPersonnelList = document.getElementById("hdnPersonnelAlert");

        if (arg == "add") 
        {
            if (txtPersonnelList.value.replace(/\s/g, "") == "") //trim the textbox
            {
                txtPersonnelList.value = "";
                return false;
            }
            
            var Spilitemail = txtPersonnelList.value.split(';');
            var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            for (var i = 0; i < Spilitemail.length; i++) {
                if (Spilitemail[i] = "" || !regex.test(Spilitemail[i])) {
                    document.getElementById('CustomPersonnalAlert').style.display = 'block';
                    return false;
                }
                else {
                    document.getElementById('CustomPersonnalAlert').style.display = 'none';
                }
            }

            var fileList1 = hdnPersonnelList.value.split(';');

            for (i = 0; i < fileList1.length; i++) {
                if (fileList1[i].toLowerCase() == txtPersonnelList.value.toLowerCase()) {
                    alert(MainList);
                    return false;
                }
            }

            if (lstPersonnelList.options.length > 0)
                hdnPersonnelList.value = hdnPersonnelList.value + ";";

            var option = document.createElement("Option");
            option.text = txtPersonnelList.value;
            option.title = txtPersonnelList.value;
            lstPersonnelList.add(option);
            hdnPersonnelList.value = hdnPersonnelList.value + txtPersonnelList.value;
        }
        else if (arg == "Rem") {
            var i;
            for (i = lstPersonnelList.options.length - 1; i >= 0; i--) {
                if (lstPersonnelList.options[i].selected) {
                    hdnPersonnelList.value = hdnPersonnelList.value.replace(lstPersonnelList.options[i].text, "").replace(";;", ";");                     
                    if (hdnPersonnelList.value == ';')
                        hdnPersonnelList.value = "";
                    lstPersonnelList.remove(i);
                }
            }
        }

        txtPersonnelList.value = "";
        txtPersonnelList.focus();

        return false;
    }

    function ClearAllPersonnelAlert() {
        if(document.getElementById("lstPersonnelAlert") != null)
            document.getElementById("lstPersonnelAlert").options.length = 0;

        if (document.getElementById("hdnPersonnelAlert") != null)
            document.getElementById("hdnPersonnelAlert").value = "";
        return false;
    }
    //ZD 103046 - End

    //ZD 100263 Start
    function Filewhitelistshowhide() {

        var enableWhiteList = document.getElementById("chkWhiteList");

        if ((enableWhiteList != null) && enableWhiteList.checked) {
            document.getElementById("txtWhiteList").style.display = "";
            document.getElementById("btnWhiteList").style.display = "";
            document.getElementById("trWhiteList").style.display = "";
        }
        else {
            document.getElementById("txtWhiteList").style.display = "None";
            document.getElementById("btnWhiteList").style.display = "None";
            document.getElementById("trWhiteList").style.display = "None";
        }
    }
    //ZD 100263 End

    //ZD 100433 start
    function fnCompareSetup(sender, args) {
        var setupDur = 0; var mcupreStart = 0;
        if(document.getElementById("txtSetupTime") != null)
            setupDur = parseInt(document.getElementById("txtSetupTime").value, 10);
        if(document.getElementById("txtMCUSetupTime") != null)    
            mcupreStart = parseInt(document.getElementById("txtMCUSetupTime").value, 10);

        if (setupDur != 0) {
            if (setupDur < mcupreStart && setupDur != mcupreStart) {
                document.getElementById("customMCUPreStart").style.display = 'block';
                return args.IsValid = false;
            }
            else {
                document.getElementById("customMCUPreStart").style.display = 'none';
                return args.IsValid = true;
            }
        }
        else {
            document.getElementById("customMCUPreStart").style.display = 'none';
            return args.IsValid = true;
        }
    }
    function fnCompareTear(sender, args) {
        var TearDown = 0; var mcupreEnd = 0;
        if(document.getElementById("txtTearDownTime") != null)
            TearDown = parseInt(document.getElementById("txtTearDownTime").value, 10);
        if(document.getElementById("txtMCUTearDownTime") != null)    
            mcupreEnd = parseInt(document.getElementById("txtMCUTearDownTime").value, 10);
        if (TearDown != 0) {
            if (TearDown < mcupreEnd && TearDown != mcupreEnd) {
                document.getElementById("customMCUPreEnd").style.display = 'block';
                return args.IsValid = false;
            }
            else {
                document.getElementById("customMCUPreEnd").style.display = 'none';
                return args.IsValid = true;
            }
        }
        else {
            document.getElementById("customMCUPreEnd").style.display = 'none';
            return args.IsValid = true;
        }

    }
    //ZD 100433 End
    //ZD 100707 Start
    function ShowHideVMROptions() {

        var EnableVMR = document.getElementById("drpEnableVMR");

        if ((EnableVMR != null) && EnableVMR.value == "1") {
            document.getElementById("tdPersonalVMR").style.display = "";
            document.getElementById("tddrpEnablePersonaVMR").style.display = "";
            document.getElementById("trEnableRoomExternalVRM").style.display = "";
        }
        else {
            document.getElementById("tdPersonalVMR").style.display = "None";
            document.getElementById("tddrpEnablePersonaVMR").style.display = "None";
            document.getElementById("trEnableRoomExternalVRM").style.display = "None";
        }
    }
    //ZD 100707 End
    //ZD 100781 Starts
    function fnUpdatePasswordExpEnable() {
        var e = document.getElementById("DrpEnablePwdExp");
        var drpVal = 0;
        if( e != null)
        drpVal =  e.options[e.selectedIndex].value;
        if (drpVal == 1) {
            document.getElementById("lblPwdExp").style.display = "";
            document.getElementById("tdDrpPwdExp").style.display = "";
        }
        else {
            document.getElementById("lblPwdExp").style.display = "None";
            document.getElementById("tdDrpPwdExp").style.display = "None";
        }
    }

    //ZD 100781 Ends
    //ZD 100815 Starts
    function fnUpdateSmartNotify() {
        var e = document.getElementById("lstEnableSmartP2P");
        var drpVal = e.options[e.selectedIndex].value;
        if (drpVal == 0) {
            document.getElementById("drpSmartP2PNotify").style.display = "None";
            document.getElementById("lblNotify").style.display = "None";
        }
        else {
            document.getElementById("drpSmartP2PNotify").style.display = "";
            document.getElementById("lblNotify").style.display = "";
        }
    }
    //ZD 100815 Ends

    //ALLDEV-826 Starts
    function chkEnableConfPwd(val) {
        var selval = val.value;
        var enableHostandGuestPwd = document.getElementById("drpEnableHostandGuestPwd").value;
        if (selval == 1) {
            if (enableHostandGuestPwd == 1) {
                alert(PasswordAlert);
                document.getElementById("lstEnableConfPassword").value = 0;
            }
        }
    }

    function chkEnableHostandGuestPwd(val) {
        var selval = val.value;
        var enableConfPwd = document.getElementById("lstEnableConfPassword").value;
        if (selval == 1) {
            if (enableConfPwd == 1) {
                alert(PasswordAlert);
                document.getElementById("drpEnableHostandGuestPwd").value = 0;
            }            
        }
    }
    //ALLDEV-826 Ends
	//ALLDEV-837 Starts
    function chkEnableSync(val) {
        var selval = val.value;
        if (selval == 1) {            
            document.getElementById("tdLaunchBuffer").style.display = "";
            document.getElementById("tdTxtLaunchBuffer").style.display = "";
        }
        else {
            document.getElementById("tdLaunchBuffer").style.display = "None";
            document.getElementById("tdTxtLaunchBuffer").style.display = "None";
        }
    }
    //ALLDEV-837 Ends
    //ALLDEV-498 Starts
    function getConfAdminAddressList() {
        var url = "../en/emaillist2main.aspx?t=e&frm=confadmin&fn=frmMainadminiatrator&n=";
        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470px,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else if (!winrtc.closed) {
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470px ,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=920ss,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }
    function deleteConfAdmin() {
        document.getElementById("txtConfAdmin").value = "";
        document.getElementById("txtAdminID").value = "";
    }
    //ALLDEV-498 Ends
</script>

<!-- JavaScript finish -->
<html>
<body>
<div style="text-align: left">
    <form name="frmMainadminiatrator" id="frmMainadminiatrator" autocomplete="off" method="Post" action="mainadministrator.aspx"
    onsubmit="return ValidateInput()" language="JavaScript" runat="server"><%--ZD 101190--%>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
     <%--<asp:scriptmanager id="OrgOptionScriptManager" runat="server" asyncpostbacktimeout="600">
        </asp:scriptmanager>--%>
    <input type="hidden" name="cmd" value="SetSystemDetails" />
    <input type="hidden" name="ClosedDay" value="" />
    <input type="hidden" id="helpPage" value="91" />
    <input type="hidden" id="Poly2MGC" runat="server" value="" />
    <input type="hidden" id="Poly2RMX" runat="server" value="" />
    <input type="hidden" id="CTMS2Cisco" runat="server" value="" />
    <input type="hidden" id="CTMS2Poly" runat="server" value="" />
    <input type="hidden" id="hdnSurURL" runat="server" value="" />
    <input type="hidden" id="hdnTimeDur" runat="server" value="" />
    <input type="hidden" id="hdnTierIDs" runat="server" value="" />
    <%--FB 2637--%>
    <input type="hidden" id="hdnZuluChange" runat="server" value="" />
    <%--FB 2588--%>
    <input type="hidden" name="hdnFileWhiteList" id="hdnFileWhiteList" runat="server" /> <%--ZD 100085--%>
    
    <%--ZD 101019 START--%>
    <input id="chkPosAVert" type="hidden" runat="server" value="0" />
    <input id="chkPosBVert" type="hidden" runat="server" value="0" />
    <input id="chkPosCVert" type="hidden" runat="server" value="0" />
    <input id="chkPosDVert" type="hidden" runat="server" value="0" />
    
    <input id="chkPosAHor" type="hidden" runat="server" value="" />
    <input id="chkPosBHor" type="hidden" runat="server" value="" />
    <input id="chkPosCHor" type="hidden" runat="server" value="" />
    <input id="chkPosDHor" type="hidden" runat="server" value="" />
    <input id="hdnMultiAdmins" type="hidden" runat="server" value="" /> <%--ZD 102085--%>
    <input type="hidden" name="hdnPersonnelAlert" id="hdnPersonnelAlert" runat="server" /> <%--ZD 103046--%>
    <%--ZD 101019 END--%>
    <input id="hdnCodian" type="hidden" runat="server" value ="" /> <%--ZD 101869--%>
    
    <div align="center">
        <h3><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_OrganizationOp%>" runat="server"></asp:Literal></h3>
        <br />
        <asp:label id="errLabel" cssclass="lblError" runat="server" ></asp:label> <%--ZD 103857--%>
    </div>
    <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
    <div align="center">
        <table cellpadding="2" cellspacing="0" width="100%" border="0">
         <%--FB 2841 start--%>
        <tr>
         <td align="left" valign="top" style="margin-left:20px">
         <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_ExpandAll%>" runat="server"></asp:Literal></span>
          <input id="chkExpandCollapse" type="checkbox" onclick="javascript:ExpandAll();" onkeydown="if(event.keyCode == 13){return false;}" /></td></tr><%--ZD 100420--%>
         <%--FB 2841 End--%>
            <tr>
            </tr>
            <tr>
                <td colspan="7" height="10">
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" valign="bottom" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton alternatetext="Expand/Collapse"  cssclass ="0" id="img_USROPT" runat="server" imageurl="image/loc/nolines_plus.gif" 
                                                height="25" width="25" vspace="0" hspace="0"  /> <%--FB 2841--%> <%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_UserOptions%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trUSROPT" runat="server" style="display: none;"><%-- FB 2841--%>
                <td colspan="7">
                    <table width="100%" border="0" cellpadding="5" style="margin-left: -10px;"> <%-- FB 2842--%>
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%;">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_ShowtheseTime%>" runat="server"></asp:Literal>                               <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgshowotime" valign="center" src="image/info.png" runat="server" ToolTip="Timezone control in overall application."/>--%>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:dropdownlist id="TimezoneSystems" runat="server" cssclass="altLong0SelectFormat"
                                    width="170px">
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableDepartme%>" runat="server"></asp:Literal>
                                <%--<asp:ImageButton ID="Imgdeptuser" valign="center" src="image/info.png" runat="server" ToolTip="Control active user display in addressbook."/>--%>
                            </td>
                            <td align="left" style="width: 20%">
                                <asp:dropdownlist id="DrpDwnListDeptUser" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 2%;">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%;">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnablePassword%>" runat="server"></asp:Literal>                                <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgpasswordrule" valign="center"  src="image/info.png" runat="server" ToolTip="Strong password rule for users"/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:dropdownlist id="DrpDwnPasswordRule" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            
                              <%--ZD 100164 START--%>
                              <td style="width: 1%">
                            </td>
                            <td id="tdEnableDisableadvanceduser" runat="server" align="left" class="blackblodtext" style="width: 27%"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_tdEnableDisableadvanceduser%>" runat="server"></asp:Literal></td>
                            <td id="tdDrpDwnAdvUserOption" runat="server" align="left" style="width: 20%">
                                <asp:dropdownlist id="DrpDwnAdvUserOption" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <%--ZD 100164 END--%>
                        
                        </tr>
                         <%--ZD 100781 Starts --%>
                        <tr>
                            <td style="width: 2%;">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%;">
                                <asp:Literal ID="Literal39" Text="<%$ Resources:WebResources, EnablePasswordExpiration%>" runat="server"></asp:Literal>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:dropdownlist id="DrpEnablePwdExp" runat="server" cssclass="alt2SelectFormat" onclick="JavaScript:fnUpdatePasswordExpEnable();">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                               <td style="width: 1%">
                            </td>
                            <td id="lblPwdExp" runat="server" align="left" class="blackblodtext" style="width: 27%">
                                <asp:Literal ID="Literal46" Text="<%$ Resources:WebResources, PasswordExpiration%>" runat="server"></asp:Literal>
                             </td>
                            <td id="tdDrpPwdExp" runat="server" align="left" style="width: 20%">
                                <asp:textbox id="txtPwdExpDays" runat="server" Maxlength="4" cssclass="altText" width="50px"> </asp:textbox>
                                (<asp:Literal ID="Literal47" Text="<%$ Resources:WebResources, Months%>" runat="server"></asp:Literal>)
                                <asp:RangeValidator ID="RangePwdExpDays" Type="Integer"
                                        MinimumValue="1" MaximumValue="9999" Display="Dynamic" ControlToValidate="txtPwdExpDays"
                                        ValidationGroup="Submit" runat="server" ErrorMessage="<%$ Resources:WebResources, ValidationPwdExpiry%>"></asp:RangeValidator>
                                <asp:regularexpressionvalidator id="RegPwdExpDays" validationgroup="Submit"
                                    controltovalidate="txtPwdExpDays" display="dynamic" runat="server"
                                    errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                                <asp:requiredfieldvalidator id="reqExpDays" runat="server" controltovalidate="txtPwdExpDays"
                                    display="dynamic" errormessage="<%$ Resources:WebResources, ValidationPwdEmpty%>"></asp:requiredfieldvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <%--ZD 100781 Ends --%>
                    </table>
                </td>
            </tr>
            <tr style="display: none;">
                <td colspan="7" align="left">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" valign="bottom" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton alternatetext="Expand/Collapse" cssclass ="0"  id="img_ROOM" runat="server" imageurl="image/loc/nolines_minus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_RoomOptions%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trROOM" runat="server" style="display: none;">
                <td colspan="8">
                    <table border="0" width="100%">
                        <tr>
                            <td style="width: 6%;">
                            </td>
                            <td align="left" valign="top" style="width: 185px;" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_RoomTreeExpan%>" runat="server"></asp:Literal></td>
                            <td style="width: 0px">
                            </td>
                            <td valign="top" align="left">
                                <%--  <asp:listitem value="0">
                                </asp:listitem>--%>
                                <asp:dropdownlist id="lstRoomTreeLevel" runat="server" cssclass="altLong0SelectFormat"
                                    width="205px">
                                        <asp:ListItem Value="1">Expanded - Top Tier Only</asp:ListItem>
                                        <asp:ListItem Value="2">Expanded - Middle Tier Only</asp:ListItem>
                                        <asp:ListItem Value="3" Selected="True">Expanded - All Levels</asp:ListItem>
                                        <asp:ListItem Value="list">List View</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td colspan="4">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_CONFOPT" alternatetext="Expand/Collapse" cssclass ="0"  runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                    {%>Hearing Options<%}
                                                else
                                                { %><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ConferenceOptions%>" runat="server"></asp:Literal><%}%></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trCONFOPT" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td> 
                                                        <asp:imagebutton id="img_CONFDEF" alternatetext="Expand/Collapse"  cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_DefaultSetting%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trCONFDEF" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-4px"> <%-- FB 2842--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" style="width: 25%;" valign="middle">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Default hearing type<%}
                                                      else
                                                      { %><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, DefaultConferenceType%>" runat="server"></asp:Literal><%}%><%--added for FB 1428 Start--%>  <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgdeconftype"  valign="center" src="image/info.png" runat="server" ToolTip="Default all conference to selected type."/>--%>
                                        </td>
                                        <td valign="middle" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="lstDefaultConferenceType" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Selected="True" Value="6" Text="<%$ Resources:WebResources, AudioOnly1%>"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, AudioVideo%>"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="<%$ Resources:WebResources, ManageConference_pttopt%>"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="<%$ Resources:WebResources, RoomOnly1%>"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="<%$ Resources:WebResources, Hotdesking%>"></asp:ListItem><%--ZD 100719--%>
                                                    <asp:ListItem Value="9" Text="<%$ Resources:WebResources, OBTP%>"></asp:ListItem><%--ZD 100513--%>
                                                </asp:dropdownlist>
                                        </td>
                                        <td align="right" style="width: 1%;" valign="middle">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label7" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label7%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgdelinerate"  valign="center" src="image/info.png" runat="server" ToolTip="Default linerate to all calls."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist cssclass="altSelectFormat" width="120px" id="lstLineRate" runat="server"
                                                datatextfield="LineRateName" datavaluefield="LineRateID">
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="middle" style="width: 25%">
                                            <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_DefaultConfere%>" runat="server"></asp:Literal></span>            <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgdeconfdur"  valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to given duration."/>--%>
                                        </td>
                                        <td valign="middle" align="left" style="width: 25%">
                                            <asp:textbox id="txtDefaultConfDuration" runat="server" cssclass="altText" width="50px" onblur="javascript:return fnConfDurRangecheck()"> </asp:textbox> <%--ZD 103925 Start--%>
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_mins%>" runat="server"></asp:Literal>
                                          <asp:RangeValidator ID="RangeDefaultConfDuration" Type="Integer" Enabled="false"
                                                    MinimumValue="15"  MaximumValue="2000000000" Display="Dynamic" ControlToValidate="txtDefaultConfDuration"
                                                    ValidationGroup="Submit" runat="server" ErrorMessage="<%$ Resources:WebResources, ConfValidationRange%>"></asp:RangeValidator> <%--ZD 103925 end--%>
                                            <asp:regularexpressionvalidator id="RegDefaultConfDuration" validationgroup="Submit"
                                                controltovalidate="txtDefaultConfDuration" display="dynamic" runat="server"
                                                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                                            <asp:requiredfieldvalidator id="reqdefaultduration" runat="server" controltovalidate="txtDefaultConfDuration"
                                                display="dynamic" errormessage="<%$ Resources:WebResources, Required%>"></asp:requiredfieldvalidator> <%--ZD 103925 removed 24 hrs validation 103925 Start --%>
                                            <%--FB 2635--%>
                                        </td> <%--ZD 101757--%>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="middle" id="tdActMsgDel" runat="server">
                                            <asp:Literal ID="Literal59" Text="<%$ Resources:WebResources, DisplayActiveMessageDelivery%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="middle" style="width: 20%;" align="left" id="tdCtlActMsgDel" runat="server">
                                            <asp:dropdownlist id="drpActiveMsgDel" runat="server" cssclass="alt2SelectFormat" onclick="javascript:fnActiveMsgDelivery()">
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--ZD 100899 start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="middle" style="width: 25%">
                                            <span style="align: left"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ScheduleLimit%>" runat="server"></asp:Literal></span>          
                                        </td>
                                        <td valign="middle" align="left" style="width: 25%">
                                            <asp:textbox id="txtScheduleLimit" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            (<asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Months%>" runat="server"></asp:Literal>)
                                            <asp:regularexpressionvalidator id="RegScheduleLimit" validationgroup="Submit"
                                                controltovalidate="txtScheduleLimit" display="dynamic" runat="server"
                                                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="middle" id="tdTextmsg" runat="server" ><%--FB 2934<%--ZD 101757--%>
                                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, mainadministrator_ActiveMessage%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgmsgolay"  valign="center"  src="image/info.png" runat="server" ToolTip="Default conference end message for Audio/Video calls."/>--%>
                                        </td>
                                        <td valign="middle" style="width: 20%;" align="left" id="tdMsgPopup"  runat="server"> <%--ZD 101757--%>
											<%--ZD 100420--%>                                     
                                                <button width="70%" runat="server" id="btmTxtmsgPopup" class="altMedium0BlueButtonFormat">
                                                    <asp:Literal ID="Literal58" Text="<%$ Resources:WebResources, mainadministrator_btmTxtmsgPopup%>" runat="server"></asp:Literal>
												</button>
											<%--ZD 100420--%>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--ZD 100899 start--%>
                                    <%--ZD 103216 Start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <asp:Literal ID="Literal68" Text="<%$ Resources:WebResources, TravelAvoidTrack %>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList ID="TravelAvodTrck" runat="server" CssClass="alt2SelectFormat">
                                            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes %>"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No %>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                       <%-- // ZD 103216 End--%>
                                        <%--  ZD 104151 Start--%>
                                    <td align="left" class="blackblodtext" style="width: 27%" valign="top">
                                    <asp:Literal ID="Literal69" Text="<%$ Resources:WebResources, Emptyconf %>" runat="server"></asp:Literal>
                                    </td>
                                    <td align="left" style="width: 100%">
                                    <asp:DropDownList runat="server" id="lstEmptyconf" cssclass="altSelectFormat"></asp:DropDownList>
                                    </td>
                                   <td style="width: 1%">
                                   </td>
                                   <%--ZD 104151 End--%>

                                    </tr>
                                    <%--ALLDEV-498 Starts--%>
                                    <tr>
                                        <td style="width:1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width:25%">
                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceAdministrator%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" style="width:25%" align="left">
                                            <asp:TextBox ID="txtConfAdmin" CssClass="altText" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtAdminID" runat="server" BackColor="Transparent" BorderColor="White" TabIndex="-1"
                                                    BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                            <a href="javascript: getConfAdminAddressList();" id="editConfAdmin" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="Edit" style="cursor:pointer;" title="<%$ Resources:WebResources, AddressBook%>" runat="server"/>
                                            </a>                                            
                                            <a href="javascript: deleteConfAdmin();" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="<%$ Resources:WebResources, Delete%>" runat="server"/>
                                            </a> 
                                        </td>
                                    </tr>
                                    <%--ALLDEV-498 Ends--%>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_CONFTYPE" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>                                                
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Types%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trCONFTYPE" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-4px"> <%-- FB 2842--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" valign="top">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable audio/video hearing<%}
                                                      else
                                                      { %><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, EnableAVConference%>" runat="server"></asp:Literal><%}%><%--added for FB 1428 Start--%>
                                            <%--<asp:ImageButton ID="Imgeavconf" valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to audio/video type."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableAudioVideoConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable audio only hearing<%}
                                                      else
                                                      { %><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, EnableAudioConference%>" runat="server"></asp:Literal><%}%><%--added for FB 1428 Start--%>
                                            <%--<asp:ImageButton ID="Imgeaconf" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to audio type."/>--%>
                                        </td>
                                        <td style="width: 20%;" valign="top" align="left">
                                            <asp:dropdownlist id="lstEnableAudioOnlyConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" valign="top">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable room hearing<%}
                                                      else
                                                      { %><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, EnableRoomConference%>" runat="server"></asp:Literal><%}%>
                                            <%--<asp:ImageButton ID="Imgeroomconf" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to room type."/></strong>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableRoomConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                            <span style="align: left">
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable point-to-point hearing<%}
                                                      else
                                                      { %><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, EnablePttoPtConference%>" runat="server"></asp:Literal><%}%><%--added for FB 1428 Start--%></span>
                                            <%--<asp:ImageButton ID="Imgppconf" valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to point-to-point type."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <%--FB 2430--%>
                                            <asp:dropdownlist id="p2pConfEnabled" runat="server" cssclass="alt2SelectFormat"
                                                onclick="javascript:ChangeEnableSmartP2P();">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="lblEnableSmartP2P" runat="server" text="<%$ Resources:WebResources, mainadministrator_lblEnableSmartP2P%>"></asp:label>    <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgesmartp2p" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to point-to-point when only two endpoints."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableSmartP2P" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                                <%--ZD 100815 Start--%>
                                            <span style="white-space: nowrap;">
                                              <span class="blackblodtext" id="lblNotify" runat="server"> <asp:Literal Text="<%$ Resources:WebResources, EditInventory_Notify%>" runat="server"></asp:Literal></span>
                                                <asp:dropdownlist id="drpSmartP2PNotify" cssclass="alt2SelectFormat" runat="server" style="width:95px">
                                                    <asp:ListItem Value="1" Selected="True"  Text="<%$ Resources:WebResources, Yes%>">></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, mainadministartor_ModifYes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                             </span>
                                            <%--ZD 100815 End--%>  
                                        </td>
                                        <td style="width: 1%;">
                                        </td><%--ZD 100719 Start--%>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                            <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, EnableHotdeskingConference%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <%--ZD 103095 Starts--%> 
                                        <td valign="top" style="width: 20%;" align="left" nowrap="nowrap">
                                            <asp:dropdownlist id="lstEnableHotdeskingConference" runat="server" cssclass="alt2SelectFormat" onclick="javascript:ChangeEnableWaitList();">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:dropdownlist>
                                            <asp:Label Text="<%$ Resources:WebResources, mainadministrator_EnableWaitList%>" CssClass="blackblodtext" runat="server" ></asp:Label>
                                            <asp:CheckBox ID="chkEnableWaitList" runat="server"/>
                                        </td>
                                        <%--ZD 103095 End--%> 
                                         <td style="width: 1%;">
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>                                        
                                        <%--FB 2870 Start--%>
                                        <td align="left" style="width: 25%;" class="blackblodtext" valign="top">
                                            <span style="align: left">
                                                <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, mainadministrator_CTSNumericID %>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList id="lstEnableNumericID" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:DropDownList>
                                        </td>                                        
                                        <%--FB 2870 End--%>                                       
                                        <%--ZD 100719 End--%>
                                        <%--ZD 100513 Starts--%>
                                         <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                            <span style="align: left"><asp:Literal ID="ltlOBTPConf" Text="<%$ Resources:WebResources, mainadministrator_EnableOBTPConference%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:dropdownlist id="drpOBTPConf" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                         <td style="width: 1%;">
                                        </td>
                                        <%--ZD 100513 Ends--%>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_FEAT" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Features%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trFEAT" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-2px" > <%-- FB 2842--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="lblEnablePublicConf" runat="server" text="<%$ Resources:WebResources, mainadministrator_lblEnablePublicConf%>"></asp:label>     <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgenpublicconf" valign="center" src="image/info.png" runat="server" ToolTip="Public conference feature will be enabled."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnablePublicConf" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 28%;" valign="top" class="blackblodtext"><%-- ZD 103550--%>
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableParticip%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgeopenfreg" valign="center"  src="image/info.png" runat="server" ToolTip="All public calls will be open for registration based on this switch."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DynamicInviteEnabled" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" valign="top" class="blackblodtext">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Default all hearing to public<%}
                                                      else
                                                      { %><asp:Literal ID="Literal41" Text="<%$ Resources:WebResources, DefaultAllConferencestoPublic%>" runat="server"></asp:Literal><%}%><%--added for FB 1428 Start--%>
                                            <%--<asp:ImageButton ID="Imgdeconfpublic" valign="center"  src="image/info.png" runat="server" ToolTip="All calls will be default to public/private"/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="DefaultPublic" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--FB 2780--%>
                                        <td align="left" style="width: 27%" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableStartMo%>" runat="server"></asp:Literal></td>
                                        <td style="width: 20%" align="left">
                                            <asp:dropdownlist id="drpEnableStartMode" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;"></td> <%-- FB 2842--%>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <div id="tdMaxParty" runat="server" style="display: none">
                                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_tdMaxParty%>" runat="server"></asp:Literal>  <%--ZD 100806--%>
                                                <%--<asp:ImageButton ID="ImageButton1" valign="center"  src="image/info.png" runat="server" ToolTip="Maximum VMR parties allowed to create conference"/>--%>
                                            </div>
                                        </td>
                                        <td id="Td1" valign="top" style="width: 25%;" align="left">
                                            <div id="tdMaxPartyCount" runat="server" style="display: none">
                                                <asp:textbox id="txtMaxVMRParty" runat="server" cssclass="altText" width="50px" maxlength="4"> </asp:textbox>
                                                <asp:regularexpressionvalidator id="RegularExpressionValidator6" validationgroup="Submit"
                                                    controltovalidate="txtMaxVMRParty" display="dynamic" runat="server" setfocusonerror="true"
                                                    errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                                            </div>
                                        </td>
                                        </tr><%-- ZD 103550--%>
                                        <%--FB 2609 Start--%>
                                       <%-- 102514 Starts --%>
                                        <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                        <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_MeetGreetBuf%>" runat="server"></asp:Literal></td>
                                        <td id="Td2" valign="top" style="width: 20%;" align="left"> <%-- FB 2842--%>
                                            <asp:TextBox id="txtMeetandGreetBuffer" runat="server" cssclass="altText" width="50px"
                                                maxlength="4"></asp:TextBox>                                               
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Td2%>" runat="server"></asp:Literal>
                                            <asp:regularexpressionvalidator ID="regmeetandgreet" ValidationGroup="Submit" ControlToValidate="txtMeetandGreetBuffer"
                                                Display="Dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>"
                                                ValidationExpression="\d+"></asp:regularexpressionvalidator> 
                                              </td> 
                                             <%-- 102514 Start  --%>                                               
                                          <td style="width: 1%;">
                                        </td>
                                         <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">  
                                         <asp:Literal Text="<%$ Resources:WebResources, OnSiteAVSupportBuffer%>" runat="server"></asp:Literal>
                                         </td>
                                           <td valign="top" style="width: 20%" align="left">
                                            <asp:TextBox ID="txtOnSiteAVSupportBuffer" runat="server" CssClass="altText" Width="50px" MaxLength ="4"></asp:TextBox> 
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Td2%>" runat="server"></asp:Literal>
                                            <asp:RegularExpressionValidator ID="regonsitesupport" ValidationGroup="Submit" ControlToValidate="txtOnsiteAVSupportBuffer" Display ="Dynamic" 
                                            runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            
                                           </td> 
                                              </tr> 
                                               <tr>
                                               <td style="width: 1%;"></td>
                                               <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">  
                                         <asp:Literal Text="<%$ Resources:WebResources, CallMonitoringBuffer%>" runat="server"></asp:Literal>
                                         </td>
                                           <td valign="top" style="width: 20%" align="left">
                                            <asp:TextBox ID="txtCallMonitoringBuffer" runat="server" CssClass="altText" Width="50px" MaxLength ="4"></asp:TextBox> 
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Td2%>" runat="server"></asp:Literal>
                                            <asp:RegularExpressionValidator ID="RegCalmonitoring" ValidationGroup="Submit" ControlToValidate="txtCallMonitoringBuffer" Display ="Dynamic" 
                                            runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                               </td>

                                               <td style="width: 1%;"></td>
                                                <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">  
                                         <asp:Literal Text="<%$ Resources:WebResources, DedicatedVNOCOperatorBuffer%>" runat="server"></asp:Literal>
                                         </td>
                                           <td valign="top" style="width: 20%" align="left">
                                            <asp:TextBox ID="txtDedicatedVNOCOperatorBuffer" runat="server" CssClass="altText" Width="50px" MaxLength ="4"></asp:TextBox> 
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Td2%>" runat="server"></asp:Literal>
                                            <asp:RegularExpressionValidator ID="RegDedicated" ValidationGroup="Submit" ControlToValidate="txtDedicatedVNOCOperatorBuffer" Display ="Dynamic" 
                                            runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                               </td>
                                               </tr>
                                       <%-- 102514 End --%>
                                     
                                        <%--FB 2609 End--%>
                                        <td style="width: 1%;"></td> <%-- FB 2842--%>
                                    </tr>
                                    <tr id="trRecurrence" runat="server"> <%--ZD 100518--%>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%">
                                           <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, EnableRecurConference%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgrecurrconf" valign="center"  src="image/info.png" runat="server" ToolTip="Recurrence feature will be enabled for calls."/>--%>
                                        </td>
                                        <td align="left" style="width: 25%">
                                            <asp:dropdownlist id="RecurEnabled" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 27%;" rowspan="1" valign="top">
                                            <%--FB 2052 - Start--%>
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableSpecial%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgesplrecurr" valign="baseline"  src="image/info.png" runat="server" ToolTip="Special recurrence will be enabled for calls."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DrpDwnListSpRecur" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%;" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableVIPConf%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgvipconf" valign="center"   src="image/info.png" runat="server" ToolTip="VIP mode will be enabled."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="DrpVIP" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableStartNo%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgimmconf" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable start now conference features."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:dropdownlist id="lstEnableImmediateConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="lblEnableConfPassword" runat="server" text="<%$ Resources:WebResources, mainadministrator_lblEnableConfPassword%>"></asp:label>     <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="ImgEconfpass" valign="center"  src="image/info.png" runat="server" ToolTip="Enable conference password feature for calls."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableConfPassword" cssclass="alt2SelectFormat" onchange="chkEnableConfPwd(this);" runat="server"> <%--ALLDEV-826--%>
                                                <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                    <%--ALLDEV-826 Starts--%>
                                        <td style="width:1%;" align="left"> 
                                        </td>
                                        <td align="left" style="width:27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_MinimumChar%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" style="width: 20%" align="left">
                                            <asp:TextBox ID="txtVMRPW" runat="server" CssClass="altText" Width="50px" MaxLength="2"></asp:TextBox><%--103847--%>
                                            <asp:RegularExpressionValidator id="Regularexpressionvalidator3" ControlToValidate="txtVMRPW" ValidationGroup="Submit"
                                                ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"
                                                Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                            <asp:RangeValidator ID="RangeVMRPwd" ControlToValidate="txtVMRPW" Type="Integer" MinimumValue="4"
                                                MaximumValue="9999" ErrorMessage="<%$ Resources:WebResources, maimainadministrator_PassReq%>"
                                                Display="Dynamic" runat="server"></asp:RangeValidator>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width:1%">
                                        </td>
                                        <td style="width:25%" align="left" class="blackblodtext">
                                            <asp:label text="<%$ Resources:WebResources, mainadministrator_lblEnableHostandGuestPassword%>" runat="server"></asp:label>
                                        </td>
                                        <td style="width:25%" align="left">
                                            <asp:dropdownlist ID="drpEnableHostandGuestPwd" onchange="chkEnableHostandGuestPwd(this);" cssclass="alt2SelectFormat" runat="server">
                                                <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                        <td style="width:1%;" align="left"> 
                                        </td>
                                        <td align="left" style="width:27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_RequireUnique%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" align="left" style="width:20%;">
                                            <asp:dropdownlist id="DrpUniquePassword" runat="server" cssclass="alt2SelectFormat">
                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                        <td style="width:1%;">
                                        </td>
                                    </tr>                                    
                                    <%--ALLDEV-826 Ends--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_AudioVideoRoom%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgvidroomconf" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to control Video room display for room only conference."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="DrpDwnDedicatedVideo" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_TelepresenceRo%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgtelepresenceroomconf" valign="center" src="image/info.png" runat="server" ToolTip="Switch to control Telepresence room display for room only conference."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DrpDwnFilterTelepresence" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="trBuffer" runat="server" align="left" style="width: 27%;" class="blackblodtext" valign="top"> <%--ZD 100518--%>
                                            <span style="align: left"><asp:Literal ID="Literal63" Text="<%$ Resources:WebResources, mainadministrator_EnableConferen%>" runat="server"></asp:Literal></span>
                                            <%--<asp:ImageButton ID="Imgbuffzone" valign="center" src="image/info.png" runat="server" ToolTip="Buffer period can be set to calls."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%">
                                            <asp:dropdownlist id="EnableBufferZone" runat="server" cssclass="alt2SelectFormat"
                                                onclick="javascript:fnBufferOptions()">
                                                    <%-- FB 2398 --%>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%; display:none;">
                                        </td>
                                        <td align="left" style="width: 25%;display:none;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal ID="Literal64" Text="<%$ Resources:WebResources, mainadministrator_EnableRoomSer%>" runat="server"></asp:Literal>        <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgeroomser" valign="center"  src="image/info.png" runat="server" ToolTip="Room service type feature can be enabled"/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;display:none;" align="left">
                                            <asp:dropdownlist id="lstEnableRoomServiceType" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="trBufferOptions" runat="server">
                                        <%-- FB 2398 --%>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SetupTime%>" runat="server"></asp:Literal></span> <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgsetuptime" valign="center"  src="image/info.png" runat="server" ToolTip="Default pre start time can be set for all calls."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:textbox id="txtSetupTime" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_mins%>" runat="server"></asp:Literal>
                                            <asp:rangevalidator id="rangesetuptime" setfocusonerror="true" type="Integer" minimumvalue="0"
                                                maximumvalue="60" display="Dynamic" controltovalidate="txtSetupTime" runat="server"
                                                errormessage="<%$ Resources:WebResources, ValidationSetup%>"></asp:rangevalidator> <%--FB 2926--%>
                                            <asp:regularexpressionvalidator id="RegtxtSetupTime" validationgroup="Submit" controltovalidate="txtSetupTime"
                                                display="dynamic" runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, NumericValuesOnly%>"
                                                validationexpression="\d+"></asp:regularexpressionvalidator>
                                                <asp:CustomValidator id="cussetup" Enabled="true"  Display="Dynamic"
                                            ControlToValidate="txtSetupTime"  runat="server"  ClientValidationFunction="fnCompareSetup"></asp:CustomValidator><%-- ZD 100433--%>
                                            <%--ZD 101755 start--%>
                                            &nbsp;
                                            <span class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Display%>" runat="server"></asp:Literal></span>
                                                 <asp:dropdownlist id="DrpSetupTimeDisplay" runat="server" cssclass="alt2SelectFormat" width="70px">
                                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist> 
                                           <%-- ZD 101755 End--%>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="27%">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_TearDownTime%>" runat="server"></asp:Literal> <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgteardowntime" valign="center"  src="image/info.png" runat="server" ToolTip="Default post conference time can be set for all calls"/>--%>
                                        </td>
                                        <td style="height: 33px; width: 20%" align="left" valign="top">
                                            <asp:textbox id="txtTearDownTime" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_mins%>" runat="server"></asp:Literal>
                                            <asp:rangevalidator id="Rangeteardowntime" setfocusonerror="true" type="Integer"
                                                minimumvalue="0" maximumvalue="60" display="Dynamic" controltovalidate="txtTearDownTime"
                                                runat="server" errormessage="<%$ Resources:WebResources, ValidationTeardown%>"></asp:rangevalidator>  <%--FB  2926--%>
                                            <asp:regularexpressionvalidator id="RegtxtTearDownTime" validationgroup="Submit"
                                                controltovalidate="txtTearDownTime" display="dynamic" runat="server" setfocusonerror="true"
                                                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                                              <asp:CustomValidator id="cusTearDown" Enabled="true" Display="Dynamic"
                                            ControlToValidate="txtTearDownTime"  runat="server"  ClientValidationFunction="fnCompareTear"></asp:CustomValidator><%-- ZD 100433--%>
                                            <%--ZD 101755 start--%>
                                            &nbsp;
                                             <span class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Display%>" runat="server"></asp:Literal></span>
                                                 <asp:dropdownlist id="DrpTeardownTimeDisplay" runat="server" cssclass="alt2SelectFormat" width="70px">
                                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist> 
                                                <%--ZD 101755 End--%>
                                        </td>
                                        <td width="1%">
                                        </td>
                                    </tr>
                                    <tr id="trMCUBufferOptions" runat="server">
                                        <%-- FB 2440 --%>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_MCUPreStartTi%>" runat="server"></asp:Literal></span>
                                            <%--<asp:ImageButton ID="Imgmcuprestart" valign="center"  src="image/info.png" runat="server" ToolTip="Default pre lauch calls to MCU"/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%" >
                                            <span style="white-space: nowrap;">
                                                <asp:TextBox id="txtMCUSetupTime" MaxLength="2" runat="server" cssclass="altText" width="50px"> </asp:TextBox><%--103847--%>
                                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_mins%>" runat="server"></asp:Literal><span class="blackblodtext">
                                                &nbsp; <%--FB 2998--%>
                                                <span class="blackblodtext"> <asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, mainadministrator_Display%>" runat="server"></asp:Literal></span>
                                                 <asp:dropdownlist id="MCUSetupDisplay" runat="server" cssclass="alt2SelectFormat" width="70px">
                                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist>            
                                            </span>                              
                                            <br /><%--ZD 100405--%>
                                           <%-- <asp:RangeValidator id="RangeValidator1" type="Integer" minimumvalue="-15"
                                                maximumvalue="15" display="Dynamic" controltovalidate="txtMCUSetupTime" runat="server"
                                                errormessage="MCU pre start time is not allowed more than 15 mins" validationgroup="Submit"></asp:RangeValidator>--%>
											<%-- ZD 100433--%>
                                            <asp:RegularExpressionValidator id="RegMCUSetupTime" validationgroup="Submit"
                                                controltovalidate="txtMCUSetupTime" display="dynamic" runat="server"
                                                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                            <%--FB 2998 Validation Part starts //ZD 100085--%>
											<%--<asp:CompareValidator  id="cmpNumbers" Enabled="true"  ErrorMessage="MCU Pre start time should be less than or equal to Setup Time." display="dynamic"
                                             ControlToCompare="txtSetupTime" ControlToValidate="txtMCUSetupTime" Operator="LessThanEqual" runat="server" Type="Integer"
                                              ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>--%>
                                             <%--FB 2998 Validation Part Ends--%>
                                             <asp:CustomValidator id="customMCUPreStart" Enabled="true"  ErrorMessage="<%$ Resources:WebResources, ValidationMCUStartTime%>" Display="Dynamic"
                                             ControlToValidate="txtMCUSetupTime"  runat="server"  ClientValidationFunction="fnCompareSetup" ValidationGroup="Submit" ></asp:CustomValidator><%-- ZD 100433--%>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="27%">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_MCUPreEndTime%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgmcuprend" valign="center"  src="image/info.png" runat="server" ToolTip="Default post conference calls to MCU"/>--%>
                                        </td>
                                        <td style="height: 33px; width: 20%" align="left" valign="top" >
                                        <span style="white-space: nowrap;">
                                            <asp:textbox id="txtMCUTearDownTime" MaxLength="2" runat="server" cssclass="altText" width="50px"> </asp:textbox><%--103847--%>
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_mins%>" runat="server"></asp:Literal>
                                            &nbsp;<%--FB 2998--%>
                                            <span class="blackblodtext"> <asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, mainadministrator_Display%>" runat="server"></asp:Literal></span>
                                             <asp:dropdownlist id="MCUTearDisplay" runat="server" cssclass="alt2SelectFormat" width = "70px">
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist>                                         
                                             </span>
                                            <br /> <%--ZD 100405--%>
                                           <%-- <asp:rangevalidator id="RangeValidator2" type="Integer" minimumvalue="-15"
                                                maximumvalue="15" display="Dynamic" controltovalidate="txtMCUTearDownTime" runat="server"
                                                errormessage="MCU pre end time is not allowed more than 15 mins" validationgroup="Submit"></asp:rangevalidator>--%>
											<%-- ZD 100433--%>
                                            <asp:regularexpressionvalidator id="RegMCUTearDownTime" validationgroup="Submit"
                                                controltovalidate="txtMCUTearDownTime" display="dynamic" runat="server" 
                                                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="^-{0,1}\d+$"></asp:regularexpressionvalidator>
                                            <%--FB 2998 Validation Part starts //ZD 100085--%>
                                            <%--<asp:CompareValidator  id="cmpTeardown" Enabled="true"  ErrorMessage="MCU Pre-End Time should be less than or equal to Tear-Down Time." display="dynamic"
                                             ControlToCompare="txtTearDownTime" ControlToValidate="txtMCUTearDownTime" Operator="LessThanEqual" runat="server" Type="Integer"
                                              ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>--%>
                                            <%--FB 2998 Validation Part Ends--%>
                                            <asp:CustomValidator id="customMCUPreEnd" Enabled="true" ErrorMessage="<%$ Resources:WebResources, ValidationMCUEndTime%>" Display="Dynamic"
                                            ControlToValidate="txtMCUTearDownTime"  runat="server" ClientValidationFunction="fnCompareTear" ValidationGroup="Submit"></asp:CustomValidator><%-- ZD 100433--%>
                                        </td>
                                        <td width="1%">
                                        </td>
                                    </tr>
                                    <%--ALLDEV-837 Starts--%>
                                    <tr>
                                        <td style="width:1%">
                                        </td>
                                        <td align="left" valign="top" rowspan="1" style="width:25%" class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableSyncLaunchBuffer%>" runat="server"></asp:Literal>
                                        </td>
                                        <td align="left" valign="top" style="width:25%">
                                            <asp:dropdownlist ID="drpDwnEnableSyncLaunchBuffer" onchange="chkEnableSync(this);" cssclass="alt2SelectFormat" runat="server">
                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                        <td align="left" style="width:1%"> 
                                        </td>
                                        <td id="tdLaunchBuffer" align="left" valign="top" rowspan="1" style="width:27%; display:none" class="blackblodtext" runat="server">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_LaunchBuffer%>" runat="server"></asp:Literal>
                                        </td>
                                        <td id="tdTxtLaunchBuffer" valign="top" align="left" style="width:20%; display:none" runat="server">
                                            <span style="white-space:nowrap">
                                                <asp:TextBox ID="txtLaunchBuffer" maxlength="2" style="width:50px" CssClass="altText" runat="server"></asp:TextBox>
                                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_mins%>" runat="server"></asp:Literal>
                                            </span>
                                            <asp:RegularExpressionValidator ControlToValidate="txtLaunchBuffer" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>"
                                                Display="dynamic" ValidationExpression="^[0-9]*$" runat="server"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width:1%">
                                        </td>
                                    </tr>
                                    <%--ALLDEV-837 Ends--%>
                                       <%--FB 2670 START--VNOC Operator design--%>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableConferenceSupport%>" runat="server"></asp:Literal></span> <%--FB 3023--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkOnsiteAV" type="checkbox" runat="server" />
                                                        <strong style="align: left"><asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ManageConference_OnSiteAVSuppo%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="chkConciergeMonitor" type="checkbox" runat="server" />
                                                        <strong style="align: left"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, ManageConference_CallMonitoring%>" runat="server"></asp:Literal></strong> <%--FB 3023--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td valign="top" align="left" style="width: 27%"> <%-- FB 2842--%>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkMeetandGret" type="checkbox" runat="server" />
                                                        <strong style="align: left"><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, ManageConference_MeetandGreet%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                    <%--ZD 103046 Start--%>
                                                    <td>                                                        
                                                        <strong style="align: left"><asp:Literal ID="Literal66" Text="<%$ Resources:WebResources, mainadministrator_PersonnelAlert%>" runat="server"></asp:Literal></strong>                                                        
                                                    </td>
                                                    <%--ZD 103046 End--%>                                                    
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="chkDedicatedVNOC" type="checkbox" runat="server" />
                                                        <strong style="align: left"><asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, ManageConference_DedicatedVNOC%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                    
                                               </tr>
                                            </table>
                                        </td>
									    <%-- FB 2842 start--%><%--ZD 103046 Start--%>
                                        <td style="width: 20%; padding-left:0%">
                                            <table>
                                                <tr>
                                                    <td style="padding-left:1%">
                                                        <asp:TextBox id="txtPersonnelAlert" runat="server" CssClass="altText" Width="65%" ValidationGroup="AddEmailAddress" ></asp:TextBox>                                            
                                                        <asp:Button id="btnPersonnelAlert" runat="server" Text="<%$ Resources:WebResources, mainadministrator_btnWhiteList%>" class="altShortBlueButtonFormat" OnClientClick="javascript:return AddRemovePersonnelAlert('add')" />                                                        
                                                        <asp:CustomValidator id="CustomPersonnalAlert" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" Display="Dynamic" ValidationGroup="AddEmailAddress"
                                                            ControlToValidate="txtPersonnelAlert" runat="server"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top:10px; padding-left:0%">
                                                       <table>
                                                           <tr>
                                                               <td style="text-align:left">
                                                                   <asp:ListBox runat="server" ID="lstPersonnelAlert" CssClass="altSelectFormat" Rows="5" SelectionMode="Multiple"  onDblClick="javascript:return AddRemovePersonnelAlert('Rem')"  ></asp:ListBox><br />                                                          
                                                                   <asp:Literal ID="Literal67" Text="<%$ Resources:WebResources, mainadministrator_Doubleclickon%>" runat="server"></asp:Literal>
                                                               </td>
                                                               <td>
                                                                   <br /><br />
                                                                   <a href="" onclick="this.childNodes[0].click();return false;"><img border='0' src='image/deleteall.gif' alt="Delete All"  style="cursor:pointer;" title="<%$ Resources:WebResources, RemoveAll%>" runat="server" id="ImgDelPerAlert" width='18' height='18' onclick="JavaScript:ClearAllPersonnelAlert()"/></a> 
                                                               </td>
                                                           </tr>
                                                       </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>										
                                        <td style="width: 1%">
                                        </td>
										<%-- FB 2842 end--%><%--ZD 103046 End--%>
                                     </tr>
                                    <%--FB 2670 END--VNOC Operator design--%>
                                    <%--FB 2637 Starts--%>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Auto-accept modified hearing<%}
                                                      else
                                                      { %><asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, AutoApproveConfModify%>" runat="server"></asp:Literal><%}%><%--added for FB 1428 Start--%>
                                            <%--<asp:ImageButton ID="Imgaamodconf" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable auto-accept modified conference."/>--%>
                                        </td>
                                        <td align="left" style="width: 25%" valign="top">
                                            <asp:dropdownlist id="AutoAcpModConf" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" width="27%"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_AlertonTier1%>" runat="server"></asp:Literal></td>
                                        <td valign="top" style="width: 20%;" align="left"> <%-- FB 2842--%>
                                            <asp:listbox runat="server" focusable="false" id="lstTier1" cssclass="altSelectFormat" datatextfield="Name"
                                                datavaluefield="ID" rows="6" selectionmode="Multiple"></asp:listbox>
                                            <%--ZD 100422 Start--%>
                                            <br /><span style='color: #666666;'>* <asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, SelectMutiTiers%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td style="width: 1%;">
                                        </td><%-- FB 2842 --%>
                                    </tr>
                                    <tr style="display: none">
                                        <td style="width: 1%;"> <%-- FB 2842 --%>
                                        </td>
                                        <td width="25%" align="left" class="blackblodtext">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable Hearing Real-time Display?<%}
                                                      else
                                                      { %><asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, EnableConferenceMonitoring%>" runat="server"></asp:Literal><%}%><%--added for FB 1428 Start--%>
                                        </td>
                                        <td align="left" style="width: 25%">
                                            <asp:dropdownlist id="RealtimeType" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td width="2%">
                                        </td>
                                    </tr>
                                    <%--FB 2637 Ends--%>
                                    <%--FB 2636 Starts --%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableDialPla%>" runat="server"></asp:Literal></td>
                                        <td valign="top" style="width: 25%;" align="left"> <%-- FB 2842 --%>
                                            <asp:dropdownlist id="DrpEnableE164DialPlan" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--FB 2636 Ends --%>
                                        <%-- FB 2641 Start--%>
                                        <td align="left" class="blackblodtext" valign="top" width="27%"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableLineRat%>" runat="server"></asp:Literal></td>
                                        <td valign="top" style="width: 20%" align="left">
                                            <asp:dropdownlist id="drpEnableLineRate" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
										<%-- FB 2842 --%>				
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%-- ZD 100263 Start--%>		
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_FileWhiteList%>" runat="server"></asp:Literal></td>
                                        <td valign="top" style="width: 25%;" align="left">
                                        <asp:CheckBox id="chkWhiteList" runat="server" onclick="JavaScript:Filewhitelistshowhide()"></asp:CheckBox>
                                        <br />
                                            <asp:TextBox id="txtWhiteList" runat="server" CssClass="altText" ></asp:TextBox>
                                            <%--<button name="btnWhiteList" id="btnWhiteList" onclick="javascript:return AddRemoveWhiteList('add')" class="altMedium0BlueButtonFormat" style="width:50pt" >Add </button>--%> <%--ZD 100420--%>
                                            <asp:Button id="btnWhiteList" runat="server" Text="<%$ Resources:WebResources, mainadministrator_btnWhiteList%>" class="altShortBlueButtonFormat" OnClientClick="javascript:return AddRemoveWhiteList('add')" />
                                        </td>
                                      </tr>
                                       <tr id="trWhiteList" runat="server">
                                    <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                        <asp:ListBox runat="server" ID="lstWhiteList" CssClass="altSelectFormat" Rows="5" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveWhiteList('Rem')"  ></asp:ListBox><%-- ZD 100420 End--%>
                                            <br /> <asp:Literal ID="Literal55" Text="<%$ Resources:WebResources, mainadministrator_Doubleclickon%>" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                      <tr id="trEnbWebIntg" runat="server">
                                      <%--ZD 100935 Starts--%>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="tdWebExIntg" runat="server" align="left" class="blackblodtext" valign="top" width="27%"><asp:Literal ID="Literal54" Text="<%$ Resources:WebResources, EnableWebExIntegration%>" runat="server"></asp:Literal> 
                                        </td>
                                        <td valign="top" style="width: 20%" align="left">
                                            <asp:DropDownList id="drpWebExIng" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
									  <%--ZD 100935 End--%>
                                      <%--ZD 100513 Starts--%>
                                        <td id="tdWebEX" runat="server" align="left" class="blackblodtext" valign="top" width="27%"><asp:Literal ID="ltlWebExMode" Text="<%$ Resources:WebResources, mainadministrator_WebExLaunch%>" runat="server"></asp:Literal> 
                                        </td>
                                        <td valign="top" style="width: 20%" align="left">
                                            <asp:DropDownList id="drpWebExMode" runat="server" cssclass="alt2SelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, Automatic%>"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Manual%>"></asp:ListItem>
                                             </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                      <%--ZD 100513 Ends--%>
                                    </tr>
                                    
                                    
                                
                                    
                                    <%-- ZD 100263 End--%>		
                                    <%--<tr><td style="width: 1%;"></td></tr>--%> <%-- FB 2842 --%>
                                    <%-- FB 2641 End--%>
                                    <%-- ZD 100707 Start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal ID="Literal42" Text="<%$ Resources:WebResources, EnableVirtualMeetingRoom%>" runat="server"></asp:Literal><%--ZD 100806--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drpEnableVMR" runat="server" onclick="JavaScript:ShowHideVMROptions();" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="tdPersonalVMR" runat="server" align="left" class="blackblodtext" valign="top" width="27%">
                                            <asp:Literal ID="Literal43" Text="<%$ Resources:WebResources, EnablePersonalVirtualMeetingRoom%>" runat="server"></asp:Literal> <%--ZD 100806--%>
                                        </td>
                                        <td id="tddrpEnablePersonaVMR" runat="server" valign="top" style="width: 20%" align="left">
                                            <asp:DropDownList id="drpEnablePersonaVMR" runat="server" cssclass="alt2SelectFormat">
                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="trEnableRoomExternalVRM" runat="server" >
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal ID="Literal44" Text="<%$ Resources:WebResources, EnableRoomVirtualMeetingRoom%>" runat="server"></asp:Literal> <%--ZD 100806--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drpEnableRoomVMR" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" width="27%">
                                            <asp:Literal ID="Literal45" Text="<%$ Resources:WebResources, EnableExternalVirtualMeetingRoom%>" runat="server"></asp:Literal> <%--ZD 100806--%>
                                        </td>
                                        <td valign="top" style="width: 20%" align="left">
                                            <asp:DropDownList id="drpEnableExternalVMR" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                              </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%-- ZD 700707 End--%>
                                    <%-- ZD 100704 Starts--%>
                                    <tr id="tr11" runat="server" >
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal ID="Literal20" Text="<%$ Resources:WebResources, ExpressFormConferenceType%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drpEnableExpressConfType" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem Value="0"  Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            <asp:ListItem Selected="True" Value="1"  Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                          <%-- ZD 100834 Starts--%>
                                           <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal ID="Literal21" Text="<%$ Resources:WebResources, DetailedExpressForm%>" runat="server"></asp:Literal>
                                        </td>
                                        <td  valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drpDetailedExpressForm" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem  Value="1"  Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            <asp:ListItem   Value="0"  Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                         <%-- ZD 100834 End--%>
                                        <td style="width: 1%;">
                                        </td><%-- ZD 103550--%>                                        
                                    </tr>
                                   <%-- ZD 100704 End--%>
								    <%--ZD 100522 Starts--%>
                                    <tr id="trVMRPIN" runat="server" >
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal ID="Literal52" Text="<%$ Resources:WebResources, mainadministrator_EnableVMRPW%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drpVMRPIN" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%-- ZD 101445 Starts--%>
                                           <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_RoomDenialComments%>" runat="server"></asp:Literal>
                                        </td>
                                        <td  valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="DrpRoomDenial" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem  Value="1"  Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            <asp:ListItem   Value="0"  Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                         <%-- ZD 101445 End--%>
                                    </tr>
                                    <%--ZD 100522 Ends--%>    
                                    
                                    <%--ZD 101562- START- Provide ability to hide templates in booking forms--%>
                                    <tr id="trtempbook" runat="server" >
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal ID="Literal57" Text="<%$ Resources:WebResources, mainadministrator_EnableTemplateBooking%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drptempbook" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <%--ZD 101562 END--%>  <%--ZD 103550 - Start--%>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="tdEnableBJNInt" runat="server" align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableBJNIntegration %>" runat="server"></asp:Literal>
                                        </td>
                                        <td  valign="top" style="width: 25%;" align="left" nowrap="nowrap" > 
                                            <asp:DropDownList id="DrpEnableBJNIntegration" runat="server" cssclass="alt2SelectFormat" onchange="JavaScript:fnBJNtypeDisplay()">
                                                <asp:ListItem  Value="1"  Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                <asp:ListItem   Value="0"  Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            </asp:DropDownList>
                                            <%--ZD 104116 - Start--%>
                                            &nbsp;
                                            <span class="blackblodtext" id="spnBJNDisplay" runat="server"> 
                                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Display%>" runat="server"></asp:Literal>
                                            </span>
                                            <asp:DropDownList id="DrpBJNDisplay" runat="server" cssclass="alt2SelectFormat" width="70px">
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            </asp:DropDownList>
                                            <%--ZD 104116 - End--%>                                             
                                        </td>
                                      </tr>
                                      <tr id="trBJNMeetingID" runat="server">
                                        <td></td>
                                        <td></td>
                                        <td> </td>                                         
                                        <td></td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_BJNMeetingID%>" runat="server"></asp:Literal>
                                        </td>
                                        <td  valign="top" style="width: 25%;" align="left" > 
                                            <asp:RadioButtonList ID="RdBJNMeetingID" runat="server">
                                                <asp:ListItem Text="<%$ Resources:WebResources, mainadministrator_PesonalMeetingID%>" Selected="True" Value= "1"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:WebResources, mainadministrator_OneTimeMeetingID%>" Value= "2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="ChkSelectable" runat="server" Text="<%$ Resources:WebResources, mainadministrator_Selectable%>" />  
                                                    </td>
                                                </tr>
                                            </table>
                                                                                          
                                        </td>
                                      </tr>                                        
                                      <%--ZD 103550 - End--%>
                                      <%-- ZD 102916 Start --%>
                                      <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_AssignPartToRoom%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="DrpAssignParttoRoom" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td> <%--ALLDEV-839 start--%>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                          <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_AllowRequestorToManage%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="DrpAllowReqtoEdit" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        </tr>
                                        <%-- ZD 102916 End --%><%--ALLDEV-839 end--%>
                                        <%--ALLDEV-833 START--%> 
                                       <tr>
                                        <td style="width: 1%;">
                                        </td>
                                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EWSPartyFreeBusy%>" runat="server"></asp:Literal>
                                        </td>
                                        <td  valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="DrpEWSPartyFreeBusy" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem  Value="1"  Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            <asp:ListItem  Value="0"  Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td>
                                           <td></td>
                                           <td></td>
                                      </tr>
                                      <%--ALLDEV-833 END--%> 
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_AUD" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_AudioAddOn%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trAUD" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-4px"> <%-- FB 2842 --%>
                                    <tr id="trUser" runat="server">
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="height: 33px; width: 25%" align="left" class="blackblodtext">
                                            <asp:label id="LblExc" runat="server" text="<%$ Resources:WebResources, mainadministrator_LblExc%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeconfcode" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference code will be visible in Audio/Video tab for audio bridge user."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="DrpConfcode" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="LblDom" runat="server" text="<%$ Resources:WebResources, mainadministrator_LblDom%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgleaderpin" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable leaderpin will be visible in Audio/Video tab for audio bridge user."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DrpLedpin" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="tr1" runat="server">
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="Label1" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label1%>"></asp:label>
                                            <%--<asp:ImageButton ID="ImgEadvavparams"  valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable advanced Audio/video params will be visible in Audio/video tab."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="Drpavprm" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label2" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label2%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeaudioparams" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable audio params will be visible in Audio/Video tab."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DrpAudprm" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="Label3" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label3%>"></asp:label>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableAudioBridges" runat="server" width="125px" cssclass="altSelectFormat" onchange="JavaScript:fncAudioBridge()">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="td4" runat="server" align="left" style="width: 27%;" class="blackblodtext"
                                            valign="top">
                                            <span style="left"><asp:Literal ID="Literal71" Text="<%$ Resources:WebResources, EnableAudfreebusy%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td id="td5" runat="server" valign="top" align="left" style="width: 20%">
                                            <asp:dropdownlist id="drpEnableAudbridgefreebusy" runat="server" width="125px" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--FB 2571 Start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableFECC%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="ImgeFECC" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable FECC checkbox while conference creation."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstenableFECC" runat="server" width="125px" cssclass="alt2SelectFormat"
                                                onclick="javascript:fnFeccOptions()">
                                                    <%--FB 2571--%>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, Hide%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Show%>"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, None%>"></asp:ListItem>
                                                    <%--FB 2571--%>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="DefaultFECC" runat="server" align="left" style="width: 27%;" class="blackblodtext"
                                            valign="top">
                                            <%--FB 2571--%>
                                            <span style="left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_DefaultFECC%>" runat="server"></asp:Literal></span>
                                            <%--<asp:ImageButton ID="ImgdFECC" valign="center" src="image/info.png" runat="server" ToolTip="Switch to make default Selection for FECC while conference creation."/>--%>
                                        </td>
                                        <td id="DefaultFECCoptions" runat="server" valign="top" align="left" style="width: 20%">
                                            <%--FB 2571--%>
                                            <asp:dropdownlist id="lstdefaultFECC" runat="server" width="125px" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--FB 2571 End--%>
                                    <%--FB 2839 Start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableProfile%>" runat="server"></asp:Literal>
                                        </td>
                                        <td id="tdEnableProfile" runat="server" valign="top" align="left" style="width: 20%">
                                            <asp:DropDownList id="lstEnableProfileSelection" runat="server" width="125px" cssclass="alt2SelectFormat">
                                                <asp:ListItem selected="True" value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                <asp:ListItem  value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <%--FB 2839 End--%>
                                        <%--ZD 101446 Starts--%>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="tdEnablePartyCode" runat="server" align="left" style="width: 27%;" class="blackblodtext"
                                            valign="top">
                                            <span style="left"><asp:Literal ID="Literal60" Text="<%$ Resources:WebResources, EnablePartyCode%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td id="tdPartyCode" runat="server" valign="top" align="left" style="width: 20%">
                                            <asp:dropdownlist id="drpPartyCode" runat="server" width="125px" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--ZD 101446 Ends--%>
                                    </tr>
                                    <%-- ZD 104221 start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, PoolorderSelcEnable%>" runat="server"></asp:Literal>
                                        </td>
                                        <td id="td3" runat="server" valign="top" align="left" style="width: 20%">
                                            <asp:DropDownList id="lstEnablePoolOrderSelection" runat="server" width="125px" cssclass="alt2SelectFormat">
                                                <asp:ListItem selected="True" value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                <asp:ListItem  value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <%--ZD 104854-Disney Starts--%>
                                        <td style="width: 1%;">
                                        </td>
                                       
                                         <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label4" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label4%>"></asp:label>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstRoomprm" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--ZD 104854-Disney Ends--%>
                                    </tr>
                                    <%--ZD 104221 End--%>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_CONFMAIL" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_MailOptions%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trCONFMAIL" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-1px" > <%-- FB 2842 --%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="Label9" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label9%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgsendconfirmemail" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable send confirmation emails to host only or all."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstSendConfirmationEmail" runat="server" cssclass="alt2SelectFormat" width="190px"> <%--ZD 102003--%>
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, All%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, DashBoard_Host%>"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, HostandAdminInCharge%>"></asp:ListItem> <%--ZD 102003--%>
                                                     <asp:ListItem Value="3" Text="<%$ Resources:WebResources, None%>"></asp:ListItem> <%--ALLDEV-841--%>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--FB 2817 Starts--%>
                                         <td style="width: 27%;" align="left" class="blackblodtext"> <%-- FB 2842 --%>
                                            <asp:label id="lblSigRoom" runat="server" text="<%$ Resources:WebResources, mainadministrator_lblSigRoom%>"></asp:label>
                                        </td>
                                        <td style="width: 20%;" align="left"> <%-- FB 2842 --%>
                                            <asp:dropdownlist id="lstSigRoomConf" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <%--FB 2817 Ends--%>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--FB 2631--%>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="25%"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableRoomAdm%>" runat="server"></asp:Literal></td>
                                        <td style="height: 33px; width: 25%" align="left" valign="top">
                                            <asp:dropdownlist id="lstEnableRoomAdminDetails" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td width="1%">
                                        </td> 
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label10" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label10%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgsendmcuemail" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send mcu alert mails to host  only or to all."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstMcuAlert" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, None%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, DashBoard_Host%>"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ManageCustomAttribute_chkMcuAdmin%>"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="<%$ Resources:WebResources, Both%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SendAttachment%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgsendatteparty" valign="center" src="image/info.png" runat="server" ToolTip="Switch to control mail attachment for external party."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="DrpDwnAttachmnts" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label8" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label8%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeconftimel" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference time in locations details in mails."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstEnableConfTZinLoc" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="Label5" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label5%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeendpoint" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable endpoints details in emails."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEPinMail" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--FB 2610 Starts--%>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="lblShwBrdge" runat="server" text="<%$ Resources:WebResources, mainadministrator_lblShwBrdge%>"></asp:label>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="drpBrdgeExt" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <%--FB 2610 Ends--%>
                                        <td style="width: 1%;">
                                        </td> <%-- FB 2842 --%>
                                    </tr>
                                    <%--FB 2632 Starts--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="lblCngSupport" runat="server" text="<%$ Resources:WebResources, mainadministrator_lblCngSupport%>"></asp:label> <%--FB 3023--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="drpCngSupport" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td>
                                        </td>
                                        <%--FB 2419--%>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label6" runat="server" text="<%$ Resources:WebResources, mainadministrator_Label6%>"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeaccdec" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable accept/decline link in confirmation mails."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstAcceptDecline" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_IncludeinEmai%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                                        <strong style="align: left"><asp:Literal ID="Literal23" Text="<%$ Resources:WebResources, ManageConference_OnSiteAVSuppo%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="chkConciergeMonitoring" type="checkbox" runat="server" />
                                                        <strong style="align: left"><asp:Literal ID="Literal24" Text="<%$ Resources:WebResources, ManageConference_CallMonitoring%>" runat="server"></asp:Literal></strong> <%--FB 3023--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td valign="top" align="left" style="width: 27%"> <%-- FB 2842 --%>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkMeetandGreet" type="checkbox" runat="server" />
                                                        <strong style="align: left"><asp:Literal ID="Literal25" Text="<%$ Resources:WebResources, ManageConference_MeetandGreet%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server" />
                                                        <strong style="align: left"><asp:Literal ID="Literal26" Text="<%$ Resources:WebResources, ManageConference_DedicatedVNOC%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
											<%-- FB 2842 STARTS --%>
                                        <td style="width: 20%;">
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
											<%-- FB 2842  END--%>
                                    </tr>
                                    <%--FB 2632 Ends--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SendInviteeiC%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imginviteeical" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send ical to parties when conference is scheduled."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="DrpListIcal" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td align="right" valign="top" style="width: 1%;">
                                            &nbsp;
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SendApprovali%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgapproical" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send ical to application when conference is scheduled from outlook."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:dropdownlist id="DrpAppIcal" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SendReminders%>" runat="server"></asp:Literal></span>  <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgsendremain" valign="center" src="image/info.png" runat="server" ToolTip="Remainder mail options for participants."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:checkbox id="WklyChk" runat="server" />
                                                        <strong style="align: left">1 <asp:Literal ID="Literal27" Text="<%$ Resources:WebResources, WorkingDays_Week%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                    <td>
                                                        <asp:checkbox id="DlyChk" runat="server" />
                                                        <strong style="align: left">1 <asp:Literal ID="Literal30" Text="<%$ Resources:WebResources, ExpressConference_Day%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:checkbox id="HourlyChk" runat="server" />
                                                        <strong style="align: left">1 <asp:Literal ID="Literal29" Text="<%$ Resources:WebResources, Hour%>" runat="server"></asp:Literal></strong>
                                                    </td>
                                                    <td>
                                                        <asp:checkbox id="MinChk" runat="server" />
                                                        <strong style="align: left">15 <asp:Literal ID="Literal28" Text="<%$ Resources:WebResources, ExpressConference_mins%>" runat="server"></asp:Literal></strong> <%--ZD 100528--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td colspan="3" style="width: 48%" rowspan="1">
                                            <%-- FB 2440 --%>
                                            <table id="tblForceMCUBuffer" runat="server" width="100%" style="display: none">
                                                <tr id="trForceMCUBuffer" style="display: none;" runat="server">
                                                    <td align="left" class="blackblodtext" valign="top" style="width: 21%">
                                                        <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_ForceMCUpres%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                    <td valign="top" align="left" style="width: 33%">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:dropdownlist id="DrpForceMCUBuffer" runat="server" cssclass="alt2SelectFormat">
                                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                            </asp:dropdownlist>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="25%">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_iCalRequestor%>" runat="server"></asp:Literal>
                                            <%--<asp:ImageButton ID="Imgicalreqemail" valign="center" src="image/info.png" runat="server" ToolTip="Ical requestor mail for CTP room in  ical invitations."/>--%>
                                        </td>
                                        <td style="height: 33px; width: 25%" align="left" valign="top">
                                            <asp:textbox id="txtiCalEmailId" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                            <br />
                                            <asp:regularexpressionvalidator id="regTestemail" runat="server" controltovalidate="txtiCalEmailId"
                                                errormessage="<%$ Resources:WebResources, InvalidEmailAddress%>" display="dynamic" validationexpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:regularexpressionvalidator>
                                            <asp:regularexpressionvalidator id="iCalReqValid2" controltovalidate="txtiCalEmailId"
                                                display="dynamic" runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters8%>"
                                                validationexpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:regularexpressionvalidator>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="25%">
                                            <asp:Literal Text="<%$ Resources:WebResources, EWSConfsAdmins%>" runat="server"></asp:Literal><br />(<asp:Literal Text="<%$ Resources:WebResources, Semicolonseparated%>" runat="server"></asp:Literal>)
                                        </td>
                                        <td style="height: 33px; width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtMultipleAssistant" Rows="2" TextMode="MultiLine" Width="250px" runat="server" CssClass="altText"
                                                onblur="javascript:checkDuplicateMail();"></asp:TextBox><%--ZD 103550 ALLBUGS-129---%>
                                            <a href="" onclick="this.childNodes[0].click();return false;"><img border='0' id="Img5" onclick="javascript:getYourOwnAddressList()" src="image/edit.gif" style="cursor: pointer;" alt="Address Book" runat="server" title="<%$ Resources:WebResources, AddressBook%>" /></a> 
                                            <a href="" onclick="this.childNodes[0].click();return false;"><img border='0' src='image/deleteall.gif' alt="Delete All"  style="cursor:pointer;" title="<%$ Resources:WebResources, RemoveAll%>" runat="server" id="ImageDel" width='18' height='18' onclick="JavaScript:ClearAllSelection()"/></a> 
                                            <asp:Label ID="lblDuplicate" runat="server" CssClass="lblError"></asp:Label> <%--ALLBUGS-129--%>
                                        </td>
                                        <td width="1%">
                                        </td>
                                    </tr>
                                    <%--ALLDEV-828 START--%>
                                     <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="25%">
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SendRoomResourceiCal%>" runat="server"></asp:Literal>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="DrpDwnSendRoomResourceiCal" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                     </tr>
                                    <%--ALLDEV-828 END--%>
                                </table>
                            </td>
                        </tr>
                        <%--FB 2595 Starts--%>
                        <tr id="trNetwork" runat="server"> <%--FB 2993--%>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_NETSWT" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_NetworkFeature%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trNETSWT" runat="server" style="display: none">
                            <td>
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-2px" > <%-- FB 2842 --%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableNetwork%>" runat="server"></asp:Literal></td>
                                        <td align="left" style="width: 25%">
                                            <asp:dropdownlist id="drpSecureSwitch" runat="server" cssclass="alt2SelectFormat"
                                                onchange="JavaScript:Securedisplay()">
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="tdsecureadminaddress" runat="server" align="left" class="blackblodtext" valign="top" style="width: 27%;"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_tdsecureadminaddress%>" runat="server"></asp:Literal></td>
                                        <td id="txtsecureadminaddress" valign="top" align="left" style="width: 20%;">
                                            <asp:textbox id="txtHardwareAdminEmail" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                            <br />
                                            <asp:regularexpressionvalidator id="RegularExpressionValidator7" runat="server" controltovalidate="txtHardwareAdminEmail"
                                                errormessage="<%$ Resources:WebResources, InvalidEmailAddress%>" display="dynamic" validationexpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:regularexpressionvalidator>
                                            <asp:regularexpressionvalidator id="RegularExpressionValidator8" controltovalidate="txtHardwareAdminEmail"
                                                display="dynamic" runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters8%>"
                                                validationexpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:regularexpressionvalidator>
                                        </td>
                                        <td style="width: 1%;">
                                        </td> <%-- FB 2842 --%>
                                    </tr>
                                    <tr id="trNwtSwtiching" runat="server">
                                    <td style="width: 1%;"></td> <%-- FB 2842--%>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_NetworkSwitchi%>" runat="server"></asp:Literal></td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="drpNwtSwtiching" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Selected="True" Value="1">None</asp:ListItem>
                                                    <asp:ListItem Value="2">NATO Secret</asp:ListItem>
                                                    <asp:ListItem Value="3">NATO Unclassified</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" valign="top" class="blackblodtext" style="width: 27%;"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_CallLaunch%>" runat="server"></asp:Literal></td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:dropdownlist id="drpNwtCallLaunch" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1">NATO Secret</asp:ListItem>
                                                    <asp:ListItem Value="2">NATO Unclassified</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="trNwtCallBuffer" runat="server">
                                    <td style="width: 1%;"></td><%-- FB 2842 --%>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SecureLaunchB%>" runat="server"></asp:Literal></td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:textbox id="txtSecureLaunch" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_secs%>" runat="server"></asp:Literal>
                                            <asp:regularexpressionvalidator id="RegularExpressionValidator9" validationgroup="Submit"
                                                controltovalidate="txtSecureLaunch" display="dynamic" runat="server" setfocusonerror="true"
                                                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                                            <asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" controltovalidate="txtSecureLaunch"
                                                display="dynamic" errormessage="<%$ Resources:WebResources, ValdiationBuffer%>"></asp:requiredfieldvalidator>
                                        </td>
                                         <td style="width: 1%;">
                                        </td> <%--FB 2993 start--%>
                                        <td align="left" valign="top" class="blackblodtext" style="width: 27%;" ><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_ResponseTimeou%>" runat="server"></asp:Literal></td>
                                        <td valign="top" align="left" style="width: 20%;" >
                                        <asp:TextBox id="txtResponseTimeout" runat="server" cssclass="altText" width="50px"></asp:TextBox>
                                        <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Mins%>" runat="server"></asp:Literal>
                                        <asp:regularexpressionvalidator id="RegularExpressionValidatortimeout" validationgroup="Submit"
                                                controltovalidate="txtResponseTimeout" display="dynamic" runat="server" setfocusonerror="true"
                                                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                                        </td><%--FB 2993 End--%>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--FB 2595 Ends--%>
                        <%--ZD 100221 Starts--%>
                        <tr id="trWebConf" runat="server">
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_WEBCONF" alternatetext="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" />
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext"><asp:Literal ID="Literal48" Text="<%$ Resources:WebResources, WebConferencing%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trWebCre" runat="server" style="display: none">
                            <td>
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-2px" >
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%" >
                                            <asp:Literal ID="Literal51" Text="<%$ Resources:WebResources, WebExAPIURL%>" runat="server"></asp:Literal>  &nbsp;&nbsp;<asp:ImageButton id="ImgWebExAPI" src="image/info.png" runat="server" alt="Info" style="cursor:default"
                                    tooltip="<%$ Resources:WebResources, WebexTooltip%>" /> <%--ZD 100935--%><%-- ZD 102590 --%>
                                        </td>
                                        <td align="left" style="width: 25%">
                                           <asp:textbox id="txtWebURL" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                           <asp:regularexpressionvalidator id="regWebURL" controltovalidate="txtWebURL"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top"
                                            style="width: 27%;">
                                            <asp:Literal ID="Literal50" Text="<%$ Resources:WebResources, SiteID%>" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:ImageButton id="ImgWebExSiteID" src="image/info.png" runat="server" alt="Info" Visible="false" style="cursor:default"
                                    tooltip="<%$ Resources:WebResources, WebexTooltip%>" /><%--ZD 100935--%><%--ZD 102371--%><%-- ZD 102590 --%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:textbox id="txtSiteID" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                            <asp:regularexpressionvalidator id="regSiteID" controltovalidate="txtSiteID"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                            <br />
                                        </td>
                                        <td style="width: 1%;">
                                        </td> 
                                    </tr>
                                    <tr>
                                    <td style="width: 1%;"></td> 
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                           <asp:Literal ID="Literal49" Text="<%$ Resources:WebResources, PartnerID %>" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:ImageButton id="ImgWebExPartnrID" src="image/info.png" runat="server" alt="Info" Visible="false" style="cursor:default"
                                    tooltip="<%$ Resources:WebResources, WebexTooltip%>" /><%--ZD 100935--%><%--ZD 102371--%><%-- ZD 102590 --%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:textbox id="txtPartnrID" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                          <asp:regularexpressionvalidator id="RegPartnerID" controltovalidate="txtPartnrID"
                                            display="dynamic" runat="server" setfocusonerror="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            ValidationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--ZD 100221 Ends--%>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_EPT" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EndpointSettin%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trEPT" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext"  valign="top"> <%-- FB 2842--%>
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableAssigned%>" runat="server"></asp:Literal>   <%--FB 2926--%>
                                <%--<asp:ImageButton ID="ImgeassMcu" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable must select mcu from assigned to mcu dropdownlist to create a new endpoint."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:dropdownlist id="DrpDwnListAssignedMCU" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" valign="top" class="blackblodtext" style="width: 27%;">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableISDNDia%>" runat="server"></asp:Literal>
                                <%--<asp:ImageButton ID="Imgedialout"  valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable dial out option in response screen."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%;">
                                <asp:dropdownlist id="DialoutEnabled" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <%--ZD 101527 Start--%>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_RoomSync" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_RoomSync%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trRoomSync" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext"  valign="top"> <%-- FB 2842--%>
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_RPRMRoomSync%>" runat="server"></asp:Literal>
                                
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:dropdownlist id="DrpRPRMRoomSync" runat="server" cssclass="alt2SelectFormat" onchange="JavaScript:FnRPRMRoomSync()">
                                        <asp:ListItem Selected="True" Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                            <td id="tdRoomSync" align="left" valign="top" class="blackblodtext" style="width: 27%;">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_RoomSync%>" runat="server"></asp:Literal>                                
                            </td>
                            <td id="tdRoomSyncAuto" valign="top" align="left" style="width: 20%;">
                                <asp:dropdownlist id="DrpRoomSyncAuto" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, Manual%>"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Automatic%>"></asp:ListItem>
                                </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                        </tr>

                        <tr id="trPoll">
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext"  valign="top"> <%-- FB 2842--%>
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_FrqAutoPoll%>" runat="server"></asp:Literal>                                
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:textbox id="TxtPollHrs" runat="server" cssclass="altText" MaxLength="4" width="50px"></asp:textbox>
                               <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Td2%>" runat="server"></asp:Literal>
                                <asp:regularexpressionvalidator id="regPollHrs" validationgroup="Submit"
                                    controltovalidate="TxtPollHrs" display="dynamic" runat="server"
                                    errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                            </td>

                            <td style="width: 1%;">
                            </td>
                            <td align="left" valign="top" class="blackblodtext" style="width: 27%;" id="trSyncPoll" runat="server">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_ManualUpdate%>" runat="server"></asp:Literal>                                
                            </td>
                            <td valign="top" align="left" style="width: 20%;">
                                <input type="button" style="vertical-align: 10px; width:100px;" id="btnPoll"  runat="server" onserverclick="ManualSync"
                                    name="RPRMPoll" value="<%$ Resources:WebResources, mainadministrator_Poll%>" class="altShortBlueButtonFormat"  />
                            </td>
                            <td style="width: 1%;">
                            </td>
                        </tr>

                        <tr id="trImport">
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext"  valign="top">
                                <asp:Literal Text="<%$ Resources:WebResources,mainadministrator_PendingEndpointImport %>" runat="server"></asp:Literal>
                                
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <input type="button" style="vertical-align: 10px; width:100px;" id="btnPendingRoomImport"  runat="server" onserverclick="RoomImportPage"
                                    name="RoomImport" value="<%$ Resources:WebResources, Manage%>" class="altShortBlueButtonFormat"  />
                            </td>

                            <td style="width: 1%;">
                            </td>
                            <td align="left" valign="top" class="blackblodtext" style="width: 27%;">
                                <asp:Literal Text="<%$ Resources:WebResources,mainadministrator_PendingRoomImport %>" runat="server"></asp:Literal>                                
                            </td>
                            <td valign="top" align="left" style="width: 20%;">
                                <input type="button" style="vertical-align: 10px; width:100px;" id="btnPendingEndpointImport" onserverclick="EndpointImportPage"  runat="server"
                                    name="EndpointImport" value="<%$ Resources:WebResources, Manage%>" class="altShortBlueButtonFormat"  />
                            </td>
                            <td style="width: 1%;">
                            </td>
                         </tr>

                    </table>
                </td>
            </tr>


            <%--ZD 101527 End--%>

            <tr id="trOnfly" runat="server">
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_FLY" alternatetext="Expand/Collapse"  cssclass ="0"  runat="server" imageurl="image/loc/nolines_plus.gif"  name="Fly"
                                                height="25" width="25" vspace="0" hspace="0"  /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_OntheFlyRoomC%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFLY" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_TopTier%>" runat="server"></asp:Literal> <span class="reqfldText">*</span>
                                <%--<asp:ImageButton ID="Imgtoptier" valign="center" src="image/info.png" runat="server" ToolTip="Default top tier for on the fly rooms."/>--%>
                            </td>
                            <td style="width: 25%" align="left" valign="top">
                                <asp:dropdownlist id="lstTopTier" width="120px" datatextfield="Name" datavaluefield="ID"
                                    runat="server" cssclass="altSelectFormat" onselectedindexchanged="UpdateMiddleTiers"
                                    autopostback="true" onchange="javascript:DataLoading(1)">
                                    </asp:dropdownlist>
                                <asp:requiredfieldvalidator id="reqTopTier" runat="server" controltovalidate="lstTopTier"
                                    display="dynamic" cssclass="lblError" setfocusonerror="true" text="<%$ Resources:WebResources, Required%>"
                                    validationgroup="Submit"></asp:requiredfieldvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_MiddleTier%>" runat="server"></asp:Literal><span class="reqfldText">*</span>
                                <%--<asp:ImageButton ID="Imgmiddletier" valign="center" src="image/info.png" runat="server" ToolTip="Default middle tier for on the fly rooms."/>--%>
                            </td>
                            <td style="width: 20%" align="left" valign="top">
                                <asp:dropdownlist id="lstMiddleTier" width="120px" datatextfield="Name" datavaluefield="ID"
                                    runat="server" cssclass="altSelectFormat">
                                    </asp:dropdownlist>
                                <asp:requiredfieldvalidator id="reqMiddleTier" runat="server" controltovalidate="lstMiddleTier"
                                    display="dynamic" cssclass="lblError" setfocusonerror="true" text="<%$ Resources:WebResources, Required%>"
                                    validationgroup="Submit"></asp:requiredfieldvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <%--ZD 101120 Starts--%>
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext"  valign="top">
                                <asp:Literal ID="Literal31" Text="<%$ Resources:WebResources, ApprovalTime%>" runat="server"></asp:Literal>
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:dropdownlist id="drpGLAppTime" runat="server" cssclass="alt2SelectFormat"> <%--ZD 101714--%>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, 24Hrs%>"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, 48Hrs%>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$ Resources:WebResources, 72Hrs%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                             <td align="left" style="width: 25%;" class="blackblodtext"  valign="top">
                               <asp:Literal ID="Literal32" Text="<%$ Resources:WebResources, EnableWarningMessage%>" runat="server"></asp:Literal>
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:dropdownlist id="drpWarningMsg" runat="server" cssclass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                </asp:dropdownlist>
                            </td>
                        </tr>
                        <%--ZD 101120 Ends--%>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_AUTO" alternatetext="Expand/Collapse"  cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_DefaultVideoD%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAUTO" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="5"> <%-- FB 2842 --%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%"> 
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_PolycomMGC%>" runat="server"></asp:Literal> &nbsp;&nbsp;<asp:image id="Image6" src="image/info.png" runat="server" alt="Info" style="cursor:default"
                                    tooltip="<%$ Resources:WebResources, VideoLayoutToolTip%>" /><%--FB 2713--%> <%--ZD 100419--%><%-- ZD 102590 --%>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:image id="imgLayoutMapping1" runat="server" alt="Layout" /><%--ZD 100419--%>
								<%--ZD 100420--%>
                                <%--<input style="vertical-align: 10px" id="butLayoutMapping1" type="button" runat="server"
                                    name="ConfLayoutSubmit" value="Change" class="altMedium0BlueButtonFormat" onclick="javascript: changeLayout('Poly2MGC',butLayoutMapping1,1);" />--%>
                                    <button style="vertical-align: 10px; width:100px;" id="butLayoutMapping1" runat="server"
                                    name="ConfLayoutSubmit" class="altMedium0BlueButtonFormat" onclick="javascript: changeLayout('Poly2MGC',butLayoutMapping1,1);return false;"><asp:Literal ID="Literal33" Text="<%$ Resources:WebResources, Change%>" runat="server"></asp:Literal></button><%--ZD 100420--%><%--ZD 100420 ZD 100393--%>
								<%--ZD 100420--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_PolycomRMX%>" runat="server"></asp:Literal> &nbsp;&nbsp;<asp:image id="Image1" src="image/info.png" runat="server" alt="Info" Visible="false" style="cursor:default"
                                    tooltip="<%$ Resources:WebResources, VideoLayoutToolTip%>" /><%--FB 2713--%><%--ZD 100419--%><%--ZD 102372--%><%-- ZD 102590 --%>
                            </td>
                            <td align="left" style="width: 20%">
                                <asp:image id="imgLayoutMapping2" runat="server" alt="Layout" /> <%--ZD 100419--%>
								<%--ZD 100420--%>
                                <button style="vertical-align: 10px; width:100px;" id="butLayoutMapping2" runat="server"
                                    name="ConfLayoutSubmit" class="altMedium0BlueButtonFormat" onclick="javascript: changeLayout('Poly2RMX',butLayoutMapping2,2);return false;" ><asp:Literal ID="Literal34" Text="<%$ Resources:WebResources, Change%>" runat="server"></asp:Literal></button><%--ZD 100420--%><%--ZD 100420 ZD 100393--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr style="display:none">
                            <td>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_CTMStoCisco%>" runat="server"></asp:Literal></td>
                            <td>
                                <asp:image id="imgLayoutMapping3" runat="server" alternatetext="Video Display Layout" /> <%--ZD 100419--%>
                                <input style="vertical-align: 10px; width:100px;" id="butLayoutMapping3" type="button" runat="server"
                                    name="ConfLayoutSubmit" value="Change" class="altShortBlueButtonFormat" onclick="javascript: changeLayout('CTMS2Cisco',butLayoutMapping3,1);" /> <%--ZD 100393--%>
                            </td>
                            <td>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_CTMStoPolycom%>" runat="server"></asp:Literal></td>
                            <td>
                                <asp:image id="imgLayoutMapping4" runat="server" alternatetext="Video Display Layout"  /> <%--ZD 100419--%>
                                <input style="vertical-align: 10px; width:100px;" id="butLayoutMapping4" type="button" runat="server"
                                    name="ConfLayoutSubmit" value="Change" class="altShortBlueButtonFormat" onclick="javascript: changeLayout('CTMS2Poly',butLayoutMapping4,2);" /> <%--ZD 100393--%>
                            </td>
                            <td>
                            </td>
                        </tr>
                       <%-- ZD 101869 start--%>
                        <tr>
                           <td style="width: 1%">
                           </td>
                           <td align="left" class="blackblodtext" style="width: 25%"> 
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Codian%>" runat="server"></asp:Literal> &nbsp;&nbsp;
                                <asp:image id="Image2" src="image/info.png" runat="server" alt="Info" Visible="false" tooltip="<%$ Resources:WebResources, VideoLayoutToolTip%>" style="cursor:default" /><%--ZD 102372--%><%-- ZD 102590 --%>
                           </td>
                           <td align="left" style="width: 25%" nowrap="nowrap" >
                            <asp:Label runat="server" id="lblCodianLO" class="blackblodtext" ></asp:Label>
                             <asp:image id="imgLayoutMapping5" runat="server" alt="Layout" />
                             <asp:image id="imgLayoutMapping6" runat="server" alt="Layout" style="display:none" />
                             <asp:image id="imgLayoutMapping7" runat="server" alt="Layout" style="display:none" /> 
                             <asp:image id="imgLayoutMapping8" runat="server" alt="Layout" style="display:none" />
                                <button style="vertical-align: 10px; width:100px;" id="butLayoutMapping5" runat="server" 
                                name="ConfLayoutSubmit" class="altMedium0BlueButtonFormat" onclick="javascript: changeLayout('hdnCodian',butLayoutMapping5,1);return false;"><asp:Literal ID="Literal61" Text="<%$ Resources:WebResources, Change%>" runat="server"></asp:Literal></button>
                           </td>
                           <%--ZD 101931 start--%>
                          <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%"> 
                             <asp:Literal ID="Literal62" Text="<%$ Resources:WebResources, mainadministrator_ShowvideoLayout%>" runat="server"></asp:Literal>
                             </td>
                             <td align="left" style="width: 20%">
                                 <asp:dropdownlist id="DrpShowvideoLayout" runat="server" cssclass="alt2SelectFormat" onclick="javascript:fnShowMCULayout()"  Width ="150px">
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, Both%>"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, NormalInterface%>"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ExpressInterface%>"></asp:ListItem>
                                 </asp:dropdownlist>
                             </td>
                             <td style="width: 1%">
                            </td>
                             <%--ZD 101931 End--%>
                        </tr>
                        <%-- ZD 101869 End--%>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_SYS" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_GeneralOptions%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSYS" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" rowspan="1" valign="top" style="width: 25%"> <%-- FB 2842--%>
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableMultiLin%>" runat="server"></asp:Literal>
                                <%--<asp:ImageButton ID="Imgmultilingual" valign="center" src="image/info.png" runat="server" ToolTip="Switch to support Multilingual all over Application (Themes/UI Text)."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 26%"> <%-- FB 2842--%>
                                <asp:dropdownlist id="DrpDwnListMultiLingual" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 27%"> <%-- FB 2842--%>
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_DefaultCalenda%>" runat="server"></asp:Literal>
                                <%--<asp:ImageButton ID="Imgdecaloffice" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference hours to office hours."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:dropdownlist id="lstDefaultOfficeHours" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <%--FB 2598 Starts (Switch for CallMonitor,EM7,CDR)--%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableCallMon%>" runat="server"></asp:Literal></td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="DrpDwnEnableCallmonitor" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableEM7%>" runat="server"></asp:Literal></td>
                            <td valign="top" style="width: 20%;" align="left">
                                <asp:dropdownlist id="DrpDwnEnableEM7" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;"></td> <%-- FB 2842--%>
                        </tr>
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableCDR%>" runat="server"></asp:Literal></td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="DrpDwnEnableCDR" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <%--FB 2598 Ends (Switch for CallMonitor,EM7,CDR)--%>
                            <%--FB 2588 Starts (ZuLu Time Zone)--%>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_ZuluSystem%>" runat="server"></asp:Literal></td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="DropDownZuLu" runat="server" cssclass="alt2SelectFormat" onclick="javascript:fntimezonesOptions()">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                             <td style="width: 1%;"></td> <%-- FB 2842--%>
                        </tr>
                        <tr> <%--ZD 100151--%> <%--ZD 101549 Start--%>                           
                            
                            <%--ZD 100963 START--%>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> <%-- FB 2842--%>
                                <asp:Literal ID="Literal35" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal3%>" runat="server"></asp:Literal>
                            </td>
                            <td valign="top" style="width: 25%;" align="left" nowrap="nowrap">
                                <asp:dropdownlist id="drpRoomCalView" runat="server" cssclass="alt2SelectFormat">
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Private%>"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, Partial%>"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="<%$ Resources:WebResources, Public%>"></asp:ListItem>
                                </asp:dropdownlist> <%--ZD 102356--%>
                                <span class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, DefaultDisplay%>" runat="server"></asp:Literal></span>
                                <asp:dropdownlist id="drpCalDefaultDisplay" runat="server" cssclass="alt2SelectFormat" Width="85px">
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ConferenceSetup_Personal%>"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ConferenceSetup_Room%>" Selected="True"></asp:ListItem>
                                </asp:dropdownlist>
                             </td>
                             <%--ZD 100963 END--%>
                            <td style="width: 1%;">
                            </td>
                            <td id="tdlblCustOpt" runat="server" align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top"><asp:Literal ID="Literal56" Text="<%$ Resources:WebResources, mainadministrator_ShowCustomAtt%>" runat="server"></asp:Literal></td>
                            <td id="tdDrpCustOpt" runat="server" valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="DrpDwnEnableCA" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <%--ZD 101549 End--%>
                        </tr>
                        <%--FB 2588 Ends (ZuLu Time Zone)--%>
                        <tr>
                            <td style="width: 1%" rowspan="4">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Open24Hours%>" runat="server"></asp:Literal> <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgopen24hours" valign="center" src="image/info.png" runat="server" ToolTip="Office Timings/Conference Timings."/>--%>
                            </td>
                            <td align="left" valign="top" style="width: 25%">
                                <asp:checkbox id="Open24" runat="server" onclick="javascript:open24()" />
                            </td>
                            <td rowspan="4" style="width: 1%">
                            </td>
                            <td align="left" valign="top" style="padding-top: 6px; width: 27%;" class="blackblodtext"
                                rowspan="3">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_DaysClosed%>" runat="server"></asp:Literal>    <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgdaysclosed" valign="center" src="image/info.png" runat="server" ToolTip="Conference can not be scheduled in closed days."/>--%>
                            </td>
                            <td rowspan="4" align="left" style="width: 20%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" nowrap="nowrap">
                                            <%-- FB 2613--%>
                                            <asp:checkboxlist id="DayClosed" runat="server" cellpadding="0" cellspacing="6" repeatcolumns="2"
                                                repeatdirection="Horizontal" repeatlayout="Table" textalign="right" CssClass="blackblodtext">
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Sunday%>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Monday%>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Tuesday%>" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Wednesday%>" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Thursday%>" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Friday%>" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Saturday%>" Value="7"></asp:ListItem>
                                                </asp:checkboxlist>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td rowspan="4" style="width: 1%">
                            </td>
                        </tr>
                        <tr>
                            <td id="Open24DIV1" class="blackblodtext" align="left" style="width: 190px" valign="baseline">
                                <br />
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Open24DIV1%>" runat="server"></asp:Literal>
                                <%--<asp:ImageButton ID="Imgstarttime" valign="center" src="image/info.png" runat="server" ToolTip="Office Timing :Start Time"/>--%>
                            </td>
                            <td id="Open24DIV2" align="left" style="width: 10px">
                                <br />
                                <mbcbb:ComboBox ID="systemStartTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                    CausesValidation="True" Style="width: auto" AutoPostBack="false">
                                </mbcbb:ComboBox> <%--ZD 100284--%>
                                <asp:RegularExpressionValidator ID="regSysStartTime" runat="server" ControlToValidate="systemStartTime:Text"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td id="Open24DIV3" class="blackblodtext" style="width: 190px" align="left" valign="baseline">
                                <br />
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Open24DIV3%>" runat="server"></asp:Literal>
                                <%--<asp:ImageButton ID="Imgendtime" valign="center" src="image/info.png" runat="server" ToolTip="Office Timing:End Time"/>--%>
                            </td>
                            <td id="Open24DIV4" align="left" style="width: 10px">
                                <br />
                                <mbcbb:ComboBox ID="systemEndTime" runat="server" CssClass="altSelectFormat" Rows="10" AutoPostBack="false"
                                    CausesValidation="True" Style="width: auto"><%-- ZD 100420--%>
                                </mbcbb:ComboBox><%--ZD 100284--%>
                                <asp:RegularExpressionValidator ID="regSysEndTime" runat="server" ControlToValidate="systemEndTime:Text"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--ZD 104862 start--%>
                         <tr>
                           <td align="left" class="blackblodtext">
                            <asp:Literal Text="<%$Resources:WebResources, EnableBlockUsersonDataimport%>" runat="server"></asp:Literal>
                           </td>
                           <td align="left">
                              <asp:dropdownlist id="DrpEnBlkusrDataImport" runat="server" cssclass="alt2SelectFormat">
                                 <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                 <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                              </asp:dropdownlist>
                           </td>
                         </tr>
                        <%--ZD 104862 End --%>
                        
                    </table>
                </td>
            </tr>
            <%--ZD 101228 Starts--%>
            <tr id="trWOOptions" runat="server">
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_WO"  runat="server"  alternatetext="Expand/Collapse" cssclass ="0" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal ID="ltlWOOptions" Text="<%$ Resources:WebResources, mainadministrator_WorkOrderOptions%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trWO" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr id="trAVWOAlert" runat="server">
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext"  valign="top"  nowrap="nowrap"> 
                                <asp:Literal ID="ltlAVWO" Text="<%$ Resources:WebResources, mainadministrator_AVWOAlert%>" runat="server"></asp:Literal>   <%--FB 2926--%>
                            </td>
                            <td valign="top" align="left" style="width: 26%;" nowrap="nowrap">
                              <asp:textbox id="txtAVWOAlert" runat="server" cssclass="altText" MaxLength="4" width="50px"></asp:textbox>
                               <asp:Literal ID="ltlAVWOHrs" Text="<%$ Resources:WebResources, mainadministrator_Td2%>" runat="server"></asp:Literal>
                                <asp:regularexpressionvalidator id="regValAVWO" validationgroup="Submit"
                                    controltovalidate="txtAVWOAlert" display="dynamic" runat="server"
                                    errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                            </td>
                            <td style="width: 1%;"></td>
                             <td style="width: 27%;"></td>
                             <td style="width: 20%;"></td>
                             <td style="width: 25%;"></td>
                        </tr>
                        <tr id="trCatWOALert" runat="server">
                            <td style="width: 1%;">
                            </td>
                            <td align="left" valign="top" class="blackblodtext"  nowrap="nowrap" >
                                <asp:Literal ID="ltlCatWO" Text="<%$ Resources:WebResources, mainadministrator_CatWOAlert%>" runat="server"></asp:Literal>
                            </td>
                            <td valign="top" align="left"  nowrap="nowrap">
                                <asp:textbox id="txtCatWOAlert" runat="server" cssclass="altText" MaxLength="4" width="50px"></asp:textbox>
                                 <asp:Literal ID="ltlCAtHrs" Text="<%$ Resources:WebResources, mainadministrator_Td2%>" runat="server"></asp:Literal>
                                <asp:regularexpressionvalidator id="RegValCatWO" validationgroup="Submit"
                                    controltovalidate="txtCatWOAlert" display="dynamic" runat="server"
                                    errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                            </td>
                            <td style="width: 1%;"></td>
                             <td style="width: 25%;"></td>
                             <td style="width: 25%;"></td>
                             <td style="width: 25%;"></td>
                        </tr>
                        <tr id="trFacWOALert" runat="server">
                            <td style="width: 1%;">
                            </td>
                            <td align="left"  class="blackblodtext"  valign="top"  nowrap="nowrap"> 
                                <asp:Literal ID="ltlFacWO" Text="<%$ Resources:WebResources, mainadministrator_FacWOAlert%>" runat="server"></asp:Literal>   <%--FB 2926--%>
                            </td>
                            <td valign="top" align="left" nowrap="nowrap">
                              <asp:textbox id="txtFacWOAlert" runat="server" cssclass="altText" MaxLength="4" width="50px"></asp:textbox>
                               <asp:Literal ID="ltlFacHrs" Text="<%$ Resources:WebResources, mainadministrator_Td2%>" runat="server"></asp:Literal>
                                <asp:regularexpressionvalidator id="RegFACWO" validationgroup="Submit"
                                    controltovalidate="txtFacWOAlert" display="dynamic" runat="server"
                                    errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
                            </td>
                            <td style="width: 1%;"></td>
                            <td style="width: 25%;"></td>
                             <td style="width: 25%;"></td>
                             <td style="width: 25%;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%--ZD 101228 Ends--%>
            <%--FB 2841--%><%--ZD 100419--%>
           <%-- <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_CONFSECDESK" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> 
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SecurityDeskO%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <%-- FB 2842--%>
            <%--FB 2926--%>
            <%--<tr id="trCONFSECDESK" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5" style="margin-left:-2px"> 
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%;margin-left: -3px;"> 
                                <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableSecurity%>" runat="server"></asp:Literal></span>      
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:dropdownlist id="drpenablesecuritybadge" runat="server" width="125px" cssclass="alt2SelectFormat"
                                    onchange="JavaScript:modedisplay()">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td id="tdSecurityType" class="blackblodtext" align="left" runat="server" style="display: none; margin-left:3px;
                                width: 27%;" valign="top"> 
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_tdSecurityType%>" runat="server"></asp:Literal>         
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:dropdownlist id="drpsecuritybadgetype" runat="server" datatextfield="Name" datavaluefield="ID"
                                    width="125px" cssclass="alt2SelectFormat" onchange="JavaScript:emaildisplay()">
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr id="tdsecdeskemailid" runat="server">
                            <td style="height: 33px; width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SecuritydeskEm%>" runat="server"></asp:Literal>  
                            </td>
                            <td align="left" style="width: 25%;">
                                <asp:textbox id="txtsecdeskemailid" runat="server" cssclass="altText" width="187px"></asp:textbox>
                                <asp:regularexpressionvalidator id="regsecdeskemailid" runat="server" controltovalidate="txtsecdeskemailid"
                                    errormessage="<%$ Resources:WebResources, InvalidEmailAddress%>" display="dynamic" validationexpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:regularexpressionvalidator>
                                <asp:regularexpressionvalidator id="RegularExpressionValidator4" controltovalidate="txtiCalEmailId"
                                    display="dynamic" runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters8%>"
                                    validationexpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:regularexpressionvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td style="width: 27%">
                            </td>
                            <td style="width: 20%">
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
             <%--FB 2724 End--%>
             <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_ICP"  runat="server" alternatetext="Expand/Collapse" cssclass ="0" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_iControlParams%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trICP" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                <span style="align: left"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_RFIDTagValue%>" runat="server"></asp:Literal></span>
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:DropDownList id="lstRFIDValue" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem selected="True" value="1" Text="<%$ Resources:WebResources, Allocation_Email%>"></asp:ListItem>
                                    <asp:ListItem value="2" Text="<%$ Resources:WebResources, Login%>"></asp:ListItem>
                                    <asp:ListItem value="3" Text="<%$ Resources:WebResources, UISettings_Custom%>"></asp:ListItem>
                                </asp:DropDownList>
                             </td>
                              <%--<td style="width: 1%;">
                             </td> --%>
                             <td align="left" style="width: 27%;" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_iControlTimeou%>" runat="server"></asp:Literal></td>
                             <td valign="top" style="width: 20%;" align="left">
                                <asp:TextBox id="txtiControlTimeout" runat="server" CssClass="altText" Width="50px" MaxLength="4"> </asp:TextBox>
                                <asp:Literal ID="Literal65" Text="<%$ Resources:WebResources, mainadministrator_secs%>" runat="server"></asp:Literal>  <%--ZD 102699 --%> 
                             </td>
                             <td style="width: 1%;">
                             </td>
                        </tr>
                        <%--  ZD 103398 Start--%>
                        <tr> 
                        <td style="width: 1%;"></td>
                        <td align ="left" style="width: 27%;" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, VideoRefreshTimer%>" runat="server"></asp:Literal></td> 
                        <td valign="top" style="width: 20%;" align="left">
                        <asp:TextBox id="txtVideoRefreshTimer" runat="server" CssClass="altText" Width="50px" MaxLength="4"></asp:TextBox>
                        <asp:Literal ID="Literal70" Text="<%$ Resources:WebResources, mainadministrator_mins%>" runat="server"></asp:Literal>                        
                        <asp:regularexpressionvalidator id="RegNumericvalidator" validationgroup="Submit" controltovalidate="txtVideoRefreshTimer"
                            display="dynamic" runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, NumericValuesOnly%>"
                            validationexpression="\d+"></asp:regularexpressionvalidator>                                               
                        </td> 
                        <%-- ALLDEV-503 Start--%>
                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top"><asp:Literal ID="Litrl1" Text="<%$ Resources:WebResources, EnableLogin %>" runat="server"></asp:Literal></td>
                        </td>
                        <td valign="top" align="left" style="width: 25%;">
                            <asp:DropDownList id="DrpEnableiControlLogin" runat="server" CssClass="alt2SelectFormat">
                                 <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        </tr>
                        <tr> 
                            <td style="width: 1%;">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                <span style="align: left"><asp:Literal ID="Literal73" Text="<%$ Resources:WebResources, EnableAvailRoom %>" runat="server"></asp:Literal></span>
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:DropDownList id="DrpEnableFindAvailRm" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem value="0" selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    <asp:ListItem value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                </asp:DropDownList>
                             </td>
                        </tr>  
                        <%-- ALLDEV-503 End--%>
                        <%--ZD 103398 End--%>
                        <%--ZD 101019 START--%> 
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" class="subtitleblueblodtext" valign="top" style="width: 25%">
                            <strong style="align: left"><asp:Literal ID="Literal36" Text="<%$ Resources:WebResources, VideoStreaming%>" runat="server"></asp:Literal></strong>
                            </td>
                         </tr>
                         
                         <tr>
                            <td style="width: 1%;">
                            </td>
                            <td valign="top" class="blackblodtext" align="left" style="width: 50%" colspan="2" >
                                <table style="width:100%" border="0">
                                    <tr>
                                        <td colspan="4" style="width:50%"><asp:Literal ID="Literal37" Text="<%$ Resources:WebResources, ScreenPosition%>" runat="server"></asp:Literal></td>
                                        <td style="width:50%"><asp:Literal ID="Literal38" Text="<%$ Resources:WebResources, VideoSourceURL%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkPosA1" runat="server" onclick="fnHandleTick(this.checked,'A2,A3,A4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosB1" runat="server" onclick="fnHandleTick(this.checked,'B2,B3,B4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosC1" runat="server" onclick="fnHandleTick(this.checked,'C2,C3,C4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosD1" runat="server" onclick="fnHandleTick(this.checked,'D2,D3,D4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtVideoSourceURL1" runat="server" CssClass="altText"></asp:TextBox>
                                             <asp:regularexpressionvalidator id="regURL1" controltovalidate="txtVideoSourceURL1"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkPosA2" runat="server" onclick="fnHandleTick(this.checked,'A1,A3,A4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosB2" runat="server" onclick="fnHandleTick(this.checked,'B1,B3,B4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosC2" runat="server" onclick="fnHandleTick(this.checked,'C1,C3,C4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosD2" runat="server" onclick="fnHandleTick(this.checked,'D1,D3,D4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtVideoSourceURL2" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:regularexpressionvalidator id="regURL2" controltovalidate="txtVideoSourceURL2"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkPosA3" runat="server" onclick="fnHandleTick(this.checked,'A1,A2,A4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosB3" runat="server" onclick="fnHandleTick(this.checked,'B1,B2,B4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosC3" runat="server" onclick="fnHandleTick(this.checked,'C1,C2,C4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosD3" runat="server" onclick="fnHandleTick(this.checked,'D1,D2,D4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtVideoSourceURL3" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:regularexpressionvalidator id="regURL3" controltovalidate="txtVideoSourceURL3"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkPosA4" runat="server" onclick="fnHandleTick(this.checked,'A1,A2,A3');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosB4" runat="server" onclick="fnHandleTick(this.checked,'B1,B2,B3');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosC4" runat="server" onclick="fnHandleTick(this.checked,'C1,C2,C3');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosD4" runat="server" onclick="fnHandleTick(this.checked,'D1,D2,D3');" type="checkbox" />
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtVideoSourceURL4" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:regularexpressionvalidator id="regURL4" controltovalidate="txtVideoSourceURL4"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                </table>
                             </td>
                         </tr>
                         <%--ZD 101019 END--%>        
                    
                    
                    </table>
                </td>
            </tr>
            <%--FB 2724 End--%>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_PIM"  runat="server"  alternatetext="Expand/Collapse" cssclass ="0" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_PIMFeatures%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trPIM" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server" updatemode="always"> <%--ZD 100068--%>
                     <ContentTemplate>
                     
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%"> <%-- FB 2842--%>
                            </td>
                            <td align="left" class="blackblodtext"  valign="top" style="width:25%"> <%-- FB 2842--%>
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_PluginConfirma%>" runat="server"></asp:Literal>       <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgpluginconfirm" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send confirmation mails when conference is scheduled via outlook."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 23%;"> <%-- FB 2842--%>
                                <asp:dropdownlist id="DrpPluginConfirm" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td  style="width: 1%"><%-- FB 2842--%>
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 27%"> <%-- FB 2842--%>
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableAvailabl%>" runat="server"></asp:Literal>          <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgeavailrooms" valign="center" src="image/info.png" runat="server" ToolTip="Switch to show availble rooms  during conference creation via outlook."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:dropdownlist id="lstEnablePIMServiceType" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr><%--ZD 100068 --%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                               <asp:Literal Text="<%$ Resources:WebResources, Tier1%>" runat="server"></asp:Literal> <span class="reqfldText">*</span> <%--ZD 100843--%>
                            </td>
                            <td style="width: 25%" align="left" valign="top">
                                <asp:DropDownList id="lstVMRTopTier" width="120px" datatextfield="Name" datavaluefield="ID"
                                    runat="server" cssclass="altSelectFormat" onselectedindexchanged="UpdateVMRMiddleTiers" 
                                     autopostback="true">
                                    </asp:DropDownList>
                                 <asp:requiredfieldvalidator id="reqTopTier1" runat="server" controltovalidate="lstVMRTopTier"
                                    display="dynamic" cssclass="lblError" validationgroup="Submit" errormessage="<%$ Resources:WebResources, Required%>">
                                </asp:requiredfieldvalidator>
                                
                                
                            </td>                          
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                <asp:Literal Text="<%$ Resources:WebResources, Tier2%>" runat="server"></asp:Literal> <span class="reqfldText">*</span> <%--ZD 100843--%>
                            </td>
                            <td style="width: 20%" align="left" valign="top">
                                <asp:DropDownList id="lstVMRMiddleTier" width="120px" datatextfield="Name" datavaluefield="ID"
                                    runat="server" cssclass="altSelectFormat" causesvalidation="true">
                                    </asp:DropDownList>                                    
                                <asp:requiredfieldvalidator id="reqMiddleTier1" runat="server" controltovalidate="lstVMRMiddleTier"
                                    display="dynamic" cssclass="lblError"  errormessage="<%$ Resources:WebResources, Required%>" validationgroup="Submit"></asp:requiredfieldvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr><%-- ZD 100068 end--%>

                        <%--ALLDEV-834 Starts--%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext"  valign="top" style="width:25%">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Enable_Room_Selection%>" runat="server"></asp:Literal>                               
                            </td>
                            <td valign="top" align="left" style="width: 23%;">
                                <asp:dropdownlist ID="DrpDwnEnableRoomSelection" cssclass="alt2SelectFormat" runat="server">
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                </asp:dropdownlist>
                            </td>
                            <td  style="width: 1%">
                            </td>
                        </tr>
                        <%--ALLDEV-834 Ends--%>

                        <tr style="display:none">
                         <td style="width: 1%;"> </td>
                            <td style="width: 25%;" align="left" class="blackblodtext"> <%-- FB 2842--%>
                                <asp:label id="lblEnableVMR" runat="server" text="<%$ Resources:WebResources, mainadministrator_lblEnableVMR%>"></asp:label>
                                <%--<asp:ImageButton ID="ImgeVMR" valign="center"  src="image/info.png" runat="server" ToolTip="Enable VMR features for calls"/>--%>
                            </td>
                            <td style="width: 25%;" align="left"> <%-- FB 2842--%>
                                <asp:dropdownlist id="lstEnableVMR" runat="server" cssclass="alt2SelectFormat">
                                                    <%--FB 2448--%>
                                                    <asp:ListItem Value="0" Selected="True" Text="None"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Personal"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Room"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="External"></asp:ListItem>
                                                    <%--FB 2481--%>
                                                </asp:dropdownlist>
                            </td>
                        </tr>
                    </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>  <%-- ZD 100068--%>      
                                   
                </td>
            </tr>
            <tr>
            
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_SUR" alternatetext="Expand/Collapse"  cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_Survey%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
           <%-- // ZD 103416 Start--%>
            <tr id="trSUR" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="5"> <%-- FB 2842--%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_EnableSurvey%>" runat="server"></asp:Literal>           <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgensurvey" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable Survey Engine field will display.Survey Engine  contain two options.There are Internal Survey,External Survey."/>--%>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:dropdownlist id="drpenablesurvey" runat="server" width="125px" cssclass="alt2SelectFormat"
                                    onchange="JavaScript:modedisplay1()">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td></tr>
                            <tr>
                            <td style="width: 1%">
                            </td>
                            
                            <td colspan="2" width="47%">
                                <div id="divSurveyURL" runat="server" style="display: none">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" style="font-weight: bold; width: 43%" class="blackblodtext">
                                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SurveyWebsite%>" runat="server"></asp:Literal>              <%--FB 2926--%>
                                                <%--<asp:ImageButton ID="Imgsurveyweburl" valign="center" src="image/info.png" runat="server" ToolTip="Survey link to user for the organzation Options."/>--%>
                                            </td>
                                            <td align="left" style="height: 21px;" width="42%">
                                                <asp:textbox id="txtSurWebsiteURL" runat="server" cssclass="altText"></asp:textbox>
                                                <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />--%>
                                                <asp:requiredfieldvalidator id="ReqSurWebsiteURL" runat="server" controltovalidate="txtSurWebsiteURL"
                                                    errormessage="<%$ Resources:WebResources, ValidationSurveyURl%>" font-names="Verdana" font-size="X-Small"         
                                                    font-bold="False"><font color="red" size="1pt"> required</font></asp:requiredfieldvalidator>  <%--FB 2926--%>
                                                <asp:regularexpressionvalidator id="RegSurWebsiteURL" controltovalidate="txtSurWebsiteURL"
                                                    display="dynamic" runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                                    validationexpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="width: 1%">
                            </td>
                        
                        <%--<tr>
                            <td style="width: 1%">
                            </td>
                            <td id="tdSurveyengine" runat="server" align="left" class="blackblodtext" width="25%"
                                style="visibility: hidden">
                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_tdSurveyengine%>" runat="server"></asp:Literal>   <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgsurveyeng" valign="center" src="image/info.png" runat="server" ToolTip="If select External Survey Option Survey Website Url,Send Survey will display."/>--%>
                            <%--</td>
                            <td id="tdsurveyoption" runat="server" align="left" width="25%" style="visibility: hidden">
                                <asp:dropdownlist id="drpsurveyoption" runat="server" width="125px" cssclass="alt2SelectFormat"
                                    onchange="JavaScript:modesurvey()">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, InternalSurvey%>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ExternalSurvey%>"></asp:ListItem>
                                    </asp:dropdownlist>
                            </td></tr>--%>
                            <td style="width: 1%">
                            </td>
                            <td colspan="2" width="47%">
                                <div id="divSurveytimedur" runat="server" style="display: none">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" style="font-weight: bold; width: 58%" class="blackblodtext">
                                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_SendSurvey%>" runat="server"></asp:Literal>     <%--FB 2926--%>
                                                <%--<asp:ImageButton ID="Imgsendsurvey" valign="center" src="image/info.png" runat="server" ToolTip="How long after the completion of the Conference where the Survey would have been required."/>--%>
                                            </td>
                                            <td style="height: 21px;" align="left" width="42%">
                                                <asp:textbox id="txtTimeDur" runat="server" maxlength="9" cssclass="altText"></asp:textbox>
                                                <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_mins%>" runat="server"></asp:Literal><br />
                                                <asp:regularexpressionvalidator id="RegTimeDur" controltovalidate="txtTimeDur" validationexpression="\d+"
                                                    display="Dynamic" errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
             </td>
            </tr>
            <%--// ZD 103416 End--%>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" align="left" class="blackblodtext">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_ADMOPT" alternatetext="Expand/Collapse"  cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif" 
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td>
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_TechnicalInfo%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trADMOPT" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellspacing="0" cellpadding="5">
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" valign="baseline" class="blackblodtext" style="width: 25%; bottom: auto"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_TechSupportCo%>" runat="server"></asp:Literal></td>
                            <td align="left" valign="baseline" style="width: 25%">
                                <asp:textbox id="ContactName" runat="server" cssclass="altText" maxlength="256" width="187px"></asp:textbox>
                                <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" controltovalidate="ContactName"
                                    errormessage="<%$ Resources:WebResources, ValidationContactName%>" font-names="Verdana" font-size="X-Small"
                                    font-bold="False"><font color="red" size="1pt"> required</font></asp:requiredfieldvalidator>
                                <asp:regularexpressionvalidator id="regContactName" controltovalidate="ContactName"
                                    display="dynamic"  Enabled="false"  runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters21%>"
                                    validationexpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=#$%&~]*$"></asp:regularexpressionvalidator><%--ZD 104391--%>
                                <%--FB 1888--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" style="height: 47px; width: 27%" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_TechSupportEm%>" runat="server"></asp:Literal></td>
                            <td align="left" style="height: 47px; width: 20%" valign="top">
                                <asp:textbox id="ContactEmail" runat="server" cssclass="altText" maxlength="512"></asp:textbox>
                                <asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" controltovalidate="ContactEmail"
                                    errormessage="<%$ Resources:WebResources, ValidationSupportEmail%>" Enabled="false" validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    font-names="Verdana" font-size="X-Small" font-bold="False"><font color="red" size="1pt">invalid</font></asp:regularexpressionvalidator> <%--ZD 104391--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" valign="top" style="width: 25%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_TechSupportPh%>" runat="server"></asp:Literal></td>
                            <td align="left" valign="middle" style="width: 25%">
                                <asp:textbox id="ContactPhone" runat="server" cssclass="altText" width="187px" maxlength="250"></asp:textbox>
                                <%--FB 2498--%>
                                <asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" controltovalidate="ContactPhone"
                                    errormessage="<%$ Resources:WebResources, ValidationContactPhone%>" font-names="Verdana" font-size="X-Small"
                                    font-bold="False"><font color="red" size="1pt"> required</font></asp:requiredfieldvalidator>
                                <asp:regularexpressionvalidator id="RegularExpressionValidator2" controltovalidate="ContactPhone"
                                    display="dynamic" runat="server" setfocusonerror="true" Enabled="false" errormessage="<%$ Resources:WebResources, InvalidCharacters13%>"
                                    validationexpression="^(a-z|A-Z|0-9)*[^\\<>^;?|!`\[\]{}\x22;=@#$%&'~]*$"></asp:regularexpressionvalidator><%--ZD 104391--%>
                                <%--FB 2319--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 27%"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_AdditionalInfo%>" runat="server"></asp:Literal></td>
                            <td align="left" valign="top" style="width: 20%">
                                <asp:textbox id="ContactAdditionInfo" runat="server" cssclass="altText" maxlength="256"></asp:textbox>
                                <asp:regularexpressionvalidator id="regContactAdditionInfo" controltovalidate="ContactAdditionInfo"
                                    display="dynamic" runat="server" setfocusonerror="true" Enabled="false" errormessage="<%$ Resources:WebResources, InvalidCharacters13%>"
                                    validationexpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:regularexpressionvalidator><%--ZD 104391--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <table width="800" border="0" cellspacing="4" cellpadding="4" align="center">
            <tr height="50">
                <td align="right">
                  <%--ZD 100263 start--%>
                    <input id="btnReset" type="Reset" name="Reset" value="<%$ Resources:WebResources, Reset%>" runat="server" class="altMedium0BlueButtonFormat" onclick="javascript:history.go(0);"/>
					<%--ZD 100420--%>
                    <%--<asp:Button type="reset" ID="btnReset" runat="server" Text="Reset" CssClass="altMedium0BlueButtonFormat" Onclientclick="javascript:history.go(0);" />--%>
                    <%--<button ID="btnReset" runat="server" class="altMedium0BlueButtonFormat" >Reset</button>--%>
					<%--ZD 100420--%>
                    <%--ZD 100263 End--%>
                </td>
                <td align="center">
                </td>
                <td align="center">
					<%--ZD 100420--%>
                    <%--<button id="btnSubmit" onclick="javascript:return Submit();" onserverclick="btnSubmit_Click"   
                        style="width:100pt" runat="server">Submit</button>--%>
                   <asp:button runat="server" id="btnSubmit" CausesValidation="true" ValidationGroup="Submit"  onclick="btnSubmit_Click"   
                        onclientclick="javascript:return Submit();" text="<%$ Resources:WebResources, mainadministrator_btnSubmit%>" width="100pt"/>
					<%--ZD 100420--%>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <ajax:ModalPopupExtender ID="MessagePopup" runat="server" TargetControlID="btmTxtmsgPopup"
                        BackgroundCssClass="modalBackground" PopupControlID="MessagePanel" DropShadow="false"
                        Drag="true" CancelControlID="btnMsgClose">
                    </ajax:ModalPopupExtender>
                    <asp:panel id="MessagePanel" runat="server" horizontalalign="Center" width="50%"
                        cssclass="treeSelectedNode">
                            <table width="100%" align="center" border="0" bgcolor="E0E0E0"><%--ZD 100426--%>
                                <tr>
                                    <td align="center" style="width :73%">
                                        <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_ActiveMessage%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td align="left">
                                     <span class="subtitleblueblodtext"><asp:Literal ID="Literal40" Text="<%$ Resources:WebResources, EndBy%>" runat="server"></asp:Literal></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tdTxtMsgDetails" runat="server" colspan="2">
                                        <table width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                        <tr id="tr2" runat="server">
                                                            <td align="left" width="5%">
                                                                <asp:CheckBox ID="chkmsg1" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration1');" />
                                                            </td>
                                                            <td align="left" width="68%">
                                                                <asp:DropDownList ID="drpdownconfmsg1" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="20%">
                                                                <asp:DropDownList ID="drpdownmsgduration1" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="7%">
                                                            </td>
                                                        </tr>
                                                        <tr id="tr3" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg2" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration2');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg2" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration2" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr4" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg3" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration3');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg3" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration3" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" style="display: none;">
                                                                <a id="displayText" href="javascript:toggle();"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_displayText%>" runat="server"></asp:Literal></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table id="toggleText" cellpadding="3" cellspacing="0" width="100%" border="0">
                                                        <tr id="tr5" runat="server">
                                                            <td align="left" width="5%">
                                                                <asp:CheckBox ID="chkmsg4" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration4');" />
                                                            </td>
                                                            <td align="left" width="68%">
                                                                <asp:DropDownList ID="drpdownconfmsg4" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="20%">
                                                                <asp:DropDownList ID="drpdownmsgduration4" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="7%">
                                                            </td>
                                                        </tr>
                                                        <tr id="tr6" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg5" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration5');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg5" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration5" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr7" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg6" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration6');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg6" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration6" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr8" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg7" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration7');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg7" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration7" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr9" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg8" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration8');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg8" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration8" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr10" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg9" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration9');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg9" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration9" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <br />
                                        <button align="middle" runat="server" id="btnMsgClose"
                                            class="altMedium0BlueButtonFormat">
													<asp:Literal Text="<%$ Resources:WebResources, Close%>" runat="server"></asp:Literal>	</button><%--ZD 100420--%>
                                        <button ID="btnMsgSubmit" runat="server" onserverclick="fnOrgTxtMsgSubmit"
                                            class="altMedium0BlueButtonFormat" ValidationGroup="Submit1">
										<asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
                                    </td>
                                </tr>
                            </table>
                        </asp:panel>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
        //Edited For FF...
        if (document.getElementById("systemStartTime_Container") != null)
            document.getElementById("systemStartTime_Container").style.width = "auto"
        if (document.getElementById("systemEndTime_Container") != null)
            document.getElementById("systemEndTime_Container").style.width = "auto"
    </script>

    <script language="javascript" type="text/javascript">
        open24();
    </script>

    </form>
</div>
</body> </html>
<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<!----------------------------------------->
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

<script type="text/javascript">

    //ZD 101527 - Start
    function FnRPRMRoomSync() {

        if (document.getElementById('DrpRPRMRoomSync').value == "0") {
            document.getElementById('DrpRoomSyncAuto').selectedIndex = "0";
            document.getElementById('TxtPollHrs').value = "0";
            document.getElementById('trPoll').style.display = 'none';
            document.getElementById('trImport').style.display = 'none';
            document.getElementById('tdRoomSync').style.visibility = "hidden";
            document.getElementById('tdRoomSyncAuto').style.visibility = "hidden";
            
        }
        else {
            
            document.getElementById('trPoll').style.display = '';
            document.getElementById('trImport').style.display = '';
            document.getElementById('tdRoomSync').style.visibility = "visible";
            document.getElementById('tdRoomSyncAuto').style.visibility = "visible";
        }
    }
    
    if ('<%=Session["EnableRPRMRoomSync"]%>' == "0")
    {
        FnRPRMRoomSync();    
    }

    //ZD 101527 - End

    // ALLOPS-60 Starts
    function fncAudioBridge() {
        if (document.getElementById('lstEnableAudioBridges').value == "0") {
            document.getElementById('drpEnableAudbridgefreebusy').selectedIndex = "0";
            document.getElementById('drpEnableAudbridgefreebusy').value = "0";
            document.getElementById('td4').style.visibility = "hidden";
            document.getElementById('td5').style.visibility = "hidden";

        }
        else {
            //document.getElementById('drpEnableAudbridgefreebusy').value = "0";
            document.getElementById('drpEnableAudbridgefreebusy').style.display = '';
            document.getElementById('td4').style.visibility = "visible";
            document.getElementById('td5').style.visibility = "visible";
        }
    }
    fncAudioBridge()
    // ALLOPS-60 Ends
    function fnHandleTick(stat, par) { //ZD 101019
        if (stat) {
            var ids = par.split(',');
            for (var i = 0; i < ids.length; i++) {
                document.getElementById('chkPos' + ids[i]).checked = false;
            }
        }
    }

    function fnUpdatePosVertStatus() { //ZD 101019
        var idList = ["chkPosA", "chkPosB", "chkPosC", "chkPosD"];
        for (var i = 0; i < idList.length; i++) {
            document.getElementById(idList[i] + 'Vert').value = "0";
            for(var j=1; j<5; j++)
            {
                if (document.getElementById(idList[i] + j.toString()).checked == true) {
                    document.getElementById(idList[i] + 'Vert').value = (i+1).toString();
                    break;
                }
            }
        }
        fnUpdatePosHorStatus();
    }

    function fnUpdatePosHorStatus() { //ZD 101019
        var idList = ["chkPosA", "chkPosB", "chkPosC", "chkPosD"];
        var chkd = "";
        for (var p = 1; p < 5; p++) {
            document.getElementById(idList[p - 1] + 'Hor').value = "";
            for (var q = 0; q < idList.length; q++) {
                if (document.getElementById(idList[q] + p.toString()).checked == true) {
                    chkd += "," + (q + 1).toString();
                }
            }
            chkd = chkd.replace(",", "");
            document.getElementById(idList[p - 1] + 'Hor').value = chkd;
            chkd = "";
        }
    }

    // FB 2384 Starts
    function findPos(obj) {
        var curleft = curtop = 0;

        if (obj.offsetParent) {
            curleft = obj.offsetLeft
            curtop = obj.offsetTop
            while (obj = obj.offsetParent) {
                curleft += obj.offsetLeft
                curtop += obj.offsetTop
            }
        }
        return [curleft, curtop];
    }
    //FB 2384 Ends
    // FB 2335 Starts
    var invokingObject;
    function changeLayout(invoker, myObj, posval) // FB 2384
    {
        invokingObject = invoker;
        promptpicture = "image/pen.gif";
        prompttitle = RSAutoScreenLayoutMapping; //ZD 101022
        epid = "01";
        rowsize = 5;
        images = "01:02:03:04:05:06:07:08:09:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:25:26:27:28:29:30:31:32:33:34:35:36:37:38:39:40:41:42:43:44:45:46:47:48:49:50:51:52:53:54:55:56:57:58:59:";

        if (invokingObject == "Poly2MGC" || invokingObject == "Poly2RMX") {

            images = "01:02:03:04:05:06:12:13:14:15:16:17:18:19:20:24:25:33:60:61:62:63:";

        }
		//ZD 101869 Starts
        if (invokingObject == "hdnCodian") {

            images = "01:02:03:04:05:06:07:08:09:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:25:26:27:28:29:30:31:32:33:34:35:36:37:38:39:40:41:42:43:44:45:46:47:48:49:50:51:52:53:54:55:56:57:58:59:60:61:62:63:";

        }
		//ZD 101869 End

        imgpath = "../en/image/displaylayout/";
        var title = new Array()
        title[0] = "Default ";
        title[1] = "Custom ";
        promptbox = document.createElement('div');
        promptbox.setAttribute('id', 'prompt');
        document.getElementsByTagName('body')[0].appendChild(promptbox);
        promptbox = document.getElementById('prompt').style; // FB 2050
        //FB 2384 starts
        var pos = findPos(myObj);
        if (posval == 1) {
            pos[0] = pos[0] - 90;
            pos[1] = pos[1] - 20;
        }
        else
            pos[0] = pos[0] - 450;//FB 2927
        pos[1] = pos[1] - 160;
        promptbox.position = 'absolute'
        promptbox.top = pos[1] + 'px'; //FB 1373 start FB 2050
        promptbox.left = pos[0] + 'px'; // FB 2050
        //FB 2384 ends
        promptbox.width = rowsize * 125 + 'px'; // FB 2050
        promptbox.border = 'outset 1 #bbbbbb';
        promptbox.height = '400px'; // FB 2050
        promptbox.overflow = 'auto'; //FB 1373 End
        promptbox.backgroundColor = '#FFFFE6'; //ZD 100426


        var varCancel = RSCancel;
        var varSubmit = RSSubmit;

        m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'>&nbsp;</td><td class='tableHeader'>" + prompttitle + "</td></tr></table>"
        m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
        imagesary = images.split(":");
        rowNum = parseInt((imagesary.length + rowsize - 2) / rowsize, 10);
        m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
        //Code Changed for Soft Edge Button
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
        m += "    <button id='btnCancelLayout' class='altMedium0BlueButtonFormat' style='width:80px'  onClick='cancelthis();'>" + varCancel + "</button>"
        m += "    <button id='btnSubmitLayout' class='altMedium0BlueButtonFormat' style='width:80px'  onClick='saveOrder(epid);'>"+ varSubmit +"</button>"
        m += "  </td></tr>"
        m += "	<tr>";
        //Window Dressing
        var DL = RSDisplayLayout //ZD 101022
        m += "    <td colspan='" + (rowsize * 2) + "' align='left' class='blackblodtext'>" + DL + "</td>";
        m += "  </tr>"
        m += "  <tr>"
        m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
        m += "  </tr>"

        //ZD 101869 start
        if (invokingObject == "hdnCodian") {

            m += "<tr><td colspan='" + (rowsize * 2) + "'>";
            m += "<table>";
            m += "  <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='100' onClick='epid=100'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Defaultfamily + "</td>";
            m += "  </tr>";

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='101' onClick='epid=101'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family1 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "05"+ ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "06" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "07" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "44" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='102' onClick='epid=102'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family2 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "01" + ".gif' width='57' height='43' alt='Layout'>"; 
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='103' onClick='epid=103'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family3 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "02" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"


            m += "  <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='104' onClick='epid=104'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family4 + "</td>";
            m += "    </td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "02" + ".gif' width='57' height='43' alt='Layout' >";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "03" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "04" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "43" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "    <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='105' onClick='epid=105'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family5 + "</td>";
            m += "    </td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "25" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>"

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "</table>";
            m += " </td></tr>"

            m += "  <tr>"
            m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
            m += "  </tr>"

        }
        //ZD 101869 End
        imgno = 0;
        for (i = 0; i < rowNum; i++) {
            m += "  <tr>";
            for (j = 0; (j < rowsize) && (imgno < imagesary.length - 1); j++) {


                m += "    <td valign='middle'>";
                m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
                m += "    </td>";
                m += "    <td valign='middle'>";
                m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43' alt='Layout'>"; //ZD 100419
                m += "    </td>";
                imgno++;
            }
            m += "  </tr>";
        }

        m += "  <tr>";
        m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
        m += "  </tr>"
        m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
        //Code Changed for Soft Edge Button
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
        m += "    <button id='btnCancelLayout1' class='altMedium0BlueButtonFormat' style='width:80px' onClick='cancelthis();'>" + varCancel + "</button>"
        m += "    <button id='btnSubmitLayout1' class='altMedium0BlueButtonFormat' style='width:80px' onClick='saveOrder(epid);'>" + varSubmit + "</button>"
        m += "  </td></tr>"
        m += "</table>"

        document.getElementById('prompt').innerHTML = m;
        //ZD 100420
        if (document.getElementById('butLayoutMapping1') != null)            
            document.getElementById('butLayoutMapping1').setAttribute("onblur", "document.getElementById('btnCancelLayout').focus(); document.getElementById('btnCancelLayout').setAttribute('onfocus', '');");
        if (document.getElementById('butLayoutMapping2') != null)
            document.getElementById('butLayoutMapping2').setAttribute("onblur", "document.getElementById('btnCancelLayout').focus(); document.getElementById('btnCancelLayout').setAttribute('onfocus', '');");
        if (document.getElementById('btnSubmitLayout1') != null)
            document.getElementById('btnSubmitLayout1').setAttribute("onblur", "document.getElementById('btnCancelLayout').focus(); document.getElementById('btnCancelLayout').setAttribute('onfocus', '');");
        
    }

    function saveOrder(id) {






        if (id < 10)
            id = "0" + id;

        if (invokingObject == "Poly2MGC") {
            if (id == "001")
                id = document.getElementById("Poly2MGC").value;
            document.getElementById("Poly2MGC").value = id;
            document.getElementById("imgLayoutMapping1").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "Poly2RMX") {
            if (id == "001")
                id = document.getElementById("Poly2RMX").value;
            document.getElementById("Poly2RMX").value = id;
            document.getElementById("imgLayoutMapping2").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "CTMS2Cisco") {
            document.getElementById("CTMS2Cisco").value = id;
            document.getElementById("imgLayoutMapping3").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "CTMS2Poly") {
            document.getElementById("CTMS2Poly").value = id;
            document.getElementById("imgLayoutMapping4").src = "image/displaylayout/" + id + ".gif";
        }
        //ZD 101869 start
        else if (invokingObject == "hdnCodian") {
            document.getElementById("hdnCodian").value = id;
            if (id == "00")
                id = "01";
            if (id == 101) {
                document.getElementById("imgLayoutMapping5").style.display = '';
                document.getElementById("imgLayoutMapping5").src = "image/displaylayout/" + "05" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = '';
                document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "06" + ".gif";
                document.getElementById("imgLayoutMapping7").style.display = '';
                document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "07" + ".gif";
                document.getElementById("imgLayoutMapping8").style.display = '';
                document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "44" + ".gif";
                document.getElementById("lblCodianLO").innerHTML = Family1; 
            }
            else if (id == 102) {
                document.getElementById("imgLayoutMapping5").style.display = '';
                document.getElementById("imgLayoutMapping5").src = "image/displaylayout/" + "01" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family2; 
            }
            else if (id == 103) {
                document.getElementById("imgLayoutMapping5").style.display = '';
                document.getElementById("imgLayoutMapping5").src = "image/displaylayout/" + "02" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family3; 
            }
            else if (id == 104) {
                document.getElementById("imgLayoutMapping5").style.display = '';
                document.getElementById("imgLayoutMapping5").src = "image/displaylayout/" + "02" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = '';
                document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "03" + ".gif";
                document.getElementById("imgLayoutMapping7").style.display = '';
                document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "04" + ".gif";
                document.getElementById("imgLayoutMapping8").style.display = '';
                document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "43" + ".gif";
                document.getElementById("lblCodianLO").innerHTML = Family4; 
            }
            else if (id == 105) {
                document.getElementById("imgLayoutMapping5").style.display = '';
                document.getElementById("imgLayoutMapping5").src = "image/displaylayout/" + "25" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family5; 
            }
            else if (id == 100) {
                document.getElementById("imgLayoutMapping5").style.display = 'none';
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Defaultfamily; 

            }
            else {
                document.getElementById("imgLayoutMapping5").src = "image/displaylayout/" + id + ".gif";
                document.getElementById("imgLayoutMapping5").style.display = '';
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = ""; 
            }
        }
        //ZD 101869 End

        cancelthis();
    }


    function cancelthis() {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
        //window.resizeTo(750,450); //FB Case 536 Saima
    }

    ////    var obj = document.getElementById("dgTxtMsg_ctl03_btnEdit");
    ////    getLabel = function(elem){
    ////    if (elem.id && elem.id=="label") {
    ////    elem.id = "disabledLabel";
    ////    }
    ////    };
    ////    Dom.getElementsBy(getLabel ,'td', obj);


    // FB 2335 Ends
    ChangeEnableSmartP2P();  //FB 2430
    function ChangeEnableSmartP2P() //FB 2430
    {
        document.getElementById("lstEnableSmartP2P").disabled = false;
        if (document.getElementById("p2pConfEnabled").value == "0") {
            document.getElementById("lstEnableSmartP2P").value = "0";
            document.getElementById("lstEnableSmartP2P").disabled = true;
        }
    }

	//ZD 103095 Starts
    ChangeEnableWaitList();
    function ChangeEnableWaitList() {
        var HDValue = document.getElementById("lstEnableHotdeskingConference");
        document.getElementById("chkEnableWaitList").disabled = false;
        if (HDValue && HDValue.value == "0") {
            document.getElementById("chkEnableWaitList").checked = false;
            document.getElementById("chkEnableWaitList").disabled = true;
        }
    }
	//ZD 103095 End
    //FB 2571 START
    function fnFeccOptions() {

        if (document.getElementById("lstenableFECC").value == "2") {

            document.getElementById("DefaultFECC").style.visibility = "hidden";
            document.getElementById("DefaultFECCoptions").style.visibility = "hidden";
        }
        else {
            document.getElementById("DefaultFECC").style.visibility = "visible";
            document.getElementById("DefaultFECCoptions").style.visibility = "visible";

        }
        if (document.getElementById("lstenableFECC").value == "0") {
            document.getElementById("lstdefaultFECC").disabled = true;
            document.getElementById("DefaultFECC").disabled = true;
            document.getElementById("lstdefaultFECC").value = "1";
        }
        else {
            document.getElementById("lstdefaultFECC").disabled = false;
        }
    }
    //FB 2571 END


</script>

<script type="text/javascript">
    function fntimezonesOptions() {

        if (document.getElementById("DropDownZuLu").value == "1") {
            document.getElementById("TimezoneSystems").disabled = true;
            document.getElementById("TimezoneSystems").value = "0";
        }
        else {
            document.getElementById("TimezoneSystems").disabled = false;
        }
    }
    //FB 2588 END
    
    //ZD 100284 - Start
    if (document.getElementById("systemStartTime_Text")) {
        var confstarttime_text = document.getElementById("systemStartTime_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('systemStartTime_Text', 'regSysStartTime',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("systemEndTime_Text")) {
        var confstarttime_text = document.getElementById("systemEndTime_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('systemEndTime_Text', 'regSysEndTime',"<%=Session["timeFormat"]%>")
        };
    }
    //ZD 100420 - Start
    if (document.getElementById('txtWhiteList') != null)
        document.getElementById('txtWhiteList').setAttribute("onblur", "document.getElementById('btnWhiteList').focus(); document.getElementById('btnWhiteList').setAttribute('onfocus', '');");               
    if (document.getElementById('lstWhiteList') != null)
        document.getElementById('lstWhiteList').setAttribute("onkeydown", "if(event.keyCode ==32){javascript:return AddRemoveWhiteList('Rem')}");               
    if (document.getElementById('btnReset') != null)
        document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');");               
    
    if (document.getElementById('TxtPollHrs') != null)
        document.getElementById('TxtPollHrs').setAttribute("onblur", "document.getElementById('btnPoll').focus(); document.getElementById('btnPoll').setAttribute('onfocus', '');");
        
    if (document.getElementById('btnPoll') != null)
        document.getElementById('btnPoll').setAttribute("onblur", "document.getElementById('btnPendingRoomImport').focus(); document.getElementById('btnPendingRoomImport').setAttribute('onfocus', '');");
        
    if (document.getElementById('btnPendingRoomImport') != null)
        document.getElementById('btnPendingRoomImport').setAttribute("onblur", "document.getElementById('btnPendingEndpointImport').focus(); document.getElementById('btnPendingEndpointImport').setAttribute('onfocus', '');");
    //ZD 100420 - End

    //ZD 103046 - Start
    if (document.getElementById('txtPersonnelAlert') != null)
        document.getElementById('txtPersonnelAlert').setAttribute("onblur", "document.getElementById('btnPersonnelAlert').focus(); document.getElementById('btnPersonnelAlert').setAttribute('onfocus', '');");               
    if (document.getElementById('lstPersonnelAlert') != null)
        document.getElementById('lstPersonnelAlert').setAttribute("onkeydown", "if(event.keyCode ==32){javascript:return AddRemovePersonnelAlert('Rem')}");           
    //ZD 103046 - End
          
</script>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    function EscClosePopup() {
			document.getElementById('btnMsgClose').click()
            if(document.getElementById("prompt") != null)
                cancelthis();
        }

        $("label").removeAttr("for"); //ZD 102374

        //ZD 103550 - Start
        if (document.getElementById('RdBJNMeetingID') != null) {
            document.getElementById('RdBJNMeetingID').style.padding = "0px";
            document.getElementById('RdBJNMeetingID').parentNode.style.padding = "0px";
        }
        if (document.getElementById('ChkSelectable') != null) {
            document.getElementById('ChkSelectable').parentNode.style.paddingLeft = "2px";
        }
        function fnBJNtypeDisplay() {
            if (document.getElementById('DrpEnableBJNIntegration') != null) {
                if (document.getElementById('DrpEnableBJNIntegration').value == "1") {
                    document.getElementById('trBJNMeetingID').style.display = "";
                    document.getElementById('spnBJNDisplay').style.display = "";//ZD 104116
                    document.getElementById('DrpBJNDisplay').style.display = "";
                }
                else {
                    document.getElementById('trBJNMeetingID').style.display = "none";
                    document.getElementById('spnBJNDisplay').style.display = "none"; //ZD 104116
                    document.getElementById('DrpBJNDisplay').style.display = "none";
                }
            }
        }
        //ZD 103550 - End

         //ZD 103857
        function fnDaysValiation() {            
            var Open24 = document.getElementById('Open24');            
            var chkBox = document.getElementById('DayClosed');            
            var options = chkBox.getElementsByTagName('input');
            var listOfSpans = chkBox.getElementsByTagName('span');
            var actualSize = options.length;
            var chkValue = 0;
            for (var i = 0; i < options.length; i++) {
                if (options[i].checked) {
                    chkValue = chkValue + 1;
                }
            }

            var sysStartVal = document.getElementById("systemStartTime_Text").value
            var sysEndVal = document.getElementById("systemEndTime_Text").value
            var isOpen24 = false;

            if (Open24.checked || (sysStartVal == "00:00" && sysEndVal == "23:59") || (sysStartVal == "12:00 AM" && sysEndVal == "11:59 PM") || (sysStartVal == "0000Z" && sysEndVal == "2359Z"))
                isOpen24 = true;

            if (chkValue == actualSize && isOpen24) {
                var errlabel = document.getElementById("errLabel");
                
                if (errlabel) {
                    errlabel.innerText = RSDaysValidation;
                    errlabel.style.display = "";
                    errlabel.setAttribute("class", "lblError");
                    errlabel.setAttribute("className", "lblError");
                    scrollTo(0, 0);
                }  
                return false;
            }

            return true
        }
       //ZD 103925 start
        function fnConfDurRangecheck() {
            if(document.getElementById('txtDefaultConfDuration') != null)
            {
                if (document.getElementById('txtDefaultConfDuration').value < 15) {
                    ValidatorEnable(document.getElementById("RangeDefaultConfDuration"), true);
                    return false;
                }
                else {
                    ValidatorEnable(document.getElementById("RangeDefaultConfDuration"), false);
                    return true;
                }
            }
        }
    //ZD 103925 End
        //ALLBUGS-129
        function checkDuplicateMail() {
            var txtMultipleAssistant = document.getElementById("txtMultipleAssistant");
            var lblDuplicate = document.getElementById("lblDuplicate");

            if (txtMultipleAssistant != null && txtMultipleAssistant.value.trim() != "") {
                var aryAssitant = txtMultipleAssistant.value.toLowerCase().trim().replace(/^\s+|\s+$/g, "").split(/\s*;\s*/); //Split the emails by ';' and remove surrounding spaces
                var sorted_arr = aryAssitant.slice().sort(); 
                
                var results = [];
                for (var i = 0; i < aryAssitant.length - 1; i++) {
                    if (sorted_arr[i].trim() == "")
                        continue;
                    if (sorted_arr[i + 1].trim().toLowerCase() == sorted_arr[i].trim().toLowerCase()) {
                        results.push(sorted_arr[i]);
                    }
                }

                if (results.length > 0) {
                    //txtMultipleAssistant.focus();
                    window.setTimeout(function () {
                        txtMultipleAssistant.focus();
                    }, 0);
                    lblDuplicate.innerText = Emailaddressduplicated;
                    lblDuplicate.innerHTML = Emailaddressduplicated;
                    return false;
                }
                else {
                    lblDuplicate.innerText = "";
                    lblDuplicate.innerHTML = "";
                }
            }
        }
</script>
<%--ZD 100428 END--%>

