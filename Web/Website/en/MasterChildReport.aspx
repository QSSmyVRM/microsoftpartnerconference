<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="MasterChildReport" EnableEventValidation="false"  Debug="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> <%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2885 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2885 Ends -->--%>
<%@ Register Assembly ="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2.Export, Version=10.2.11.0, Culture=neutral,PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %> 


<%--Basic validation function--%>
<script type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
  var servertoday = new Date();
     
    function OnShowButtonClick(s) 
    {    
           
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        var ctrl;
         // Code for Department Selection
//        if(hdnSubmitValue.value == "3")
//           ctrl = chk_DeptR;
//        else if(hdnSubmitValue.value == "4")
//           ctrl = chk_DeptE;
//        else if(hdnSubmitValue.value == "5")
//           ctrl = chk_DeptM;
        
        if (s == "dptPopup" || s == "dptPopupChk")   
        {
            var canShow = 0;
             // Code for Department Selection
//            if( (s== "dptPopup" && (hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")) 
//             || (s== "dptPopup" && (hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5") && ctrl != undefined && ctrl.GetChecked())    
//             || (s=="dptPopupChk" && ctrl != undefined && ctrl.GetChecked()) )
//                canShow = 1;
            if(hdnSubmitValue.value == "4" || hdnSubmitValue.value =="5" || hdnSubmitValue.value == "6") //FB 2593
            {
                canShow = 0; 
                
                if(chk_SpecMCUM.GetChecked() || hdnSubmitValue.value == "6")
                {
                    mcuPopup.ShowAtPos(375,0); //ZD 102399
                    BindMCU();
                    
                    if(chk_SpecEptM.GetChecked())
                        BindEndpointList();
                }
            }
            else
                canShow = 1;
                
            if(OrgList.GetSelectedItems().length > 0 && canShow == 1) 
                dptPopup.ShowAtPos(365, 0);
            else
                dptPopup.Hide();
        }
        else if (s == "assignmentPopup") 
        {
            if(lstDepartment.GetSelectedItems().length > 0)
            {
               if(hdnSubmitValue.value == "1")
               {
                    showAssignmentCtrols('block')                
                    assignmentPopup.ShowAtPos(530, 0);        
                    resourcePopup.ShowAtPos(700, 0);  
               } 
               else if(hdnSubmitValue.value == "2")
               {
                    showAssignmentCtrols('block')                
                    assignmentPopup.ShowAtPos(530, 0);        
                    confPopup.ShowAtPos(700, 0);  
               }
            }
            else
            {
                assignmentPopup.Hide();
                resourcePopup.Hide();
                confPopup.Hide();
            }
            
           if(hdnSubmitValue.value == "3")
           {
                if(chk_TiersR.GetChecked())
                    tierPopup.ShowAtPos(530, 0);  
                else 
                    tierPopup.Hide();
           }

            // ZD 102835 Start
            /*
            if (hdnSubmitValue.value == "7") {
                if (chk_wotiers.GetChecked())
                    tierPopup.ShowAtPos(230, 0);
                else
                    tierPopup.Hide();
            }
            */
            // ZD 102835 End

           if(hdnSubmitValue.value == "4")
           {
                //FB 2593
                var e = 0;
                if(chk_ENameE.GetChecked())
                {
                    eptPopup.ShowAtPos(365, 0);
                    e = 1;
                }
                else
                    eptPopup.Hide();
                    
                if(chk_RmNameE.GetChecked())
                {
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                    if(e == 1)
                    {
                        tierPopup.ShowAtPos(530, 0);  
                        rmInfoPopup.ShowAtPos(690, 0);  
                    }
                    {
                        tierPopup.ShowAtPos(365, 0);  
                        rmInfoPopup.ShowAtPos(530, 0);
                    }
                }
                else
                {
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                }   
           }
           
           if(hdnSubmitValue.value == "5")
           {
                var r = 0;
                if(chk_RmNameM.GetChecked() || chk_SpecRoomM.GetChecked()) //FB 2593
                {
                    if(chk_SpecRoomM.GetChecked())
                    {
                        rmPopup.ShowAtPos(210, 170);  //ZD 102399
                        r = 1;
                    }
                    else
                        rmPopup.Hide();
                    
                    tierPopup.Hide();
                    rmInfoPopup.Hide();

                    //ZD 102399
                    if(r == 1)
                    {
                        tierPopup.ShowAtPos(365, 170);  
                        rmInfoPopup.ShowAtPos(530, 170);  
                    }
                    else
                    {
                        tierPopup.ShowAtPos(200, 170);  
                        rmInfoPopup.ShowAtPos(365, 170);
                    }
                }
                else
                {
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                    rmPopup.Hide();
                }
                
                var i = 0, j = 0
                if(chk_SpecMCUM.GetChecked())
                {
                    mcuPopup.Hide()
                    mcuPopup.ShowAtPos(365,0)
                    i = 1;
                    
                    if(OrgList.GetSelectedItems().length > 0)
                    {
                        BindMCU();
                        
                        if(chk_SpecEptM.GetChecked())
                            BindEndpointList();
                    }
                }
                else
                    mcuPopup.Hide()
                
                if(chk_ENameM.GetChecked() || chk_SpecEptM.GetChecked())
                {
                    if(chk_SpecEptM.GetChecked())
                    {
                        endPtPopup.Hide();
                        if(i == 0)
                        {
                            endPtPopup.ShowAtPos(365, 0);
                            i = 1;
                        }
                        else
                        {
                            endPtPopup.ShowAtPos(530, 0);
                            i = 2;j = 1;
                        }
                        
                        if(lstMCU.GetSelectedItems().length > 0)
                            BindEndpointList();
                    } 
                    
                    eptPopup.Hide();
                    if(i == 0)
                    { 
                        if(j == 0)
                            eptPopup.ShowAtPos(365, 0);
                        else
                            eptPopup.ShowAtPos(530, 0);
                    }                        
                    else 
                    {
                        if(j == 0)
                            eptPopup.ShowAtPos(530, 0);
                        else
                            eptPopup.ShowAtPos(690, 0);
                    }
                }
                else
                {
                    eptPopup.Hide();
                    endPtPopup.Hide();
                }
           }
        }
        else if(s == "rmInfoPopup")
        {
//            if(lstTiers.GetSelectedItems().length > 0)
//            {
                if(hdnSubmitValue.value == "3" && chk_NameR.GetChecked())
                   rmInfoPopup.ShowAtPos(700, 0);  
                else if((hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5") && chk_RmNameE.GetChecked())
                   rmInfoPopup.ShowAtPos(850, 0);
                // ZD 102835 Start
                /*
                else if (hdnSubmitValue.value == "7" && chk_woroomname.GetChecked())
                    rmInfoPopup.ShowAtPos(400, 0);
                */
                // ZD 102835 End
                else
                   rmInfoPopup.Hide();
                    
//            }
//            else
//               rmInfoPopup.Hide();
        }
        else if(s == "rmAssetPopup")
        {
            if(chk_AssetsR.GetChecked())
                rmAssetPopup.ShowAtPos(700, 0);  
            else
                rmAssetPopup.Hide();
        }
        else if(s == "assignment")
        {
            if(chk_UserAssignmentR.GetChecked())
            {
                showAssignmentCtrols('None')
                if(assignmentPopup_lstRmApprover)
                    assignmentPopup_lstRmApprover.style.display = 'block';
                assignmentPopup.ShowAtPos(530, 0);        
            }
            else
                assignmentPopup.Hide();
        }        
        else if(s == "tierPopup")
        {
            if(lstEptDetailsM.GetSelectedItems().length > 0)
                tierPopup.ShowAtPos(690, 0);        
            else
                tierPopup.Hide();
        }  
         else if(s == "confOccurencePopup")
        {
            if(lstConfStatus.GetSelectedItems().length > 0)
                confOccurencePopup.ShowAtPos(360, 319);    //ZD 102399  
            else
                confOccurencePopup.Hide();
        }
         else if(s == "confDetailsPopup")
        {
            if(lstConfOccurence.GetSelectedItems().length > 0)
                confDetailsPopup.ShowAtPos(520, 360);        
            else
                confDetailsPopup.Hide();
        }
        else if(s == "CDR")
        {
            popup.Hide();
            popup.ShowAtPos(212, 0); //ZD 102399
            
            if(OrgList.GetSelectedItems().length > 0) 
            {
                mcuPopup.Hide();
                mcuPopup.ShowAtPos(365,0);
            }
        }
        else
        {
            var isChecked
            
            if(s == "popupU")
                isChecked = chk_OrgU.GetChecked();    
            else if(s == "popupR")
                isChecked = chk_OrgR.GetChecked();        
            else if(s == "popupE")
                isChecked = chk_OrgE.GetChecked();        
            else if(s == "popupM")
                isChecked = chk_OrgM.GetChecked();                
            else
                isChecked = chk_Org.GetChecked();
            
            if(isChecked)
                popup.ShowAtPos(212, 0); //ZD 102399
            else
            {
                popup.Hide(); 
                fnClearListSelection(OrgList);
                
                dptPopup.Hide();                
                if(ctrl != undefined)
                    ctrl.SetChecked(false);                    
                             
                if(hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")
                {
                    fnRightParamsClear('O')
                    assignmentPopup.Hide();
                    resourcePopup.Hide();
                    confPopup.Hide();
                }
                //rmInfoPopup.Hide();                
                //eptPopup.Hide();
            }
        }
            
        if(OrgList.GetSelectedItems().length > 0) 
        {        
            if ((s == "dptPopup" && (hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")) || s == "dptPopupChk" 
            || (ctrl != undefined && ctrl.GetChecked() && s != "assignmentPopup" && s != "rmInfoPopup"))        
                getArrayList();
                
            if(s == "popupR" || s == "popupE" || s == "popupM")
                getArrayList();
        }
        else if(ctrl != undefined && s == "dptPopupChk")
            ctrl.SetChecked (false);
            
        if(hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
            PopupPositions(hdnSubmitValue.value)
    }
    
    function PopupPositions(popVal)
    {
        if(popVal == "2")
        {
            
        }
        else if(popVal == "3")
        {
        
        }
        else if(popVal == "4")
        {
        
        }
    }
    
    function showAssignmentCtrols(SorHVar)
    {   
        if(assignmentPopup_lstRmApprover)
            assignmentPopup_lstRmApprover.style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu1))
            document.getElementById(assignmentPopup_ASPxMenu1).style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu2))
            document.getElementById(assignmentPopup_ASPxMenu2).style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu3))
            document.getElementById(assignmentPopup_ASPxMenu3).style.display = SorHVar;
        if(assignmentPopup_lstMCUApprover)
            assignmentPopup_lstMCUApprover.style.display = SorHVar;
        if(assignmentPopup_lstWOAdmin)
            assignmentPopup_lstWOAdmin.style.display = SorHVar;
    }
    
    function getArrayList()
    {        
        var hdnDeptList = document.getElementById("hdnDepartmentList");        
        
        //hdnDeptList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New Dept";
        var AllDeptList = hdnDeptList.value.split("``");
        
        var deptList;
        
        lstDepartment.BeginUpdate();
        
        var lstCnt = lstDepartment.GetItemCount();
         for (var c = 0; c < lstCnt; c++) 
		    lstDepartment.RemoveItem(0);		 
        
        var n =0;
        for (var i = 0; i < AllDeptList.length; i++) 
        {                
            deptList = AllDeptList[i].split('!'); 
            var s = OrgList.GetSelectedItems()
            if(deptList[0] == s[0].value)
            {
                lstDepartment.InsertItem(n,deptList[2],deptList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstDepartment.GetItemCount() > 0)
            lstDepartment.InsertItem(n, RSAll, "0");        
        else
            lstDepartment.InsertItem(n, RSNoDept, "-1");        

        lstDepartment.EndUpdate();
    }
    //FB 2593
    function BindEptFromMCU()
    {
        if(chk_SpecEptM.GetChecked())
              BindEndpointList();
    }
    
    function BindMCU()
    {        
        var hdnMCUList = document.getElementById("hdnMCUList");        
        
        //hdnMCUList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New MCU";
        var AllMCUList = hdnMCUList.value.split("``");
        
        var mcuList;
        
        lstMCU.BeginUpdate();
        
        var lstCnt = lstMCU.GetItemCount();
         for (var c = 0; c < lstCnt; c++) 
		    lstMCU.RemoveItem(0);		 
        
        var n =0; p = 0
        for (var i = 0; i < AllMCUList.length; i++) 
        {                
            var mcuName = "";
            mcuList = AllMCUList[i].split('!'); 
            if(OrgList.GetSelectedItems().length > 0)
            {
                var s = OrgList.GetSelectedItems()
                if(mcuList[0] == s[0].value || mcuList[0] == "0")
                {   
                    p = 0;
                    mcuName = mcuList[2];
                    if(mcuList[0] == "0")
                        p = 1;
                    
                    if(p == 1)
                        mcuName = "(*) " + mcuName ;
                    
                    lstMCU.InsertItem(n,mcuName,mcuList[1]);   
//                    if(p == 1)
//                    {  
//                        var html = document.getElementById("mcuPopup_lstMCU_LBT").childNodes[i].innerHTML;
//                        var htmlArray = html.split('>');
//                        var htmlArray2 = htmlArray[1].split('<');
//                        var starInsert = "<span style='color:red'>*</span>";
//                        var newHtml = htmlArray[0] + '>' + htmlArray2[0] + starInsert + '<' + htmlArray2[1];
//                        document.getElementById("mcuPopup_lstMCU_LBT").childNodes[i].innerHTML = newHtml;
//                    }
                                     
                    n = n + 1;
                }
            }
            else
            {
                mcuName = mcuList[2];
                if(mcuList[0] == "0")
                    mcuName = "(*) " + mcuName ;
                lstMCU.InsertItem(n,mcuName,mcuList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstMCU.GetItemCount() > 0)
            lstMCU.InsertItem(n, RSAll, "0");        
        else
            lstMCU.InsertItem(n, RSNoMCU, "-1");        

        lstMCU.EndUpdate();
    }
    
    function BindEndpointList()
    {        
        var hdnEptList = document.getElementById("hdnEptList");        
        
        //hdnEptList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New MCU";
        var AllEptList = hdnEptList.value.split("``");
        
        var eptList;
        
        lstendPt.BeginUpdate();
        
        var lstCnt = lstendPt.GetItemCount();
         for (var c = 0; c < lstCnt; c++) 
		    lstendPt.RemoveItem(0);		 
        
        var ol = "";
        if(OrgList.GetSelectedItems().length > 0)
            ol = OrgList.GetSelectedItems()[0].value;   
                    
        var n =0;
        for (var i = 0; i < AllEptList.length; i++) 
        {                
            eptList = AllEptList[i].split('!'); 
            if(lstMCU.GetSelectedItems().length > 0 && lstMCU.GetSelectedItems()[0].value != "0")
            {
                var s = lstMCU.GetSelectedItems()
                if(eptList[0] == s[0].value)
                {
                    if(ol != "" && ol != eptList[3])
                        continue;
                    
                    lstendPt.InsertItem(n,eptList[2],eptList[1]);                    
                    n = n + 1;
                }
            }
            else
            {
                if(ol != "" && ol != eptList[3])
                    continue;
                        
                lstendPt.InsertItem(n,eptList[2],eptList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstendPt.GetItemCount() > 0)
            lstendPt.InsertItem(n, RSAll, "0");        
        else
            lstendPt.InsertItem(n, RSNoEpt, "-1");        

        lstendPt.EndUpdate();
    } 
	//FB 2593 Starts
    function BindRoomList()
    {        
        var hdnRoomList = document.getElementById("hdnRoomList");        
        
        //hdnEptList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New MCU";
        var AllRoomList = hdnRoomList.value.split("``");
        
        var roomList;
        lstRm.BeginUpdate();
        
        var lstCnt = lstRm.GetItemCount();
        for (var c = 0; c < lstCnt; c++) 
		   lstRm.RemoveItem(0);		 
        
        var n =0;
        for (var i = 0; i < AllRoomList.length; i++) 
        {                
            roomList = AllRoomList[i].split('!'); 
            if(OrgList.GetSelectedItems().length > 0)
            {
                var s = OrgList.GetSelectedItems()
                if(roomList[0] == s[0].value)
                {
                    lstRm.InsertItem(n,roomList[2],roomList[1]);                    
                    n = n + 1;
                }
            }
            else
            {
                lstRm.InsertItem(n,roomList[2],roomList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstRm.GetItemCount() > 0)
            lstRm.InsertItem(n, RSAll, "0");        
        else
            lstRm.InsertItem(n, RSNoRm, "-1");        

        lstRm.EndUpdate();
    }
    //FB 2593 End

    function fnCancel() //ZD 100420
    {
        window.location.href = 'ReportDetails.aspx';
        return false;
    }
</script>

<%--Show or Hide Popup function --%>
<script type="text/javascript">
    
    function HidePopups()
    {
        popup.Hide();
        dptPopup.Hide();
        assignmentPopup.Hide();
        resourcePopup.Hide();
        confPopup.Hide();
        tierPopup.Hide();
        rmInfoPopup.Hide();
        rmAssetPopup.Hide();
        confStatusPopup.Hide();
        confOccurencePopup.Hide();
        confDetailsPopup.Hide();
        eptDetailsPopup.Hide();
        woDetailsPopup.Hide();
        mcuDetailsPopup.Hide();
        eptPopup.Hide();
        mcuPopup.Hide();//FB 2593
    }
    
    function fnShow(submitVal)
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");    
        var popup2 = $find('ConferencePopUp');
        
        if(rbAdhoc.GetChecked())
        {
            SavedRptListPopup.Hide();
            
            if(popup2)
            {
                if(hdnSubmitValue.value != submitVal)
                {
                    HidePopups();
                    fnClear();
                }
                
                hdnSubmitValue.value = submitVal;
                popup2.show();
                MainPopup.ShowAtPos(0, 0);
                
                var tblConference = document.getElementById("tblConference");            
                var tblUser = document.getElementById("tblUser");            
                var tblRoom = document.getElementById("tblRoom");    
                var tblEndPoint = document.getElementById("tblEndPoint");    
                var tblMCU = document.getElementById("tblMCU");    
                var tblCDR = document.getElementById("tblCDR"); //FB 2593
                var tblWorkOrder = document.getElementById("tblWO"); // ZD 102835
                    
                tblConference.style.display = 'None';
                tblUser.style.display = 'None';
                tblRoom.style.display = 'None';
                tblEndPoint.style.display = 'None';
                tblMCU.style.display = 'None';
                tblCDR.style.display = 'None'; //FB 2593
                tblWorkOrder.style.display = 'None'; // ZD 102835
                
//                btnSave.SetVisible(true);
//                txtSaveReport.SetVisible(true);
                    
                if(submitVal == "1")
                {
                    tblConference.style.display = 'block';
                    MainPopup.SetHeaderText(RSConferences);
                    //ZD 104686
                    var checkboxCollection = document.getElementById('<%=tdCustomOptions.ClientID %>').getElementsByTagName('input');
                    var hdnCustomOptions = document.getElementById("hdnCustomOptions").value;
                    var strCusOption = hdnCustomOptions.split(",");
                    if (hdnCustomOptions != "") {
                        for (var i = 0; i < checkboxCollection.length; i++) {
                            if (checkboxCollection[i].type.toString().toLowerCase() == "checkbox") {
                                var chkID = checkboxCollection[i].id.split('_')[1].replace("chkCustom", "");
                                if (hdnCustomOptions.indexOf(chkID + ",") >= 0)
                                    checkboxCollection[i].checked = true;
                            }
                        }
                    }  
                }
                else  if(submitVal == "2")
                {
                    tblUser.style.display = 'block';
                    MainPopup.SetHeaderText(RSUsers);
                }                
                else  if(submitVal == "3")
                {
                    tblRoom.style.display = 'block';
                    MainPopup.SetHeaderText(RSRooms);
                }    
                else  if(submitVal == "4")
                {
                    tblEndPoint.style.display = 'block';
                    MainPopup.SetHeaderText(RSEndpoints);
                }    
                else  if(submitVal == "5")
                {
                    tblMCU.style.display = 'block';
                    MainPopup.SetHeaderText(RSMCU);
                }
                else  if(submitVal == "6") //FB 2593
                {
                    tblCDR.style.display = 'block';
                    MainPopup.SetHeaderText(RSCDR);
                    //btnSave.SetVisible(false);
                    //txtSaveReport.SetVisible(false);
                    //rbConferenceCDR.SetChecked(true);
                    OnShowButtonClick('CDR');
                }
                else if (submitVal == "7") { // ZD 102835
                    tblWorkOrder.style.display = 'block';
                    MainPopup.SetHeaderText(RSWO);
                }
            }
         
          fnDefaultOrgSelection(submitVal); //FB 2593
                           
        }
        else if (rbSaved.GetChecked())
        {
            var hdnReportNames = document.getElementById("hdnReportNames");        
            var hdnAllReports = document.getElementById("hdnAllReports");        
            var hdnSubmitValue = document.getElementById("hdnSubmitValue");
            var rptList;
            var detailsList;
            var subdetailsList;
            
            hdnSubmitValue.value = submitVal;            
            lstReportList.ClearItems()
            
            var AllRptList = hdnAllReports.value.split("�");
            var n = 0;
            
            for (var i = 0; i < AllRptList.length; i++)
            {
                rptList = AllRptList[i].split('�');
                
                if(rptList[0] == hdnSubmitValue.value)
                {
                    detailsList = rptList[1].split('�');
                                        
                    for (var j = 0; j < detailsList.length; j++)
                    {
                        if(detailsList[j] != "")
                        {
                            subdetailsList = detailsList[j].split('�');
                            
                            lstReportList.InsertItem(n, subdetailsList[0], subdetailsList[1]);
                            
                            n = n + 1;
                        }
                    }
                }
            }
            //FB 2808             
            lstReportList.SetEnabled(true);
                        
            if(lstReportList.GetItemCount() == 0)
            {
                lstReportList.InsertItem(n,"No Saved Reports","-1");
                lstReportList.SetEnabled(false);
                document.getElementById('<%= btnRptDelete.ClientID %>').disabled = true; //FB 2886
                document.getElementById('<%= btnRptView.ClientID %>').disabled = true;
                document.getElementById('<%= btnRptDelete.ClientID %>').className="btndisable";
                document.getElementById('<%= btnRptView.ClientID %>').className="btndisable";
                //btnRptDelete.SetEnabled(false);
                //btnRptView.SetEnabled(false);
            }
            else
            {
                document.getElementById('<%= btnRptDelete.ClientID %>').disabled = false;//FB 2886
                document.getElementById('<%= btnRptView.ClientID %>').disabled = false;
                document.getElementById('<%= btnRptDelete.ClientID %>').className="altShortBlueButtonFormat";
                document.getElementById('<%= btnRptView.ClientID %>').className="altShortBlueButtonFormat";
                //btnRptDelete.SetEnabled(true);
                //btnRptView.SetEnabled(true);
                lstReportList.SetEnabled(true);
            }
            
            SavedRptListPopup.ShowAtPos(160, 220);
            
        }
    }
    //FB 2808 - start
    function fnSelect()
    {
        var items = lstReportList.GetSelectedItems();
        var hdnReportSelection = document.getElementById("hdnReportSelection");
        hdnReportSelection.value = ""; //FB 2886
        //FB 2808 
        if(lstReportList.GetSelectedItems().length > 0)
        { 
            if(lstReportList.GetSelectedItems()[0].value != "-1" )
            {
                for(var i = 0; i < items.length; i++) 
                {
                    if(hdnReportSelection.value == "")
                        hdnReportSelection.value = items[i].index;
                    else
                        hdnReportSelection.value = hdnReportSelection.value + "," + items[i].index;
                }
                //FB 2886
                if(hdnReportSelection.value.indexOf(',') > 0)
                {
                    document.getElementById('<%= btnRptView.ClientID %>').disabled = true;
                    document.getElementById('<%= btnRptView.ClientID %>').className="btndisable";
                    
                    //btnRptView.SetEnabled(false);
                }
                else
                {
                    document.getElementById('<%= btnRptView.ClientID %>').disabled = false;
                    document.getElementById('<%= btnRptView.ClientID %>').className="altShortBlueButtonFormat";
                    //btnRptView.SetEnabled(true);
                }
            }
        }
    }
    
    function fnDelete()
    {
        var hdnReportSelection = document.getElementById("hdnReportSelection");
        
        if(hdnReportSelection.value != "" && lstReportList.GetSelectedItems()[0].value != "-1")
        {
			//FB 2886
            var r = confirm(Reportdelete);
            if (r == true)
            {
                var hdnRptSave = document.getElementById("hdnRptSave");
                if(hdnRptSave)
                    hdnRptSave.value = "1";

                return true;
                DataLoading(1); //ZD 100176       
            }
            else  //FB 2886
                return false;
        }
        else
        {
            alert(MCReportDel);
            return false;
        }
    }
    
    function SavedReportSelection()
    {
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
       
        if(lstReportList.GetSelectedItems().length > 0) {
            DataLoading(1); //ZD 100176
            var sr = lstReportList.GetSelectedItems();
            var rptValue = sr[0].value; 
            
            if(rptValue != "" && rptValue != "-1")
            {
                var subdetailsList = rptValue.split('??');
                
                if(subdetailsList[0] != 'undefined' && subdetailsList[0] != null)                
                    hdnMainIPValue.value = subdetailsList[0];
                    
                if(subdetailsList[1] != 'undefined' && subdetailsList[1] != null)                
                    hdnRightMenuValue.value = subdetailsList[1];
                    
                //fnSubmit();
                //fnClosePopup();
                var hdnOkValue = document.getElementById("hdnOkValue");
                hdnOkValue.value = "1";
            
                SavedRptListPopup.Hide();
            
                //FB 2886
                var btnPreview = document.getElementById("btnPreview");
                btnPreview.click(); 
                //btnPreview.DoClick(); 
                
            }
        }
        else //FB 2808
        {
            alert(MCReportView);
            return false;
        }
        
        return true;
    }
    //FB 2593 - end
   
    function fnClose()
    {    
        var closePup = document.getElementById("ClosePUp");
        
        if(closePup != null)
            closePup.click();        
            
        return false;
    }
    
    function fnPersonAll()
    { 
        if(chk_AllPerson.GetChecked())
        {
            chk_PFN.SetChecked (true); 
            chk_PLN.SetChecked (true); 
            chk_PMail.SetChecked (true); 
            chk_PRole.SetChecked (true); 
            //chk_PPerson.SetChecked (true); 
        }
        else
        {
            chk_PFN.SetChecked (false); 
            chk_PLN.SetChecked (false); 
            chk_PMail.SetChecked (false); 
            chk_PRole.SetChecked (false); 
            //chk_PPerson.SetChecked (false);
        }
    }
    
    function fnHostAll()
    { 
        if(chk_AllHost.GetChecked())
        {
            chk_HFN.SetChecked (true); 
            chk_HLN.SetChecked (true); 
            chk_HMail.SetChecked (true); 
            chk_HRole.SetChecked (true); 
            //chk_HPerson.SetChecked (true); 
        }
        else
        {
            chk_HFN.SetChecked (false); 
            chk_HLN.SetChecked (false); 
            chk_HMail.SetChecked (false); 
            chk_HRole.SetChecked (false); 
            //chk_HPerson.SetChecked (false);             
        }
    }
        
    function fnScheduledAll()
    { 
        if(chk_AllConf.GetChecked())
        {
            chk_Completed.SetChecked (true); 
            chk_Scheduled.SetChecked (true); 
            chk_Deleted.SetChecked (true); 
            chk_Terminated.SetChecked (true); 
            chk_Pending.SetChecked (true); 
        }
        else
        {
            chk_Completed.SetChecked (false); 
            chk_Scheduled.SetChecked (false); 
            chk_Deleted.SetChecked (false); 
            chk_Terminated.SetChecked (false); 
            chk_Pending.SetChecked (false); 
        }
    }
    
    function fnConfTypeAll()
    { 
        if(chk_AllConfTypes.GetChecked())
        {
            chk_Audio.SetChecked (true); 
            chk_Video.SetChecked (true); 
            chk_Room.SetChecked (true); 
            chk_PP.SetChecked (true); 
            //chk_TP.SetChecked (true); 
            
        }
        else
        {
            chk_Audio.SetChecked (false); 
            chk_Video.SetChecked (false); 
            chk_Room.SetChecked (false); 
            chk_PP.SetChecked (false); 
            //chk_TP.SetChecked (false); 
        }
    }
    
    function fnRecurAll()
    { 
        if(chk_AllMode.GetChecked())
        {
            chk_Single.SetChecked (true); 
            chk_Recurring.SetChecked (true); 
        }
        else
        {
            chk_Single.SetChecked (false); 
            chk_Recurring.SetChecked (false); 
        }
    }
    
    function fnRmTypeAll()
    { 
        if(chk_AllTypeR.GetChecked())
        {
            chk_VideoRoomR.SetChecked (true); 
            chk_AudioRoomR.SetChecked (true); 
            chk_MeetingRoomR.SetChecked (true); 
        }
        else
        {
            chk_VideoRoomR.SetChecked (false); 
            chk_AudioRoomR.SetChecked (false); 
            chk_MeetingRoomR.SetChecked (false); 
        }
    }
    
    function fnBehaviorAll()
    { 
        if(chk_AllBehaviorR.GetChecked())
        {
            chk_VideoConferenceR.SetChecked (true); 
            chk_AudioConferenceR.SetChecked (true); 
            chk_RoomConferenceR.SetChecked (true); 
            
            fnBehaviorAllChecked();
        }
        else
        {
            chk_VideoConferenceR.SetChecked (false); 
            chk_AudioConferenceR.SetChecked (false); 
            chk_RoomConferenceR.SetChecked (false);  
            
            confStatusPopup.Hide();
            confOccurencePopup.Hide();
            confDetailsPopup.Hide();
            eptDetailsPopup.Hide();
            woDetailsPopup.Hide();
            mcuDetailsPopup.Hide();
        }
    }
    
    function RemoveALL(e1,e2)
    {
        if(e1.GetChecked() == false)
            e2.SetChecked(false);
    }
    //FB 2654
    function fnConceirgeAll()
    { 
        if(chk_AllConcierge.GetChecked())
        {
            chk_Onsite.SetChecked (true); 
            chk_Meet.SetChecked (true); 
            chk_Monitor.SetChecked (true); 
            chk_VNOC.SetChecked (true); 
        }
        else
        {
            chk_Onsite.SetChecked(false); 
            chk_Meet.SetChecked (false); 
            chk_Monitor.SetChecked (false); 
            chk_VNOC.SetChecked (false); 
        }
    }

    //ZD 101950
    function fnVMRAll() {

        if (chk_AllVMR.GetChecked()) {
            chk_Personal.SetChecked(true);
            chk_RoomVMR.SetChecked(true);
            chk_External.SetChecked(true);
        }
        else {
            chk_Personal.SetChecked(false);
            chk_RoomVMR.SetChecked(false);
            chk_External.SetChecked(false);
        }
    }
        
</script>

<%--fnSubmit function --%>
<script type="text/javascript">

    function fnCheck()
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        if(hdnSubmitValue.value == "")
            return false;
        DataLoading(1);//ZD 100176
        return true;        
    }
    
    function fnSubmit() 
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        var args = fnSubmit.arguments; //FB 2886
        var hdnGroup = document.getElementById("hdnGroup"); //FB 2965
        hdnGroup.value = "";
        if(rbAdhoc.GetChecked()) {
            if(hdnSubmitValue.value == "1")
                fnGetConferenceMenuInput();
            else if(hdnSubmitValue.value == "2")
                fnGetUserMenuInput();
            else if(hdnSubmitValue.value == "3")
                fnGetRoomMenuInput();
            else if(hdnSubmitValue.value == "4")
                fnGetMCUMenuInput(); //FB 2593
                //fnGetEndPointMenuInput();
            else if(hdnSubmitValue.value =="5") //FB 2593
                fnGetMCUMenuInput();
            else if(hdnSubmitValue.value == "6")
                fnGetCDRMenuInput();
            else if (hdnSubmitValue.value == "7") // ZD 102835
                fnGetWorkOrderMenuInput();
        }
        //FB 2886
        if(args[0] == "1")
        {
            fnClose();
            return false;
        }
        else
            return true;
    }

	// ZD 102835 Start
    function fnAssignValue(element, type, cellVal) {
        if (type == "W1" || type == "W2" || type == "W3" || (type =="R" && cellVal != "-" && "<%=Session["EnableTravelAvoidTrack"]%>" == "1") ) { //ZD 103216
            if(type =="R")
                MoreInfoPopup.ShowAtElement(element);
            else
                MoreInfoPopup.ShowAtPos(400,350);
            memo.SetVisible(false);
            if(type =="R")
            {
            cellVal = cellVal.replace(">HostHeading", " align='center'>");
            cellVal = cellVal.replace("border=\"1\"", " border=\"0\"");
            cellVal = cellVal.replace("border:1px", " border:0px");            
            cellVal = cellVal.replace(">ImgRoomHost", " align='center'><input type='image' id='hst' src='../en/image/blue_button.png' Width='12px' >");
            cellVal = cellVal.replace(new RegExp(">AttAlign", "g"), " align='right'>");
            }
            document.getElementById('divInfo').innerHTML = cellVal;

            if (type == "W1")
                MoreInfoPopup.SetHeaderText("<asp:Literal Text='<%$ Resources:WebResources, ConferenceSetup_AudiovisualWor%>' runat='server' />");
            else if (type == "W2")
                MoreInfoPopup.SetHeaderText("<asp:Literal Text='<%$ Resources:WebResources, ConferenceSetup_CateringWorkO%>' runat='server' />");
            else if (type == "W3")
                MoreInfoPopup.SetHeaderText("<asp:Literal Text='<%$ Resources:WebResources, ConferenceSetup_FacilityWorkO%>' runat='server' />");
            else if (type == "R")//ZD 103216
                MoreInfoPopup.SetHeaderText("<asp:Literal Text='<%$ Resources:WebResources, TemplateDetails_Rooms%>' runat='server'></asp:Literal>");
        }
        else {
            memo.SetVisible(true);
            document.getElementById('divInfo').innerHTML = "";
            memo.SetText(cellVal.replace(new RegExp(",", "g"), ";\n"));
            MoreInfoPopup.ShowAtElement(element)
            //ZD 103216
            
            if (type == "R")
                MoreInfoPopup.SetHeaderText("<asp:Literal Text='<%$ Resources:WebResources, TemplateDetails_Rooms%>' runat='server'></asp:Literal>");
            else if (type == "E")
                MoreInfoPopup.SetHeaderText("<asp:Literal Text='<%$ Resources:WebResources, Endpoint_s%>' runat='server'></asp:Literal>");
            else if (type == "D")
                MoreInfoPopup.SetHeaderText("<asp:Literal Text='<%$ Resources:WebResources, Department_s%>' runat='server'></asp:Literal>");
        }
        return false; 
    }
	// ZD 102835 End
</script>

<%--Clear functions--%>
<script type="text/javascript">
    
    function fnClear()
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        
        if(hdnSubmitValue.value == "1")
            fnConferenceMenuClear();
        else if(hdnSubmitValue.value == "2")
            fnUserMenuClear();
        else if(hdnSubmitValue.value == "3")
            fnRoomMenuClear();
        else if(hdnSubmitValue.value == "4")
            fnEndPointMenuClear()
        else if(hdnSubmitValue.value == "5")
            fnMCUMenuClear()
        else if(hdnSubmitValue.value == "6")
            fnCDRMenuClear()
        else if (hdnSubmitValue.value == "7") // ZD 102835
            fnWorkOrderMenuClear();
        return false;
    }
    
    function fnConferenceMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_Org.SetChecked (false); 
            OnShowButtonClick();
        }
        else
        {
			//FB 2882
            //fnClearListSelection(OrgList);
            //dptPopup.Hide();              
            fnClearListSelection(lstDepartment);  
            assignmentPopup.Hide();
            resourcePopup.Hide();
        }    
        chk_ConfID.SetChecked (false); 
        chk_ConfTitle.SetChecked (false); 
		chk_CTSNumericID.SetChecked(false);//Fb 2870
        
        fnRightParamsClear('');
               
        chk_AllHost.SetChecked (false); 
        fnHostAll()
        
        chk_AllPerson.SetChecked (false); 
        fnPersonAll()
        
        chk_AllConf.SetChecked (false);         
        fnScheduledAll()
        
        chk_AllConfTypes.SetChecked (false);         
        fnConfTypeAll();
        
        chk_AllMode.SetChecked (false);         
        fnRecurAll();
        //FB 2654
        chk_AllConcierge.SetChecked (false);
        fnConceirgeAll();
        //ZD 101950
        chk_AllVMR.SetChecked(false);
        fnVMRAll()

        chk_Date.SetChecked (false); 
        chk_Time.SetChecked (false); 
        //chk_Hours.SetChecked (false); 
        chk_Minutes.SetChecked(false);

        //ZD 104686
        chkCustom0.SetChecked(false);
        fnCustomAll();

    }
    
    function fnUserMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_OrgU.SetChecked (false); 
                OnShowButtonClick('popupU');
        }
        else
        {
			//FB 2882
            //fnClearListSelection(OrgList);
            //dptPopup.Hide();
            fnClearListSelection(lstDepartment);  
        }    
        fnRightParamsClear('');
       
        chk_DeptU.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_FNU.SetChecked (false); 
        chk_LNU.SetChecked (false); 
        //chk_PersonU.SetChecked (false); 
        
        chk_UserEmailU.SetChecked (false); 
        chk_SecondaryEmailU.SetChecked (false); 
        chk_RoleU.SetChecked (false); 
        chk_TimeZoneU.SetChecked (false); 
        chk_ConfHostU.SetChecked (false); 
        chk_ConfPartU.SetChecked (false);
        
        chk_AccExpirU.SetChecked (false); 
        chk_MinU.SetChecked (false); 
        chk_ExchangeU.SetChecked (false);
        chk_DominoU.SetChecked(false);
        
        
    }
    
    function fnRoomMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_OrgR.SetChecked (false); 
                OnShowButtonClick('popupR');
        }    
        else
        {
			//FB 2882
            //fnClearListSelection(OrgList);
            //dptPopup.Hide();
            fnClearListSelection(lstDepartment);  
        }    
        
        fnRightParamsClear('');
        
        chk_DeptR.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_TiersR.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_NameR.SetChecked (false); 
            OnShowButtonClick('rmInfoPopup');
            
        chk_HostR.SetChecked (false); 
    
        chk_AllTypeR.SetChecked (false);         
        fnRmTypeAll();
        
        chk_AllBehaviorR.SetChecked (false);         
        fnBehaviorAll();
    }
    
    function fnEndPointMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_OrgE.SetChecked (false); 
                OnShowButtonClick('popupE');
        }   
        //else //FB 2882
        //  fnClearListSelection(OrgList);
        
        fnRightParamsClear('');
        
        //chk_DeptE.SetChecked (false); //FB 2593
            
        chk_ENameE.SetChecked (false); 
        //chk_SpecEptE.SetChecked (false); 
        chk_RmNameE.SetChecked (false); 
        //chk_SpecRoomE.SetChecked (false); 
        
            OnShowButtonClick('assignmentPopup');
        
        chk_ConfE.SetChecked (false); 
            fnBehaviorAllChecked();
    }
    
    function fnMCUMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_OrgM.SetChecked (false); 
                OnShowButtonClick('popupM');
        }    
        //else //FB 2882
            //fnClearListSelection(OrgList);
            
        fnRightParamsClear('');
		//FB 2593
        //chk_DeptM.SetChecked (false); 
            //OnShowButtonClick('assignmentPopup');
        chk_MCUNameM.SetChecked (false); 
        chk_SpecMCUM.SetChecked (false); 
        chk_ENameM.SetChecked (false); 
        chk_SpecEptM.SetChecked (false); 
        chk_RmNameM.SetChecked (false); 
        chk_SpecRoomM.SetChecked(false);
            OnShowButtonClick('assignmentPopup');
        
        chk_ConfM.SetChecked (false); 
            fnBehaviorAllChecked();
    }
    //FB 2593
    function fnCDRMenuClear()
    {
		//FB 2882
        if(document.getElementById("hdnAdmin").value != "U") //FB 2882
            fnClearListSelection(OrgList);
        fnClearListSelection(lstMCU);
        
        if(document.getElementById("hdnAdmin").value != "U") //FB 2882
           mcuPopup.Hide();
    }

    // ZD 102835 Start
    function fnWorkOrderMenuClear() {
        fnRightParamsClear('');

        chk_woallwork.SetChecked(false);
        fnwoallwork(false);

        chk_woallpersonnel.SetChecked(false);
        fnwoallpersonnel(false);

        chk_wodept.SetChecked(false);
        chk_wotiers.SetChecked(false);
        //OnShowButtonClick('assignmentPopup');
        chk_woroomname.SetChecked(false);
        //OnShowButtonClick('rmInfoPopup');
        chk_wohost.SetChecked(false);

        chk_woconfid.SetChecked(false);
        chk_woconftitle.SetChecked(false);
        chk_woorg.SetChecked(false);
        //fnwoallconference(false);

        chk_woallhost.SetChecked(false);
        fnwoallhost(false);

        chk_woallschedule.SetChecked(false);
        fnwoallschedule(false);

        chk_woallconftype.SetChecked(false);
        fnwoallconftype(false);

        chk_woallbehavior.SetChecked(false);
        fnwoallbehavior(false);

        chk_wodate.SetChecked(false);
        chk_wotime.SetChecked(false);

        chk_wominutes.SetChecked(false);
    }

    function fnwoallwork(stat) {
        chk_woav.SetChecked(stat);
        chk_wocater.SetChecked(stat);
        chk_wofacility.SetChecked(stat);
    }

    function fnwoallpersonnel(stat) {
        chk_woadminincharge.SetChecked(stat);
        chk_wopersonincharge.SetChecked(stat);
    }

    /*
    function fnwoallconference(stat) {
        chk_woconfid.SetChecked(stat);
        chk_wopersonin.SetChecked(stat);
    }
    */

    function fnwoallhost(stat) {
        chk_wofirstname.SetChecked(stat);
        chk_wolastname.SetChecked(stat);
        chk_woemail.SetChecked(stat);
        chk_worole.SetChecked(stat);
        //chk_woperson.SetChecked(stat);
    }

    function fnwoallschedule(stat) {
        chk_wocompleted.SetChecked(stat);
        chk_woscheduled.SetChecked(stat);
        chk_wodeleted.SetChecked(stat);
        chk_woterminated.SetChecked(stat);
        chk_wopending.SetChecked(stat);
    }

    function fnwoallconftype(stat) {
        chk_woaudioonly.SetChecked(stat);
        chk_wovideoonly.SetChecked(stat);
        chk_woroomonly.SetChecked(stat);
        chk_wopointtopoint.SetChecked(stat);
        //chk_wotelepresence.SetChecked(stat);
    }

    function fnwoallbehavior(stat) {
        chk_wosingle.SetChecked(stat);
        chk_worecur.SetChecked(stat);
    }
    // ZD 102835 End
    
    function fnRightParamsClear(m)
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        
        if(hdnSubmitValue.value == "1")
        {
            if(document.getElementById("hdnAdmin").value != "U")
                fnClearListSelection(OrgList);
            fnClearListSelection(lstDepartment);
            fnClearListSelection(lstRmApprover);
            fnClearListSelection(lstMCUApprover);
            fnClearListSelection(lstWOAdmin);
            fnClearListSelection(lstAttendee);
            fnClearListSelection(lstRmType);
            fnClearListSelection(lstEPYes);
            fnClearListSelection(lstMCUYes);
            fnClearListSelection(lstConfSpeedYes);
            fnClearListSelection(lstConfProYes);
            fnClearListSelection(lstWOYes);
        }   
        else if(hdnSubmitValue.value == "2")
        {
            //FB 2882
            if(document.getElementById("hdnAdmin").value != "U")
                fnClearListSelection(OrgList);
            fnClearListSelection(lstDepartment);
            fnClearListSelection(lstRmApprover);
            fnClearListSelection(lstMCUApprover);
            fnClearListSelection(lstWOAdmin);
            fnClearListSelection(lstConfType);
        }       
        //else if(hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5" || hdnSubmitValue.value == "7") // ZD 102835
        else if(hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
        {
            if(m == "" || m == "O")
            {
				//FB 2882
                if(document.getElementById("hdnAdmin").value != "U")
                    fnClearListSelection(OrgList);
                fnClearListSelection(lstDepartment);

                if(hdnSubmitValue.value == "3")
                    fnClearListSelection(lstRmInfo);
                
                if(hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
                    fnClearListSelection(lstEptDetailsM);
                    
                fnClearListSelection(lstTiers);
                fnClearListSelection(lstRmInfo);
            }
            
            if(m == "" || m == "C")
            {
            
                fnClearListSelection(lstConfStatus);
                fnClearListSelection(lstConfOccurence);
                fnClearListSelection(lstConfDetails);
                fnClearListSelection(lstEptDetails);
                fnClearListSelection(lstWODetails);
                fnClearListSelection(lstMCUDetails);
            }
        }
    }
    
    function fnClearListSelection(e)
    {   
        //OrgList.UnselectAll();
        
        if(e.GetSelectedItems().length > 0)
        {   
            var lstCnt = e.GetItemCount();
            
            var n =0;
            for (var i = 0; i < lstCnt; i++) 
            {
                TempList.InsertItem(n,e.GetItem(i).value,e.GetItem(i).text);                    
                n = n + 1;
            }
        
            e.ClearItems()
            
            n = 0;
            for (var j = 0; j < lstCnt; j++) 
            {
                e.InsertItem(n,TempList.GetItem(j).value,TempList.GetItem(j).text);                    
                n = n + 1;
            } 
            
            TempList.ClearItems()
        }
    }
    
</script>

<%--Get Input Values from Conference Menu--%>
<script type="text/javascript">

    function fnClosePopup()
    {
        MainPopup.Hide();
//        popup.Hide();
//        dptPopup.Hide();
        //assignmentPopup.Hide();
        //resourcePopup.Hide();
    }
    
    function fnGetConferenceMenuInput()
    {
        var Conference = "";
        var hosts = "";
        var participants = "";
        var scheduledConf = "";
        var confTypes = "";
        var confBehavior = "";
        var confTime = "";
        var duraion = "";
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
            hdnMainIPValue.value = "";
        var orgvalue = "";
        var departments = "";
        var rmIncharge = "";
        var mcuIncharge = "";
        var woIncharge = "";
        var attendees = "";
        var roomsParty = "";
        var endPoints = "";
        var mcu = "";
        var confSpeed = "";
        var conProtocol = "";
        var confWO = "";
        var confVMR = ""; //ZD 101950
        var hdnOkValue = document.getElementById("hdnOkValue");
            hdnOkValue.value = "1";
        
        if(chk_ConfID.GetChecked())
            Conference += "ConfnumName as [Conf.ID] ,";
        if(chk_ConfTitle.GetChecked())
            Conference += "ExternalName as [Conf.Title],";
        if (chk_CTSNumericID.GetChecked()) //FB 2870
            Conference += "E164DialNumber as [CTS Numeric ID],";
        
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
            
        if(chk_Org.GetChecked())
        {
            //Conference += "OrgID,";
            
            hdnRightMenuValue.value = "";
            //Right Side Popups
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                //1:orgid;2:Departmentid;3(Rooms):LastName [Assistant Name],LastName as [Pri Appr.],LastName as [Sec. Appr. 1],LastName as [Sec. Appr. 2]
                //;4(MCU):LastName [Assistant Name],LastName as [Pri Appr.],LastName as [Sec. Appr. 1],LastName as [Sec. Appr. 2]
                //;5(WO):LastName [In Charge],
                //6:[Company Relationship];7:(invitee) 1 - External / 2 - Internal
                //8:(EP)0|1 End Point refer "GetConferenceEndpoint"
                //9:(MCU)0|1 can get from End Point
                //10:(Conf Speed)0|1 can get from End Point
                //11:(Conn Protocol)0|1 can get from End Point
                //12:(WO) 0|1 Name check with type is it A/V | CAT | HK
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";                    
                    
                    //Assignments Menu Start
                    if(lstRmApprover.GetSelectedItems().length > 0)
                    {
                        var r = lstRmApprover.GetSelectedItems()
                    
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 5)
                            rmIncharge = "1,2,3,4,";
                           else
                            rmIncharge += r[i].value +  ",";
                        }
                        
                        if(rmIncharge != "")
                            hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
                    }
                    
                    if(lstMCUApprover.GetSelectedItems().length > 0)
                    {
                        lr = "";
                        var m = lstMCUApprover.GetSelectedItems()
                        
                        for(i = 0; i < m.length; i++)
                        {
                            if(m[i].value == 5)
                                mcuIncharge = "1,2,3,4,";
                            else
                                mcuIncharge += m[i].value +  ",";
                        }
                        
                        if(mcuIncharge != "")
                            hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
                    }
                    
                    if(lstWOAdmin.GetSelectedItems().length > 0)
                    {
                        var w = lstWOAdmin.GetSelectedItems()
                        for(i = 0; i < w.length; i++)
                        {
                            if(w[i].value == 3) 
                                woIncharge = "1,2,";
                            else                            
                                woIncharge += w[i].value +  ",";
                        }
                        if(woIncharge != "")
                            hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
                    }
                    //Assignments Menu End
                    
                    //Resource Menu Start
                    if(lstAttendee.GetSelectedItems().length > 0)
                    {
                        var at = lstAttendee.GetSelectedItems()
                        for(i = 0; i < at.length; i++)
                        {
                            if(at[i].value == 3) 
                                attendees = "1,2,";
                            else   
                                attendees += at[i].value +  ",";
                        }
                        
                        if(attendees != "")
                            hdnRightMenuValue.value += "6:" + attendees.substring(0, attendees.length-1) + "|";    
                    }
                    
                    if(lstRmType.GetSelectedItems().length > 0)
                    {
                        var rm = lstRmType.GetSelectedItems()
                        
                        for(i = 0; i < rm.length; i++)
                        {
                            if(rm[i].value == 3) 
                                attendees = "1,2,";
                            else                            
                                roomsParty += rm[i].value +  ",";
                        }
                        
                        if(roomsParty != "")
                            hdnRightMenuValue.value += "7:" + roomsParty.substring(0, roomsParty.length-1) + "|";    
                    }
                    
                    if(lstEPYes.GetSelectedItems().length > 0)
                    {
                        var ep = lstEPYes.GetSelectedItems()
                    
                        for(i = 0; i < ep.length; i++)
                            endPoints += ep[i].value +  ",";
                        
                        if(endPoints != "")
                            hdnRightMenuValue.value += "8:" + endPoints.substring(0, endPoints.length-1) + "|";    
                    }
                    
                    if(lstMCUYes.GetSelectedItems().length > 0)
                    {
                        var mc = lstMCUYes.GetSelectedItems()
                        for(i = 0; i < mc.length; i++)
                            mcu += mc[i].value +  ",";
                        
                        if(mcu != "")
                            hdnRightMenuValue.value += "9:" + mcu.substring(0, mcu.length-1) + "|";    
                    }
                    
                    if(lstConfSpeedYes.GetSelectedItems().length > 0)
                    {
                        var sp = lstConfSpeedYes.GetSelectedItems()
                        for(i = 0; i < sp.length; i++)
                            confSpeed += sp[i].value +  ",";
                        
                        if(confSpeed != "")
                            hdnRightMenuValue.value += "10:" + confSpeed.substring(0, confSpeed.length-1) + "|";    
                    }
                    var conProtocol = "";
                    if(lstConfProYes.GetSelectedItems().length > 0)
                    {
                        var pr = lstConfProYes.GetSelectedItems()
                        for(i = 0; i < pr.length; i++)
                            conProtocol += pr[i].value +  ",";
                        
                        if(conProtocol != "")
                            hdnRightMenuValue.value += "11:" + conProtocol.substring(0, conProtocol.length-1) + "|";    
                    }
                    
                    if(lstWOYes.GetSelectedItems().length > 0)
                    {
                        var wo = lstWOYes.GetSelectedItems()
                        for(i = 0; i < wo.length; i++)
                            confWO += wo[i].value +  ",";
                        
                        if(confWO != "")
                            hdnRightMenuValue.value += "12:" + confWO.substring(0, confWO.length-1) + "|";    
                    }
                    
                    //Resource Menu End                    
                }
            }
        }
        
        if(hdnRightMenuValue.value != "")
            Conference += "right,";
            
        if(Conference != "")
            hdnMainIPValue.value = "1:" + Conference.substring(0, Conference.length-1) + "|";
        
        if(chk_HFN.GetChecked())
            hosts += "h.FirstName as [Host First Name],";
        if(chk_HLN.GetChecked())
            hosts += "h.LastName as [Host Last Name],";
        if(chk_HMail.GetChecked())
            hosts += "h.Email as [Host Email],";
        if(chk_HRole.GetChecked())
            hosts += "hr.roleName as [Host Role],";
//        if(chk_HPerson.GetChecked())
//            hosts += "Person,";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
            
        if(chk_PFN.GetChecked())
            participants += "p.FirstName as [Part. First Name],";
        if(chk_PLN.GetChecked())
            participants += "p.LastName as [Part. Last Name],";
        if(chk_PMail.GetChecked())
            participants += "p.Email as [Part. Email],";
//        if(chk_PRole.GetChecked())//ZD 102131 start
//            participants += "p.roleName as [Part. Role],";
        if (chk_PRole.GetChecked()) {
            participants += "p.Attended as [Attended Conference],"; //ZD 102131
            //if (chk_PRole.GetChecked())
            participants += "p.MeetingSigninTime as [Date/Time Signed-In],";
        }  //ZD 102131 End
//        if(chk_PPerson.GetChecked())
//            participants += "Person,";
            
        if(participants != "")
            hdnMainIPValue.value += "3:" + participants.substring(0, participants.length-1) + "|";
        
        if(chk_Completed.GetChecked())
            scheduledConf += "7,";
        if(chk_Scheduled.GetChecked())
            scheduledConf += "0,";
        if(chk_Deleted.GetChecked())
            scheduledConf += "9,";
        if(chk_Terminated.GetChecked())
            scheduledConf += "3,";
        if(chk_Pending.GetChecked())
            scheduledConf += "1,";
            
        if(scheduledConf != "")
            hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
        if(chk_Audio.GetChecked())
            confTypes += "6,";
        if(chk_Video.GetChecked())
            confTypes += "2,";
        if(chk_Room.GetChecked())
            confTypes += "7,";    
        if(chk_PP.GetChecked())
            confTypes += "4,";   
//        if(chk_TP.GetChecked())
//            confTypes += "3,";      
       
        if(confTypes != "")
            hdnMainIPValue.value += "5:" + confTypes.substring(0, confTypes.length-1) + "|";
        
        if(chk_Single.GetChecked())
            confBehavior += "0,";
        if(chk_Recurring.GetChecked())
            confBehavior += "1,";          
       
        if(confBehavior != "")
            hdnMainIPValue.value += "6:" + confBehavior.substring(0, confBehavior.length-1) + "|";
            
        if(chk_Date.GetChecked())
            confTime += "ConfDate as [Date of Conf.],";
        if(chk_Time.GetChecked())
            confTime += "ConfTime [Start Time],";   
       
        if(confTime != "")
            hdnMainIPValue.value += "7:" + confTime.substring(0, confTime.length-1) + "|";
        //FB 2654
        var concierge = "";
        if(chk_Onsite.GetChecked())
            concierge += "O,";
        if(chk_Meet.GetChecked())
            concierge += "M,";
        if(chk_Monitor.GetChecked())
            concierge += "C,";
        if(chk_VNOC.GetChecked())
            concierge += "D,";
            
        if(concierge != "")
            hdnMainIPValue.value += "21:" + concierge.substring(0, concierge.length - 1) + "|";

        //ZD 101950
        confVMR = "";
        if (chk_Personal.GetChecked())
            confVMR += "1,";
        if (chk_RoomVMR.GetChecked())
            confVMR += "2,";
        if (chk_External.GetChecked())
            confVMR += "3,";
        
        if (confVMR != "")
            hdnMainIPValue.value += "27:" + confVMR.substring(0, confVMR.length - 1) + "|";
        //ZD 104686 - Start
        
        var customOpt = "";
        var checkboxCollection = document.getElementById('<%=tdCustomOptions.ClientID %>').getElementsByTagName('input');

        for (var i = 0; i < checkboxCollection.length; i++) {
            if (checkboxCollection[i].type.toString().toLowerCase() == "checkbox") {
                if (checkboxCollection[i].checked) {
                    var chkID = checkboxCollection[i].id.split('_')[1].replace("chkCustom", "");
                    if(chkID != "0")
                        customOpt += chkID + "," ;
                }
            }
        }

        if (customOpt != "") {
            hdnMainIPValue.value += "30:" + customOpt.substring(0, customOpt.length - 1) + "|";

            document.getElementById("hdnCustomOptions").value = customOpt;
        }
        //ZD 104686 - End

//        if(chk_Hours.GetChecked())
//            duraion += "Hours,";

        if(chk_Minutes.GetChecked())
            duraion += "a.Duration as [Duration (Mins)],";   //FB 2654     
       
        if(duraion != "")
            hdnMainIPValue.value += "8:" + duraion.substring(0, duraion.length - 1);

        
    }
    
</script>

<%--Get Input Values from User Menu--%>
<script type="text/javascript">
    
    function fnGetUserMenuInput()
    {
        //hdnMainIPValue Values Start from 9
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        var conference = "";
        hdnMainIPValue.value = "";
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
        
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        if(chk_OrgU.GetChecked())
        {
            //conference += "OrgID,";
            hdnRightMenuValue.value = "";
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                        
                    if(lstConfType.GetSelectedItems().length > 0)
                    {
                        var r = lstConfType.GetSelectedItems()
                        var conftypeVal = "";
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 4)
                            conftypeVal = "6,2,7,";
                           else
                            conftypeVal += r[i].value +  ",";
                        }
                        
                         if(conftypeVal != "")
                            hdnMainIPValue.value += "5:" + conftypeVal.substring(0, conftypeVal.length-1) + "|";    
                    }
                    
                    var rmIncharge = "";
                    var mcuIncharge = "";
                    var woIncharge = "";
                    //Assignments Menu Start
                    if(lstRmApprover.GetSelectedItems().length > 0)
                    {
                        var r = lstRmApprover.GetSelectedItems()
                    
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 5)
                            rmIncharge = "1,2,3,4,";
                           else
                            rmIncharge += r[i].value +  ",";
                        }
                        
                        if(rmIncharge != "")
                            hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
                    }
                    
                    if(lstMCUApprover.GetSelectedItems().length > 0)
                    {
                        lr = "";
                        var m = lstMCUApprover.GetSelectedItems()
                        
                        for(i = 0; i < m.length; i++)
                        {
                            if(m[i].value == 5)
                                mcuIncharge = "1,2,3,4,";
                            else
                                mcuIncharge += m[i].value +  ",";
                        }
                        
                        if(mcuIncharge != "")
                            hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
                    }
                    
                    if(lstWOAdmin.GetSelectedItems().length > 0)
                    {
                        var w = lstWOAdmin.GetSelectedItems()
                        for(i = 0; i < w.length; i++)
                        {
                            if(w[i].value == 3) 
                                woIncharge = "1,2,";
                            else                            
                                woIncharge += w[i].value +  ",";
                        }
                        if(woIncharge != "")
                            hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
                    }
                    //Assignments Menu End
                }
            }
        }        
        
        if(hdnRightMenuValue.value != "")
            conference += "right,";
        
        if(conference != "")
            hdnMainIPValue.value = "1:" + conference.substring(0, conference.length-1) + "|";
            
        var hosts = "";
        if(chk_ConfHostU.GetChecked())
            hosts += "h.FirstName as [Host First Name], h.LastName as [Host Last Name],";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
          
        var participants = "";
        if (chk_ConfPartU.GetChecked())
            participants = "p.FirstName as [Part. First Name],p.LastName as [Part. Last Name],p.Email as [Part. Email],p.Attended as [Attended Conference],p.MeetingSigninTime as [Date/Time Signed-In],"; //ZD 102131
            
        if(participants != "")
            hdnMainIPValue.value += "3:" + participants.substring(0, participants.length-1) + "|";
        
        if(chk_FNU.GetChecked())
            commonSelect += "a.FirstName as [First Name],";
        if(chk_LNU.GetChecked())
            commonSelect += "a.LastName as [Last Name],";
        if(chk_UserEmailU.GetChecked())
            commonSelect += "a.Email,";
        if(chk_SecondaryEmailU.GetChecked())
            commonSelect += "a.AlternativeEmail as [Secondary Email],";
        if(chk_RoleU.GetChecked())
            commonSelect += "c.RoleName as [Role Name],";
        if(chk_TimeZoneU.GetChecked())
            commonSelect += "d.StandardName as [Time Zone],";
        if(chk_AccExpirU.GetChecked())
            commonSelect += "a.AccountExpiry as [Account Expiration],";
        if(chk_MinU.GetChecked())
            commonSelect += "e.Timeremaining as [Minutes],";
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
        
        if(chk_ExchangeU.GetChecked())                     
            hdnMainIPValue.value += "10:1|";
        
        if(chk_DominoU.GetChecked())                     
            hdnMainIPValue.value += "11:1|";
    }
</script>

<%--Get Input Values from Room Menu--%>
<script type="text/javascript">
    
    function ShowConfMenu(s)
    {   
    
//        if(s.GetSelectedItems().length >0)
//        {
            var items = s.GetSelectedItems()
            var isEptChecked = 0
            var isMCUChecked = 0
            var isWOChecked = 0
            
            for(i = 0; i < items.length; i++)
            {
                if(items[i].value == 3)
                    isEptChecked = 1;
                else if(items[i].value == 5)
                    isMCUChecked = 1;
                else if(items[i].value == 6)
                    isWOChecked = 1;
                else if(items[i].value == 7)
                {
                    isEptChecked = 1;
                    isWOChecked = 1;
                    isMCUChecked = 1;
                }
            }
            
            if(isEptChecked == 1)
                eptDetailsPopup.ShowAtPos(685, 310);
            else
                eptDetailsPopup.Hide();
                
            if(isMCUChecked == 1)
            {
                if("<%=Session["browser"]%>" == "Firefox") 
                    mcuDetailsPopup.ShowAtPos(365, 469); //ZD 102399
                else
                    mcuDetailsPopup.ShowAtPos(365, 610);
            }
            else
                mcuDetailsPopup.Hide();
                
            if(isWOChecked == 1)
                woDetailsPopup.ShowAtPos(675, 665); //ZD 102380
            else
                woDetailsPopup.Hide();
//        }
    }
    
    function fnBehaviorAllChecked()
    {   
        if(chk_VideoConferenceR.GetChecked() || chk_AudioConferenceR.GetChecked() || chk_RoomConferenceR.GetChecked() || chk_AllBehaviorR.GetChecked() 
        || chk_ConfE.GetChecked() || chk_ConfM.GetChecked()) 
            confStatusPopup.ShowAtPos(207, 319); //ZD 102399
        else
        {
            fnRightParamsClear('C');
            
            confStatusPopup.Hide();
            confOccurencePopup.Hide();
            confDetailsPopup.Hide();
            eptDetailsPopup.Hide();
            woDetailsPopup.Hide();
            mcuDetailsPopup.Hide();
        }
    }
    
    function fnGetRoomMenuInput()
    {
        //hdnMainIPValue Values Start from 12
        //hdnRightMenuValue Values Start from 13
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        hdnMainIPValue.value = "";
        var commonSelect = "";
        var conference = "";
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
            
        if(chk_TiersR.GetChecked())
            commonSelect += " [Top Tier], [Middle Tier],";
        if(chk_NameR.GetChecked())
            commonSelect += " a.[Name],";
            
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
            hdnRightMenuValue.value = "";
            
        if(chk_OrgR.GetChecked())
        {
            //conference += "OrgID,";
            
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                    
                }
            }
        }
        
        var rmIncharge = "";
       
        //Assignments Menu Start
        if(lstRmApprover.GetSelectedItems().length > 0)
        {
            var r = lstRmApprover.GetSelectedItems()
        
            for(i = 0; i < r.length; i++)
            {
               if(r[i].value == "5")
                rmIncharge = "1,2,3,4,";
               else
                rmIncharge += r[i].value +  ",";
            }
            
            if(rmIncharge != "")
                hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
        }
        //Assignments Menu End
                
        var tierInfo = "";
        if(lstTiers.GetSelectedItems().length > 0)
        {
            var ti = lstTiers.GetSelectedItems()
            for(i = 0; i < ti.length; i++)
                tierInfo += ti[i].value + ",";
        }
        
        if(tierInfo != "")
            hdnRightMenuValue.value += "20:" + tierInfo.substring(0, tierInfo.length-1) + "|";  
                        
        var rminfo = "";
        if(lstRmInfo.GetSelectedItems().length > 0)
        {
            var l = lstRmInfo.GetSelectedItems()
            for(i = 0; i < l.length; i++)
            {
                if(l[i].value == "1") commonSelect += " RoomPhone,";
                if(l[i].value == "2") commonSelect += " Capacity,";
                if(l[i].value == "3") commonSelect += " Address1,";
                if(l[i].value == "4") commonSelect += " Address2,";
                if(l[i].value == "5") commonSelect += " RoomFloor,";
                if(l[i].value == "6") commonSelect += " RoomNumber,";
                if(l[i].value == "7") commonSelect += " City,";
                if(l[i].value == "8") commonSelect += " State,";
                if(l[i].value == "9") commonSelect += " Country,";
                if(l[i].value == "10") commonSelect += " ZipCode,";
                if(l[i].value == "11") commonSelect += " TimeZone,";
                if(l[i].value == "12") commonSelect += " [Approver 1],";
                if(l[i].value == "13") commonSelect += " [Approver 2],";
                if(l[i].value == "14") commonSelect += " [Approver 3],";
                
                if(l[i].value == "15")
                {
                    commonSelect += "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                    commonSelect += ",[Approver 2],[Approver 3],";
                }
            }
        }
        
         var rmAssetinfo = "";
        if(lstRmAsset.GetSelectedItems().length > 0)
        {
            var ra = lstRmAsset.GetSelectedItems()
            for(i = 0; i < ra.length; i++)
            {
                if(ra[i].value == "3")
                    rmAssetinfo = "1,2";
                else 
                    rmAssetinfo += ra[i].value;
            }
            
            if(rmAssetinfo != "")
                hdnRightMenuValue.value += "18:" + rmAssetinfo.substring(0, rmAssetinfo.length-1) + "|";    
        }
                
        if(hdnRightMenuValue.value != "")
            conference += "right,"
        
        if(conference != "")
            hdnMainIPValue.value = "1:" + conference.substring(0, conference.length-1) + "|";
            
        var hosts = "";
        if(chk_HostR.GetChecked())
            hosts += "h.FirstName as [Host First Name], h.LastName as [Host Last Name],";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
        
        var rmType = "";
        if(chk_VideoRoomR.GetChecked())
            rmType += "2,";
        if(chk_AudioRoomR.GetChecked())
            rmType += "1,";
        if(chk_MeetingRoomR.GetChecked())
            rmType += "0,";  
        
        if(rmType != "")
            hdnMainIPValue.value += "12:" + rmType.substring(0, rmType.length-1) + "|";
            
        var confType = "";
        if(chk_VideoConferenceR.GetChecked())
            confType += "2,";
        if(chk_AudioConferenceR.GetChecked())
            confType += "6,";
        if(chk_RoomConferenceR.GetChecked())
            confType += "7,";  
        
        if(confType != "") //If Behavior Checkbox's clicked need to handle right menus
        {
            hdnMainIPValue.value += "5:" + confType.substring(0, confType.length-1) + "|";
        
            var scheduledConf = "";
            var immediate = "";
            if(lstConfStatus.GetSelectedItems().length > 0)
            {
                var cs = lstConfStatus.GetSelectedItems()
                        
                for(i = 0; i < cs.length; i++)
                {
                    if(cs[i].value == "15")
                        immediate = "1," ;
                    if(cs[i].value == "16")
                        scheduledConf = "7,0,9,3,1,";
                    else
                        scheduledConf += cs[i].value +  ",";
                }
            }
            
            if(scheduledConf != "")
                hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
            if(immediate != "")
                hdnMainIPValue.value += "13:" + immediate.substring(0, immediate.length-1) + "|";
            
            var behavConf = "";
            if(lstConfOccurence.GetSelectedItems().length > 0)
            {
                var co = lstConfOccurence.GetSelectedItems()
                for(i = 0; i < co.length; i++)
                {
                    if(co[i].value == "2")
                        behavConf = "0,1,";
                    else
                        behavConf += co[i].value +  ",";
                }
            }
           
            if(behavConf != "")
                hdnMainIPValue.value += "6:" + behavConf.substring(0, behavConf.length-1) + "|";
            
            var subConfSelect = "";
            if(lstConfDetails.GetSelectedItems().length > 0)
            {
                var cd = lstConfDetails.GetSelectedItems()
                
                var eptDetails = "";
                var mcuDetails = "";
                var woDetails = "";
                       
                for(i = 0; i < cd.length; i++)
                {
                    if(cd[i].value == "1")
                        subConfSelect += "ConfNumName,";
                    else if(cd[i].value == "2")
                        subConfSelect += "ExternalName,";
                    else if(cd[i].value == "3")
                    {
                        if(lstEptDetails.GetSelectedItems().length > 0)
                        {
                            var e = lstEptDetails.GetSelectedItems()
                            for(j = 0; j < e.length; j++)
                            {
                                if(e[j].value == "5")
                                    eptDetails = "1,2,3,4,";
                                else 
                                    eptDetails += e[j].value +  ",";
                            }
                                
                           if(eptDetails != "")
                                hdnMainIPValue.value += "15:" + eptDetails.substring(0, eptDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "5")
                    {
                        if(lstMCUDetails.GetSelectedItems().length > 0)
                        {
                        var k = 0;
                            var m = lstMCUDetails.GetSelectedItems()
                            for(k = 0; k < m.length; k++)       
                            {
                                if(m[k].value == "4")                         
                                    mcuDetails = "1,2,3,";
                                else
                                    mcuDetails += m[k].value +  ",";
                            }
                                
                           if(mcuDetails != "")
                                hdnMainIPValue.value += "16:" + mcuDetails.substring(0, mcuDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "6")
                    {
                        if(lstWODetails.GetSelectedItems().length > 0)
                        {
                            var m = lstWODetails.GetSelectedItems()
                            for(l = 0; l < m.length; l++)      
                            {                          
                                if(m[l].value == "4")
                                    woDetails =  "1,2,3,";
                                else
                                    woDetails += m[l].value +  ",";
                            }
                                
                           if(woDetails != "")
                                hdnMainIPValue.value += "17:" + woDetails.substring(0, woDetails.length-1) + "|";
                        }
                    }
                }
            }
           
            if(subConfSelect != "")
                hdnMainIPValue.value += "14:" + subConfSelect.substring(0, subConfSelect.length-1) + "|";
        }
       
        if(commonSelect == "")
            commonSelect = " a.[Name],";
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
        
    }
</script>

<%--Get Input Values from Endpoint Menu--%>
<script type="text/javascript">
    
    function fnGetEndPointMenuInput()
    {
        //hdnMainIPValue Values Start from 12
        //hdnRightMenuValue Values Start from 13
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        
        //if(chk_DeptE.GetChecked())
        //    commonSelect += "[Top Tier], [Middle Tier],";
        if(chk_RmNameE.GetChecked())
            commonSelect += " a.[Name],";
            
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        var conference = ""; 
        
        if(chk_OrgE.GetChecked() || chk_OrgM.GetChecked())
        {
            //conference += "OrgID,";            
            
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                }
            }
        }
        
        
        var rmIncharge = "";
        var mcuIncharge = "";
        var woIncharge = "";
        //Assignments Menu Start
        if(lstRmApprover.GetSelectedItems().length > 0)
        {
            var r = lstRmApprover.GetSelectedItems()
        
            for(i = 0; i < r.length; i++)
            {
               if(r[i].value == "5")
                rmIncharge = "1,2,3,4,";
               else
                rmIncharge += r[i].value +  ",";
            }
            
            if(rmIncharge != "")
                hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
        }
        if(lstMCUApprover.GetSelectedItems().length > 0)
        {
            lr = "";
            var m = lstMCUApprover.GetSelectedItems()
            
            for(i = 0; i < m.length; i++)
            {
                if(m[i].value == 5)
                    mcuIncharge = "1,2,3,4,";
                else
                    mcuIncharge += m[i].value +  ",";
            }
            
            if(mcuIncharge != "")
                hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
        }
        
        if(lstWOAdmin.GetSelectedItems().length > 0)
        {
            var w = lstWOAdmin.GetSelectedItems()
            for(i = 0; i < w.length; i++)
            {
                if(w[i].value == 3) 
                    woIncharge = "1,2,";
                else                            
                    woIncharge += w[i].value +  ",";
            }
            if(woIncharge != "")
                hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
        }
        
        //Assignments Menu End
        
        var tierInfo = "";
        if(lstTiers.GetSelectedItems().length > 0)
        {
            var ti = lstTiers.GetSelectedItems()
            for(i = 0; i < ti.length; i++)
                tierInfo += ti[i].value + ",";
        }
        
        if(tierInfo != "")
            hdnRightMenuValue.value += "20:" + tierInfo.substring(0, tierInfo.length-1) + "|";  
            
        var rminfo = "";
        if(lstRmInfo.GetSelectedItems().length > 0)
        {
            var l = lstRmInfo.GetSelectedItems()
            for(i = 0; i < l.length; i++)
            {
                if(l[i].value == "1") commonSelect += " RoomPhone,";
                if(l[i].value == "2") commonSelect += " Capacity,";
                if(l[i].value == "3") commonSelect += " Address1,";
                if(l[i].value == "4") commonSelect += " Address2,";
                if(l[i].value == "5") commonSelect += " RoomFloor,";
                if(l[i].value == "6") commonSelect += " RoomNumber,";
                if(l[i].value == "7") commonSelect += " City,";
                if(l[i].value == "8") commonSelect += " State,";
                if(l[i].value == "9") commonSelect += " Country,";
                if(l[i].value == "10") commonSelect += " ZipCode,";
                if(l[i].value == "11") commonSelect += " TimeZone,";
                if(l[i].value == "12") commonSelect += " [Approver 1],";
                if(l[i].value == "13") commonSelect += " [Approver 2],";
                if(l[i].value == "14") commonSelect += " [Approver 3],";
                
                if(l[i].value == "15")
                {
                    commonSelect += "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                    commonSelect += ",[Approver 2],[Approver 3],";
                }
            }
        }
        
        var rmAssetinfo = "";
        if(lstRmAsset.GetSelectedItems().length > 0)
        {
            var ra = lstRmAsset.GetSelectedItems()
            for(i = 0; i < ra.length; i++)
            {
                if(ra[i].value == "3")
                    rmAssetinfo = "1,2";
                else 
                    rmAssetinfo += ra[i].value;
            }
            
            if(rmAssetinfo != "")
                hdnRightMenuValue.value += "18:" + rmAssetinfo.substring(0, rmAssetinfo.length-1) + "|";    
        }
        
        var eptInfo = "";
        
        if(chk_ENameE.GetChecked())
            eptInfo += "[Endpoint Name],";
            
        if(lstEptDetailsM.GetSelectedItems().length > 0)
        {
            var ed = lstEptDetailsM.GetSelectedItems()
            for(i = 0; i < ed.length; i++)
            {
                if(ed[i].value == "1") eptInfo += " [Default Profile Name],";
                if(ed[i].value == "2") eptInfo += " [Number of Profiles],";
                if(ed[i].value == "3") eptInfo += " [Address Type],";
                if(ed[i].value == "4") eptInfo += " Address,";
                if(ed[i].value == "5") eptInfo += " Model,";
                if(ed[i].value == "6") eptInfo += " [Preferred Bandwidth],";
                if(ed[i].value == "7") eptInfo += " [MCU Assignment],";
                if(ed[i].value == "8") eptInfo += " [Pre. Dialing Option],";
                if(ed[i].value == "9") eptInfo += " [Default Protocol],";
                if(ed[i].value == "10") eptInfo += " [Web Access URL],";
                if(ed[i].value == "11") eptInfo += " [Network Location],";                            
                if(ed[i].value == "12") eptInfo += " [Telnet Enabled],";
                if(ed[i].value == "13") eptInfo += " [Email ID],";
                if(ed[i].value == "14") eptInfo += " [API Port],";
                if(ed[i].value == "15") eptInfo += " [iCal Invite],";
                
                if(ed[i].value == "16")
                {
                     eptInfo = " [Endpoint Name],[Default Profile Name], [Number of Profiles], [Address Type], Address, Model, [Preferred Bandwidth],";
                     eptInfo += " [MCU Assignment], [Pre. Dialing Option], [Default Protocol], [Web Access URL], [Network Location], ";
                     eptInfo += " [Approver 1], [Telnet Enabled], [Email ID], [API Port], [iCal Invite],";
                }
            }
            
            if(eptInfo != "")
                hdnMainIPValue.value += "19:" + eptInfo.substring(0, eptInfo.length-1) + "|";
        }
        
        if(hdnRightMenuValue.value != "")
            conference += "right,"
            
        if(conference != "")
            hdnMainIPValue.value += "1:" + conference.substring(0, conference.length-1) + "|";
                
        if(chk_ConfE.GetChecked())//If Behavior Checkbox's clicked need to handle right menus
        {
            var scheduledConf = "";
            var immediate = "";
            if(lstConfStatus.GetSelectedItems().length > 0)
            {
                var cs = lstConfStatus.GetSelectedItems()
                        
                for(i = 0; i < cs.length; i++)
                {
                    if(cs[i].value == "9")
                        immediate = "1," ;
                    if(cs[i].value == "16")
                        scheduledConf = "7,0,9,3,1,";
                    else
                        scheduledConf += cs[i].value +  ",";
                }
            }
            
            if(scheduledConf != "")
                hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
            if(immediate != "")
                hdnMainIPValue.value += "13:" + immediate.substring(0, immediate.length-1) + "|";
            
            var behavConf = "";
            if(lstConfOccurence.GetSelectedItems().length > 0)
            {
                var co = lstConfOccurence.GetSelectedItems()
                for(i = 0; i < co.length; i++)
                {
                    if(co[i].value == "2")
                        behavConf = "0,1,";
                    else
                        behavConf += co[i].value +  ",";
                }
            }
           
            if(behavConf != "")
                hdnMainIPValue.value += "6:" + behavConf.substring(0, behavConf.length-1) + "|";
            
            var subConfSelect = "";
            if(lstConfDetails.GetSelectedItems().length > 0)
            {
                var cd = lstConfDetails.GetSelectedItems()
                
                var eptDetails = "";
                var mcuDetails = "";
                var woDetails = "";
                       
                for(i = 0; i < cd.length; i++)
                {
                    if(cd[i].value == "1")
                        subConfSelect += "ConfNumName,";
                    else if(cd[i].value == "2")
                        subConfSelect += "ExternalName,";
                    else if(cd[i].value == "3")
                    {
                        if(lstEptDetails.GetSelectedItems().length > 0)
                        {
                            var e = lstEptDetails.GetSelectedItems()
                            for(i = 0; i < e.length; i++)                                
                                eptDetails += e[i].value +  ",";
                                
                           if(eptDetails != "")
                                hdnMainIPValue.value += "15:" + eptDetails.substring(0, eptDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "5")
                    {
                        if(lstMCUDetails.GetSelectedItems().length > 0)
                        {
                            var m = lstMCUDetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                                mcuDetails += m[i].value +  ",";
                                
                           if(mcuDetails != "")
                                hdnMainIPValue.value += "16:" + mcuDetails.substring(0, mcuDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "6")
                    {
                        if(lstWODetails.GetSelectedItems().length > 0)
                        {
                            var m = lstWODetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                                woDetails += m[i].value +  ",";
                                
                           if(woDetails != "")
                                hdnMainIPValue.value += "17:" + woDetails.substring(0, woDetails.length-1) + "|";
                        }
                    }
                }
            }
           
            if(subConfSelect != "")
                hdnMainIPValue.value += "14:" + subConfSelect.substring(0, subConfSelect.length-1) + "|";
        }
       
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
    }
</script>

<%--Get Input Values from MCU Menu--%>
<script type="text/javascript">
    
    function fnGetMCUMenuInput()
    {
    
        //hdnMainIPValue Values Start from 12
        //hdnRightMenuValue Values Start from 13
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        hdnMainIPValue.value = "";
        //if(chk_DeptE.GetChecked())
        //    commonSelect += "[Top Tier], [Middle Tier],";
        if(chk_RmNameE.GetChecked() || chk_RmNameM.GetChecked())
            commonSelect += " a.[Name] as [Room Name],";
            
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        var conference = ""; 
        
        if(chk_OrgE.GetChecked() || chk_OrgM.GetChecked())
        {
            //conference += "OrgID,";            
            
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
            }
        }
        
        var tierInfo = "";
        if((chk_RmNameE.GetChecked() || chk_SpecRoomM.GetChecked() || chk_RmNameM.GetChecked()) && lstTiers.GetSelectedItems().length > 0)
        {
            var ti = lstTiers.GetSelectedItems()
            for(i = 0; i < ti.length; i++)
            {
                if(ti[i].value == "1") tierInfo += " Tier1 as [Tier 1],";
                if(ti[i].value == "2") tierInfo += " Tier2 as [Tier 2],";
                
                if(ti[i].value == "3")
                    tierInfo += " Tier1 as [Tier 1], Tier2 as [Tier 2],";
            }
        }
        
        if(tierInfo != "")
            hdnMainIPValue.value += "20:" + tierInfo.substring(0, tierInfo.length-1) + "|"; 
            
        var mcu = "";
        if(chk_SpecMCUM.GetChecked())
        {
            if(lstMCU.GetSelectedItems().length > 0)
            {
                var m = lstMCU.GetSelectedItems();
                for(i = 0; i < m.length; i++)
                {
                    if(m[i].value == "0")
                    {
                        var lstCnt = lstMCU.GetItemCount();  
                        mcu = "";                          
                        for (var lc = 0; lc < lstCnt; lc++)                             
                            mcu += lstMCU.GetItem(lc).value + ",";
                    }
                    else if(m[i].value == "-1")
                        mcu = "";
                    else
                        mcu += m[i].value +  ",";
                }
            }
        }
        
        if(mcu != "")
            hdnMainIPValue.value += "24:" + mcu.substring(0, mcu.length-1) + "|";   
        
        var specEpt = "";
        if(chk_SpecEptM.GetChecked())
        {
            if(lstendPt.GetSelectedItems().length > 0)
            {
                var m = lstendPt.GetSelectedItems();
                for(i = 0; i < m.length; i++)
                {
                    if(m[i].value == "0")
                    {
                        var lstCnt = lstendPt.GetItemCount();  
                        specEpt = "";                          
                        for (var lc = 0; lc < lstCnt; lc++)                             
                            specEpt += lstendPt.GetItem(lc).value + ",";
                    }
                    else if(m[i].value == "-1")
                        specEpt = "";
                    else
                        specEpt += m[i].value +  ",";
                }
            }
        }
        
        if(specEpt != "")
            hdnMainIPValue.value += "26:" + specEpt.substring(0, specEpt.length-1) + "|";   
            
        var specRoom = "";
        if(chk_SpecRoomM.GetChecked())
        {
            if(lstRm.GetSelectedItems().length > 0)
            {
                var m = lstRm.GetSelectedItems();
                for(i = 0; i < m.length; i++)
                {
                    if(m[i].value == "0")
                    {
                        var lstCnt = lstRm.GetItemCount();  
                        specRoom = "";                          
                        for (var lc = 0; lc < lstCnt; lc++)                             
                            specRoom += lstRm.GetItem(lc).value + ",";
                    }
                    else if(m[i].value == "-1")
                        specRoom = "";
                    else
                        specRoom += m[i].value +  ",";
                }
            }
        }
        
        if(specRoom != "")
            hdnMainIPValue.value += "25:" + specRoom.substring(0, specRoom.length-1) + "|";                 
            
        var rminfo = "";
        if((chk_RmNameE.GetChecked() || chk_SpecRoomM.GetChecked() || chk_RmNameM.GetChecked()) && lstRmInfo.GetSelectedItems().length > 0)
        {
            var l = lstRmInfo.GetSelectedItems()
            for(i = 0; i < l.length; i++)
            {
                if(l[i].value == "1") commonSelect += " RoomPhone,";
                if(l[i].value == "2") commonSelect += " Capacity,";
                if(l[i].value == "3") commonSelect += " Address1,";
                if(l[i].value == "4") commonSelect += " Address2,";
                if(l[i].value == "5") commonSelect += " RoomFloor,";
                if(l[i].value == "6") commonSelect += " RoomNumber,";
                if(l[i].value == "7") commonSelect += " City,";
                if(l[i].value == "8") commonSelect += " State,";
                if(l[i].value == "9") commonSelect += " Country,";
                if(l[i].value == "10") commonSelect += " ZipCode,";
                if(l[i].value == "11") commonSelect += " TimeZone,";
                if(l[i].value == "12") commonSelect += " [Approver 1],";
                if(l[i].value == "13") commonSelect += " [Approver 2],";
                if(l[i].value == "14") commonSelect += " [Approver 3],";
                
                if(l[i].value == "15")
                {
                    commonSelect += "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                    commonSelect += ",[Approver 2],[Approver 3],";
                }
            }
        }
        
        var eptInfo = "";
        
        if(chk_ENameE.GetChecked() || chk_ENameM.GetChecked())
            eptInfo += "[Endpoint Name],";
            
        if((chk_ENameE.GetChecked() || chk_ENameM.GetChecked()) && lstEptDetailsM.GetSelectedItems().length > 0)
        {
            var ed = lstEptDetailsM.GetSelectedItems()
            for(i = 0; i < ed.length; i++)
            {
                if(ed[i].value == "1") eptInfo += " [Default Profile Name],";
                if(ed[i].value == "2") eptInfo += " [Number of Profiles],";
                if(ed[i].value == "3") eptInfo += " [Address Type],";
                if(ed[i].value == "4") eptInfo += " Address,";
                if(ed[i].value == "5") eptInfo += " Model,";
                if(ed[i].value == "6") eptInfo += " [Preferred Bandwidth],";
                if(ed[i].value == "7") eptInfo += " [MCU Assignment],";
                if(ed[i].value == "8") eptInfo += " [Pre. Dialing Option],";
                if(ed[i].value == "9") eptInfo += " [Default Protocol],";
                if(ed[i].value == "10") eptInfo += " [Web Access URL],";
                if(ed[i].value == "11") eptInfo += " [Network Location],";                            
                if(ed[i].value == "12") eptInfo += " [Telnet Enabled],";
                if(ed[i].value == "13") eptInfo += " [Email ID],";
                if(ed[i].value == "14") eptInfo += " [API Port],";
                if(ed[i].value == "15") eptInfo += " [iCal Invite],";
                
                if(ed[i].value == "16")
                {
                     eptInfo = " [Endpoint Name],[Default Profile Name], [Number of Profiles], [Address Type], Address, Model, [Preferred Bandwidth],";
                     eptInfo += " [MCU Assignment], [Pre. Dialing Option], [Default Protocol], [Web Access URL], [Network Location], ";
                     eptInfo += " [Approver 1], [Telnet Enabled], [Email ID], [API Port], [iCal Invite],";
                }
            }
            
            if(eptInfo != "")
                hdnMainIPValue.value += "19:" + eptInfo.substring(0, eptInfo.length-1) + "|";
        }
        
        if(hdnRightMenuValue.value != "")
            conference += "right,"
            
        if(conference != "")
            hdnMainIPValue.value += "1:" + conference.substring(0, conference.length-1) + "|";
                
        if(chk_ConfE.GetChecked() || chk_ConfM.GetChecked())//If Behavior Checkbox's clicked need to handle right menus
        {
            hdnMainIPValue.value += "23:1|";
            
            var scheduledConf = "";
            var immediate = "";
            if(lstConfStatus.GetSelectedItems().length > 0)
            {
                var cs = lstConfStatus.GetSelectedItems()
                        
                for(i = 0; i < cs.length; i++)
                {
                    if(cs[i].value == "9")
                        immediate = "1," ;
                    if(cs[i].value == "16")
                        scheduledConf = "7,0,9,3,1,";
                    else
                        scheduledConf += cs[i].value +  ",";
                }
            }
            
            if(scheduledConf != "")
                hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
            if(immediate != "")
                hdnMainIPValue.value += "13:" + immediate.substring(0, immediate.length-1) + "|";
            
            var behavConf = "";
            if(lstConfOccurence.GetSelectedItems().length > 0)
            {
                var co = lstConfOccurence.GetSelectedItems()
                for(i = 0; i < co.length; i++)
                {
                    if(co[i].value == "2")
                        behavConf = "0,1,";
                    else
                        behavConf += co[i].value +  ",";
                }
            }
           
            if(behavConf != "")
                hdnMainIPValue.value += "6:" + behavConf.substring(0, behavConf.length-1) + "|";
            
            var subConfSelect = "";
            if(lstConfDetails.GetSelectedItems().length > 0)
            {
                var cd = lstConfDetails.GetSelectedItems()
                
                var eptDetails = "";
                var mcuDetails = "";
                var woDetails = "";
                var confDetails = "";       
                for(c = 0; c < cd.length; c++)
                {
                    if(cd[c].value == "7")
                    {
                        subConfSelect = "";eptDetails=""; mcuDetails = ""; woDetails = ""; confDetails = "";
                    }
                    if(cd[c].value == "1" || cd[c].value == "7")
                        subConfSelect += "ConfNumName,";
                    if(cd[c].value == "2" || cd[c].value == "7")
                        subConfSelect += "ExternalName,";
                    if(cd[c].value == "3" || cd[c].value == "7")
                    {
                        if(lstEptDetails.GetSelectedItems().length > 0)
                        {
                            var e = lstEptDetails.GetSelectedItems()
                            for(i = 0; i < e.length; i++)                                
                            {
                                if(e[i].value == "5") 
                                    eptDetails = "1,2,3,4,";
                                else
                                    eptDetails += e[i].value +  ",";
                                
                            }   
                           if(eptDetails != "")
                                confDetails += "15:" + eptDetails.substring(0, eptDetails.length-1) + "|";
                        }
                    }
                    if(cd[c].value == "5" || cd[c].value == "7")
                    {
                        if(lstMCUDetails.GetSelectedItems().length > 0)
                        {
                            var m = lstMCUDetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                            {
                                if(m[i].value == "4")
                                    mcuDetails = "1,2,3,";
                                else
                                    mcuDetails += m[i].value +  ",";
                            }   
                           if(mcuDetails != "")
                                confDetails += "16:" + mcuDetails.substring(0, mcuDetails.length-1) + "|";
                        }
                    }
                    if(cd[c].value == "6" || cd[c].value == "7")
                    {
                        if(lstWODetails.GetSelectedItems().length > 0)
                        {
                            var m = lstWODetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                            {                          
                                if(m[i].value == "4")
                                    woDetails =  "1,2,3,";
                                else
                                    woDetails += m[i].value +  ",";
                            }
                                
                           if(woDetails != "")
                                confDetails += "17:" + woDetails.substring(0, woDetails.length-1) + "|";
                        }
                    }
                    
                }
                
                if(confDetails != "")
                    hdnMainIPValue.value += confDetails;
            }
           
            if(subConfSelect != "")
                hdnMainIPValue.value += "14:" + subConfSelect.substring(0, subConfSelect.length-1) + "|";
        }
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
    }
</script>

<%--Get Input Values from CDR Menu--%>
<script type="text/javascript">
    
    function fnGetCDRMenuInput()
    {
    
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        var hdnOkValue = document.getElementById("hdnOkValue");
        var hdnCDROption = document.getElementById("hdnCDROption");
        
        hdnOkValue.value = "1";
        
        if(rbConferenceCDR.GetChecked())
        {
            commonSelect += "C,";
            hdnCDROption.value = "C";
        }
        else
        {
            commonSelect += "V,";
            hdnCDROption.value = "V";
        }   
        if(commonSelect != "")
            hdnMainIPValue.value = "22:" + commonSelect.substring(0, commonSelect.length-1) + "|";
        //FB 2944    
		var conference = "";
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        if(OrgList.GetSelectedItems().length >0)
        {
            var s = OrgList.GetSelectedItems()
            
            var departments = "";
            hdnRightMenuValue.value = "1:" + s[0].value + "|";
        }
        if(hdnRightMenuValue.value != "")
            conference += "right,"
            
        if(conference != "")
            hdnMainIPValue.value += "1:" + conference.substring(0, conference.length-1) + "|";
        var mcu = "";
        if(lstMCU.GetSelectedItems().length > 0)
        {
            var m = lstMCU.GetSelectedItems();
            for(i = 0; i < m.length; i++)
            {
                if(m[i].value == "0")
                {
                    var lstCnt = lstMCU.GetItemCount();  
                    mcu = "";                          
                    for (var lc = 0; lc < lstCnt; lc++)                             
                        mcu += lstMCU.GetItem(lc).value + ",";
                }
                else if(m[i].value == "-1")
                    mcu = "";
                else
                    mcu += m[i].value +  ",";
            }
        }
        
        if(mcu != "")
            hdnMainIPValue.value += "24:" + mcu.substring(0, mcu.length-1) + "|";   
    }

    // ZD 102835 Start
    function fnGetWorkOrderMenuInput() {
        var workorder = "";
        var personnel = "";
        var hosts = "";
        var scheduledConf = "";
        var confTypes = "";
        var confBehavior = "";
        var confTime = "";
        var duraion = "";

        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        hdnMainIPValue.value = "";

        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";

        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        hdnRightMenuValue.value = "";

        // NEW CASE
        if (chk_woav.GetChecked()) {
            workorder = "1,";
        }
        if (chk_wocater.GetChecked()) {
            workorder += "2,";
        }
        if (chk_wofacility.GetChecked()) {
            workorder += "3,";
        }
        if (chk_woallwork.GetChecked()) {
            workorder = "1,2,3,";
        }
        if (workorder != "")
            hdnMainIPValue.value += "28:" + workorder.substring(0, workorder.length - 1) + "|";

        // NEW CASE
        if (chk_woadminincharge.GetChecked()) {
            personnel = "1,";
        }
        if (chk_wopersonincharge.GetChecked()) {
            personnel += "2,";
        }
        if (chk_woallpersonnel.GetChecked()) {
            personnel += "3,";
        }
        if (personnel != "")
            hdnMainIPValue.value += "29:" + personnel.substring(0, personnel.length - 1) + "|";

        // Rooms - Rooms / location
        var departments = ""
        if (chk_wodept.GetChecked()) {
            departments = "d,"; // Department
        }

        var tierInfo = "";
        if (chk_wotiers.GetChecked()) {
            /*
            if (lstTiers.GetSelectedItems().length > 0) {
                var ti = lstTiers.GetSelectedItems()
                for (i = 0; i < ti.length; i++) {
                    if (ti[i].value == "1") tierInfo = " (select [Name] from Loc_Tier3_D l3 where l3.id = r.l3LocationID) as [Top Tier], ";
                    if (ti[i].value == "2") tierInfo += " (select [Name] from Loc_Tier2_D l2 where l2.id = r.l2LocationID) as [Middle Tier], ";

                    if (ti[i].value == "3") {
                        tierInfo = " (select [Name] from Loc_Tier3_D l3 where l3.id = r.l3LocationID) as [Top Tier], ";
                        tierInfo += "(select [Name] from Loc_Tier2_D l2 where l2.id = r.l2LocationID) as [Middle Tier],";
                    }
                }
            }
            */
            tierInfo = " (select [Name] from Loc_Tier3_D l3 where l3.id = r.l3LocationID) as [Top Tier], ";
            tierInfo += "(select [Name] from Loc_Tier2_D l2 where l2.id = r.l2LocationID) as [Middle Tier],";
        }
        if (tierInfo != "")
            hdnMainIPValue.value += "20:" + tierInfo.substring(0, tierInfo.length - 1) + "|"; 

        var rminfo = "";
        var commonSelect = "";
        if (chk_woroomname.GetChecked()) {
            /*
            if (lstRmInfo.GetSelectedItems().length > 0) {
                var l = lstRmInfo.GetSelectedItems()
                for (i = 0; i < l.length; i++) {
                    if (l[i].value == "1") commonSelect = " RoomPhone,";
                    if (l[i].value == "2") commonSelect += " Capacity,";
                    if (l[i].value == "3") commonSelect += " Address1,";
                    if (l[i].value == "4") commonSelect += " Address2,";
                    if (l[i].value == "5") commonSelect += " RoomFloor,";
                    if (l[i].value == "6") commonSelect += " RoomNumber,";
                    if (l[i].value == "7") commonSelect += " City,";
                    if (l[i].value == "8") commonSelect += " State,";
                    if (l[i].value == "9") commonSelect += " Country,";
                    if (l[i].value == "10") commonSelect += " ZipCode,";
                    if (l[i].value == "11") commonSelect += " TimeZone,";
                    if (l[i].value == "12") commonSelect += " [Approver 1],";
                    if (l[i].value == "13") commonSelect += " [Approver 2],";
                    if (l[i].value == "14") commonSelect += " [Approver 3],";

                    if (l[i].value == "15") {
                        commonSelect = "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                        commonSelect += ",[Approver 2],[Approver 3],";
                    }
                }
            }
            */
            commonSelect = "r.Name [Room Name],";
        }
        if (commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length - 1) + "|";

        if (chk_wohost.GetChecked()) {
            departments += "h,"; // Host (Room Host)
        }

        if (departments != "")
            hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length - 1) + "|";     

        // NEW CASE
        var conference = "";
        if (chk_woconfid.GetChecked()) {
            conference = "1,";
        }
        if (chk_woconftitle.GetChecked()) {
            conference += "2,";
        }
        if (chk_woorg.GetChecked()) {
            conference += "3,";
        }
        if (hdnRightMenuValue.value != "")
            conference += "right,"

        if (conference != "")
            hdnMainIPValue.value += "1:" + conference.substring(0, conference.length - 1) + "|";

        // Hosts - Conferences / Hosts
        if (chk_wofirstname.GetChecked()) {
            hosts += "h.FirstName as [Host First Name],";
        }
        if (chk_wolastname.GetChecked()) {
            hosts += "h.LastName as [Host Last Name],";
        }
        if (chk_woemail.GetChecked()) {
            hosts += "h.Email as [Host Email],";
        }
        if (chk_worole.GetChecked()) {
            hosts += "hr.roleName as [Host Role],";
        }
        //if (chk_woperson.GetChecked()) {
        //}

        if (hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length - 1) + "|";


        // Scheduled conference - Conferences / Scheduled conference
        if (chk_wocompleted.GetChecked()) {
            scheduledConf += "7,";
        }
        if (chk_woscheduled.GetChecked()) {
            scheduledConf += "0,";
        }
        if (chk_wodeleted.GetChecked()) {
            scheduledConf += "9,";
        }
        if (chk_woterminated.GetChecked()) {
            scheduledConf += "3,";
        }
        if (chk_wopending.GetChecked()) {
            scheduledConf += "1,";
        }
        if (scheduledConf != "")
            hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length - 1) + "|";


        // Conference types - Conferences / Conference types
        if (chk_woaudioonly.GetChecked()) {
            confTypes += "6,";
        }
        if (chk_wovideoonly.GetChecked()) {
            confTypes += "2,";
        }
        if (chk_woroomonly.GetChecked()) {
            confTypes += "7,";
        }
        if (chk_wopointtopoint.GetChecked()) {
            confTypes += "4,";
        }
        //if (chk_wotelepresence.GetChecked()) {
        //}
        if (confTypes != "")
            hdnMainIPValue.value += "5:" + confTypes.substring(0, confTypes.length - 1) + "|";


        // Conference behaviour - Conferences / Conference behavious
        if (chk_wosingle.GetChecked()) {
            confBehavior += "0,";
        }
        if (chk_worecur.GetChecked()) {
            confBehavior += "1,";
        }
        if (confBehavior != "")
            hdnMainIPValue.value += "6:" + confBehavior.substring(0, confBehavior.length - 1) + "|";


        // Conference time - Conferences / Conference time
        if (chk_wodate.GetChecked()) {
            confTime += "ConfDate as [Date of Conf.],";
        }
        if (chk_wotime.GetChecked()) {
            confTime += "ConfTime [Start Time],";
        }
        if (confTime != "")
            hdnMainIPValue.value += "7:" + confTime.substring(0, confTime.length - 1) + "|";


        // Duration - Conferences / duration
        if (chk_wominutes.GetChecked()) {
            duraion += "a.Duration as [Duration (Mins)],";
        }
        if (duraion != "")
            hdnMainIPValue.value += "8:" + duraion.substring(0, duraion.length - 1);


    }
    // ZD 102835 End

</script>

<%--Save funtion--%>
<script  type="text/javascript">

function fnSave()
{

    //var errLabel = document.getElementById('errLabel');

    if (txtSaveReport.GetValue().trim() == ReportName || txtSaveReport.GetValue().trim() == "") //ZD 101022
    {
        alert(MCReportValidName);
//        if(errLabel)
//        {
//            errLabel.innerText = "Please enter the valid Report Name.";
//            errLabel.style.display = "";
//        }
        return false;
    }
    else
    {
        var hdnSave = document.getElementById("hdnSave");
        var hdnRptSave = document.getElementById("hdnRptSave");
        
        hdnSave.value = "1";
        fnSubmit();
        fnClose();
        if(hdnRptSave)
            hdnRptSave.value = "1";
        DataLoading(1); //ZD 100176
        return true;
        //cmdConfOk.DoClick();
    }
}
       

function OntextFocus(s, e) 
{
    var ele = s.GetInputElement();
    if (ele.value == ReportName)
		ele.value = "";
	else if(ele.value.trim() == "")
	    ele.value = ReportName;
}

function OntextunFocus(s, e) 
{
    var ele = s.GetInputElement();
	
	if(ele.value.trim() == "")
	    ele.value = ReportName;
}

function fnValidation()
{
    var sDate = startDateedit.GetDate();
    var eDate = endDateedit.GetDate();
    
    if(sDate == null || eDate == null)
    {
        var isExport = confirm(MCReportTime)
        
        if(isExport == false)
            return false;
        else
            return true;
    }
    
    return true;
}

//FB 2808
function fnClearRptValue()
{
    var hdnReportSelection = document.getElementById("hdnReportSelection");
    
    SavedRptListPopup.Hide()
    hdnReportSelection.value = "";
    
    fnClearListSelection(lstReportList);
}

//FB 2808
function fnClearErrlbl()
{
    var errLabel =  document.getElementById("errLabel");
    if(errLabel)
    {
        errLabel.innerText = "";
        errLabel.innerHTML = "";
    }
}
//FB 2801 Start
function fndaterange() 
{
    if(rbAdhoc.GetChecked())
    {
        var sDate = startDateedit.GetDate();
        var eDate = endDateedit.GetDate();

        if (sDate == null || eDate == null) 
        {
            alert(MCReportDaterange)
            return false;
        }
        DataLoading(1); //ZD 100176
    }
    return true;
}
//FB 2801 End

    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
    <title>Report</title>    
    <script type="text/javascript" src="inc/functions.js"></script>
    <%--FB 2886--%>
    <style type="text/css">   
    LABEL 
    {
        FONT-SIZE: 8pt;
        VERTICAL-ALIGN: top;
        COLOR: black;
        FONT-FAMILY: Arial, Helvetica;
        TEXT-DECORATION: none;
    }    
    .altLongSmallButtonFormat
    {
    	width:132pt;
    }
    .altLongSmallButtonFormat:active
    {
    	width:132pt;
    }
    .altLongSmallButtonFormat:hover
    {
    	width:132pt;
    }
    .altBlueButtonFormat
    {
    	width:40pt;
    }
    .altBlueButtonFormat:active
    {
    	width:40pt;
    }
    .altBlueButtonFormat:hover
    {
    	width:40pt;
    }
    .btndisable
    {
    	width:40pt;
    }
    /* ZD 100856 Starts */
    #MainGrid_DXHeadersRow th
    {
        cursor:move;
    }
    #MainGrid_DXHeadersRow img
    {
        cursor:pointer;
    }
    #MainGrid_DXTitle + div th
    {
        cursor:move;
    }
    #MainGrid_DXTitle + div img
    {
        cursor:pointer;
    }
    /* ZD 100856 Ends */
    
    /* ZD 102835 Start */
    label[for=MainPopup_chk_wopointtopoint_I]
    {
        white-space: nowrap;
    }
    /* ZD 102835 End */
    </style>
</head>
<body>
    <form id="frmReport" runat="server">   
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" id="hdnMainIPValue" runat="server" />
    <input type="hidden" id="hdnDepartmentList" runat="server" />
    <input type="hidden" id="hdnRightMenuValue" runat="server" />
    <input type="hidden" id="hdnOkValue" runat="server" />
    <input type="hidden" id="hdnSubmitValue" runat="server" />
    <input type="hidden" id="hdnSave" runat="server" />
    <input type="hidden" id="hdnReportNames" runat="server" />
    <input type="hidden" id="hdnAllReports" runat="server" />
    <input type="hidden" id="hdnAdmin" runat="server" /> <%--FB 2593--%>
    <input type="hidden" id="hdnMCUList" runat="server" />
    <input type="hidden" id="hdnEptList" runat="server" />
    <input type="hidden" id="hdnRoomList" runat="server" />
    <input type="hidden" id="hdnRptSave" runat="server" /> <%--FB 2593--%>
    <input type="hidden" id="hdnCDROption" runat="server" /> <%--FB 2593--%>
    <input type="hidden" id="hdnReportSelection" runat="server" /><%--FB 2808--%>
	<input type="hidden" id="hdnGroup" runat="server" /><%--FB 2965--%>
    <input type="hidden" id="hdnCustomOptions" runat="server" /> <%--ZD 104686--%>
    <%--FB 2886 Start--%>
	<table width="100%" >
    <tr>
            <td colspan="2"  align="center" height="10px" >
                <h3> 
                      <asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:WebResources, ReportDetails_btnMCCRpt%>"></asp:Label><!-- FB 2570 -->
                </h3>                
            </td>
        </tr>
    </table>
    <div id="dataLoadingDIV"  style="display:none" align="center" >
        <img border='0' src='image/wait1.gif'  alt='Loading..' />
    </div> <%--ZD 100678 End--%>
    <table width="100%" border="1" style="border-color:Gray;border-style:solid;" cellpadding="0" cellspacing="0">
        
        <tr>
            <td colspan="2" >
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError" ></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <table width="100%" border="0" style="left: 0;" id="tbleBtns">
                                <tr>
                                    <td width="10%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, MasterChildReport_ReportType%>" runat="server"></asp:Literal><%--ZD 100423--%>
                                         <dx:ASPxRadioButton ID="rbAdhoc" runat="server" TabIndex="0" Text="<%$ Resources:WebResources, Adhoc%>" GroupName="ReportType" Checked="True" TextWrap="False" ClientInstanceName="rbAdhoc">                                            
                                            <ClientSideEvents CheckedChanged="function(s, e){fnClearRptValue();btnPreview.style.display='';}" /><%--FB 2808 FB 2886--%>
                                        </dx:ASPxRadioButton>  
                                    </td>
                                    <td width="8%" class="blackblodtext">&nbsp;
                                        <dx:ASPxRadioButton ID="rbSaved" runat="server" Text="<%$ Resources:WebResources, Saved%>" GroupName="ReportType"  TextWrap="False" ClientInstanceName="rbSaved">   
                                            <ClientSideEvents CheckedChanged="function(s, e){fnClearErrlbl(); btnPreview.style.display='None';}" /> <%--FB 2808 FB 2886--%>
                                        </dx:ASPxRadioButton> 
                                    </td>
                                    <td width="10%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, MasterChildReport_StartDate%>" runat="server"></asp:Literal>
                                       <dx:ASPxDateEdit ID="startDateedit" ClientInstanceName="startDateedit" runat="server" Width="100px">                                        
										<%--ZD 100423--%>
                                        <ClientSideEvents KeyDown="function(s, e) {
                                                if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                                                    s.HideDropDown();
                                            }" GotFocus="function(s, e) {
                                                s.ShowDropDown();
                                            }" />
                                       </dx:ASPxDateEdit>
                                    </td>
                                    <td width="15%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, MasterChildReport_EndDate%>" runat="server"></asp:Literal>
                                       <dx:ASPxDateEdit ID="endDateedit" ClientInstanceName="endDateedit" runat="server" Width="100px">                                       
										<%--ZD 100423--%>
                                          <ClientSideEvents KeyDown="function(s, e) {
                                                if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                                                    s.HideDropDown();
                                            }" GotFocus="function(s, e) {
                                                s.ShowDropDown();
                                            }" />
                                           </dx:ASPxDateEdit>
                                    </td>	
                                     <td width="10%" class="blackblodtext" valign="middle"> <%--FB 2886--%><br />
                                       <asp:Button ID="btnPreview" runat="server" CssClass="altShortBlueButtonFormat"  OnClick="btnOk_Click" 
                                        OnClientClick="javascript:return fndaterange();fnCheck();fnSubmit();" Text="<%$ Resources:WebResources, MasterChildReport_btnPreview%>" />         
                                    </td>							                                    
                                    <td align="right">
                                        <dx:ASPxMenu ID="menuExport" runat="server" AutoSeparators="RootOnly" SeparatorHeight="100%" SeparatorWidth="3px"
                                            CssClass="altLong1BlueButtonFormat"  ItemSpacing="2px" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true" 
                                             OnItemClick="menuExport_OnItemClick" AccessibilityCompliant="true"><%--ZD 100423--%>
                                            <Items>
                                                <dx:MenuItem Text="XLS" Name="mXls" ToolTip="<%$ Resources:WebResources, ExporttoExcel%>"><%-- FB 3034--%>
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/Ntable-excel.png" AlternateText="Excel" /> <%--ZD 100419--%>
                                                </dx:MenuItem>
                                                <dx:MenuItem Text="PDF" Name="mPdf" ToolTip="<%$ Resources:WebResources, ManageConference_btnPDF%>"><%-- FB 3034--%>
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/NPDFpage_white_acrobat.png" AlternateText="PDFpage_white_acrobat "  /><%--ZD 100419--%>
                                                </dx:MenuItem>
                                                <dx:MenuItem Text="RTF" Name="mDoc"  ToolTip="<%$ Resources:WebResources, ExporttoWord%>"><%-- FB 3034--%>
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/Nwordpage_white_word.png"  AlternateText="WordPage"/> <%--ZD 100419--%>
                                                </dx:MenuItem>                                               
                                            </Items>
                                            <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                            <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                            <SubMenuStyle GutterWidth="0px" />
                                            <ClientSideEvents ItemClick="function(s, e) {e.processOnServer = fnValidation();} " />
                                        </dx:ASPxMenu>
                                        <span class="lblError"><asp:Literal Text="<%$ Resources:WebResources, MasterChildReport_PleaseclickVi%>" runat="server"></asp:Literal></span>
                                    </td>
                                </tr>
                             </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="10%" style="border-right-color:Gray;" valign="top"> <%--FB 2050 FB 2886--%>
                 <table id="tblLeft" style="padding-right:5px" cellpadding="1" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>           
                            <ajax:ModalPopupExtender ID="ConferencePopUp" runat="server" TargetControlID="ChgOrg" BackgroundCssClass="modalBackground"
                                 PopupControlID="ConferencePnl" DropShadow="false" Drag="true" CancelControlID="ClosePUp">
                            </ajax:ModalPopupExtender>                
                            <asp:Panel ID="ConferencePnl" runat="server" HorizontalAlign="Center" style="width:100%;height:100%; overflow:auto;"><%--FB 2654--%>
                                <table width="100%">
                                    <tr>
                                        <td><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="MainPopup" runat="server" HeaderStyle-CssClass = "altLongSmallButtonFormat"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold = "true"
                                                HeaderText="Conference Selection" 
                                                 PopupHorizontalAlign="WindowCenter"  HeaderStyle-HorizontalAlign="Center"
                                                PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="True" ClientInstanceName="MainPopup"
                                                CloseAction="None" ShowCloseButton="False">                                                
                                                                                               
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"> <%--FB 2654--%>
                                                            <tr>
                                                                <td colspan="3">
                                                                   <span style="color:Red;font-size:smaller;"><asp:Literal Text="<%$ Resources:WebResources, MasterChildReport_Submenumultipl%>" runat="server"></asp:Literal></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="tblConference" style="display:none;" >
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >
                                                                                <dx:ASPxLabel ID="ASPxLabel13" runat="server"  Text="<%$ Resources:WebResources, Conference%>" ForeColor="Black"></dx:ASPxLabel>:
                                                                            </td>
                                                                            <td valign="top" align="right" ><%--FB 2886--%>
                                                                                <asp:Button ID="btnCClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="<%$ Resources:WebResources, MasterChildReport_btnCClear%>" />                                                                                 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left"  colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_ConfID" runat="server" ClientInstanceName="chk_ConfID" Text="<%$ Resources:WebResources, ReportDetails_ConferenceID%>" Font-Size="5px">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfTitle" runat="server" ClientInstanceName="chk_ConfTitle" Text="<%$ Resources:WebResources, ReportDetails_ConferenceTitl%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <%--FB 2870 Start--%>
                                                                                <dx:ASPxCheckBox ID="chk_CTSNumericID" runat="server" ClientInstanceName="chk_CTSNumericID" Text="<%$ Resources:WebResources, mainadministrator_CTSNumericID%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <%--FB 2870 End--%>
                                                                                <dx:ASPxCheckBox ID="chk_Org" runat="server" ClientInstanceName="chk_Org" Text="<%$ Resources:WebResources, ReportDetails_Organization%>">
                                                                                    <ClientSideEvents CheckedChanged="OnShowButtonClick" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Text=" <%$ Resources:WebResources, Hosts:%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                  
                                                                                <dx:ASPxCheckBox ID="chk_HFN" runat="server" ClientInstanceName="chk_HFN" Text="<%$ Resources:WebResources, Allocation_FirstName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HLN" runat="server" ClientInstanceName="chk_HLN" Text="<%$ Resources:WebResources, Allocation_LastName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HMail" runat="server" ClientInstanceName="chk_HMail" Text="<%$ Resources:WebResources, Email%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>                                                                     
                                                                                <dx:ASPxCheckBox ID="chk_HRole" runat="server" ClientInstanceName="chk_HRole" Text="<%$ Resources:WebResources, Role%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HPerson" runat="server" ClientInstanceName="chk_HPerson" Text="<%$ Resources:WebResources, Person%>" Enabled="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllHost" runat="server" ClientInstanceName="chk_AllHost" Text="<%$ Resources:WebResources, All%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnHostAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Bold="True" Text="  <%$ Resources:WebResources, Participants:%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                
                                                                                <dx:ASPxCheckBox ID="chk_PFN" runat="server" ClientInstanceName="chk_PFN" Text="<%$ Resources:WebResources, FirstName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PLN" runat="server" ClientInstanceName="chk_PLN" Text="<%$ Resources:WebResources, LastName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PMail" runat="server" ClientInstanceName="chk_PMail" Text="<%$ Resources:WebResources, Email%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td align="left">  <%--ZD 102131  --%>                                                                  
                                                                                <dx:ASPxCheckBox ID="chk_PRole" runat="server" ClientInstanceName="chk_PRole" Text="<%$ Resources:WebResources, AttendedConference%>" > <%--ZD 102131--%>
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PPerson" runat="server" ClientInstanceName="chk_PPerson" Text="<%$ Resources:WebResources, Person%>"  Enabled="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllPerson" runat="server" ClientInstanceName="chk_AllPerson" Text="<%$ Resources:WebResources, All%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnPersonAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Bold="True" Text="  <%$ Resources:WebResources, ScheduledConferences:%>"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" >                                                                
                                                                                <dx:ASPxCheckBox ID="chk_Completed" runat="server" ClientInstanceName="chk_Completed" Text="<%$ Resources:WebResources, Completed%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Scheduled" runat="server" ClientInstanceName="chk_Scheduled" Text="<%$ Resources:WebResources, Scheduled%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Deleted" runat="server" ClientInstanceName="chk_Deleted" Text="<%$ Resources:WebResources, Deleted%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Terminated" runat="server" ClientInstanceName="chk_Terminated" Text="<%$ Resources:WebResources, Terminated%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Pending" runat="server" ClientInstanceName="chk_Pending" Text="<%$ Resources:WebResources, Pending%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllConf" runat="server" ClientInstanceName="chk_AllConf" Text="<%$ Resources:WebResources, All%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnScheduledAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Bold="True" Text="  <%$ Resources:WebResources, ConferenceTypes:%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                    
                                                                                <dx:ASPxCheckBox ID="chk_Audio" runat="server" ClientInstanceName="chk_Audio" Text="<%$ Resources:WebResources, AudioOnly%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Video" runat="server" ClientInstanceName="chk_Video" Text="<%$ Resources:WebResources, VideoOnly%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Room" runat="server" ClientInstanceName="chk_Room" Text="<%$ Resources:WebResources, RoomOnly%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td align="left"> <%--ZD 102131 start--%>
                                                                                 <dx:ASPxCheckBox ID="chk_PP" runat="server" ClientInstanceName="chk_PP" Text="<%$ Resources:WebResources, PointtoPoint%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TP" runat="server" ClientInstanceName="chk_TP" Text="<%$ Resources:WebResources, EditEndpoint_TelePresence%>" Enabled="False">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllConfTypes" runat="server" ClientInstanceName="chk_AllConfTypes" Text="<%$ Resources:WebResources, All%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnConfTypeAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr> <%--ZD 101950--%>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Bold="True" Text="  <%$ Resources:WebResources, VMR%>" ForeColor="Black">
                                                                                </dx:ASPxLabel><b>:</b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                    
                                                                                <dx:ASPxCheckBox ID="chk_Personal" runat="server" ClientInstanceName="chk_Personal" Text="<%$ Resources:WebResources, ConferenceSetup_Personal%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllVMR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_RoomVMR" runat="server" ClientInstanceName="chk_RoomVMR" Text="<%$ Resources:WebResources, ConferenceSetup_Room%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllVMR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                 <dx:ASPxCheckBox ID="chk_External" runat="server" ClientInstanceName="chk_External" Text="<%$ Resources:WebResources, ConferenceSetup_External%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllVMR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllVMR" runat="server" ClientInstanceName="chk_AllVMR" Text="<%$ Resources:WebResources, All%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnVMRAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Bold="True" Text="  <%$ Resources:WebResources, ConferencesBehavior%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >                                                                   
                                                                                <dx:ASPxCheckBox ID="chk_Single" runat="server" ClientInstanceName="chk_Single" Text="<%$ Resources:WebResources, Single%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllMode.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Recurring" runat="server" ClientInstanceName="chk_Recurring" Text="<%$ Resources:WebResources, Recurring%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllMode.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <dx:ASPxCheckBox ID="chk_AllMode" runat="server" ClientInstanceName="chk_AllMode" Text="<%$ Resources:WebResources, All%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnRecurAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabecon" runat="server" Font-Bold="True" Text="  <%$ Resources:WebResources, ConferenceSupport1%>" ForeColor="Black"><%--FB 3023--%>
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">      <%--FB 2654--%>                                                              
                                                                                <dx:ASPxCheckBox ID="chk_Onsite" runat="server" ClientInstanceName="chk_Onsite" Text="<%$ Resources:WebResources, ManageConference_OnSiteAVSuppo%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConcierge.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Meet" runat="server" ClientInstanceName="chk_Meet" Text="<%$ Resources:WebResources, ManageConference_MeetandGreet%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConcierge.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Monitor" runat="server" ClientInstanceName="chk_Monitor" Text="<%$ Resources:WebResources, ExpressConference_CallMonitoring%>"> <%--FB 3023--%>
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConcierge.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_VNOC" runat="server" ClientInstanceName="chk_VNOC" Text="<%$ Resources:WebResources, ManageConference_DedicatedVNOC%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConcierge.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllConcierge" runat="server" ClientInstanceName="chk_AllConcierge" Text="<%$ Resources:WebResources, All%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnConceirgeAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <%--ZD 104686 - Start--%>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Bold="True" Text="  <%$ Resources:WebResources, ConferenceSetup_CustomOptions%>" ForeColor="Black"><%--FB 3023--%>
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top" runat="server" id="trCustomOptions">
                                                                            <td align="left" colspan="2"  runat="server" id="tdCustomOptions"> 
                                                                            </td>
                                                                        </tr>
                                                                        <%--ZD 104686 - End--%>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Bold="True" Text="  <%$ Resources:WebResources, ConferencesTime%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>      
                                                                                <dx:ASPxCheckBox ID="chk_Date" runat="server" ClientInstanceName="chk_Date" Text="<%$ Resources:WebResources, Date%>">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Time" runat="server" ClientInstanceName="chk_Time" Text="<%$ Resources:WebResources, Time%>">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Bold="True" ClientInstanceName="ASPxLabel8" Text=" <%$ Resources:WebResources, ConferenceList_Duration%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Minutes" runat="server" ClientInstanceName="chk_Minutes" Text="<%$ Resources:WebResources, Minutes%>">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>                                                                   
                                                                                <dx:ASPxCheckBox ID="chk_Hours" runat="server" ClientInstanceName="chk_Hours" Text="<%$ Resources:WebResources, Hours%>" Visible="False"> <%--FB 2654--%>
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblUser" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left">
                                                                                <dx:ASPxLabel ID="ASPxLabel14" runat="server"  Text="  <%$ Resources:WebResources, Conference%>"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                             <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnUClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="<%$ Resources:WebResources, MasterChildReport_btnCClear%>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" colspan="2">   
                                                                                <dx:ASPxCheckBox ID="chk_OrgU" runat="server" ClientInstanceName="chk_OrgU" Text="<%$ Resources:WebResources, Organization%>" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupU');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptU" runat="server" ClientInstanceName="chk_DeptU" Text="<%$ Resources:WebResources, Department%>">                                                                                    
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_FNU" runat="server" ClientInstanceName="chk_FNU" Text="<%$ Resources:WebResources, FirstName%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_LNU" runat="server" ClientInstanceName="chk_LNU" Text="<%$ Resources:WebResources, LastName%>">
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_PersonU" runat="server" ClientInstanceName="chk_PersonU" Text="<%$ Resources:WebResources, Person%>" Enabled="false">
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxCheckBox ID="chk_UserEmailU" runat="server" ClientInstanceName="chk_UserEmailU" Text="<%$ Resources:WebResources, ManageUserProfile_UserEmail%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SecondaryEmailU" runat="server" ClientInstanceName="chk_SecondaryEmailU" Text="<%$ Resources:WebResources, ManageUserProfile_tdSecondaryEmail%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_RoleU" runat="server" ClientInstanceName="chk_RoleU" Text="<%$ Resources:WebResources, Role%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TimeZoneU" runat="server" ClientInstanceName="chk_TimeZoneU" Text="<%$ Resources:WebResources, TimeZone%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfHostU" runat="server" ClientInstanceName="chk_ConfHostU" Text="<%$ Resources:WebResources, ExpressConference_Field4%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfPartU" runat="server" ClientInstanceName="chk_ConfPartU" Text="<%$ Resources:WebResources, ConferenceParticipant%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxCheckBox ID="chk_AccExpirU" runat="server" ClientInstanceName="chk_AccExpirU" Text="<%$ Resources:WebResources, ManageUserProfile_AccountExpirat%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MinU" runat="server" ClientInstanceName="chk_MinU" Text="<%$ Resources:WebResources, Minutes%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ExchangeU" runat="server" ClientInstanceName="chk_ExchangeU" Text="<%$ Resources:WebResources, Exchange%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DominoU" runat="server" ClientInstanceName="chk_DominoU" Text="<%$ Resources:WebResources, Domino%>">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>    
                                                             <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblRoom" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel15" runat="server"  Text="<%$ Resources:WebResources, ConferenceSetup_Location%>"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnRClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="<%$ Resources:WebResources, MasterChildReport_btnCClear%>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_OrgR" runat="server" ClientInstanceName="chk_OrgR" Text="<%$ Resources:WebResources, Organization%>" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupR');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptR" runat="server" ClientInstanceName="chk_DeptR" Text="<%$ Resources:WebResources, Department%>">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TiersR" runat="server" ClientInstanceName="chk_TiersR" Text="<%$ Resources:WebResources, UserMenuController_Tiers%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_NameR" runat="server" ClientInstanceName="chk_NameR" Text="<%$ Resources:WebResources, ManageRoom_RoomName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('rmInfoPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_HostR" runat="server" ClientInstanceName="chk_HostR" Text="<%$ Resources:WebResources, ReportDetails_Host%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxLabel ID="ASPxLabel16" runat="server"  Text="  <%$ Resources:WebResources, RoomType%>"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                                <dx:ASPxCheckBox ID="chk_VideoRoomR" runat="server" ClientInstanceName="chk_VideoRoomR" Text="<%$ Resources:WebResources, VideoRoom%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AudioRoomR" runat="server" ClientInstanceName="chk_AudioRoomR" Text="<%$ Resources:WebResources, VideoRoom%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MeetingRoomR" runat="server" ClientInstanceName="chk_MeetingRoomR" Text="<%$ Resources:WebResources, MeetingRoom%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllTypeR" runat="server" ClientInstanceName="chk_AllTypeR" Text="<%$ Resources:WebResources, AllType%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnRmTypeAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxLabel ID="ASPxLabel17" runat="server"  Text="  <%$ Resources:WebResources, Behavior%>"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                                <dx:ASPxCheckBox ID="chk_VideoConferenceR" runat="server" ClientInstanceName="chk_VideoConferenceR" Text="<%$ Resources:WebResources, VideoConference%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AudioConferenceR" runat="server" ClientInstanceName="chk_AudioConferenceR" Text="<%$ Resources:WebResources, AudioConference%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_RoomConferenceR" runat="server" ClientInstanceName="chk_RoomConferenceR" Text="<%$ Resources:WebResources, RoomCalendar_spnRmHrg%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllBehaviorR" runat="server" ClientInstanceName="chk_AllBehaviorR" Text="<%$ Resources:WebResources, AllTypes%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxLabel ID="ASPxLabel18" runat="server"  Text="  <%$ Resources:WebResources, Other%>"
                                                                                    ForeColor="Black" Visible="false" >
                                                                                </dx:ASPxLabel>:
                                                                                <dx:ASPxCheckBox ID="chk_UserAssignmentR" runat="server" ClientInstanceName="chk_UserAssignmentR" Text="<%$ Resources:WebResources, UserAssignment%>" Visible="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignment');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MaximumCapacityR" runat="server" ClientInstanceName="chk_MaximumCapacityR" Text="<%$ Resources:WebResources, ManageRoomProfile_MaximumCapacit%>" Visible="false">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AssetsR" runat="server" ClientInstanceName="chk_AssetsR" Text="<%$ Resources:WebResources, Assets%>" Visible="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('rmAssetPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ScheduledDurationR" runat="server" Visible="false" ClientInstanceName="chk_ScheduledDurationR" Text="<%$ Resources:WebResources, ScheduledDuration%>">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>      
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblEndPoint" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel19" runat="server"  Text="  <%$ Resources:WebResources, EndPoint%>"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                            <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnEClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="<%$ Resources:WebResources, MasterChildReport_btnCClear%>" />
                                                                            </td>  
                                                                        </tr>                                                                        
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_OrgE" runat="server" ClientInstanceName="chk_OrgE" Text="<%$ Resources:WebResources, Organization%>" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupE');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptE" runat="server" Visible="false" ClientInstanceName="chk_DeptE" Text="<%$ Resources:WebResources, Department%>">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ENameE" runat="server" ClientInstanceName="chk_ENameE" Text="<%$ Resources:WebResources, AddTerminalEndpoint_EndpointName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecEptE" runat="server" ClientInstanceName="chk_SpecEptE" Text="<%$ Resources:WebResources, SpecificEndpoint%>" Enabled="false">
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_RmNameE" runat="server" ClientInstanceName="chk_RmNameE" Text="<%$ Resources:WebResources, ManageRoom_RoomName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecRoomE" runat="server" ClientInstanceName="chk_SpecRoomE" Text="<%$ Resources:WebResources, SpecificRoom%>" Enabled="false" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfE" runat="server" ClientInstanceName="chk_ConfE" Text="<%$ Resources:WebResources, BridgeDetails_Conferences%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td> 
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>  
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblMCU" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel20" runat="server"  Text="  <%$ Resources:WebResources, MCU%>"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>  :
                                                                            </td>
                                                                             <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnMClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="<%$ Resources:WebResources, MasterChildReport_btnCClear%>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">                                                                       
                                                                                <dx:ASPxCheckBox ID="chk_OrgM" runat="server" ClientInstanceName="chk_OrgM" Text="<%$ Resources:WebResources, Organization%>" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupM');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptM" runat="server" Visible="false" ClientInstanceName="chk_DeptM" Text="<%$ Resources:WebResources, Department%>">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MCUNameM" runat="server" ClientInstanceName="chk_MCUNameM" Text="<%$ Resources:WebResources, approvalstatus_MCUName%>">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecMCUM" runat="server" ClientInstanceName="chk_SpecMCUM" Text="<%$ Resources:WebResources, SpecificMCU%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');BindMCU();}" /> <%--FB 2593--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ENameM" runat="server" ClientInstanceName="chk_ENameM" Text="<%$ Resources:WebResources, AddTerminalEndpoint_EndpointName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecEptM" runat="server" ClientInstanceName="chk_SpecEptM" Text="<%$ Resources:WebResources, SpecificEndpoint%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');BindEndpointList();}" /> 
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_RmNameM" runat="server" ClientInstanceName="chk_RmNameM" Text="<%$ Resources:WebResources, ManageRoom_RoomName%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecRoomM" runat="server" ClientInstanceName="chk_SpecRoomM" Text="<%$ Resources:WebResources, SpecificRoom%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');BindRoomList();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfM" runat="server" ClientInstanceName="chk_ConfM" Text="<%$ Resources:WebResources, BridgeDetails_Conferences%>">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>      
                                                            <tr> <%--FB 2593--%>
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblCDR" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="lblCDR" runat="server"  Text="  <%$ Resources:WebResources, CDR%>"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                             <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnCDRClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="<%$ Resources:WebResources, MasterChildReport_btnCClear%>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">                                                                       
                                                                                <dx:ASPxRadioButton ID="rbConferenceCDR" runat="server" Text="<%$ Resources:WebResources, ConferenceCDR%>" GroupName="CDRType" Checked="True" TextWrap="False" ClientInstanceName="rbConferenceCDR">                                            
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){SavedRptListPopup.Hide();}" />
                                                                                </dx:ASPxRadioButton>  
                                                                                <dx:ASPxRadioButton ID="rbVMRCDR" runat="server" Text="<%$ Resources:WebResources, VirtualMeetingRoomCDR%>" GroupName="CDRType" TextWrap="False" ClientInstanceName="rbVMRCDR">  <%--ZD 100806--%>                                          
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){SavedRptListPopup.Hide();document.getElementById('hdnRptSave').value='1';}" /> <%--FB 2593--%>
                                                                                </dx:ASPxRadioButton>  
                                                                            </td>
                                                                            <td valign="right" align="center" style="display:none";>
                                                                                <dx:ASPxButton ID="btnCDRPoll" ClientInstanceName="btnCDRPoll" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                                    CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                                    Text="<%$ Resources:WebResources, SuperAdministrator_Poll%>" Width="52px" OnClick="PollReport" >
                                                                                  <%--  <ClientSideEvents Click="function(s, e) {e.processOnServer = fnSave();}" />--%>
                                                                                </dx:ASPxButton> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="25px"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            
                                                            <%-- ZD 102835 Start --%>
                                                            <tr>
                                                                <td align="left" colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblWO" style="display:none;" border="0" >
                                                                        
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="lblWO" runat="server"  Text="<%$ Resources:WebResources, WorkOrder%>
"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                             <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnWOclear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="<%$ Resources:WebResources, MasterChildReport_btnCClear%>" />
                                                                            </td>
                                                                        </tr>                                                                        

                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_woav" runat="server" ClientInstanceName="chk_woav" Text="<%$ Resources:WebResources, AV%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallwork.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wocater" runat="server" ClientInstanceName="chk_wocater" Text="<%$ Resources:WebResources, DefaultLicense_Catering%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallwork.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wofacility" runat="server" ClientInstanceName="chk_wofacility" Text="<%$ Resources:WebResources, Facilities%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallwork.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woallwork" runat="server" ClientInstanceName="chk_woallwork" Text="<%$ Resources:WebResources, All%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnwoallwork(chk_woallwork.GetChecked());}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxLabel ID="lbl_wopersonnel" runat="server" Font-Bold="True" ClientInstanceName="lbl_wopersonnel" Text="<%$ Resources:WebResources, Personnel%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_woadminincharge" runat="server" ClientInstanceName="chk_woadminincharge" Text="<%$ Resources:WebResources, EditInventory_Adminincharge%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallpersonnel.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wopersonincharge" runat="server" ClientInstanceName="chk_wopersonincharge" Text="<%$ Resources:WebResources, ViewWorkorderDetails_Personincharge%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallpersonnel.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woallpersonnel" runat="server" ClientInstanceName="chk_woallpersonnel" Text="<%$ Resources:WebResources, All%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnwoallpersonnel(chk_woallpersonnel.GetChecked());}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxLabel ID="lbl_worooms" runat="server" Font-Bold="True" ClientInstanceName="lbl_worooms" Text="<%$ Resources:WebResources, ConferenceList_lblRooms%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_wodept" runat="server" ClientInstanceName="chk_wodept" Text="<%$ Resources:WebResources, approvalstatus_Department%>" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wotiers" runat="server" ClientInstanceName="chk_wotiers" Text="<%$ Resources:WebResources, UserMenuController_Tiers%>" >
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woroomname" runat="server" ClientInstanceName="chk_woroomname" Text="<%$ Resources:WebResources, approvalstatus_RoomName%>" >
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('rmInfoPopup');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wohost" runat="server" ClientInstanceName="chk_wohost" Text="<%$ Resources:WebResources, DashBoard_Host%>" >
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxLabel ID="lbl_woconference" runat="server" Font-Bold="True" ClientInstanceName="lbl_woconference" Text="<%$ Resources:WebResources, Conference%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_woconfid" runat="server" ClientInstanceName="chk_woconfid" Text="<%$ Resources:WebResources, ReportDetails_ConferenceID%>" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woconftitle" runat="server" ClientInstanceName="chk_woconftitle" Text="<%$ Resources:WebResources, ReportDetails_ConferenceTitl%>" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woorg" runat="server" ClientInstanceName="chk_woorg" Text="<%$ Resources:WebResources, ReportDetails_Organization%>" >
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxLabel ID="lbl_wohosts" runat="server" Font-Bold="True" ClientInstanceName="lbl_wohosts" Text="<%$ Resources:WebResources, Hosts%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_wofirstname" runat="server" ClientInstanceName="chk_wofirstname" Text="<%$ Resources:WebResources, Allocation_FirstName%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallhost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wolastname" runat="server" ClientInstanceName="chk_wolastname" Text="<%$ Resources:WebResources, Allocation_LastName%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallhost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woemail" runat="server" ClientInstanceName="chk_woemail" Text="<%$ Resources:WebResources, Allocation_Email%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallhost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_worole" runat="server" ClientInstanceName="chk_worole" Text="<%$ Resources:WebResources, Role%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallhost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woperson" runat="server" ClientInstanceName="chk_woperson" Text="<%$ Resources:WebResources, Person%>" Enabled="false" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woallhost" runat="server" ClientInstanceName="chk_woallhost" Text="<%$ Resources:WebResources, All%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnwoallhost(chk_woallhost.GetChecked());}" />
                                                                                </dx:ASPxCheckBox>
                                                                            
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2" >
                                                                                <dx:ASPxLabel ID="lbl_woscheduled" runat="server" Font-Bold="True" ClientInstanceName="lbl_woscheduled" Text="<%$ Resources:WebResources, ScheduledConferences%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>



                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_wocompleted" runat="server" ClientInstanceName="chk_wocompleted" Text="<%$ Resources:WebResources, Completed%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallschedule.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woscheduled" runat="server" ClientInstanceName="chk_woscheduled" Text="<%$ Resources:WebResources, Scheduled%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallschedule.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wodeleted" runat="server" ClientInstanceName="chk_wodeleted" Text="<%$ Resources:WebResources, Deleted%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallschedule.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_woterminated" runat="server" ClientInstanceName="chk_woterminated" Text="<%$ Resources:WebResources, Terminated%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallschedule.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wopending" runat="server" ClientInstanceName="chk_wopending" Text="<%$ Resources:WebResources, Pending%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallschedule.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woallschedule" runat="server" ClientInstanceName="chk_woallschedule" Text="<%$ Resources:WebResources, All%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnwoallschedule(chk_woallschedule.GetChecked());}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>



                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxLabel ID="lbl_woconftype" runat="server" Font-Bold="True" ClientInstanceName="lbl_woconftype" Text="<%$ Resources:WebResources, ConferenceTypes%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>



                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_woaudioonly" runat="server" ClientInstanceName="chk_woaudioonly" Text="<%$ Resources:WebResources, AudioOnly%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallconftype.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wovideoonly" runat="server" ClientInstanceName="chk_wovideoonly" Text="<%$ Resources:WebResources, VideoOnly%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallconftype.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woroomonly" runat="server" ClientInstanceName="chk_woroomonly" Text="<%$ Resources:WebResources, RoomOnly%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallconftype.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_wopointtopoint" runat="server" ClientInstanceName="chk_wopointtopoint" Text="<%$ Resources:WebResources, PointtoPoint%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallconftype.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_wotelepresence" runat="server" ClientInstanceName="chk_wotelepresence" Text="<%$ Resources:WebResources, EditEndpoint_TelePresence%>" Enabled="false" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_woallconftype" runat="server" ClientInstanceName="chk_woallconftype" Text="<%$ Resources:WebResources, All%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnwoallconftype(chk_woallconftype.GetChecked());}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>



                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2" >
                                                                                <dx:ASPxLabel ID="lbl_woconfbehave" runat="server" Font-Bold="True" ClientInstanceName="lbl_woconfbehave" Text="<%$ Resources:WebResources, ConfBehavior%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_wosingle" runat="server" ClientInstanceName="chk_wosingle" Text="<%$ Resources:WebResources, Single%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallbehavior.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_worecur" runat="server" ClientInstanceName="chk_worecur" Text="<%$ Resources:WebResources, Recurring%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_woallbehavior.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td style="vertical-align:top" >
                                                                                <dx:ASPxCheckBox ID="chk_woallbehavior" runat="server" ClientInstanceName="chk_woallbehavior" Text="<%$ Resources:WebResources, All%>" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnwoallbehavior(chk_woallbehavior.GetChecked());}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>



                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <dx:ASPxLabel ID="lbl_woconftime" runat="server" Font-Bold="True" ClientInstanceName="lbl_woconftime" Text="<%$ Resources:WebResources, ConfTime%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_wodate" runat="server" ClientInstanceName="chk_wodate" Text="<%$ Resources:WebResources, DashBoard_Date%>" >
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_wotime" runat="server" ClientInstanceName="chk_wotime" Text="<%$ Resources:WebResources, EventLog_Time%>" >
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>



                                                                        <tr>
                                                                            <td colspan="2" >
                                                                                <dx:ASPxLabel ID="lbl_woduration" runat="server" Font-Bold="True" ClientInstanceName="lbl_woduration" Text="<%$ Resources:WebResources, ConferenceList_Duration%>" ForeColor="Black">
                                                                                </dx:ASPxLabel>:
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2" >
                                                                                <dx:ASPxCheckBox ID="chk_wominutes" runat="server" ClientInstanceName="chk_wominutes" Text="<%$ Resources:WebResources, GraphicalReport_Minutes%>" >
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>


                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <%-- ZD 102835 End --%>
                                                               
                                                            <tr>
                                                                <td align="left" colspan="3">
                                                                    <dx:ASPxTextBox ID="txtSaveReport" ClientInstanceName="txtSaveReport" runat="server" Width="180px" Text="<%$ Resources:WebResources, ReportName%>">
                                                                        <ClientSideEvents GotFocus="OntextFocus" LostFocus="OntextunFocus" />
                                                                    </dx:ASPxTextBox>                                                                   
                                                                </td>
                                                            </tr>                                                       
                                                            <tr valign="middle">
                                                                <td align="center"><%--FB 2886--%>
                                                                <asp:Button ID="btnClose" runat="server" CssClass="altBlueButtonFormat" UseSubmitBehavior="false" style="font-size:smaller;width:50px;"
                                                                    OnClientClick="javascript:return fnSubmit('1');" Text="<%$ Resources:WebResources, MasterChildReport_btnClose%>" />                                                                     
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <asp:Button ID="btnSave" runat="server" CssClass="altBlueButtonFormat"  OnClick="SaveReport" style="font-size:smaller;width:71px;"
                                                                    OnClientClick="javascript:return fnSave();" Text="<%$ Resources:WebResources, MasterChildReport_btnSave%>" />                                                                     
                                                                </td>
                                                                <td align="center">
                                                                    <asp:Button ID="cmdConfOk" runat="server" CssClass="altBlueButtonFormat" UseSubmitBehavior="false" style="font-size:smaller;width:51px;"
                                                                    OnClientClick="javascript:return fnSubmit('1');" Text="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" /> 
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <CloseButtonImage Height="14px" Width="14px" />
                                            </dx:ASPxPopupControl>
                                            <%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="pcPopup" runat="server" ClientInstanceName="popup" EncodeHtml="false"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White" Width="150px" 
                                                HeaderText="<%$ Resources:WebResources, Organization%>" CloseAction="CloseButton" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxListBox ID="OrgList" runat="server"  Width="100%" ClientInstanceName="OrgList">
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('dptPopup');}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>      
                                                         <dx:ASPxListBox ID="TempList" runat="server" Height="0px" Width="100%" Border-BorderStyle=None ClientInstanceName="TempList" > </dx:ASPxListBox>                                                 
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="8px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="dptPopup" runat="server" ClientInstanceName="dptPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, Department%>" Width="150px" CloseAction="CloseButton" HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                         <dx:ASPxListBox ID="lstDepartment" SelectionMode="Multiple"  runat="server"  Width="100%"  ClientInstanceName="lstDepartment">
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>                                                          
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="8px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="assignmentPopup" runat="server" ClientInstanceName="assignmentPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, Assignments%>" Width="150px" CloseAction="CloseButton" HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxMenu ID="ASPxMenu1" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, RoomCalendar_Rooms%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstRmApprover" runat="server" SelectionMode="Multiple" Height="100px" Width="98%" ClientInstanceName="lstRmApprover" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AssInCharge%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, BridgeDetails_PrimaryApprove%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, SecAppI%>" Value="3" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, SecAppII%>" Value="4" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                        <dx:ASPxMenu ID="ASPxMenu2" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, MCU%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstMCUApprover" runat="server" SelectionMode="Multiple" Height="100px" Width="98%" ClientInstanceName="lstMCUApprover" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Administrator%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, BridgeDetails_PrimaryApprove%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, SecAppI%>" Value="3" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, SecAppII%>" Value="4" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                        <dx:ASPxMenu ID="ASPxMenu3" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, WorkOrders%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstWOAdmin" runat="server" SelectionMode="Multiple" Height="100px" Width="98%" ClientInstanceName="lstWOAdmin" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ViewWorkorderDetails_Personincharge%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Staff%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="3" />                                                             
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="resourcePopup"  AllowDragging ="true" Height="250px" runat="server" ClientInstanceName="resourcePopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, Resources%>" Width="150px" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl BackColor="Red">
                                                        <dx:ASPxMenu ID="ASPxMenu10" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, Attendees%>" >
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstAttendee" runat="server" SelectionMode="Multiple" Height="60px" Width="100%" ClientInstanceName="lstAttendee" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Staff%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, UserMenuController_Guests%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                        
                                                        <dx:ASPxMenu ID="ASPxMenu4" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, ManageConference_Participants%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstRmType" runat="server" SelectionMode="Multiple" Height="60px" Width="100%" ClientInstanceName="lstRmType" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Internal%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ExpressConference_External%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                           
                                                        <dx:ASPxMenu ID="ASPxMenu5" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, DefaultLicense_EndPoints%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstEPYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstEPYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Yes%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, No%>" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                           
                                                        <dx:ASPxMenu ID="ASPxMenu6" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, ConferenceSetup_LblEPUsers%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstMCUYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstMCUYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Yes%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, No%>" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                        <dx:ASPxMenu ID="ASPxMenu7" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, ConferenceSpeed%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                       <dx:ASPxListBox ID="lstConfSpeedYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstConfSpeedYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Yes%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, No%>" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                        <dx:ASPxMenu ID="ASPxMenu8" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, ConferenceProtocol%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstConfProYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstConfProYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Yes%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, No%>" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                         <dx:ASPxMenu ID="ASPxMenu9" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, WorkOrders%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstWOYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstWOYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Yes%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, No%>" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                <ContentStyle> 
                                                    <Paddings PaddingBottom="1px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="confPopup" runat="server" ClientInstanceName="confPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, BridgeDetails_Conferences%>" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxMenu ID="ASPxMenu11" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="<%$ Resources:WebResources, DashBoard_tdType%>">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstConfType" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfType" Rows="4" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AudioVideo%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AudioOnly%>" Value="6" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, RoomOnly%>" Value="7" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="tierPopup" runat="server" ClientInstanceName="tierPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, UserMenuController_Tiers%>" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstTiers" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstTiers" Rows="4" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Tier1%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Tier2%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="rmInfoPopup" runat="server" ClientInstanceName="rmInfoPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, RoomInformation%>" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstRmInfo" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRmInfo" Rows="14" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageOrganizationProfile_PhoneNumber%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, RoomSearch_Capacity%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageOrganizationProfile_Address1%>" Value="3" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageOrganizationProfile_Address2%>" Value="4" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageRoomProfile_Floor%>" Value="5" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageRoomProfile_lblTitle%>" Value="6" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageRoomProfile_City%>" Value="7" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageOrganizationProfile_StateProvince%>" Value="8" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageOrganizationProfile_Country%>" Value="9" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ZIPCountry%>" Value="10" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, TimeZone%>" Value="11" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, BridgeDetails_PrimaryApprove%>" Value="12" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, SecAppI%>" Value="13" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, SecAppII%>" Value="14" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="15" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                    
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="rmAssetPopup" runat="server" ClientInstanceName="rmAssetPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, RoomAssets%>" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstRmAsset" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRmAsset" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ManageRoom_Projector%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, UserMenuController_Catering%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="confStatusPopup" runat="server" ClientInstanceName="confStatusPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, ConferenceStatus%>" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfStatus" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfStatus" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                             <dx:ListEditItem Text="<%$ Resources:WebResources, Scheduled%>" Value="0" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Pending%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Completed%>" Value="7" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Deleted%>" Value="9" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Terminated%>" Value="3" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Immediate%>" Value="15" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="16" />                                                              
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('confOccurencePopup');}" />
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="confOccurencePopup" runat="server" ClientInstanceName="confOccurencePopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, Occurence%>" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfOccurence" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfOccurence" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Single%>" Value="0" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Recurring%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="2" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('confDetailsPopup');}" />
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="confDetailsPopup" runat="server" ClientInstanceName="confDetailsPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, ConfMCUInfo_ConferenceDeta%>" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfDetails" Rows="7" >
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){ShowConfMenu(s);}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ReportDetails_ConferenceID%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, ReportDetails_ConferenceTitl%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, UserMenuController_Endpoints%>" Value="3" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, approvalstatus_MCU%>" Value="5" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, WorkOrders%>" Value="6" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="7" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="eptDetailsPopup" runat="server" ClientInstanceName="eptDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="<%$ Resources:WebResources, EndPoint%>" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstEptDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstEptDetails" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, InventoryManagement_lblHName%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AddNewEndpoint_AddressType%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AddTerminalEndpoint_Address%>" Value="3" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, DialingOption%>" Value="4" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="woDetailsPopup" runat="server" ClientInstanceName="woDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="<%$ Resources:WebResources, WorkOrder%>"  CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstWODetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstWODetails" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, UserMenuController_Inventory%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, UserMenuController_Catering%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, UserMenuController_Facility%>" Value="3" /> <%-- FB 2570 --%>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="mcuDetailsPopup" runat="server" ClientInstanceName="mcuDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="<%$ Resources:WebResources, approvalstatus_MCU%>" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstMCUDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstMCUDetails" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, UsedinConference%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, Default%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, EditEndpoint_Address%>" Value="3" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="eptPopup" runat="server" ClientInstanceName="eptPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, endpointdetails_EndpointDetail%>" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstEptDetailsM" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstEptDetailsM" Rows="16" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, DefaultProfileName%>" Value="1" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, NumberofProfiles%>" Value="2" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AddNewEndpoint_AddressType%>" Value="3" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AddTerminalEndpoint_Address%>" Value="4" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AddTerminalEndpoint_Model%>" Value="5" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, PreferredBandwith%>" Value="6" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, MCUAssigment%>" Value="7" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, EditEndpoint_PreferredDiali%>" Value="8" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, EditUserTemplate_DefaultProtoco%>" Value="9" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, EditEndpoint_WebAccessURL%>" Value="10" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, NetworkLocation%>" Value="11" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, TelnetEnabled%>" Value="12" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, AddTerminalEndpoint_EmailID%>" Value="13" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, BridgeDetails_APIPort%>" Value="14" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, EditEndpoint_iCalInvite%>" Value="15" />
                                                              <dx:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="16" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2593--%><%--FB 2886--%> <%--ZD 102399--%>
                                            <dx:ASPxPopupControl ID="mcuPopup" runat="server" ClientInstanceName="mcuPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, DefaultLicense_MCUs%>" Width="150px" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxListBox ID="lstMCU" runat="server" SelectionMode="Multiple" Height="80px" Width="100%" ClientInstanceName="lstMCU" Rows="10" >
                                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){BindEptFromMCU();}" />
                                                                        <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                                     </dx:ASPxListBox>         
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Literal Text="<%$ Resources:WebResources, MasterChildReport_PublicMCU%>" runat="server"></asp:Literal></td>
                                                            </tr>
                                                        </table>                                                                                                                                                                  
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%> <%--ZD 102399--%>
                                            <dx:ASPxPopupControl ID="endPtPopup" runat="server" ClientInstanceName="endPtPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, Endpoints%>" Width="150px" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstendPt" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstendPt" Rows="10" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="rmPopup" runat="server" ClientInstanceName="rmPopup"
                                                EncodeHtml="false" HeaderText="<%$ Resources:WebResources, Rooms%>" Width="150px" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstRm" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRm" Rows="10" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                        </td>
                                    </tr>
                                    <tr style="display: none; background-color: red;">
                                        <td>
                                            <input align="middle" type="button" runat="server" style="width: 100px; height: 21px;
                                                display: none;" id="ClosePUp" value=" <%$ Resources:WebResources, Close%>" class="altButtonFormat" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>                    
                    <tr valign="top" id="trSwt" runat="server" style="display: none">
                        <td align="right" valign="top">
                            <a id="ChgOrg" runat="server" href="#" class="blueblodtext"><asp:Literal Text="<%$ Resources:WebResources, MasterChildReport_ChgOrg%>" runat="server"></asp:Literal></a>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td> <%--FB 2886--%>
                            <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="100px" >
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server">
                                        <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="../Image/category.png" AlternateText="Category"> <%--ZD 100419--%>
                                        </dx:ASPxImage>
                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:WebResources, ReportDetails_Categories%>" >
                                        </dx:ASPxLabel>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                        </td>
                    </tr>
                    <%--Menu Start--%>
                    <tr>
                        <td><%--FB 2808--%><%--FB 2886--%>
                         <dx:ASPxPopupControl ID="SavedRptListPopup" runat="server" AllowDragging="false" AllowResize="false"
                            CloseAction="CloseButton" HeaderStyle-CssClass = "altLongSmallButtonFormat" HeaderStyle-ForeColor="White"
                            EnableViewState="False"  PopupHorizontalAlign="Center"
                            PopupVerticalAlign="Middle" Width="150px"
                            HeaderText="<%$ Resources:WebResources, ReportList%>" ClientInstanceName="SavedRptListPopup" EnableHierarchyRecreation="True">
                                <ContentCollection>
                                    <dx:PopupControlContentControl>
                                         <dx:ASPxListBox ID="lstReportList" runat="server" Height="80px" Width="100%"
                                          ClientInstanceName="lstReportList" SelectionMode="CheckColumn"  >
                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                            <ClientSideEvents SelectedIndexChanged="function(s,e){fnSelect();}" />
                                         </dx:ASPxListBox>
                                         <table width="80%" align="center">
                                            <tr>
                                                <td><%--FB 2886--%>
                                                    <asp:Button ID="btnRptDelete" runat="server" CssClass="altBlueButtonFormat"  OnClick="SaveReport"  UseSubmitBehavior="false"
                                                                    OnClientClick="javascript:if(!fnDelete()) return false;" Text="<%$ Resources:WebResources, MasterChildReport_btnRptDelete%>" />                                                                     
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnRptView" runat="server" CssClass="altBlueButtonFormat"  OnClick="btnOk_Click"  UseSubmitBehavior="false"
                                                                    OnClientClick="javascript:return SavedReportSelection();" Text="<%$ Resources:WebResources, MasterChildReport_btnRptView%>" />                                                                     
                                                </td>
                                            </tr>
                                        </table>           
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                                 <ContentStyle> 
                                    <Paddings PaddingBottom="8px" />
                                </ContentStyle>
                            </dx:ASPxPopupControl>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><%--FB 2886--%><%--ZD 100423--%>
                            <dx:ASPxMenu ID="MenuConference" runat="server" AutoSeparators="RootOnly" AccessibilityCompliant="true"
                                CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px" ClientInstanceName="MenuConference"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true" >
                                <ClientSideEvents ItemClick="function(s, e){fnShow('1');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="<%$ Resources:WebResources, Conferences%>" Name="conf_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr >
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuUsers" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true"
                                AccessibilityCompliant="true"><%--ZD 100423--%>
                                <ClientSideEvents ItemClick="function(s, e){fnShow('2');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="<%$ Resources:WebResources, Users%>" Name="Users_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr >
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuRooms" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true"
                                AccessibilityCompliant="true"> <%--ZD 100423--%>
                                <ClientSideEvents ItemClick="function(s, e){fnShow('3');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="<%$ Resources:WebResources, Rooms%>" Name="Rooms_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuEndpt" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true"
                                AccessibilityCompliant="true"><%--ZD 100423--%>
                                <ClientSideEvents ItemClick="function(s, e){fnShow('4');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="<%$ Resources:WebResources, Endpoints%>" Name="Ept_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuMCU" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true"
                                AccessibilityCompliant="true"><%--ZD 100423--%>
                                <ClientSideEvents ItemClick="function(s, e){fnShow('5');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="<%$ Resources:WebResources, MCU%>" Name="Mcu_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr> <%--FB 2593--%>
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuCDR" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true"
                                AccessibilityCompliant="true"><%--ZD 100423--%>
                                <ClientSideEvents ItemClick="function(s, e){fnShow('6');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="<%$ Resources:WebResources, CDR%>" Name="CDR_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>

                    <%-- ZD 102835 Start --%>
                    <tr>
                        <td>
                            <dx:ASPxMenu ID="MenuWorkOrder" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true"
                                AccessibilityCompliant="true">
                                <ClientSideEvents ItemClick="function(s, e){fnShow('7');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="<%$ Resources:WebResources, WorkOrders%>" Name="WorkOrder_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>

                    <tr>
                        <td style="height: 320px;">
                        </td>
                    </tr>
                    <%-- ZD 102835 End --%>

                    <%--Menu End--%>
                </table>
            </td>
            <td width="8000px" valign="top"> <%--FB 2050 FB 2886--%> 
                <table width="100%" align="center" border="0" >
                    <tr runat="server" id="rptImgRow">
                        <td align="center">
                            <img src="../Image/ReportsII.png" alt="Reports" runat="server" id="imgRpt" width="50" height="50" title="<%$ Resources:WebResources, ReportPanel%>" /> <%--ZD 100422--%>
                        </td>
                    </tr>
                    <tr valign="top" runat="server" id="trDetails">
                        <td>
                            <dx:ASPxPopupControl ID="MoreInfoPopup" runat="server" ClientInstanceName="MoreInfoPopup" Width="150px" >
                                <ContentCollection>
                                    <dx:PopupControlContentControl>   
                                        <dx:ASPxMemo ID="txtMemo" runat="server" Height="91px" Width="170px" ClientInstanceName="memo" Border-BorderStyle="None">
                                            <ClientSideEvents Init="function(s, e) {s.GetInputElement().style.overflowY='hidden';OnInit();}" /><%--ZD 100423--%>
                                        </dx:ASPxMemo>
                                        <div id="divInfo"> <%-- ZD 102835 --%>
                                        
                                        </div>                                        
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                                 <ContentStyle> 
                                    <Paddings PaddingBottom="5px" />
                                </ContentStyle>
                            </dx:ASPxPopupControl>
                            <%--Main Grid--%> <%--FB 2886--%>
                            <div id="MainDiv" style="width:1000px;overflow-y:auto;overflow-x:auto; word-break:break-all;HEIGHT: 540px;" >
                            <dx:ASPxGridView ID="MainGrid" ClientInstanceName="MainGrid" runat="server" Width="100%" EnableCallBacks="true" 
                              OnHtmlRowCreated="MainGrid_HtmlRowCreated" AutoGenerateColumns ="false"  AccessibilityCompliant="true" EnableRowsCache="false" EnableViewState="False"
                             OnDetailRowGetButtonVisibility="masterGrid_DetailRowGetButtonVisibility" OnDataBound="MainDetailsGrid_DataBound"> <%--FB 2593--%><%--ZD 100423--%> <%--ZD 101223--%>
                             <ClientSideEvents RowExpanding ="function(s,e){fnUpdateState(e.visibleIndex,'X');}" RowCollapsing ="function(s,e){fnUpdateState(e.visibleIndex,'R');}" ColumnMoving ="function(s,e){fnUpdateState(e.visibleIndex,'M');}" DetailRowExpanding="function(s,e){fnUpdateState(e.visibleIndex,'E');}" DetailRowCollapsing="function(s,e){fnUpdateState(e.visibleIndex,'C');}"/> <%--FB 2965--%>
                                <Templates>
                                    <DetailRow>
                                        <div style="width:100%;overflow-y:auto;overflow-x:auto; word-break:break-all;" runat="server" id="DetailsDiv" >
                                            <dx:ASPxGridView ID="DetailGrid"  ClientInstanceName="DetailGrid" runat="server" Width="100%" 
                                            OnPageIndexChanged="detailGrid_DataSelect" OnInit="detailGrid_DataSelect" 
                                            OnDetailRowGetButtonVisibility="detailsGrid_DetailRowGetButtonVisibility" OnDataBound="DetailsGrid_DataBound">
                             				<ClientSideEvents  DetailRowExpanding="function(s,e){fnUpdateState(e.visibleIndex,'E');}" DetailRowCollapsing="function(s,e){fnUpdateState(e.visibleIndex,'C');}"/><%--FB 2965--%>
                                                <Settings ShowFooter="True"  />    
                                                <SettingsPager Mode="ShowAllRecords"/>
                                                <SettingsBehavior AllowSort="false" /> <%--ZD 104807--%>
                                                <SettingsDetail IsDetailGrid="True" ShowDetailRow="True" />  
                                                <Templates>
                                                    <DetailRow>
                                                        <dx:ASPxGridView ID="SubDetailGrid"  ClientInstanceName="SubDetailGrid" runat="server" Width="100%" 
                                                           OnPageIndexChanged="subDetailGrid_DataSelect" OnInit="subDetailGrid_DataSelect" OnDataBound="subDetailGrid_DataBound">
                                                            <SettingsPager Mode="ShowAllRecords"/>  
                                                            <SettingsDetail IsDetailGrid="True" ShowDetailRow="True"  ExportMode="All" />  
                             								<ClientSideEvents DetailRowExpanding="function(s,e){fnUpdateState(e.visibleIndex,'E');}" DetailRowCollapsing="function(s,e){fnUpdateState(e.visibleIndex,'C');}"/> <%--FB 2965--%>
                                                            <Templates>
                                                                <DetailRow>
                                                                    <dx:ASPxGridView ID="ChildDetailGrid"  ClientInstanceName="ChildDetailGrid" runat="server" Width="100%" 
                                                                       OnPageIndexChanged="ChildDetailGrid_DataSelect" OnInit="ChildDetailGrid_DataSelect" OnDataBound="childDetailGrid_DataBound">
                                                                        <SettingsPager Mode="ShowAllRecords"/>  
                                                                    </dx:ASPxGridView>
                                                                </DetailRow>
                                                            </Templates>  
                                                        </dx:ASPxGridView>
                                                    </DetailRow>
                                                </Templates>
                                            </dx:ASPxGridView>
                                        </div>
                                    </DetailRow>
                                </Templates>
                                <SettingsDetail ShowDetailRow="true" ExportMode="All" />
                                <Styles> <%--FB 2886--%>
                                    <Header ImageSpacing="9px" SortingImageSpacing="9px" CssClass="tableHeader" Font-Size="9pt"></Header>
                                    <Cell    Font-Size="9pt"></Cell>                                 
                                </Styles>
                                <Images ImageFolder="../en/App_Themes/Plastic Blue/{0}/">
                                    <CollapsedButton Height="10px" Url="../en/App_Themes/Plastic Blue/GridView/gvCollapsedButton.png" AlternateText="Collapse" Width="9px" /> <%--ZD 100419--%>
                                    <ExpandedButton Height="9px" Url="../en/App_Themes/Plastic Blue/GridView/gvExpandedButton.png" AlternateText="Expand" Width="9px" /> <%--ZD 100419--%>
                                    <HeaderFilter Height="14px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png"  AlternateText="HeaderFilter" Width="14px" /> <%--FB 2886--%> <%--ZD 100419--%>
                                    <HeaderActiveFilter Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilterActive.png" AlternateText="HeaderFilterActive" Width="11px" /> <%--ZD 100419--%>
                                    <HeaderSortDown Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortDown.png" AlternateText="HeaderSortDown"  Width="11px" /> <%--ZD 100419--%>
                                    <HeaderSortUp Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortUp.png" AlternateText="HeaderSortUp"  Width="11px" /><%--ZD 100419--%>
                                    <FilterRowButton Height="13px" Width="13px" />
                                    <CustomizationWindowClose Height="14px" Width="14px" />
                                    <PopupEditFormWindowClose Height="14px" Width="14px" />
                                    <FilterBuilderClose Height="14px" Width="14px" />
                                </Images>  
                                <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="True" ShowTitlePanel="True" 
                                ShowHeaderFilterBlankItems="False" /> <%-- FB 2880 --%>
                                <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" HeaderFilterShowAll="<%$ Resources:WebResources, All%>" GroupPanel="<%$ Resources:WebResources, GridViewGroupMsg%>"/>
                                <SettingsPager ShowDefaultImages="False" Mode="ShowPager" AlwaysShowPager="true" Position="Top" Summary-Text="<%$ Resources:WebResources, GridViewPageText%>">
                                    <AllButton Text="<%$ Resources:WebResources, All%>"></AllButton>
                                    <NextPageButton Text="<%$ Resources:WebResources, Next1%>"></NextPageButton>
                                    <PrevPageButton Text="<%$ Resources:WebResources, Prev1%>"></PrevPageButton>
                                </SettingsPager>
                            </dx:ASPxGridView>
                            </div>
                            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="MainGrid" ></dx:ASPxGridViewExporter>
                         </td>
                    </tr>
                </table>
            </td>
        </tr>
     </table>
     <%--ZD 100428 START--%>
     <table border="0" width="100%">
      <tr align="center">
            <td align="center"> 
              <button type="button" id="btnCancel" class="altLongYellowButtonFormat" onclick="fnCancel();" style="width:100px">
              <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
            </td> 
         </tr>
     </table>
     <%-- ZD 100428 END--%>
   </form>
   
         
        
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript">
    var mainDiv = document.getElementById("MainDiv");
    if(mainDiv)
    {  //Difference 180
    
        if (window.screen.width <= 1024)
            mainDiv.style.width = "845px";
        else
            mainDiv.style.width = "1184px";        
    }
    //FB 2886
    var btnPreview = document.getElementById("btnPreview");
    if (rbSaved.GetChecked())
         btnPreview.style.display ='None'; 

    //FB 2593
    function fnDefaultOrgSelection()
    {
        var args = fnDefaultOrgSelection.arguments;
        
        if(document.getElementById("hdnAdmin"))
        {
            if(document.getElementById("hdnAdmin").value == "U")
            {
                if(args[0] == "1")
                {                
                    chk_Org.SetChecked (true);
                    chk_Org.SetEnabled(false);
                    OnShowButtonClick();
                    OnShowButtonClick('dptPopup'); //FB 2882
                }
                else if(args[0] == "2")
                {
                    chk_OrgU.SetChecked (true);
                    chk_OrgU.SetEnabled(false);
                    OnShowButtonClick('popupU');
                    OnShowButtonClick('dptPopup'); //FB 2882
                }
                else if(args[0] == "3")
                {
                    chk_OrgR.SetChecked (true);
                    chk_OrgR.SetEnabled(false);
                    OnShowButtonClick('popupR');
                    OnShowButtonClick('dptPopup'); //FB 2882
                }
                else if(args[0] == "4")
                {
                    chk_OrgE.SetChecked (true);
                    chk_OrgE.SetEnabled(false);
                    OnShowButtonClick('popupE');
                }
                else if(args[0] == "5")
                {
                    chk_OrgM.SetChecked (true);
                    chk_OrgM.SetEnabled(false);
                    OnShowButtonClick('popupM');
                }
                else if(args[0] == "6")  //FB 2882
                {
                    OnShowButtonClick('CDR');
                    BindMCU();
                }
                else if(args[0] == "7") // ZD 102835
                {
                    //OnShowButtonClick();
                }
            }   
        }
    } 
   //FB 2965
   function fnUpdateState(id,status)   
    {
        var hdngrop = document.getElementById("hdnGroup");

//        else if(status == "E" || status == "C")
//            hdngrop.value = "";
//        else if(status == "M" || status == "R" || status == "X")        
//            hdngrop.value = "";
//        else
            hdngrop.value = "";
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End  
		<%--ZD 100423--%>
     function OnInit(s, e) {
    ASPxClientUtils.AttachEventToElement(window.document, "keydown", function(evt) {
        if(evt.keyCode == ASPxClientUtils.StringToShortcutCode("ESCAPE"))
            MoreInfoPopup.Hide();
        });
}
//ZD 104686
function fnCustomAll()
{
    var boo = false;
    
    if(chkCustom0.GetChecked())
        boo = true;

    var checkboxCollection = document.getElementById('<%=tdCustomOptions.ClientID %>').getElementsByTagName('input');  
    
    for(var i=0;i<checkboxCollection.length;i++)  
    {  
        if(checkboxCollection[i].type.toString().toLowerCase() == "checkbox")  
        {  
            checkboxCollection[i].checked = boo;  
        }  
    }  
}

</script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2885 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- FB 2885 Ends -->--%>
