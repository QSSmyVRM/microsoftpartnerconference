﻿<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>

<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ParticipantAssignment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>");

    function grid_SelectionChanged(s, e) {
        s.GetSelectedFieldValues("Email", GetSelectedFieldValuesCallback);
    }

    function GetSelectedFieldValuesCallback(values) {
        if (document.getElementById("hdnPartyInfo") != null)
            document.getElementById("hdnPartyInfo").value = values;
        return false;
    }

    function WinClose() {
        if (document.getElementById("UpdatePartyLst") != null)
            document.getElementById('UpdatePartyLst').click();
    }
    function closeMe() {
        window.close();
    }
    
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <input type="hidden" id="hdnPartyInfo" runat="server" />
    <div>
        <table width="100%">
            <tr>
                <td>
                    <b><font class="subtitleblueblodtext">
                        <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ConferenceSetup_SelectParticip%>"
                            runat="server"></asp:Literal>
                    </font></b>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel runat="server" HorizontalAlign="Center" Width="100%" CssClass="treeSelectedNode">
                        <table width="100%" align="center" border="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <dxwgv:ASPxGridView runat="server" AllowSort="true" ID="PartcipantGrid" ClientInstanceName="PartcipantGrid"
                                        KeyFieldName="Email" Width="100%" EnableRowsCache="True" OnDataBound="ASPxGridView1_DataBound">
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="<%$ Resources:WebResources, AudioAddOnBridge_Name%>"
                                                HeaderStyle-CssClass="tableHeader" VisibleIndex="0" HeaderStyle-HorizontalAlign="Center"
                                                CellStyle-HorizontalAlign="Center" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Email" Caption="<%$ Resources:WebResources, Email%>"
                                                VisibleIndex="1" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Center"
                                                CellStyle-HorizontalAlign="Center" />
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="true" HeaderStyle-HorizontalAlign="Center"
                                                CellStyle-HorizontalAlign="Center" VisibleIndex="3" Caption="<%$ Resources:WebResources, Select%>"
                                                HeaderStyle-CssClass="tableHeader">
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="PartyID" Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="RoomID" Visible="False" />
                                        </Columns>
                                        <Styles>
                                            <CommandColumn Paddings-Padding="1" />
                                        </Styles>
                                        <SettingsBehavior AllowMultiSelection="false" />
                                        <Settings ShowFilterRowMenu="true" />
                                        <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" GroupPanel="<%$ Resources:WebResources, GridViewGroupMsg%>" />
                                        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true" Position="Bottom"
                                            Summary-Text="<%$ Resources:WebResources, GridViewPageText%>">
                                        </SettingsPager>
                                        <ClientSideEvents SelectionChanged="grid_SelectionChanged" />
                                    </dxwgv:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input type="button" id="UpdatePartyLst" runat="server" style="display: none;" onserverclick="UpdatePartyList" />
                                    <input align="middle" type="button" runat="server" style="cursor: pointer" id="ClosePUp"
                                        value=" <%$ Resources:WebResources, Close%> " onclick="javascript:WinClose();"
                                        class="altMedium0BlueButtonFormat" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript">
        window.onbeforeunload = WinClose;
    </script>
</body>

</html>
