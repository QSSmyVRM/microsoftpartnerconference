﻿<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>

<%@ Page Language="C#" Inherits="ns_MyVRM.ExchangeLookup" Buffer="true" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<script type="text/javascript" src="script/wincheck.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>");   
    var globalObj;
    function OnCheckedChanged()
    {   
        if (globalObj.checked) {            
            GetSelectedFieldValuesCallback();
        }
        else {
            fnMouseDown();
        }
    }

    function fnMouseDown() {        
            var PartyEmail;
            var PartyAry;
            var txtParty;
            var PartyList = "";
        
            if (globalObj.className == "dxgvCommandColumnItem dxgv__cci")
            {
                txtParty = parent.opener.document.getElementById("txtPartysInfo").value;
                if (globalObj.parentNode.parentNode.childNodes[3] != null)
                    PartyEmail = globalObj.parentNode.parentNode.childNodes[3].innerHTML;
                else
                    PartyEmail = globalObj.parentNode.parentNode.childNodes[2].innerHTML;

                if (txtParty != "") {
                    PartyAry = txtParty.split('||');

                    if (PartyAry.length > 0) {
                        for (var i = 0; i < PartyAry.length; i++) {
                            if (PartyAry[i].indexOf(PartyEmail) > -1)
                                PartyAry[i] = "";

                            if (PartyAry[i] != "")
                                PartyList += PartyAry[i] + '||';

                        }
                    }
                }
                parent.opener.document.getElementById("txtPartysInfo").value = PartyList;
                parent.opener.ifrmPartylist.location.reload(true);
            }
          
        }
   

        function GetSelectedFieldValuesCallback() {
            var partyinfo = "";
            var nonEWSuser = document.getElementById("nonEWSuser");
            var PartyAry;

            var ExistingParty;
            ExistingParty = parent.opener.document.getElementById("txtPartysInfo").value;

            if (globalObj.className == "dxgvCommandColumnItem dxgv__cci") {
                if (globalObj.checked == true) {

                    if (globalObj.parentNode.parentNode.childNodes[3] != null)
                    {
                        PartyLogin = globalObj.parentNode.parentNode.childNodes[2].innerHTML;
                        PartyEmail = globalObj.parentNode.parentNode.childNodes[3].innerHTML;                        
                    } else
                    {
                        PartyLogin = globalObj.parentNode.parentNode.childNodes[1].innerHTML;
                        PartyEmail = globalObj.parentNode.parentNode.childNodes[2].innerHTML;
                    }

                    PartyAry = PartyLogin + "|" + PartyEmail;
                }
            }
            if (PartyAry != null && PartyAry != undefined && PartyAry != "") {
                partyinfo += Addparticipants(PartyAry);
            }        
        
            if (ExistingParty != "")
                partyinfo = partyinfo + ExistingParty;


            parent.opener.document.getElementById("txtPartysInfo").value = partyinfo;        
            parent.opener.ifrmPartylist.location.reload(true);

        }

        function Addparticipants(pinfo) {

            pary = pinfo.split("|");

            var confType = parent.opener.document.getElementById("CreateBy").value;

            pfname = pary[0]; pemail = pary[1]; plname = pary[1].split("@")[0];
        

            if (pemail == "") {
                alert(AudioPartUser);
                return;
            }

            var partysinfo = "";

            if (partysinfo.indexOf("!!" + pemail + "!!") == -1) {
                var txtTemp;

                if (confType == "2" || confType == "4" || confType == "9")
                    txtTemp = "!!0!!1!!0!!1!!1!!0!!";
                else if (confType == "6")
                    txtTemp = "!!0!!1!!0!!1!!0!!1!!";
                else if (confType == "7")
                    txtTemp = "!!0!!1!!0!!1!!0!!0!!";
            

                mp = "new!!" + pfname + "!!" + plname + "!!" + pemail + txtTemp;
            
                mp += "!!!!!!-5!!!!!!0!!0!!0!!0!!1!!0||";


                if (pemail == "") {
                    alert(AudioPartUser);
                }
                else {
                    if ((partysinfo.indexOf("!!" + pemail + "!!") == -1)) {
                        partysinfo += mp
                    }
                    else {
                        lp = partysinfo.lastIndexOf("||", partysinfo.indexOf("!!" + pemail + "!!")) + 1;
                        rp = partysinfo.indexOf("||", partysinfo.indexOf("!!" + pemail + "!!")) + 1;

                        newpartysinfo = partysinfo.substring(0, (lp == "-1") ? 0 : lp);
                        newpartysinfo += mp;
                        newpartysinfo += partysinfo.substring(rp, partysinfo.length);
                        partysinfo = newpartysinfo;
                    }
                }
            }
            return partysinfo;
        }

</script>
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx");

    }
    
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>EWS Address Book</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <div>
        <table width="100%">
            <tr>
                <td align="center">                    
                    <input type="hidden" id="nonEWSuser" runat="server" name="nonEWSuser" value="" />
                    <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="100%"
                        CssClass="treeSelectedNode">
                        <table width="100%" align="center" border="0">
                            <tr>
                                <td>
                                    <h3>
                                        <asp:Label ID="LblHeading" runat="server" Text="<%$ Resources:WebResources, EWSAddressBook%>"></asp:Label></h3>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="blackblodtext">Search:</span>
                                    <asp:TextBox ID="txtSearch" runat="server" Width="170px" CssClass="altText" />                                    
                                    <asp:Button ID="btnSearch" Text="<%$ Resources:WebResources, FindPeople%>" CssClass="altMedium0BlueButtonFormat"
                                        OnClick="SearchEWSUser" Width="140px" runat="server" />
                                    <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtSearch" ID="regtxtSearch" ValidationExpression = "^[\s\S]{3,}$" runat="server" ErrorMessage="<%$ Resources:WebResources, SearchPeople%>"></asp:RegularExpressionValidator>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <div>
                                        <dxwgv:ASPxGridView AllowSort="true" ID="grid" ClientInstanceName="grid" runat="server"
                                            KeyFieldName="Email" Width="100%" EnableRowsCache="True" OnDataBound="ASPxGridView1_DataBound">
                                            <Columns>                                                
                                                <dxwgv:GridViewCommandColumn   Width="10%" ShowSelectCheckbox="true" Visible="true" 
                                                    Caption="<%$ Resources:WebResources, Select%>" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center">                                                    
                                                    </dxwgv:GridViewCommandColumn>
                                                <dxwgv:GridViewDataTextColumn  Width="45%" Settings-AllowAutoFilter="False" FieldName="LoginName"
                                                    Caption="<%$ Resources:WebResources, Login%>" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center" />
                                                <dxwgv:GridViewDataTextColumn Width="45%" Settings-AllowAutoFilter="False" FieldName="Email"
                                                    Caption="<%$ Resources:WebResources, Email%>" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center" />
                                                <dxwgv:GridViewDataColumn FieldName="ifrmDetails" Visible="False" />
                                            </Columns>
                                            <Styles>
                                                <CommandColumn Paddings-Padding="1" />
                                            </Styles>
                                            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true" Position="Bottom">
                                            </SettingsPager>                                                                                         
                                        </dxwgv:ASPxGridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input align="middle" type="button" runat="server" style="cursor: pointer" id="ClosePUp"
                                        value=" <%$ Resources:WebResources, Close%> " onclick="javascript:window.close();"
                                        class="altMedium0BlueButtonFormat" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<script language="javascript" type="text/javascript">        

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }

    $(document).ready(function () {
        $("td[class='dxgvCommandColumn dxgv']").click(function (e) {            
            if (this.className == 'dxgvCommandColumnItem dxgv__cci')
                globalObj = this;
            else
                globalObj = this.childNodes[0];
            setTimeout('OnCheckedChanged()', 250);
        })
    });
    
   
</script>
