/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
function mouse_click_menu_prompt() 
{
	closethis();
	
	var argv = mouse_click_menu_prompt.arguments;
	var argc = argv.length;
	
	if (argc < 1)
		return 0;
	
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'mousemenu');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('mousemenu').style");
	
	if (IE)
		getMouseXY();
	
	promptbox.position = 'absolute';
	w = 180;
	document.getElementById('mousemenu').style.top = mousedownY; 
	document.getElementById('mousemenu').style.left = mousedownX;
	document.getElementById('mousemenu').style.overflow = "visible";
	document.getElementById('mousemenu').style.zIndex = 5;
	
	promptbox.width = w; 
	promptbox.border = 'outset 2px #bbbbbb' 
	
	var m = "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
	var idx = 0;
	for (var i = 0; i < argc; i=i+2) {
		m += "  <tr class='normal' onMouseOver='this.className=\"highlight\"' onMouseOut='this.className=\"normal\"'>";
		m += "    <td><a onclick=\"" + argv[i+1] + "\">" + argv[i] + "</a></td>";
		m += "  </tr>"
	}
	m += "</table>" 

	document.getElementById('mousemenu').innerHTML = m;
} 


function closethis()
{
	if (document.getElementById("mousemenu"))
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("mousemenu"));
}
