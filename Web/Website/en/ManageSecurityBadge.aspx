<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_ManageSecurityBadge" %>

<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--ZD 101028 --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
<%--FB 2790 Starts--%>
   <script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>   
<%--FB 2790 Ends--%>
    <title>myVRM</title>
    <style type="text/css">
        
        /* ZD 100426 Starts */
        .file_input_textbox {
            height:20px; 
            width:200px; 
            float:left; 
            }

        .file_input_div {
            position: relative;
            width:80px; 
            height:30px;
            overflow: hidden;
            float:left; }

        .file_input_button {
            width: 80px;
            position:absolute;
            top:0px; 
            border:1px solid #A7958B;
            padding:2px 8px 2px 8px; 
            font-weight: normal;
            height:24px;
            margin:0px; 
            margin-right:5px; 
            vertical-align:bottom; 
            background-color:#D4D0C8; 
            }

        .file_input_hidden {
            font-size :45px;
            position:absolute;
            right:0px;top:0px;
            cursor:pointer; 
            opacity:0; 
            filter:alpha(opacity=0); 
            -ms-filter:"alpha(opacity=0)";
            -khtml-opacity:0;
            -moz-opacity:0;
            }
        /* ZD 100426 End */ 
        .imgHolder
        {
            width: 400px;
            height: 300px;
            overflow: hidden;
            background-color: Transparent;
            z-index: 10;
            margin-top: -2px;
        }
        .bar
        {
            position: absolute;
            border-style: solid;
            border-width: thin;
            background-color: Transparent;
            cursor: move;
            z-index: 103;
        }
        .area
        {
            position: absolute;
            border-style: solid;
            border-width: thin;
            background-color: Transparent;
            cursor: move;
            z-index: 102;
        }
        .info
        {
            position: absolute;
            border-style: solid;
            border-width: thin;
            background-color: Transparent;
            cursor: move;
            z-index: 101;
        }
        .leftDiv
        {
            position: absolute;
            left: 0;
            top: 0; /*background-color:#eeeeee;*/
            width: 240px;
            margin-left: -250px;
            margin-bottom: 117px;
            height: 400px;
            overflow: scroll;
            background-color: Transparent;
            overflow-x: hidden; /* style="width:200px; height:100px; overflow:scroll;" */
        }
        .rightDiv
        {
            margin-top: 3px;
            position: absolute;
            right: 0;
            margin-right: 400px;
            margin-bottom: 140px;
            bottom: 0; /*background-color:#eeeeee;*/
            width: 405px;
            height: 240px;
            background-color: Transparent;
        }
        .mask
        {
            z-index: 25;
            background-color: Transparent;
            left: 0px;
            top: 0px;
            position: absolute;
            display: none;
            filter: alpha(opacity=0);
            opacity: 0;
        }
        .modal
        {
            left: 250px;
            top: 70px;
            z-index: 50;
            position: absolute;
            display: none;
            width: 800px;
            height: 600px;
            vertical-align: top;
        }
        img
        {
            border: none;
        }
    </style>

    <script language="JavaScript" type="text/javascript">
    <!--
        
    //function noError(){return true;}
    //window.onerror = noError;

    var _startX = 0;            // mouse starting positions
    var _startY = 0;
    var _offsetX = 0;           // current element offset
    var _offsetY = 0;
    var _dragElement;           // needs to be passed from OnMouseDown to OnMouseMove
    var _oldZIndex = 0;         // we temporarily increase the z-index during drag
    var contWidth = 0;
    var contHeight = 0;

    InitDragDrop();
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    function InitDragDrop()
    {
        document.onmousedown = OnMouseDown;
        document.onmouseup = OnMouseUp;
    }

    function findPosX(obj)
    {
        var curleft = 0;
        if(obj.offsetParent)
            while(1) 
            {
                curleft += obj.offsetLeft;
                if(!obj.offsetParent)
                    break;
                obj = obj.offsetParent;
            }
        else if(obj.x)
            curleft += obj.x;
            
        return curleft;
    }

    function findPosY(obj)
    {
        var curtop = 0;
        if(obj.offsetParent)
            while(1)
            {
                curtop += obj.offsetTop;
                if(!obj.offsetParent)
                    break;
                obj = obj.offsetParent;
            }
        else if(obj.y)
            curtop += obj.y;
        return curtop;
    }

    // checking object overlap
    function isObjOnObj()
    {
        var obj1,obj2,obj3;

        obj1 = document.getElementById('areaDiv');
        obj2 = document.getElementById('barDiv');
        obj3 = document.getElementById('infoDiv');

        if(checkOverlap(obj1,obj2))
        {
            alert(overlapImage);
            return false;
        }

        if(checkOverlap(obj1,obj3))
        {
            alert(overlapImage);
            return false;
        }

        if(checkOverlap(obj3,obj2))
        {
            alert(overlapImage);
            return false;
        }
        return true;
    }

    function checkOverlap(a,b)
    {
        var al = getVal(a.style.left);
        var ar = getVal(a.style.left) + getVal(a.style.width);
        var bl = getVal(b.style.left);
        var br = getVal(b.style.left) + getVal(b.style.width);
        
        var at = getVal(a.style.top);
        var ab = getVal(a.style.top) + getVal(a.style.height);
        var bt = getVal(b.style.top);
        var bb = getVal(b.style.top) + getVal(b.style.height);
        
        if(bl>ar || br<al){return false;} //overlap not possible
        if(bt>ab || bb<at){return false;} //overlap not possible
        if(bl>al && bl<ar){return true;}
        if(br>al && br<ar){return true;}
        if(bt>at && bt<ab){return true;}
        if(bb>at && bb<ab){return true;}

        return true;
    }

    function OnMouseDown(e)
    {
        // IE is retarded and doesn't pass the event object
        if (e == null) 
            e = window.event; 
        
        // IE uses srcElement, others use target
        var target = e.target != null ? e.target : e.srcElement;
        
        // This block takes mouse x and y position to display rename div
        // for IE, left click == 1
        // for Firefox, left click == 0
        
        if ((e.button == 1 && window.event != null || e.button == 0) && ((target.className == 'area') || (target.className == 'bar') || (target.className == 'info')))
        {
            // grab the mouse position
            _startX = e.clientX;
            _startY = e.clientY;
            
            // grab the clicked element's position
            _offsetX = ExtractNumber(target.style.left);
            _offsetY = ExtractNumber(target.style.top);
            
            // bring the clicked element to the front while it is being dragged
            _oldZIndex = target.style.zIndex;
            target.style.zIndex = 1000;
            
            // we need to access the element in OnMouseMove
            _dragElement = target;

            // tell our code to start moving the element with the mouse
            document.onmousemove = OnMouseMove;
            
            // cancel out any text selections
            document.body.focus();

            // prevent text selection in IE
            document.onselectstart = function () { return false; };
            // prevent IE from trying to drag an image
            target.ondragstart = function() { return false; };
            
            return false;
        }
    }

    function OnMouseMove(e)
    {
        if (e == null) 
            var e = window.event; 

        if(_dragElement != null)
        {
            var left = document.getElementById(_dragElement.id).style.left;
            var top = document.getElementById(_dragElement.id).style.top;
        }
        
        var width, height;
        if(_dragElement != null)
        {
            var tempW = document.getElementById(_dragElement.id).style.width;
            var tempH = document.getElementById(_dragElement.id).style.height;
            width = tempW.substring(0,tempW.length-2)*1; //width2;
            height = tempH.substring(0,tempH.length-2)*1; //height2;
        }
        
        var valX = _offsetX + e.clientX - _startX;
        var valY = _offsetY + e.clientY - _startY;
        
        if(valX > ((contWidth)-width))
            valX=(contWidth)-width;
        else if(valX<0)
            valX=1;
        
        if(valY > ((contHeight)-height))
            valY=(contHeight)-height;
        else if(valY<0)
            valY=1;
        
        if( (valX > 0 && valX < (contWidth-width+1) ) && (valY > 0 && valY < (contHeight-height+1) ) )
        {
            // this is the actual "drag code"
            _dragElement.style.left = valX + 'px';
            _dragElement.style.top = valY + 'px';
        }
        
        updateCoordinates();
    }

    function OnMouseUp(e)
    {
        if (e == null) 
            e = window.event; 
        
        // IE uses srcElement, others use target
        var target = e.target != null ? e.target : e.srcElement;
        
        var swap=target.innerHTML;

        if (_dragElement != null)
        {
            if(target.id=="areaDiv" || target.id=="barDiv" || target.id=="infoDiv")
                target.style.zIndex = _oldZIndex;
            else
            {
                document.getElementById("barDiv").style.zIndex=103;
                document.getElementById("areaDiv").style.zIndex=102;
                document.getElementById("infoDiv").style.zIndex=101;
            }
            
            _dragElement=null;
            
            return false;
        }
    }    

    function ExtractNumber(value)
    {
        var n = parseInt(value);
        return n == null || isNaN(n) ? 0 : n;
    }

    // this is simply a shortcut for the eyes and fingers
    function $(id)
    {
        return document.getElementById(id);
    }

    function updateCoordinates()
    {
        var x1 = getVal(document.getElementById("infoDiv").style.left);
        var y1 = getVal(document.getElementById("infoDiv").style.top);

        var x2 = getVal(document.getElementById("barDiv").style.left);
        var y2 = getVal(document.getElementById("barDiv").style.top);

        var x3 = getVal(document.getElementById("areaDiv").style.left);
        var y3 = getVal(document.getElementById("areaDiv").style.top);

        // place this in hidden file
        if(document.getElementById('hdnSecImg1Axis').id != null)
        {
            var updatedValue = x1 + "|" + y1 + "," + x2 + "|" + y2 + "," + x3 + "|" + y3;        
            document.getElementById('hdnSecImg1Axis').value = updatedValue;
        }
    }

    function checkFileType()
    {
        clearLabel();
        var filepath=document.getElementById('uploadImage1').value;

        var len = filepath.length;
        var type=filepath.substring(len-4,len).toUpperCase();
        if(type==".JPG" || type=="JPEG" || type==".GIF" || type == ".PNG" || type == ".TIF" || type == "TIFF" || type == ".BMP")
            return true;
        else
        {
            alert(ValidImageFormat);
            return false;
        }
    }

    function showDiv(w,h)
    {
        contWidth = w*1;
        contHeight = h*1;
        // areadiv bardiv infodiv
        if(contWidth==0 || contHeight==0)
        {
            document.getElementById("areaDiv").style.display='block';
            document.getElementById("barDiv").style.display='block';
            document.getElementById("infoDiv").style.display='block';
        }

        document.body.scrollTop = 0;
        document.getElementById("maskDiv").style.display='block';
        document.getElementById("maskDiv").style.width=screen.width;
        document.getElementById("maskDiv").style.height=screen.height;

        document.body.style.overflow = "hidden";

        document.getElementById('xModDiv').style.left=250;
        document.getElementById('xModDiv').style.top=70;

        document.getElementById('xModDiv').style.display='block';
        updateCoordinates();
        return false;
    }

    function hideDiv(divId)
    {
        document.getElementById(divId).style.display='none';
        document.getElementById("maskDiv").style.display='none';

        document.body.style.overflow = "auto";
        self.close();
    }

    function changeImage(Ww,Hh)
    {
        contWidth = parseInt(Ww);
        contHeight = parseInt(Hh);
    }

    function fnAddOrRemoveItem(status, item, name)
    {    
        var tmpOptArray = item.split(".");
        var optValue = tmpOptArray[0].substring(8, tmpOptArray[0].length);
        var optText = tmpOptArray[0];

        if(status == 0) // Remove
            opener.DelImage(item);
        else if(status == 1) //Add
        {
            var x = window.opener.document.getElementById("drpSecImgList");
            var drpArray = window.opener.document.getElementById("drpSecImgList").options;
            var i;
            var cnt = drpArray.length;
            for(i=0 ; i<cnt; i++)
            {
                if(item == drpArray[i].value)
                    return false;
            }

            var newOption = window.opener.document.createElement('<option value=' + item + '>');
            window.opener.document.all.drpSecImgList.options.add(newOption);
            newOption.innerText = name;
            
            var hdnSelecOption = window.opener.document.getElementById("hdnSelecOption");
            
            if(hdnSelecOption)
                hdnSelecOption.value += item + "," + name + "|";
        }
    }

    var labelStatus = 0;
    function clearLabel()
    {
        if(labelStatus == 1)
        {
            if(document.getElementById('errLabel')!=null)
                document.getElementById('errLabel').innerText = "";
        }
        labelStatus=1;
    }

    // >> Please refer fnUpdateHdnField() in ManageRoomProfile.aspx <<
    var postback = 1;
    function updateImage(link, axis, ids, ind)
    {
        clearLabel();
        document.getElementById('imgCont').src = link;
        var w = document.getElementById('imgCont').clientWidth;
        var h = document.getElementById('imgCont').clientHeight;
        changeImage(w, h);

        var imgName = link.split("/");
        var selImage = imgName[(imgName.length)-1];
        var tempNameArray = selImage.split(".");
        
        document.getElementById("selImageName").innerHTML  = tempNameArray[0];
        document.getElementById("hdnSelFile").value  = selImage;

        var axisArray = axis.split(",");

        var infoDivAxis = axisArray[0].split("|");
        document.getElementById("infoDiv").style.left = infoDivAxis[0] + "px";
        document.getElementById("infoDiv").style.top = infoDivAxis[1] + "px";

        var barDivAxis = axisArray[1].split("|");
        document.getElementById("barDiv").style.left = barDivAxis[0] + "px";
        document.getElementById("barDiv").style.top = barDivAxis[1] + "px";

        var areaDivAxis = axisArray[2].split("|");
        document.getElementById("areaDiv").style.left = areaDivAxis[0] + "px";
        document.getElementById("areaDiv").style.top = areaDivAxis[1] + "px";

        indArray = ind.split("/");
        var drpDownId = indArray[(indArray.length)-1];

        document.getElementById('clickedImage').value = drpDownId;

        /* Veni Fix
        var x = window.opener.document.getElementById("drpSecImgList");
        var drpArray = window.opener.document.getElementById("drpSecImgList").options;
        //alert(drpArray.length);
        //alert("Index: " + y[x].index + " is " + y[x].text);
        var i;
        var cnt = drpArray.length;
        for(i=0 ; i<cnt; i++)
        {
            if(drpDownId == drpArray[i].value)
            {
                if(postback == 1)
                    x.selectedIndex = drpArray[i].index;   
            }
        }

        */
        postback = 1;
        updateCoordinates();
    }

    // this function converts pixel to value i.e, 200px to 200 for processing
    function getVal(obj)
    {
        var val = obj.substring(0,obj.length-2)*1;
        return val;
    }
    //FB 2909 start
    function getfilename(obj) {
        var fileInputVal = obj.value;
        fileInputVal = fileInputVal.replace("C:\\fakepath\\", "");
        if (navigator.userAgent.indexOf("MSIE") > -1)
            obj.parentNode.parentNode.childNodes[0].value = fileInputVal;
        else
            obj.parentNode.parentNode.childNodes[1].value = fileInputVal;
    }
    // FB 2909 End
       
    //-->
    </script>

</head>
<body>
    <form name="frmManageimage" autocomplete="off" method="Post" action="" id="frmManageimage" runat="server"><%--ZD 101190--%>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" runat="server" id="hdnSecImg1Axis" />
    <input type="hidden" runat="server" id="clickedImage" />
    <input type="hidden" runat="server" id="hdnSelFile" />
    <center>
        <div id="dataLoadingDIV" style="display:none" align="center">
              <img border='0' src='image/wait1.gif'  alt='Loading..' />
         </div> <%--ZD 100678 End--%>
        <h3>
            <asp:Label ID="lblTitle" runat="server" CssClass="h3" Text=""></asp:Label>
        </h3>
    </center>
    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
        <tr>
            <td align="center">
                <table cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="Label1" runat="server" CssClass="subtitleblueblodtext" Text="<%$ Resources:WebResources, ManageSecurityBadge_Label1%>"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td align="left">
                            <table id="Table5" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                <tr>
                                    <td colspan="2">
                                        <div id="maskDiv" class="mask">
                                        </div>
                                        <div id="hiddenDiv" style="display: none">
                                            <asp:Button ID="picBut" runat="server" Text="<%$ Resources:WebResources, Submit%>" />
                                            <input id="selectedFile1" type="text" runat="server" />
                                        </div>
                                        <div id="xModDiv" class="modal">
                                            <div>
                                                <table width="100%" align="center" border="0" style="border-collapse: collapse">
                                                    <tr>
                                                        <td>
                                                            <div id="pnlImgArray" class="leftDiv" runat="server">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <div class="imgHolder">
                                                                <!-- <asp:Label ID="Label12" runat="server" CssClass="subtitleblueblodtext" Text="Edit Security Badge" ></asp:Label><br /><br /> -->
                                                                <asp:Image ID="imgCont" runat="server" Visible="false" AlternateText="Image Content" /> <%--ZD 100419--%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div id="areaDiv" class="area" style="background: url(../image/BlankPerson.jpg);
                                                    left: 1px; top: 1px; width: 90px; height: 120px; border: none">
                                                </div>
                                                <div id="barDiv" class="bar" style="background: url(../image/BarCode.jpg); left: 1px;
                                                    top: 152px; width: 95px; height: 28px; border: none">
                                                </div>
                                                <%if (Session["language"].ToString().Equals("en"))
                                                  { %>
                                                <div id="infoDiv" class="info" style="background: url(../image/ConfInfo.jpg); left: 103px;top: 60px; width: 170px; height: 120px; border: none">
                                                <%}
                                                  else
                                                  { %>
                                                <div id="infoDiv" class="info" style="background: url(../image/ConfInfosp.jpg); left: 103px;top: 60px; width: 170px; height: 120px; border: none">
                                                <%} %>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="rightDiv">
                                                <table cellpadding="2" cellspacing="2" border="0" style="width: 100%">
                                                    <tr>
                                                        <td align="left" class="blackblodtext">
                                                            <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Image%>" runat="server"></asp:Literal>: <%--FB 2579--%>
                                                            <asp:Label ID="selImageName" CssClass="blackItalictext" Text="" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnSave" runat="server" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, MasterChildReport_btnSave%>"
                                                                OnClick="UpdateImageAxis" OnClientClick="javascript:return isObjOnObj();" />
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnRemove" runat="server" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, Remove%>"
                                                                OnClick="RemoveSecImage" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="3">
                                                            <span class="subtitleblueblodtext" runat="server" id="spnConf"><asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ManageSecurityBadge_spnConf%>" runat="server"></asp:Literal></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="blackblodtext">
                                                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SecurityBadgeImage%>" runat="server"></asp:Literal> <%--FB 2579--%>
                                                        </td>
                                                        <td colspan="2"> <%--FB 2909 start--%>
                                                            <div>
                                                                <input type="text" runat="server" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' style="width:156px"/>
                                                                <div class="file_input_div"><input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button"  onclick="document.getElementById('uploadImage1').click();return false;"  /><%--FB 3055-Filter in Upload Files --%>
                                                                <asp:FileUpload ID="uploadImage1" accept="image/*" runat="server" class="file_input_hidden" TabIndex="-1" onclick="javascript:return clearLabel();" OnChange="getfilename(this)" /><br /></div></div> <%--FB 2909 End--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="right">
                                                            <asp:Label ID="hdnImageSize" CssClass="blackItalictext"
                                                                runat="server">(<asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ManageSecurityBadge_hdnImageSize%>" runat="server"></asp:Literal>)</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="right">
                                                            <input id="Button4" type="button" runat="server" class="altMedium0BlueButtonFormat" value="<%$ Resources:WebResources, Close%>"
                                                                onclick="javascript:hideDiv('xModDiv');" />
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="saveLink" runat="server" Width="100pt" Text="<%$ Resources:WebResources, Upload%>"
                                                                OnClick="UploadSecurtiyImage" OnClientClick="javascript:return checkFileType()" /> <%--FB 2796--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>

    <script language="JavaScript" type="text/javascript">
<!--
    
function refreshImage()
{
  var obj = document.getElementById("imgCont");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
  }
  
  showDiv(273,180);
  
  return false;
}

function updateImageHolderValues()
{
    var imgHold = document.getElementById('imgCont');
    if(imgHold) //FB 2023
        updateImage(imgHold.src, imgHold.alt, imgHold.title, imgHold.longDesc);
}

function onloadFuncs()
{
    refreshImage();
    updateImageHolderValues();
}

window.onload = onloadFuncs;

//FB 2487 - Start
    var obj = document.getElementById("errLabel");    
    if (obj != null) {
    var strInput = obj.innerHTML.toUpperCase();

    if (((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("EXITOSA") > -1) && !(strInput.indexOf("FALLIDA") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("SUCC�S") > -1 || strInput.indexOf("�TABLIE") > -1) && !(strInput.indexOf("�CHEC") > -1) && !(strInput.indexOf("ERREUR") > -1)) //French            
            ) {
        obj.setAttribute("class", "lblMessage");
        obj.setAttribute("className", "lblMessage");
    }
    else {
        obj.setAttribute("class", "lblError");
        obj.setAttribute("className", "lblError");
    }
}
//FB 2487 - End  
//-->
    </script>
    
<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">
    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
</script>
<%--ZD 100428--%>

</body>
</html>
