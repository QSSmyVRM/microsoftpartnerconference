<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_GraphicalReport" EnableEventValidation="false" %>
<%@ Register TagPrefix="cc1" Namespace="Microsoft.Samples.ReportingServices" Assembly="ReportViewer" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>    
<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%} %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx"-->
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %>
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830
    }    
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reports</title>
    <link href="Css/myVRMStyle.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="extract.js"></script>
    <script type="text/javascript" src="inc/functions.js"></script>
    <style type="text/css">
        .verticaltext
        {
            font:bold 10px Tahoma; color: #000000; writing-mode: tb-rl;
        }
        .verticaltextlong
        {
            font:bold 8pt Arial; color: #000000;
        }
    </style>
    <style type="text/css">
        @media print
        {
            /*****   Styles for print   ******/    
            #InputParamDiv
            {
                display: none;
            }
            #trExptCtrls
            {
                display: none;
            }
           .header, .hide { visibility: hidden } 

        }
    </style>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css"  /><%--FB 1947--%>
    <script src="script/mytreeNET.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript">
        var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
        parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
        
    </script>
   
    <script language="javascript" src='script/lib.js'></script>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="script/calview.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    
    <script type="text/javascript" language="javascript">    
     var rtn;
     //ZD 100604 start
     var img = new Image();
     img.src = "../en/image/wait1.gif";
     //ZD 100604 End 
    function fnMovePrev()
    {
        var args = fnMovePrev.arguments;
        var lstmonth = document.getElementById("lstMonth");
        var lstyear = document.getElementById("lstYear");
        var viewReport = document.getElementById("btnviewRep");
         var txtdte = document.getElementById("txtDate");
        var txtstart = document.getElementById("txtDateFrom");
        if(args[0] == "2")
        {
            if(lstmonth.selectedIndex > 0)
            {
                lstmonth.selectedIndex = lstmonth.selectedIndex - 1 ;
                viewReport.click();
            }
            else
            {
                if(lstyear.selectedIndex > 0)
                {
                    lstyear.selectedIndex = lstyear.selectedIndex - 1 ;
                    lstmonth.selectedIndex = 11;
                    viewReport.click();
                }
                else
                {
                    alert(GraphReportMonth);
                }
            }
        }
        else if(args[0] == "1")
        {
            if(lstyear.selectedIndex > 0)
            {
                lstyear.selectedIndex = lstyear.selectedIndex - 1 ;
                viewReport.click();
            }
            else
            {
                alert(GraphReportYear);
            }
        }
        else if(args[0] == "4")
        {
            if(eval(txtdte))
            {
                var curdate = GetDefaultDate(txtdte.value,'<%=format%>');
                txtdte.value = dateAddition(curdate,'d',-1).format('<%=format%>');
                viewReport.click();
            }
            else
            {
                alert(GraphReportNDate);
            }
        }
        else if(args[0] == "5")
        {
            if(eval(txtstart))
            {
                var curdate = GetDefaultDate(txtstart.value,'<%=format%>');
                txtstart.value = dateAddition(curdate,'d',-6).format('<%=format%>');
                viewReport.click();
            }
            else
            {
                alert(GraphReportNDate);
            }
        }
    }
    
    function fnMoveNext()
    {
        var args = fnMoveNext.arguments;
        var lstmonth = document.getElementById("lstMonth");
        var lstyear = document.getElementById("lstYear");
        var viewReport = document.getElementById("btnviewRep");
        var txtdte = document.getElementById("txtDate");
        var txtstart = document.getElementById("txtDateFrom");
        if(args[0] == "2")
        {
           if(lstmonth.selectedIndex < 11)
            {
                lstmonth.selectedIndex = lstmonth.selectedIndex + 1 ;
                viewReport.click();
            }
            else
            {
                if(lstyear.selectedIndex < lstyear.options.length -1)
                {
                    lstyear.selectedIndex = lstyear.selectedIndex + 1 ;
                    lstmonth.selectedIndex = 0;
                    viewReport.click();
                }
                else
                {
                    alert(GraphReportNMonth);
                }
            }
        }
        else if(args[0] == "1")
        {
            if(lstyear.selectedIndex < lstyear.options.length -1)
            {
                lstyear.selectedIndex = lstyear.selectedIndex + 1 ;
                viewReport.click();
            }
            else
            {
                alert(GraphReportNYear);
            }
        }
        else if(args[0] == "4")
        {
            if(eval(txtdte))
            {
                var curdate = GetDefaultDate(txtdte.value,'<%=format%>');
                txtdte.value = dateAddition(curdate,'d',1).format('<%=format%>');
                viewReport.click();
            }
            else
            {
                alert(GraphReportNDate);
            }
        }
        else if(args[0] == "5")
        {
            if(eval(txtstart))
            {
                var curdate = GetDefaultDate(txtstart.value,'<%=format%>');
                txtstart.value = dateAddition(curdate,'d',6).format('<%=format%>');
                viewReport.click();
            }
            else
            {
                alert(GraphReportNDate);
            }
        }
    }
    function fnNavigate()
    {
        var args = fnNavigate.arguments;
        var lstmonth = document.getElementById("lstMonth");
        var lstyear = document.getElementById("lstYear");
        var viewReport = document.getElementById("btnviewRep");
        if(args[0] == "0") //Move Previous
        {
            fnMovePrev(args[1]);
        }
        else if(args[0] == "1") //Move Next
        {
            fnMoveNext(args[1]);
        }
        
        
    }
    function fnNavigation()
    {
        var lstReport = document.getElementById("lstReport").value;
        var args = fnNavigation.arguments;
        if(queryField != null)
        {
            if(queryField("Report") == "TU")
            {
                if(lstReport == "3")
                    lstReport = "2";
	            fnNavigate(args[0],lstReport);
            }
            else if(queryField("Report") == "SU")
            {
                if(lstReport == "3")
                    lstReport = "2";
	            fnNavigate(args[0],lstReport);
            }
            else if(queryField("Report") == "ML")
            {
                if(lstReport == "1")
                    lstReport = "2";
                else if(lstReport == "3")
                    lstReport = "4";
                else if(lstReport == "2")
                    lstReport = "5";
	            fnNavigate(args[0],lstReport);
            }
            else if(queryField("Report") == "SL")
            {
                if(lstReport == "1")
                    lstReport = "2";
                 else if(lstReport == "2")
                    lstReport = "5";
                else if(lstReport == "3")
                    lstReport = "4";
	            fnNavigate(args[0],lstReport);
            }
            else if(queryField("Report") == "MC")
            {
                if(lstReport == "3")
                    lstReport = "2";
	            fnNavigate(args[0],lstReport);
            }
            else if(queryField("Report") == "SC")
            {
                if(lstReport == "1")
                    lstReport = "2";    
                else if(lstReport == "3")
                    lstReport = "4";           
	            fnNavigate(args[0],lstReport);
            }
        }
    }
    
    function HideControls()
    {    
        var args = HideControls.arguments;
        if(args[0] != "4")
            document.getElementById("tdAllType").style.display = "none";            
        
        if(args[0] != "1")
            document.getElementById("ConfScheRptCell").style.display = "none";
        
         document.getElementById("tdConfType").style.display = "none";
        document.getElementById("tdLoc").style.display = "none";
        document.getElementById("tdMCU").style.display = "none";
        document.getElementById("tdCountry").style.display = "none";
        document.getElementById("tdZipCode").style.display = "none";
        document.getElementById("tdBridge").style.display = "none";        
        document.getElementById("UsageReportCell").style.display = "none";
        document.getElementById("TimeRangeCell").style.display = "none";
        document.getElementById("ChrtTypeCell").style.display = "none";
        //document.getElementById("SQLReportParamRow").style.display = "none";
        document.getElementById("ToDateRow").style.display = "none";
        document.getElementById("FromDateRow").style.display = "none";        
        document.getElementById("RoomRow").style.display = "none";
        document.getElementById("UserReportsCell").style.display = "none";
        document.getElementById("SummaryDiv").style.display = "none";
        document.getElementById("tdCusFrmDte").style.display = "none";
        document.getElementById("tdCusToDte").style.display = "none";
        document.getElementById("ImgPrinter").style.display = "Block"; //Added for Graphical Reports
        document.getElementById("btnExcel").style.display = "Block";//Added for Graphical Reports
        document.getElementById("btnPDF").style.display = "Block";//Added for Graphical Reports
    }
    
    function fnShowMenu()
    {
        var reportsList = document.getElementById("ReportsList");
        var type = document.getElementById("DrpAllType").value;
        var confScheRptDivList = document.getElementById("ConfScheRptDivList");
        var lstreport = document.getElementById("lstReport");
        var paramRow = document.getElementById("ParamRow");
        
        var rptListValue
//        if(reportsList.value == "5")//FB 1750
//        {
//            window.location.replace("UtilizationReport.aspx");
//            return;
//        }
        
        if(document.getElementById("hdnValue").value == "")
        {
            document.getElementById("hdnValue").value = "Y";
            if(reportsList)
                reportsList.value = "1";
            if(confScheRptDivList)
                confScheRptDivList.value = "1";
        }
        
        if(reportsList)
            rptListValue = reportsList.value;
            
        HideControls(rptListValue);
        
        if(rptListValue != "4")
        {
            var cmbStrtTime_Text = document.getElementById("CmbStrtTime_Text");
            var cmbEndTime_Text = document.getElementById("CmbEndTime_Text");
            if(cmbStrtTime_Text)
                cmbStrtTime_Text.style.width = "70px";
            
            if(cmbEndTime_Text)
                cmbEndTime_Text.style.width = "70px";        
        }
        
        if(paramRow)
            paramRow.style.display = "Block";
        
        switch(rptListValue)
        {
            case "1":
               // document.getElementById("SQLReportParamRow").style.display = "Block";  
                document.getElementById("FromDateRow").style.display = "Block";  
                document.getElementById("ToDateRow").style.display = "Block";  
                              
                document.getElementById("ConfScheRptCell").style.display = "Block";
                 if(document.getElementById("ConfScheRptDivList").value == "1")
                 {
                    document.getElementById("RoomRow").style.display = "Block";  
                    var calendarRow = document.getElementById("CalendarRow");
                    
                    if(calendarRow)
                    {
                         if(calendarRow.style.display == "block")
                            document.getElementById("SummaryDiv").style.display = "Block";
                    }
                 }
                 else if(confScheRptDivList.value == "2" || confScheRptDivList.value == "3" || confScheRptDivList.value == "4")
                 {
                    return false;
                 }
                    
                break;
            case "2":
                document.getElementById("UserReportsCell").style.display = "Block"; 
                paramRow.style.display = "None";
                break;            
            case "3":       
                //document.getElementById("SQLReportParamRow").style.display = "Block";     
                document.getElementById("FromDateRow").style.display = "Block";  
                document.getElementById("ToDateRow").style.display = "Block";            
                document.getElementById("UsageReportCell").style.display = "Block";
                break;
            case "4":
                document.getElementById("tdAllType").style.display = "Block";                
                var drpchrtValue = document.getElementById("DrpChrtType").value;               
                if(type == "1")
                {
                    document.getElementById("ChrtTypeCell").style.display = "Block";                
                    document.getElementById("TimeRangeCell").style.display = "Block";
                    document.getElementById("tdConfType").style.display = "block";
                    fnSetCntrlValue("DrpLocations","1");
                    fnSetCntrlValue("DrpMCU","1");
                    
                        if(lstreport.value == "6" && drpchrtValue == "1")
                        {
                            document.getElementById("btnPDF").style.display = "none";
                            document.getElementById("ImgPrinter").style.display = "none";
                        }
                        else if(drpchrtValue != "1")
                        {
                            document.getElementById("btnExcel").style.display = "none";
                        }
                }
                else if(type == "2")
                {
                    document.getElementById("ChrtTypeCell").style.display = "Block";                
                    document.getElementById("TimeRangeCell").style.display = "Block";
                    document.getElementById("tdLoc").style.display = "block";                     
                    if(document.getElementById("DrpLocations").value == "1")
                        document.getElementById("RoomRow").style.display = "block";
                                         
                    fnSetCntrlValue("DrpMCU","1");

                        if(drpchrtValue == "1")
                        {
                            document.getElementById("btnPDF").style.display = "none";
                            document.getElementById("ImgPrinter").style.display = "none";
                        }
                        else if(drpchrtValue != "1")
                        {
                           document.getElementById("btnExcel").style.display = "none";
                        }
                    
                }
                else if(type == "3")
                {
                    document.getElementById("ChrtTypeCell").style.display = "Block";                
                    document.getElementById("tdMCU").style.display = "block";                    
                    document.getElementById("TimeRangeCell").style.display = "block";
                    var drpMCU = document.getElementById("DrpMCU");
                    
                    fnSetCntrlValue("DrpLocations","1");
                    
                    if(drpchrtValue == "1" && drpMCU.value != "2")
                    {
                        document.getElementById("btnPDF").style.display = "none";
                        document.getElementById("ImgPrinter").style.display = "none";
                    }
                    else if(drpchrtValue != "1")
                    {
                       document.getElementById("btnExcel").style.display = "none";
                    }
                        
//                    if(lstreport.value == "6" && drpchrtValue == "1")
//                    {
//                        document.getElementById("btnPDF").style.display = "none";
//                        document.getElementById("ImgPrinter").style.display = "none";
//                    }
//                    else if(drpchrtValue != "1")
//                    {
//                        document.getElementById("btnExcel").style.display = "none";
//                    }    
                }    
                
                if(lstreport.value == "6")
                {                
                    document.getElementById("tdCusFrmDte").style.display = "block";
                    document.getElementById("tdCusToDte").style.display = "block";    
//                    document.getElementById("ImgPrinter").style.display = "None";    
                }
            break;
            case "5":  //FB 1750
                paramRow.style.display = "None";
                break;  
        }
        return true;
    }
    
    function fnSetCntrlValue(cntrl,value)
    {
         var _cntrl = document.getElementById(cntrl);
         if(_cntrl)
            _cntrl.value = value;
    }
    
    function fnShow()
    {
        var reportsList = document.getElementById("ReportsList");
        
        if(reportsList)
        {
            if(reportsList.value == "4")
            {
                
                if(document.getElementById("DrpLocations").value == "1" && document.getElementById("DrpAllType").value == "2")
                     document.getElementById("RoomRow").style.display = "block";
                else
                     document.getElementById("RoomRow").style.display = "None";
                
                if(document.getElementById("DrpLocations").value == "2")
                    document.getElementById("tdCountry").style.display = "block";
                else
                    document.getElementById("tdCountry").style.display = "none";
                
                if(document.getElementById("DrpLocations").value == "3")
                    document.getElementById("tdZipCode").style.display = "block";
                else
                    document.getElementById("tdZipCode").style.display = "none";
                
                if(document.getElementById("DrpMCU").value == "2")
                    document.getElementById("tdBridge").style.display = "block";
                else
                {
                    document.getElementById("TimeRangeCell").style.display = "block";
                    document.getElementById("tdBridge").style.display = "none";
                }
                
                if(document.getElementById("lstReport").value == "6")
                {
                    document.getElementById("tdCusFrmDte").style.display = "block";
                    document.getElementById("tdCusToDte").style.display = "block";
                }
                else
                {
                    document.getElementById("tdCusFrmDte").style.display = "none";
                    document.getElementById("tdCusToDte").style.display = "none";
                    document.getElementById("tdCusFrmDte").value = "";
                    document.getElementById("tdCusToDte").value = "";
                }

                if(document.getElementById("DrpAllType").value == "0" && document.getElementById("lstReport").value == "0" && document.getElementById("DrpChrtType").value == "0")
                { 
                    document.getElementById("lblHeading").style.display = "none";
                }
                if(document.getElementById("DrpAllType").value == "2" && document.getElementById("DrpLocations").value != "1")
                    return false;
            }
        }
    }
    
    function fnSubmit()
     {  
        var reportsList = document.getElementById("ReportsList");
        
        if(reportsList)        
        {
            if(reportsList.value == "1" || reportsList.value == "3")            
            {
//                if(reportsList.value == "1")
//                {
//                    if(document.form1.txtRooms.value == "")
//                     {
//                        alert("Please select the room");
//                        return false;
//                     }
//                }
            
                if(document.form1.txtStartDate.value != "" && document.form1.txtEndDate.value == "")
                {
                    alert(UserToDate);
                    document.form1.txtEndDate.focus();
                    return false;
                }
                else if(document.form1.txtEndDate.value != "" && document.form1.txtStartDate.value == "")
                {
                    alert(UserFromDate);
                    document.form1.txtStartDate.focus();
                    return false;
                }
            
                textValue = document.form1.txtStartDate.value;
                index = textValue.length;
                startDate = textValue.substring(0,index);
                textValue = document.form1.txtEndDate.value;
                index = textValue.length;
                endDate = textValue.substring(0,index);   
                if ('<%=format%>'=='dd/MM/yyyy') // dd/MM/yyyy
                {
                    var mDay = startDate.substring(0,2);
                    var mMonth = startDate.substring(3,5);
                    var mYear = startDate.substring(6,10);
                    var strSeperator='/';
                    startDate = mMonth+strSeperator+mDay+strSeperator+mYear;  
                    var emDay = endDate.substring(0,2);
                    var emMonth = endDate.substring(3,5);
                    var emYear = endDate.substring(6,10);              
                    endDate = emMonth+strSeperator+emDay+strSeperator+emYear; 
                }   
                                  
                timeValue = document.form1.CmbStrtTime_Text.value;
                if(timeValue!="")
                    startTime = timeValue.substring(0,textValue.length);
             
                timeValue = document.form1.CmbEndTime_Text.value;
                if(timeValue!="")
                    endTime = timeValue.substring(0,timeValue.length);  
                
               var compareVal = fnCompareDate(startDate,endDate)
               
                if(compareVal == "L")
                {
                    alert(UserDate);
                    return false;
                }
                else
                {                
                    if(compareVal == "E")
                    {
                        if((startTime!="")&&(endTime !=""))
                        {
                            var start = startTime.split(':');
                            var end = endTime.split(':');
                            
                            var stTime= start[1].substring(start[1].length,2);
                            var endTime= end[1].substring(end[1].length,2);
                            
                              if((stTime==" PM")&&(endTime==" AM"))
                                {
                                    alert(GraphReportTime);
                                    return false;
                                  
                                }
                             else if(((stTime ==" AM")&&(endTime==" AM"))||((stTime==" PM")&&(endTime==" PM")))
                                {
                                    if(start[0]!="12")
                                    {
                                        if(start[0] >  end[0])
                                        {
                                            alert(GraphReportTime);
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                       /* if((stTime ==" AM")&&(endTime==" AM") && (start[0] > end[0]))
                                        {
                                            alert("From Time should be lesser than To time.");
                                            return false;
                                        }
                                        else*/ if((stTime ==" AM")&&(endTime==" AM") && (start[0] == end[0]))
                                        {
                                            if(start[1] > end[1])
                                            {
                                                alert(GraphReportTime);
                                                return false;
                                            }
                                            else if(start[1] == end[1])
                                            {
                                                alert(GraphReportTime);
                                                return false;
                                            }
                                        }
                                     }
                               }
                        }
                    }
                }        
            }
            else if(reportsList.value == "4")
            {
               if(document.getElementById("lstReport").value == "6")
                {
                    if(document.getElementById("txtCusDateFrm").value == "" || document.getElementById("txtCusDateTo").value == "")
                    {
                        if(document.getElementById("txtCusDateFrm").value == "")
                        {
                            reqCusFrom.style.display = 'block';
                            document.getElementById("txtCusDateFrm").focus();
                            return false;
                        }                
                        
                        if(document.getElementById("txtCusDateTo").value == "")
                        {
                            reqCusTo.style.display = 'block';
                            document.getElementById("txtCusDateTo").focus();
                            return false;
                        }
                    }
                    else if(document.getElementById("txtCusDateTo").value != "")
                    { 
                        reqCusTo.style.display = 'none';
                    }
                    else if(document.getElementById("txtCusDateFrm").value != "")
                    { 
                        reqCusFrom.style.display = 'none';
                    }
                    if(document.getElementById("txtCusDateFrm").value  != "" && document.getElementById("txtCusDateTo").value != "")
                    {
                        ChangeEndDate(0);                    
                        return rtn;
                    }
                }
                var txtzipcode = document.getElementById('<%=txtZipCode.ClientID%>');
                if (txtzipcode.value != '' && txtzipcode.value.search(/^(\(|\d|\))*$/)==-1)
                {        
                    regZipCode.style.display = 'block';
                    txtzipcode.focus();
                    return false;
                }
            }
            else if(reportsList.value == "5")
            {
                 window.location.replace("UtilizationReport.aspx");
                 return false;
            }
        }
     }
    
    function setPrint()
    {
        alert(GraphReportPrint);
        var reportsList = document.getElementById("ReportsList");
        var drpChrttype = document.getElementById("DrpChrtType");
        if(reportsList)        
        {
          if(reportsList.value == "4" && drpChrttype.value != "1")
          {
	           window.print();
	           return false;
	      }	      		    
	      else
	      {
            window.open("PrintInterface.aspx","myVRM",'status=yes,width=750,height=400,scrollbars=yes,resizable=yes');	    
            return false;
	      }
	   }
    } 
    
    function ChangeEndDate(frm)
    {
        var confenddate = '';
        if(document.getElementById("txtCusDateTo").value != "")
        confenddate = GetDefaultDate(document.getElementById("txtCusDateTo").value,'<%=format%>');
        var confstdate = '';
        if(document.getElementById("txtCusDateFrm").value != "")
        confstdate = GetDefaultDate(document.getElementById("txtCusDateFrm").value,'<%=format%>');
        
        if(document.getElementById("txtCusDateFrm").value != "")
            reqCusFrom.style.display = 'none';
            
        if (Date.parse(confenddate) < Date.parse(confstdate))
        {
            if (frm == "0") 
            {
                alert(UserDate);
                rtn = false;
                 return false;
            }
        }
        else
        {
            rtn = true;
            return true;
        }
    }
        
    function ChangeStartDate(frm)
    {
        var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("txtCusDateTo").value,'<%=format%>');
        var confstdate = '';
        confstdate = GetDefaultDate(document.getElementById("txtCusDateFrm").value,'<%=format%>');
        
        if(document.getElementById("txtCusDateTo").value != "")
            reqCusTo.style.display = 'none';
        
        if (Date.parse(confstdate) > Date.parse(confenddate))
        {
            if (frm == "0") 
            {
                alert(UserDate);
                rtn = false;
                 return false;
            }
        }
        else
        {
            rtn = true;
            return true;
        }
    }
              
    </script>
    
    <script type="text/javascript">
    function fnShowImg()
    {
        var args = fnShow.arguments;
        var detailsDiv = document.getElementById("DetailsDiv");
        var imgDiv = document.getElementById("imgDiv");
        
        if(detailsDiv)
        {
            if(detailsDiv.style.display == "block")
            {
                detailsDiv.style.display = "None";
                if(imgDiv)
                    imgDiv.src = imgDiv.src.replace("minus", "plus");
            }
            else
            {
                 detailsDiv.style.display = "Block";
                if(imgDiv)
                    imgDiv.src = imgDiv.src.replace("plus", "minus");
            }
        }
        
        return false;
    }
    
     var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
        parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
       
        function fnShowRooms()
        {
            var _cntrl = document.getElementById('<%=RoomDiv.ClientID%>');
            if(_cntrl.style.display == 'none')
                    _cntrl.style.display = 'block';
            else
                _cntrl.style.display = 'none';
        }
        
        function fnSelectAll()
        {
            var args = fnSelectAll.arguments;
            var rooms = '';
            if(args[0] != null)
            {
                if(args[1] != null)
                {
                    var this_ = document.getElementById(args[1]);
                   
                   
                    fnAssignChecked(this_,args[0].checked);
                }
            }
        }
        
        function fnAssignChecked(this_,checkValue)
        {
            
            if(this_ != null)
            {
                 var checkBoxArray = this_.getElementsByTagName('input');
                 
                 for (var i=0; i<checkBoxArray.length; i++) 
                 { 
                     var checkBoxRef = checkBoxArray[i];
                     
                     checkBoxRef.checked = checkValue;                                 
                 }
            }
        }
        
        function fnDeselectAll()
        {
            var args = fnDeselectAll.arguments;
            
            if(args[0] != null)
            {
                if(!args[0].checked)
                {
                    if(args[1] != null)
                    {
                        var allCheck = document.getElementById(args[1])
                        if(allCheck)
                        {
                            if(allCheck.checked)
                                allCheck.checked = false;
                        }
                    }
                }        
            }
        }
        
        function fnAssignValue()
        { 
            document.form1.txtRooms.value = ''
            document.form1.hdnRoomIDs.value = ''
            
            var args = fnAssignValue.arguments;
            var rooms = '';
           
            if(args[0] != null)
            {
                var this_ = document.getElementById(args[0])
                if(this_)
                {
                    var checkBoxArray = this_.getElementsByTagName('input');
                    var checkedValues = '';
               
                    for (var i=0; i<checkBoxArray.length; i++) 
                    { 
                         var checkBoxRef = checkBoxArray[i];
                         if(checkBoxRef.checked)
                         {
                              
                            var labelArray = checkBoxRef.parentNode.getElementsByTagName('label');      //Edited for FF
                
                            if ( labelArray.length > 0 )
                            {
                                if (rooms.length > 0 )
                                    rooms += ', ';

                                rooms += labelArray[0].innerHTML;
                             }
                         }                            
                    }
                    
                    if(rooms != '')
                        document.form1.txtRooms.value = rooms;
                }
            }
        }
        
        function fnAssignDefault()
        {
            var args = fnAssignDefault.arguments            
            if(args[0] == 'S')
                fnAssignValue('<%=cblRoom.ClientID%>');
            else
            {            
                document.form1.chkSelectall.checked = true;
                fnSelectAll(document.form1.chkSelectall,'<%=cblRoom.ClientID%>');
                fnAssignValue('<%=cblRoom.ClientID%>');
            }
        } 
         
</script>
</head>
<body style="overflow-x:hidden;overflow-y:auto;">
    <form id="form1" runat="server" method="post">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" id="hdnPrtCtrl" runat="server" />
    <input type="hidden" id = "hdnRowColor" runat="server" />
    <input type="hidden" id = "hdnConfNum" runat="server" />
    <input type="hidden" name="hdnRoomIDs" id="hdnRoomIDs" runat="server" />   
    <input type="hidden" id = "hdnChartName" runat="server" />       
    <input type="hidden" id = "hdnValue" runat="server" />        
    
    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="Table8"  >
     <tr>
        <td>
            <table width="100%" border="0" style="left:0;" id="tbleBtns">                
                <tr id="trExptCtrls">
                    <td align="right">                                               
                        <table width="8%" cellspacing="0">
                           <tr>
                               <td>
                                   <asp:ImageButton id="ImgPrinter" runat="server" src="image/print.gif" alt="Print Report" OnClientClick='Javscript:return setPrint();' style="cursor: hand;vertical-align:middle;" ToolTip="Print" /> <%--FB 3034--%>
                               </td>
                               <td>
                                   <asp:ImageButton ID="btnExcel" AlternateText="Export to Excel" OnClick="ExportExcel"  src="image/excel.gif"  runat="server" style="vertical-align:middle;" ToolTip="Export to Excel"/> <%--ZD 100419--%>
                               </td>
                               <td>
                                   <asp:ImageButton ID="btnPDF" AlternateText="Export to PDF" OnClick="ExportPDF" OnClientClick="javascript:return pdfReport();" src="image/adobe.gif"  runat="server" style="vertical-align:middle;" ToolTip="Export to PDF"/> <%--ZD 100419--%>
                               </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width:5%;"></td>
                </tr>
            </table>
         </td>  
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="5" cellspacing="0" width="100%" onclick='fnAssignValue("<%=cblRoom.ClientID%>")'>               
                <tr>
                    <td valign="top" style="width:20%" id="tblControls" >
                        <div id="InputParamDiv"  style="background-color:#f3f3f3; border-color:Blue;border-width:1px;border-style:Solid;width:100%;Height:470px;overflow:auto;text-align:left;">
                          <table width="100%" cellpadding="5" cellspacing="0" >
                            <tr>
                                <td class="tableHeader">                                    
                                   <img id="TypeImg" runat="server" src="image/collapse.jpg" alt="Collapse"  />  <%--ZD 100419--%>                           
                                   <span class="blackblodtext"><b>Categories</b></span>
                                </td>
                            </tr>
                          <tr>
                            <td>
                                <ajax:CollapsiblePanelExtender ID="TypeExtender" runat="server" TargetControlID="TypePanel" 
                                ImageControlID="TypeImg" CollapseControlID="TypeImg" ExpandControlID="TypeImg" 
                                ExpandedImage="image/expand.jpg" Collapsed="false" CollapsedImage="image/collapse.jpg" 
                                CollapsedSize="2"></ajax:CollapsiblePanelExtender> 
                                <asp:Panel id="TypePanel" runat="server">
                                <table cellpadding="3" >
                                    <tr><td>
                                       <div id="ReportsDiv">
                                       <asp:UpdatePanel ID="ReportsListTab" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                            <Triggers></Triggers>
                                            <ContentTemplate>
                                            <asp:DropDownList ID="ReportsList" runat="server" CssClass="altText" onclick="javascript:return fnShowMenu()" onchange="javascript:return fnShowMenu()" OnSelectedIndexChanged="DisplayLocations" AutoPostBack="true">
                                                <asp:ListItem Selected="True" Value="1">Conference Scheduling Reports</asp:ListItem>
                                                <asp:ListItem Value="2">User Reports </asp:ListItem>
                                                <asp:ListItem Value="3">Usage Reports</asp:ListItem>
                                                <asp:ListItem Value="4">Graphical Reports</asp:ListItem>                                                
                                            </asp:DropDownList>
                                            </ContentTemplate>
                                       </asp:UpdatePanel>
                                        </div>                                     
                                        </td>
                                    </tr>
                                    <tr id="ConfScheRptCell" style="display:none;"> 
                                        <td  class="blackblodtext">
                                            <b>Conference Scheduling Reports</b>
                                            <div id="ConfScheRptDiv"> 
                                            <asp:UpdatePanel ID="ConfTab" runat="server" UpdateMode="Conditional">
                                                <Triggers> </Triggers>
                                                <ContentTemplate>                                           
                                                <asp:DropDownList ID="ConfScheRptDivList" runat="server" CssClass="altText" onclick="javascript:return fnShowMenu()" OnSelectedIndexChanged="DisplayLocations" AutoPostBack="true">
                                                    <asp:ListItem Selected="True" Value="2">Daily Schedule</asp:ListItem>
                                                    <asp:ListItem  Value="1">Calendar Report</asp:ListItem>                                                    
                                                    <asp:ListItem Value="3">PRI Allocation</asp:ListItem>
                                                    <asp:ListItem Value="4">Resource Allocation</asp:ListItem>
                                                </asp:DropDownList>
                                                </ContentTemplate>
                                             </asp:UpdatePanel>                                                
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id="UserReportsCell" style="display:none;">
                                        <td  class="blackblodtext">
                                           <b>User Reports</b>
                                            <div id="Div3">
                                                <asp:DropDownList ID="UserReportsList" runat="server" CssClass="altSelectFormat" Width="120px">
                                                    <asp:ListItem Value="1">Contact List </asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id="UsageReportCell" style="display:none;">
                                        <td  class="blackblodtext">
                                           <b>Usage Reports</b>
                                            <div id="Div1">
                                                <asp:DropDownList ID="UsageReportList" runat="server" CssClass="altSelectFormat" >
                                                    <asp:ListItem Selected="True" Value="1">Aggregate Usage</asp:ListItem>
                                                    <asp:ListItem Value="2">Usage by Room </asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id="tdAllType"  style="display:none;">
                                        <td  class="blackblodtext" nowrap>
                                            <span class="blackblodtext" style="width:80PX"><b>Category</b></span><br />
                                           <div id="CategoryDiv">
                                                <asp:DropDownList ID="DrpAllType" runat="server" CssClass="altSelectFormat" onclick="javascript:return fnShowMenu()">
                                                    <asp:ListItem Selected="True" Value="1">Conference Type</asp:ListItem>
                                                    <asp:ListItem Value="2">Locations</asp:ListItem>
                                                    <asp:ListItem Value="3">MCU</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id="tdConfType" style="display:none;">
                                        <td  class="blacktext"  nowrap>
                                        <span class="blackblodtext" style="width:90PX"><b>Conference Type</b></span><br /> 
                                           <div id="ConferenceTypeDiv">                                            
                                                <asp:DropDownList ID="lstConference" runat="server" CssClass="altSelectFormat" >                                               
                                                    <asp:ListItem Selected="True" Value="1">All</asp:ListItem>
                                                    <asp:ListItem Value="6">Audio Conferences Only</asp:ListItem>
                                                    <asp:ListItem Value="2">Video Conferences Only</asp:ListItem>
                                                    <asp:ListItem Value="7">Room Conferences Only</asp:ListItem>
                                                    <asp:ListItem Value="4">Point-to-Point Only</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                               </table>  
                             </asp:Panel>
                           </td>
                           </tr>
                           <tr id="ParamRow" class="tableHeader"> <%--Edited for FF--%>
                                <td class="tableHeader">
                                    <img id="ParamImg" runat="server" alt="collapse" src="image/collapse.jpg" />  <%--ZD 100419--%>
                                    <span class="blackblodtext"><b>Parameters</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="ParamExtender" runat="server" TargetControlID="ParamPanel1" 
                                    ImageControlID="ParamImg" CollapseControlID="ParamImg" ExpandControlID="ParamImg" 
                                    ExpandedImage="image/expand.jpg" Collapsed="false" CollapsedImage="image/collapse.jpg" 
                                    CollapsedSize="8"></ajax:CollapsiblePanelExtender> 
                            <asp:Panel id="ParamPanel1" runat="server">
                             <table cellpadding="3" width="100%"  border="0">                            
                            <tr id="tdLoc" style="display: none">
                                <td  class="blacktext"  runat="server" nowrap>
                                    <span class="blackblodtext" style="width:110PX"><b>Locations</b></span><br />
                                    <div id="LocationsDiv">
                                        <asp:DropDownList ID="DrpLocations" runat="server" CssClass="altSelectFormat" Width="160PX" onclick="javascript:return fnShow();">
                                            <asp:ListItem Selected="True" Value="1">Room Name</asp:ListItem>
                                            <asp:ListItem Value="2">Country/State</asp:ListItem>
                                            <asp:ListItem Value="3">Zip Code</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tdMCU" style="display: none">
                                <td  class="blacktext" nowrap>         
                                   <span class="blackblodtext" style="width:110PX"><b>MCU Reports</b></span><br /> 
                                   <div id="MCUDiv">
                                        <asp:DropDownList ID="DrpMCU" runat="server" CssClass="altSelectFormat" Width="160PX" onclick="javascript:return fnShow();">
                                            <asp:ListItem Selected="True" Value="1">All</asp:ListItem>
                                            <asp:ListItem Value="2">Custom(Select one)</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tdCountry" style="display:none">
                                <td align="left" class="blacktext"  nowrap >
                                <span class="blackblodtext" style="width:110PX"><b>Country/State</b></span>
                                   <div id="lstCountriesDiv">
                                   <asp:UpdatePanel ID="CountryPan" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <Triggers></Triggers>
                                        <ContentTemplate>
                                            <asp:DropDownList ID="lstCountries" CssClass="altText" runat="server" DataTextField="Name" DataValueField="ID" OnSelectedIndexChanged="UpdateStates" AutoPostBack="true" Width="160PX">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="lstStates" CssClass="altText" Width="50" runat="server" DataTextField="Code" DataValueField="ID">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                   </asp:UpdatePanel>
                                  </div>
                                </td>
                            </tr>
                            <tr id="tdZipCode" style="display:none">
                                <td align="left" class="blacktext"  nowrap>
                                <span class="blackblodtext" style="width:110PX"><b>Zip Code</b></span>
                                   <div id="txtZipCodeDiv">                                                
                                        <asp:TextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, EnterNumbersonly%>" ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                  </div>
                                </td>
                            </tr>
                            <tr id="tdBridge" style="display: none">
                                <td align="left" class="blacktext"  nowrap>
                                   <span class="blackblodtext" style="width:110PX">Select a MCU</span>
                                      <div id="lstBridgesDiv">
                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstBridges" runat="server" DataTextField="BridgeName"
                                            DataValueField="BridgeID">
                                        </asp:DropDownList>
                                     </div>
                                </td>
                            </tr>
                            <tr id="FromDateRow">
                                <td  style="left:0px;"><span class="blackblodtext">From Date / Time</span>
                                    <br />
                                   <asp:TextBox id="txtStartDate" CssClass="altText" MaxLength="20" runat="server" Width="80"></asp:TextBox>
                                  <img src="image/calendar.gif" alt="Date Selector" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align:middle" title="Date selector" onblur="javascript:ChangeStartDate(0)" onclick="return showCalendar('<%=txtStartDate.ClientID %>', 'cal_triggerd', 0, '<%=format%>');" /> <%--ZD 100419--%>
                                 <mbcbb:ComboBox id="CmbStrtTime" runat="server" BackColor="White" CssClass="altText" onblur="javascript:ChangeStartDate(0)" Rows="10" Style="width:auto" CausesValidation="True" AutoPostBack="false">		<%--Edited for FF--%>			                        
                                    <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                 </mbcbb:ComboBox>
                                </td>                                            
                            </tr>
                            <tr id="ToDateRow">
                                <td ><span class="blackblodtext">To Date / Time</span><br />                                            
                                    <asp:textbox id="txtEndDate" CssClass="altText" MaxLength="20" Runat="server" Width="80"></asp:textbox> 
                                    <img src="image/calendar.gif" alt="Date Selector" border="0" width="20" height="20" id="cal_trigger1" style="cursor: pointer;vertical-align:middle;" title="Date selector" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('<%=txtEndDate.ClientID %>', 'cal_trigger1', 0, '<%=format%>');" /> <%--ZD 100419--%>                                          
                                   <mbcbb:ComboBox id="CmbEndTime" runat="server" BackColor="White" CssClass="altText" onblur="javascript:ChangeStartDate(0)" Rows="10" style="width:auto" CausesValidation="True" AutoPostBack="false"><%--Edited for FF--%>					                        
                                    <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                  </mbcbb:ComboBox>
                                </td>                                                
                            </tr>
                            <tr style="display:none;">
                                <td class="ParamLabelCell" id="LblTZTD" runat="server" ><span class="blackblodtext">Time Zone</span>	<br />					
                                    <asp:DropDownList ID="drpTimeZone" runat="server" DataTextField="timezoneName" DataValueField="timezoneID" ></asp:DropDownList>
                                </td>		
                             </tr>
                             <tr id="RoomRow">
                                <td class="ParamLabelCell" nowrap>
                                    <span class="blackblodtext" >Select a Room</span><br />
                                    <asp:UpdatePanel ID="LocTab" runat="server" UpdateMode="Conditional">
                                            <Triggers> </Triggers>
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtRooms" runat="server" Width="170px" CssClass="altText"/>
                                    <img id="imgRoom" src="Image/DDImage.gif" onclick="fnShowRooms()" style="cursor:hand;" alt="DDImage"/> <%--ZD 100419--%>
                                    <br />
                                    <div id="RoomDiv" class="RoomDiv" runat="server" style="display:none;">
                                    
                                                &nbsp;<asp:CheckBox ID="chkSelectall" runat="server" Text="Select All" />
                                               <span class="blackblodtext" > <asp:CheckBoxList ID="cblRoom" runat="server" DataTextField="roomName" DataValueField="roomID"></asp:CheckBoxList></span>
                                            
                                    </div>
                                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                                    </div><%--ZD 100678--%>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>  
                                    
                                </td> 
                                				                        
                            </tr>
                             
                            <tr id="TimeRangeCell" style="display:none;">
                                <td align="left" class="blacktext"  nowrap >
                                <span class="blackblodtext" style="width:80PX"><b>Time Range</b></span>
                                   <div id="lstReportDiv" >
                            
                            <asp:DropDownList ID="lstReport" runat="server" onclick="javascript:return fnShow();"
                                CssClass="altSelectFormat" Width="120PX">
                                <asp:ListItem Selected="True" Value="1">Yesterday</asp:ListItem>
                                <asp:ListItem Value="2">Last week</asp:ListItem>
                                <asp:ListItem Value="3">Last month</asp:ListItem>
                                <asp:ListItem Value="4">This week</asp:ListItem>
                                <asp:ListItem Value="5">Year to Date</asp:ListItem>
                                <asp:ListItem Value="6">Custom Date</asp:ListItem>                                                
                            </asp:DropDownList>
                                  </div>
                                </td>
                             </tr>
                            
                            <tr id="tdCusFrmDte" style="display: none">
                                <td class="blacktext"  nowrap>
                                <font class="blackblodtext" style="width:110PX"><b>From Date</b></font>
                                   <div id="CustomDateDiv" nowrap>  
                                            <asp:TextBox ID="txtCusDateFrm" runat="server"  CssClass="altText" onblur="javascript:ChangeEndDate(0)"
                                                Width="80PX"></asp:TextBox>
                                            <img src="image/calendar.gif" style="width: 20; height: 20; vertical-align: bottom" alt="Date Selector"
                                                id="cal_triggerFrm" style="cursor: pointer;" title="Date selector" onblur="javascript:ChangeEndDate(0)"
                                                onclick="return showCalendar('txtCusDateFrm', 'cal_triggerFrm', 0, '<%=format%>');"/>
                                            <asp:RequiredFieldValidator ID="reqCusFrom" runat="server" ControlToValidate="txtCusDateFrm" Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="" ValidationGroup="DateSubmit"></asp:RequiredFieldValidator>    
                                    </div>
                                </td>
                            </tr>
                            <tr id="tdCusToDte" style="display: none">
                                <td class="blacktext"  nowrap>
                                <font class="blackblodtext" style="width:80PX"><b>To Date</b></font>
                                   <div id="CusDateToDiv" nowrap>
                                            <asp:TextBox ID="txtCusDateTo" runat="server" CssClass="altText" onblur="javascript:ChangeStartDate(0)"
                                                Width="80PX"></asp:TextBox>
                                            <img src="image/calendar.gif" style="width: 20; height: 20; vertical-align: bottom;cursor: pointer;" alt="Date Selector"
                                                id="cal_triggerTo" title="Date selector" onblur="javascript:ChangeStartDate(0)"
                                                onclick="return showCalendar('txtCusDateTo', 'cal_triggerTo', 0, '<%=format%>');" /> <%--ZD 100419--%>
                                                <asp:RequiredFieldValidator ID="reqCusTo" runat="server" ControlToValidate="txtCusDateTo" Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="" ValidationGroup="DateSubmit"></asp:RequiredFieldValidator>
                                   </div>
                                </td>
                            </tr>
                            <tr id="ChrtTypeCell"  style="display: none">
                                <td align="left" class="blacktext" nowrap>
                                <span class="blackblodtext" style="width:80PX"><b>Chart Type</b></span>
                               <div id="DrpChrtTypeDiv">
                                    <asp:DropDownList ID="DrpChrtType" runat="server" CssClass="altSelectFormat" Width="120PX" >
                                        <asp:ListItem Selected="True" Value="2">Bar</asp:ListItem>
                                        <asp:ListItem Value="3">Line</asp:ListItem>
                                        <asp:ListItem Value="1">Table</asp:ListItem>
                                        <%--<asp:ListItem Value="4">Pie</asp:ListItem>--%>
                                    </asp:DropDownList>
                              </div>
                            </td>
                         
                           </table>
                           </asp:Panel>
                            
                           
                           </td>
                           </tr>
                           <tr>
                                <td align="right">
                                    <asp:Button ID="btnviewRep" runat="server" Text="View Report" CssClass="altShortBlueButtonFormat" ValidationGroup="DateSubmit" OnClientClick="javascript:return fnSubmit()" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center"> 
                                    <asp:Button ID="btnAdvRep" runat="server" Text="View Advanced Report" CssClass="altLongBlueButtonFormat"  OnClientClick="javascript:return fnAdvanceReports('A')" Visible="false"/>&nbsp;&nbsp;
                                </td>
                            </tr>
                             <tr>
                                <td align="center"> 
                                    <asp:Button ID="Button1" runat="server" Text="Master-Child-Child Report" CssClass="altLongBlueButtonFormat"  OnClientClick="javascript:return fnAdvanceReports('M')" Visible="true"/>&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                    </td>                   
                    <td valign="top" width="70%">
                        <div id="dvControl" style="vertical-align:top;width:100%;height:470px;border-color:Blue;border-width:1px;border-style:Solid;">
                            <table id="Graph1" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:block;">
                                <tr>
                                <td>
                                <table width="730px" border="0">
                                <tr id="trGraph1M" runat="server" width="100%">
                                                <td align="center" >
                                                    <div style="Width:100%">
                                                    <table border="0" style="Width:100%">
                                                    <tr><td>
                                                    <h3><asp:Label ID="lblHeading" runat="server"  /></h3>
                                                    </td></tr>                                                    
                                                    </table>                                                                                           
                                                    </div>
                                                </td>
                                            </tr>
                                <tr id="trDateDisplay" runat="server" align="right">
                                            <td align="right">
                                               <span class="blackblodtext"> <asp:Label ID ="lblDate" runat="server" ></asp:Label></span>  
                                            </td>
                                        </tr>                                                                                   
                                <tr>
                                              <td align="center">
                                                  <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                                              </td>
                                            </tr>
                                 </table>
                                 </td>
                                </tr> <%--Edited for FF--%>
                                <tr>
                                    <td>
                                        <table align="center" style="width:100%;" border="0">
                                                
                                            <tr id="Chart1Row" runat="server" visible="false">
                                               <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart1" runat="server" Width="550px" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                            <asp:Series Name="Series2">
                                                            </asp:Series>
                                                            <asp:Series Name="Series3">
                                                            </asp:Series>
                                                            <asp:Series Name="Series4">
                                                            </asp:Series>
                                                        </Series>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>
                                            <tr id="Chart13Row" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart13" runat="server" Width="550px" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                        </Series>
                                                        <Titles>
                                                            <asp:Title Name="Location" Font="Arial" ForeColor="Black">
                                                            </asp:Title>
                                                        </Titles>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                            
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>                                            
                                            <tr id="Chart19Row" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart19" runat="server" Width="550PX" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                        </Series>
                                                        <Titles>
                                                            <asp:Title Name="MCU" Font="Arial" ForeColor="Black">
                                                            </asp:Title>
                                                        </Titles>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>
                                            <tr id="Chart7Row" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart7" runat="server" Width="550px" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                        </Series>
                                                        <Titles>
                                                            <asp:Title Name="Conference">
                                                            </asp:Title>
                                                        </Titles>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>
                                            <tr id="Chart8Row" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart8" runat="server" Width="550px" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                        </Series>
                                                        <Titles>
                                                            <asp:Title Name="Conference">
                                                            </asp:Title>
                                                        </Titles>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>
                                            <tr id="Chart9Row" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart9" runat="server" Width="550px" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                        </Series>
                                                        <Titles>
                                                            <asp:Title Name="Conference">
                                                            </asp:Title>
                                                        </Titles>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>
                                            <tr id="Chart10Row" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart10" runat="server" Width="550px" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                        </Series>
                                                        <Titles>
                                                            <asp:Title Name="Conference">
                                                            </asp:Title>
                                                        </Titles>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>
                                            <tr id="Chart11Row" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart11" runat="server" Width="550px" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                        </Series>
                                                        <Titles>
                                                            <asp:Title Name="Conference">
                                                            </asp:Title>
                                                        </Titles>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>
                                            <tr id="Chart12Row" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:Chart ID="Chart12" runat="server" Width="550px" Visible="false">
                                                        <Legends>
                                                            <asp:Legend LegendStyle="Column" IsTextAutoFit="False" Enabled="true" DockedToChartArea="ChartArea1"
                                                                Docking="Right" IsDockedInsideChartArea="false" Name="legend2" BackColor="Transparent"
                                                                Font="Trebuchet MS, 7.25pt, style=Bold" Alignment="Near">
                                                            </asp:Legend>
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Series1">
                                                            </asp:Series>
                                                        </Series>
                                                        <Titles>
                                                            <asp:Title Name="Conference">
                                                            </asp:Title>
                                                        </Titles>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>                  
                                            <tr id="tbReportsRow" runat="server" visible="false">
                                                <td align="left" colspan="2">
                                                <div style="overflow-x:auto;Width:72%;height:415px;position:absolute;" runat="server" id="divTab">
                                                    <asp:Table runat="server" ID="tbReports" Visible="false" CellPadding="1" CellSpacing="0" Width="100%" >
                                                    </asp:Table> 
                                                    </div>                  
                                                </td>
                                              </tr>                                                  
                                            <%-- UI Changes for FB 1750 SQL Reports Start--%>
                                             <tr runat="server" id="CalendarRow" style="display:none;">
                                                   <td valign="top">
                                                    <div style="overflow-x:auto;Width:72%;height:340px;position:absolute;"><%--Edited for FF--%>
                                                        <asp:DataGrid id="CalendarGrid" Width="100%" HorizontalAlign="Center" runat="server" AllowPaging="True" PageSize="13" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Left"
                                                            PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                            GridLines="Both" CellPadding="3" ItemStyle-Wrap="false" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-Font-Bold="true"  AutoGenerateColumns="false" CssClass="tableBody">
                                                            <HeaderStyle CssClass="tableHeader" />
                                                            <Columns>
                                                              <asp:BoundColumn HeaderText="Date" ItemStyle-Wrap="false"> <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Start" ItemStyle-Wrap="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="End" ItemStyle-Wrap="false" > <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Duration" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Right"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Room" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Meeting Title" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Last Name" HeaderStyle-Wrap ="false" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="First Name" HeaderStyle-Wrap ="false" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="email" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="State" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="City" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Conf #" HeaderStyle-Wrap="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Meeting Type" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Connection Type" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Protocol" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Status" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Remarks" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                            </Columns>
                                                        </asp:DataGrid>
                                                       </div>
                                                        
                                                   </td>
                                              </tr>    
                                              <tr runat="server" id="DailyScheduleRow" style="display:none;" >
                                                   <td >
                                                    <div style="overflow-x:auto;Width:72%;height:380px;position:absolute;"> <%--Edited for FF--%>
                                                        <asp:DataGrid id="DailyScheduleGrid" Width="100%" HorizontalAlign="Center" runat="server" AllowPaging="True" PageSize="17" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Left"
                                                            PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                            GridLines="Both" CellPadding="3" CellSpacing="0" HeaderStyle-Font-Bold="true" AutoGenerateColumns="false" CssClass="tableBody">
                                                            <HeaderStyle CssClass="tableHeader"  />
                                                            <Columns>
                                                              <asp:BoundColumn HeaderText="Date" ItemStyle-CssClass="ReportFont" ItemStyle-Wrap="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>             
                                                              <asp:BoundColumn HeaderText="Start" ItemStyle-CssClass="ReportFont" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>             
                                                              <asp:BoundColumn HeaderText="End" ItemStyle-CssClass="ReportFont" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>               
                                                              <asp:BoundColumn HeaderText="Room" ItemStyle-CssClass="ReportFont" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>              
                                                              <asp:BoundColumn HeaderText="Meeting Title" ItemStyle-CssClass="ReportFont" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>     
                                                              <asp:BoundColumn HeaderText="Last Name" ItemStyle-CssClass="ReportFont" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>  
                                                              <asp:BoundColumn HeaderText="Conf#" ItemStyle-CssClass="ReportFont"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                              <asp:BoundColumn HeaderText="Remarks" ItemStyle-CssClass="ReportFont" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>  
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        </div>
                                                   </td>
                                              </tr>
                                              <tr runat="server" id="ContactListRow" style="display:none;">
                                                  <td> 
                                                        <div style="overflow-x:auto;Width:72%;height:375px;position:absolute;"> <%--Edited for FF--%>                                                    
                                                       <asp:DataGrid id="ContactListGrid" HorizontalAlign="Center" Width="100%" runat="server" AllowPaging="True" PageSize="10" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Left"
                                                            PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                            GridLines="Both" CellPadding="3" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-BackColor="Gold"  AutoGenerateColumns="false" CssClass="tableBody">
                                                            <Columns>
                                                              <asp:BoundColumn HeaderText="State"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                                                             
                                                              <asp:BoundColumn HeaderText="City" HeaderStyle-Wrap ="false" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>             
                                                              <asp:BoundColumn HeaderText="Room" HeaderStyle-Wrap ="false" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>             
                                                              <asp:BoundColumn HeaderText="VoIP #"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                                                           
                                                              <asp:BoundColumn HeaderText="ISDN #"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                                                           
                                                              <asp:BoundColumn HeaderText="Contact Name" HeaderStyle-Wrap ="false" ItemStyle-Wrap="false"> <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>   
                                                              <asp:BoundColumn HeaderText="Contact #"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                                                        
                                                              <asp:BoundColumn HeaderText="VTC Unit Name" HeaderStyle-Wrap ="false" ItemStyle-Wrap="false"> <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>  
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        </div>
                                                  </td>
                                              </tr>
                                              <tr runat="server" id="PRIAllocationGRow" style="display:none;">
                                                <td>
                                                    <div style="overflow-x:auto;Width:72%;height:375px;position:absolute;">
                                                    <asp:DataGrid id="PRIAllocationGrid" HorizontalAlign="Center" Width="100%" runat="server" AllowPaging="True" PageSize="14" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Left"
                                                            PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                            GridLines="Both" CellPadding="3"  HeaderStyle-CssClass="verticaltext" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" AutoGenerateColumns="false" CssClass="tableBody">
                                                            <HeaderStyle CssClass="tableHeader" Wrap="false" />
                                                            <Columns>
                                                              <asp:BoundColumn HeaderText="Date" HeaderStyle-Height="25px"><HeaderStyle CssClass="tableHeader"  /></asp:BoundColumn>                                                               
                                                              <asp:BoundColumn HeaderText="PRI"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                                                                
                                                              <asp:BoundColumn HeaderText="Meeting Title" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>      
                                                              <asp:BoundColumn HeaderText="00:00" HeaderStyle-CssClass="verticaltext" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                         
                                                              <asp:BoundColumn HeaderText="00:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="01:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="01:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="02:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="02:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="03:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="03:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="04:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="04:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="05:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="05:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="06:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="06:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="07:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="07:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="08:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="08:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="09:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="09:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="10:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="10:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="11:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="11:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="12:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="12:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="13:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="13:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="14:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="14:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="15:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="15:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="16:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="16:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="17:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="17:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="18:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="18:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="19:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="19:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="20:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="20:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="21:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="21:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="22:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="22:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="23:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                              <asp:BoundColumn HeaderText="23:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </td>
                                              </tr>
                                              <tr runat="server" id="ResourceAllocationReportRow" style="display:none;">
                                                <td>
                                                    <div style="overflow-x:auto;Width:72%;height:375px;position:absolute;">
                                                    <asp:DataGrid id="ResourceAllocationReportGrid" HorizontalAlign="Center" Width="100%" runat="server" AllowPaging="True" PageSize="13" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Left"
                                                        PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                        GridLines="Both" CellPadding="3"  HeaderStyle-CssClass="verticaltext" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" AutoGenerateColumns="false" CssClass="tableBody">
                                                            <HeaderStyle CssClass="tableHeader" Wrap="false" />
                                                        <Columns>
                                                          <asp:BoundColumn HeaderText="Date"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                          <asp:BoundColumn HeaderText="Start" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                          <asp:BoundColumn HeaderText="End" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                          <asp:BoundColumn HeaderText="Room" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                          
                                                          <asp:BoundColumn HeaderText="Meeting Title" ItemStyle-Wrap="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>                 
                                                          <asp:BoundColumn HeaderText="00:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>           
                                                          <asp:BoundColumn HeaderText="00:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="01:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="01:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="02:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="02:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="03:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="03:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="04:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="04:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="05:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="05:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="06:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="06:30" HeaderStyle-CssClass="verticaltext" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>           
                                                          <asp:BoundColumn HeaderText="07:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="07:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="08:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="08:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="09:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="09:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="10:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="10:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="11:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="11:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="12:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="12:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="13:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="13:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="14:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="14:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="15:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="15:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="16:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="16:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="17:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="17:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="18:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="18:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="19:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="19:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="20:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="20:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="21:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="21:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="22:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="22:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="23:00" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                          <asp:BoundColumn HeaderText="23:30" HeaderStyle-CssClass="verticaltext"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    </div>
                                                </td>
                                              </tr>
                                              <tr runat="server" id="AggregateUsageRow" style="display:none;">
                                                   <td>
                                                        <div style="overflow-x:auto;Width:70%;height:435px;position:absolute;">
                                                        <asp:DataGrid id="AggregateUsageGrid" Width="100%" HorizontalAlign="left" runat="server"  BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                            GridLines="Both" CellPadding="3" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Right" AutoGenerateColumns="false" CssClass="tableBody">
                                                            <Columns>
                                                              <asp:BoundColumn HeaderText="Total Audio Conferences" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Total Audio/Video Conferences" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn> <%-- ZD 100528--%>          
                                                              <asp:BoundColumn HeaderText="Total Point-to-Point Conferences" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Total Room Conferences"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Total Conferences"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                              <asp:BoundColumn HeaderText="Total Duration(hrs)"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn> <%--ZD 100528--%>          
                                                              <asp:BoundColumn HeaderText="Total Participants"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        </div>
                                                   </td>
                                              </tr>
                                                   <tr ><td><table border="0" align="center" style="width:50%;"> <%--Edited for FF--%>                                              
                                              <tr runat="server" id="UsageByRoomRow" style="display:none;">
                                                   <td align="center">
                                                        <div id="Div2" align="center" runat="server" >
                                                            <asp:DataGrid id="UsageByRoomGrid" Width="85%" HorizontalAlign="Center" runat="server" AllowPaging="True"  PageSize="15" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Left"
                                                                PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                                GridLines="Both" CellPadding="1" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-Font-Bold="true"  AutoGenerateColumns="false" CssClass="tableBody"><%--Edited for FF--%>
                                                                <Columns>
                                                                  <asp:BoundColumn HeaderText="Room Name" HeaderStyle-Wrap ="false" ItemStyle-Width="55%" ItemStyle-HorizontalAlign="left"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>        <%--Edited for FF--%>
                                                                  <asp:BoundColumn HeaderText="Total Minutes" HeaderStyle-Wrap ="false" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Right"> <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>            
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </div>
                                                   </td>
                                              </tr>
                                                   </table></td></tr>
                                              
                                             
                                            <%-- UI Changes for FB 1750 SQL Reports End--%>
                                                                     
                                        </table>
                                    </td>
                                </tr>
                            </table>
                         </div>                         
                    </td>
                </tr>
                <tr><%--Added for FF--%>
                <td></td>
                <td align="left">
                        <div id="SummaryDiv" style="display:none;vertical-align:middle">
                         <br /> 
                          <%--Organixation/CSS Module--%>
                         <span class="blackblodtext">Summary:</span>
                        <table id="SummaryTable" runat="server" class="tableBody" style="border-style:solid;border-width:1px;" border="1" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td>
                                     <span class="blackblodtext">Meetings Summary</span>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><span class="blackblodtext">Pt-Pt Meetings</span></td>
                                <td width="10%"> <asp:Label ID="PtPtCnt" runat="server">&nbsp;</asp:Label> </td>
                                <td> &nbsp;</td>
                            </tr>
                              <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><span class="blackblodtext">Multi-Pt Meetings </span></td>
                                <td><asp:Label ID="MultiPtCnt" runat="server">&nbsp;</asp:Label></td>
                                <td>&nbsp;</td>
                            </tr>
                             <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><span class="blackblodtext">VTC Meeting Totals</span></td>
                                <td><asp:Label ID="VTCMtTotCnt" runat="server">&nbsp;</asp:Label></td>
                                <td>&nbsp; </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><span class="blackblodtext">Internal Meetings</span></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><span class="blackblodtext">Meetings</span> </td>
                            </tr>
                        </table>
                        
                        <br />
                        <table id="DetailsHeadingTable" runat="server" width="80%" class="tableHeader" style="border-width:1px;" border="1" cellpadding="3" cellspacing="0">
                            <tr align="center" style="font-weight:bold;">
                                <td width="12%">&nbsp;</td>
                                <td width="10%">Mins</td> <%--ZD 100528--%>
                                <td width="10%">Hrs</td> <%--ZD 100528--%>
                                <td width="10%">Connects</td>
                                <td width="10%">ISDN Outbound <br />(hrs)</td> <%--ZD 100528--%>
                                <td width="10%">ISDN Outbound <br /> %</td>
                                <td width="10%">VoIP (hrs)</td> <%--ZD 100528--%>
                                <td width="10%">VoIP %</td>
                                <td width="10%">Inbound ISDN <br /> (hrs)</td> <%--ZD 100528--%>
                            </tr>
                       </table>
                       <div id="DetailsDiv"  runat="server" style="display:none;Width:80%">
                           <asp:Table ID="VTCDetails" runat="server"  class="tableBody" Width="100%" style="border-style:solid;border-width:1px;" border="1" cellpadding="3" cellspacing="0" ></asp:Table>                     
                       </div>
                           <asp:Table ID="VTCTotal" runat="server" class="tableBody" Height="60%" Width="80%" style="border-style:solid;border-width:1px;" border="1" cellpadding="3" cellspacing="0"> <%--Edited for FF--%>
                                <asp:TableRow ID="FirstRow" runat="server" HorizontalAlign="Right">
                                    <asp:TableCell Width="12%" > 
                                        <asp:ImageButton ID="imgDiv" runat="server" ImageUrl="image/loc/nolines_plus.gif" OnClientClick="Javascript:return fnShowImg()"  AlternateText="Expand"/><%--ZD 100419--%>
                                         VTC Totals
                                    </asp:TableCell>
                                </asp:TableRow>
                           </asp:Table>
                       </div>
                
                </td>
                </tr><%--Added for FF--%>
                <tr id="pdftr"><%--Added for Graphical reports--%>
                    <td>
                        <asp:TextBox  style="display: none" ID="tempText" TextMode="multiline" Rows="4" runat="server" width="0" Height="0" ForeColor="black" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="right" class="blackblodtext">
            <br />
          <span class="blackblodtext">   Report generated on : </span>
            <asp:Label ID="lblCur" runat="server" Text=""></asp:Label>
        </td>                   
    </tr>
    </table>
    </form>
</body>
</html>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
 
 <script type="text/javascript">
        function fnAdvanceReports(param)
        {
            if(param == "A")
                window.location.replace('managereports.aspx');
            else if(param == "M")
                window.location.replace('MasterChildReport.aspx');
            return false;
        }
    </script>

<script type="text/javascript">

    fnShowMenu();
    fnAssignDefault('S');
    fnShow();
    
     //added -- Graphical Reports --Start
    function pdfReport()
    {
        var reportsList = document.getElementById("ReportsList");
        var drpChrtType = document.getElementById("DrpChrtType");
        if(reportsList)        
        {
              if(reportsList.value == "4" && drpChrtType.value != "1")
              {
        
                    var loc = document.location.href; 
                    loc = loc.substring(0,loc.indexOf("GraphicalReport.aspx"));

                    //Start
                     var hdnchartname = document.getElementById("hdnChartName");
                     var src = "";
                     var srcAry;
                     var path = '<%=Session["OrgCSSPath"]%>';
                     path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
                     if(hdnchartname != "")                     
                     {                                                
                        var img1 = document.getElementById(hdnchartname.value);
                        src = img1.src;
                        srcAry = src.split("?");        
                        img1.src = loc + "/image/Chart1.jpeg?" + srcAry[1];      
                     }
                     //End
                     
                    var htmlString = document.getElementById("Table8").innerHTML;
                    
                    //remove Control table from PDF
                    var toBeRemoved = document.getElementById("tblControls");
                    
                   
                    if (toBeRemoved != null)
                        htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
                        
                    var toBeRemoved = document.getElementById("tbleBtns");
                        
                    if (toBeRemoved != null)
                        htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
                     
                 
                     htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + path +"'><body><center><table><tr><td></td></tr></table>" + htmlString + "</center></body></html>";
                    if (document.getElementById("tempText") != null)
                    {
                        document.getElementById("tempText").value = "";
                        document.getElementById("tempText").value = htmlString;
                    }
                    
                    return true;
            }
        }
    }
    function DataLoading(val)
    {
    if (val=="1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    } 
    
    var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(initializeRequest);

        prm.add_endRequest(endRequest);

        var postbackElement;
          
      function initializeRequest(sender, args) 
      {
        //document.body.style.cursor = "wait";
        DataLoading(1);
    }

    function endRequest(sender, args) 
    {
        document.body.style.cursor = "default";
        DataLoading(0);
    }
    
</script>

<script type="text/javascript" src="inc/softedge.js"></script>


