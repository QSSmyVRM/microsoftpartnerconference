﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using RTC_v181.WhyGoServices;
using cryptography;
using System.Text.RegularExpressions;

namespace NS_WhyGO
{
    class WhyGo
    {
        public enum CurrencyType {AUD =1 , GBP, USD };

        #region Parameters
        WhygoServiceClient client;
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;
        internal string ErrMsg = "";
        string URL = "http://localhost/api/WhygoService.svc";
        string UserName = "myvrm";
        string Password = "myvrm";
        private NS_MESSENGER.SysSettings sysSettings;
        System.ServiceModel.EndpointAddress whygoAddress;
        int whygoUserID = 1394;
        #endregion

        #region constructor
        internal WhyGo(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
            IntegrateWhygoServer();
        }
        #endregion


        private bool IntegrateWhygoServer()
        {
            try
            {
                ErrMsg = "";
                db.FetchSysSettingsDetails(ref sysSettings);
                
                URL = sysSettings.WhyGoURL.Trim();
                UserName = sysSettings.WhyGoUserName.Trim();
                Password = sysSettings.WhyGoPassword.Trim();
                whygoUserID = sysSettings.WhygoUserID;
                if (URL == "" || UserName == "")
                {
                    ErrMsg = "Error in WhyGo Credentials, URL:" + sysSettings.WhyGoURL +"UserName:" +sysSettings.WhyGoUserName;
                    logger.Trace(ErrMsg);
                    return false;
                }
                logger.Trace("FetchSysSettings:" + URL);

                whygoAddress =  new System.ServiceModel.EndpointAddress(URL);
                logger.Trace("URL:" + URL);
                client = new WhygoServiceClient("WSHttpBinding_IWhygoService");
                logger.Trace("UserName:" + UserName);               
                client.Endpoint.Address = whygoAddress;
                client.ClientCredentials.UserName.UserName = UserName;
                client.ClientCredentials.UserName.Password = Password;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = 0;//X509CertificateValidationMode.None;  
                client.Endpoint.Binding.CloseTimeout = new TimeSpan(0, 10, 0);
                client.Endpoint.Binding.ReceiveTimeout= new TimeSpan(0, 10, 0);
                client.Endpoint.Binding.OpenTimeout = new TimeSpan(0, 10, 0);
                client.Endpoint.Binding.SendTimeout = new TimeSpan(0, 10, 0);
                logger.Trace("Password:" + Password);               
            }
            catch(Exception ex)
            {
                logger.Trace("IntegrateWhygoServer Exception:" + ex.StackTrace);
                return false;
            }
            return true;
        }

        #region GetLocationUpdate
        /// <summary>
        /// GetLocationUpdate
        /// </summary>
        /// <param name="InXML"></param>
        /// <param name="RecieveXML"></param>
        /// <returns></returns>
        internal bool GetLocationUpdate(ref string InXML, ref string outXML, bool updateConfs)
        {
            double offsetAddition = 0;
            int offset = 5, Linerate = 384; //FB 3069
            List<NS_MESSENGER_CUSTOMIZATIONS.Room> rooms = null;
            DateTime PublicRoomLastModified = DateTime.UtcNow;

            try
            {
                if (client == null)
                {
                    outXML = "<errorCode>100</errorCode><error>Whygo intilization failed.Please check Whygo config.</error>";
                    return true; 
                }

                rooms = new List<NS_MESSENGER_CUSTOMIZATIONS.Room>();
                db.GetPublicRoomLastModified(ref PublicRoomLastModified);
                var locationDetails = client.getLocationUpdate(PublicRoomLastModified.ToString("s").Trim() + "Z", whygoUserID); //FB 2543
                #region Bind Public Rooms
                NS_MESSENGER_CUSTOMIZATIONS.Room room = null;

                if (locationDetails.Length <= 0)
                {
                    ErrMsg = "Rooms are upto date or No rooms to poll.";
                    return false; 
                }

                for (int i = 0; i < locationDetails.Length; i++)
                {
                    try
                    {
                        locationDetails[i] = (RTC_v181.WhyGoServices.GetLocationDetailsResponse) db.SetPropertyValues((object)locationDetails[i]);
                        rooms = new List<NS_MESSENGER_CUSTOMIZATIONS.Room>();
                        room = new NS_MESSENGER_CUSTOMIZATIONS.Room();
                        offsetAddition = 0;

                        //if its ISDN capable, ISDN no & max speed, if its IP capable, IP no and max speed
                        room.WhygoRoomId = locationDetails[i].locationID;
                        room.Address1 = locationDetails[i].address.Trim();   //Endpoint start
                        room.AUXEquipment = locationDetails[i].equipmentDescription;
                        room.speed = locationDetails[i].speed; //Endpoint end this needs to be set in future by ipspeed and isdn speed
                        room.Type = locationDetails[i].type;
                        room.extraNotes = locationDetails[i].extraNotes;
                        room.genericSellPrice = locationDetails[i].genericSellPrice;
                        room.geoCodeAddress = locationDetails[i].geoCodeAddress;
                        room.isAutomatic = Convert.ToInt32(locationDetails[i].isAutomatic);
                        room.isHDCapable = Convert.ToInt32(locationDetails[i].isHDCapable);
                        room.isInternetCapable = Convert.ToInt32(locationDetails[i].isInternetCapable);
                        room.isInternetFree = Convert.ToInt32(locationDetails[i].isInternetFree);
                        room.isIPCapable = Convert.ToInt32(locationDetails[i].isIPCapable);
                        room.isIPCConnectionCapable = Convert.ToInt32(locationDetails[i].isIPCConnectionCapable);
                        room.isIPDedicated = Convert.ToInt32(locationDetails[i].isIPDedicated);
                        room.isTP = Convert.ToInt32(locationDetails[i].isTP);
                        room.isVCCapable = Convert.ToInt32(locationDetails[i].isVCCapable);
                        room.isISDNCapable = Convert.ToInt32(locationDetails[i].isISDNCapable); 
                        room.CurrencyType = locationDetails[i].resellerCurrency;
                        room.internetBiller = locationDetails[i].internetBiller;
                        room.RoomDescription = locationDetails[i].description;
                        locationDetails[i].locationName = Regex.Replace(locationDetails[i].locationName, @"[\\<>^+?|!`\[\]{}\=@$%&~',]", " "); //ZD 103424
                        room.name = locationDetails[i].locationName;
                        room.Capacity = locationDetails[i].capacity;
                        try
                        {
                            if (locationDetails[i].stateName != "")
                            {
                                room.StateName = locationDetails[i].stateName;
                                room.CountryName = locationDetails[i].countryName;



                                var state = (from states in RTC_v181.WhygoConstants.StatesDict
                                             where ((KeyValuePair<int,string>)states.Key).Value.ToString().ToUpper().Trim() == locationDetails[i].stateName.ToUpper().Trim()
                                                 select states).FirstOrDefault();

                                room.State = ((KeyValuePair<int, string>)state.Key).Key.ToString();
                                room.Country = state.Value.ToString();
                                //ZD 100888 Starts
                                var country = (from countries in RTC_v181.WhygoConstants.CountriesDict
                                               where countries.Value.ToString().ToUpper().Trim() == locationDetails[i].countryName.ToUpper().Trim()
                                               select (countries.Key).Value.ToString()).FirstOrDefault();
                                room.myVRMID = country.ToString();
                                //ZD 100888 Ends
                            }

                        }
                        catch (Exception)
                        { }
                        
                        room.City = locationDetails[i].citySuburb;
                        room.videoAvailable = 2; // Type name?
                        room.RoomPhone = locationDetails[i].generalPhone;
                        room.Zipcode = locationDetails[i].zipcode;
                        room.Maplink = locationDetails[i].imageLocation;
                        

                        room.IsEarlyHoursEnabled = Convert.ToInt32(locationDetails[i].isEarlyHoursSupported);
                        room.EHStartTime = locationDetails[i].earlyHoursStart;
                        room.EHEndTime = locationDetails[i].earlyHoursEnd;
                        room.EHCost = locationDetails[i].earlyHoursPrice;
                        room.EHFullyAuto = 0; //locationDetails[i].afterHoursPrice; //FB 2543

                        room.openHours = locationDetails[i].openHours;
                        room.OHStartTime = locationDetails[i].officeHoursStart;
                        room.OHEndTime = locationDetails[i].officeHoursEnd;
                        room.OHCost = locationDetails[i].officeHoursPrice;

                        room.IsAfterHourEnabled = Convert.ToInt32(locationDetails[i].isAfterHoursSupported);
                        room.AHStartTime = locationDetails[i].afterHoursStart;
                        room.AHEndTime = locationDetails[i].afterHoursEnd;
                        room.AHCost = locationDetails[i].afterHoursPrice;
                        room.AHFullyAuto = 0;// locationDetails[i].afterHoursPrice; //FB 2543

                        room.isCrazyHoursSupported = Convert.ToInt32(locationDetails[i].isCrazyHoursSupported);
                        room.CHtartTime = locationDetails[i].crazyHoursStart;
                        room.CHEndTime = locationDetails[i].crazyHoursEnd;
                        room.CHFullyAuto = 0; // locationDetails[i].crazyHoursEnd; //FB 2543
                        if (locationDetails[i].crazyHoursPrice == "")
                            locationDetails[i].crazyHoursPrice = "0";
                        room.CHCost = Convert.ToDecimal(locationDetails[i].crazyHoursPrice);

                        room.Is24HoursEnabled = Convert.ToInt32(locationDetails[i].isAutomated24x7);
                        room.PublicRoomLastModified = DateTime.Now;

                        try
                        {
                            if (locationDetails[i].locationTimeZone.EndsWith("30"))
                                offsetAddition = .5;

                            offset = (Convert.ToInt32(locationDetails[i].locationTimeZone.Replace('"', ' '))) / 100;

                            room.timezoneOffset = Convert.ToDouble(-(offset + offsetAddition));


                        }
                        catch (Exception)
                        { }

                        room.isDST = Convert.ToInt32(locationDetails[i].hasDST);
                        room.isdnAddress = locationDetails[i].ISDNNumber;
                        room.ISDNSpeed = locationDetails[i].ISDNSpeed.ToLower().Replace("kbps", "").Replace("mbps", "");
                        int.TryParse(room.ISDNSpeed, out Linerate);
                        if (Linerate < 10)
                            Linerate = Linerate * 1024;
                        room.ISDNSpeed = Linerate.ToString();
                        if (room.ISDNSpeed == "" || room.ISDNSpeed == "0") //FB 3069
                            room.ISDNSpeed = "384";
                        room.ipAddress = locationDetails[i].IPAddress;
                        room.IPSpeed = locationDetails[i].IPspeed.ToLower().Replace("kbps", "").Replace("mbps", "");
                        int.TryParse(room.IPSpeed, out Linerate);
                        if (Linerate < 10)
                            Linerate = Linerate * 1024;
                        room.IPSpeed = Linerate.ToString();
                        if (room.IPSpeed == "" || room.IPSpeed == "0") //FB 3069
                            room.IPSpeed = "384";
                        room.latitude = locationDetails[i].latitude;
                        room.longitude = locationDetails[i].longitude;
                        //logger.Trace("whygo Room ID,Latitude,longitude" + locationDetails[i].locationID.ToString() + "," + locationDetails[i].latitude + "," + locationDetails[i].longitude);
                        if (room.ipAddress.Trim() == "" && room.isdnAddress.Trim() == "")
                            room.videoAvailable = 0;
                        rooms.Clear();
                        rooms.Add(room);

                        if (!db.SaveUpdatePublicRooms(ref rooms, ref ErrMsg, updateConfs))
                            logger.Trace("whygo Room " + locationDetails[i].locationName + " not updated properly");
                    }
                    catch (Exception ex1)
                    {
                        logger.Trace("GetLocationUpdate room error:" + ex1.StackTrace);
                    }

                }
                #endregion

                outXML = "<success>Operation succesful.</success>";

                
            }
            catch (Exception ex)
            {
                logger.Trace("GetLocationUpdate" + ex.StackTrace);
                ErrMsg = ex.Message;
                if (ErrMsg.ToLower().Contains("sequence contains no elements"))
                    ErrMsg = "Rooms are upto date";
                return false;
            }
            return true;

        }
        #endregion

        #region GetPublicRoomsAvailability
        /// <summary>
        /// GetPublicRoomsAvailability
        /// </summary>
        /// <param name="InXML"></param>
        /// <param name="RecieveXML"></param>
        /// <returns></returns>
        internal bool GetPublicRoomsAvailability(ref string InXML, ref string outXML)
        {
            string[] SelectedLoc = null;
            int[] locationIds = null;
            string stSet = "AM";
            DateTime Stdate = DateTime.Now;
            DateTime Enddate = Stdate.AddMinutes(1);
            int i = 0, hr = 12, min = 0, durationMin = 15, Avail = 1;
            XmlDocument xmldoc = null;
            int myVRMLocationID = 0;
            int timezoneID = 0;
            DateTime ConvertedDate = DateTime.MinValue;
            Dictionary<int,int> locLst = null;
            try
            {
                if (client == null)
                {
                    outXML = "<errorCode>100</errorCode><error>Whygo intilization failed.Please check Whygo config.</error>";
                    return true;
                }


                xmldoc = new XmlDocument();
                logger.Trace("GetPublicRoomsAvailability InXML" + InXML);
                xmldoc.LoadXml(InXML);
                if (xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/selectedLocationID") == null)
                {
                    logger.Trace("LocationID is null or empty");
                    return false;
                }

                if (xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/selectedLocationID").InnerText.Trim() == "")
                {
                    logger.Trace("LocationID is null or empty");
                    return false;
                }

                SelectedLoc = xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/selectedLocationID").InnerText.Trim().Split(',');
                locationIds = new int[SelectedLoc.Length];
                for (i = 0; i < SelectedLoc.Length; i++)
                {
                    int.TryParse(SelectedLoc[i].Trim(), out myVRMLocationID);
                    locationIds[i] = db.GetPublicRoomID(myVRMLocationID);
                    if (locLst == null)
                        locLst = new Dictionary<int, int>();

                    locLst.Add(locationIds[i], myVRMLocationID);
                }

                locationIds = locationIds.Where(x => x > 0).ToArray();

                if (locationIds.Length <= 0)
                    return false;

                if (xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/startDate") != null)
                    DateTime.TryParse(xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/startDate").InnerText.Trim(), out Stdate);

                if (xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/startSet") != null)
                    stSet = xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/startSet").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/startMin") != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/startMin").InnerText.Trim(), out min);

                if (xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/startHour") != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/startHour").InnerText.Trim(), out hr);

                if (xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/durationMin") != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/durationMin").InnerText.Trim(), out durationMin);
                
                if (stSet == "PM")
                    hr += 12;

                if (xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/timeZone") != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetPublicRoomsAvailability/timeZone").InnerText.Trim(), out timezoneID);

                Stdate = new DateTime(Stdate.Year, Stdate.Month, Stdate.Day, hr, min,0);
                logger.Trace("SelectedLocation ID" + locationIds + "StartTime:  " + Stdate.ToString("s") + "  EndTime:" + Stdate.AddMinutes(durationMin).ToString("s"));

                ConvertedDate = db.ConverttoGMT(Stdate, timezoneID);
                outXML = "<GetPublicRoomsAvailability>"
                          + "<Locationlist>";

                if (ConvertedDate > DateTime.MinValue)
                {

                    GetAvailabilityResponse availResponse = client.getAvailability(locationIds, ConvertedDate.ToString("s").Trim() + "Z", ConvertedDate.AddMinutes(durationMin).ToString("s").Trim() + "Z");

                   
                    for (i = 0; i < availResponse.locations.Length; i++)
                    {
                        myVRMLocationID = -1;
                        if (locLst[availResponse.locations[i].locationID] != null)
                            myVRMLocationID = locLst[availResponse.locations[i].locationID];
                        
                        Avail = 0;
                        if (availResponse.locations[i].isTotallyFree)
                            Avail = 1;

                        outXML += "<Location>";
                        outXML += "<LocationID>" + myVRMLocationID + "</LocationID>";
                        outXML += "<Available>" + Avail + "</Available>";
                        outXML += "</Location>";
                    }                    
                }

                outXML += "</Locationlist>"
                           + "</GetPublicRoomsAvailability>";

                logger.Trace("GetPublicRoomsAvailability OutXML :" + outXML);

            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                logger.Trace("GetPublicRoomsAvailability" + ex.StackTrace);
                /*
                outXML = "<GetPublicRoomsAvailability>"
                      + "<Locationlist>";
                for (int j = 0; j < SelectedLoc.Length; j++)
                {
                    outXML += "<Location>";
                    outXML += "<LocationID>" + SelectedLoc[j] + "</LocationID>";
                    outXML += "<Available>1</Available>";
                    outXML += "</Location>";
                }
                outXML += "</Locationlist>"
                       + "</GetPublicRoomsAvailability>";*/

                return false;
            }
            return true;

        }
        #endregion

        #region GetPublicRoomsPrices
        /// <summary>
        /// GetPublicRoomsPrices
        /// </summary>
        /// <param name="InXML"></param>
        /// <param name="RecieveXML"></param>
        /// <returns></returns>
        internal bool GetPublicRoomsPrices(ref string InXML, ref string outXML)
        {
            
            int myVRMLocationID = 0;
            PriceListResponse priceResponse = null;
            string CurrencyType = "AUD";
            int userID = 21, locationId = 0 , whygoUserID = 21;
            XmlDocument xmldoc = new XmlDocument();
            NS_MESSENGER_CUSTOMIZATIONS.Room whygoRoom = null;
            try
            {
                if (client == null)
                {
                    outXML = "<errorCode>100</errorCode><error>Whygo intilization failed.Please check Whygo config.</error>";
                    return true;
                }
                
                //string testResponse = client.testDBConnection();
                
                logger.Trace("GetPublicRoomsPrices InXML" + InXML);
                xmldoc.LoadXml(InXML);
                if (xmldoc.SelectSingleNode("//GetPublicRoomsPrices/selectedLocationID") == null )
                {
                    logger.Trace("LocationID is null or empty");
                    return false;
                }

                if (xmldoc.SelectSingleNode("//GetPublicRoomsPrices/selectedLocationID").InnerText.Trim() == "")
                {
                    logger.Trace("LocationID is null or empty");
                    return false;
                }

                if (xmldoc.SelectSingleNode("//GetPublicRoomsPrices/userID") != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetPublicRoomsPrices/userID").InnerText.Trim(), out userID);

                if (xmldoc.SelectSingleNode("//GetPublicRoomsPrices/CurrencyType") != null)
                    CurrencyType = xmldoc.SelectSingleNode("//GetPublicRoomsPrices/CurrencyType").InnerText.Trim();

                String[] SelectedLoc = xmldoc.SelectSingleNode("//GetPublicRoomsPrices/selectedLocationID").InnerText.Trim().Split(',');

                db.GetWhygoUserDetails(userID, ref whygoUserID, ref CurrencyType);

                outXML = "<GetPublicRoomsPrices>"
                       + "<Locationlist>";
                for (int i = 0; i < SelectedLoc.Length; i++)
                {
                    userID = 21;//Harcoded for now as whygo has no user creation
                    int.TryParse(SelectedLoc[i].Trim(), out myVRMLocationID);
                    locationId = db.GetPublicRoomID(myVRMLocationID);
                    
                    if (locationId <= 0)
                    {
                        logger.Trace("Empty Location ID");
                        continue;
                    }
                    try
                    {
                        priceResponse = client.getPriceList(locationId, whygoUserID, CurrencyType);

                        outXML += "<Location>";
                        outXML += "<LocationID>" + myVRMLocationID + "</LocationID>";
                        outXML += "<afterHoursPrice>" + priceResponse.afterHoursPrice + "</afterHoursPrice>";
                        outXML += "<crazyHoursPrice>" + priceResponse.crazyHoursPrice + "</crazyHoursPrice>";
                        outXML += "<earlyHoursPrice>" + priceResponse.earlyHoursPrice + "</earlyHoursPrice>";
                        outXML += "<officeHoursPrice>" + priceResponse.officeHoursPrice + "</officeHoursPrice>";
                        outXML += "<CurrencyType>" + priceResponse.currency + "</CurrencyType>";
                        outXML += "</Location>";

                    }
                    catch (Exception)
                    {
                        whygoRoom = db.GetPublicRoom(myVRMLocationID);

                        outXML += "<Location>";
                        outXML += "<LocationID>" + myVRMLocationID + "</LocationID>";
                        outXML += "<afterHoursPrice>" + whygoRoom.AHCost + "</afterHoursPrice>";
                        outXML += "<crazyHoursPrice>" + whygoRoom.CHCost + "</crazyHoursPrice>";
                        outXML += "<earlyHoursPrice>" + whygoRoom.EHCost + "</earlyHoursPrice>";
                        outXML += "<officeHoursPrice>" + whygoRoom.OHCost + "</officeHoursPrice>";
                        outXML += "<CurrencyType>" + whygoRoom.CurrencyType + "</CurrencyType>";
                        outXML += "</Location>";
                    }
                   
                }
                outXML += "</Locationlist>"
                       + "</GetPublicRoomsPrices>";

                logger.Trace("GetPublicRoomsPrices OutXML :" + outXML);
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                logger.Trace("GetPublicRoomsPrices" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion

        #region PushtoWhyGO
        /// <summary>
        /// PushtoWhyGO
        /// </summary>
        /// <param name="InXML"></param>
        /// <param name="RecieveXML"></param>
        /// <returns></returns>
        internal bool PushtoWhyGO(ref string InXML, ref string RecieveXML)
        {
            int confId = 0, instanceId = 0;
            string strConfId = "",whygoCurrencyType = "";
            bool ret = false;
            NS_OPERATIONS.Operations ops = null;
            List<inBookingLocation> publicRooms = null;
            List<inBookingLocationPrivate> privateRooms = null;
            List<whyGOConference> confs = null;
            int userID = 11;  // Harcoded for now
            int confID = -1;
            //FB 2543 Starts
            BridgingDetails publicBridgingDetails = null; //FB 2558
            string orgContact="",orgPhone="",orgEmail="";
            //FB 2543 Ends
            string WhygoRoomIDs = "", stringConfID = ""; //ZD 100367
            string NewWhygoID = "";//ZD 100694
            //ZD 100729 Starts
            inMyVrmConfig myVRMConfig = null; 
            int FetchWhygoStatus = 0;
            CreateConferenceResponse vConfReponse = null;//ZD 100884
            //ZD 100729 Ends
            try
            {
                db.FetchWhygoStatus(ref FetchWhygoStatus);//ZD 100884
                if (FetchWhygoStatus == 1) //ZD 100729 
                {
                    XmlDocument xmldoc = new XmlDocument();

                    if (InXML.IndexOf("<error>") >= 0 || InXML.Trim() == "")
                    {
                        logger.Trace("SaveUpdateConferenceonWhyGO" + InXML);
                        ErrMsg = InXML;
                    }
                    else
                    {
                        xmldoc.LoadXml(InXML);
                        if (xmldoc.SelectSingleNode("//SetAdvancedAVSettings/ConfID") != null)
                        {
                            strConfId = xmldoc.SelectSingleNode("//SetAdvancedAVSettings/ConfID").InnerText;

                            #region parse the inxml
                            if (xmldoc.SelectSingleNode("//SetAdvancedAVSettings/UserID") != null)
                                int.TryParse(xmldoc.SelectSingleNode("//SetAdvancedAVSettings/UserID").InnerText.Trim(), out userID);


                            if (strConfId.IndexOf(",") > 0)
                            {
                                // Just in case ASP sends the confid in this format - 10,1 (confid,instanceid)					
                                ops = new NS_OPERATIONS.Operations(this.configParams);
                                ret = ops.ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                                if (!ret)
                                {
                                    logger.Trace("Invalid conf/instance id.");
                                    return false;
                                }
                            }
                            else
                            {
                                confId = int.Parse(strConfId);
                                instanceId = -1;
                            }
                            logger.Trace("Confid = " + confId.ToString() + ", InstanceID = " + instanceId.ToString());
                            #endregion
                            db.GetWhygoUserDetails(userID, ref whygoUserID, ref whygoCurrencyType);
                            db.FetchConfforWhygo(confId, instanceId, ref confs);


                            for (int i = 0; i < confs.Count; i++)
                            {
                                publicBridgingDetails = new BridgingDetails();//FB 2543 //FB 2558
                                stringConfID = confs[i].iDbID + "," + confs[i].iInstanceID; //ZD 100367
                                //ZD 100729 Starts
                                myVRMConfig = new inMyVrmConfig();
                                myVRMConfig.confId = stringConfID;
                                myVRMConfig.myVrmUsername = confs[i].sHostEmail;
                                myVRMConfig.myVrmPassword = confs[i].sHostPassword;
                                //ZD 100729 Ends
                                db.FetchRoomsforWhygo(confs[i].iDbID, confs[i].iInstanceID, ref publicRooms, ref privateRooms);
                                db.GetOrgTechInfo(confs[i].iOrgID, ref orgContact, ref orgEmail, ref orgPhone); //FB 2543

                                if (publicRooms != null && publicRooms.Count > 0)
                                {
                                    if (privateRooms == null)
                                        privateRooms = new List<inBookingLocationPrivate>();
                                    //FB 2543 Starts
                                    publicBridgingDetails.bridge = true;
                                    if (confs[i].etType != NS_MESSENGER.Conference.eType.ROOM_CONFERENCE)
                                    {
                                        publicBridgingDetails.bridge = false;
                                        publicBridgingDetails.contactName = orgContact;
                                        publicBridgingDetails.contactEmail = orgEmail;
                                        publicBridgingDetails.contactPhone = orgPhone;
                                    }
                                    try
                                    {
                                        if (publicRooms.Count > 0)
                                            WhygoRoomIDs = String.Join(",", publicRooms.Select(rm => rm.locationID.ToString()).ToArray());
                                    }
                                    catch(Exception)
                                    {

                                    }
                                    //confID = -1;
                                    logger.Trace("User ID used for conference is: " + whygoUserID.ToString());
                                    logger.Trace("publicRooms used for conference is: " + WhygoRoomIDs);
                                    logger.Trace("Conf ZStartTime " + confs[i].sZStartTime.ToString());
                                    logger.Trace("Conf ZEndTime " + confs[i].sZEndTime.ToString());
                                    logger.Trace("No Of Attendee: " + confs[i].iNoofAttendee.ToString());
                                    logger.Trace("publicBridgingDetails: Bridge Name" + (publicBridgingDetails.bridge.ToString()));
                                    logger.Trace("privateRooms: " + privateRooms.Count.ToString());
                                    logger.Trace("ConfID :" + stringConfID);
                                    logger.Trace("HostEmail :" + confs[i].sHostEmail);
                                    logger.Trace("HostPassword :" + confs[i].sHostPassword);

                                    vConfReponse = client.createConference(whygoUserID, publicRooms.ToArray(), confs[i].sZStartTime, confs[i].sZEndTime, confs[i].iNoofAttendee, publicBridgingDetails, privateRooms.ToArray(), myVRMConfig); //ZD 100367 //ZD 100729

                                    logger.Trace("CreateConferenceResponse" + vConfReponse);
                                    if (vConfReponse != null)
                                        int.TryParse(vConfReponse.bookingId.ToString(), out confID);
                                    //FB 2543 Ends

                                    if (confID > 0)
                                    {
                                        //ZD 100694 Starts
                                        if (confs[i].iconfMode == 2)
                                        {
                                            NewWhygoID = confID.ToString();
                                            SendMailtoAdmin(NewWhygoID, confs[i], 0);
                                        }
                                        //ZD 100694 Ends
                                        db.UpdateConferenceforWhygo(confs[i].iDbID, confs[i].iInstanceID, confID.ToString(), "Whygo");
                                    }
                                    else
                                    {
                                        // Update conf alerts table
                                        SendMail(confs[i], ref vConfReponse, ref publicRooms,ref privateRooms);// ZD 100884
                                    }
                                }

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                logger.Trace("SaveUpdateConferenceonWhyGO" + ex.StackTrace);

                if (confs != null && confs.Count > 0)
                    SendMail(confs[0], ref vConfReponse, ref publicRooms,ref privateRooms); // ZD 100884

                return false;
            }
            return true;

        }
        #endregion

        #region CreateUser
        internal bool CreateUserinWhyGo(ref string InXML, ref string RecieveXML)
        {
            try
            {
                FetchRegionURL();
                if (!SendRequest(ref InXML, ref RecieveXML, ref URL))
                    return false;

                if (RecieveXML.IndexOf("<error>") >= 0 || RecieveXML.Trim() == "")
                {
                    ErrMsg = RecieveXML;
                    logger.Trace("CreateUser" + RecieveXML);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                logger.Trace("Exception in CreateUser" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion

        #region DeleteConference
        internal bool DeleteConference(ref string InXML, ref string RecieveXML)
        {
            try
            {
                FetchRegionURL();
                if (!SendRequest(ref InXML, ref RecieveXML, ref URL))
                    return false;

                if (RecieveXML.IndexOf("<error>") >= 0 || RecieveXML.Trim() == "")
                {
                    ErrMsg = RecieveXML;
                    logger.Trace("CreateUser" + RecieveXML);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                logger.Trace("Exception in CreateUser" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion

        #region FetchRegionURL
        /// <summary>
        /// FetchRegionURL
        /// </summary>
        private void FetchRegionURL()
        {
            try
            {
                NS_MESSENGER.ESSettings esSet = new NS_MESSENGER.ESSettings();
                //if (db.FetchSystemESSettings(ref esSet, esType))
                //    URL = esSet.ProxyURL;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                logger.Trace("FetchRegionURL" + ex.StackTrace);
            }
        }
        #endregion

        #region SendRequest
        /// <summary>
        /// SendRequest
        /// </summary>
        /// <param name="InXML"></param>
        /// <param name="outXML"></param>
        /// <param name="RegURL"></param>
        /// <returns></returns>
        private bool SendRequest(ref string SendXML, ref string ReceiveXML, ref string RegURL)
        {
            Uri mcuUri = null;
            NetworkCredential credential = null;
            System.IO.Stream stream = null;
            byte[] arrBytes = null;
            WebResponse resp = null;
            Stream respStream = null;
            StreamReader rdr = null;
            DateTime tryDate = DateTime.Now;
            try
            {
                logger.Trace("WhyGO  Region:" + SendXML);
                if (RegURL.Trim() == "")
                {
                    logger.Exception(100, "Emtpy Region URL");
                    ErrMsg = "<error><message>Region URL is Empty</message></error>";
                    return false;
                }

                logger.Trace("SendXML to WhyGO :" + SendXML);
                if (SendXML.Trim() == "")
                {
                    logger.Exception(100, "Emtpy InXML");
                    return false;
                }

                mcuUri = new Uri(RegURL);  //Similarly polycom process
                credential = new NetworkCredential();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(mcuUri);
                CredentialCache CredentialCache = new CredentialCache();

                CredentialCache.Add(mcuUri, "Basic", credential);
                req.Credentials = credential;
                req.ReadWriteTimeout = 60000;
                req.ContentType = "text/xml; charset=UTF-8;";
                req.Method = "POST";
                stream = req.GetRequestStream();
                arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(SendXML);
                stream.Write(arrBytes, 0, arrBytes.Length);
                stream.Close();

                tryDate = DateTime.Now;
                resp = req.GetResponse();
                respStream = resp.GetResponseStream();
                rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                ReceiveXML = rdr.ReadToEnd();
                logger.Trace("Responese From WhyGO :" + ReceiveXML);
            }
            catch (Exception e)
            {
                string debugString = "Error sending data to portal. Error = " + e.Message;
                logger.Exception(100, debugString);
                ReceiveXML = debugString;
                return false;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
                if (respStream != null)
                {
                    respStream.Close();
                    respStream.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Close();
                    rdr.Dispose();
                }
            }
            return true;
        }
        #endregion

        #region Check object for nulls and fill
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objtoCheck"></param>
        /// <returns></returns>
        public void SetPropertyValues(ref RTC_v181.WhyGoServices.GetLocationDetailsResponse objtoCheck)
        {
            Type objType = null;
            object objValue = null;
            try
            {
                if (objtoCheck == null)
                {
                    return ;
                }
                objType = objtoCheck.GetType();

                foreach (PropertyInfo propertyInfo in objType.GetProperties())
                {
                    try
                    {
                        if (propertyInfo.CanRead)
                        {
                            if (propertyInfo.PropertyType == typeof(string))
                            {
                                objValue = propertyInfo.GetValue(objtoCheck, null);

                                if (objValue == null)
                                {
                                    if (propertyInfo.PropertyType == typeof(string))
                                        objValue = "";
                                }
                                else
                                {
                                    if (objValue.GetType() == typeof(string))
                                        objValue = objValue.ToString().Trim().Replace("'", "").Replace("<", "").Replace(">", "").Replace("-", "").Replace("&"," and ");

                                }

                                propertyInfo.SetValue(objtoCheck, objValue, null);
                            }
                        }
                    }

                    catch (Exception ex1)
                    {
                        logger.Trace("GetLocationUpdate" + ex1.StackTrace);

                    }
                }

            }
            catch (Exception ex)
            {
                
            }
        }
        #endregion

        #region SendMail
        private void SendMail(whyGOConference conf)
        {
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            DateTime time = DateTime.UtcNow;
            bool ret = false; String whygoErrMessage = "Error in creating conference in Whygo.";// ZD 100884
            try
            {
                if (conf.sHostEmail != "")
                {
                    logger.Trace("SendEsConfFailMail.....");
                    db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 42); //FB 3069
                    db.ChangeGMTTimeToUserPreferredTimeZone(conf.iHostUserID, ref time);
                    logoId = ""; attchmnt = ""; logoName = "";
                    if (db.CheckMailLogo(conf.iOrgID))
                    {
                        logoId = "LogoImg";
                        logoName = "Org_" + conf.iOrgID + "mail_logo.gif";
                        attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                    }
                    emailBody = emailBody.Replace("{1}", attchmnt);
                    emailBody = emailBody.Replace("{4}", conf.sExternalName).Replace("{5}", conf.iDbNumName.ToString()).Replace("{6}", time.ToString("MM/dd/yyyy hh:mm tt"))
                                      .Replace("{10}", conf.sHostName); //FB 3069
                    if (emailBody.Trim() != "")
                    {
                        email.orgID = conf.iOrgID;
                        email.To = conf.sHostEmail;
                        email.From = "";
                        db.FetchTechInfo(email.orgID, ref emailBody); //FB 3069
                        email.Body = emailBody.Replace("{2}", conf.sHostName);
                        email.Subject = emailSubject;
                        ret = db.InsertEmailInQueue(email);
                        if (!ret)
                        {
                            //errMsg += sendEmail.errMsg;
                            logger.Trace("SMTP connection failed.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
        }
        private void SendMail(whyGOConference conf, ref CreateConferenceResponse vConfReponse, ref List<inBookingLocation> publicRooms, ref  List<inBookingLocationPrivate> privateRooms)// ZD 100884
        {
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();           
            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            DateTime time = DateTime.UtcNow;
            bool ret = false; String whygoErrMessage = "Error in creating conference in Whygo.";// ZD 100884
            String sPublicRooms = ""; String sPrivateRooms = "";
            try
            {
                if (conf.sHostEmail != "")
                {
                    logger.Trace("SendEsConfFailMail.....");
                    db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 42); //FB 3069
                    db.ChangeGMTTimeToUserPreferredTimeZone(conf.iHostUserID, ref time);
                    logoId = ""; attchmnt = ""; logoName = "";
                    if (db.CheckMailLogo(conf.iOrgID))
                    {
                        logoId = "LogoImg";
                        logoName = "Org_" + conf.iOrgID + "mail_logo.gif";
                        attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                    }
                    emailBody = emailBody.Replace("{1}", attchmnt);
                    emailBody = emailBody.Replace("{4}", conf.sExternalName).Replace("{5}", conf.iDbNumName.ToString()).Replace("{6}", time.ToString("MM/dd/yyyy hh:mm tt"))
                                      .Replace("{10}", conf.sHostName); //FB 3069
                    if (emailBody.Trim() != "")
                    {
                        email.orgID = conf.iOrgID;
                        email.To = conf.sHostEmail;
                        email.From = "";
                        db.FetchTechInfo(email.orgID, ref emailBody); //FB 3069
                        email.Body = emailBody.Replace("{2}", conf.sHostName);
                        email.Subject = emailSubject;
                        ret = db.InsertEmailInQueue(email);
                        if (!ret)
                        {
                            //errMsg += sendEmail.errMsg;
                            logger.Trace("SMTP connection failed.");
                        }
                    }

                    if (conf.iconfMode == 2 && conf.iDeleted <= 0 && !String.IsNullOrEmpty(conf.sExternalSchedulingID))//ZD 100884
                    {
                        email = new NS_MESSENGER.Email();  
                        if (vConfReponse != null && (!String.IsNullOrEmpty(vConfReponse.errorId) || !String.IsNullOrEmpty(vConfReponse.errorMessage)))
                            whygoErrMessage = vConfReponse.errorId + ":" + vConfReponse.errorMessage;
                        try
                        {
                            if (publicRooms != null)
                                sPublicRooms = String.Join(",", publicRooms.Select(rm => rm.locationID.ToString()).ToArray());
                        }
                        catch (Exception)
                        {
                            logger.Trace("failed Conversion.");
                        }

                        db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 61);
                        emailBody = emailBody.Replace("{1}", attchmnt);
                        emailBody = emailBody.Replace("{4}", conf.sExternalName).Replace("{5}", conf.iDbNumName.ToString()).Replace("{84}", conf.sExternalSchedulingID.ToString())
                                          .Replace("{10}", conf.sHostName).Replace("{96}", whygoErrMessage).Replace("{6}", conf.sZStartTime.ToString()).Replace("{9}", conf.iDuration.ToString());
                        email.orgID = conf.iOrgID;
                        email.To = sysSettings.WhygoAdminEmail;
                        email.From = "";
                        db.FetchTechInfo(email.orgID, ref emailBody); //FB 3069
                        email.Body = emailBody.Replace("{2}", "");
                        email.Subject = emailSubject;
                        ret = db.InsertEmailInQueue(email);
                        if (!ret)
                        {
                            //errMsg += sendEmail.errMsg;
                            logger.Trace("SMTP insert failed.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
        }
        #endregion

        #region GetLocationAvailability
        /// <summary>
        /// GetLocationAvailability. This command is initially done with monlty in Mind. We can later push it to weekly
        /// </summary>
        /// <param name="InXML"></param>
        /// <param name="outXML"></param>
        /// <returns></returns>
        internal bool GetLocationAvailability(ref string InXML, ref string outXML)
        {
            double offsetAddition = 0;
            int roomID = -1;
            DateTime startDate = DateTime.UtcNow;
            DateTime endDate = DateTime.UtcNow;
            DateTime slotStartDate = DateTime.UtcNow;
            DateTime slotEndDate = DateTime.UtcNow;
            int numOfDays = 30;
            int[] rooms = null;
            StringBuilder xmlBuilder = new StringBuilder();
            int tCnt = 0 ;
            
            XPathDocument xDoc = null;
            StringReader xStrReader = null;
            XPathNavigator xNode = null;
            XPathNavigator xNavigator = null;
            List<RTC_v181.WhyGoServices.TimeSlot> tSlots = null;
            RTC_v181.WhyGoServices.TimeSlot tSlot = null;
            NS_MESSENGER_CUSTOMIZATIONS.Room room = null;
            try
            {
                if (client == null)
                {
                    outXML = "";
                    return true;
                }


                using (xStrReader = new StringReader(InXML))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//calendarView/room");

                        if (xNode != null)
                        {
                            int.TryParse(xNode.Value.Trim(), out roomID);

                            room = db.GetPublicRoomLimited(roomID);
                            if (room != null)
                            {
                                rooms = new int[1] { room.WhygoRoomId };
                            }
                        }


                        if (roomID < 11 || rooms == null || room == null)
                        {
                            outXML = "";
                            return true;
                        }

                        xNode = xNavigator.SelectSingleNode("//calendarView/date");

                        if (xNode != null)
                        {
                            if (!DateTime.TryParse(xNode.Value.Trim(), out startDate))
                            {
                                outXML = "";
                                return true;
                            }
                        }

                        /*
                        xNode = xNavigator.SelectSingleNode("//calendarView/numOfDays");

                        if (xNode != null)
                        {
                            if (!Int32.TryParse(xNode.Value.Trim(), out numOfDays))
                            {
                                obj.outXml = "<error>Invalid Date</error>";
                                return false;
                            }
                        }*/
                    }
                }


                startDate = new DateTime(startDate.Year, startDate.Month, 1);
                endDate = startDate.AddMonths(1).AddSeconds(-1);

                var locationAvailabilityDetails = client.getAvailability(rooms, startDate.ToString("s").Trim() + "Z", endDate.ToString("s").Trim() + "Z");

                if (locationAvailabilityDetails != null)
                {
                    if (!locationAvailabilityDetails.locations[0].isTotallyFree)
                    {
                        tSlots = locationAvailabilityDetails.locations[0].timeSlots.Where(tslt => tslt.isFree == false).ToList();

                        for (tCnt = 0; tCnt < tSlots.Count; tCnt++)
                        {
                            tSlot = tSlots[tCnt];
                            DateTime.TryParse(tSlot.slotTimeStart, out slotStartDate);
                            DateTime.TryParse(tSlot.slotTimeEnd, out slotEndDate);
                            xmlBuilder.Append("<days>");
                            xmlBuilder.Append("<day>");
                            xmlBuilder.Append("<date>" + slotStartDate.ToString("MM/dd/yyyy") + "</date>");
                            xmlBuilder.Append("<conferences>");
                            xmlBuilder.Append("<conference>");
                            xmlBuilder.Append("<confID>" + roomID + "," + tCnt + "</confID>");
                            xmlBuilder.Append("<uniqueID>" + slotStartDate.Hour + tCnt + roomID + "</uniqueID>");
                            xmlBuilder.Append("<confName>Public Conference External</confName>");
                            xmlBuilder.Append("<ConferenceType>7</ConferenceType>");
                            xmlBuilder.Append("<deleted>0</deleted>");
                            xmlBuilder.Append("<isVIP>0</isVIP>");
                            xmlBuilder.Append("<isVMR>0</isVMR>");
                            xmlBuilder.Append("<confPassword></confPassword>");
                            xmlBuilder.Append("<icalID></icalID>");
                            xmlBuilder.Append("<confDate>" + slotStartDate.ToString("MM/dd/yyyy") + "</confDate>");
                            xmlBuilder.Append("<confTime>" + slotStartDate.ToString("hh:mm tt") + "</confTime>");
                            xmlBuilder.Append("<setupTime>" + slotStartDate.ToString("hh:mm tt") + "</setupTime>");
                            xmlBuilder.Append("<teardownTime>" + slotStartDate.ToString("hh:mm tt") + "</teardownTime>");
                            xmlBuilder.Append("<durationMin>15</durationMin>");
                            xmlBuilder.Append("<owner>11</owner>");
                            xmlBuilder.Append("<party></party>");
                            xmlBuilder.Append("<mainLocation>");
                            xmlBuilder.Append("<location>");
                            xmlBuilder.Append("<locationID>" + roomID + "</locationID>");
                            xmlBuilder.Append("<locationName>" + room.name + "</locationName>");
                            xmlBuilder.Append("</location>");
                            xmlBuilder.Append("</mainLocation>");
                            xmlBuilder.Append("<isImmediate>0</isImmediate>");
                            xmlBuilder.Append("<isFuture>1</isFuture>");
                            xmlBuilder.Append("<isPublic>1</isPublic>");
                            xmlBuilder.Append("<isPending>0</isPending>");
                            xmlBuilder.Append("<isApproval>0</isApproval>");
                            xmlBuilder.Append("</conference>");
                            xmlBuilder.Append("</conferences>");
                            xmlBuilder.Append("</day>");
                            xmlBuilder.Append("</days>");
                        }
 
                    }
                }


                outXML = xmlBuilder.ToString();


            }
            catch (Exception ex)
            {
                logger.Trace("GetLocationUpdate" + ex.StackTrace);
                ErrMsg = ex.Message;
                if (ErrMsg.ToLower().Contains("sequence contains no elements"))
                    ErrMsg = "Rooms are upto date";
                
            }
            return true;

        }
        #endregion

        //ZD 100694 Starts

        #region EmailtoWhygoAdmin
         /// <summary>
         /// EmailtoWhygoAdmin
         /// </summary>
         /// <param name="InXML"></param>
         /// <param name="RecieveXML"></param>
         /// <returns></returns>
        internal bool EmailtoWhygoAdmin(ref string InXML, ref string RecieveXML)
        {
            int confId = 0, instanceId = 0, confMode = 0;
            string strConfId = "";
            bool ret = false;
            NS_OPERATIONS.Operations ops = null;
            List<whyGOConference> confs = null;

            List<inBookingLocation> publicRooms = null;
            List<inBookingLocationPrivate> privateRooms = null;
            BridgingDetails publicBridgingDetails = null;
            string  stringConfID = "";
            int FetchWhygoStatus = 0;
            db.FetchWhygoStatus(ref FetchWhygoStatus); 
            try
            {
                XmlDocument xmldoc = new XmlDocument();

                if (FetchWhygoStatus == 1)
                {
                    if (InXML.IndexOf("<error>") >= 0 || InXML.Trim() == "")
                    {
                        logger.Trace("EmailtoWhygoAdmin" + InXML);
                        ErrMsg = InXML;
                    }
                    else
                    {
                        xmldoc.LoadXml(InXML);
                        if (xmldoc.SelectSingleNode("//EmailtoWhygoAdmin/ConfID") != null)
                        {
                            strConfId = xmldoc.SelectSingleNode("//EmailtoWhygoAdmin/ConfID").InnerText;

                            if (xmldoc.SelectSingleNode("//EmailtoWhygoAdmin/isDelete") != null)
                                int.TryParse(xmldoc.SelectSingleNode("//EmailtoWhygoAdmin/isDelete").InnerText, out confMode);
                            #region parse the inxml

                            if (strConfId.IndexOf(",") > 0)
                            {
                                ops = new NS_OPERATIONS.Operations(this.configParams);
                                ret = ops.ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                                if (!ret)
                                {
                                    logger.Trace("Invalid conf/instance id.");
                                    return false;
                                }
                            }
                            else
                            {
                                confId = int.Parse(strConfId);
                                instanceId = -1;
                            }
                            logger.Trace("Confid = " + confId.ToString() + ", InstanceID = " + instanceId.ToString());
                            #endregion
                            db.FetchConfforWhygo(confId, instanceId, ref confs);
                           

                            for (int i = 0; i < confs.Count; i++)
                            {
                                publicBridgingDetails = new BridgingDetails();
                                stringConfID = confs[i].iDbID + "," + confs[i].iInstanceID;
                                db.FetchRoomsforWhygo(confs[i].iDbID, confs[i].iInstanceID, ref publicRooms, ref privateRooms);

                                if (publicRooms != null && publicRooms.Count > 0)
                                {
                                    SendMailtoAdmin("", confs[i], confMode);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                logger.Trace("EmailtoWhygoAdmin" + ex.StackTrace);

                if (confs != null && confs.Count > 0)
                    SendMail(confs[0]);

                return false;
            }
            return true;

        }
        #endregion

        #region SendMailtoAdmin
        private void SendMailtoAdmin(string NewWhygoID, whyGOConference conf, int confMode)
        {
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            DateTime time = DateTime.UtcNow;
            try
            {
                if (sysSettings.WhygoAdminEmail != "")
                {
                    logger.Trace("SendMailtoAdmin.....");
                    if (confMode == 0)
                    {
                        db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 47);
                    }
                    else if (confMode == 1)
                    {
                        db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 48);
                    }

                    logoId = ""; attchmnt = ""; logoName = "";
                    if (db.CheckMailLogo(conf.iOrgID))
                    {
                        logoId = "LogoImg";
                        logoName = "Org_" + conf.iOrgID + "mail_logo.gif";
                        attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                    }
                    emailBody = emailBody.Replace("{1}", attchmnt);
                    emailBody = emailBody.Replace("{4}", conf.sExternalName).Replace("{5}", conf.sExternalSchedulingID.ToString()).Replace("{84}", NewWhygoID)
                                      .Replace("{10}", conf.sHostName);
                    if (emailBody.Trim() != "")
                    {
                        email.orgID = conf.iOrgID;
                        email.To = sysSettings.WhygoAdminEmail;
                        email.From = conf.sHostEmail;
                        db.FetchTechInfo(email.orgID, ref emailBody);
                        email.Body = emailBody.Replace("{2}", "");
                        email.Subject = emailSubject;
                        email.CC = "";
                        email.BCC = "";
                        email.Attachment = "";
                        email.LastRetryDateTime = DateTime.Now;
                        bool ret = db.InsertEmailInQueue(email);
                        if (!ret)
                        {
                            logger.Trace("SMTP connection failed.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
        }

        #endregion
        //ZD 100694 Ends

    }

    class whyGOConference : NS_MESSENGER.Conference
    {
        internal int iPushed, iDeleted, iNoofAttendee;
        internal string sExternalSchedulingID, sExternalSchedulingType;
        internal string sZStartTime, sZEndTime;
        internal bool isBridge;

    }

    
}
