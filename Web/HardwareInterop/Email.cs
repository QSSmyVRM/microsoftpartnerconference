//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* <summary>
 * FILE : Email.cs
 * DESCRIPTION : All SMTP commands are stored in this file. 
 * AUTHOR : Kapil M
 * </summary>
 */
namespace NS_EMAIL
{
	#region References
	using System;	
	using System.Threading;
	using System.Collections;
	using Dart.PowerTCP.Mail;
    using System.IO;
    using System.Net; //ICAL Fix
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using System.Linq;//FB 2410
    //using System.Net.Mime;
    //using System.Net.Mail;
	#endregion 

	class Email
	{
		private NS_LOGGER.Log logger;
		private NS_MESSENGER.ConfigParams configParams;
		internal string errMsg = null;
        private int mailLogo = 0; //FB 1658
        private string mailLogoPath = "";
        private string mailLogoName = ""; //FB 1658

        //Added for FB 1710 Start
        private string footerImageName = "";
        private bool hasFooterImage = false;
        //bool hasSecBadge = false;   //FB 2136
        //Added for FB 1710 End
        FileStream newFile;//FB 2410

		internal Email(NS_MESSENGER.ConfigParams config)
		{
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;										
			logger = new NS_LOGGER.Log(configParams);
		}

        private void smtp1_Trace(object sender, Dart.PowerTCP.Mail.SegmentEventArgs e)
		{
			// Smtp traffic trace 
            logger.Trace(e.Segment.ToString());
		}
		
		// Create MailMessage object to send thru SMTP
        private bool CreateEmail(NS_MESSENGER.Email emailObj, string footerMessage, ref MessageStream msg)
        {
            try
            {
                // REPLY-TO
                if (emailObj.From != null)
                {
                    try
                    {
                        msg.Header.Add(HeaderLabelType.ReplyTo, emailObj.From.Trim());
                    }
                    catch (Exception)
                    {
                        logger.Trace("No FROM address...");
                    }
                }

                // TO
                if (emailObj.To.Length > 3)
                {
                    msg.To.Add(new MailAddress(emailObj.To.Trim()));
                }
                //ZD 104852 starts
                //else
                //{
                if (emailObj.CC != null)
                {
                    try
                    {
                        string[] emaillst = emailObj.CC.ToString().Split(';');
                        for (int e = 0; e < emaillst.Length; e++)
                            msg.To.Add(new MailAddress(emaillst[e].Trim()));
                    }
                    catch (Exception)
                    {
                        logger.Trace("No CC address...");
                    }
                }
                if (emailObj.BCC != null)
                {
                    try
                    {
                        string[] emaillst = emailObj.CC.ToString().Split(';');
                        for (int e = 0; e < emaillst.Length; e++)
                            msg.To.Add(new MailAddress(emaillst[e].Trim()));
                    }
                    catch (Exception)
                    {
                        logger.Trace("No BCC address...");
                    }
                }
                //}
                //ZD 104852 ends

                // Subject
                msg.Subject = emailObj.Subject;

                //FB 1658 - Code Changes - start

                // Append the footer text to message
                if (mailLogo == 0 && !hasFooterImage) //FB 1710 & FB 2136 //&& !hasSecBadge
                {
                    if (footerMessage != null)
                    {
                        if (footerMessage.Trim().Length > 2)
                        {
                            emailObj.Body += "<HR><BR>" + footerMessage.Trim();
                        }
                    }
                    msg.Parts.Add(new MessagePartStream(emailObj.Body, ContentType.TextHtml, ContentEncoding.Base64, "UTF-8"));//ZD 100878
                }
                //FB 1658 - Code Changes - end

                // Attachment
                if (emailObj.Attachment != null)
                {
                    if (emailObj.Attachment.Length > 3)
                    {
                        // attachment has valid text 
                        logger.Trace("Email has an attachment : " + emailObj.Attachment.Trim());
                        //FB 2410 starts
                        MimeAttachmentStream attach = null;
                        if (emailObj.isCalender == 1)
                        {
                            byte[] content = System.Text.Encoding.UTF8.GetBytes(emailObj.Attachment.Trim()); //FB 2208
                            MemoryStream m = new MemoryStream(content);
                            attach = new MimeAttachmentStream(m, "ConferenceMeeting.ics", ContentType.MultipartRelated, ContentEncoding.Base64, "");
                        }
                        else
                        {
                            newFile = new FileStream(emailObj.Attachment, FileMode.Open);
                            attach = new MimeAttachmentStream(newFile);
                        }
                        //Add it to the message.
                        //FB 2410 Ends
                        msg.Attachments.Add(attach);
                        logger.Trace("Email attachment Finished Successfully");
                        attach = null;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

		private bool SendEmail (NS_MESSENGER.Smtp smtpServer,ref MessageStream msg) 
		{	
			try 
			{
                // Send mail 
                Smtp smtp = new Smtp();

				// Specify the server					
				smtp.Server = smtpServer.ServerAddress;
				smtp.ServerPort = smtpServer.PortNumber;
				smtp.Timeout = 10000; // 10 sec
                if (smtpServer.Login == null)
                {
                    smtpServer.Login = "";
                }
                if (smtpServer.Password == null)
                {
                    smtpServer.Password = "";
                }

                smtp.Username = smtpServer.Login;
                smtp.Password = smtpServer.Password;
            

				System.Collections.IEnumerator myEnumerator = msg.To.GetEnumerator();
				while ( myEnumerator.MoveNext() )
				{
					MailAddress ea = (MailAddress)myEnumerator.Current;					
					logger.Trace("Email being sent to : " + ea.Address);
				}
                
				// Message Headers
                logger.Trace("Adding message headers...");
                //msg.Header.Add(HeaderLabelType.From,"\"" + smtpServer.DisplayName +"\"<"+ smtpServer.CompanyMailAddress + ">");
                //msg.Header.Add(HeaderLabelType.ContentType,"text/html;"); ////charset = \"iso-8859-1\"
                msg.Header.Add(HeaderLabelType.Encoding, "quoted-printable"); //FB 2957 Starts
                //msg.Header.Add(HeaderLabelType.Subject,msg.Subject);				

                msg.Header.Add(HeaderLabelType.ContentType, "multipart/mixed;");
                msg.Header.Add(HeaderLabelType.Date, DateTime.UtcNow.ToString("R")); // current date time of the server				
				//FB 2957 Ends
		
		        // From
                logger.Trace("Adding From address...");
                msg.From = new MailAddress("\"" + smtpServer.DisplayName +"\"<" + smtpServer.CompanyMailAddress + ">");
                                
                
				// Send the message                
                logger.Trace("Trying to send email...");
				SmtpResult sr = smtp.Send(msg);
					
				// Check the information about the message sent
				logger.Trace ("Success ! Bytes of data sent = " + sr.SentLength);
				//foreach(MailAddress ma in sr.Recipients)
				//	logger.Trace("Mail sent to: " + ma.Address);

				// Check if connection is active
				if(smtp.Connected)
				{
					try
					{
						// Try to close gracefully
						smtp.Close();
					}
					catch(Exception ex)
					{
						this.errMsg = ex.Message;
						logger.Exception (100,ex.Message);
						// If an error occurs here, just abruptly close.
						smtp.Dispose();
					}
				}

				return true;
			} 
			catch(Exception e)
			{	
				this.errMsg = e.Message;
				logger.Exception(100,e.Message + " --- " + e.StackTrace);				
				return false;
			}			
		}

        // New method added forICAL Fix
        private bool SendCalendar(NS_MESSENGER.Smtp smtpServer,ref NS_MESSENGER.Email emailobj) 
        {
            try
            {
                DateTime startcaldelay = DateTime.Now;

                //while (DateTime.Now.Subtract(startcaldelay).Seconds < 15)
                //{
                //    System.Threading.Thread.Sleep(1);
                //}
               

                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(smtpServer.ServerAddress, smtpServer.PortNumber);
                smtpClient.Credentials = new NetworkCredential(smtpServer.Login, smtpServer.Password);
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                System.Net.Mail.MailMessage app = new System.Net.Mail.MailMessage();

                System.Net.Mime.ContentType calType = new System.Net.Mime.ContentType("text/calendar");
                calType.Parameters.Add("name", "meeting.ics");
                calType.Parameters.Add("method", "REQUEST"); 

                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(emailobj.Body, calType);
                htmlView.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;//ZD 100878
                app.AlternateViews.Add(htmlView);
                app.From = new System.Net.Mail.MailAddress(emailobj.From);
                app.Subject = emailobj.Subject;

                foreach (String toAdd in emailobj.To.Split(';'))
                {
                    if(toAdd != "")
                        app.To.Add(toAdd);
                }
                logger.Trace("Trying to send email...");
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtpClient.Send(app);
                

                // Check the information about the message sent
                logger.Trace("Success ! Bytes of data sent");
                //foreach(MailAddress ma in sr.Recipients)
                //	logger.Trace("Mail sent to: " + ma.Address);

                smtpClient = null;

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message + " --- " + e.StackTrace);
                return false;
            }
        }

        //FB 1658 - Embedded Image Starts...
        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);
                
                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                logger.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //FB 1658 -  Embedded Image Ends...


        
		internal bool SendTestEmail(NS_MESSENGER.Smtp smtpServer,NS_MESSENGER.Email email)
		{
            try
            {
                //FB 1830 - start
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                
                string siteURL = email.Body;
                string Displayname = smtpServer.DisplayName;//FB 2189
                string emailBody = "", emailSubject = "";
                db.FetchEmailString(11, ref emailBody, ref emailSubject, 2); //Test Mail Server Connection
                emailBody = emailBody.Replace("{36}", siteURL);
                emailBody = emailBody.Replace("{2}", Displayname);//FB 2189
                db.FetchTechInfo(11, ref emailBody);//FB 2189
                email.Body = emailBody;
                email.Subject = emailSubject;
                //FB 1830 - end
                
                //FB 2189 Starts
                // SMTP server object				
                NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();

                mailLogo = 0;
                mailLogoName = "Test_11mail_logo.gif";

                if (File.Exists((configParams.mailLogoPath + mailLogoName)))
                    mailLogo = 1;

                string tempStr = "";
                if (mailLogo > 0)
                    tempStr = "<img id=\"MImgId2\" src=\"" + mailLogoName + "\" alt />";

                email.Body = email.Body.Replace("{1}", tempStr); 


                MessageStream msg = null;
                MemoryStream msgStream = null;

                if (mailLogo > 0)
                {

                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim()); //FB 2208
                    msgStream = new MemoryStream(byteArray);
                    msg = new MessageStream(msgStream, configParams.mailLogoPath);
                }
                else
                {
                    msg = new MessageStream();
                }
                //FB 2189 Ends

                //Create the email
                bool ret = CreateEmail(email, smtpServer.FooterMessage, ref msg);
                if (!ret)
                {
                    logger.Trace("Test email create failed.");
                    return false;
                }

                // Send the email 
                ret = false;
                ret = SendEmail(smtpServer, ref msg);
                if (!ret)
                {
                    logger.Trace("Test email send failed.");
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Trace(e.Message);
            }
            return true;
        }

        //Method added for FB 1758
        #region Test Company Email
        /// <summary>
        /// Test Company Email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="footerMessage"></param>
        /// <returns></returns>
        internal bool SendTestCompanyMail(NS_MESSENGER.Email email, String footerMessage)
        {
            try
            {
                //Get the SMTP Server info
                logger.Trace("Initializing db object...");
                logger.Trace(configParams.databaseLogin);
                logger.Trace(configParams.databaseName);
                logger.Trace(configParams.databasePwd);
                logger.Trace(configParams.databaseServer);

                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                // SMTP server object				
                NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();

                logger.Trace("Fetching smtp server info...");
                bool ret = db.FetchSmtpServerInfo(ref smtpServerInfo);
                if (!ret)
                {
                    logger.Trace("Error retreiving SMTP server info from db.");
                    return false;
                }
                if (smtpServerInfo.ServerAddress.Length < 1)
                {
                    logger.Exception(100, "Mail server not valid");
                    return false;
                }
                //FB 1830 - start
                string emailBody="", emailSubject="";

                db.FetchEmailString(email.orgID, ref emailBody, ref emailSubject,3); //Test Mail Connection content
                emailBody = emailBody.Replace("{7}", DateTime.Now.ToString("F"));//ALLBUGS-157
                db.FetchTechInfo(email.orgID, ref emailBody);
                email.Body = emailBody;
                email.Subject = emailSubject;
                //FB 1830 - end

                mailLogo = 0;
                mailLogoName = "Test_" + email.orgID + "mail_logo.gif";
                
                if (File.Exists((configParams.mailLogoPath + mailLogoName)))
                    mailLogo = 1;

                string tempStr = "";
                if (mailLogo > 0)
                    tempStr = "<img id=\"MImgId2\" src=\"" + mailLogoName + "\" alt />";
                   
                email.Body = email.Body.Replace("{1}", tempStr); //FB 1830

                hasFooterImage = false;
                footerImageName = "Test_" + email.orgID + "_footerimage.gif";

                if (footerMessage.IndexOf("<img") >= 0)
                    hasFooterImage = true;

                smtpServerInfo.FooterMessage = footerMessage;
                
                MessageStream msg = null;
                MemoryStream msgStream = null;
                                    
                if (mailLogo > 0)
                {
                    // Append the footer text to message
                    if (smtpServerInfo.FooterMessage != null)
                    {
                        if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                        {
                            email.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();
                        }
                    }

                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim()); //FB 2208
                    msgStream = new MemoryStream(byteArray);
                    msg = new MessageStream(msgStream, configParams.mailLogoPath);
                }
                else
                {
                    if (hasFooterImage) 
                    {
                        // Append the footer text to message
                        if (smtpServerInfo.FooterMessage != null)
                        {
                            if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                            {
                                email.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();
                            }
                        }

                        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim()); //FB 2208
                        msgStream = new MemoryStream(byteArray);
                        msg = new MessageStream(msgStream, configParams.mailLogoPath);
                    }
                    else
                    {
                        msg = new MessageStream();
                    }
                }
                //Create the email
                ret = CreateEmail(email, smtpServerInfo.FooterMessage, ref msg);
                if (!ret)
                {
                    logger.Trace("Test company email create failed.");
                    return false;
                }

                // Send the email 
                ret = false;
                ret = SendEmail(smtpServerInfo, ref msg);
                if (!ret)
                {
                    logger.Trace("Test company email send failed.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace(ex.Message);
            }
            return true;
        }
        #endregion

        //Method added for FB 2363
        #region SendESEmail
        /// <summary>
        /// SendESConfFailureEmail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        internal bool SendESConfFailureEmail(NS_MESSENGER.Email email)
        {
            try
            {
                String emailBody = "", emailSubject = "", footerMessage = "";
                byte[] footerImage = null;

                //Get the SMTP Server info
                logger.Trace("Initializing db object...");
                logger.Trace("DB Login"+configParams.databaseLogin + " /DB Name" + configParams.databaseName + " /DB Pwd:" + configParams.databasePwd + " /Server:" + configParams.databaseServer);

                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();// SMTP server object				

                logger.Trace("Fetching smtp server info...");
                bool ret = db.FetchSmtpServerInfo(ref smtpServerInfo);
                if (!ret)
                {
                    logger.Trace("Error retreiving SMTP server info from db.");
                    return false;
                }
                if (smtpServerInfo.ServerAddress.Length < 1)
                {
                    logger.Exception(100, "Mail server not valid");
                    return false;
                }
                emailBody = email.Body;
                footerMessage = db.FetchFooterMessage(email.orgID, ref footerImage);
                //db.FetchEmailString(email.orgID, ref emailBody, ref emailSubject, 6); //Test Mail Connection content
                db.FetchTechInfo(email.orgID, ref emailBody);

                email.Body = emailBody;
                //email.Subject = emailSubject;
                if (email.From.Trim() == "")
                 email.From = smtpServerInfo.CompanyMailAddress;

                emailBody = emailBody.Replace("{6}", DateTime.Now.ToString("F"));
                mailLogo = 0;
                mailLogoName = "Test_" + email.orgID + "mail_logo.gif";

                if (File.Exists((configParams.mailLogoPath + mailLogoName)))
                    mailLogo = 1;

                string tempStr = "";
                if (mailLogo > 0)
                    tempStr = "<img id=\"MImgId2\" src=\"" + mailLogoName + "\" alt />";

                email.Body = email.Body.Replace("{1}", tempStr);
                hasFooterImage = false;
                footerImageName = "Test_" + email.orgID + "_footerimage.gif";

                if (footerMessage.IndexOf("<img") >= 0)
                    hasFooterImage = true;

                smtpServerInfo.FooterMessage = footerMessage;
                MessageStream msg = null;
                MemoryStream msgStream = null;
                if (mailLogo > 0)
                {
                    if (smtpServerInfo.FooterMessage != null)// Append the footer text to message
                        if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                            email.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();

                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim());
                    msgStream = new MemoryStream(byteArray);
                    msg = new MessageStream(msgStream, configParams.mailLogoPath);
                }
                else
                {
                    if (hasFooterImage)// Append the footer text to message
                    {
                        if (smtpServerInfo.FooterMessage != null)
                            if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                                email.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();

                        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim());
                        msgStream = new MemoryStream(byteArray);
                        msg = new MessageStream(msgStream, configParams.mailLogoPath);
                    }
                    else
                        msg = new MessageStream();
                }

                ret = CreateEmail(email, smtpServerInfo.FooterMessage, ref msg);//Create the email
                if (!ret)
                {
                    logger.Trace("Test company email create failed.");
                    return false;
                }

                ret = false;
                ret = SendEmail(smtpServerInfo, ref msg);// Send the email 
                if (!ret)
                {
                    logger.Trace("Test company email send failed.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("SendESConfFailureEmail"+ex.StackTrace);
            }
            return true;
        }
        #endregion

        internal bool SendAllEmails()
		{
			try
			{
                mailLogo = 0; // FB 1658
                byte[] mailLogoImage = null;
                byte[] footerImage = null; //Added for FB 1710
                int tempOrgid = 0; //FB 1758
                String footerMessage = ""; //FB 1758
				// fetch & send vrm emails 

				//Get the SMTP Server info
				logger.Trace ("Initializing db object...");
				logger.Trace (configParams.databaseLogin);
				logger.Trace (configParams.databaseName);
				logger.Trace (configParams.databasePwd);
				logger.Trace (configParams.databaseServer);
		
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			
				// SMTP server object				
				NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();
				
				logger.Trace ("Fetching smtp server info...");
				bool ret = db.FetchSmtpServerInfo(ref smtpServerInfo);
				if (!ret) 
				{
					logger.Trace("Error retreiving SMTP server info from db."); 
					return false;
				}

				if (smtpServerInfo.ServerAddress.Length < 1)
				{
					logger.Exception (100,"Mail server not valid");
					return false;
				}

				//email queue declaration
				Queue newEmails = new Queue();

				// fetch new emails
				ret =false;
				ret = db.FetchNewEmails(ref newEmails);
				if (!ret) return false;
                
				// Check if queue contains emails
				while (newEmails.Count > 0 )
				{
					// Retreive email details from the queue
					NS_MESSENGER.Email emailObj = new NS_MESSENGER.Email();						
					emailObj = (NS_MESSENGER.Email) newEmails.Dequeue();

                    try
					{
						// Check if email has already been tried more than 100 times  
						//if (emailObj.RetryCount > 100) 
                       // logger.Trace("smtpServerInfo.RetryCount); 
                        if (emailObj.RetryCount >= smtpServerInfo.RetryCount )//FB 1784 //FB 2552
						{
							NS_MESSENGER.Email failureEmail = new NS_MESSENGER.Email();
                            
							if (emailObj.From != null)
							{
								failureEmail.To = emailObj.From;
								//failureEmail.Subject = "[VRM Alert] Email delivery failed after 100 attempts.";
                                failureEmail.Subject = "[VRM Alert] Email delivery failed after"+ smtpServerInfo.RetryCount.ToString() + " attempts."; // FB 1784 //FB 2552
								failureEmail.Body = " <html> <body> This is a system notification for email delivery failure. A copy of email is attached below:";
								failureEmail.Body += "<BR> --------------" ;
								failureEmail.Body += "<BR> From: " + emailObj.From;
								failureEmail.Body += "<BR> To: " + emailObj.To;
								failureEmail.Body += "<BR> Subject: " + emailObj.Subject;
								failureEmail.Body += "<BR> Message: " + emailObj.Body;
                                failureEmail.orgID = emailObj.orgID;    //FB 1710
								//failureEmail.Body += "<BR> --------------" ;
							
								ret = false;
								ret = db.InsertEmailInQueue (failureEmail);
								if (ret)
								{
									db.DeleteEmail(emailObj);
									continue;
								}
							}
						}

                        logger.Trace("Entering create email...");

                        //Create the email
                        ret = false;
                        bool sent = false; //ICAL Fix

                        //Fetch Email Logo Status - FB 1658 code starts
                                                
                        mailLogoName = "Org_" + emailObj.orgID + "mail_logo.gif";
                        
                        if (tempOrgid != emailObj.orgID)
                        {
                            if (File.Exists((configParams.mailLogoPath + mailLogoName)))
                                File.Delete((configParams.mailLogoPath + mailLogoName));
                            mailLogo = 0;
                            mailLogoImage = null;
                            footerImage = null;
                            footerMessage = "";
                            hasFooterImage = false;
                            //hasSecBadge = false; //FB 2136
                        }

                        if (!File.Exists((configParams.mailLogoPath + mailLogoName)))
                        {
                            mailLogo = db.FetchMailLogo(emailObj.orgID, ref mailLogoImage);
                            if (mailLogoImage != null)
                                WriteToFile((configParams.mailLogoPath + mailLogoName), ref mailLogoImage);
                        }
                        
                        emailObj.Body = emailObj.Body.Replace("�", "").Replace("�", "");
                        //Fetch Email Logo Status - FB 1658 code ends
                        
                        //Code Added for FB 1710 Footer Image Start

                        if (footerMessage == "") //FB 1758 - Modified
                        {
                            footerMessage = db.FetchFooterMessage(emailObj.orgID, ref footerImage);

                            if (footerMessage.IndexOf("<img") >= 0)
                                hasFooterImage = true;

                            footerImageName = "Org_" + emailObj.orgID + "_footerimage.gif";
                            
                            if (File.Exists(configParams.mailLogoPath + footerImageName))
                                File.Delete((configParams.mailLogoPath + footerImageName));
                            
                            if (footerImage != null)
                                WriteToFile((configParams.mailLogoPath + footerImageName), ref footerImage);
                        }
                        smtpServerInfo.FooterMessage = footerMessage; 
                        //FB 1710 Footer Image End

                        tempOrgid = emailObj.orgID; //FB 1758

                        //FB 2136 - Start
                        /*hasSecBadge = false;
                        List<Uri> links = new List<Uri>();
                        string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                        string regexAltSrc = @"<img[^>]*?alt\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                        MatchCollection matchesImgSrc = Regex.Matches(emailObj.Body, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        MatchCollection matchesAltSrc = Regex.Matches(emailObj.Body, regexAltSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        Int32 i = 0;
                        string secBadgeName = "";
                        foreach (Match m in matchesImgSrc)
                        {
                            secBadgeName = m.Groups[1].Value;

                            if (secBadgeName.IndexOf("SecImg_") >= 0)
                                break;

                            i = i + 1;
                        }
                        String srcFile = "";
                        if (secBadgeName != "" && secBadgeName.IndexOf("SecImg_") >= 0)
                        {
                            string secImageCont = "";
                            secImageCont = matchesAltSrc[0].Groups[1].Value;
                            srcFile = configParams.mailLogoPath + secBadgeName;
                            byte[] secImageArr = null;
                            
                            if(secImageCont != "")
                                secImageArr = Convert.FromBase64String(secImageCont);

                            if (File.Exists(srcFile))
                                File.Delete(srcFile);

                            WriteToFile(srcFile, ref secImageArr);
                            hasSecBadge = true;
                        }*/
                        //FB 2136 - End

                        //MessageStream msg = new MessageStream(); //FB 1658 Commented
                        MessageStream msg = null; //FB 1658
                        MemoryStream msgStream = null; //FB 1658
                        if (emailObj.isCalender == 1) //ICAL Fix
                        {
                            msg = new MessageStream(); //FB 1658
                            logger.Trace("Entering send email...");
                            // Send the email
                            sent = SendCalendar(smtpServerInfo, ref emailObj);

                        }
                        else
                        {
                            //FB 1658 code starts...
                            if (mailLogo > 0)
                            {
                                // Append the footer text to message
                                if (smtpServerInfo.FooterMessage != null)
                                {
                                    if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                                    {
                                        emailObj.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();
                                    }
                                }

                                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(emailObj.Body.Trim());//FB 2208
                                msgStream = new MemoryStream(byteArray);
                                msg = new MessageStream(msgStream, configParams.mailLogoPath);
                            }
                            else
                            {
                                // Added for FB 1710 Start
                                if (hasFooterImage) 
                                {
                                    // Append the footer text to message
                                    if (smtpServerInfo.FooterMessage != null)
                                    {
                                        if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                                        {
                                            emailObj.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();
                                        }
                                    }
                                    
                                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(emailObj.Body.Trim()); //FB 2208
                                    msgStream = new MemoryStream(byteArray);
                                    msg = new MessageStream(msgStream, configParams.mailLogoPath);
                                } // Added for FB 1710 End
                                else
                                {
                                    msg = new MessageStream(); //ZD 102157
                                }
                                /*else
                                {
                                    //FB 2136 start
                                    if(hasSecBadge)
                                    {
                                        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(emailObj.Body.Trim()); //FB 2208
                                        msgStream = new MemoryStream(byteArray);
                                        msg = new MessageStream(msgStream, configParams.mailLogoPath);
                                        logger.Trace("hasSecImage: " + hasSecBadge);
                                    } //FB 2136 end
                                    else
                                    {
                                        msg = new MessageStream();
                                    }
                                }*/
                            }
                            ret = CreateEmail(emailObj, smtpServerInfo.FooterMessage, ref msg);
                            if (!ret) continue;

                            logger.Trace("Entering send email...");
                            // Send the email 

                            sent = SendEmail(smtpServerInfo, ref msg); //ICAL Fix
                          
                        }
						if (sent)
						{
                            if (!string.IsNullOrEmpty(emailObj.Attachment))
                                newFile.Close();//FB 2410
							db.DeleteEmail (emailObj);
                            String destFile = emailObj.Attachment;
                            logger.Trace(destFile);
                            logger.Trace("Hardware - Deleted Email Attachement");
                            if (File.Exists(destFile))
                                File.Delete(destFile);
						}
						else
						{
							db.UpdateUnsentEmail(emailObj);
						}
					}
					catch (Exception e)
					{
						logger.Exception(100,e.Message);
					}
				}
			
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				logger.Exception (100,e.Message);
				return false;

			}
        }
        //FB 2599 Start
		//Vidyo
        #region SendWelcomeEmail
        /// <summary>
        /// SendWelcomeEmail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        internal bool SendWelcomeEmail(NS_MESSENGER.Email email, NS_MESSENGER.Smtp smtpServerInfo)
        {
            try
            {
                String emailBody = "", emailSubject = "", footerMessage = "";
                byte[] footerImage = null;

                //Get the SMTP Server info
                logger.Trace("Initializing db object...");
                logger.Trace("DB Login" + configParams.databaseLogin + " /DB Name" + configParams.databaseName + " /DB Pwd:" + configParams.databasePwd + " /Server:" + configParams.databaseServer);

                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                //NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();// SMTP server object				

                //logger.Trace("Fetching smtp server info...");
                //bool ret = db.FetchSmtpServerInfo(ref smtpServerInfo);
                //if (!ret)
                //{
                //    logger.Trace("Error retreiving SMTP server info from db.");
                //    return false;
                //}
                if (smtpServerInfo.ServerAddress.Length < 1)
                {
                    logger.Exception(100, "Mail server not valid");
                    return false;
                }
                emailBody = email.Body;
                footerMessage = db.FetchFooterMessage(email.orgID, ref footerImage);
                //db.FetchEmailString(email.orgID, ref emailBody, ref emailSubject, 6); //Test Mail Connection content
                db.FetchTechInfo(email.orgID, ref emailBody);

                email.Body = emailBody;
                //email.Subject = emailSubject;
                if (email.From.Trim() == "")
                    email.From = smtpServerInfo.CompanyMailAddress;

                emailBody = emailBody.Replace("{6}", DateTime.Now.ToString("F"));
                mailLogo = 0;
                mailLogoName = "Test_" + email.orgID + "mail_logo.gif";

                if (File.Exists((configParams.mailLogoPath + mailLogoName)))
                    mailLogo = 1;

                string tempStr = "";
                if (mailLogo > 0)
                    tempStr = "<img id=\"MImgId2\" src=\"" + mailLogoName + "\" alt />";

                email.Body = email.Body.Replace("{1}", tempStr);
                hasFooterImage = false;
                footerImageName = "Test_" + email.orgID + "_footerimage.gif";

                if (footerMessage.IndexOf("<img") >= 0)
                    hasFooterImage = true;

                smtpServerInfo.FooterMessage = footerMessage;
                MessageStream msg = null;
                MemoryStream msgStream = null;
                if (mailLogo > 0)
                {
                    if (smtpServerInfo.FooterMessage != null)// Append the footer text to message
                        if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                            email.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();

                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim());
                    msgStream = new MemoryStream(byteArray);
                    msg = new MessageStream(msgStream, configParams.mailLogoPath);
                }
                else
                {
                    if (hasFooterImage)// Append the footer text to message
                    {
                        if (smtpServerInfo.FooterMessage != null)
                            if (smtpServerInfo.FooterMessage.Trim().Length > 2)
                                email.Body += "<HR><BR>" + smtpServerInfo.FooterMessage.Trim();

                        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(email.Body.Trim());
                        msgStream = new MemoryStream(byteArray);
                        msg = new MessageStream(msgStream, configParams.mailLogoPath);
                    }
                    else
                        msg = new MessageStream();
                }

                bool ret = CreateEmail(email, smtpServerInfo.FooterMessage, ref msg);//Create the email
                if (!ret)
                {
                    logger.Trace("Test company email create failed.");
                    return false;
                }

                ret = false;
                ret = SendEmail(smtpServerInfo, ref msg);// Send the email 
                if (!ret)
                {
                    logger.Trace("Test company email send failed.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("SendWelcomeEmail" + ex.StackTrace);
            }
            return true;
        }
        #endregion
        //FB 2599 End

        //FB 2595 Start

        #region SendHardwareAdminEmail
        /// <summary>
        /// SendHardwareAdminEmail
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="HardwareAdmin"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        internal bool SendHardwareAdminEmail(NS_MESSENGER.Conference conf, string HardwareAdmin, string errorMessage)
        {

            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            try
            {
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                if (HardwareAdmin == "")
                    throw new Exception("Hardware admin email address is empty");

                email.To = HardwareAdmin;
                email.From = conf.sHostEmail;
                email.orgID = conf.iOrgID;

                db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 38); //38-Hardware Admin
                db.FetchTechInfo(conf.iOrgID, ref emailBody);

                logoId = ""; attchmnt = ""; logoName = "";
                if (db.CheckMailLogo(conf.iOrgID))
                {
                    logoId = "LogoImg";
                    logoName = "Org_" + conf.iOrgID + "mail_logo.gif";
                    attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                }
                emailBody = emailBody.Replace("{1}", attchmnt);
                emailBody = emailBody.Replace("{4}", conf.sExternalName);
                emailBody = emailBody.Replace("{5}", conf.iDbNumName.ToString());
                emailBody = emailBody.Replace("{6}", conf.dtSetUpDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtSetUpDateTime.ToString("hh:mm")) + " GMT");
                emailBody = emailBody.Replace("{7}", conf.dtStartDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtStartDateTime.ToString("hh:mm")) + " GMT");
                emailBody = emailBody.Replace("{8}", conf.dtEndDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtEndDateTime.ToString("hh:mm")) + " GMT");
                emailBody = emailBody.Replace("{9}", conf.iDuration.ToString());
                emailBody = emailBody.Replace("{65}", conf.sHostName);
                if (conf.sDescription.Trim() == "")
                    conf.sDescription = "N/A";
                emailBody = emailBody.Replace("{11}", conf.sDescription);
                emailBody = emailBody.Replace("{94}", errorMessage); //ZD 100959

                email.Body = emailBody;
                email.Subject = emailSubject;
                bool ret = db.InsertEmailInQueue(email);
                if (!ret)
                {
                    logger.Trace("Error inserting email in queue for: " + email.To);
                }

            }
            catch (Exception e)
            {
                logger.Trace("SendHardwareAdminEmail" + e.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region Send email to host & participants on call termination due to switching error
        /// <summary>
        /// SendCancellationEmail
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool SendCancellationEmail(NS_MESSENGER.Conference conf)
        {
            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            bool ret = false;
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();

            try
            {
                //Get the SMTP Server info
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                //Fetch participants
                Queue emailQ = new Queue();
                ret = db.FetchParticipantsEmails(conf.iDbID, conf.iInstanceID, ref emailQ);
                if (!ret)
                {
                    logger.Trace("Error fetching participants emails.");
                }
                if (emailQ.Count > 0)
                {
                    email.From = conf.sHostEmail;
                    email.orgID = conf.iOrgID;
                    db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 39);//39-Cancellation type 
                    db.FetchTechInfo(conf.iOrgID, ref emailBody);

                    logoId = ""; attchmnt = ""; logoName = "";
                    if (db.CheckMailLogo(conf.iOrgID))
                    {
                        logoId = "LogoImg";
                        logoName = "Org_" + conf.iOrgID + "mail_logo.gif";
                        attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                    }
                    emailBody = emailBody.Replace("{1}", attchmnt);
                    emailBody = emailBody.Replace("{4}", conf.sExternalName);
                    emailBody = emailBody.Replace("{5}", conf.iDbNumName.ToString());
                    emailBody = emailBody.Replace("{6}", conf.dtSetUpDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtSetUpDateTime.ToString("hh:mm")) + " GMT");
                    emailBody = emailBody.Replace("{7}", conf.dtStartDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtStartDateTime.ToString("hh:mm")) + " GMT");
                    emailBody = emailBody.Replace("{8}", conf.dtEndDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtEndDateTime.ToString("hh:mm")) + " GMT");
                    emailBody = emailBody.Replace("{9}", conf.iDuration.ToString());
                    emailBody = emailBody.Replace("{65}", conf.sHostName);
                    if (conf.sDescription.Trim() == "")
                        conf.sDescription = "N/A";
                    emailBody = emailBody.Replace("{11}", conf.sDescription);

                    email.Body = emailBody;
                    email.Subject = emailSubject;

                    emailQ.Enqueue(conf.sHostEmail);
                    // Send email reminder to all
                    while (emailQ.Count > 0)
                    {
                        // participants & room admins who are going to recv reminders
                        email.To = (string)emailQ.Dequeue();
                        ret = db.InsertEmailInQueue(email);
                        if (!ret)
                        {
                            logger.Trace("Error inserting email in queue for: " + email.To);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Trace("SendCancellationEmail" + e.Message);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2595 End

        //FB 2441 - Starts
        #region SendEmailtoHostAndMCUAdmin
        /// <summary>
        /// SendEmailtoHostAndMCUAdmin
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="MCUAdmin"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        internal bool SendEmailtoHostAndMCUAdmin(NS_MESSENGER.Conference conf, string errorMessage)
        {

            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            try
            {
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                email.From = conf.sHostEmail;
                email.orgID = conf.iOrgID;

                db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 40); //40-Creation failed.
                db.FetchTechInfo(conf.iOrgID, ref emailBody);

                logoId = ""; attchmnt = ""; logoName = "";
                if (db.CheckMailLogo(conf.iOrgID))
                {
                    logoId = "LogoImg";
                    logoName = "Org_" + conf.iOrgID + "mail_logo.gif";
                    attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                }
                emailBody = emailBody.Replace("{1}", attchmnt);
                //emailBody = emailBody.Replace("{2}", conf.cMcu.sAdminName);
                emailBody = emailBody.Replace("{4}", conf.sExternalName);
                emailBody = emailBody.Replace("{5}", conf.iDbNumName.ToString());
                emailBody = emailBody.Replace("{6}", conf.dtSetUpDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtSetUpDateTime.ToString("hh:mm")) + " GMT");
                emailBody = emailBody.Replace("{7}", conf.dtStartDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtStartDateTime.ToString("hh:mm")) + " GMT");
                emailBody = emailBody.Replace("{8}", conf.dtEndDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtEndDateTime.ToString("hh:mm")) + " GMT");
                emailBody = emailBody.Replace("{9}", conf.iDuration.ToString());
                emailBody = emailBody.Replace("{65}", conf.sHostName);
                if (conf.sDescription.Trim() == "")
                    conf.sDescription = "N/A";
                emailBody = emailBody.Replace("{11}", conf.sDescription);
                emailBody = emailBody.Replace("{94}", errorMessage); //ZD 100959

                email.Body = emailBody;
                email.Subject = emailSubject;


                bool ret = false;
                for (int i = 0; i < 2; i++) //Doubt-Valli
                {
                    ret = false;
                    if (i == 0)
                        email.To = conf.sHostEmail;
                    else
                        email.To = conf.cMcu.sAdminEmail;
                    ret = db.InsertEmailInQueue(email);
                    if (!ret)
                    {
                        logger.Trace("Error inserting email in queue for: " + email.To);
                    }
                }

            }
            catch (Exception e)
            {
                logger.Trace("SendEmailtoHostAndMCUAdmin" + e.Message);
                return false;
            }
            return true;
        }
        #endregion
        //FB 2441 - End

        //ZD 100369_MCU - Starts

        #region SendFailureEmailtoMCUAdmin
        /// <summary>
        /// SendFailureEmailtoMCUAdmin
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="MCUAdmin"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        internal bool SendFailureEmailtoMCUAdmin(string MCUName, int Orgid, List<string> EmailAddress)
        {

            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();
            bool ret = false;
            try
            {
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                db.FetchSmtpServerInfo(ref smtpServerInfo);

                email.From = smtpServerInfo.CompanyMailAddress;
                email.orgID = Orgid;

                logoId = ""; attchmnt = ""; logoName = "";
                if (db.CheckMailLogo(Orgid))
                {
                    logoId = "LogoImg";
                    logoName = "Org_" + Orgid + "mail_logo.gif";
                    attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                }

                db.FetchEmailString(email.orgID, ref emailBody, ref emailSubject, 45); //MCU Failure Mail
                db.FetchTechInfo(email.orgID, ref emailBody);

                emailBody = emailBody.Replace("{1}", attchmnt);
                emailBody = emailBody.Replace("{2}", "");
                emailBody = emailBody.Replace("{3}", MCUName);
                email.Body = emailBody;
                email.Subject = emailSubject;


                for (int i = 0; i < EmailAddress.Count; i++)
                {
                    email.To = EmailAddress[i];
                    ret = db.InsertEmailInQueue(email);
                    if (!ret)
                    {
                        logger.Trace("Error inserting email in queue for: " + email.To);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Trace("SendEmailtoSyncAdmin" + e.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region SendSuccessEmailtoMCUAdmin
        /// <summary>
        /// SendSuccessEmailtoMCUAdmin
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="MCUAdmin"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        internal bool SendSuccessEmailtoMCUAdmin(string MCUName, int Orgid, List<string> EmailAddress)
        {

            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();
            bool ret = false;
            try
            {
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                db.FetchSmtpServerInfo(ref smtpServerInfo);

                email.From = smtpServerInfo.CompanyMailAddress;
                email.orgID = Orgid;

                logoId = ""; attchmnt = ""; logoName = "";
                if (db.CheckMailLogo(Orgid))
                {
                    logoId = "LogoImg";
                    logoName = "Org_" + Orgid + "mail_logo.gif";
                    attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                }

                db.FetchEmailString(email.orgID, ref emailBody, ref emailSubject, 46); //MCU Success Mail
                db.FetchTechInfo(email.orgID, ref emailBody);

                emailBody = emailBody.Replace("{1}", attchmnt);
                emailBody = emailBody.Replace("{2}", "");
                emailBody = emailBody.Replace("{3}", MCUName);
                email.Body = emailBody;
                email.Subject = emailSubject;


                for (int i = 0; i < EmailAddress.Count; i++)
                {
                    email.To = EmailAddress[i];
                    ret = db.InsertEmailInQueue(email);
                    if (!ret)
                    {
                        logger.Trace("Error inserting email in queue for: " + email.To);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Trace("SendEmailtoSyncAdmin" + e.Message);
                return false;
            }
            return true;
        }
        #endregion

        //ZD 100369_MCU - End


        // ZD 100221 Email
        #region SendWebExFailureEmail
        internal bool SendWebExFailureEmail(NS_MESSENGER.Conference conf, string errorMessage)
        {

            string emailBody = "", emailSubject = "";
            string logoId = "", attchmnt = "", logoName = "";
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            try
            {
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                email.From = conf.sHostEmail;
                email.orgID = conf.iOrgID;

                db.FetchEmailString(conf.iOrgID, ref emailBody, ref emailSubject, 58); //58-WebEx Failure Notification.
                db.FetchTechInfo(conf.iOrgID, ref emailBody);

                logoId = ""; attchmnt = ""; logoName = "";
                if (db.CheckMailLogo(conf.iOrgID))
                {
                    logoId = "LogoImg";
                    logoName = "Org_" + conf.iOrgID + "mail_logo.gif";
                    attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                }
                emailBody = emailBody.Replace("{1}", attchmnt);
                emailBody = emailBody.Replace("{2}", "");
                emailBody = emailBody.Replace("{4}", conf.sExternalName);
                emailBody = emailBody.Replace("{5}", conf.iDbNumName.ToString());
                emailBody = emailBody.Replace("{6}", conf.dtSetUpDateTime.ToString(("MM/dd/yyyy") + " " + conf.dtSetUpDateTime.ToString("hh:mm")) + " GMT");
                emailBody = emailBody.Replace("{10}", conf.sHostName);
                emailBody = emailBody.Replace("{94}", errorMessage);

                email.Body = emailBody;
                email.Subject = emailSubject;


                bool ret = false;
                for (int i = 0; i < 1; i++) //Doubt-Valli
                {
                    ret = false;
                    if (i == 0)
                        email.To = conf.sHostEmail;
                    //else
                    //    email.To = conf.sWebEXEmail;
                    email.CC = "";
                    email.BCC = "";
                    email.Attachment = "";
                    email.LastRetryDateTime = DateTime.Now;
                    ret = db.InsertEmailInQueue(email);
                    if (!ret)
                    {
                        logger.Trace("Error inserting email in queue for: " + email.To);
                    }
                }

            }
            catch (Exception e)
            {
                logger.Trace("SendWebExFailureEmail" + e.Message);
                return false;
            }
            return true;
        }
        #endregion
        //ZD 100221 Email
    }
}