//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* FILE : PolycomAccord.cs
 * DESCRIPTION : All Polycom MCU api commands are stored in this file. 
 * AUTHOR : Kapil M
 */

namespace NS_POLYCOMRPRM
{
    #region References
    using System;
    using System.Xml;
    using System.Net;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Diagnostics;
    using System.Xml.Linq;
    using Ionic.Zip; // FB 2683
    using System.Xml.XPath; //ZD 100685
    
    #endregion
    class POLYCOMRPRM
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;

        bool ret = false;
        StringBuilder str_build;
        XmlWriterSettings settings;
        XmlWriter xWriter = null;
        XDocument xdoc;
        int DMAorRPRM = 0;
        string credentials = "";
        string MCUIP = "";
        string MCULogin = "";
        string MCUPassword = "";
        int MCUPort = 0;
        string strUrl = "";
        string ContentType = "", RequestMethod = "", RestURI = "", responseXml = "", Etag = "";
        DateTime Date = new DateTime();
        List<NS_MESSENGER.Conference> confList = null;
        internal bool isRecurring = false; 

        #region POLYCOMRPRM
        /// <summary>
        /// polycomRPMS
        /// </summary>
        /// <param name="config"></param>
        internal POLYCOMRPRM(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }
        #endregion

        #region GeneralMethod

        #region CallType
        /// <summary>
        /// CallType
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        // <xs:enumeration value="IN_PERSON"/>  
        //<xs:enumeration value="AUDIO"/>  
        //<xs:enumeration value="VIDEO"/> 
        private string CallType(ref NS_MESSENGER.Party.eCallType eType)
        {
            string CallType = "IN_PERSON";
            if (eType == NS_MESSENGER.Party.eCallType.VIDEO)
                CallType = "VIDEO";
            else if (eType == NS_MESSENGER.Party.eCallType.AUDIO)
                CallType = "AUDIO";

            return CallType;
        }
        #endregion

        public enum lockunlockconference
        {
            lockconfernece = 1, unlockconference = 0
        };

        public enum Startstoprecord
        {
            startRecord = 1, stopRecord = 0
        };

        #region AddressType
        /// <summary>AddressType
        //  <xs:enumeration value="H323"/>  
        //  <xs:enumeration value="ISDN"/>  
        //  <xs:enumeration value="SIP"/>  
        //  <xs:enumeration value="H323_E164"/>  
        //  <xs:enumeration value="H323_ANNEX_O"/>  
        //  <xs:enumeration value="H323_ID"/>  
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        private string AddressType(ref NS_MESSENGER.Party.eAddressType eAddType)
        {
            string AddType = "";

            if (eAddType == NS_MESSENGER.Party.eAddressType.H323_ID)
                AddType = "H323_ID";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.IP_ADDRESS)
                AddType = "H323";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.ISDN_PHONE_NUMBER)
                AddType = "ISDN";
			// FB 2689 Starts
            else if (eAddType == NS_MESSENGER.Party.eAddressType.E_164) //ZD 102723
                AddType = "H323_E164";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.SIP)
                AddType = "SIP";
            else
                AddType = "H323";
			// FB 2689 Ends
            return AddType;
        }
        #endregion

        #region DialDirection
        /// <summary>DialDirection
        //<xs:enumeration value="DIAL_OUT"/>  
        // <xs:enumeration value="DIAL_IN"/>   
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        private string DialDirection(ref NS_MESSENGER.Party.eConnectionType ConnType)
        {
            string DialDir = "DIAL_OUT"; //ZD 102600

            if (ConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                DialDir = "DIAL_IN";
            else if (ConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                DialDir = "DIAL_OUT";

            return DialDir;
        }
        #endregion

        #region isValidIPAddress
        /// <summary>
        /// isValidIPAddress
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        private bool isValidIPAddress(string ip)
        {
            ret = true;
            string[] octets = ip.Split('.');
            if (octets.Length != 4)
                ret = false;
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    int dig = Convert.ToInt32(octets[i]);
                    if (dig < 0 || dig > 255)
                    {
                        ret = false;
                        break;
                    }
                }
            }
            return ret;
        }
        #endregion

        #region EquateVideoCodecs
        /// <summary>
        /// EquateVideoCodecs
        /// </summary>
        /// <param name="videoCodec"></param>
        /// <param name="polycomVideoCodec"></param>
        /// <returns></returns>

        private bool EquateVideoCodecs(NS_MESSENGER.Conference.eVideoCodec videoCodec, ref string polycomVideoCodec)
        {
            try
            {
                // equating to VRM to Polycom values
                if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.AUTO)
                {
                    polycomVideoCodec = "auto";
                }
                else
                {
                    if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H261)
                    {
                        polycomVideoCodec = "h261";
                    }
                    else
                    {
                        if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H263)
                        {
                            polycomVideoCodec = "h263";
                        }
                        else
                        {
                            polycomVideoCodec = "h264";
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region EquateAudioCodec
        /// <summary>
        /// EquateAudioCodec
        /// </summary>
        /// <param name="audioCodec"></param>
        /// <param name="polycomAudioCodec"></param>
        /// <returns></returns>
        private bool EquateAudioCodec(NS_MESSENGER.Conference.eAudioCodec audioCodec, ref string polycomAudioCodec)
        {
            try
            {

                if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_56)
                    polycomAudioCodec = "g722_56";
                else
                {
                    if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G728)
                        polycomAudioCodec = "g728";
                    else
                    {
                        if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_32)
                            polycomAudioCodec = "g722_32";
                        else
                        {
                            if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_24)
                                polycomAudioCodec = "g722_24";
                            else
                            {
                                if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G711_56)
                                    polycomAudioCodec = "g711_56";
                                else
                                {
                                    if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.AUTO)
                                        polycomAudioCodec = "auto";
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region EquateLineRate
        /// <summary>
        /// EquateLineRate
        /// </summary>
        /// <param name="lineRate"></param>
        /// <param name="isIP"></param>
        /// <param name="polycomLineRate"></param>
        /// <returns></returns>
        private bool EquateLineRate(NS_MESSENGER.LineRate.eLineRate lineRate, bool isIP, ref string polycomLineRate)
        {
            try
            {
                // equating VRM to Polycom values


                if (isIP)
                {
                    #region IP linerate mappings
                    //Modified during FB 1742
                    switch (lineRate)
                    {
                        case NS_MESSENGER.LineRate.eLineRate.K64:
                            {
                                polycomLineRate = "64";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K128:
                            {
                                polycomLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K192:
                            {
                                polycomLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K256:
                            {
                                polycomLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K320:
                            {
                                polycomLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K384:
                            {
                                polycomLineRate = "384";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K512:
                            {
                                polycomLineRate = "512";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K768:
                            {
                                polycomLineRate = "768";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1024:
                            {
                                polycomLineRate = "1024";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1152:
                            {
                                polycomLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1250:
                            {
                                polycomLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1472:
                            {
                                polycomLineRate = "1472";
                                break;
                            }

                        case NS_MESSENGER.LineRate.eLineRate.M1536:
                            {
                                polycomLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1792:
                            {
                                polycomLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1920:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2048:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2560:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3072:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3584:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M4096:
                            {
                                polycomLineRate = "4096";
                                break;
                            }
                        default:
                            {
                                // default is 384 kbps
                                polycomLineRate = "384";
                                break;
                            }
                    }
                    #endregion

                    #region IP linerate mappings - Commented during FB 1742

                    //if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                    //    polycomLineRate = "64";
                    //else
                    //{
                    //    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                    //        polycomLineRate = "128";
                    //    else
                    //    {
                    //        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                    //            polycomLineRate = "256";
                    //        else
                    //        {
                    //            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                    //                polycomLineRate = "512";
                    //            else
                    //            {
                    //                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                    //                    polycomLineRate = "768";
                    //                else
                    //                {
                    //                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                    //                        polycomLineRate = "1152";
                    //                    else
                    //                    {
                    //                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                    //                            polycomLineRate = "1472";
                    //                        else
                    //                        {
                    //                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                    //                                polycomLineRate = "1536";
                    //                            else
                    //                            {
                    //                                if (lineRate >= NS_MESSENGER.LineRate.eLineRate.M1920)
                    //                                    polycomLineRate = "1920";
                    //                            }
                    //                        }

                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    #region ISDN linerate mappings
                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                        polycomLineRate = "channel_1";
                    else
                    {
                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                            polycomLineRate = "channel_2";
                        else
                        {
                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                                polycomLineRate = "channel_4";
                            else
                            {
                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                                    polycomLineRate = "channel_8";
                                else
                                {
                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                                        polycomLineRate = "channel_12";
                                    else
                                    {
                                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                                            polycomLineRate = "channel_18";
                                        else
                                        {
                                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                                                polycomLineRate = "channel_23";
                                            else
                                            {
                                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                                                    polycomLineRate = "channel_24";
                                                else
                                                {
                                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1920)
                                                        polycomLineRate = "channel_30";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region EquateVideoLayout
        /// <summary>
        /// EquateVideoLayout
        /// </summary>
        /// <param name="videoLayoutID"></param>
        /// <param name="polycomVideoLayoutValue"></param>
        /// <returns></returns>
        private bool EquateVideoLayout(int videoLayoutID, ref string polycomVideoLayoutValue)
        {

#if COMMENT_DO_NOT_DELETE
Dan's email 10/22/06 - MGC equivalent mappings of Codian layout code :
enumeration 	1x1 Codian # 1 – Classic and Quad Mode 
enumeration 	1x2 No Codian layout avail – Classic and Quad Mode Image 16
enumeration 	2x1 No Codian layout avail – Classic and Quad Mode 
enumeration 	2x2 Codian # 2 – Classic and Quad Mode
enumeration 	1and5 Codian # 5 – Classic Mode
enumeration 	3x3 Codian # 3 – Classic and Quad Mode
enumeration 	1x2Ver Codian # 16 – Classic Mode
enumeration 	1x2Hor Codian # 12 – Classic Mode
enumeration 	1and2Hor No Codian layout avail – Classic Mode Image 61
enumeration 	1and2Ver Codian # 17 – Classic Mode
enumeration 	1and3Hor No Codian layout avail – Classic Mode Image 62
enumeration 	1and3Ver Codian # 18 – Classic Mode
enumeration 	1and4Ver Codian # 19 – Classic Mode
enumeration 	1and4Hor No Codian layout avail – Classic Mode Image 63
enumeration 	1and8Central Codian # 24 – Classic Mode
enumeration 	1and8Upper No Codian layout avail – Classic Mode Image 60
enumeration 	1and2HorUpper Codian # 13 – Classic Mode
enumeration 	1and3HorUpper Codian # 14 – Classic Mode
enumeration 	1and4HorUpper Codian # 15 – Classic Mode
enumeration 	1and8Lower Codian # 20 – Classic Mode
enumeration 	1and7 Codian # 6 – Classic Mode
enumeration 	4x4 Codian # 4 – Quad Mode
enumeration 	2and8 Codian # 25 – Quad Mode
enumeration 	1and12 Codian # 33 – Quad Mode
enumeration 	1x1Qcif No Codian layout avail – Quad Mode
#endif
            polycomVideoLayoutValue = "1 cell";
            try
            {
                switch (videoLayoutID)
                {
                    case 1:
                        {
                            polycomVideoLayoutValue = "1 cell";
                            break;
                        }
                    case 2:
                        {
                            polycomVideoLayoutValue = "2 horizontal by 2 vertical";
                            break;
                        }
                    case 3:
                        {
                            polycomVideoLayoutValue = "3 horizontal by 3 vertical";
                            break;
                        }
                    case 4: //FB 2335
                        {
                            polycomVideoLayoutValue = "4 horizontal by 4 vertical";
                            break;
                        }
                    case 5:
                        {
                            polycomVideoLayoutValue = "1 plus 3 bottom and 2 right";
                            break;
                        }
                    case 6:
                        {
                            polycomVideoLayoutValue = "1 plus 4 bottom and 3 right";
                            break;
                        }
                    // case 7-11 : layout is not supported 
                    case 12:
                        {
                            polycomVideoLayoutValue = "2 horizontal wide";
                            break;
                        }
                    case 13:
                        {
                            polycomVideoLayoutValue = "1 plus 2 bottom";
                            break;
                        }
                    case 14:
                        {
                            polycomVideoLayoutValue = "1 plus 3 bottom";
                            break;
                        }
                    case 15:
                        {
                            polycomVideoLayoutValue = "1 plus 4 bottom";
                            break;
                        }
                    case 16:
                        {
                            // Changed from 1x2Ver to 1x2 on BTBoces request (case 1606)
                            polycomVideoLayoutValue = "2 vertical tall";
                            break;
                        }
                    case 17:
                        {
                            polycomVideoLayoutValue = "1 plus 2 right";
                            break;
                        }
                    case 18:
                        {
                            polycomVideoLayoutValue = "1 plus 3 right";
                            break;
                        }
                    case 19:
                        {
                            polycomVideoLayoutValue = "1 plus 4 right";
                            break;
                        }
                    case 20:
                        {
                            polycomVideoLayoutValue = "1 plus 4 bottom and 4 bottom";
                            break;
                        }

                    case 21:
                        {
                            polycomVideoLayoutValue = "1 plus 4 top and 4 top";
                            break;
                        }
                    //case 22-23 : layout is not supported
                    case 24:
                        {
                            polycomVideoLayoutValue = "1 plus 4 top and 4 bottom";
                            break;
                        }
                    case 25:
                        {
                            polycomVideoLayoutValue = "2 horizontal plus 4 top and 4 bottom";
                            break;
                        }
                    //case 26-31: layout is not supported	
                    case 32:
                        {
                            polycomVideoLayoutValue = "1 center plus 4 top and 4 bottom and 2 right and 2 left";
                            break;
                        }
                    case 33:
                        {
                            polycomVideoLayoutValue = "1 center plus 4 top and 4 bottom and 2 right and 2 left";
                            break;
                        }
                    //case 34-37: layout is not supported
                    case 38:
                        {
                            polycomVideoLayoutValue = "1 plus 3 bottom";
                            break;
                        }
                    //case 39-59: layout is not supported
                    case 60://FB 2335
                        {
                            polycomVideoLayoutValue = "1 plus 4 top and 4 top";
                            break;
                        }
                    case 61://FB 2335
                        {
                            polycomVideoLayoutValue = "1 plus 2 bottom";
                            break;
                        }
                    case 62://FB 2335
                        {
                            polycomVideoLayoutValue = "1 plus 3 bottom";
                            break;
                        }
                    //case 33-42: layout is not supported					
                    //case 101-109 are all custom to btboces for MGC accord bridges
                    default:
                        {
                            // layout not supported
                            this.errMsg = "This video layout is not supported on Polycom MCU.";
                            return false;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region ProcessError
        /// <summary>
        /// ProcessError
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        private string ProcessError(string error)
        {
            string retError = "";
            XElement elem = null;
            try
            {
                elem = XElement.Parse(error);

                XNamespace env = "urn:com:polycom:api:rest:plcm-error";

                retError = "Error Code :" + elem.Element(env + "status-code").Value + " Error :" + elem.Element(env + "description").Value;
                retError = retError.Replace("'", "");


            }
            catch (Exception ex)
            {
                retError = error;

            }
            return retError;
        }
        #endregion

        #region encodeString
        /// <summary>
        /// encodeString
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string encodeString(string data)
        {
            string encoded = "";
            try
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);
                encoded = System.Convert.ToBase64String(bytes, Base64FormattingOptions.None);


            }
            catch (Exception ex)
            {
            }

            return encoded;

        }
        #endregion

        #endregion

        #region PublicMethod

        //FB 2883 Starts
        #region TestMCUConnection
        /// <summary>
        /// TestMCUConnection
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        internal bool TestMCUConnection(NS_MESSENGER.MCU mcu, int type)
        {
            try
            {
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                DMAorRPRM = type;
              
                if (DMAorRPRM == (int)NS_MESSENGER.AccessMode.FromWebsite) //FB 2556
                {
                    RestURI = "/api/rest/users/" + mcu.sRPRMDomain + "/" + mcu.sRPRMLogin;
                   
                    RequestMethod = "GET";
                    conf.cMcu = mcu;
                    //ZD 102205
                    if (conf.cMcu.sSoftwareVer == "8.0.x" || conf.cMcu.sSoftwareVer == "8.1.x") //ALLDEV-779
                        ContentType = "*/*";
                    else
                        ContentType = "application/vnd.plcm.plcm-user-list+xml";

                    ret = false; responseXml = ""; str_build = new StringBuilder(); responseXml = ""; 
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        errMsg = "TestConnection failed";
                        logger.Exception(100, "TestConnection failed");
                        return false;
                    }
                }
                else
                {
                    RestURI = "/api/rest/status";
                    RequestMethod = "GET";
                    conf.cMcu = mcu;
                    //ZD 102205
                    if (conf.cMcu.sSoftwareVer == "8.0.x" || conf.cMcu.sSoftwareVer == "8.1.x") //ALLDEV-779
                        ContentType = "*/*";
                    else
                        ContentType = "application/vnd.plcm.plcm-user-list+xml";

                    ret = false; responseXml = ""; str_build = new StringBuilder(); responseXml = ""; 
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        errMsg = "TestConnection failed";
                        logger.Exception(100, "TestConnection failed");
                        return false;
                    }
                }               
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //FB 2883 Ends

        internal bool FetchMCUProfileDetails(NS_MESSENGER.MCU mcu)
        {
            try
            {
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf.cMcu = mcu;
                RestURI = "/api/rest/conference-templates";
                ContentType = "application/vnd.plcm.plcm-conference-template-list+xml";
                RequestMethod = "GET";
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556

                ret = false; responseXml = ""; str_build = new StringBuilder(); responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "SetConference SendTransaction failed");
                    return false;
                }
                xdoc = XDocument.Parse(responseXml);

                //ZD 100685 starts
                XPathDocument _xmlns = new XPathDocument(new StringReader(xdoc.ToString()));
                XPathNavigator _Xnav = _xmlns.CreateNavigator();
                _Xnav.MoveToFollowing(XPathNodeType.Element);
                var namespacelist = _Xnav.GetNamespacesInScope(XmlNamespaceScope.All).ToList();

                var ns4 = namespacelist.Where(data => data.Key == "ns4").ToList();
                var ns5 = namespacelist.Where(data => data.Key == "ns5").ToList();

                XNamespace NS4 = ns4[0].Value;
                XNamespace NS5 = ns5[0].Value;

                var Template = (from temp in xdoc.Descendants(NS5 + ns5[0].Value.Split(':')[5]).Elements(NS4 + ns4[0].Value.Split(':')[5])
                                select new NS_MESSENGER.MCUProfile
                                {
                                    iId = int.Parse(temp.Element(NS4 + "conference-template-identifier").Value),
                                    sDisplayname = temp.Element(NS4 + "name").Value,
                                    sName = ""
                                }).ToList();

                //ZD 100685 Ends
                

                ret = false;
                ret = db.UpdateMcuProfile(conf.cMcu.iDbId, Template);
                if (!ret)
                {
                    logger.Exception(100, "FetchMCUProfileDetails failed");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        //ZD 104221 start
        internal bool FetchMCUPoolorderDetails(NS_MESSENGER.MCU mcu)
        {
            try
            {
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf.cMcu = mcu;
                RestURI = "/api/rest/mcu-pool-orders";
                ContentType = "application/vnd.plcm.plcm-mcu-pool-order-list+xml";
                RequestMethod = "GET";
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556

                ret = false; responseXml = ""; str_build = new StringBuilder(); responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "SetConference SendTransaction failed");
                    return false;
                }
                xdoc = XDocument.Parse(responseXml);

                //ZD 100685 starts
                XPathDocument _xmlns = new XPathDocument(new StringReader(xdoc.ToString()));
                XPathNavigator _Xnav = _xmlns.CreateNavigator();
                _Xnav.MoveToFollowing(XPathNodeType.Element);
                var namespacelist = _Xnav.GetNamespacesInScope(XmlNamespaceScope.All).ToList();

                var ns3 = namespacelist.Where(data => data.Key == "ns3").ToList();
                var ns4 = namespacelist.Where(data => data.Key == "ns4").ToList();

                XNamespace NS3 = ns3[0].Value;
                XNamespace NS4 = ns4[0].Value;

                var Template = (from temp in xdoc.Descendants(NS4 + ns4[0].Value.Split(':')[5]).Elements(NS3 + ns3[0].Value.Split(':')[5])
                                select new NS_MESSENGER.MCUProfile
                                {
                                    iId = int.Parse(temp.Element(NS3 + "mcu-pool-order-identifier").Value)+1,
                                    sDisplayname = temp.Element(NS3 + "name").Value,
                                    sName = ""
                                }).ToList();

                //ZD 100685 Ends


                ret = false;
                ret = db.UpdateMcuPoolOrder(conf.cMcu.iDbId, Template);  
                if (!ret)
                {
                    logger.Exception(100, "FetchMCUPoolorderDetails failed");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 104221 End

        #region SetConference
        /// <summary>
        /// SetConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool SetConference(NS_MESSENGER.Conference templateConf)
        {
            logger.Trace("Entering SetConference function...");
            int cnfCnt = 0;
            NS_MESSENGER.Conference conf = null;
            NS_EMAIL.Email sendEmail = null;
            try
            {
                sendEmail = new NS_EMAIL.Email(configParams);
                ret = false;

                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(templateConf);

                for (cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    xWriter = null;
                    settings = new XmlWriterSettings();
                    str_build = new StringBuilder();
                    settings.CheckCharacters = false;
                    settings.Encoding = Encoding.UTF8;
                    xWriter = XmlWriter.Create(str_build, settings);
                    conf = confList[cnfCnt];

                    ret = CreateXML_Reservation(ref conf, ref xWriter); //Call for Generate XML


                    if (!ret || str_build.ToString().Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed ");
                        return false;
                    }

                    if (str_build.ToString().Contains("utf-16"))
                        str_build.Replace("utf-16", "utf-8");
                    if (str_build.ToString().Contains(":n1"))
                        str_build.Replace(":n1", "");
                    if (str_build.ToString().Contains(":n2"))
                        str_build.Replace(":n2", "");
                    if (str_build.ToString().Contains(":n3"))
                        str_build.Replace(":n3", "");

                    ContentType = "application/vnd.plcm.plcm-reservation+xml";
                    if (conf.ESId == "")
                    {
                        RequestMethod = "POST";
                        RestURI = "/api/rest/reservations";
                    }
                    else
                    {
                        RequestMethod = "PUT";
                        RestURI = "/api/rest/reservations/" + conf.ESId;
                    }


                    //Change the innerText of "start-time" and "end-time" for all instances.

                    ret = false; responseXml = "";
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "SetConference SendTransaction failed");
                        sendEmail.SendEmailtoHostAndMCUAdmin(conf, this.errMsg);
                        return false;
                    }

                    if (conf.ESId.Trim() != "")
                    {

                        ret = false;
                        ret = db.UpdateConferenceExternalID(conf.iDbID, conf.iInstanceID, conf.ESId);
                        if (!ret)
                        {
                            logger.Exception(100, "SetConference ProcessXML_Reservation failed");
                            return false;
                        }
                        ret = false; responseXml = "";
                        ret = GetReservationDialString(conf, ref responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, "GetReservationDialString failed");
                            return false;
                        }
                        ret = Process_ReservationDialString(conf, responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, "Process_ReservationDialString failed");
                            return false;
                        }
                        //ZD 100085
                        conf.etStatus = NS_MESSENGER.Conference.eStatus.SCHEDULED; //ZD 102200
                        db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus);
                        ret = Process_PoolOrder(conf, ref responseXml, 0);  //ZD 104256 //ALLDEV-779
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region SetConference for reccurence
        /// <summary>
        /// SetConference for reccurence
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool SetConferenceRecurrence(NS_MESSENGER.Conference templateConf)
        {
            logger.Trace("Entering SetConference function...");
            int cnfCnt = 0;
            NS_MESSENGER.Conference conf = null;
            NS_EMAIL.Email sendEmail = null;
            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>(); // FB 2441 II
            bool ret2 = false; //ALLBUGS-165
            try
            {
                sendEmail = new NS_EMAIL.Email(configParams);
                ret = false;

                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(templateConf);

                if (templateConf.iRecurring == 1 && isRecurring) 
                {
                    DeleteSyncConference(templateConf);
					//FB 2441 II Starts
                    db.FetchConf(ref confList,ref confs,NS_MESSENGER.MCU.eType.RPRM, 0); //for new and edit conference need to send delStatus as 0 //ZD 100221
                    confList=confs;
					//FB 2441 II Ends
                }

                for (cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    xWriter = null;
                    conf = null;
                    //DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromWebsite; //ZD 104496
                    settings = new XmlWriterSettings();
                    str_build = new StringBuilder();
                    settings.CheckCharacters = false;
                    settings.Encoding = Encoding.UTF8;
                    xWriter = XmlWriter.Create(str_build, settings);
                    conf = confList[cnfCnt];


                    if (templateConf.iRecurring == 1 && isRecurring) //ALLBUGS-165
                        ret2 = db.GetandSetMCUStartandEnd(ref conf);

                    ret = CreateXML_Reservation(ref conf, ref xWriter); //Call for Generate XML


                    if (!ret || str_build.ToString().Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed ");
                        
                    }

                    if (str_build.ToString().Contains("utf-16"))
                        str_build.Replace("utf-16", "utf-8");
                    if (str_build.ToString().Contains(":n1"))
                        str_build.Replace(":n1", "");
                    if (str_build.ToString().Contains(":n2"))
                        str_build.Replace(":n2", "");
                    if (str_build.ToString().Contains(":n3"))
                        str_build.Replace(":n3", "");

                    ContentType = "application/vnd.plcm.plcm-reservation+xml";
                    if (conf.ESId == "")
                    {
                        RequestMethod = "POST";
                        RestURI = "/api/rest/reservations";
                    }
                    else
                    {
                        RequestMethod = "PUT";
                        RestURI = "/api/rest/reservations/" + conf.ESId;
                    }


                    //Change the innerText of "start-time" and "end-time" for all instances.

                    ret = false; responseXml = "";
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, (int)NS_MESSENGER.AccessMode.FromWebsite, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "SetConference SendTransaction failed");
                        sendEmail.SendEmailtoHostAndMCUAdmin(conf, this.errMsg);
                        
                    }

                    if (conf.ESId.Trim() != "")
                    {

                        ret = false;
                        ret = db.UpdateConferenceExternalID(conf.iDbID, conf.iInstanceID, conf.ESId);
                        if (!ret)
                        {
                            logger.Exception(100, "SetConference ProcessXML_Reservation failed");
                            
                        }
                        ret = false; responseXml = "";
                        ret = GetReservationDialString(conf, ref responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, "GetReservationDialString failed");
                            
                        }
                        ret = false;
                        ret = Process_ReservationDialString(conf, responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, "Process_ReservationDialString failed");
                            
                        }
                        //ZD 100085
                        conf.etStatus = NS_MESSENGER.Conference.eStatus.SCHEDULED; //ZD 102200
                        db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus);
                        ret = Process_PoolOrder(conf, ref responseXml, 0); //ZD 104256  //ALLDEV-779                      
                    }
                }

                return ret;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region GetReservationDialString
        /// <summary>
        /// GetReservationDialString
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool GetReservationDialString(NS_MESSENGER.Conference conf, ref string responseXml)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromWebsite; //FB 2556
                RestURI = "/api/rest/reservations/" + conf.ESId;
                RequestMethod = "GET";
                ContentType = "application/vnd.plcm.plcm-reservation+xml";

                ret = false; responseXml = ""; str_build = new StringBuilder();
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "SetConference SendTransaction failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region GetTerminalStatus
        /// <summary>
        /// GetTerminalStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party particpant) // ZD 101157 added method
        {
            bool bRet = false;
            try
            {

                RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants";
                RequestMethod = "GET";
                ContentType = "application/vnd.plcm.plcm-participant-list+xml";

                ret = false; str_build = new StringBuilder(); DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; responseXml = ""; //FB 2556
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "GetTerminalStatus SendTransaction failed");
                    return false;
                }
                //responseXml ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ns3:plcm-participant-list xmlns:ns2=\"urn:com:polycom:api:rest:plcm-participant\" xmlns:ns3=\"urn:com:polycom:api:rest:plcm-participant-list\"><ns2:plcm-participant><link title=\"Participant MCU\" type=\"application/vnd.plcm.plcm-mcu+xml\" rel=\"urn:com:polycom:api:rest:link-relations:participant-mcu\" href=\"https://140.242.10.63:8443/api/rest/mcus/87466c58-792a-48e4-a11f-ccfcd5841054\"/><link title=\"Self Relationship\" type=\"application/vnd.plcm.plcm-participant+xml\" rel=\"self\" href=\"https://140.242.10.63:8443/api/rest/conferences/e36af0ec-0220-42c5-9a5a-3ee84db25cb0/participants/befa3e77-8e55-4c67-b4ce-74ce7a2bd1cf\"/><link title=\"Hosting Conference\" type=\"application/vnd.plcm.plcm-conference+xml\" rel=\"urn:com:polycom:api:rest:link-relations:host-conference\" href=\"https://140.242.10.63:8443/api/rest/conferences/e36af0ec-0220-42c5-9a5a-3ee84db25cb0\"/><ns2:participant-identifier>befa3e77-8e55-4c67-b4ce-74ce7a2bd1cf</ns2:participant-identifier><ns2:display-name>o</ns2:display-name><ns2:endpoint-name>64.47.30.244</ns2:endpoint-name><ns2:dial-string>64.47.30.244</ns2:dial-string><ns2:conference-identifier>e36af0ec-0220-42c5-9a5a-3ee84db25cb0</ns2:conference-identifier><ns2:mcu-name>SandBoxMCU</ns2:mcu-name><ns2:connection-status>CONNECTED_DIAL_OUT</ns2:connection-status><ns2:chairperson>false</ns2:chairperson><ns2:lecturer>false</ns2:lecturer><ns2:audio-mute>false</ns2:audio-mute><ns2:video-mute>false</ns2:video-mute><ns2:signaling-protocol>H323</ns2:signaling-protocol><ns2:encrypted-media>false</ns2:encrypted-media><ns2:current-call-bitrate-kbps>0</ns2:current-call-bitrate-kbps><ns2:current-layout>layout is controlled by conference</ns2:current-layout><ns2:passback>0603</ns2:passback><ns2:passthru>myvrm</ns2:passthru><ns2:speaker>false</ns2:speaker><ns2:entity-tag>a11204f180eac17033cf130d7e25055b</ns2:entity-tag><ns2:audio-mute-by-endpoint>true</ns2:audio-mute-by-endpoint><ns2:video-mute-by-endpoint>false</ns2:video-mute-by-endpoint><ns2:audio-mute-by-mcu>false</ns2:audio-mute-by-mcu><ns2:connection-start-time>2015-03-18T15:51:50.082+05:30</ns2:connection-start-time></ns2:plcm-participant><ns2:plcm-participant><link title=\"Participant MCU\" type=\"application/vnd.plcm.plcm-mcu+xml\" rel=\"urn:com:polycom:api:rest:link-relations:participant-mcu\" href=\"https://140.242.10.63:8443/api/rest/mcus/87466c58-792a-48e4-a11f-ccfcd5841054\"/><link title=\"Self Relationship\" type=\"application/vnd.plcm.plcm-participant+xml\" rel=\"self\" href=\"https://140.242.10.63:8443/api/rest/conferences/e36af0ec-0220-42c5-9a5a-3ee84db25cb0/participants/7e2f1c98-2664-44ec-b9f3-fc4ad0f25668\"/><link title=\"Hosting Conference\" type=\"application/vnd.plcm.plcm-conference+xml\" rel=\"urn:com:polycom:api:rest:link-relations:host-conference\" href=\"https://140.242.10.63:8443/api/rest/conferences/e36af0ec-0220-42c5-9a5a-3ee84db25cb0\"/><ns2:participant-identifier>7e2f1c98-2664-44ec-b9f3-fc4ad0f25668</ns2:participant-identifier><ns2:display-name></ns2:display-name><ns2:conference-identifier>e36af0ec-0220-42c5-9a5a-3ee84db25cb0</ns2:conference-identifier><ns2:mcu-name>SandBoxMCU</ns2:mcu-name><ns2:connection-status>CONNECTED_DIAL_OUT</ns2:connection-status><ns2:chairperson>false</ns2:chairperson><ns2:lecturer>false</ns2:lecturer><ns2:audio-mute>false</ns2:audio-mute><ns2:video-mute>false</ns2:video-mute><ns2:encrypted-media>false</ns2:encrypted-media><ns2:current-layout>layout is controlled by conference</ns2:current-layout><ns2:passback>0603</ns2:passback><ns2:passthru>myvrm</ns2:passthru><ns2:speaker>false</ns2:speaker><ns2:entity-tag>fce18dfab58a37cc120e64f99816ddd3</ns2:entity-tag><ns2:audio-mute-by-endpoint>false</ns2:audio-mute-by-endpoint><ns2:video-mute-by-endpoint>false</ns2:video-mute-by-endpoint><ns2:audio-mute-by-mcu>false</ns2:audio-mute-by-mcu><ns2:connection-start-time>2015-03-18T16:27:17.576+05:30</ns2:connection-start-time></ns2:plcm-participant></ns3:plcm-participant-list>";
                xdoc = XDocument.Parse(responseXml);

                XNamespace ns2 = "urn:com:polycom:api:rest:plcm-participant";
                XNamespace ns3 = "urn:com:polycom:api:rest:plcm-participant-list";

                //ZD 103184
                var UserData = (from usr in xdoc.Descendants(ns3 + "plcm-participant-list").Elements(ns2 + "plcm-participant")
                                select new NS_MESSENGER.User
                                {
                                    ParticipantGUID = usr.Elements(ns2 + "participant-identifier").Count() > 0 ? (string)usr.Element(ns2 + "participant-identifier").Value : "",
                                    Status = usr.Elements(ns2 + "connection-status").Count() > 0 ? usr.Element(ns2 + "connection-status").Value == "CONNECTED_DIAL_OUT" || usr.Element(ns2 + "connection-status").Value == "CONNECTED_DIAL_IN" ? 2 : 0 : 0, //ZD 102723
                                    IPISDNAddress = usr.Elements(ns2 + "endpoint-name").Count() > 0 ? (string)usr.Element(ns2 + "endpoint-name").Value : "",
                                    DialAddress = usr.Elements(ns2 + "dial-string").Count() > 0 ? (string)usr.Element(ns2 + "dial-string").Value : ""
                                }).ToList();


                //ZD 102817 Starts
                ret = false;
                ret = db.UpdateAllPartyStatus(conf.iDbID, conf.iInstanceID, ref  UserData, ref particpant); //ZD 103184
                if (!ret)
                {
                    logger.Exception(100, "GetEndpointStatus  update failed");
                    return false;
                }
                //ZD 102817 Ends
                for (int i = 0; i < UserData.Count; i++)
                {
                    bRet = false; ret = false;

                    if (!string.IsNullOrEmpty(UserData[i].DialAddress)) //ZD 104214
                        bRet = db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, UserData[i].DialAddress, UserData[i].ParticipantGUID, UserData[i].Status);

                    if (!bRet)
                    {
                        if (!string.IsNullOrEmpty(UserData[i].IPISDNAddress)) //ZD 104214
                        {
                            if (!db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, UserData[i].IPISDNAddress, UserData[i].ParticipantGUID, UserData[i].Status))
                            {
                                logger.Exception(100, "GetEndpointStatus  update failed");
                                bRet = false;
                            }
                        }

                         
                    }

                }

                return bRet;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf) // ZD 101157 Changes in whole method
        {
            try
            {

                RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants";
                RequestMethod = "GET";
                ContentType = "application/vnd.plcm.plcm-participant-list+xml";

                ret = false; str_build = new StringBuilder(); DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; responseXml = ""; //FB 2556
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "GetTerminalStatus SendTransaction failed");
                    return false;
                }
                xdoc = XDocument.Parse(responseXml);
                XNamespace ns2 = "urn:com:polycom:api:rest:plcm-participant";
                XNamespace ns3 = "urn:com:polycom:api:rest:plcm-participant-list";
                //ZD 103184
                var UserData = (from usr in xdoc.Descendants(ns3 + "plcm-participant-list").Elements(ns2 + "plcm-participant")
                                select new NS_MESSENGER.User
                                {
                                    ParticipantGUID = usr.Elements(ns2 + "participant-identifier").Count() > 0 ? (string)usr.Element(ns2 + "participant-identifier").Value : "",
                                    Status = usr.Elements(ns2 + "connection-status").Count() > 0 ? usr.Element(ns2 + "connection-status").Value == "CONNECTED_DIAL_OUT" || usr.Element(ns2 + "connection-status").Value == "CONNECTED_DIAL_IN" ? 2 : 0 : 0, //ZD 102723
                                    IPISDNAddress = usr.Elements(ns2 + "endpoint-name").Count() > 0 ? (string)usr.Element(ns2 + "endpoint-name").Value : "",
                                    DialAddress = usr.Elements(ns2 + "dial-string").Count() > 0 ? (string)usr.Element(ns2 + "dial-string").Value : ""
                                }).ToList();

                for (int i = 0; i < UserData.Count; i++)
                {
                    ret = false;
                    ret = db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, UserData[i].IPISDNAddress, UserData[i].ParticipantGUID, UserData[i].Status);
                    if (!ret)
                    {
                        logger.Exception(100, "GetEndpointStatus  update failed");
                        return false;
                    }

                }
                return true;
            }
            catch (Exception e)
            {                
                logger.Exception(100, e.InnerException.ToString());//ZD 102359
                logger.Exception(100, e.StackTrace);
                logger.Exception(100, e.Message);
                return false;
            }
        }


       
        //ZD 102359 Starts

        internal bool GetAddNewEndpointStatus(NS_MESSENGER.Conference conf) 
        {
            try
            {

                RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants";
                RequestMethod = "GET";
                ContentType = "application/vnd.plcm.plcm-participant-list+xml";

                ret = false; str_build = new StringBuilder(); DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; responseXml = ""; //FB 2556
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "GetTerminalStatus SendTransaction failed");
                    return false;
                }
                xdoc = XDocument.Parse(responseXml);
                XNamespace ns2 = "urn:com:polycom:api:rest:plcm-participant";
                XNamespace ns3 = "urn:com:polycom:api:rest:plcm-participant-list";
                var UserData = (from usr in xdoc.Descendants(ns3 + "plcm-participant-list").Elements(ns2 + "plcm-participant")
                                select new NS_MESSENGER.User
                                {
                                    ParticipantGUID = usr.Element(ns2 + "participant-identifier").Value,
                                    Status = usr.Element(ns2 + "connection-status").Value == "CONNECTED_DIAL_OUT" || usr.Element(ns2 + "connection-status").Value == "CONNECTED_DIAL_IN" ? 2 : 0, //ZD 102723
                                }).ToList();
                for (int i = 0; i < UserData.Count; i++)
                {
                    ret = false;
                    ret = db.UpdateParticipantStatus(conf.iDbID, conf.iInstanceID, UserData[i].ParticipantGUID, UserData[i].Status);
                    if (!ret)
                    {
                        logger.Exception(100, "GetEndpointStatus  update failed");
                        return false;
                    }

                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.InnerException.ToString());
                logger.Exception(100, e.StackTrace);
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 102359 Ends
        #endregion

        #region TerminateConference
        /// <summary>
        /// TerminateConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                List<NS_MESSENGER.Conference>  confs = new List<NS_MESSENGER.Conference>(); // FB 2441
                confList.Add(conf);
				// FB 2441 II Starts
                if (conf.iRecurring == 1 && isRecurring)
                {
                    db.FetchConf(ref confList, ref confs,NS_MESSENGER.MCU.eType.RPRM, 1); //for delete mode need to send delStatus as 1 //ZD 100221
                    confList = confs;
                }
				// FB 2441 II Ends
                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    DMAorRPRM = confList[cnfCnt].iAccessType; //FB 2556
                    if (DMAorRPRM == (int)NS_MESSENGER.AccessMode.FromWebsite) //FB 2556
                        RestURI = "/api/rest/reservations/" + confList[cnfCnt].ESId;
                    else if(!string.IsNullOrEmpty(confList[cnfCnt].sGUID))//ALLBUGS-48
                        RestURI = "/api/rest/conferences/" + confList[cnfCnt].sGUID;
                    else
                    {
                        RestURI = "/api/rest/reservations/" + confList[cnfCnt].ESId; 
                        DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromWebsite;
                    }//ALLBUGS-48
                    ContentType = "*/*";
                    RequestMethod = "DELETE";
                    ret = false; str_build = new StringBuilder(); responseXml = "";
                    ret = SendTransaction_overXMLHTTP(confList[cnfCnt], str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "TerminateConference failed");
                        //return false;
                    }
                    else
                    {
                        //ZD 100221 Starts
                        if (confList[cnfCnt].iWebExconf != 1)
                        {
                            ret = false;
                            ret = db.UpdateSyncStatus(confList[cnfCnt]);
                            if (!ret)
                                logger.Exception(100, "Delete failed in confSyncMCUAdj");
                        }
                        //ZD 100221 Ends
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }

        internal bool DeleteSyncConference(NS_MESSENGER.Conference conf)
        {
            List<NS_MESSENGER.Conference> confListDelete = null;
            try
            {
                confListDelete = new List<NS_MESSENGER.Conference>();
                confListDelete.Add(conf);

                db.FetchSyncAdjustments(ref confListDelete,NS_MESSENGER.MCU.eType.RPRM); //ZD 100221

                for (int cnfCnt = 0; cnfCnt < confListDelete.Count; cnfCnt++)
                {

                    confListDelete[cnfCnt].cMcu = conf.cMcu;
                    DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromWebsite; //FB 2556
                    RestURI = "/api/rest/reservations/" + confListDelete[cnfCnt].ESId;
                    ContentType = "*/*";
                    RequestMethod = "DELETE";
                    ret = false; str_build = new StringBuilder(); responseXml = "";
                    ret = SendTransaction_overXMLHTTP(confListDelete[cnfCnt], str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "TerminateConference failed");
                        //return false;
                    }
					// FB 2441 II Starts
                    else
                    {
                        //ZD 100221 Starts
                        if (confList[cnfCnt].iWebExconf != 1)
                        {
                            ret = false;
                            ret = db.UpdateSyncStatus(confListDelete[cnfCnt]);
                            if (!ret)
                                logger.Exception(100, "Delete failed in confSyncMCUAdj");
                            
                        }
                        //ZD 100221 Ends

                    }
					// FB 2441 II Ends
                   
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }

        #endregion
		
		// FB 2441 II Starts
        internal bool DeleteConfChangeMCU(NS_MESSENGER.Conference conf)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromWebsite; //FB 2556
                RestURI = "/api/rest/reservations/" + conf.ESId;
                ContentType = "application/vnd.plcm.plcm-reservation+xml";
                RequestMethod = "DELETE";
                ret = false; str_build = new StringBuilder(); responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "TerminateConference failed for conference ESID" + conf.ESId);
                    return false;
                }
             
       
                return true;


                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }
		// FB 2441 II Ends
        #region ConnectDisconnectEndpoint
        /// <summary>
        /// ConnectDisconnectEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="connectDisconnect"></param>
        /// <returns></returns>
        internal bool ConnectDisconnectEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool connectDisconnect)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                conf.cMcu = party.cMcu;
				//ZD 102359 Starts
                if (connectDisconnect == true)
                {
                    AddPartyToMcu(conf, party);
                }
                else
                {

                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/disconnect";
                    RequestMethod = "POST";
                    ContentType = "*/*";
                    ret = false; str_build = new StringBuilder(); responseXml = "";
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "Disconnect Endpoint is Failed");
                        return false;
                    }
                    //FB 2989 Starts
                    else
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;

                        if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                        {
                            db.UpdateP2PConferenceEndpointStatus(conf.iDbID, conf.iInstanceID, party.etStatus);
                        }
                        else
                        {
                            if (party.etType == NS_MESSENGER.Party.eType.USER || party.etType == NS_MESSENGER.Party.eType.GUEST)
                            {
                                db.UpdateConferenceUserStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                            }
                            else
                            {
                                if (party.etType == NS_MESSENGER.Party.eType.ROOM)
                                {
                                    db.UpdateConferenceRoomStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                                }
                                else
                                {
                                    db.UpdateConferenceCascadeStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                                }
                            }
                        }
                    }
                }
                //FB 2989 Ends //ZD 102359 Ends
                return true;
            }
            catch (Exception e)
            {

                logger.Trace("ConnectDisconnectEndpoint:" + e.Message);
                return false;
            }
        }
        #endregion

        #region ConferenceMessage
        /// <summary>
        /// ConferenceMessage
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        internal bool ConferenceMessage(NS_MESSENGER.Conference conf, string Message)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "POST";
                RestURI = "/api/rest/conferences/" + conf.sGUID + "/set-display-text/" + Message;

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "ConferenceMessage SendTransaction failed");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("ConferenceMessage:" + ex.Message);
                return false;

            }
        }
        #endregion

        #region LockUnlockConference
        /// <summary>
        /// LockUnlockConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="lockunlock"></param>
        /// <returns></returns>
        internal bool LockUnlockConference(NS_MESSENGER.Conference conf, int lockunlock)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "POST";

                if (lockunlock == (int)lockunlockconference.lockconfernece)
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/lock-conference";
                else
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/unlock-conference";


                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "LockUnlockConference SendTransaction failed");
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("LockUnlockConference Block:" + ex.Message);
                return false;
            }
        }
        #endregion


        //FB 2709 

        #region CreateUser
        /// <summary>
        /// CreateUser
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="Userlogin"></param>
        /// <returns></returns>
        internal bool CreateUser(NS_MESSENGER.MCU mcu, string Userlogin,ref bool UserExist)
        {
            NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
            str_build = new StringBuilder();
            try
            {
                conf.cMcu = mcu;
                xWriter = null;
                settings = new XmlWriterSettings();
                settings.CheckCharacters = false;
                settings.Encoding = Encoding.UTF8;
                settings.OmitXmlDeclaration = true;
                xWriter = XmlWriter.Create(str_build, settings);
                ret = CreateXML_CreateUser(ref mcu, Userlogin, ref xWriter);
                if (!ret)
                {
                    logger.Exception(100, "Create User XML failed");
                }

                if (str_build.ToString().Contains("utf-16"))
                    str_build.Replace("utf-16", "utf-8");
                if (str_build.ToString().Contains(":n1"))
                    str_build.Replace(":n1", "");
                if (str_build.ToString().Contains(":n2"))
                    str_build.Replace(":n2", "");
                if (str_build.ToString().Contains(":n3"))
                    str_build.Replace(":n3", "");

                ContentType = "application/vnd.plcm.plcm-user+xml";
                RequestMethod = "POST";
                RestURI = "/api/rest/users";

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    if (responseXml.Contains("409"))
                        UserExist = true;
                    else
                        UserExist = false;
                    logger.Exception(100, "CreateUser SendTransaction failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("CreateUser:" + e.Message);
                return false;
            }
        }
        #endregion


        #region MuteUnMuteParties
        /// <summary>
        /// MuteUnMuteParties
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="PartyList"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteUnMuteParties(NS_MESSENGER.Conference conf, List<NS_MESSENGER.Party> PartyList, bool mute)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                settings.CheckCharacters = false;
                settings.Encoding = Encoding.UTF8;
                xWriter = XmlWriter.Create(str_build, settings);

                ret = false;
                //mute true-MuteAllPartiesExcept; false-UnMuteAllParties; 
                if (mute)
                {
                    ret = CreateXML_MuteAllExceptParticipant(ref conf, PartyList, ref xWriter);

                    if (!ret || str_build.ToString().Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed");
                        return false;
                    }

                    if (str_build.ToString().Contains("utf-16"))
                        str_build.Replace("utf-16", "utf-8");
                    if (str_build.ToString().Contains(":n1"))
                        str_build.Replace(":n1", "");
                }

                ContentType = "*/*";
                RequestMethod = "POST";

                if (mute)
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/mute-all-except";
                else
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/unmute-all";

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "MuteUnMuteParties SendTransaction failed");
                    return false;
                }
                return true;
            }

            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region StartStopRecord
        /// <summary>
        /// StartStopRecord
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="StartStopRecord"></param>
        /// <returns></returns>
        internal bool StartStopRecord(NS_MESSENGER.Conference conf, int StartStopRecord)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "POST";

                if (StartStopRecord == (int)Startstoprecord.startRecord)
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/start-recording";
                else
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/stop-recording";

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "StartStopRecord SendTransaction failed ");
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("Start Stop Record block:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region ExtendConference
        /// <summary>
        /// ExtendConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="extendTime"></param>
        /// <returns></returns>
        internal bool ExtendConference(NS_MESSENGER.Conference conf, int extendTime)
        {
            logger.Trace("Entering Extend Conference function...");
            ret = false;
            try
            {
                if (conf != null)
                {
                    conf.iDuration = conf.iDuration + extendTime; //ALLDEV-779
                    Process_PoolOrder(conf, ref responseXml, 1); //ALLDEV-779
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

        #region MuteEndpoint
        /// <summary>
        /// MuteEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="mute"></param>
        /// <param name="PartyTerminalType"></param>
        /// <returns></returns>
        internal bool MuteEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute, int PartyTerminalType)
        {
            logger.Trace("Entering MuteEndpoint function...");
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                conf.cMcu = party.cMcu;
                if (mute == true)
                {
                    if (PartyTerminalType == (int)NS_OPERATIONS.Operations.AudioVideoParty.Audio)
                        RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/mute-audio";
                    else
                        RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/suspend-video";
                }
                else
                {
                    if (PartyTerminalType == (int)NS_OPERATIONS.Operations.AudioVideoParty.Audio)
                        RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/unmute-audio";
                    else
                        RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/resume-video";
                }

                ContentType = "*/*";
                RequestMethod = "POST";

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "MuteEndpoint SendTransaction failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

        }
        #endregion

        #region GetETagValue
        /// <summary>
        /// GetETagValue
        /// </summary>
        /// <param name="URI"></param>
        /// <param name="credentials"></param>
        /// <returns></returns>
        internal bool GetETagValue(string URI, string credentials, ref DateTime Date, ref string Etag, int Timeout)
        {
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.Expect100Continue = false;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(URI);
                //req.Credentials = cache;

                req.MediaType = "HTTP::/1.1";
                req.Headers["Authorization"] = "Basic " + credentials;
                req.Method = "GET";
                req.KeepAlive = true;
                req.Timeout = Timeout;
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                if (resp.Headers["ETag"] != null)
                    Etag = resp.Headers["ETag"].ToString();
                if (resp.Headers["Date"] != null)
                    Date = DateTime.Parse(resp.Headers["Date"].ToString());
                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
           
        }
        #endregion

        #region AddPartyToMcu

        /// <summary>
        /// AddPartyToMcu
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool AddPartyToMcu(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                settings.CheckCharacters = false;
                settings.Encoding = Encoding.UTF8;
                xWriter = XmlWriter.Create(str_build, settings);
                // FB 2441 II Starts
                ret = false;
                ret = CreateXML_AddParty(conf, party, ref xWriter);
                // FB 2441 II Ends

                if (!ret || str_build.ToString().Length < 1)
                {
                    logger.Exception(100, "Creation of AddPartyToMcu failed . commonXML = " + str_build.ToString());
                    return false;
                }

                if (str_build.ToString().Contains("utf-16"))
                    str_build.Replace("utf-16", "utf-8");
                // FB 2441 II Starts
                if (str_build.ToString().Contains(":n1"))
                    str_build.Replace(":n1", "");
                // FB 2441 II Ends
                ContentType = "application/vnd.plcm.plcm-participant+xml"; //ZD 100685
                RequestMethod = "POST";
                RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants";


                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "AddPartyToMcu SendTransaction failed");
                    return false;
                }
                // FB 2441 II Starts
                else if (party.bMute)
                {
                    ContentType = "*/*";
                    RequestMethod = "POST";
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/set-join-new-calls-muted/true";
                    ret = false; responseXml = ""; str_build = new StringBuilder();
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);

                }
                if (!ret)
                {
                    logger.Exception(100, "SetJoin muteparty SendTransaction failed");
                    return false;
                }
                else
                    return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.InnerException.ToString());//ZD 102359
                logger.Exception(100, e.StackTrace);
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region DeletePartyFromMcu
        /// <summary>
        /// DeletePartyFromMcu
        /// </summary>
        /// <param name="party"></param>
        /// <param name="dbConfName"></param>
        /// <returns></returns>
        internal bool DeletePartyFromMcu(NS_MESSENGER.Party party, string dbConfName)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region  OngoingConfOps
        /// <summary>
        /// OngoingConfOps
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="msg"></param>
        /// <param name="newMuteValue"></param>
        /// <param name="newLayoutValue"></param>
        /// <param name="extendTime"></param>
        /// <param name="connectOrDisconnect"></param>
        /// <param name="terminalStatus"></param>
        /// <returns></returns>
        internal bool OngoingConfOps(string operation, NS_MESSENGER.Conference conf, int layout)
        {
            try
            {
                bool ret = false;
                switch (operation)
                {
                    case "ChangeConferenceDisplayLayout":
                        {
                            ret = false;
                            ret = ChangeDisplayLayoutOfConference(conf, layout);
                            if (!ret) return false;
                            break;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region ChangeDisplayLayoutOfConference
        /// <summary>
        /// ChangeDisplayLayoutOfConference
        /// </summary>
        /// <param name="newLayoutPartyXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="layout"></param>
        /// <returns></returns>
        private bool ChangeDisplayLayoutOfConference(NS_MESSENGER.Conference conf, int layout)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "POST";
                string strLayout = "";

                ret = false;
                ret = EquateVideoLayout(layout, ref strLayout);

                RestURI = "/api/rest/conferences/" + conf.sGUID + "/set-conference-layout/" + strLayout;  // NEED to change the Dynamic Layout 

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "ChangeDisplayLayoutOfConference SendTransaction failed");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CheckMcuTime
        /// <summary>
        /// CheckMcuTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchMCUDetails(NS_MESSENGER.MCU mcu, int Timeout)
        {
            try
            {
                int pollStatus = (int)NS_MESSENGER.MCU.PollState.PASS;//ZD 100369_MCU
                MCUIP = mcu.sDMAip;
                MCUPort = mcu.iDMAHttpport;
                MCULogin = mcu.sDMALogin;
                MCUPassword = mcu.sDMAPassword;
                
                if (MCUPort == 8443)
                    strUrl = "https://" + MCUIP + ":" + MCUPort;
                else
                    strUrl = "http://" + MCUIP + ":" + MCUPort;

                credentials = MCULogin + ":" + MCUPassword;
                credentials = encodeString(credentials);

                Date = new DateTime(); ret = false;
                ret = GetETagValue(strUrl, credentials, ref Date, ref Etag, Timeout);
                if (!ret)
                {

                    logger.Exception(100, "FetchMCUProfileDetails failed");
                    pollStatus = (int)NS_MESSENGER.MCU.PollState.FAIL;//ZD 100369_MCU
                }
                else
                    if (Date != DateTime.MinValue)
                    {
                        db = new NS_DATABASE.Database(configParams);
                        db.UpdateMcuCurrentTime(mcu.iDbId, Date);
                    }

                db.UpdateMcuStatus(mcu, pollStatus);//ZD 100369_MCU
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }


            //return true;
        }
        #endregion


        // FB 2683 Starts
        #region PolycomRPRMCDR
        /// <summary>
        /// PolycomRPRMCDR
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="Directory"></param>
        /// <param name="LastModifiedTime"></param>
        /// <returns></returns>
        internal bool PolycomRPRMCDR(NS_MESSENGER.MCU MCU, ref string Directory, ref DateTime LastModifiedTime)
        {
            bool ret = false;
            try
            {
                logger.Trace("Entering into Polycom RPRM CDR");
                string FileName = "", startDate = "";
                DateTime confStartTime = new DateTime();
                LastModifiedTime = new DateTime();

                ret=db.selectLastModifiedDateTime(MCU, ref startDate);
                if (!ret)
                {
                    logger.Trace("Error in fetching start DateTime");
                    return false;
                }

                confStartTime = DateTime.Parse(startDate);
                LastModifiedTime = DateTime.UtcNow;

                if (LastModifiedTime <= confStartTime)//ALLBUGS-154
                    confStartTime = LastModifiedTime.AddDays(-1);

                if (MCU.iDMAHttpport == 8443)
                    strUrl = "https://" + MCU.sDMAip + ":" + MCU.iDMAHttpport;
                else if (MCU.iDMAHttpport == 443)
                    strUrl = "https://" + MCU.sDMAip;
                else
                    strUrl = "http://" + MCU.sDMAip + ":" + MCU.iDMAHttpport;

                strUrl = strUrl + "/api/rest/billing?to-date=" + LastModifiedTime.ToString(@"yyyy-MM-dd\THH:mm:ss\Z") + "&from-date=" + confStartTime.ToString(@"yyyy-MM-dd\THH:mm:ss\Z");
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                ((sender1, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.Expect100Continue = false;

                Directory =Directory+ @"\RPRMCDR\" + DateTime.Now.ToString("ssMMssddssyyyyfff");
                DirectoryInfo dir = new DirectoryInfo(Directory);
                if (!dir.Exists)
                    dir.Create();
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(MCU.sDMALogin, MCU.sDMAPassword);
                    FileName = DateTime.Now.ToString("ssMMssddyy") + "CDR.zip";
                    client.DownloadFile(strUrl, Directory + "\\" + FileName);
                }
                using (ZipFile zip = ZipFile.Read(Directory + "\\" + FileName))
                {
                    zip.ExtractAll(Directory, ExtractExistingFileAction.OverwriteSilently);
                }
                
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("Error in Fetching CDR from DMA"+ex.Message);
                return false;
            }
        }
        #endregion
        //FB 2683 Ends

        #region GetDMAConferenceDetails
        /// <summary>
        /// GetDMAConferenceDetails
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        public bool GetDMAConferenceDetails(NS_MESSENGER.Conference conf)
        {
            try
            {
                string DialinNumber = ""; // FB 2689
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; //FB 2556
                str_build = new StringBuilder();
                ContentType = "application/vnd.plcm.plcm-conference-list+xml";
                RequestMethod = "GET";
                // FB 2689 start

                DialinNumber = conf.sDialString.Trim();

                int DMADialinprefixlen;
                if (conf.sDialString.Trim().IndexOf(conf.cMcu.sDMADialinprefix.Trim(), 0) >= 0)
                {
                    DMADialinprefixlen = conf.cMcu.sDMADialinprefix.Trim().Length;
                   
                    string Replacestring = conf.sDialString.Trim().Substring(0, DMADialinprefixlen);
                    if (Replacestring == conf.cMcu.sDMADialinprefix.Trim())
                    {
                        DialinNumber = conf.sDialString.Trim().Substring(DMADialinprefixlen);
                    }
                }
                RestURI = "/api/rest/conferences?conference-room-identifier=" + DialinNumber;
                
                // FB 2689 end
                

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Trace(RestURI + " dial str is : " + conf.sDialString + "Prefix is :" + conf.cMcu.sDMADialinprefix.ToString());  //FB 2689
                    logger.Exception(100, "GetDMAConferenceDetails SendTransaction failed" + responseXml);
                    return false;
                }

                if (responseXml.Length > 0)
                {
                    xdoc = XDocument.Parse(responseXml);
                    XNamespace ns4 = "urn:com:polycom:api:rest:plcm-conference-list";
                    XNamespace ns3 = "urn:com:polycom:api:rest:plcm-conference";
                    conf.sGUID = xdoc.Element(ns4 + "plcm-conference-list").Element(ns3 + "plcm-conference").Element(ns3 + "conference-identifier").Value;
					
					//FB 2683 Starts
                    ret = false;
                    conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING; //ZD #100093 & ZD #100085
                    ret = db.UpdateConference(conf.iDbID, conf.iInstanceID, conf.sGUID, conf.etStatus, string.Empty);//ZD 101217 //ZD 100085
                    if (!ret)
                        logger.Trace("GUID Updatation is failed");

                    ret = false;
                    ret = db.UpdateConfUIDonMCU(conf.sGUID, conf);
                    if (!ret)
                    {
                        logger.Trace("Error in Update the ConfGUIDonMCU");
                        return false;
                    }
					//FB 2683 Ends
                }
            }
            catch (Exception ex)
            {
                logger.Trace("GetDMAConference failed: " + ex.Message);
                return false;
            }

            return true;
        }
        #endregion

        #region Process_PoolOrder
        /// <summary>
        /// Process_PoolOrder
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="DialinNumber"></param>
        /// <returns></returns>
        internal bool Process_PoolOrder(NS_MESSENGER.Conference conf, ref string responseXml, int isExtdConf) //ALLDEV-779
        {
            try
            {
                string ConfPoolName = "", DialinNumber = "", VMROwnername = "", ConfTermpalte = ""; //ZD 104688 //ALLDEV-779
                string confStartTime = "", ConfEndTime = "";
                TimeSpan timeDiff = conf.dtStartDateTimeInUTC.Subtract(DateTime.UtcNow);

                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService; 

                #region EtagUpdate VMROwnername

                MCUIP = conf.cMcu.sDMAip;
                MCUPort = conf.cMcu.iDMAHttpport;
                MCULogin = conf.cMcu.sDMALogin;
                MCUPassword = conf.cMcu.sDMAPassword;

                int DMADialinprefixlen;
                if (conf.sDialString.Trim().IndexOf(conf.cMcu.sDMADialinprefix.Trim(), 0) >= 0)
                {
                    DMADialinprefixlen = conf.cMcu.sDMADialinprefix.Trim().Length;

                    string Replacestring = conf.sDialString.Trim().Substring(0, DMADialinprefixlen);
                    if (Replacestring == conf.cMcu.sDMADialinprefix.Trim())
                        DialinNumber = conf.sDialString.Trim().Substring(DMADialinprefixlen);
                }

                credentials = MCULogin + ":" + MCUPassword;
                credentials = encodeString(credentials);

                //ZD 104688 Starts
                ret = false; responseXml = ""; str_build = new StringBuilder(); 

                RequestMethod = "GET";
                ContentType = "*/*";
                RestURI = "/api/rest/conference-rooms/" + DialinNumber;

                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Trace(RestURI + " dial str is : " + conf.sDialString + "Prefix is :" + conf.cMcu.sDMADialinprefix.ToString());
                    logger.Exception(100, "Update Poll Order SendTransaction failed" + responseXml);
                    return false;
                }
                xdoc = XDocument.Parse(responseXml);
                XNamespace ns4 = "urn:com:polycom:api:rest:plcm-conference-room";
                if (xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "owner-username") != null)
                    VMROwnername = xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "owner-username").Value;
                if (xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "entity-tag") != null)
                    Etag = xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "entity-tag").Value;
                //ZD 104688 Ends
                if (isExtdConf == 1) //ALLDEV-779
                {
                    if (xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "mcu-pool-order-name") != null) //ALLDEV-779
                        ConfPoolName = xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "mcu-pool-order-name").Value;
                    if (xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "not-before") != null) //ALLDEV-779
                        confStartTime = xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "not-before").Value;
                    if (xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "conference-template-name") != null) //ALLDEV-779
                        ConfTermpalte = xdoc.Element(ns4 + "plcm-conference-room").Element(ns4 + "conference-template-name").Value;
                }
                #endregion

				//ZD 104465 Starts
                if (isExtdConf == 0) //ALLDEV-779
                {
                    if (conf.iConfPoolOrderID > 0)
                        db.FetchPoolOrderName(conf.iConfPoolOrderID, conf.cMcu.iDbId, ref ConfPoolName);
                    else if (conf.cMcu.iPoolOrder > 0)
                        db.FetchPoolOrderName(conf.cMcu.iPoolOrder, conf.cMcu.iDbId, ref ConfPoolName);
                    else
                        return true;
                    //ZD 104465 Ends                              

                    if (timeDiff.Days != 0 || timeDiff.Hours != 0 || timeDiff.Minutes >= 5)
                        confStartTime = conf.dtStartDateTimeInUTC.AddMinutes(-conf.iSyncLaunchBuffer).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");    // scheduled reservation //ALLDEV-837
                    else
                        confStartTime = DateTime.UtcNow.AddSeconds(180).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");// immediate conf. so it needs adjustment. //FB 2689


                    if (conf.iConfRMXServiceID > 0) //ZD 104465 Starts
                        ConfTermpalte = conf.sPolycomTemplate;
                    else
                        ConfTermpalte = conf.cMcu.sTemplateName; //ZD 104465 Ends
                }

                ConfEndTime = conf.dtStartDateTimeInUTC.AddMinutes(conf.iDuration).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");

                str_build = new StringBuilder();
                ContentType = "application/vnd.plcm.plcm-conference-room+xml";
               
                RequestMethod = "PUT";

                RestURI = "/api/rest/conference-rooms/" + DialinNumber;

                settings = new XmlWriterSettings();
                settings.CheckCharacters = false;
                settings.Encoding = Encoding.UTF8;
                xWriter = XmlWriter.Create(str_build, settings);

                xWriter.WriteStartElement("plcm-conference-room");
                xWriter.WriteAttributeString("xmlns", "n3", null, "urn:com:polycom:api:rest:plcm-conference-room");
                xWriter.WriteElementString("owner-domain", conf.cMcu.sRPRMDomain);
                xWriter.WriteElementString("owner-username", VMROwnername); //ZD 104688
                xWriter.WriteElementString("conference-room-identifier", DialinNumber);
                xWriter.WriteElementString("dial-in-number", DialinNumber);
                xWriter.WriteElementString("conference-template-name", ConfTermpalte); //ALLDEV-779
                xWriter.WriteElementString("mcu-pool-order-name", ConfPoolName);
                xWriter.WriteElementString("chairperson-required", "false");
                xWriter.WriteElementString("entity-tag", Etag);
                xWriter.WriteElementString("auto-dial-out", "false");
                xWriter.WriteElementString("not-before", confStartTime);
                xWriter.WriteElementString("not-after", ConfEndTime);
                xWriter.WriteEndElement();
                xWriter.Flush();

                if (str_build.ToString().Contains("utf-16"))
                    str_build.Replace("utf-16", "utf-8");
                if (str_build.ToString().Contains(":n1"))
                    str_build.Replace(":n1", "");
                if (str_build.ToString().Contains(":n2"))
                    str_build.Replace(":n2", "");
                if (str_build.ToString().Contains(":n3"))
                    str_build.Replace(":n3", "");


                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Trace(RestURI + " dial str is : " + conf.sDialString + "Prefix is :" + conf.cMcu.sDMADialinprefix.ToString()); 
                    logger.Exception(100, "UpdateDMAConferenceDetails SendTransaction failed" + responseXml);
                    //return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.InnerException.ToString());//ZD 102359
                logger.Exception(100, e.StackTrace);
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        //ZD 101527 Start
        #region FetchEndpoints
        internal bool FetchEndpoints(NS_MESSENGER.MCU mcu) //FetchEndpoints
        {
            try
            {
                int OrgID = 11;
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf.cMcu = mcu;
                RestURI = "/api/rest/devices";
                ContentType = "application/vnd.plcm.plcm-device-list+xml";
                              
                NS_MESSENGER.RPRMEndpointsProfile EptProfile = new NS_MESSENGER.RPRMEndpointsProfile();

                RequestMethod = "GET";
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromWebsite; 

                ret = false; responseXml = ""; str_build = new StringBuilder(); responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);

                xdoc = XDocument.Parse(responseXml);

                XPathDocument _xmlns = new XPathDocument(new StringReader(xdoc.ToString()));
                XPathNavigator _Xnav = _xmlns.CreateNavigator();
                _Xnav.MoveToFollowing(XPathNodeType.Element);
                var namespacelist = _Xnav.GetNamespacesInScope(XmlNamespaceScope.All).ToList();

                var ns5 = namespacelist.Where(data => data.Key == "ns5").ToList();
                var ns6 = namespacelist.Where(data => data.Key == "ns6").ToList();
                var ns7 = namespacelist.Where(data => data.Key == "ns7").ToList();
                var ns8 = namespacelist.Where(data => data.Key == "ns8").ToList();
                var ns11 = namespacelist.Where(data => data.Key == "ns11").ToList();
                var ns10 = namespacelist.Where(data => data.Key == "ns10").ToList();

                XNamespace NS5 = ns5[0].Value;
                XNamespace NS6 = ns6[0].Value;
                XNamespace NS7 = ns7[0].Value;
                XNamespace NS8 = ns8[0].Value;
                XNamespace NS11 = ns11[0].Value;
                XNamespace NS10 = ns10[0].Value;

                var Template = (from temp in xdoc.Descendants(NS11 + ns11[0].Value.Split(':')[5]).Elements(NS10 + ns10[0].Value.Split(':')[5])
                                select new NS_MESSENGER.RPRMEndpoints
                                {
                                    orgID =  OrgID,
                                    EndpointName = RemoveSplCharacters(temp.Element(NS10 + "device-name").Value), //ZD 102713
                                    EndpointProfileName = RemoveSplCharacters(temp.Element(NS10 + "device-name").Value), //ZD 102713
                                    identifier = Convert.ToInt32(temp.Element(NS10 + "device-identifier").Value),
                                    mcuID = mcu.iDbId,
                                    h323 = Convert.ToBoolean(temp.Element(NS10 + "supports-h323").Value),
                                    sip = Convert.ToBoolean(temp.Element(NS10 + "supports-sip").Value),
                                    isdn = Convert.ToBoolean(temp.Element(NS10 + "supports-isdn").Value),
                                    address = temp.Element(NS10 + "ip-address").Value,
                                    ModelType = temp.Element(NS10 + "device-type").Value,
                                    ProfileXML = "<EptProfileXML>"+ (((Convert.ToBoolean(temp.Element(NS10 + "supports-h323").Value) == true) ? temp.Element(NS7 + "plcm-h323-identity").ToString().Trim() : "") +
                                                 ((Convert.ToBoolean(temp.Element(NS10 + "supports-sip").Value) == true) ? temp.Element(NS5 + "plcm-sip-identity").ToString().Trim() : "") +
                                                 ((Convert.ToBoolean(temp.Element(NS10 + "supports-isdn").Value) == true) ? temp.Element(NS6 + "plcm-isdn-identity").ToString().Trim() : "")) + "</EptProfileXML>",
                                }).ToList();


                XDocument xmldoc = new XDocument();
                XmlDocument xmldocs = new XmlDocument();
                XmlNodeList xnodelist = null;
                string xNS5 = "xmlns:ns5=\"" + ns5[0].Value + "\"";
                string xNS6 = "xmlns:ns6=\"" + ns6[0].Value + "\"";
                string xNS7 = "xmlns:ns7=\"" + ns7[0].Value + "\"";
                string xNS8 = "xmlns:ns8=\"" + ns8[0].Value + "\"";
                for (int t = 0; t < Template.Count; t++)
                {
                    if (!string.IsNullOrEmpty(Template[t].ProfileXML))
                    {
                        Template[t].EptProfile = new List<NS_MESSENGER.RPRMEndpointsProfile>();
                       

                        xmldoc = XDocument.Parse(Template[t].ProfileXML);
                        xmldoc.Descendants().Select(o => o.Name = o.Name.LocalName).ToArray();
                        Template[t].ProfileXML = xmldoc.ToString();
                        Template[t].ProfileXML = Template[t].ProfileXML.Replace(xNS5, "").Replace(xNS6, "").Replace(xNS7, "").Replace(xNS8, "");
                        xmldocs.LoadXml(Template[t].ProfileXML);
                        if (Template[t].h323 == true)
                        {
                            xnodelist = xmldocs.SelectNodes("//plcm-h323-identity/h323-alias");
                            for (int h = 0; h < xnodelist.Count; h++)
                            {
                                EptProfile = new NS_MESSENGER.RPRMEndpointsProfile();
                                EptProfile.aliastype = 2; //H323
                                //ZD 102713 // Remove these special characters & < > % \ / ? | ^ = ` [ ] { } $ and ~
                                EptProfile.address = xnodelist[h].SelectSingleNode("value").InnerText.Replace("&", "").Replace("<", "").Replace("<", "").Replace("%", "").Replace("\"", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("^", "").Replace("=", "").Replace("`", "").Replace("[", "").Replace("]", "").Replace("{", "").Replace("}", "").Replace("$", "").Replace("~", "");//ZD 102713 
                                EptProfile.EndpointProfileName = RemoveSplCharacters(xnodelist[h].SelectSingleNode("plcm-h323-alias-type").InnerText); //ZD 102713
                                Template[t].EptProfile.Add(EptProfile);
                            }
                        }
                        if (Template[t].isdn == true)
                        {
                            xnodelist = xmldocs.SelectNodes("//plcm-isdn-identity/isdn-alias");
                            for (int h = 0; h < xnodelist.Count; h++)
                            {
                                EptProfile = new NS_MESSENGER.RPRMEndpointsProfile();
                                EptProfile.aliastype = 4; //ISDN
                                EptProfile.address = xnodelist[h].SelectSingleNode("value").InnerText;
                                EptProfile.EndpointProfileName = RemoveSplCharacters(xnodelist[h].SelectSingleNode("plcm-isdn-alias-type").InnerText); //ZD 102713
                                Template[t].EptProfile.Add(EptProfile);
                            }
                        }
                        if (Template[t].sip == true)
                        {
                            EptProfile = new NS_MESSENGER.RPRMEndpointsProfile();
                            //xnodelist = xmldocs.SelectNodes("//plcm-sip-identity/sip-alias");
                            if (xmldocs.SelectSingleNode("//plcm-sip-identity/sip-uri") != null)//ALLBUGS-130
                                EptProfile.address = xmldocs.SelectSingleNode("//plcm-sip-identity/sip-uri").InnerText;
                            EptProfile.aliastype = 6; //SIP
                            EptProfile.EndpointProfileName = "";
                            Template[t].EptProfile.Add(EptProfile);
                        }
                    }
                }

                ret = false;
                ret = db.SaveRPRMEndpointList(Template, mcu.iOrgId, 11);
                if (!ret)
                {
                    logger.Exception(100, "SaveRPRMEndpointList failed");
                    return false;
                }

                ret = false;
                ret = db.UpdateLastSyncDate(mcu.iOrgId);
                if (!ret)
                {
                    logger.Exception(100, "SaveRPRMEndpointList failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //ZD 101527 End
        //ZD 102713 starts
        #region RemoveSplCharacters

        protected string RemoveSplCharacters(string checkSplChar)
        {
            try
            {
                //Removing the special characters: & < > + % \ ? | ^ = ! ` [ ] { } $ @ and ~ 
                checkSplChar = checkSplChar.Replace("&amp;", " ");
                checkSplChar = checkSplChar.Replace("&", " ");
                checkSplChar = checkSplChar.Replace("<", "");
                checkSplChar = checkSplChar.Replace(">", "");
                checkSplChar = checkSplChar.Replace("+", "");
                checkSplChar = checkSplChar.Replace("%", "");
                checkSplChar = checkSplChar.Replace("\"", "");
                checkSplChar = checkSplChar.Replace("?", "");
                checkSplChar = checkSplChar.Replace("$", "");
                checkSplChar = checkSplChar.Replace("@", "");
                checkSplChar = checkSplChar.Replace("^", "");
                checkSplChar = checkSplChar.Replace("[", "");
                checkSplChar = checkSplChar.Replace("]", "");
                checkSplChar = checkSplChar.Replace("{", "");
                checkSplChar = checkSplChar.Replace("}", "");
                checkSplChar = checkSplChar.Replace("~", "");
                checkSplChar = checkSplChar.Replace("|", "");
                checkSplChar = checkSplChar.Replace("=", "");
                checkSplChar = checkSplChar.Replace("!", "");
                checkSplChar = checkSplChar.Replace("'", "");
                checkSplChar = checkSplChar.Replace(",", "");
            }
            catch (Exception e)
            {
                logger.Trace("Error in RemoveSplCharacters :" + e.StackTrace);
                errMsg = e.Message;
            }
            return checkSplChar;
        }

        #endregion
        //ZD 102713 Ends
       
        #endregion

        #region SendCommand
        /// <summary>
        /// SendTransaction_overXMLHTTP
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="sendXML"></param>
        /// <param name="receiveXML"></param>
        /// <param name="method"></param>
        /// <param name="esID"></param>
        /// <returns></returns>
        private bool SendTransaction_overXMLHTTP(NS_MESSENGER.Conference conf, string sendXML, string ContentType, string RestURI, string method, int DMAorRPRM, ref string responseXml)
        {

            String[] delimitedArr = { @"\" };
            string error = "";

            logger.Trace("Entering into SendTransaction_overXMLHTTP"); //FB 2689

            if (sendXML.Length < 1)
            {
                logger.Trace("No data being sent so it is delete command");

            }
            try
            {
                bool tryAgain = false;
                do
                {
                    tryAgain = false;

                    if (DMAorRPRM == (int)NS_MESSENGER.AccessMode.FromWebsite) //FB 2556
                    {
                        MCUIP = conf.cMcu.sIp.Trim();
                        MCUPort = conf.cMcu.iHttpPort;
                        MCULogin = conf.cMcu.sLogin;
                        MCUPassword = conf.cMcu.sPwd;
                    }
                    else
                    {
                        MCUIP = conf.cMcu.sDMAip;
                        MCUPort = conf.cMcu.iDMAHttpport;
                        MCULogin = conf.cMcu.sDMALogin;
                        MCUPassword = conf.cMcu.sDMAPassword;
                    }

                    if (MCUPort == 8443)
                        strUrl = "https://" + MCUIP + ":" + MCUPort + RestURI;
                    else if (MCUPort == 443)
                        strUrl = "https://" + MCUIP + RestURI;
                    else
                        strUrl = "http://" + MCUIP + ":" + MCUPort + "/" + RestURI;

                    logger.Trace("*********SendXML***********");
					// FB 2689 Starts
                    logger.Trace("Call Manage URL: " + strUrl);
                    logger.Trace("Call Reservation: "+ sendXML);

                    Uri mcuUri = new Uri(strUrl);
                    logger.Trace("URL SENT IS :" + strUrl);
					// FB 2689 Ends
                    credentials = MCULogin + ":" + MCUPassword;
                    credentials = encodeString(credentials);

                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                    ServicePointManager.Expect100Continue = false;

                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);

                    req.MediaType = "HTTP::/1.1";
                    req.Headers["Authorization"] = "Basic " + credentials;
                    req.Method = method;
                    req.KeepAlive = true;
                    
                    req.Headers.Add("Accept-Encoding", "gzip,deflate");//x-www-form-urlencoded";
                    if (sendXML != "")
                    {
                        req.ContentType = ContentType;
                        System.IO.Stream stream = req.GetRequestStream();
                        byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                        stream.Write(arrBytes, 0, arrBytes.Length);
                        stream.Close();
                    }
                    else if(!String.IsNullOrEmpty(ContentType))
                        req.Accept = ContentType;
                   
                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                    

                    Stream respStream = resp.GetResponseStream();
                    
                    StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.UTF8); //ZD 102384
                    responseXml = rdr.ReadToEnd();
                   
                    logger.Trace("*********ResponseXML***********");
                    logger.Trace(responseXml);
                    if (DMAorRPRM == (int)NS_MESSENGER.AccessMode.FromWebsite) //FB 2556
                    {
                        if (resp.Headers["Location"] != null)
                        {
                            conf.ESId = resp.Headers["Location"].ToString();
                            conf.ESId = conf.ESId.Split('/')[conf.ESId.Split('/').Length - 1];
                            conf.sEtag = resp.Headers["ETag"].ToString();
                            logger.Trace("Reservation Succeed:" + resp.StatusDescription);
                        }
                        if (RestURI == "/api/rest/status")
                        {
                            if (resp.StatusDescription.ToString() == "No Content")
                                return true;
                            else
                                return false;
                        }
                    }
                    rdr.Close();
                    resp.Close();
                    respStream.Close();
                    GC.Collect();
                    GC.WaitForFullGCComplete();
                }
                while (tryAgain == true);
                return true;
            }

            catch (WebException wex)
            {
				//ZD 101884
                logger.Trace("Error in SendTransaction_overXMLHTTP RPRM" + wex.Message);
                
                //FB 2689 Starts
                error = "Authentication Failed."; //ZD 100288

                logger.Exception(100, error);
				//FB 2689 Ends
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
							//FB 2689 Starts
                            error = "Error exception from DMA_RPRM = " + error;
                            logger.Exception(100, error);
							//FB 2689 Ends
                        }
                    }
                }
				//FB 2689 Starts
                if (error == "")
                {
                    throw wex;
                    logger.Exception(100, "EMTPY!!!!!");
                }

                this.errMsg = ProcessError(error);
                logger.Exception(100, "Fianlly Processed :" + this.errMsg);
                return false;
				//FB 2689 Ends



            }

            catch (Exception e)
            {
                this.errMsg = e.Message;
                string debugString = "Error sending data to bridge. Error = " + e.Message;
                debugString = "SendXML = " + sendXML;
                logger.Exception(100, debugString);
                return false;
            }

        }
        #endregion

        #region GenerateXML


        #region CreateXML_Reservation
        /// <summary>
        /// Block for Generate Reservation XML
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="reservationXml"></param>
        /// <returns></returns>
        private bool CreateXML_Reservation(ref NS_MESSENGER.Conference conf, ref XmlWriter xWriter)
        {
            try
            {
                string confStartTime = "", ConfEndTime = "";
                TimeSpan timeDiff = conf.dtStartDateTimeInUTC.Subtract(DateTime.UtcNow);

                if (timeDiff.Days != 0 || timeDiff.Hours != 0 || timeDiff.Minutes >= 5)
                    confStartTime = conf.dtStartDateTimeInUTC.AddMinutes(-conf.iSyncLaunchBuffer).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");    // scheduled reservation //ALLDEV-837
                else
                    confStartTime = DateTime.UtcNow.AddSeconds(180).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");// immediate conf. so it needs adjustment. //FB 2689


                ConfEndTime = conf.dtStartDateTimeInUTC.AddMinutes(conf.iDuration).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");

                xWriter.WriteStartElement("plcm-reservation");
                xWriter.WriteAttributeString("xmlns", "n1", null, "urn:com:polycom:api:rest:plcm-reservation");
                xWriter.WriteStartElement("plcm-reserved-participant-list");
                xWriter.WriteAttributeString("xmlns", "n2", null, "urn:com:polycom:api:rest:plcm-reserved-participant-list");


                CreatePartyListXML(ref conf, ref xWriter);  // Call for  ADD participant XML
                xWriter.WriteElementString("name", conf.sDbName.Replace("?", "").Replace(";", "").Replace("'", "").Replace("Á", "").Replace("É", "").Replace("Í", "").Replace("Ú", "").Replace("Ñ", "").Replace("Ü", "").Replace("á", "").Replace("é", "").Replace("Ó", "").Replace("í", "").Replace("ó", "").Replace("ú", "").Replace("ñ", "").Replace("ü", "").Replace("¿", "").Replace("¡", "").Replace("«", "").Replace("»", "").Replace("€", ""));

                if (conf.ESId != "")
                    xWriter.WriteElementString("reservation-identifier", conf.ESId);

                if (conf.sPwd.Trim() != "")
                    xWriter.WriteElementString("conf-passcode", conf.sPwd);

                xWriter.WriteElementString("chair-passcode", ""); // chair person and passcode is not myvrm so set as empty

                //ZD 104465 Starts
                string ConfProfileName = "";
                if (conf.iConfRMXServiceID > 0)
                {
                    db.FetchConferenceProfile(conf.iConfRMXServiceID, conf.cMcu.iDbId, ref ConfProfileName);
                    conf.sPolycomTemplate = ConfProfileName;
                    xWriter.WriteElementString("template-name", conf.sPolycomTemplate);
                }
                else
                    xWriter.WriteElementString("template-name", conf.cMcu.sTemplateName); // FB 2441 II //ZD 104465 Ends


                if (conf.cMcu.iDMASendmail == 1 && conf.iPolycomSendMail == 1)
                    xWriter.WriteElementString("send-email", "true");
                else
                    xWriter.WriteElementString("send-email", "false");

                xWriter.WriteElementString("supported-language-enum", "ENGLISH");
                if (conf.ESId == "")
                    xWriter.WriteElementString("dial-in-number", conf.iDbNumName.ToString());
                else
                {
                    xWriter.WriteElementString("dial-in-number", conf.sDialString.ToString());
                    xWriter.WriteElementString("entity-tag", conf.sEtag);
                }
                xWriter.WriteElementString("start-time", confStartTime);
                xWriter.WriteElementString("end-time", ConfEndTime); //2012-03-17T02:01:01+05:30 
                xWriter.WriteEndElement();
                xWriter.Flush();
            }

            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }//end of Reservation XML
        #endregion

        #region CreatePartyListXML

        /// <summary>
        /// CreatePartyListXML
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>


        private void CreatePartyListXML(ref NS_MESSENGER.Conference conf, ref XmlWriter xWriter)
        {

            string setConfowner = "false";   // As of now it is set as true need to change depends on host
			//ZD 101884 Starts
            if (conf.cMcu.sSoftwareVer == "8.0.x")
                setConfowner = "true";
            else
                setConfowner = "false";
			//ZD 101884 End
            try
            {
                while (conf.qParties.Count > 0)
                {
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)conf.qParties.Dequeue();
                    //if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate) //ALLDEV-779
                    //    party.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    SinglePartyXML(ref conf, ref party, ref setConfowner, ref xWriter);

                }
				//ZD 101884
                //if (conf.cMcu.sSoftwareVer == "8.1.x" || conf.cMcu.sSoftwareVer == "8.2.x" || conf.cMcu.sSoftwareVer == "8.3.x" || conf.cMcu.sSoftwareVer == "8.4.x" || conf.cMcu.sSoftwareVer == "8.4.1.x")//ZD 103467 //ZD ZD 104465 //ALLDEV-825
                if (!(conf.cMcu.sSoftwareVer == "8.0.x")) //ALLDEV-779
                    SetConfOwnerXML(ref conf, ref xWriter); // ZD 101337

                xWriter.WriteEndElement();

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);

            }

        }
        #endregion

        #region SinglePartyXML
        /// <summary>
        /// SinglePartyXML
        /// </summary>
        /// <param name="Conf"></param>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        /// ZD 101337 Changes
        internal void SinglePartyXML(ref NS_MESSENGER.Conference Conf, ref NS_MESSENGER.Party partyObj, ref string setConfOwner, ref XmlWriter xWriter)
        {
            string videoBitRate = "";

            try
            {
                logger.Trace("Party = " + partyObj.sName);
                if (partyObj.sName == null || partyObj.sName.Length < 3)
                {
                    logger.Trace("Invalid participant Name:");
                }

                EquateLineRate(partyObj.stLineRate.etLineRate, true, ref videoBitRate);

                xWriter.WriteStartElement("plcm-reserved-participant");
                xWriter.WriteAttributeString("xmlns", "n3", null, "urn:com:polycom:api:rest:plcm-reserved-participant");

                //ZD 101884
                if (Conf.cMcu.sSoftwareVer == "8.0.x" && setConfOwner == "true")
                {
                    xWriter.WriteElementString("username", Conf.sRPRMLogin); //FB 2709
                    xWriter.WriteElementString("domain", Conf.cMcu.sRPRMDomain); // FB 2441 II
                }
                xWriter.WriteElementString("participant-name", partyObj.sName);
                xWriter.WriteElementString("connection-type-enum", AddressType(ref partyObj.etAddressType));
                xWriter.WriteElementString("dial-direction-enum", DialDirection(ref partyObj.etConnType));
                xWriter.WriteElementString("dial-number", partyObj.sAddress.Trim());
                xWriter.WriteElementString("chairperson", "false");
                xWriter.WriteElementString("conf-owner", setConfOwner.ToString());
                xWriter.WriteElementString("email-address", partyObj.Emailaddress);
                xWriter.WriteElementString("user-type-enum", "GUEST"); //ALLDEV-779
                xWriter.WriteElementString("mode-enum", "VIDEO"); //ALLDEV-779
                xWriter.WriteElementString("bit-rate-enum", "RATE384"); //ALLDEV-779
                xWriter.WriteElementString("device-id", ""); //ALLDEV-779
                xWriter.WriteEndElement();

                //ZD 101884
                if (Conf.cMcu.sSoftwareVer == "8.0.x")
                    setConfOwner = "false";

            }
            catch (Exception e)
            {
                logger.Exception(100, e.StackTrace);

            }
        }

        internal void SetConfOwnerXML(ref NS_MESSENGER.Conference Conf, ref XmlWriter xWriter) // ZD 101337
        {
            
            try
            {

                xWriter.WriteStartElement("plcm-reserved-participant");
                xWriter.WriteAttributeString("xmlns", "n3", null, "urn:com:polycom:api:rest:plcm-reserved-participant");               
                xWriter.WriteElementString("username", Conf.sRPRMLogin); 
                xWriter.WriteElementString("domain", Conf.cMcu.sRPRMDomain); 
                xWriter.WriteElementString("participant-name", "Conf Owner");
                xWriter.WriteElementString("connection-type-enum", "H323");
                xWriter.WriteElementString("dial-direction-enum", "DIAL_IN");                
                xWriter.WriteElementString("chairperson", "false");
                xWriter.WriteElementString("conf-owner", "true");
                xWriter.WriteElementString("email-address", "admin@myvrm.com");
                xWriter.WriteElementString("user-type-enum", "USER"); //ALLDEV-779
                xWriter.WriteElementString("mode-enum", "IN_PERSON");
                xWriter.WriteElementString("bit-rate-enum", "RATE384"); //ALLDEV-779
                xWriter.WriteElementString("device-id", ""); //ALLDEV-779
                xWriter.WriteEndElement();

            }
            catch (Exception e)
            {
                logger.Exception(100, e.StackTrace);

            }
        }

        #endregion

        #region CreateXML_AddParty
        /// <summary>
        /// CreateXML_AddParty
        /// </summary>
        /// <param name="addPartyXml"></param>
        /// <param name="party"></param>
        /// <param name="confIdOnMcu"></param>
        /// <returns></returns>
        private bool CreateXML_AddParty(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, ref XmlWriter xmlwriter)
        {
            try
            {
                xmlwriter.WriteStartElement("plcm-participant");
                xWriter.WriteAttributeString("xmlns", "n1", null, "urn:com:polycom:api:rest:plcm-participant");
                xWriter.WriteElementString("endpoint-name", party.sName);
                if (party.etAddressType == NS_MESSENGER.Party.eAddressType.SIP) // ZD 101157
                    party.sAddress = "sip:" + party.sAddress;
                xWriter.WriteElementString("dial-string", party.sAddress.Trim());
                xWriter.WriteElementString("passback", DateTime.Now.ToString("HHMM"));
                xWriter.WriteElementString("passthru", conf.cMcu.sRPRMDomain + @"\" + conf.cMcu.sDMALogin); // Example: Local\Larry
                xWriter.WriteEndElement();
                xWriter.Flush();

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_MuteAllExceptParticipant
        /// <summary>
        /// CreateXML_MuteAllExceptParticipant
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="partyList"></param>
        /// <param name="xWriter"></param>
        /// <returns></returns>
        private bool CreateXML_MuteAllExceptParticipant(ref NS_MESSENGER.Conference conf, List<NS_MESSENGER.Party> partyList, ref XmlWriter xWriter)
        {
            try
            {
                xWriter.WriteStartElement("plcm-conference-mute-all-except-request");
                xWriter.WriteAttributeString("xmlns", "n1", null, "urn:com:polycom:api:rest:plcm-conference-mute-all-except-request");

                for (int i = 0; i < partyList.Count; i++)
                {
                    xWriter.WriteStartElement("excluded-participant-identifier");
                    xWriter.WriteElementString("excluded-participant-identifier", partyList[i].sGUID);
                    xWriter.WriteEndElement();
                }
                xWriter.WriteEndElement();
                xWriter.Flush();

                return true;
            }
            catch (Exception e)
            {

                logger.Exception(100, e.StackTrace);
                return false;
            }
        }
        #endregion

        #region CreateXML_GetMcuTime
        /// <summary>
        /// CreateXML_GetMcuTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="getTimeXml"></param>
        /// <returns></returns>
        internal bool CreateXML_GetMcuTime(NS_MESSENGER.MCU mcu, ref string getTimeXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
		 //FB 2709

        #region CreateXML_CreateUser
        /// <summary>
        /// Create User Dynamically
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="reservationXml"></param>
        /// <returns></returns>
        private bool CreateXML_CreateUser(ref NS_MESSENGER.MCU mcu, string login, ref XmlWriter xWriter)
        {
            try
            {

                xWriter.WriteStartElement("plcm-user");
                xWriter.WriteAttributeString("xmlns", "n1", null, "urn:com:polycom:api:rest:plcm-user");

                xWriter.WriteStartElement("plcm-user-role-list");
                xWriter.WriteAttributeString("xmlns", "n3", null, "urn:com:polycom:api:rest:plcm-user-role-list");

                xWriter.WriteStartElement("plcm-user-role");
                xWriter.WriteAttributeString("xmlns", "n2", null, "urn:com:polycom:api:rest:plcm-user-role");

                xWriter.WriteElementString("name", "Scheduler");
                xWriter.WriteEndElement();
                xWriter.WriteEndElement();

                if (login != null && login != "")
                    xWriter.WriteElementString("username", login);

                xWriter.WriteElementString("first-name", login);
                xWriter.WriteElementString("last-name", "Scheduler");
                xWriter.WriteElementString("email-address", mcu.sRPRMEmailaddress);
                xWriter.WriteElementString("password", mcu.sPwd);
                xWriter.WriteElementString("disabled", "false");
                xWriter.WriteElementString("locked", "false");
                xWriter.WriteEndElement();
                xWriter.Flush();
            }

            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }
        #endregion


        #endregion

        #region ResponseXML

        #region Process_ReservationDialString
        /// <summary>
        /// Process_ReservationDialString
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        private bool Process_ReservationDialString(NS_MESSENGER.Conference conf, string responseXml)
        {
            try
            {
                xdoc = XDocument.Parse(responseXml);
                XNamespace ns4 = "urn:com:polycom:api:rest:plcm-reservation";
                if (xdoc.Element(ns4 + "plcm-reservation").Element(ns4 + "dial-in-number") != null)
                    conf.sDialString = xdoc.Element(ns4 + "plcm-reservation").Element(ns4 + "dial-in-number").Value;
                if (xdoc.Element(ns4 + "plcm-reservation").Element(ns4 + "entity-tag") != null)
                    conf.sEtag = xdoc.Element(ns4 + "plcm-reservation").Element(ns4 + "entity-tag").Value;

                if (conf.sDialString.Length > 0)
                {
                    ret = false;
                    ret = db.UpdateConfDailString(conf);
                    if (!ret)
                    {
                        logger.Exception(100, "UpdateConfDailString failed");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

        }
        #endregion

        #region ProcessXML_OngoingConf
        /// <summary>
        /// ProcessXML_OngoingConf
        /// </summary>
        /// <param name="responseXml"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        private bool ProcessXML_OngoingConf(string responseXml, ref NS_MESSENGER.Conference conf)
        {
            // process each conf to get the latest status for each participant
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXml);

                XmlNodeList nodelist;
                XmlNode node;

                // if description = OK , then continue processing
                node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION");
                string description = node.InnerXml;
                description = description.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = description;
                    return false; //error
                }

                // Retreive all conferences	
                nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF/GET/CONFERENCE/ONGOING_PARTY_LIST/ONGOING_PARTY");

                //cycle through each party one-by-one
                foreach (XmlNode node1 in nodelist)
                {
                    // party object 
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    XmlNode partyNode = node1.SelectSingleNode("PARTY");

                    // name , partyID on mcu
                    party.sMcuName = partyNode.ChildNodes.Item(0).InnerText; //NAME
                    party.iMcuId = int.Parse(partyNode.ChildNodes.Item(1).InnerText); //ID

                    //interface type  - ip or isdn
                    //XmlNode interfaceType = node1.SelectSingleNode("INTERFACE");
                    //string interfaceTypeVal = interfaceType.InnerText ; 					
                    string interfaceTypeVal = partyNode.ChildNodes.Item(3).InnerText;
                    interfaceTypeVal = interfaceTypeVal.Trim();
                    if (interfaceTypeVal == "h323")
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                    else
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;

                    // connection type - dial-in or dial-out 
                    //XmlNode connectionType = node1.SelectSingleNode("CONNECTION");
                    //string connectionTypeVal = connectionType.InnerText;
                    string connectionTypeVal = partyNode.ChildNodes.Item(4).InnerText;
                    connectionTypeVal = connectionTypeVal.Trim();
                    if (connectionTypeVal == "dial_in")
                        party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_IN;
                    else
                        party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_OUT;

                    // ip or isdn address 
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                    {
                        //XmlNode ipaddress = node1.SelectSingleNode("IP");						
                        //party.m_mcuAddress =ipaddress.InnerText ; 
                        party.sMcuAddress = partyNode.SelectSingleNode("IP").InnerText;
                    }
                    else
                    {
                        // party is connected on ISDN
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;
                        if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        {
                            // Dial-In : Fetch the address from mcu phone list 
                            //XmlNode isdnaddress = node1.SelectSingleNode("MCU_PHONE_LIST");						
                            party.sMcuAddress = partyNode.SelectSingleNode("MCU_PHONE_LIST").SelectSingleNode("PHONE1").InnerText;
                        }
                        else
                        {
                            // Dial-Out : Fetch the address from the participant phone list
                            //XmlNode isdnaddress = node1.SelectSingleNode("");						
                            party.sMcuAddress = partyNode.SelectSingleNode("BONDING_PHONE").InnerText;
                        }
                    }

                    // party ongoing status
                    XmlNode partyStatus = node1.SelectSingleNode("ONGOING_PARTY_STATUS");
                    string ongoingPartyStatus = partyStatus.ChildNodes.Item(1).InnerText; //description
                    ongoingPartyStatus = ongoingPartyStatus.Trim();
                    if (ongoingPartyStatus.CompareTo("connected") == 0)
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                    else
                    {
                        if (ongoingPartyStatus.CompareTo("connecting") == 0)
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                        else
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                    }
                    logger.Trace("Ongoing party status : " + party.etStatus.ToString());

                    //Audio Mute status 
                    XmlNode partyAudioMute = node1.SelectSingleNode("AUDIO_MUTE");
                    string audioMute = partyAudioMute.InnerXml.Trim();
                    if (audioMute == "true")
                        party.bMute = true;
                    else
                        party.bMute = false;

                    // individual party details added to the resp conf
                    conf.qParties.Enqueue(party);
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region ProcessXML_TerminateConf
        /// <summary>
        /// ProcessXML_TerminateConf
        /// </summary>
        /// <param name="responseXml"></param>
        /// <returns></returns>
        private bool ProcessXML_TerminateConf(string responseXml)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }

            // either correct confirmation not recd or conf doesnot exist on the bridge anymore.		
            return false;
        }
        #endregion

        #endregion

        //ALLDEV-847- START
        #region AddPartyToBlueJean

        /// <summary>
        /// AddPartyToBlueJean
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool AddPartyToBlueJean(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
                DMAorRPRM = (int)NS_MESSENGER.AccessMode.FromService;
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                settings.CheckCharacters = false;
                settings.Encoding = Encoding.UTF8;
                xWriter = XmlWriter.Create(str_build, settings);
                ret = false;
                ret = CreateXML_AddPartyInBJN(conf, party, ref xWriter);


                if (!ret || str_build.ToString().Length < 1)
                {
                    logger.Exception(100, "Creation of AddPartyToBlueJean failed . commonXML = " + str_build.ToString());
                    return false;
                }
                if (str_build.ToString().Contains("utf-16"))
                    str_build.Replace("utf-16", "utf-8");

                if (str_build.ToString().Contains(":n1"))
                    str_build.Replace(":n1", "");

                ContentType = "application/vnd.plcm.plcm-participant+xml";
                RequestMethod = "POST";
                RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants";


                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "AddPartyToBlueJean SendTransaction failed");
                    return false;
                }
                else
                    return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.InnerException.ToString());
                logger.Exception(100, e.StackTrace);
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_CreateXML_AddPartyInBJN
        /// <summary>
        /// CreateXML_CreateXML_AddPartyInBJN
        /// </summary>
        /// <param name="addPartyXml"></param>
        /// <param name="party"></param>
        /// <param name="confIdOnMcu"></param>
        /// <returns></returns>
        private bool CreateXML_AddPartyInBJN(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, ref XmlWriter xmlwriter)
        {
            try
            {
                xmlwriter.WriteStartElement("plcm-participant");
                xWriter.WriteAttributeString("xmlns", "n1", null, "urn:com:polycom:api:rest:plcm-participant");
                xWriter.WriteElementString("dial-string", "199.48.152.152" + "##" + conf.sBJNMeetingid + "#" + conf.sBJNPasscode);
                xWriter.WriteElementString("signaling-protocol", "h323");
                xWriter.WriteElementString("forward-dtmf-source", conf.sBJNMeetingid + "#" + conf.sBJNPasscode);
                xWriter.WriteEndElement();
                xWriter.Flush();

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //ALLDEV-847- END       


    }
}

